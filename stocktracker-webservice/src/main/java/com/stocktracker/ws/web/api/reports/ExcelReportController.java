/*
 * Copyright (c) 2020.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.ws.web.api.reports;

import com.desktopapp.service.reports.ExcelReportService;
import com.stocktracker.domainobjs.json.reports.ExcelReportDO;
import com.stocktracker.exceptions.BusinessException;
import com.stocktracker.exceptions.TechnicalException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.util.List;

@RequestMapping(value="/api/excelreport")
@RestController
public class ExcelReportController{

	/**
	 * The ExcelReportService business service.
	 */
	@Autowired
	private ExcelReportService service;

	@RequestMapping(
			value = "/mandiReconReport/{industryCode}",
			method = RequestMethod.POST)
	public ResponseEntity<InputStreamResource> reteriveMandiReconReport(String industryCode) throws BusinessException, TechnicalException {
		return reteriveMandiReconReport(industryCode,null,null);
	}

	@RequestMapping(
			value = "/mandiReconReport/{industryCode}/{lampName}/{mandiName}",
			method = RequestMethod.POST)
	public ResponseEntity<InputStreamResource> reteriveMandiReconReport(String industryCode, String lampName, String mandiName) throws BusinessException,TechnicalException {

		ByteArrayOutputStream stream = (ByteArrayOutputStream)service.reteriveMandiReconReport(industryCode,lampName,mandiName);

		HttpHeaders headers = new HttpHeaders();
		headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
		headers.add("Pragma", "no-cache");
		headers.add("Expires", "0");

		return ResponseEntity
				.ok()
				.headers(headers)
				.contentType(
						MediaType.parseMediaType("application/"
								+ ""
								+ ""
								+ "octet-stream"))
				.body(new InputStreamResource(new ByteArrayInputStream(stream.toByteArray())));
	}

	public ExcelReportService getService() {
		return service;
	}

}
