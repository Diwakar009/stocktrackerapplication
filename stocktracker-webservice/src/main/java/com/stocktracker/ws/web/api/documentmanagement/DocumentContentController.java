/*
 * Copyright (c) 2020.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.ws.web.api.documentmanagement;

import com.desktopapp.service.AbstractService;
import com.desktopapp.service.DocumentContentService;
import com.stocktracker.domainobjs.json.DocContentDO;
import com.stocktracker.model.DocContent;
import com.stocktracker.model.DocContentBlob;
import com.stocktracker.model.DocHolder;
import com.stocktracker.util.Base64Util;
import com.stocktracker.util.CommonFileUtil;
import com.stocktracker.util.DateTimeUtil;
import com.stocktracker.ws.Response;
import com.stocktracker.ws.web.api.GenericBaseController;
import org.apache.commons.io.FilenameUtils;
import org.apache.tomcat.util.http.fileupload.FileItem;
import org.apache.tomcat.util.http.fileupload.FileItemIterator;
import org.apache.tomcat.util.http.fileupload.FileItemStream;
import org.apache.tomcat.util.http.fileupload.FileUploadException;
import org.apache.tomcat.util.http.fileupload.disk.DiskFileItemFactory;
import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;
import org.aspectj.lang.annotation.After;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/api/documentContent")
public class DocumentContentController extends GenericBaseController<DocContentDO> {

    final String DOCSTORE_HOME_FOLDER = "/DocstoreTemp/";

    @Autowired
    DocumentContentService service;

    @Override
    public DocContentDO find(DocContentDO docContentDO) {
        return service.find(docContentDO);
    }

    @Override
    public DocContentDO save(DocContentDO docContentDO) {
        return (DocContentDO)service.create(docContentDO);
    }

    @Override
    public boolean delete(DocContentDO docContentDO) {
        service.remove(docContentDO);
        return true;
    }

    @Override
    public boolean deleteList(List<DocContentDO> doList){
        service.remove(doList);
        return true;
    }

    @Override
    public AbstractService getService() {
        return service;
    }

    /**
     * Web service endpoint to download the document
     *
     * @return A ResponseEntity with wraped inputstreamresource
     */
    @RequestMapping(
            value = "/downloadDocument",
            method = RequestMethod.POST)
    public ResponseEntity<InputStreamResource> downloadDocument(@RequestBody DocContentDO docContentDO) {

        logger.info("> downloadDocument");
        ByteArrayInputStream in = service.downloadDocument(docContentDO);

        HttpHeaders headers = new HttpHeaders();
        headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
        headers.add("Pragma", "no-cache");
        headers.add("Expires", "0");

        logger.info("< downloadDocument");

        return ResponseEntity
                .ok()
                .headers(headers)
                .contentType(
                        MediaType.parseMediaType("application/"
                                + ""
                                + ""
                                + "octet-stream"))
                .body(new InputStreamResource(in));
    }

    //public  ResponseEntity<Boolean> uploadDocument(@RequestBody(required=true) MultipartFile file,@RequestBody TableRowMapDO<String, String> idmap) {
    //public  ResponseEntity<Boolean> uploadDocument(@PathVariable String holderId,HttpServletRequest request, HttpServletResponse response) {
    //public  ResponseEntity<Boolean> uploadDocument(@PathVariable String holderId,HttpServletRequest request) {

    /*
    boolean isMultipart = ServletFileUpload.isMultipartContent(request);
    if (!isMultipart) {
    	System.out.println("File upload started...." + isMultipart);
    }

    MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
    Map<String,MultipartFile> map = multipartRequest.getFileMap();
    MultipartFile multipartFile = null;
    String fileName = "";
    for(Map.Entry<String,MultipartFile> iterator : map.entrySet() ){
            multipartFile = (MultipartFile)iterator.getValue();
            fileName =  iterator.getValue().getOriginalFilename();
            System.out.println(" fileName " +  multipartFile);
    }
  */
    //System.out.println("inputStream " + inputStrea  );

    /**
     * Web service endpoint to upload the document (Works for unlimited size data)
     *
     * @return
     */
    @RequestMapping(
            value = "/uploadInputStream/{holderId}/{filePath}",
            method = RequestMethod.POST, consumes = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public ResponseEntity<Boolean> uploadInputStream(InputStream file, @PathVariable String holderId, @PathVariable String filePath) {
        logger.info("> uploadDocument");
        Long contentId = new Long(holderId);
        String decodedFilePath = Base64Util.decode(filePath);
        inputStreamToFile(file, decodedFilePath,holderId);
        return ResponseEntity.ok().body(true);
    }


    private byte[] getByteFromInputStream(InputStream is){
        return null;
    }

    private void inputStreamToFile(InputStream is, String filePath,String holderId) {
        try {
            String fileNameWithExt = FilenameUtils.getName(filePath);

            File theDir = new File(DOCSTORE_HOME_FOLDER +"/" + holderId + "/");

            if (!theDir.exists()) {
                System.out.println("creating directory: " + theDir.getName());
                boolean result = false;

                try{
                    theDir.mkdir();
                    result = true;
                }
                catch(SecurityException se){
                    //handle it
                }
                if(result) {
                    System.out.println("DIR created");
                }
            }

            OutputStream os = new FileOutputStream(DOCSTORE_HOME_FOLDER +"/" + holderId + "/" + fileNameWithExt);
            byte[] buffer = new byte[1024];
            int bytesRead;
            //read from is to buffer
            while ((bytesRead = is.read(buffer)) != -1) {
                os.write(buffer, 0, bytesRead);
            }
            is.close();
            //flush OutputStream to write any buffered data to file
            os.flush();
            os.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     *
     * Save n upload file
     * @param request
     * @param docContentDO
     * @return
     */
    @RequestMapping(value = "/saveNUpload", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public @ResponseBody
    Response<String> saveNUpload(HttpServletRequest request, @RequestBody DocContentDO docContentDO ) {

        try {

            boolean isMultipart = ServletFileUpload.isMultipartContent(request);

            if (!isMultipart) {
                // Inform user about invalid request
                Response<String> responseObject = new Response<String>(false, "Not a multipart request.", "");
                return responseObject;
            }
            // Create a new file upload handler
            ServletFileUpload upload = new ServletFileUpload();
            //String decodedFilePath = Base64Util.decode(filePath);
            String decodedFilePath = docContentDO.getDocDirPath();
            FileItemIterator iter = upload.getItemIterator(request);
             while (iter.hasNext()) {

                FileItemStream item = iter.next();
                //String name = item.getFieldName();
                InputStream stream = item.openStream();
                 /*if (!item.isFormField()) {
                    String filename = item.getName();
                    // Process the input stream
                    OutputStream out = new FileOutputStream(filename);
                    IOUtils.copy(stream, out);
                    stream.close();
                    out.close();
                }*/
               inputStreamToFile(stream, decodedFilePath,docContentDO.getDocHolderId().toString());
            }
        } catch (FileUploadException e) {
            return new Response<String>(false, "File upload error", e.toString());
        } catch (IOException e) {
            return new Response<String>(false, "Internal server IO error", e.toString());
        }

        return new Response<String>(true, "Success", "");
    }

    /**
     *
     *  1) Receive the attachment file and copy it the temporary location in the folder - UUID_folderID and file name as UUID.
     *  2) After all the attachment are submitted call commit with all the details.
     *
     * @param request
     * @param holderId
     * @param filePath
     * @return
     */
    @RequestMapping(value = "/uploadWithWait/{holderId}/{filePath}", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public @ResponseBody
    Response<String> uploadWithWait(HttpServletRequest request, @PathVariable String holderId, @PathVariable String filePath) {

        try {
            boolean isMultipart = ServletFileUpload.isMultipartContent(request);
            if (!isMultipart) {
                // Inform user about invalid request
                Response<String> responseObject = new Response<String>(false, "Not a multipart request.", "");
                return responseObject;
            }
            // Create a new file upload handler
            ServletFileUpload upload = new ServletFileUpload();
            String decodedFilePath = Base64Util.decode(filePath);
            FileItemIterator iter = upload.getItemIterator(request);

            while (iter.hasNext()) {

                FileItemStream item = iter.next();
                //String name = item.getFieldName();
                InputStream stream = item.openStream();

               /*if (!item.isFormField()) {
                    String filename = item.getName();
                    // Process the input stream
                    OutputStream out = new FileOutputStream(filename);
                    IOUtils.copy(stream, out);
                    stream.close();
                    out.close();
                }*/

               /*
                DocHolder holder = new DocHolder();
                holder.setId(Long.parseLong(holderId));

                DocContent content = new DocContent();
                content.setDocHolder(holder);
                content.setDocContentName("Sample Content");
                content.setDocDirPath(decodedFilePath);
                content.setDocFileExtension("pdf");
                content.setDocFileName("SampleFileName.pdf");
                content.setDocContentKeywords("Sample Content Keywords");

                DocContentBlob docContentBlob = new DocContentBlob();
                docContentBlob.setDocContent(content);

                byte[] docContentByte = CommonFileUtil.getByteArrayFromInputStream(stream);

                docContentBlob.setDocContentBlob(docContentByte);
                List<DocContentBlob> contentBlobs = new ArrayList<DocContentBlob>();
                contentBlobs.add(docContentBlob);

                content.setDocContentBlobs(contentBlobs);
                service.save(content);
               */
                inputStreamToFile(stream, decodedFilePath,holderId);
            }
        } catch (FileUploadException e) {
            return new Response<String>(false, "File upload error", e.toString());
        } catch (IOException e) {
            return new Response<String>(false, "Internal server IO error", e.toString());
        }

        return new Response<String>(true, "Success", "");
    }

    /**
     * Upload with dummy details
     *
     * @param request
     * @param holderId
     * @param filePath
     * @return
     */

    @RequestMapping(value = "/upload/{holderId}/{filePath}", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public @ResponseBody
    Response<String> upload(HttpServletRequest request, @PathVariable String holderId, @PathVariable String filePath) {
        try {
            boolean isMultipart = ServletFileUpload.isMultipartContent(request);
            if (!isMultipart) {
                // Inform user about invalid request
                Response<String> responseObject = new Response<String>(false, "Not a multipart request.", "");
                return responseObject;
            }
            // Create a new file upload handler
            ServletFileUpload upload = new ServletFileUpload();
            String decodedFilePath = Base64Util.decode(filePath);
            FileItemIterator iter = upload.getItemIterator(request);

            while (iter.hasNext()) {

                FileItemStream item = iter.next();
                //String name = item.getFieldName();
                InputStream stream = item.openStream();

               /*if (!item.isFormField()) {
                    String filename = item.getName();
                    // Process the input stream
                    OutputStream out = new FileOutputStream(filename);
                    IOUtils.copy(stream, out);
                    stream.close();
                    out.close();
                }*/

                DocHolder holder = new DocHolder();
                holder.setId(Long.parseLong(holderId));

                DocContent content = new DocContent();
                content.setDocHolder(holder);
                content.setDocContentName("Sample Content");
                content.setDocDirPath(decodedFilePath);
                content.setDocFileExtension("pdf");
                content.setDocFileName("SampleFileName.pdf");
                content.setDocContentKeywords("Sample Content Keywords");

                DocContentBlob docContentBlob = new DocContentBlob();
                docContentBlob.setDocContent(content);

                byte[] docContentByte = CommonFileUtil.getByteArrayFromInputStream(stream);

                docContentBlob.setDocContentBlob(docContentByte);
                List<DocContentBlob> contentBlobs = new ArrayList<DocContentBlob>();
                contentBlobs.add(docContentBlob);

                content.setDocContentBlobs(contentBlobs);
                service.create(new DocContentDO(content));

                //inputStreamToFile(stream, decodedFilePath);
            }
        } catch (FileUploadException e) {
            return new Response<String>(false, "File upload error", e.toString());
        } catch (IOException e) {
            return new Response<String>(false, "Internal server IO error", e.toString());
        }

        return new Response<String>(true, "Success", "");
    }




}
