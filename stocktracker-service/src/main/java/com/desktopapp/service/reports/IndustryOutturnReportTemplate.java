/*
 * Copyright (c) 2022.
 *
 * All Rights are with Diwakar C
 */

package com.desktopapp.service.reports;

import org.apache.poi.ss.usermodel.*;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class IndustryOutturnReportTemplate implements ExcelReportTemplate{

    public static Integer NO_OF_COLUMNS = Integer.valueOf(3);
    public static Integer INDEX_OF_DATA_ROW = Integer.valueOf(3);

    public static final Character COLUMNS[] = {'B','D'};

    public Workbook workbook;
    public Map<Integer, CellStyle> cellStyleMap;
    public Map<Integer, CellStyle> highlightedCellStyleMap;
    public Map<Integer, CellStyle> warningCellStyleMap;
    public Map<Integer, CellStyle> greenHighlightedCellStyleMap;
    public Map<Integer, CellStyle> summaryCellStyleMap;
    public static final String INDUSTRY_OUTTURN_TEMPLATE = "../templates/IndustryOutturnTemplate.xls";

    public IndustryOutturnReportTemplate(Workbook workbook) {
        this.workbook = workbook;
        this.cellStyleMap = getTemplateCellStyles(this.workbook);
        this.highlightedCellStyleMap=getTemplateHighLightCellStyles(this.workbook,this.cellStyleMap);
    }
    private Map<Integer, CellStyle> getTemplateCellStyles(Workbook workbook) {
        Map<Integer, CellStyle> cellStyleMap = new HashMap<>();
        Integer numberOfCol = NO_OF_COLUMNS;
        Integer row = INDEX_OF_DATA_ROW;
        Sheet sheet = workbook.getSheet("Template");
        if(sheet != null){
            for (int col = 0; col < numberOfCol; col++) {
                cellStyleMap.put(col, sheet.getRow(row).getCell(col).getCellStyle());
            }
        }
        return cellStyleMap;
    }

    private Map<Integer, CellStyle> getTemplateHighLightCellStyles(Workbook workbook, Map<Integer, CellStyle> cellStyleMap) {
        Map<Integer, CellStyle> highLightedCellStyleMap = new HashMap<>();
        cellStyleMap.forEach((integer, cellStyle) -> {
            CellStyle cellStyle1 = workbook.createCellStyle();
            cellStyle1.cloneStyleFrom(cellStyle);
            cellStyle1.setFillForegroundColor(IndexedColors.LIGHT_CORNFLOWER_BLUE.getIndex());
            cellStyle1.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            highLightedCellStyleMap.put(integer, cellStyle1);
        });
        return highLightedCellStyleMap;
    }

    public Map<Integer, CellStyle> getCellStyleMap() {
        return cellStyleMap;
    }

    @Override
    public Workbook getWorkbook() {
        return workbook;
    }
}
