package com.desktopapp.fx.view;


import com.desktopapp.fx.DesktopFxApplication;
import com.desktopapp.fx.app.AppFontIcons;
import com.desktopapp.fx.common.InitCompletionHandler;
import com.desktopapp.fx.control.fonticon.FontIconColor;
import com.desktopapp.fx.control.fonticon.FontIconFactory;
import com.desktopapp.fx.control.fonticon.FontIconSize;
import com.desktopapp.fx.manager.LoginManager;
import com.desktopapp.fx.util.I18n;
import com.desktopapp.fx.util.ViewHelpers;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import org.apache.commons.lang.StringUtils;
import org.controlsfx.control.PopOver;
import org.controlsfx.control.textfield.CustomPasswordField;
import org.controlsfx.control.textfield.CustomTextField;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Login view
 *
 * @author diwakar009
 */
public class LoginView extends StackPane {


    private CustomTextField tfUsername;
    private CustomPasswordField pfPassword;
    private Hyperlink linkForgotPassword;
    private InitCompletionHandler loginActionHandler;
    private LoginManager loginManager;


    public LoginView(LoginManager loginManager) {
        super();
        this.loginManager = loginManager;
        buildView();
    }

    private void buildView() {
        getStyleClass().add("login-view");

        // info
        Label appFontIcon = new Label();
        appFontIcon.setGraphic(FontIconFactory.createIcon(AppFontIcons.APP, FontIconSize.XL, FontIconColor.GREEN));
        appFontIcon.getStyleClass().add("info-app-icon");

        Label infoTitle = new Label(I18n.COMMON.getString("login.infoTitle"));
        infoTitle.setWrapText(true);
        infoTitle.getStyleClass().add("info-title");

        VBox infoBox = new VBox();
        infoBox.getStyleClass().add("info");
        infoBox.getChildren().addAll(appFontIcon, infoTitle);

        // form
        Label formTitle = new Label(I18n.COMMON.getString("login.formTitle") + "( " + DesktopFxApplication.getLoginSession().getPreferences().getEnvironment() + " )");
        formTitle.getStyleClass().add("form-title");

        tfUsername = new CustomTextField();
        tfUsername.setLeft(FontIconFactory.createIcon(AppFontIcons.APP_USER));
        tfUsername.setPromptText(I18n.COMMON.getString("login.username"));

        pfPassword = new CustomPasswordField();
        pfPassword.setLeft(FontIconFactory.createIcon(AppFontIcons.APP_PASSWORD));
        pfPassword.setPromptText(I18n.COMMON.getString("login.password"));

        linkForgotPassword = new Hyperlink(I18n.COMMON.getString("login.forgotPassword"));
        linkForgotPassword.setOnAction(event -> onForgotPassword());

        Button btnLogin = ViewHelpers.createFontIconButton(I18n.COMMON.getString("login.login"), AppFontIcons.LOGIN,
                FontIconSize.XS, ContentDisplay.RIGHT);
        btnLogin.setDefaultButton(true);
        btnLogin.setOnAction(event -> onLogin());
        GridPane.setHalignment(btnLogin, HPos.RIGHT);

        GridPane loginForm = new GridPane();
        loginForm.getStyleClass().add("form");
        loginForm.add(formTitle, 0, 0, 2, 1);
        loginForm.add(tfUsername, 0, 1, 2, 1);
        loginForm.add(pfPassword, 0, 2, 2, 1);
        loginForm.add(linkForgotPassword, 0, 3);
        loginForm.add(btnLogin, 1, 3);

        HBox loginBox = new HBox();
        loginBox.getStyleClass().add("login-box");
        loginBox.getChildren().addAll(infoBox, loginForm);

        tfUsername.setText("diwakar009");
        pfPassword.setText("diwakar123");

        getChildren().add(loginBox);
    }

    private void onLogin() {
        if (StringUtils.isNotEmpty(tfUsername.getText()) && StringUtils.isNotEmpty(pfPassword.getText())) {
            this.loginManager.login(tfUsername.getText(), pfPassword.getText());
        }
    }

    private void onForgotPassword() {
        Text text = new Text(I18n.COMMON.getString("login.forgotPasswordInfo"));
        VBox content = new VBox(10);
        content.setPadding(new Insets(10, 10, 0, 10));
        content.getChildren().add(text);

        PopOver popOver = new PopOver(content);
        popOver.setArrowLocation(PopOver.ArrowLocation.TOP_CENTER);
        popOver.show(linkForgotPassword);
    }
}
