/*
 * Copyright (c) 2020.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.excel;


import com.stocktracker.exceptions.ExcelCellRenderingException;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;

import java.util.Date;
import java.util.List;

public class ExcelUtils {

    public static void setCellStyle(Row currentRow, int col,CellStyle cellStyle) throws ExcelCellRenderingException{
        try {
            if (cellStyle != null) {
                if(currentRow.getCell(col) == null) {
                    currentRow.createCell(col);
                }
                currentRow.getCell(col).setCellStyle(cellStyle);
            }
        }catch (Exception e){
            throw new ExcelCellRenderingException(e.getMessage());
        }
    }

    public static void setCellFormula(Row currentRow, int col, String formula) throws ExcelCellRenderingException {
        try {
            if(currentRow.getCell(col) == null) {
                currentRow.createCell(col);
            }
            currentRow.getCell(col).setCellFormula(formula);
        }catch (Exception e){
            throw new ExcelCellRenderingException(e.getMessage());
        }
    }

    public static void setCellValue(Row currentRow, int col, Object value) throws ExcelCellRenderingException {
        try {
            if (value instanceof Date) {
                currentRow.getCell(col).setCellValue(value != null ? (Date) value : null);
            } else if (value instanceof Long) {
                currentRow.getCell(col).setCellValue(value != null ? ((Long) value).longValue() : null);
            } else if (value instanceof Double) {
                currentRow.getCell(col).setCellValue(value != null ? ((Double) value).doubleValue() : null);
            } else {
                currentRow.getCell(col).setCellValue(value != null ? (String) value : "");
            }
        }catch (Exception e){
            throw new ExcelCellRenderingException(e.getMessage());
        }
    }

    public static void setCellValue(Row currentRow, int col, Object value, CellStyle cellStyle) throws ExcelCellRenderingException {
        try {
            /*if (cellStyle != null) {
                if(currentRow.getCell(col) == null) {
                    currentRow.createCell(col);
                }
                currentRow.getCell(col).setCellStyle(cellStyle);
            }*/
            setCellStyle(currentRow,col,cellStyle);
            setCellValue(currentRow, col, value);
        }catch (ExcelCellRenderingException ee){
            throw ee;
        }catch (Exception e){
            throw new ExcelCellRenderingException(e.getMessage());
        }
    }

    public static void setAutoFilter(Sheet sheet,int startRow,int lastRow, int startCol,int lastCol){
        sheet.setAutoFilter(new CellRangeAddress(startRow, lastRow , startCol, lastCol));
    }


}
