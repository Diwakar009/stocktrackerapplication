/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.domainobjs.json.sms;

import com.stocktracker.domainobjs.json.BaseDomainObject;
import com.stocktracker.model.BaseEntity;
import com.stocktracker.model.sms.DeliveryDetails;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * DeliveryDetailsDO .
 * 
 */
@JsonIgnoreProperties(ignoreUnknown=true)
@JsonInclude(Include.NON_EMPTY)
public class DeliveryDetailsDO extends BaseDomainObject implements Serializable {
	private static final long serialVersionUID = 1L;

	public static String CLASS_NAME = ContractDO.class.getName();

	public DeliveryDetailsDO() {
	}

	public DeliveryDetailsDO(DeliveryDetails deliveryDetails) {
		copyEntity2DomainObject(deliveryDetails);
	}

	@JsonProperty("id")
	private Long id;

	@JsonProperty("deliveryTypeCode")
	private String deliveryTypeCode;

	@JsonProperty("deliveryDepo")
	private String deliveryDepo;

	@JsonProperty("deliveryLatNo")
	private Long deliveryLatNo;

	@JsonProperty("deliveryAddress")
	private String deliveryAddress;

	@JsonProperty("moreDetails")
	private String moreDetails;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMoreDetails() {
		return moreDetails;
	}

	public void setMoreDetails(String moreDetails) {
		this.moreDetails = moreDetails;
	}

	public String getDeliveryTypeCode() {
		return deliveryTypeCode;
	}

	public void setDeliveryTypeCode(String deliveryTypeCode) {
		this.deliveryTypeCode = deliveryTypeCode;
	}

	public String getDeliveryDepo() {
		return deliveryDepo;
	}

	public void setDeliveryDepo(String deliveryDepo) {
		this.deliveryDepo = deliveryDepo;
	}

	public Long getDeliveryLatNo() {
		return deliveryLatNo;
	}

	public void setDeliveryLatNo(Long deliveryLatNo) {
		this.deliveryLatNo = deliveryLatNo;
	}

	public String getDeliveryAddress() {
		return deliveryAddress;
	}

	public void setDeliveryAddress(String deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}

	@JsonIgnore
	@Override
	public Object getDomainObjectKey() {
		return getId();
	}

	@Override
	public DeliveryDetails copyDomainObject2Entity(BaseEntity entity) {

		DeliveryDetails deliveryDetails = null;

		if(entity == null){
			deliveryDetails = new DeliveryDetails();
		}else {
			deliveryDetails = (DeliveryDetails)entity;
		}

		deliveryDetails.setCreatedBy(this.getCreatedBy());
		deliveryDetails.setUpdatedBy(this.getUpdatedBy());
		deliveryDetails.setCreatedAt(this.getCreatedAt());
		deliveryDetails.setUpdatedAt(this.getUpdatedAt());

		deliveryDetails.setId(this.getId());
		deliveryDetails.setDeliveryTypeCode(this.getDeliveryTypeCode());
		deliveryDetails.setDeliveryDepo(this.getDeliveryDepo());
		deliveryDetails.setDeliveryAddress(this.getDeliveryAddress());
		deliveryDetails.setDeliveryLatNo(this.getDeliveryLatNo());
		deliveryDetails.setMoreDetails(this.getMoreDetails());
		return deliveryDetails;
	}
	@Override
	public void copyEntity2DomainObject(BaseEntity entity) {
		DeliveryDetails deliveryDetails = (DeliveryDetails)entity;
		this.setCreatedBy(deliveryDetails.getCreatedBy());
		this.setUpdatedBy(deliveryDetails.getUpdatedBy());
		this.setCreatedAt(deliveryDetails.getCreatedAt());
		this.setUpdatedAt(deliveryDetails.getUpdatedAt());

		this.setId(deliveryDetails.getId());
		this.setDeliveryTypeCode(deliveryDetails.getDeliveryTypeCode());
		this.setDeliveryDepo(deliveryDetails.getDeliveryDepo());
		this.setDeliveryAddress(deliveryDetails.getDeliveryAddress());
		this.setDeliveryLatNo(deliveryDetails.getDeliveryLatNo());
		this.setMoreDetails(deliveryDetails.getMoreDetails());
	}

	@Override
	public String toString() {
		return "DeliveryDetailsDO{" +
				"id=" + id +
				", deliveryTypeCode='" + deliveryTypeCode + '\'' +
				", deliveryDepo='" + deliveryDepo + '\'' +
				", deliveryLatNo='" + deliveryLatNo + '\'' +
				", deliveryAddress='" + deliveryAddress + '\'' +
				", moreDetails='" + moreDetails + '\'' +
				'}';
	}
}