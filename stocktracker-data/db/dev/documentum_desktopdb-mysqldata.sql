-- Documentum Desktop Database Data
-- Version 1.0

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

USE documentum_desktopdb;

select * from doc_content_blob; 

--
-- Dumping data for table country
--
SET AUTOCOMMIT=0;

select * from static_code_decode;


INSERT INTO static_code_decode (CODE_NAME,LANGUAGE,CODE_VALUE,CODE_DESC,created_by,created_at,updated_by,updated_at) 
VALUES ('CD_ACCESSLEVEL','EN_US','AD','Admin',NULL,'2015-08-16 22:14:54','Admin User',NULL);
INSERT INTO static_code_decode (CODE_NAME,LANGUAGE,CODE_VALUE,CODE_DESC,created_by,created_at,updated_by,updated_at) 
VALUES ('CD_ACCESSLEVEL','EN_US','RO','Read Only',NULL,'2015-08-16 22:15:14','Admin User',NULL);
INSERT INTO static_code_decode (CODE_NAME,LANGUAGE,CODE_VALUE,CODE_DESC,created_by,created_at,updated_by,updated_at) 
VALUES ('CD_ACCESSLEVEL','EN_US','RW','Read Write',NULL,'2015-08-16 22:15:26','Admin User',NULL);
INSERT INTO static_code_decode (CODE_NAME,LANGUAGE,CODE_VALUE,CODE_DESC,created_by,created_at,updated_by,updated_at) 
VALUES ('CD_DOCTYPE','EN_US','OF','Official','Admin User','2015-07-08 21:01:33',NULL,NULL);
INSERT INTO static_code_decode (CODE_NAME,LANGUAGE,CODE_VALUE,CODE_DESC,created_by,created_at,updated_by,updated_at) 
VALUES ('CD_DOCTYPE','EN_US','OT','Others','Admin User','2015-07-08 21:02:08',NULL,NULL);
INSERT INTO static_code_decode (CODE_NAME,LANGUAGE,CODE_VALUE,CODE_DESC,created_by,created_at,updated_by,updated_at) 
VALUES ('CD_DOCTYPE','EN_US','PE','Personal','Admin User','2015-07-08 20:59:59',NULL,NULL);
INSERT INTO static_code_decode (CODE_NAME,LANGUAGE,CODE_VALUE,CODE_DESC,created_by,created_at,updated_by,updated_at) 
VALUES ('CD_FILETYPE','EN_US','doc','Word Document (DOC)','Admin User','2015-07-08 21:05:54',NULL,NULL);
INSERT INTO static_code_decode (CODE_NAME,LANGUAGE,CODE_VALUE,CODE_DESC,created_by,created_at,updated_by,updated_at)
VALUES ('CD_FILETYPE','EN_US','docx','Word Document (DOCX)','Admin User','2015-07-08 21:05:19',NULL,NULL);
INSERT INTO static_code_decode (CODE_NAME,LANGUAGE,CODE_VALUE,CODE_DESC,created_by,created_at,updated_by,updated_at) 
VALUES ('CD_FILETYPE','EN_US','jpg','JPG File','Admin User','2015-07-08 21:04:19',NULL,NULL);
INSERT INTO static_code_decode (CODE_NAME,LANGUAGE,CODE_VALUE,CODE_DESC,created_by,created_at,updated_by,updated_at) 
VALUES ('CD_FILETYPE','EN_US','pdf','PDF Document','Admin User','2015-07-08 21:03:14',NULL,NULL);
INSERT INTO static_code_decode (CODE_NAME,LANGUAGE,CODE_VALUE,CODE_DESC,created_by,created_at,updated_by,updated_at) 
VALUES ('CD_FILETYPE','EN_US','xls','Excel Document','Admin User','2015-07-08 21:06:36',NULL,NULL);
INSERT INTO static_code_decode (CODE_NAME,LANGUAGE,CODE_VALUE,CODE_DESC,created_by,created_at,updated_by,updated_at) 
VALUES ('CD_LANGUAGE','EN_US','EN_US','English','Admin User','2015-07-08 20:36:05',NULL,NULL);
INSERT INTO static_code_decode (CODE_NAME,LANGUAGE,CODE_VALUE,CODE_DESC,created_by,created_at,updated_by,updated_at) 
VALUES ('CD_LANGUAGE','HI_IN','HI_IN','Hindi','Admin User','2015-07-08 20:36:05',NULL,NULL);
INSERT INTO static_code_decode (CODE_NAME,LANGUAGE,CODE_VALUE,CODE_DESC,created_by,created_at,updated_by,updated_at) 
VALUES ('CD_SECQUES','EN_US','BM','What is your birth day month',NULL,'2015-09-01 08:32:42','Admin User',NULL);
INSERT INTO static_code_decode (CODE_NAME,LANGUAGE,CODE_VALUE,CODE_DESC,created_by,created_at,updated_by,updated_at) 
VALUES ('CD_SECQUES','EN_US','MM','What is your mother maiden name',NULL,'2015-09-01 08:32:01','Admin User',NULL);
INSERT INTO static_code_decode (CODE_NAME,LANGUAGE,CODE_VALUE,CODE_DESC,created_by,created_at,updated_by,updated_at) 
VALUES ('CD_SECQUES','EN_US','PN','What is your pet name',NULL,'2015-09-01 08:31:26','Admin User',NULL);
INSERT INTO static_code_decode (CODE_NAME,LANGUAGE,CODE_VALUE,CODE_DESC,created_by,created_at,updated_by,updated_at)
VALUES ('CD_USERTITLE','EN_US','MR','Mr','Admin User','2015-08-16 23:19:01',NULL,NULL);
INSERT INTO static_code_decode (CODE_NAME,LANGUAGE,CODE_VALUE,CODE_DESC,created_by,created_at,updated_by,updated_at) 
VALUES ('CD_USERTITLE','EN_US','MRS','Mrs',NULL,'2015-08-16 23:19:36','Admin User',NULL);
INSERT INTO static_code_decode (CODE_NAME,LANGUAGE,CODE_VALUE,CODE_DESC,created_by,created_at,updated_by,updated_at) 
VALUES ('CD_USERTITLE','EN_US','MS','Ms',NULL,'2015-08-16 23:19:18','Admin User',NULL);
INSERT INTO static_code_decode (CODE_NAME,LANGUAGE,CODE_VALUE,CODE_DESC,created_by,created_at,updated_by,updated_at) 
VALUES ('CD_USERTITLE','EN_US','MSTR','Mstr',NULL,'2015-08-16 23:19:51','Admin User',NULL);


INSERT INTO static_code_decode (CODE_NAME,LANGUAGE,CODE_VALUE,CODE_DESC,created_by,created_at,updated_by,updated_at) 
VALUES ('CD_YESNO','EN_US','Y','Yes',NULL,'2015-08-16 23:19:18','Admin User',NULL);
INSERT INTO static_code_decode (CODE_NAME,LANGUAGE,CODE_VALUE,CODE_DESC,created_by,created_at,updated_by,updated_at) 
VALUES ('CD_YESNO','EN_US','N','No',NULL,'2015-08-16 23:19:51','Admin User',NULL);

commit;
--
-- Dumping data for table country
--
SET AUTOCOMMIT=0;+
INSERT INTO country 
VALUES (1, 'TR', 'Turkey', NULL, 'Admin', '2013-10-12 14:34:29', NULL, NULL),
(2, 'DE', 'Germany', NULL, 'Admin', '2013-10-12 14:34:30', NULL, NULL),
(3, 'GB', 'England', NULL, 'Admin', '2013-10-12 14:34:31', NULL, NULL);
COMMIT;

--
-- Dumping data for table category
--
SET AUTOCOMMIT=0;
INSERT INTO category 
VALUES (1, 'Discounter', NULL, 'Admin', '2013-10-12 14:34:32', NULL, NULL),
(2, 'Reseller', NULL, 'Admin', '2013-10-12 14:34:33', NULL, NULL),
(3, 'End user', NULL, 'Admin', '2013-10-12 14:34:34', NULL, NULL);
COMMIT;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

select * from country;

