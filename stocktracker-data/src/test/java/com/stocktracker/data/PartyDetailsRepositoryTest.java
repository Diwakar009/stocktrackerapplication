/*
 * Copyright (c) 2020.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.data;

import com.stocktracker.jpa.spring.extended.PartyDetailsRepository;
import com.stocktracker.jpa.spring.extended.config.ExtendedRepositoryConfig;
import com.stocktracker.model.sms.PartyDetails;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by diwakar009 on 17/12/2018.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = { ExtendedRepositoryConfig.class })
public class PartyDetailsRepositoryTest {

    @Resource
    private PartyDetailsRepository partyDetailsRepository;

    @Before
    public void setup() {


    }

    @Test
    public void dataSetup(){



    }

    @Test
    public void testFindPartyName(){
       List<PartyDetails> partyNames = partyDetailsRepository.findPartyNames("%Beh%");
       partyNames.forEach(partyName -> System.out.println(partyName.getPartyName1()));
    }

}
