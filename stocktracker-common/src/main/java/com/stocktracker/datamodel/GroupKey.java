/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.datamodel;

public interface GroupKey {
    public String getGroupKey();
   
}
