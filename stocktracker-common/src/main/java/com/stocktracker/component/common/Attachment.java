/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.component.common;

import java.io.Serializable;
import java.util.Objects;

/**
 * The class for email attachment
 * 
 * @author "Diwakar Choudhury"
 *
 */
public class Attachment implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Indicates whether the attachment is to be displayed inline as part of content
	 */
	private final boolean inline;
	
	/**
	 * The content Id (for use in a {@code cid:} URL). Mandatory if 
	 * {@code inline} is true; may be null otherwise
	 */
	private final String contentId;
	
	
	/**
	 * A suggested file name for the attachment (for use in a {@code Content-Disposition} header).
	 * Mandatory if {@code inline} is false; may be null otherwise.
	 */
	private final String filename;
	
	
	/**
	 * The content tpe of the attachment in RFC 2046 syntax
	 * (for use in a{@code Content-Type} header); never null
	 */
	private final String mimeType;
	
	/**
	 * The attachment data as supplied by the trigger system
	 * never null. Binary data ought to be base-64 encoded,
	 * but this is not checked by this class
	 * 
	 */
	private final String content;

	
	/**
	 * Creates a new Instance with the specified properties.
	 * The {@code mimeType} and {@code content} properties.
	 * must always be specified. A {@code inLine} is {@code true} and a
	 * {@code filename} must be specified if {@code inline} is false
	 * 
	 * 
	 * @param inline
	 * @param contentId
	 * @param filename
	 * @param mimeType
	 * @param content
	 */
	
	public Attachment(boolean inline, String contentId, String filename,
			String mimeType, String content) {
		
		if(inline){
			Objects.requireNonNull(contentId,"Null 'contentId' argument passed to Attachment constructor for inline attachment");
		}else{
			Objects.requireNonNull(filename,"Null 'filename' argument passed to Attachment constructor for out of line attachment");
		}
		
		
		this.inline = inline;
		this.contentId = contentId;
		this.filename = filename;
		this.mimeType = Objects.requireNonNull(mimeType,"Null 'mimeType' argument passed to Attachment constructor");
		this.content = Objects.requireNonNull(content,"Null 'content' argument passed to Attachment constructor");;
	}
	
	
	/**
	 * Indicates whether the attachment should be presented embeded
	 * into the notification content or as a seperate document
	 * @return
	 */
	public boolean isInline(){
		return inline;
	}


	public String getContentId() {
		return contentId;
	}


	public String getFilename() {
		return filename;
	}


	public String getMimeType() {
		return mimeType;
	}


	public String getContent() {
		return content;
	}


	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder(150);
		sb.append(getClass().getSimpleName()).append("(");
		if(inline){
			sb.append("inLine, ");
			sb.append(", contentId = ").append(contentId);
		}else{
			sb.append("out-of-line, ");
			sb.append(", filename = ").append(filename);
		}
		
		sb.append(", mimeType = ").append(mimeType);
		sb.append(", content = ").append(content.length()).append(" chars (in base-64)");
		sb.append(")");
		
		return sb.toString();
	}
	
	
	
	
	

}
