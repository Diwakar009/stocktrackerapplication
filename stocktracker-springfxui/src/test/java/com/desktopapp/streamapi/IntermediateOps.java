/*
 * Copyright (c) 2020.
 *
 * All Rights are with Diwakar C
 */

package com.desktopapp.streamapi;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class IntermediateOps {

    public static void main(String args[]){
        List<Integer> number = Arrays.asList(2,4,6,8,10);
        List<Integer> square = number.stream().map(x -> x * x ).collect(Collectors.toList());
        square.forEach(sq -> {System.out.println(sq);});

        int even =  number.stream().filter(x -> x % 2==0).reduce(0,(ans,i)-> ans+i);

        System.out.println(even);



    }

}
