/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.desktopapp.fx.control;

import com.desktopapp.fx.common.IGenericComponent;
import com.desktopapp.fx.common.Quantity;
import com.desktopapp.fx.util.I18n;
import javafx.scene.Node;
import javafx.scene.layout.HBox;
import org.controlsfx.validation.ValidationSupport;
import org.controlsfx.validation.Validator;

import java.math.BigDecimal;


/**
 * Quantity field for number of bags and total weightment.
 *
 * Created by diwakar009 on 3/12/2018.
 */
public class QuantityField implements IGenericComponent{

    public static Quantity BLANK_QUANTITY = new Quantity(new BigDecimal(0.0));
    public static BigDecimal PKT_DEFAULT_WEIGHTMENT = new BigDecimal(0.50);

    public static String QUANTITY_PKT_UNIT = "PKT";
    public static String QUANTITY_WEIGHTMENT_UNIT = "QTL";

    protected IntegerField ifNoOfPackets;
    protected BigDecimalField bfWeightment;


    public QuantityField() {
        init();
    }

    public QuantityField(Quantity quantity) {
        init();
    }

    public void init(){
        this.ifNoOfPackets = new IntegerField();
        this.ifNoOfPackets.setPrefWidth(100);

        this.bfWeightment = new BigDecimalField(2);
        this.bfWeightment.setPrefWidth(100);

        ifNoOfPackets.addValueChangeListener(() -> {
            Long noOfPackets = ifNoOfPackets.getValue();
            if(noOfPackets != null) {
                bfWeightment.setDataValue(new BigDecimal(noOfPackets.longValue()).multiply(PKT_DEFAULT_WEIGHTMENT));
            }
        });

    }

    public Node asControl(){
        HBox hBox = new HBox();
        this.ifNoOfPackets.setPromptText(QUANTITY_PKT_UNIT);
        hBox.getChildren().add(this.ifNoOfPackets);
        this.bfWeightment.setPromptText(QUANTITY_WEIGHTMENT_UNIT);
        hBox.getChildren().add(this.bfWeightment);
        hBox.setSpacing(0);
        return hBox;
    }

    @Override
    public void setAsReadOnly(boolean readOnly) {
         this.ifNoOfPackets.setAsReadOnly(readOnly);
         this.bfWeightment.setAsReadOnly(readOnly);
    }

    @Override
    public void destroyComponent() {

    }

    @Override
    public boolean isDataExchangeApplicable() {
        return true;
    }

    @Override
    public void setDataValue(String value) {

    }

    public void setDataValue(Quantity quantity) {
        if(quantity != null) {

            if(quantity.getNumberOfPackets() != null) {
                this.ifNoOfPackets.setDataValue(quantity.getNumberOfPackets().toPlainString());
            }
            if(quantity.getWeightment() != null) {
                this.bfWeightment.setDataValue(quantity.getWeightment().toPlainString());
            }
        }
    }

    @Override
    public String getDataValue() {
        return getQuantity().toString();
    }


    public Quantity getQuantity() {
        Quantity quantity = new Quantity();
        if(this.ifNoOfPackets.getValue() != null && this.bfWeightment.getValue() != null) {
            quantity.setNumberOfPackets(new BigDecimal(this.ifNoOfPackets.getValue()));
            quantity.setWeightment(this.bfWeightment.getValue());
        }
        return quantity;
    }

    @Override
    public boolean isComponentShowing() {
        return (ifNoOfPackets.isComponentShowing() && bfWeightment.isComponentShowing());
    }

    @Override
    public boolean validateInput() {
        return false;
    }

    @Override
    public void clearDataValue() {
        this.ifNoOfPackets.clearDataValue();
        this.bfWeightment.clearDataValue();
    }

    @Override
    public boolean isReadonly() {
        return (ifNoOfPackets.isReadonly() && bfWeightment.isReadonly());
    }

    public IntegerField getIfNoOfPackets() {
        return ifNoOfPackets;
    }

    public void setIfNoOfPackets(IntegerField ifNoOfPackets) {
        this.ifNoOfPackets = ifNoOfPackets;
    }

    public BigDecimalField getBfWeightment() {
        return bfWeightment;
    }

    public void setBfWeightment(BigDecimalField bfWeightment) {
        this.bfWeightment = bfWeightment;
    }
}
