/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.configuration;

public class RestWebService {
	private String host;
	private String port;
	private String baseURL = null;
	
	public String getHost() {
		return host;
	}
	public void setHost(String host) {
		this.host = host;
	}
	public String getPort() {
		return port;
	}
	public void setPort(String port) {
		this.port = port;
	}
	
	public String getWebserviceBaseURL(){
		if(baseURL == null){
			baseURL = "https://" + host +":" + port + "/api/";
		}
		
		return baseURL;
	}
	@Override
	public String toString() {
		return getWebserviceBaseURL();
	}
	
}
