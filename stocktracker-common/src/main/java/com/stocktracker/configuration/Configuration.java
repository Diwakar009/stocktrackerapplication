/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.configuration;

public class Configuration {
	
	private GlobalProperties globalProperties;
	private RestWebService restWebService;

	public RestWebService getRestWebService() {
		return restWebService;
	}

	public void setRestWebService(RestWebService restWebService) {
		this.restWebService = restWebService;
	}

	public GlobalProperties getGlobalProperties() {
		return globalProperties;
	}

	public void setGlobalProperties(GlobalProperties globalProperties) {
		this.globalProperties = globalProperties;
	}

	

}
