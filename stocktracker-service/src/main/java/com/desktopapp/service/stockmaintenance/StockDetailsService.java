/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.desktopapp.service.stockmaintenance;


import com.desktopapp.bean.ContractChangeIdentier;
import com.desktopapp.bean.StockChangeIndentier;
import com.desktopapp.exceptions.NoOpenContractAvailableException;
import com.desktopapp.service.AbstractService;
import com.desktopapp.service.StaticCodeDecodeService;
import com.stocktracker.component.constants.CodeDecodeConstants;
import com.stocktracker.domainobjs.json.CodeDataItemDO;
import com.stocktracker.domainobjs.json.DOPage;
import com.stocktracker.domainobjs.json.ResultDO;
import com.stocktracker.domainobjs.json.sms.*;
import com.stocktracker.jpa.spring.extended.StockDetailsRepository;
import com.stocktracker.jpa.spring.extended.bean.MandiReconcilationData;
import com.stocktracker.jpa.spring.extended.bean.StockDetailsSummary;
import com.stocktracker.model.BaseEntity;
import com.stocktracker.model.sms.Stock;
import com.stocktracker.model.sms.StockDetails;
import com.stocktracker.util.DateTimeUtil;
import com.stocktracker.domainobjs.IDomainObject;
import com.stocktracker.domainobjs.json.TableRowMapDO;
import com.stocktracker.util.MathUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.time.Instant;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Stock Details Service
 *
 * @author Ratnala Diwakar Choudhury
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS,readOnly = true)
public class StockDetailsService extends AbstractService<StockDetailsDO> {

    @Resource
    private StockDetailsRepository stockDetailsRepository;

    @Resource
    private StockService stockService;

    @Resource
    private StaticCodeDecodeService staticCodeDecodeService;

    @Resource
    private PartyDetailsService partyDetailsService;

    @Resource
    private ContractService  contractService;


   /*
    @Resource
    private DeliveryDetailsService deliveryDetailsService;

    @Resource
    private TransportDetailsService transportDetailsService;

    @Resource
    private ContractService contractService;
    */


    public StockDetailsService() {
        super(StockDetailsDO.class);
    }

    @Override
    public Class<StockDetails> getEntityClass() {
        return StockDetails.class;
    }

    @Override
    public Page<? extends BaseEntity> findByFilter(Map<String, Object> parameters, PageRequest pageRequest) {
        String industryCodeFilter = (String)parameters.get("industryCode");
        String stockCodeFilter = (String)parameters.get("stockCode");
        String stockVarietyCodeFilter = (String)parameters.get("stockVarietyCode");
        String transTypeFilter = (String)parameters.get("transactionType");
        String rstNumberFilter = (String)parameters.get("rstNumber");
        String vehicleNumberFilter = (String)parameters.get("vehicleNumber");
        String partyNameFilter = (String)parameters.get("partyName");
        Date startDateFilter = (Date)parameters.get("transStartDate");
        Date endDateFilter = (Date)parameters.get("transEndDate");
        String onBehalfFilter = (String)parameters.get("onBehalf");
        String lampNameFilter = (String)parameters.get("lampName");
        String mandiNameFilter = (String)parameters.get("mandiName");
        String revisitFlagFilter = (String)parameters.get("revisitFlag");
        return getRepository().findAllStockDetailsByFilter(industryCodeFilter,stockCodeFilter,stockVarietyCodeFilter,transTypeFilter,rstNumberFilter,vehicleNumberFilter,startDateFilter,endDateFilter,partyNameFilter,onBehalfFilter,lampNameFilter,mandiNameFilter,revisitFlagFilter,pageRequest);
    }


    @Override
    public Page<? extends BaseEntity> findAll(Map<String, Object> parameters,PageRequest pageRequest){
        return findByFilter(new HashMap<String, Object>(),pageRequest);
    }

    @Transactional
    public DOPage<TableRowMapDO<String, String>> findSummaryByFilter(Map<String, Object> parameters) {
        DOPage<TableRowMapDO<String, String>> doPage = new DOPage<TableRowMapDO<String, String>>();
        int page = Integer.parseInt((String) parameters.get("page"));
        int size = Integer.parseInt((String) parameters.get("size"));
        Page<StockDetailsSummary> pagedData = findSummaryByFilter(parameters,PageRequest.of(page,size)); // getRepository().findAll(new PageRequest(page,size));
        copyPageToDOPageSettings(pagedData, doPage);
        List<StockDetailsSummaryDO> domainObjects = returnSummaryDomainObjects(pagedData.getContent());
        Instant start = Instant.now();
        domainObjects.add(calculateTotalsOnSummary(domainObjects));
        //domainObjects.add(calculateMandiSummary(domainObjects));
        //Instant finish = Instant.now();
        //long timeElapsed = Duration.between(start, finish).toMillis();  //in millis
        //System.out.println("Time taken with out threading " + timeElapsed);

        if (domainObjects != null) {
            for (StockDetailsSummaryDO domainObject : domainObjects) {
                doPage.getList().add(copySummaryDomainObjToTableRowMap(domainObject));
            }
        }
        return doPage;
    }


    /**
     * Calculate Mandi Summary
     *
     * Mandi  = (Mandi Received + Mandi Excess Purchase) - (Mandi Final Entry + Mandi Adjustment)
     *
     * @param summaryList
     * @return
     */
    public StockDetailsSummaryDO calculateMandiSummary(List<StockDetailsSummaryDO> summaryList){
        StockDetailsSummaryDO summaryDO = new StockDetailsSummaryDO();
        Map<Boolean,List<StockDetailsSummaryDO>> partitionByMandiRecords = summaryList.stream()
                .collect(Collectors.partitioningBy(stockDetailsSummaryDO -> ("MDRE".equals(stockDetailsSummaryDO.getTransactionType())
                        || "MDAJ".equals(stockDetailsSummaryDO.getTransactionType())
                        || "MDEN".equals(stockDetailsSummaryDO.getTransactionType())
                        || "MDEP".equals(stockDetailsSummaryDO.getTransactionType()))));

        Long summaryPackets = new Long(0);
        Double summaryWeightment = new Double(0.0);
        Double summaryLampEntry = new Double(0.0);
        Double summaryLampFinalEntry = new Double(0.0);
        Double summaryLampAmount = new Double(0.0);

        for (StockDetailsSummaryDO detailsSummaryDO: partitionByMandiRecords.get(true)) {
            switch (detailsSummaryDO.getTransactionType()){
                case "MDRE":
                    summaryPackets = summaryPackets + detailsSummaryDO.getNoOfPacketsSummary();
                    summaryWeightment = summaryWeightment + detailsSummaryDO.getWeightmentSummary();
                    break;
                case  "MDAJ":
                    summaryLampEntry = summaryLampEntry + detailsSummaryDO.getLampEntrySummary();
                    summaryLampAmount = summaryLampAmount + detailsSummaryDO.getActualAmountSummary();
                    summaryWeightment = summaryWeightment + detailsSummaryDO.getLampEntrySummary();
                    summaryLampFinalEntry= summaryLampFinalEntry + detailsSummaryDO.getLampEntrySummary();
                    break;
                case  "MDEN":
                    summaryLampEntry = summaryLampEntry + detailsSummaryDO.getLampEntrySummary();
                    summaryLampFinalEntry= summaryLampFinalEntry + detailsSummaryDO.getLampEntryAftAdjSummary();
                    break;
                case "MDEP":
                default:
                    break;
            }
        }

        if(partitionByMandiRecords.get(true) != null && partitionByMandiRecords.get(true).size() > 0) {
            summaryDO.setTransactionType("MLAMS");
            summaryDO.setNoOfPacketsSummary(summaryPackets);
            summaryDO.setWeightmentSummary(summaryWeightment);
            summaryDO.setLampEntrySummary(summaryLampEntry);
            summaryDO.setLampEntryAftAdjSummary(summaryLampFinalEntry);
            summaryDO.setActualAmountSummary(summaryLampAmount);
       }
        return summaryDO;
    }


    /**
     * Calculate totals on summary list
     *
     * @param summaryList
     * @return
     */
    public StockDetailsSummaryDO calculateTotalsOnSummary (List<StockDetailsSummaryDO> summaryList){
       StockDetailsSummaryDO summaryDO = new StockDetailsSummaryDO();
       summaryDO.setTransactionType("TOTL");
       // Number of packets and Qtl Summary Total
       List<StockDetailsSummaryDO> qtlplusSummary = new ArrayList<>();
       Map<Boolean,List<StockDetailsSummaryDO>> summaryPartition4rPktNWt = summaryList.stream()
                .collect(Collectors.partitioningBy(stockDetailsSummaryDO -> ("MDRE".equals(stockDetailsSummaryDO.getTransactionType())
                        || "PACK".equals(stockDetailsSummaryDO.getTransactionType())
                        || "PURC".equals(stockDetailsSummaryDO.getTransactionType())
                        || "RECV".equals(stockDetailsSummaryDO.getTransactionType())
                        || "MDAJ".equals(stockDetailsSummaryDO.getTransactionType())
                )));

        List<StockDetailsSummaryDO> pktWtPlusSummary = summaryPartition4rPktNWt.get(true);
        List<StockDetailsSummaryDO> pktWtMinusSummary = summaryPartition4rPktNWt.get(false);
        Long numberOfPlusPacketTotal = pktWtPlusSummary.stream()
                .filter(stockDetailsSummaryDO -> stockDetailsSummaryDO.getNoOfPacketsSummary() != null)
                .mapToLong(stockDetailsSummaryDO -> stockDetailsSummaryDO.getNoOfPacketsSummary())
                .sum();

        Long numberOfMinusPacketTotal = pktWtMinusSummary.stream()
                .filter(stockDetailsSummaryDO -> stockDetailsSummaryDO.getNoOfPacketsSummary() != null)
                .mapToLong(stockDetailsSummaryDO -> stockDetailsSummaryDO.getNoOfPacketsSummary())
                .sum();

       summaryDO.setNoOfPacketsSummary(numberOfPlusPacketTotal - numberOfMinusPacketTotal);


       Double numberOfPlusWmtTotal = pktWtPlusSummary.stream()
                .filter(stockDetailsSummaryDO -> stockDetailsSummaryDO.getWeightmentSummary() != null)
                .mapToDouble(stockDetailsSummaryDO -> stockDetailsSummaryDO.getWeightmentSummary())
                .sum();

       Double numberOfMinusWmtTotal = pktWtMinusSummary.stream()
                .filter(stockDetailsSummaryDO -> stockDetailsSummaryDO.getWeightmentSummary() != null)
                .mapToDouble(stockDetailsSummaryDO -> stockDetailsSummaryDO.getWeightmentSummary())
                .sum();

       summaryDO.setWeightmentSummary(numberOfPlusWmtTotal - numberOfMinusWmtTotal);

       // Mandi Entry and Mandi Adjustment
        Map<Boolean,List<StockDetailsSummaryDO>> mandiEntrySummaryPart = summaryList.stream()
                .collect(Collectors.partitioningBy(stockDetailsSummaryDO -> ("MDEN".equals(stockDetailsSummaryDO.getTransactionType())
                        /*|| "MDAJ".equals(stockDetailsSummaryDO.getTransactionType())*/)));

        List<StockDetailsSummaryDO> mandiEntrySummaryList = mandiEntrySummaryPart.get(true);

        Double mandiEntrySummaryTotal = mandiEntrySummaryList.stream()
                .filter(stockDetailsSummaryDO -> stockDetailsSummaryDO.getLampEntrySummary() != null)
                .mapToDouble(stockDetailsSummaryDO -> stockDetailsSummaryDO.getLampEntrySummary())
                .sum();

        summaryDO.setLampEntrySummary(mandiEntrySummaryTotal);

        Double mandiEntryAdjSummaryTotal = mandiEntrySummaryList.stream()
                .filter(stockDetailsSummaryDO -> stockDetailsSummaryDO.getLampEntryAftAdjSummary() != null)
                .mapToDouble(stockDetailsSummaryDO -> stockDetailsSummaryDO.getLampEntryAftAdjSummary())
                .sum();

        summaryDO.setLampEntryAftAdjSummary(mandiEntryAdjSummaryTotal);

        // Actual Amount Summary Total
       Map<Boolean,List<StockDetailsSummaryDO>> summaryPartition4rAmount = summaryList.stream()
               .collect(Collectors.partitioningBy(
                       stockDetailsSummaryDO -> (
                               "PYRE".equals(stockDetailsSummaryDO.getTransactionType())
                                       || "PYIS".equals(stockDetailsSummaryDO.getTransactionType())
                       || "PURC".equals(stockDetailsSummaryDO.getTransactionType())
                       || "PYDU".equals(stockDetailsSummaryDO.getTransactionType()))));

        List<StockDetailsSummaryDO> amtPlusSummary = summaryPartition4rAmount.get(false);
        List<StockDetailsSummaryDO> amtMinusSummary = summaryPartition4rAmount.get(true);
        Double actualAmountPlusTotal = amtPlusSummary.stream()
                .filter(stockDetailsSummaryDO -> stockDetailsSummaryDO.getActualAmountSummary() != null)
                .mapToDouble(stockDetailsSummaryDO -> stockDetailsSummaryDO.getActualAmountSummary())
                .sum();

        Double actualAmountMinusTotal = amtMinusSummary.stream()
                .filter(stockDetailsSummaryDO -> stockDetailsSummaryDO.getActualAmountSummary() != null)
                .mapToDouble(stockDetailsSummaryDO -> stockDetailsSummaryDO.getActualAmountSummary())
                .sum();
        summaryDO.setActualAmountSummary(actualAmountPlusTotal-actualAmountMinusTotal);
        return summaryDO;
    }

    @Transactional
    public DOPage<TableRowMapDO<String, String>> reteriveStockBalanceByFilter(Map<String, Object> parameters) {
        DOPage<TableRowMapDO<String, String>> doPage = new DOPage<TableRowMapDO<String, String>>();
        int page = Integer.parseInt((String) parameters.get("page"));
        int size = Integer.parseInt((String) parameters.get("size"));
        Page<Stock> pagedData = getStockBalanceByFilter(parameters,PageRequest.of(page,size)); // getRepository().findAll(new PageRequest(page,size));
        copyPageToDOPageSettings(pagedData, doPage);
        List<? extends IDomainObject> domainObjects = returnStockBalancesDO(pagedData.getContent());
        if (domainObjects != null) {
            for (IDomainObject domainObject : domainObjects) {
                doPage.getList().add(copyStockDOToTableRowMap((StockDO) domainObject));
            }
        }
        return doPage;
    }

    private Page<Stock> getStockBalanceByFilter(Map<String, Object> parameters, PageRequest pageRequest) {
        String industryCodeFilter = (String)parameters.get("industryCode");
        String stockCodeFilter = (String)parameters.get("stockCode");
        String stockVarietyCodeFilter = (String)parameters.get("stockVarietyCode");
        return stockService.getStockBalance(industryCodeFilter,stockCodeFilter,stockVarietyCodeFilter,pageRequest);
    }

    private Page<StockDetailsSummary> findSummaryByFilter(Map<String, Object> parameters, PageRequest pageRequest) {

        String industryCodeFilter = (String)parameters.get("industryCode");
        String stockCodeFilter = (String)parameters.get("stockCode");
        String stockVarietyCodeFilter = (String)parameters.get("stockVarietyCode");
        String transTypeFilter = (String)parameters.get("transactionType");
        String rstNumberFilter = (String)parameters.get("rstNumber");
        String vehicleNumberFilter = (String)parameters.get("vehicleNumber");
        String partyNameFilter = (String)parameters.get("partyName");
        Date startDateFilter = (Date)parameters.get("transStartDate");
        Date endDateFilter = (Date)parameters.get("transEndDate");
        String onBehalfFilter = (String)parameters.get("onBehalf");
        String lampNameFilter = (String)parameters.get("lampName");
        String mandiNameFilter = (String)parameters.get("mandiName");
        String revisitFlagFilter = (String)parameters.get("revisitFlag");


        return getRepository().findStockDetailsSummaryByFilter(industryCodeFilter,stockCodeFilter,stockVarietyCodeFilter,transTypeFilter,rstNumberFilter,vehicleNumberFilter,startDateFilter,endDateFilter,partyNameFilter,onBehalfFilter,lampNameFilter,mandiNameFilter,revisitFlagFilter,pageRequest);
    }

    private List<StockDetailsSummaryDO> returnSummaryDomainObjects(List<StockDetailsSummary> stockDetailsSummaries){
        final List<StockDetailsSummaryDO> summaryDOS = new ArrayList<StockDetailsSummaryDO>();
        stockDetailsSummaries.forEach(stockDetailsSummary -> {
            StockDetailsSummaryDO stockDetailsSummaryDO = new StockDetailsSummaryDO();
            stockDetailsSummaryDO.setIndustryCode(stockDetailsSummary.getIndustryCode());
            stockDetailsSummaryDO.setStockCode(stockDetailsSummary.getStockCode());
            stockDetailsSummaryDO.setStockVarietyCode(stockDetailsSummary.getStockVarietyCode());
            stockDetailsSummaryDO.setTransactionType(stockDetailsSummary.getTransactionType());
            stockDetailsSummaryDO.setNoOfPacketsSummary(stockDetailsSummary.getTotalNoOfPackets());
            stockDetailsSummaryDO.setWeightmentSummary(stockDetailsSummary.getTotalWeightment());
            stockDetailsSummaryDO.setLampEntrySummary(stockDetailsSummary.getTotalLampEntryInQtl());
            stockDetailsSummaryDO.setLampEntryAftAdjSummary(stockDetailsSummary.getTotalLampEntryAfterAdjInQtl());
            stockDetailsSummaryDO.setActualAmountSummary(stockDetailsSummary.getTotalAmount());
            summaryDOS.add(stockDetailsSummaryDO);
        });
        return summaryDOS;
    }

    private List<StockDO> returnStockBalancesDO(List<Stock> stockBalances){
        final List<StockDO> summaryDOS = new ArrayList<StockDO>();
        stockBalances.forEach(stock -> {
            StockDO st = new StockDO();
            st.setIndustryCode(stock.getIndustryCode());
            st.setStockCode(stock.getStockCode());
            st.setStockVarietyCode(stock.getStockVarietyCode());
            st.setTotalNoOfPackets(stock.getTotalNoOfPackets());
            st.setTotalWeightment(stock.getTotalWeightment());
            st.setAsOnDate(stock.getAsOnDate());
            summaryDOS.add(st);
        });
        return summaryDOS;
    }


    @Transactional
    public List<StockDetailsDO> saveOrUpdateStockDetails(List<StockDetailsDO> newStockDetails) {
        List<StockDetailsDO> result = new ArrayList<>();
        for (StockDetailsDO stockDetailsDO : newStockDetails){
            result.add(saveOrUpdateStockDetails(stockDetailsDO));
        }
        return result;
    }

   /**
     * Save or update the stock details
     *
     *
     * @param newStockDetails
     */
    @Transactional
    public StockDetailsDO saveOrUpdateStockDetails(StockDetailsDO newStockDetails) {

        Optional<ResultDO> resultDO = newStockDetails.validate();

        if (resultDO.isPresent() && !resultDO.get().isError()) {

            try {

                boolean isUpdate = false;
                StockDetailsDO oldStockDetails = find(newStockDetails); // find old stock details if any using ID
                if (oldStockDetails != null) {
                    isUpdate = true;
                } else {
                    oldStockDetails = new StockDetailsDO();
                }

                if (!newStockDetails.getTransactionType().equals(oldStockDetails.getTransactionType())) {
                    // if the transaction type are unequal then fetch the sign of the operation
                    Optional<List<CodeDataItemDO>> codeDataItemDOList = staticCodeDecodeService.getCodeDataItemList(CodeDecodeConstants.CD_PLUS_OR_MINUS_ON_STOCK_TOTAL, newStockDetails.getTransactionType(), CodeDecodeConstants.DEFAULT_LANGUAGE, true);
                    String sign = "+"; // default is +
                    if (codeDataItemDOList.isPresent() && !codeDataItemDOList.get().isEmpty()) {
                        CodeDataItemDO signDO = codeDataItemDOList.get().get(0);
                        sign = signDO.getCodeDescription();
                        newStockDetails.setPlusOrMinusOnTotal(sign.charAt(0));
                    }
                }

                if (newStockDetails.getCostPriceDetailsDO() != null) {
                    Optional<List<CodeDataItemDO>> plusOrMinusOnAmtTotal = staticCodeDecodeService.getCodeDataItemList(CodeDecodeConstants.CD_PLUS_OR_MINUS_ON_AMT_TOTAL, newStockDetails.getTransactionType(), CodeDecodeConstants.DEFAULT_LANGUAGE, true);
                    String sign = "+"; // default is +
                    if (plusOrMinusOnAmtTotal.isPresent() && !plusOrMinusOnAmtTotal.get().isEmpty()) {
                        CodeDataItemDO signDO = plusOrMinusOnAmtTotal.get().get(0);
                        sign = signDO.getCodeDescription();
                        newStockDetails.getCostPriceDetailsDO().setPlusOrMinusOnTotal(sign.charAt(0));
                    }
                }

                if(isUpdate){
                    ContractChangeIdentier newChangeIdentifier = new ContractChangeIdentier(newStockDetails);
                    if (newChangeIdentifier.compareTo(new ContractChangeIdentier(oldStockDetails)) == -1) {
                        newStockDetails.setContractDO(contractService.reviseContract(newStockDetails,oldStockDetails));
                    }
                }else{
                        newStockDetails.setContractDO(contractService.updateOpenContract(newStockDetails.getOnBeHalf(), newStockDetails.getTransactionType(), newStockDetails.getTotalWeightment()));
                }

                StockDetailsDO updatedStockDetailsDo = (StockDetailsDO) update(newStockDetails);

                StockDO updatedStockDO = null;

                if (isUpdate) {
                    StockChangeIndentier newChangeIdentifier = new StockChangeIndentier(newStockDetails);

                    if (newChangeIdentifier.compareTo(new StockChangeIndentier(oldStockDetails)) == -1) {
                        updatedStockDO = stockService.reviseStockTotals(newStockDetails, oldStockDetails);
                    }

                } else { // if New
                    updatedStockDO = stockService.updateStockTotals(newStockDetails);
                }

                if (updatedStockDO != null) {
                    updatedStockDetailsDo.setStockDO(updatedStockDO);
                }


                updatedStockDetailsDo.getResultDO().addResultIdentifier(Long.toString(updatedStockDetailsDo.getId()));
                return updatedStockDetailsDo;

            }catch (Exception anyException) {
                anyException.printStackTrace();
                ResultDO res = new ResultDO();
                res.setError(true);
                List<String> errorList = new ArrayList<String>();
                errorList.add("ERR0001 : Generic Exception, Contact Adiminstrator");
                newStockDetails.setResultDO(res);
                return newStockDetails;
            }
        }

        newStockDetails.setResultDO(resultDO.get());
        return newStockDetails;

    }

    /**
     * Delete stock details
     *
     *  1) Delete the stock details records
     *  2) reverse the stock totals
     *
     * @param stockDetails
     * @return
     */
    @Transactional
    public ResultDO deleteStockDetails(StockDetailsDO stockDetails){
        ResultDO resultDO = new ResultDO();

        StockDetailsDO deleteableStockDetails = find(stockDetails); // find old stock details if any using ID

        if(deleteableStockDetails != null) {
            try {
                contractService.reviseContract(null, deleteableStockDetails);
            }catch (NoOpenContractAvailableException noOpenContractAvailableException){
                resultDO.setError(true);
                resultDO.getErrorList().add(noOpenContractAvailableException.getMessage());
                return resultDO;
            }
            stockService.reverseStockTotals(deleteableStockDetails);
            remove(deleteableStockDetails);
            return resultDO;
        }else {
            resultDO.setError(true);
            return resultDO;
        }
    }

    @Transactional
    public ResultDO deleteMultipleStockDetails(List<StockDetailsDO> stockDetailsDOList){
         ResultDO resultDO = new ResultDO();

        try {
            stockDetailsDOList.forEach(stockDetailsDO -> {
                deleteStockDetails(stockDetailsDO);
            });

            return resultDO;

        }catch (Exception anyException){
            resultDO.setError(true);
            return resultDO;
        }
    }

    @Override
    public StockDetailsDO returnDomainObject(BaseEntity entity) {

        if (entity == null) {
            return null;
        }

        StockDetailsDO domainObject = new StockDetailsDO((StockDetails)entity);
        return domainObject;
    }

    @Override
    public List<StockDetailsDO> returnDomainObjects(List<? extends BaseEntity> entities) {
        if (entities == null) {
            return null;
        }
        List<StockDetailsDO> domainObjects = new ArrayList<StockDetailsDO>();

        if (entities != null) {
            entities.forEach(entity -> {
                StockDetailsDO domainObject = new StockDetailsDO((StockDetails) entity);
                domainObjects.add(domainObject);
            });
        }
        return domainObjects;
    }

    @Override
    public BaseEntity returnEntity(StockDetailsDO domainObject) {
        StockDetailsDO stockDetailsDO = (StockDetailsDO) domainObject;
        StockDetails stockDetails = new StockDetails();
        stockDetailsDO.copyDomainObject2Entity(stockDetails);
        return stockDetails;
    }

   @Override
    public List<? extends BaseEntity> returnEntities(List<StockDetailsDO> domainObjects) {
        List<StockDetails> entities = new ArrayList();
        for (IDomainObject domainObject : domainObjects) {
            StockDetailsDO stockDetailsDO = (StockDetailsDO) domainObject;
            StockDetails stockDetails = new StockDetails();
            stockDetailsDO.copyDomainObject2Entity(stockDetails);
            entities.add(stockDetails);
        }
        return entities;
    }

    @Override
    public TableRowMapDO<String, String> copyDomainObjToTableRowMap(IDomainObject domainObject) {
        StockDetailsDO stockDetailsDO = (StockDetailsDO) domainObject;
        TableRowMapDO<String, String> tableRowMapDO = new TableRowMapDO<String, String>();
        tableRowMapDO.add("id", stockDetailsDO.getId().toString());
        tableRowMapDO.add("industryCode",stockDetailsDO.getIndustryCode());
        tableRowMapDO.add("stockCode",stockDetailsDO.getStockCode());
        tableRowMapDO.add("stockVarietyCode",stockDetailsDO.getStockVarietyCode());
        tableRowMapDO.add("transactionType",stockDetailsDO.getTransactionType());
        tableRowMapDO.add("numberOfPackets",String.valueOf(stockDetailsDO.getNoOfPackets()));
        tableRowMapDO.add("weightment",String.valueOf(stockDetailsDO.getTotalWeightment()));

        tableRowMapDO.add("lampNameCode",stockDetailsDO.getLampNameCode());
        tableRowMapDO.add("mandiNameCode",stockDetailsDO.getMandiNameCode());

        if(stockDetailsDO.getLampEntryInQtl() != null) {
            tableRowMapDO.add("lampEntry", String.valueOf(stockDetailsDO.getLampEntryInQtl()));
        }

        if(stockDetailsDO.getLampEntryAfterAdjInQtl() != null) {
            tableRowMapDO.add("lampEntryAfterAdjInQtl", String.valueOf(stockDetailsDO.getLampEntryAfterAdjInQtl()));
        }

        if(stockDetailsDO.getTransportDetailsDO() != null) {
            tableRowMapDO.add("rstNumber", stockDetailsDO.getTransportDetailsDO().getRstNumber());
            tableRowMapDO.add("vehicleNumber", stockDetailsDO.getTransportDetailsDO().getVehicleNumber());
        }
        tableRowMapDO.add("tranactionDate",String.valueOf(DateTimeUtil.convertDateToUTCString(stockDetailsDO.getTransactionDate())));

        if(stockDetailsDO.getPartyDetailsDO() != null){
            tableRowMapDO.add("partyName", stockDetailsDO.getPartyDetailsDO().getPartyName1());
        }

        tableRowMapDO.add("remarks",stockDetailsDO.getRemarks());
        tableRowMapDO.add("onBehalf",stockDetailsDO.getOnBeHalf());
        tableRowMapDO.add("revisitFlag",stockDetailsDO.getRevisitFlag());

        if(stockDetailsDO.getCostPriceDetailsDO() != null && stockDetailsDO.getCostPriceDetailsDO().getAppliedRate() != null) {
            tableRowMapDO.add("appliedRate", String.valueOf(stockDetailsDO.getCostPriceDetailsDO().getAppliedRate()));
        }

        if(stockDetailsDO.getCostPriceDetailsDO() != null && stockDetailsDO.getCostPriceDetailsDO().getActualAmount() != null) {
            tableRowMapDO.add("actualAmount", String.valueOf(stockDetailsDO.getCostPriceDetailsDO().getActualAmount()));
        }

        if(stockDetailsDO.getDeliveryDetailsDO() != null && stockDetailsDO.getDeliveryDetailsDO().getDeliveryLatNo() != null) {
            tableRowMapDO.add("deliveryLatNo", String.valueOf(stockDetailsDO.getDeliveryDetailsDO().getDeliveryLatNo()));
        }

        tableRowMapDO.setClazzName(StockDetailsDO.CLASS_NAME);
        return tableRowMapDO;
    }

    private TableRowMapDO<String, String> copySummaryDomainObjToTableRowMap(StockDetailsSummaryDO stockDetailsSummaryDO) {

        TableRowMapDO<String, String> tableRowMapDO = new TableRowMapDO<String, String>();
        tableRowMapDO.add("industryCode",stockDetailsSummaryDO.getIndustryCode());
        tableRowMapDO.add("stockCode",stockDetailsSummaryDO.getStockCode());
        tableRowMapDO.add("stockVarietyCode",stockDetailsSummaryDO.getStockVarietyCode());
        tableRowMapDO.add("transactionType",stockDetailsSummaryDO.getTransactionType());
        if(stockDetailsSummaryDO.getNoOfPacketsSummary() != null) {
            tableRowMapDO.add("noOfPacketsSummary",String.valueOf(stockDetailsSummaryDO.getNoOfPacketsSummary()));
        }
        if(stockDetailsSummaryDO.getWeightmentSummary() != null) {
            tableRowMapDO.add("weightmentSummary", String.valueOf(stockDetailsSummaryDO.getWeightmentSummary()));
        }
        if(stockDetailsSummaryDO.getLampEntrySummary() != null) {
            tableRowMapDO.add("lampEntrySummary", String.valueOf(stockDetailsSummaryDO.getLampEntrySummary()));
        }

        if(stockDetailsSummaryDO.getLampEntryAftAdjSummary() != null) {
            tableRowMapDO.add("finalEntrySummary", String.valueOf(stockDetailsSummaryDO.getLampEntryAftAdjSummary()));
        }

        if(stockDetailsSummaryDO.getActualAmountSummary() != null) {
            tableRowMapDO.add("actualAmountSummary", String.valueOf(stockDetailsSummaryDO.getActualAmountSummary()));
        }
        tableRowMapDO.setClazzName(StockDetailsSummaryDO.CLASS_NAME);
        return tableRowMapDO;
    }

    private TableRowMapDO<String, String> copyStockDOToTableRowMap(StockDO stockDO) {
        TableRowMapDO<String, String> tableRowMapDO = new TableRowMapDO<String, String>();
        tableRowMapDO.add("industryCode",stockDO.getIndustryCode());
        tableRowMapDO.add("stockCode",stockDO.getStockCode());
        tableRowMapDO.add("stockVarietyCode",stockDO.getStockVarietyCode());
        if(stockDO.getTotalNoOfPackets() != null) {
            tableRowMapDO.add("noOfPacketsSummary",String.valueOf(stockDO.getTotalNoOfPackets()));
        }
        if(stockDO.getTotalWeightment() != null) {
            tableRowMapDO.add("weightmentSummary", String.valueOf(stockDO.getTotalWeightment()));
        }
        if(stockDO.getAsOnDate() != null) {
            tableRowMapDO.add("asOnDate",String.valueOf(DateTimeUtil.convertDateToUTCString(stockDO.getAsOnDate())));
        }
        tableRowMapDO.setClazzName(StockDO.CLASS_NAME);
        return tableRowMapDO;
    }

    public Optional<List<PartyDetailsDO>> getPartyNameSuggestions(String partyName){
        return Optional.of(partyDetailsService.getPartyNameSuggestions(partyName));
    }

    public List<MandiReconcilationDataDO> getMandiReconciliationData(String industryCode,String lampNameCode,String mandiNameCode) {
        List<MandiReconcilationData> mandiReconciliationDataList = getRepository().getMandiReconciliationReportData(industryCode, lampNameCode, mandiNameCode);
        return mandiReconciliationDataList.stream().map(mandiReconciliationData -> {
            MandiReconcilationDataDO reconciliationDataDO = new MandiReconcilationDataDO();
            reconciliationDataDO.copyEntity2DomainObject(mandiReconciliationData);
            return reconciliationDataDO;
        }).collect(Collectors.toList());
    }

    public Map<String,IndustryOutturnReportDO>  getIndustryOutturnReportData(){

        List<StockDetailsSummary> summaryList = getRepository().getStockDetailSummary();
        Map<String, Map<String, IndustryOutturnReportDO>> map =
                summaryList.stream()
                        .collect(Collectors.groupingBy(StockDetailsSummary::getIndustryCode
                                ,Collectors.groupingBy(StockDetailsSummary::getStockCode
                                        ,Collectors.collectingAndThen(Collectors.toList(),list -> {
                                            IndustryOutturnReportDO industryOutturnReportDO = new IndustryOutturnReportDO();
                                            if(list != null) {
                                                industryOutturnReportDO.setIndustryName(list.get(0).getIndustryCode());
                                                industryOutturnReportDO.setStockCode(list.get(0).getStockCode());
                                            }
                                            list.forEach(stockDetailsSummary -> {
                                                   switch (stockDetailsSummary.getStockCode()){
                                                       case "PADD": // Paddy
                                                           switch (stockDetailsSummary.getTransactionType()){
                                                               case "MDRE": // Mandi Recv
                                                                   industryOutturnReportDO.setTotalMandiRecvPaddyInWmt(MathUtil.add(industryOutturnReportDO.getTotalMandiRecvPaddyInWmt(),stockDetailsSummary.getTotalWeightment()));
                                                                   industryOutturnReportDO.setTotalMandiRecvPaddyInPkt(MathUtil.add(industryOutturnReportDO.getTotalMandiRecvPaddyInPkt(),stockDetailsSummary.getTotalNoOfPackets()));
                                                                   break;
                                                               case "OFFER":
                                                                   industryOutturnReportDO.setTotalPaddyOfferInPkt(MathUtil.add(industryOutturnReportDO.getTotalPaddyOfferInPkt(),stockDetailsSummary.getTotalNoOfPackets()));
                                                                   industryOutturnReportDO.setTotalPaddyOfferInWmt(MathUtil.add(industryOutturnReportDO.getTotalPaddyOfferInWmt(),stockDetailsSummary.getTotalWeightment()));
                                                                   break;
                                                               case "PURC":
                                                                   industryOutturnReportDO.setTotalPrivateRecvPaddyInWmt(MathUtil.add(industryOutturnReportDO.getTotalPrivateRecvPaddyInWmt(),stockDetailsSummary.getTotalWeightment()));
                                                                   industryOutturnReportDO.setTotalPrivateRecvPaddyInPkt(MathUtil.add(industryOutturnReportDO.getTotalPrivateRecvPaddyInPkt(),stockDetailsSummary.getTotalNoOfPackets()));
                                                                   break;
                                                               case "MDAJ":
                                                                   industryOutturnReportDO.setTotalMandiAdjPaddyInWmt(stockDetailsSummary.getTotalWeightment());
                                                                   // No Amount in Query
                                                                   break;
                                                           }
                                                           break;
                                                       case "RICE":
                                                            switch (stockDetailsSummary.getTransactionType()){
                                                               case "PACK":
                                                                   industryOutturnReportDO.setTotalRiceProducedInPkt(MathUtil.add(industryOutturnReportDO.getTotalRiceProducedInPkt() , stockDetailsSummary.getTotalNoOfPackets()));
                                                                   industryOutturnReportDO.setTotalRiceProducedInWmt(MathUtil.add(industryOutturnReportDO.getTotalRiceProducedInWmt() ,stockDetailsSummary.getTotalWeightment()));
                                                                   break;
                                                               case "KMDEL":
                                                                   // TODO:
                                                                   break;
                                                               default:
                                                                   break;
                                                           }
                                                           break;
                                                       case "BRRI":
                                                           switch (stockDetailsSummary.getTransactionType()){
                                                               case "PACK":
                                                                   industryOutturnReportDO.setTotalBrokenRiceProducedInPkt(MathUtil.add(industryOutturnReportDO.getTotalBrokenRiceProducedInPkt(),stockDetailsSummary.getTotalNoOfPackets()));
                                                                   industryOutturnReportDO.setTotalBrokenRiceProducedInWmt(MathUtil.add(industryOutturnReportDO.getTotalBrokenRiceProducedInWmt(),stockDetailsSummary.getTotalWeightment()));
                                                                   break;
                                                               default:
                                                                   break;
                                                           }
                                                           break;
                                                       case "BRAN":
                                                           switch (stockDetailsSummary.getTransactionType()){
                                                               case "SALE":
                                                                   industryOutturnReportDO.setTotalBranProducedInWmt(stockDetailsSummary.getTotalWeightment());
                                                                   break;
                                                               case "PACK":
                                                                   industryOutturnReportDO.setTotalBranProducedInPkt(stockDetailsSummary.getTotalNoOfPackets());
                                                                   break;
                                                               default:
                                                                   break;
                                                           }
                                                           break;
                                                       case "BRBR":
                                                           switch (stockDetailsSummary.getTransactionType()){
                                                               case "PACK":
                                                                   industryOutturnReportDO.setTotalBranBrokenProducedInPkt(stockDetailsSummary.getTotalNoOfPackets());
                                                                   industryOutturnReportDO.setTotalBranBrokenProducedInWmt(stockDetailsSummary.getTotalWeightment());
                                                                   break;
                                                               default:
                                                                   break;
                                                           }
                                                           break;
                                                       case "HUSK":
                                                           switch (stockDetailsSummary.getTransactionType()){
                                                               case "SALE":
                                                                   industryOutturnReportDO.setTotalHuskProducedInWmt(stockDetailsSummary.getTotalWeightment());
                                                                   break;
                                                               default:
                                                                   break;
                                                           }
                                                           break;
                                                       default:
                                                           break;
                                                   }

                                            });

                                            switch (industryOutturnReportDO.getStockCode()){
                                                case "PADD":
                                                    industryOutturnReportDO.setTotalRunningStockInPkt(MathUtil.substract(MathUtil.add(industryOutturnReportDO.getTotalMandiRecvPaddyInPkt(),
                                                            industryOutturnReportDO.getTotalPrivateRecvPaddyInPkt()), industryOutturnReportDO.getTotalPaddyOfferInPkt()));
                                                    industryOutturnReportDO.setTotalRunningStockInWmt(MathUtil.substract(MathUtil.add(industryOutturnReportDO.getTotalMandiRecvPaddyInWmt(),
                                                            industryOutturnReportDO.getTotalPrivateRecvPaddyInWmt()), industryOutturnReportDO.getTotalPaddyOfferInWmt()));
                                                    break;
                                                default:
                                                    break;
                                            }
                                            return industryOutturnReportDO;
                                        }))));
        // consolidate all the totals
        Map<String,IndustryOutturnReportDO> industryOutturnDo = new HashMap<String,IndustryOutturnReportDO>();
        map.entrySet().forEach( industryCd -> {
            IndustryOutturnReportDO reportDO = new IndustryOutturnReportDO();
            industryCd.getValue().entrySet().forEach(stockEntry -> {
                reportDO.copyDo(stockEntry.getValue());
                industryOutturnDo.put(industryCd.getKey(),reportDO);
            });
        });

        return industryOutturnDo;
    }


    @Override
    public StockDetailsRepository getRepository() {
        return this.stockDetailsRepository;
    }

    @Transactional
    public ResultDO toggleRevisitFlag(StockDetailsDO stockDetailsDO) {
        StockDetailsDO detailsDO = this.find(stockDetailsDO.getId());
        if (detailsDO.getRevisitFlag() != null){

            if(detailsDO.getRevisitFlag().equals("Y")){
                detailsDO.setRevisitFlag("N");
            }else{
                detailsDO.setRevisitFlag("Y");
            }
        }else{
            detailsDO.setRevisitFlag("Y");
        }
        return saveOrUpdateStockDetails(detailsDO).getResultDO();
   }
}
