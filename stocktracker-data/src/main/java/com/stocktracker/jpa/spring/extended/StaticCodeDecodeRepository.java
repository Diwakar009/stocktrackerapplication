/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.jpa.spring.extended;

import com.stocktracker.jpa.spring.extended.custom.ExtendedRepository;
import com.stocktracker.model.StaticCodeDecode;
import com.stocktracker.model.StaticCodeDecodePK;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StaticCodeDecodeRepository extends ExtendedRepository<StaticCodeDecode, StaticCodeDecodePK> {

	/**
	 * Finds all unique codename and language.
	 *
	 * @param pagable
	 * @return
	 */

	@Query("SELECT new StaticCodeDecode(s.id.codeName,s.id.language) FROM StaticCodeDecode s " +
			" GROUP BY s.id.codeName,s.id.language ")
	public Page<StaticCodeDecode> findUniqueAll(Pageable pagable);

	/**
	 * Finds all unique codename and language with internal filter on code description or code name or language.
	 *
	 * @param filter
	 * @param pageable
	 * @return
	 */

	@Query("SELECT new StaticCodeDecode(s.id.codeName,s.id.language) FROM StaticCodeDecode s " +
			" WHERE s.id.codeName like :filter OR s.id.language like :filter OR s.codeDesc LIKE :filter " +
			" GROUP BY s.id.codeName,s.id.language")
	public Page<StaticCodeDecode> findUniqueAllByFilter(@Param("filter") String filter, Pageable pageable);

	/**
	 * Find all static code decode applying fiter on codeName or codevalue or codeDesc
	 *
	 * @param pageable
	 * @return
	 */
	@Query("SELECT s FROM StaticCodeDecode s " +
			" WHERE s.id.codeName LIKE :filter OR s.id.codeValue LIKE :filter OR s.codeDesc like :filter " +
			" ORDER BY s.codeDesc")
	public Page<StaticCodeDecode> findByCodeNameORDesc(@Param("filter") String filter, Pageable pageable);


	/**
	 *
	 * Find all static code decode using codename and language.
	 *
	 * @param codeName
	 * @param language
	 * @return
	 */
	@Query("SELECT s FROM StaticCodeDecode s " +
			" WHERE s.id.codeName = :codeName AND s.id.language=:language " +
			" ORDER BY s.codeDesc")
	public List<StaticCodeDecode> findByCodeNameNLang(@Param("codeName") String codeName, @Param("language") String language);

	/**
	 * Find all static code decodes using exact search on codename and language and partial search on code value and code desc.
	 *
	 * @param codeName
	 * @param language
	 * @param filter
	 * @param pageable
	 * @return
	 */
	@Query("SELECT s FROM StaticCodeDecode s " +
			" WHERE (s.id.codeName = :codeName AND s.id.language=:language) AND (s.id.codeValue like :filter OR s.codeDesc like :filter)" +
			" ORDER BY s.codeDesc")
	public Page<StaticCodeDecode> listByCodeNameNLangNFilter (@Param("codeName") String codeName, @Param("language") String language,@Param("filter") String filter, Pageable pageable);


	/**
	 * Find all static code decodes using exact search on codeName,language and codevaluefilter
	 *
	 * @param codeName
	 * @param codeValueFilter
	 * @param language
	 * @return
	 */

	@Query("SELECT s FROM StaticCodeDecode s " +
			" WHERE s.id.codeName = :codeName AND s.id.language=:language AND s.id.codeValueFilter=:codeValueFilter " +
			" ORDER BY s.codeDesc")
	public List<StaticCodeDecode> findByCodeNameAndCodeValueFilter(@Param("codeName") String codeName, @Param("codeValueFilter") String codeValueFilter, @Param("language") String language);


	@Modifying
	@Query("DELETE FROM StaticCodeDecode s " +
			"WHERE s.id.codeName=:codeName AND s.id.language=:language")
	public void deleteByCodeNameAndLanguage(@Param("codeName") String codeName,@Param("language")String language);


	@Query("SELECT s FROM StaticCodeDecode s " +
			" WHERE s.id.codeName = :codeName AND s.id.codeValue=:codeValue AND s.id.language=:language")
    List<StaticCodeDecode> getCodeValueDescription(@Param("codeName") String codeName, @Param("codeValue")String codeValue, @Param("language")String language);
}
