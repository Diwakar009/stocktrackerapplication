/*
 * Copyright (C) 2017 Cem Ikta
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 *
 */

package com.desktopapp.fx.controller;


import com.desktopapp.fx.DesktopFxApplication;
import com.desktopapp.fx.common.PageDetails;
import com.desktopapp.fx.control.MessageBox;
import com.desktopapp.fx.mvc.AbstractDataPageController;
import com.desktopapp.fx.mvc.DataPageView;
import com.desktopapp.fx.util.I18n;
import com.desktopapp.fx.view.StaticCodeDecodeForm;
import com.desktopapp.fx.view.StaticCodeDecodePage;

import com.desktopapp.service.AbstractService;
import com.desktopapp.service.StaticCodeDecodeService;
import javafx.geometry.Pos;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import org.controlsfx.control.Notifications;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.stocktracker.domainobjs.json.CodeDataItemDO;
import com.stocktracker.domainobjs.json.DOPage;
import com.stocktracker.domainobjs.json.StaticCodeDecodeDO;
import com.stocktracker.domainobjs.json.TableRowMapDO;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static com.desktopapp.service.QueryParameter.with;

/**
 * Static code decode controller
 *
 * @author diwakar009
 */
@Component
public class StaticCodeDecodeController extends AbstractDataPageController<StaticCodeDecodeDO> {

    private final Logger logger = LoggerFactory.getLogger(StaticCodeDecodeController.class);

    @Autowired
    private StaticCodeDecodeService staticCodeDecodeService;

    /**
     * Get code data Items using codename and lanugaga
     *
     * @param codeName
     * @param language
     * @return Optional
     */
    public Optional<List<CodeDataItemDO>> getStaticCodeDataItemList(String codeName, String language){
        Optional<List<CodeDataItemDO>> codeDataItemList = ((StaticCodeDecodeService) getService()).getStaticCodeDataItemList(codeName, language);
        if(codeDataItemList.isPresent()) {
            return codeDataItemList;
        }
        return Optional.empty();
    }

    /**
     * Get code data Items using codename, filterCodeValue and language
     *
     * @param codeName
     * @param filterCodeValue
     * @param language
     * @return
     */
    public Optional<List<CodeDataItemDO>> getStaticCodeDataItemList(String codeName,String filterCodeValue, String language){
        Optional<List<CodeDataItemDO>> codeDataItemList = ((StaticCodeDecodeService)getService()).getStaticCodeDataItemList(codeName, filterCodeValue, language);
        if(codeDataItemList.isPresent()) {
            return codeDataItemList;
        }
        return Optional.empty();
    }

    /**
     * List by code name and language
     *
     * @param codeName
     * @param filter
     * @param language
     * @param pageDetails
     * @return
     */
    public DOPage<TableRowMapDO<String,String>> listByCodeNameNLang(String codeName, String filter, String language, PageDetails pageDetails) {
        return ((StaticCodeDecodeService)getService()).listByCodeNameNLang(with("page", String.valueOf(pageDetails.getPageNumber())).
                and("size", String.valueOf(pageDetails.getSize())).
                and("start",pageDetails.getStart()).
                and("end",pageDetails.getEnd()).
                and("codeName",codeName).
                and("language",language).
                and("search", filter == null ? "%":"%" + filter + "%").parameters());
    }

    /**
     * On Delete of static code decode
     *
     * @param codeName
     * @param language
     */
    public void onDeleteByCodeNameAndLanguage(String codeName, String language){
        logger.debug("form view on delete action");

        Optional<ButtonType> result = MessageBox.create()
                .owner(DesktopFxApplication.get().getMainStage())
                .contentText(I18n.COMMON.getString("confirm.delete"))
                .showDeleteConfirmation();

        if (result.isPresent() && result.get().getButtonData() == ButtonBar.ButtonData.OK_DONE) {
            try {

                ((StaticCodeDecodeService)getService()).deleteByCodeNameAndLanguage(codeName,language);

                onRefresh();

                if (DesktopFxApplication.get().getLoginSession().getPreferences().isShowInfoPopups()) {
                    Notifications.create()
                            .text(I18n.COMMON.getString("notification.deleted"))
                            .position(Pos.TOP_RIGHT).showInformation();
                }
            } catch (Exception e) {
                MessageBox.create()
                        .owner(DesktopFxApplication.get().getMainStage())
                        .contentText(I18n.COMMON.getString("exception.delete"))
                        .showError(e);
            }
        }

    }

    /**
     * On delete
     *
     * @param staticCodeDecodeDO
     */
    public void onDelete(StaticCodeDecodeDO staticCodeDecodeDO) {

        logger.debug("form view on delete action");

        Optional<ButtonType> result = MessageBox.create()
                .owner(DesktopFxApplication.get().getMainStage())
                .contentText(I18n.COMMON.getString("confirm.delete"))
                .showDeleteConfirmation();

        if (result.isPresent() && result.get().getButtonData() == ButtonBar.ButtonData.OK_DONE) {
            try {
                getService().remove(staticCodeDecodeDO);
                if (DesktopFxApplication.get().getLoginSession().getPreferences().isShowInfoPopups()) {
                    Notifications.create()
                            .text(I18n.COMMON.getString("notification.deleted"))
                            .position(Pos.TOP_RIGHT).showInformation();
                }
            } catch (Exception e) {
                MessageBox.create()
                        .owner(DesktopFxApplication.get().getMainStage())
                        .contentText(I18n.COMMON.getString("exception.delete"))
                        .showError(e);
            }
        }

    }

    @Override
    protected AbstractService<StaticCodeDecodeDO> createService() {
        return staticCodeDecodeService;
    }

    @Override
    protected DataPageView<StaticCodeDecodeDO> createDataPageView() {
        return new StaticCodeDecodePage();
    }

    @Override
    public void openFormView(StaticCodeDecodeDO staticCodeDecodeDO) {

        Map<String,Object> intent = new HashMap<String,Object>();

        if(!staticCodeDecodeDO.isNew()){
            intent.put("is_edit",true);
        }


        new StaticCodeDecodeForm(this, staticCodeDecodeDO,this.getMultScreenPane(),intent);
    }

    @Override
    public void onAddNew() {
      this.openFormView( new StaticCodeDecodeDO());
    }

    @Override
    public String getName() {
        return "StaticCodeDecodeController";
    }
}