package com.stocktracker.model;

import com.stocktracker.framework.IEntity;

import java.io.Serializable;

import javax.persistence.*;


/**
 * The persistent class for the app_user_login database table.
 * 
 */
@Entity
@Table(name="app_user_login")
@NamedQueries({
    @NamedQuery(name = AppUserLogin.FIND_ALL,
            query="SELECT a FROM AppUserLogin a"),
            @NamedQuery(name = AppUserLogin.FIND_BY_APPUSER_ID,
            query="SELECT d FROM AppUserLogin d WHERE d.appUser.id = :appUserId"),
    @NamedQuery(name = AppUserLogin.FIND_BY_APPUSERLOGIN_LIKE_ALL,
            query="SELECT d FROM AppUserLogin d WHERE d.appUser.id = :appUserId and d.userName LIKE :userName ORDER BY d.userName"),
    @NamedQuery(name = AppUserLogin.FIND_BY_APPUSERLOGIN,
            query="SELECT d FROM AppUserLogin d WHERE d.userName=:userName")
})
public class AppUserLogin extends BaseEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	public static String PREFIX_NAMEDQUERY = "AppUserLogin.";

	public static final String FIND_ALL = "AppUserLogin.findAll";
	public static final String FIND_BY_APPUSER_ID = "AppUserLogin.findByAppUserId";
	public static final String FIND_BY_APPUSERLOGIN_LIKE_ALL = "AppUserLogin.findByAppUserLoginLikeAll";
	public static final String FIND_BY_APPUSERLOGIN = "AppUserLogin.findByAppUserLogin";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "appuserlogin_id", nullable = false, columnDefinition = "BIGINT UNSIGNED")
	private Long id;

	@Column(name="fn_access_level", length = 3)
	private String fnAccessLevel;

	@Column(name="login_attempts")
	private int loginAttempts;

	@Column(name="user_name", length = 100)
	private String userName;

	@Column(name="password", length = 1000)
	private String password;
	
	@Column(name="sec_question", length = 50)
	private String secQuestion;

	@Column(name="sec_question_ans", length = 50)
	private String secQuestionAns;

	//bi-directional many-to-one association to AppUser
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="appuser_id")
	private AppUser appUser;

	public AppUserLogin() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFnAccessLevel() {
		return this.fnAccessLevel;
	}

	public void setFnAccessLevel(String fnAccessLevel) {
		this.fnAccessLevel = fnAccessLevel;
	}

	public int getLoginAttempts() {
		return this.loginAttempts;
	}

	public void setLoginAttempts(int loginAttempts) {
		this.loginAttempts = loginAttempts;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getSecQuestion() {
		return this.secQuestion;
	}

	public void setSecQuestion(String secQuestion) {
		this.secQuestion = secQuestion;
	}

	public String getSecQuestionAns() {
		return this.secQuestionAns;
	}

	public void setSecQuestionAns(String secQuestionAns) {
		this.secQuestionAns = secQuestionAns;
	}
	

	public AppUser getAppUser() {
		return this.appUser;
	}

	public void setAppUser(AppUser appUser) {
		this.appUser = appUser;
	}

	@Override
	public Serializable getEntityPK() {
		// TODO Auto-generated method stub
		return getId();
	}

	@Override
	public void copyEntity(IEntity source) {
		AppUserLogin appUserLoginSrc = (AppUserLogin)source;

		this.setCreatedBy(appUserLoginSrc.getCreatedBy());
		this.setUpdatedBy(appUserLoginSrc.getUpdatedBy());
		this.setCreatedAt(appUserLoginSrc.getCreatedAt());
		this.setUpdatedAt(appUserLoginSrc.getUpdatedAt());
		this.setUserName(appUserLoginSrc.getUserName());
		this.setFnAccessLevel(appUserLoginSrc.getFnAccessLevel());
		this.setPassword(appUserLoginSrc.getPassword());
		this.setSecQuestion(appUserLoginSrc.getSecQuestion());
		this.setSecQuestionAns(appUserLoginSrc.getSecQuestionAns());
		this.setLoginAttempts(appUserLoginSrc.getLoginAttempts());

		AppUser appUser = new AppUser();
		appUser.copyEntity(appUserLoginSrc.getAppUser());
		this.setAppUser(appUser);

		this.setId(appUserLoginSrc.getId());

	}

}