package com.desktopapp.service.common.impl;

import com.desktopapp.service.common.IEmailGatewayService;
import com.stocktracker.component.common.Attachment;
import org.springframework.stereotype.Service;

import javax.mail.*;
import javax.mail.Message.RecipientType;
import javax.mail.internet.*;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.*;

@Service
public class EmailGatewayServiceImpl implements IEmailGatewayService {

    private Session emailSession = null;
    private Properties props = null;

    public static final String PROPERTY_FILE = "emailsettings.properties";


    public EmailGatewayServiceImpl() {

        if (emailSession == null) {

            props = new Properties();
            InputStream inputStream = EmailGatewayServiceImpl.class.getClassLoader().getResourceAsStream(PROPERTY_FILE);
            try {
                if (inputStream != null) {
                    props.load(inputStream);
                    final String username = props.getProperty("username");
                    final String password = props.getProperty("password");

                    emailSession = Session.getDefaultInstance(props,
                            new javax.mail.Authenticator() {
                                protected PasswordAuthentication getPasswordAuthentication() {
                                    return new PasswordAuthentication(username, password);
                                }
                            });

                } else {
                    throw new FileNotFoundException("property file '" + PROPERTY_FILE + "' not found in the classpath");
                }

            } catch (Exception anyException) {
                anyException.printStackTrace();
            }

        }
    }

    public EmailGatewayServiceImpl(Session emailSession) {
        super();
        this.emailSession = emailSession;
    }

    @Override
    public void send(List<String> toList, List<String> ccList,
                     List<String> bccList, String subject, String body,
                     Collection<Attachment> attachments) {
        for (String to : toList) {
            send(to, ccList, bccList, subject, body, attachments);
        }
    }

    @Override
    public void send(String to, List<String> ccList,
                     List<String> bccList, String subject, String body,
                     Collection<Attachment> attachments) {
        MimeMessage mail = null;
        try {
            String from = "";

            from = (String) emailSession.getProperties().get("mail.from");

            InternetAddress sender = from != null ? new InternetAddress(from) : null;

            List<InternetAddress> allReceipents = new ArrayList<InternetAddress>();

            InternetAddress toRecipient = new InternetAddress(to);
            allReceipents.add(toRecipient);

            List<InternetAddress> ccAddressList = new ArrayList<InternetAddress>();
            List<InternetAddress> bccAddressList = new ArrayList<InternetAddress>();

            // Add to the cc as username for copy.
            ccAddressList.add(new InternetAddress(from));

            if (ccList != null && !ccList.isEmpty()) {
                for (String cc : ccList) {
                    ccAddressList.add(new InternetAddress(cc));
                }
                // Add the main email id to the
            }

            allReceipents.addAll(ccAddressList);

            if (bccList != null && !bccList.isEmpty()) {
                for (String bcc : ccList) {
                    bccAddressList.add(new InternetAddress(bcc));
                }

                allReceipents.addAll(bccAddressList);
            }

            // Yahoo Session


            Iterator<Attachment> iter = attachments.iterator();
            Collection<Attachment> embeddedObjects = new ArrayList<>();
            Collection<Attachment> properAttachments = new ArrayList<>();

            while (iter.hasNext()) {
                Attachment attachment = iter.next();
                if (attachment.isInline() && attachment.getContentId() != null && !attachment.getContentId().isEmpty()) {
                    embeddedObjects.add(attachment);
                } else {
                    properAttachments.add(attachment);
                }
            }

            mail = buildMail(emailSession, sender, toRecipient, ccAddressList.toArray(new InternetAddress[ccAddressList.size()]), bccAddressList.toArray(new InternetAddress[bccAddressList.size()]), subject, body, embeddedObjects, properAttachments);


            Transport.send(mail, allReceipents.toArray(new InternetAddress[allReceipents.size()]));


        } catch (SendFailedException fe) {

            Address[] invalidAddress = fe.getInvalidAddresses();
            for (Address a : invalidAddress) {
                System.out.println("Invalid Address " + a.toString());
            }
            /*
		    if(fe.getCause() instanceof SMTPAddressSucceededException){
		    	try{
					Store store = this.emailSession.getStore("pop3");
					
					final String username = props.getProperty("username");
	    			final String password = props.getProperty("password");
	    			final String host = props.getProperty("mail.smtp.host");
					store.connect(host,username,password);
					
					Folder folder = store.getFolder("Document Store");
					if (folder == null) {
					    System.err.println("Can't get record folder.");
					    return;
					}
					if (!folder.exists())
					    folder.create(Folder.HOLDS_MESSAGES);
		
					Message[] msgs = new Message[1];
					msgs[0] = mail;
					folder.appendMessages(msgs);
					System.out.println("Mail was recorded successfully.");
					
				}catch(NoSuchProviderException e ){
					e.printStackTrace();
				}catch(MessagingException e){
					e.printStackTrace();
				}
		    }	
		    */
        } catch (MessagingException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }


    private static MimeMessage buildMail(Session session, InternetAddress sender, InternetAddress toRecipient, InternetAddress[] ccAddress, InternetAddress[] bccAddress, String subject, String htmlBody, Collection<Attachment> embededObjects, Collection<Attachment> attachments) throws MessagingException {

        MimeMessage message = new MimeMessage(session);

        message.setSubject(subject);

        String senderHost = "";

        if (sender != null) {
            message.setFrom(sender);
            message.setSender(sender);
            //senderHost="yahoo.com";
            senderHost = (String) session.getProperties().get("senderHost");
        }

        message.setRecipient(RecipientType.TO, toRecipient);

        if (ccAddress.length > 0) {
            message.setRecipients(RecipientType.CC, ccAddress);
        }

        if (bccAddress.length > 0) {
            message.setRecipients(RecipientType.BCC, bccAddress);
        }

        message.setSentDate(new Date());

        /**
         * Fill in the body
         *
         */

        if (embededObjects != null && embededObjects.size() > 0) { // HTML with embeded objects

            MimeMultipart multipart = createdRelaed(senderHost, htmlBody, embededObjects);

            if (attachments != null && attachments.size() > 0) {
                multipart = createMixed(asBody(multipart), attachments);
            }

            message.setContent(multipart);

        } else if (attachments != null && attachments.size() > 0) {

            MimeBodyPart bodyPart = new MimeBodyPart();
            bodyPart.setText(htmlBody, "utf-8", "html");
            bodyPart.setDisposition(Part.INLINE);
            message.setContent(createMixed(bodyPart, attachments));
        } else {
            message.setText(htmlBody, "utf-8", "html");
        }

        message.saveChanges();

        return message;

    }

    private static MimeMultipart createMixed(MimeBodyPart firstBodyPart,
                                             Collection<Attachment> attachments) throws MessagingException {
        MimeMultipart mixed = new MimeMultipart();
        mixed.addBodyPart(firstBodyPart);

        for (Attachment part : attachments) {
            mixed.addBodyPart(makeBody(part.getMimeType(), Part.ATTACHMENT, wrap72(part.getContent()), UUID.randomUUID().toString(), part.getFilename()));
        }
        return mixed;
    }

    /**
     * Create a body part
     *
     * @param mimeType
     * @param attachment
     * @param content    base 64 encoded
     * @param string
     * @param filename
     * @return
     * @throws MessagingException
     */
    private static BodyPart makeBody(String mimeType, String disposition,
                                     String content, String cid, String filename) throws MessagingException {

        PreencodedMimeBodyPart preencodedMimeBodyPart = new PreencodedMimeBodyPart("base64");

        preencodedMimeBodyPart.setText(content);
        preencodedMimeBodyPart.setContentID(cid);
        preencodedMimeBodyPart.setDisposition(disposition);

        if (filename != null) {
            preencodedMimeBodyPart.setFileName(filename);
        }

        preencodedMimeBodyPart.setHeader("Content-Type", mimeType);
        return preencodedMimeBodyPart;
    }

    private static MimeBodyPart asBody(MimeMultipart multipart) throws MessagingException {
        MimeBodyPart wrap = new MimeBodyPart();
        wrap.setContent(multipart);
        return wrap;
    }

    private static MimeMultipart createdRelaed(String senderHost, String body,
                                               Collection<Attachment> embededObjects) throws MessagingException {

        String htmlCID = UUID.randomUUID().toString() + "@" + senderHost;
        MimeBodyPart htmlPart = new MimeBodyPart();
        htmlPart.setText(body, "utf-8", "html");
        htmlPart.setContentID("<" + htmlCID + ">");
        htmlPart.setDisposition(Part.INLINE);
        MimeMultipart related = new MimeMultipart("related; type=\"text/html\"; start= \"<" + htmlCID + ">\"");
        related.addBodyPart(htmlPart);
        for (Attachment part : embededObjects) {
            related.addBodyPart(makeBody(part.getMimeType(), Part.INLINE, wrap72(part.getContentId()), part.getContentId(), null));
        }

        return related;

    }


    /**
     * Format string to conform to 72 character width
     *
     * @param in
     * @return string hard warraped at 72 characters
     */
    private static String wrap72(String in) {

        final int length = 72;
        int i = 0;
        int j = length;

        StringBuilder out = new StringBuilder(in.length() * 74 / 72);

        while (j < in.length()) {
            out.append(in.substring(i, j)).append("\r\n");
            i = j;
            j += length;
        }

        if (i < in.length() - 1) {
            out.append(in.substring(i));
        }

        return out.toString();

    }


}
