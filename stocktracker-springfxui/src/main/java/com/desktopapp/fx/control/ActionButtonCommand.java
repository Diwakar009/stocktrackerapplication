/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.desktopapp.fx.control;

import com.desktopapp.fx.DesktopFxApplication;
import com.desktopapp.fx.commands.ICommand;
import com.desktopapp.fx.control.fonticon.FontIcon;
import com.desktopapp.fx.controller.StockMaintenanceController;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.input.KeyCodeCombination;

/**
 * Created by diwakar009 on 18/11/2019.
 */
public class ActionButtonCommand extends ActionCommand{

    private static final String BUTTTON_STYLE_CLASS = "button-command";


    public ActionButtonCommand(String text, KeyCodeCombination keyCodeCombination){
        super(text,keyCodeCombination);
        getStyleClass().add(BUTTTON_STYLE_CLASS);
    }

    public ActionButtonCommand(String text, KeyCodeCombination keyCodeCombination, EventHandler<ActionEvent> action){
        super(text,keyCodeCombination,action);
        getStyleClass().add(BUTTTON_STYLE_CLASS);
     }

    public ActionButtonCommand(String text, EventHandler<ActionEvent> action){
        super(text,action);
        getStyleClass().add(BUTTTON_STYLE_CLASS);
    }

    public ActionButtonCommand(String text){
        super(text);
        getStyleClass().add(BUTTTON_STYLE_CLASS);
    }

    public ActionButtonCommand(String text,ProgressIndicator progressIndicator, EventHandler<ActionEvent> action){
        super(text);
        getStyleClass().add(BUTTTON_STYLE_CLASS);
        if(progressIndicator != null) {
            progressIndicator.setMaxSize(30, 30);
            progressIndicator.setVisible(false);
            EventHandler<ActionEvent>  actionWrapper = new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent actionEvent) {
                    Task<Boolean> task = new Task<Boolean>() {
                        @Override
                        protected Boolean call() throws Exception {
                            action.handle(actionEvent);
                            return Boolean.TRUE;
                        }
                    };
                    progressIndicator.visibleProperty().bind(task.runningProperty());
                    new Thread(task).start();
                }
            };
            setOnActionEvent(actionWrapper);
        }else{
            setOnActionEvent(action);
        }
    }

}
