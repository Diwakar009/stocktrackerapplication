package com.stocktracker.model;

import com.stocktracker.framework.IEntity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;


/**
 * The persistent class for the static_code_decode database table.
 * 
 */
@Entity
@Table(name="static_code_decode")
public class StaticCodeDecode extends BaseEntity implements Serializable {
	private static final long serialVersionUID = 1L;


	@EmbeddedId
	private StaticCodeDecodePK id;

	@Column(name="CODE_DESC", length = 1000)
	private String codeDesc;

	
	public StaticCodeDecode() {
	}

	public StaticCodeDecode(String codeName,String language) {
		setCodeName(codeName);
		setLanguage(language);
	}

		
	public String getCodeName() {
		if(getId() == null){
			this.id = new StaticCodeDecodePK();
		}
		
		return this.id.getCodeName();
	}
	
	public void setCodeName(String codeName) {
		
		if(getId() == null){
			this.id = new StaticCodeDecodePK();
		}
		
		this.id.setCodeName(codeName);
	}
	public String getLanguage() {
		if(getId() == null){
			this.id = new StaticCodeDecodePK();
		}
		
		return this.id.getLanguage();
	}
	public void setLanguage(String language) {
		
		if(getId() == null){
			this.id = new StaticCodeDecodePK();
		}
		
		this.id.setLanguage(language);
	}
	public String getCodeValue() {
		if(getId() == null){
			this.id = new StaticCodeDecodePK();
		}
		
		return this.id.getCodeValue();
	}
	public void setCodeValue(String codeValue) {
		
		if(getId() == null){
			this.id = new StaticCodeDecodePK();
		}
		
		this.id.setCodeValue(codeValue);
	}

	public StaticCodeDecodePK getId() {
		return this.id;
	}

	public void setId(StaticCodeDecodePK id) {
		this.id = id;
	}

	public String getCodeDesc() {
		return this.codeDesc;
	}

	public void setCodeDesc(String codeDesc) {

		this.codeDesc = codeDesc;
	}

	public String getCodeValueFilter() {
		return this.id.getCodeValueFilter();
	}

	public void setCodeValueFilter(String codeValueFilter) {
		if(getId() == null){
			this.id = new StaticCodeDecodePK();
		}

		this.id.setCodeValueFilter(codeValueFilter);
	}
	
	@Override
	public Serializable getEntityPK() {
		return getId();
	}

	@Override
	public void copyEntity(IEntity sourceEntity) {
		StaticCodeDecode source = (StaticCodeDecode)sourceEntity;
		this.setCreatedBy(source.getCreatedBy());
		this.setUpdatedBy(source.getUpdatedBy());
		this.setCreatedAt(source.getCreatedAt());
		this.setUpdatedAt(source.getUpdatedAt());
		this.setCodeName(source.getCodeName());
		this.setLanguage(source.getLanguage());
		this.setCodeValue(source.getCodeValue());
		this.setCodeDesc(source.getCodeDesc());
		this.setCodeValueFilter(source.getCodeValueFilter());
	}


	@Override
	public String toString() {
		return "StaticCodeDecode{" +
				"id=" + id +
				", codeDesc='" + codeDesc + '\'' +
				'}';
	}
}