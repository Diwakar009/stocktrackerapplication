/*
 * Copyright (C) 2017 Cem Ikta
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 *
 */

package com.desktopapp.fx.control;

import com.desktopapp.fx.common.IGenericComponent;
import com.stocktracker.util.DateTimeUtil;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.util.StringConverter;
import javafx.util.converter.IntegerStringConverter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.TimeZone;
import java.util.function.UnaryOperator;


/**
 * Integer field control
 *
 * @see TextField
 * @see IntegerStringConverter
 * @see TextFormatter
 * @see UnaryOperator
 *
 * @author Diwakar C
 */
public class DateTimeField extends DatePicker implements IGenericComponent{

    private FxDatePickerConverter converter;

    public DateTimeField(LocalDate localDate){
        super(localDate);
        init();
    }

    public DateTimeField() {
        super();
        init();
    }

    public void init(){
        setPrefWidth(120);
        converter = new FxDatePickerConverter();
        this.setPromptText("DD/MM/YYYY");

        /*.date-picker-popup > .month-year-pane {
            -fx-padding: 0.588883em 0.5em 0.666667em 0.5em; *//* 7 6 8 6 *//*
            -fx-background-color: derive(-fx-box-border,30%),
                    linear-gradient(to bottom, derive(-fx-base,-3%),
                            derive(-fx-base,5%) 50%,
                            derive(-fx-base,-3%));
            -fx-background-insets: 0 0 0 0, 0 0 1 0;
        }*/

        setEditable(true);
        addEventFilter(KeyEvent.ANY, (KeyEvent event) -> {
            if(event.getCode().isDigitKey()
                    || event.getCode().equals(KeyCode.DOWN)
                    || event.getCode().equals(KeyCode.KP_DOWN)) {

                if(!isShowing()) {
                    show();
                    getEditor().requestFocus();
                }
                event.consume();
            }else{
                if(event.getCode().equals(KeyCode.TAB)
                        || event.getCode().equals(KeyCode.BACK_SPACE)
                        || event.getCode().equals(KeyCode.DELETE)){
                    // continuw
                }else{
                    event.consume();
                }
            }
        });

       /* focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observableValue, Boolean aBoolean, Boolean t1) {
                if(t1){
                    System.out.println("Focus In.............................. ");
                    if(!isShowing()) {
                        show();
                        getEditor().requestFocus();
                    }
                }else{
                    System.out.println("Focus Out.............................. ");
                }
            }
        });*/
    }

    @Override
    public void setDataValue(String value) {
        setValue(converter.convertUTCDateStringToLocalDate(value));
    }

    /**
     * Set Data Value using Date
     *
     * @param dataValue
     */
    public void setDataValue(Date dataValue){
        if(dataValue != null) {
            setValue(converter.convertDateToLocalDate(dataValue));
        }
    }



    @Override
    public void setAsReadOnly(boolean readOnly) {
         this.setDisabled(readOnly);
    }

    @Override
    public void destroyComponent() {

    }


    @Override
    public boolean isDataExchangeApplicable() {
        return true;
    }

    @Override
    public String getDataValue() {
         Date date = converter.convertLocalDateInUTC(getValue());
         return DateTimeUtil.convertDateToUTCString(date);
    }

    public Date getDataValueAsDate() {

        if(getValue() != null){
            return converter.convertLocalDateInUTC(getValue());
        }

        return null;
    }

    @Override
    public boolean isComponentShowing() {
        return isShowing();
    }

    @Override
    public void clearDataValue() {
        this.setValue(null);
    }

    @Override
    public boolean isReadonly() {
        return super.isDisabled();
    }


}

/**
 *
 */
class FxDatePickerConverter  extends StringConverter<LocalDate>{

    private String pattern = "dd/MM/yyyy";
    private DateTimeFormatter dtFormatter;

    public FxDatePickerConverter()
    {
        dtFormatter = DateTimeFormatter.ofPattern(pattern);
    }

    @Override
    public String toString(LocalDate localDate) {
        return dtFormatter.format(localDate);
    }

    public LocalDate fromString(String text)
    {
        LocalDate date = null;

        if (text != null && !text.trim().isEmpty()){
            date = LocalDate.parse(text, dtFormatter);
        }

        return date;
    }

    public LocalDate convertDateToLocalDate(Date date){
        //Getting the default zone id
        ZoneId defaultZoneId = ZoneId.systemDefault();

        //Converting the date to Instant
        Instant instant = date.toInstant();

        //Converting the Date to LocalDate
        LocalDate localDate = instant.atZone(defaultZoneId).toLocalDate();

        return localDate;
    }

    public Date convertLocalDateInUTC(LocalDate date) {
        return Date.from(date.atStartOfDay().toInstant(ZoneOffset.UTC));
    }

    public LocalDate convertUTCDateStringToLocalDate(String utcDate){
        Date date = null;

        try {
            date = DateTimeUtil.convertUTCStringToDate(utcDate);
        }catch (ParseException parseException){
            parseException.printStackTrace();
        }
        if(date != null){
            return convertDateToLocalDate(date);
        }
       return null;
    }


}