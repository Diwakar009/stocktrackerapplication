/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.domainobjs.json;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.stocktracker.domainobjs.IComboBoxAdapter;

@JsonIgnoreProperties (ignoreUnknown=true)
@JsonInclude(Include.NON_EMPTY)
@JsonPropertyOrder({"codeName","codeValue","language","codeFilterValue","codeDescription"})
public class CodeDataItemDO implements Serializable ,IComboBoxAdapter<CodeDataItemDO> {

	public static CodeDataItemDO BLANK_CODEDATA = new CodeDataItemDO();
	
	private static final long serialVersionUID = -410974513980892815L;

	public static String CLASS_NAME = CodeDataItemDO.class.getName();

	public static final String DEFAULT_LANGUAGE="EN_US";
	
	@JsonProperty("codeName")
	private String codeName;

	@JsonProperty("codeValue")
	private String codeValue;

	@JsonProperty("codeFilterValue")
	private String codeFilterValue;

	@JsonProperty("language")
	private String language;

	@JsonProperty("codeDescription")
	private String codeDescription;


	public CodeDataItemDO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CodeDataItemDO(String codeName, String codeValue, String language) {
		this.codeName = codeName;
		this.codeValue = codeValue;
		this.language = language;
	}

	public CodeDataItemDO(String codeName, String codeValue, String codeFilterValue, String language, String codeDescription) {
		super();
		this.codeName  = codeName;
		this.codeValue  =codeValue;
		this.codeFilterValue=codeFilterValue;
		this.language=language;
		this.codeDescription = codeDescription;
	}

	public CodeDataItemDO(String codeName, String codeValue, String codeFilterValue, String language) {
		this.codeName = codeName;
		this.codeValue = codeValue;
		this.codeFilterValue = codeFilterValue;
		this.language = language;
	}

	public String getCodeName() {
		return codeName;
	}
	public void setCodeName(String codeName) {
		this.codeName = codeName;
	}
	public String getCodeValue() {
		return codeValue;
	}
	public void setCodeValue(String codeValue) {
		this.codeValue = codeValue;
	}
	public String getCodeDescription() {
		return codeDescription;
	}
	public void setCodeDescription(String codeDescription) {
		this.codeDescription = codeDescription;
	}

	public String getCodeFilterValue() {
		return codeFilterValue;
	}

	public void setCodeFilterValue(String codeFilterValue) {
		this.codeFilterValue = codeFilterValue;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}


	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		CodeDataItemDO that = (CodeDataItemDO) o;

		if (codeName != null ? !codeName.equals(that.codeName) : that.codeName != null) return false;
		if (codeValue != null ? !codeValue.equals(that.codeValue) : that.codeValue != null) return false;
		if (codeFilterValue != null ? !codeFilterValue.equals(that.codeFilterValue) : that.codeFilterValue != null)
			return false;
		if (language != null ? !language.equals(that.language) : that.language != null) return false;
		return codeDescription != null ? codeDescription.equals(that.codeDescription) : that.codeDescription == null;
	}

	@Override
	public int hashCode() {
		int result = codeName != null ? codeName.hashCode() : 0;
		result = 31 * result + (codeValue != null ? codeValue.hashCode() : 0);
		result = 31 * result + (codeFilterValue != null ? codeFilterValue.hashCode() : 0);
		result = 31 * result + (language != null ? language.hashCode() : 0);
		result = 31 * result + (codeDescription != null ? codeDescription.hashCode() : 0);
		return result;
	}

	@Override
	public String toString() {
		return codeDescription;
	}

	@Override
	public String getSelectedItemText(CodeDataItemDO itemDO) {
		return itemDO.getCodeDescription();
	}
}
