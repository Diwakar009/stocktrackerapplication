package com.desktopapp.fx.config;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * Created by diwakar009 on 14/12/2018.
 */
@JsonIgnoreProperties(ignoreUnknown=true)
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class SplashConfig implements Serializable{

    /**
     * Override JSON Property for the splash font icon.
     * splashIcon : Splash font Icon
     * splashIconColor: Splash Icon Color
     *
     */

    @JsonProperty("splashIcon")
    private String splashIcon;

    @JsonProperty("splashIconColor")
    private String splashIconColor;

    /**
     * Override property using image path.
     *
     */
    @JsonProperty("splashImagePath")
    private String splashImagePath;

    /**
     *  Font Icon Size
     *
     */
    @JsonProperty("fontIconSize")
    private String fontIconSize;

    public String getSplashIcon() {
        return splashIcon;
    }

    public void setSplashIcon(String splashIcon) {
        this.splashIcon = splashIcon;
    }

    public String getSplashIconColor() {
        return splashIconColor;
    }

    public void setSplashIconColor(String splashIconColor) {
        this.splashIconColor = splashIconColor;
    }

    public String getSplashImagePath() {
        return splashImagePath;
    }

    public void setSplashImagePath(String splashImagePath) {
        this.splashImagePath = splashImagePath;
    }

    public String getFontIconSize() {
        return fontIconSize;
    }

    public void setFontIconSize(String fontIconSize) {
        this.fontIconSize = fontIconSize;
    }
}
