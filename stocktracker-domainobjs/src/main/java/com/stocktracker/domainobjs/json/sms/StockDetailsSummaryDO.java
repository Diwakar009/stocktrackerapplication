/*
 * Copyright (c) 2020.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.domainobjs.json.sms;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * Created by diwakar009 on 6/11/2019.
 */
@JsonIgnoreProperties(ignoreUnknown=true)
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class StockDetailsSummaryDO implements Serializable {

    public static String CLASS_NAME = StockDetailsSummaryDO.class.getName();

    @JsonProperty("industryCode")
    private String industryCode;

    @JsonProperty("stockCode")
    private String stockCode;

    @JsonProperty("stockVarietyCode")
    private String stockVarietyCode;

    @JsonProperty("transactionType")
    private String transactionType;

    @JsonProperty("noOfPacketsSummary")
    private Long noOfPacketsSummary;

    @JsonProperty("weightmentSummary")
    private Double weightmentSummary;

    @JsonProperty("lampEntrySummary")
    private Double lampEntrySummary;

    @JsonProperty("lampEntryAftAdjSummary")
    private Double lampEntryAftAdjSummary;

    @JsonProperty("actualAmountSummary")
    private Double actualAmountSummary;


    public StockDetailsSummaryDO() {
    }

    public String getStockCode() {
        return stockCode;
    }

    public void setStockCode(String stockCode) {
        this.stockCode = stockCode;
    }

    public String getStockVarietyCode() {
        return stockVarietyCode;
    }

    public void setStockVarietyCode(String stockVarietyCode) {
        this.stockVarietyCode = stockVarietyCode;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public Long getNoOfPacketsSummary() {
        return noOfPacketsSummary;
    }

    public void setNoOfPacketsSummary(Long noOfPacketsSummary) {
        this.noOfPacketsSummary = noOfPacketsSummary;
    }

    public Double getWeightmentSummary() {
        return weightmentSummary;
    }

    public void setWeightmentSummary(Double weightmentSummary) {
        this.weightmentSummary = weightmentSummary;
    }

    public Double getLampEntrySummary() {
        return lampEntrySummary;
    }

    public void setLampEntrySummary(Double lampEntrySummary) {
        this.lampEntrySummary = lampEntrySummary;
    }

    public Double getLampEntryAftAdjSummary() {
        return lampEntryAftAdjSummary;
    }

    public void setLampEntryAftAdjSummary(Double lampEntryAftAdjSummary) {
        this.lampEntryAftAdjSummary = lampEntryAftAdjSummary;
    }

    public Double getActualAmountSummary() {
        return actualAmountSummary;
    }

    public void setActualAmountSummary(Double actualAmountSummary) {
        this.actualAmountSummary = actualAmountSummary;
    }

    public String getIndustryCode() {
        return industryCode;
    }

    public void setIndustryCode(String industryCode) {
        this.industryCode = industryCode;
    }

}
