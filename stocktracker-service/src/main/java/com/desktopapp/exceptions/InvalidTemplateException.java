/*
 * Copyright (c) 2020.
 *
 * All Rights are with Diwakar C
 */

package com.desktopapp.exceptions;

import com.stocktracker.exceptions.BusinessException;

public class InvalidTemplateException extends BusinessException {

    public InvalidTemplateException(String s) {
        super(s);
    }


}
