/*
 * Copyright (c) 2020.
 *
 * All Rights are with Diwakar C
 */

package com.desktopapp.fx.view.stockmaintenance;

import com.desktopapp.fx.app.AppFontIcons;
import com.desktopapp.fx.common.ITableDataViewBehaviour;
import com.desktopapp.fx.common.PageDetails;
import com.desktopapp.fx.control.QuantityField;
import com.desktopapp.fx.control.TableColumnBuilder;
import com.desktopapp.fx.control.fonticon.FontIcon;
import com.desktopapp.fx.controller.StockMaintenanceController;
import com.stocktracker.component.constants.CodeDecodeConstants;
import com.stocktracker.domainobjs.json.DOPage;
import com.stocktracker.domainobjs.json.TableRowMapDO;
import com.stocktracker.domainobjs.json.sms.StockDetailsSummaryDO;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.scene.Node;
import javafx.scene.control.TableColumn;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Stock Details Table properties and behaviour
 */
class StockDetialsSummaryTableBehaviour implements ITableDataViewBehaviour<StockDetailsSummaryDO> {

    public static  final DecimalFormat DECIMAL_FORMAT = new DecimalFormat("0.00");

    private  StockDetailsTransactionSearchPage page;

    public StockDetialsSummaryTableBehaviour(StockDetailsTransactionSearchPage page) {
        this.page = page;
    }

    @Override
    public List<TableColumn<StockDetailsSummaryDO, ?>> getTableViewColumns() {

        List<TableColumn<StockDetailsSummaryDO, ?>> columns = new ArrayList<>();

        TableColumn<StockDetailsSummaryDO, String> industryCodeCol = TableColumnBuilder.<StockDetailsSummaryDO, String> create()
                .fieldName("industryCode")
                .prefWidth(100)
                .decodeCode(CodeDecodeConstants.CD_INDUSTRY_CODE)
                .title("Industry Code")
                .build();

        columns.add(industryCodeCol);

        TableColumn<StockDetailsSummaryDO, String> stockCodeCol = TableColumnBuilder.<StockDetailsSummaryDO, String> create()
                .fieldName("stockCode")
                .prefWidth(100)
                .decodeCode(CodeDecodeConstants.CD_STOCK_CODE)
                .title("Stock Code")
                .build();

        columns.add(stockCodeCol);

        TableColumn<StockDetailsSummaryDO, String> stockVarietyCodeCol = TableColumnBuilder.<StockDetailsSummaryDO, String> create()
                .fieldName("stockVarietyCode")
                .prefWidth(100)
                .decodeCode(CodeDecodeConstants.CD_STOCK_VARIETY)
                .title("Stock Variety Code")
                .build();

        columns.add(stockVarietyCodeCol);

        TableColumn<StockDetailsSummaryDO, String> transactionTypeCol = TableColumnBuilder.<StockDetailsSummaryDO, String> create()
                .fieldName("transactionType")
                .prefWidth(100)
                .decodeCode(CodeDecodeConstants.CD_TRANSACTION_TYPE)
                .title("Transaction Type")
                .build();

        columns.add(transactionTypeCol);

        TableColumn<StockDetailsSummaryDO, Long> noOfPacketsSummaryCol = TableColumnBuilder.<StockDetailsSummaryDO, Long> create()
                .prefWidth(100)
                .title("Packets Summary")
                .build();

        noOfPacketsSummaryCol.setCellValueFactory(cellvaluefac -> {
            if(cellvaluefac.getValue().getNoOfPacketsSummary() != null) {
                return new ReadOnlyObjectWrapper(cellvaluefac.getValue().getNoOfPacketsSummary() + " " + TableColumnBuilder.getNoOfPacketsUnit(cellvaluefac.getValue().getStockCode(),
                        cellvaluefac.getValue().getStockVarietyCode()));
            }
            return new ReadOnlyObjectWrapper("");
        });

        columns.add(noOfPacketsSummaryCol);

        TableColumn<StockDetailsSummaryDO, Double> weightmentSummaryCol = TableColumnBuilder.<StockDetailsSummaryDO, Double> create()
                .fieldName("stockQuantity")
                .prefWidth(100)
                .title("Weightment Summary")
                .build();

        weightmentSummaryCol.setCellValueFactory(cellvaluefac -> {
            if(cellvaluefac.getValue().getWeightmentSummary() != null) {
                return new ReadOnlyObjectWrapper(DECIMAL_FORMAT.format(cellvaluefac.getValue().getWeightmentSummary()) + " " + TableColumnBuilder.getWeightmentUnit(cellvaluefac.getValue().getStockCode(),
                        cellvaluefac.getValue().getStockVarietyCode()));
            }
            return new ReadOnlyObjectWrapper("");
        });

        columns.add(weightmentSummaryCol);

        TableColumn<StockDetailsSummaryDO, Double> lampEntryCol = TableColumnBuilder.<StockDetailsSummaryDO, Double> create()
                .title("Lamp Entry Summary")
                .build();

        lampEntryCol.setCellValueFactory(cellvaluefac -> {
            if(cellvaluefac.getValue().getLampEntrySummary() != null) {
                return new ReadOnlyObjectWrapper( DECIMAL_FORMAT.format(cellvaluefac.getValue().getLampEntrySummary()) + " " + QuantityField.QUANTITY_WEIGHTMENT_UNIT);
            }
            return new ReadOnlyObjectWrapper("");
       });
        columns.add(lampEntryCol);


        TableColumn<StockDetailsSummaryDO, Double> finalEntryCol = TableColumnBuilder.<StockDetailsSummaryDO, Double> create()
                .title("Final Entry Summary")
                .build();

        finalEntryCol.setCellValueFactory(cellvaluefac -> {
            if(cellvaluefac.getValue().getLampEntryAftAdjSummary() != null) {
                return new ReadOnlyObjectWrapper( DECIMAL_FORMAT.format(cellvaluefac.getValue().getLampEntryAftAdjSummary()) + " " + QuantityField.QUANTITY_WEIGHTMENT_UNIT);
            }
            return new ReadOnlyObjectWrapper("");
        });

        columns.add(finalEntryCol);

        TableColumn<StockDetailsSummaryDO, Double> actualAmountSummary = TableColumnBuilder.<StockDetailsSummaryDO, Double> create()
                .fieldName("actualAmountSummary")
                .title("Amount Summary")
                .dataType(TableColumnBuilder.DATATYPE_AMOUNT)
                .build();

        columns.add(actualAmountSummary);


        return columns;
    }

    @Override
    public DOPage<TableRowMapDO<String, String>> getData(String filter, PageDetails details) {
        return ((StockMaintenanceController)getPage().getController()).getSummaryData(getPage().getCdIndustry().getDataValue(),
                getPage().getCdStock().getDataValue(),
                getPage().getCdStockVariety().getDataValue(),
                getPage().getCdTransactionType().getDataValue(),
                getPage().getDfStartDate().getDataValueAsDate(),
                getPage().getDfEndDate().getDataValueAsDate(),
                getPage().getTfRstNumber().getDataValue(),
                getPage().getTfVehicleNumber().getDataValue(),
                getPage().getTfPartyName().getDataValue(),
                getPage().getCdOnBehalf().getDataValue(),
                getPage().getCdLampName().getDataValue(),
                getPage().getCdMandiName().getDataValue(),
                getPage().getCdRevisitFlag().getDataValue(),
                details);
    }

    @Override
    public StockDetailsSummaryDO mapTableRowToDO(TableRowMapDO<String,String> tableRowMapDO) {
        StockDetailsSummaryDO stockDetailsSummaryDO = new StockDetailsSummaryDO();
        stockDetailsSummaryDO.setIndustryCode(tableRowMapDO.getValue("industryCode"));
        stockDetailsSummaryDO.setStockCode(tableRowMapDO.getValue("stockCode"));
        stockDetailsSummaryDO.setStockVarietyCode(tableRowMapDO.getValue("stockVarietyCode"));
        stockDetailsSummaryDO.setTransactionType(tableRowMapDO.getValue("transactionType"));

        if(tableRowMapDO.getValue("noOfPacketsSummary") != null) {
            stockDetailsSummaryDO.setNoOfPacketsSummary(Long.parseLong(tableRowMapDO.getValue("noOfPacketsSummary")));
        }

        if(tableRowMapDO.getValue("weightmentSummary") != null) {
            stockDetailsSummaryDO.setWeightmentSummary(Double.parseDouble(tableRowMapDO.getValue("weightmentSummary")));
        }

        if(tableRowMapDO.getValue("lampEntrySummary") != null) {
            stockDetailsSummaryDO.setLampEntrySummary(Double.parseDouble(tableRowMapDO.getValue("lampEntrySummary")));
        }

        if(tableRowMapDO.getValue("finalEntrySummary") != null) {
            stockDetailsSummaryDO.setLampEntryAftAdjSummary(Double.parseDouble(tableRowMapDO.getValue("finalEntrySummary")));
        }

        if(tableRowMapDO.getValue("actualAmountSummary") != null) {
            stockDetailsSummaryDO.setActualAmountSummary(Double.parseDouble(tableRowMapDO.getValue("actualAmountSummary")));
        }
        return stockDetailsSummaryDO;
    }

    @Override
    public void onSelectedItemChanged() {

    }

    @Override
    public Node asNode() {
        return null;
    }

    @Override
    public FontIcon getFontIcon() {
        return AppFontIcons.STOCKMANAGEMENT;
    }

    @Override
    public String getTitle() {
        return "Stock Summary";
    }

    public StockDetailsTransactionSearchPage getPage() {
        return page;
    }

    public void setPage(StockDetailsTransactionSearchPage form) {
        this.page = form;
    }
}
