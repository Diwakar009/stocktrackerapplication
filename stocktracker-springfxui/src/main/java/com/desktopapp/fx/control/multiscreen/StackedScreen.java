package com.desktopapp.fx.control.multiscreen;

import com.desktopapp.fx.common.IAccelerator;
import com.desktopapp.fx.common.IStackedScreenProperties;
import javafx.scene.Node;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;

import java.util.Stack;

/**
 *
 * This class features screen over screen. also encapsulates the controls of keyboard accelerator switch.
 *
 *
 *
 * Created by diwakar009 on 9/6/2019.
 */


public class StackedScreen extends StackPane {


    /**
     * Maintain stack of the screens in screen stack.
     */
    private Stack<StackedScreenWrapper> screenStack;
    private StackPane center;


    public StackedScreen() {
        super();
        screenStack = new Stack<StackedScreenWrapper>();
        center = new StackPane();
        buildMultiScreenPane();
    }

    private void buildMultiScreenPane(){
        BorderPane borderPane = new BorderPane();
        borderPane.getStyleClass().addAll("stacked-screen");
        borderPane.setCenter(getCenter());
        getChildren().add(borderPane);
    }

    /**
     * Returns the current active screen.
     *
     * @return
     */
    private StackedScreenWrapper getCurrentActiveScreen(){

        if(screenStack.empty()){
            return null;
        }
        return screenStack.peek();
    }

    /**
     * Adds screen over another screen.
     *
     * Adding screen over another disables all the CHILD_ACCELERATORS tagged accelerators of old screen.
     * and enables parent and child accelerators of the current active screen.
     *
     * Parent tagged accelerator is enabled through out the life of the screen, it gets removed once the screen explicitily removed
     *
     * @param node
     * @param properties
     */
    public void addScreen(Node node, IStackedScreenProperties properties){

        if(getCurrentActiveScreen() != null) {

            if(this.getScene() != null) {
                /**
                 * Mask out old screen child accelerator.
                 */
                if (getCurrentActiveScreen().getProperties().getAccelerator().getAccelerators() != null) {
                    IAccelerator.removeChildAccelerators(this.getScene().getAccelerators(), getCurrentActiveScreen().getProperties().getAccelerator().getAccelerators());
                }
                /**
                 * Enable new screen parent and child accelerators
                 */
                if (properties != null) {
                    IAccelerator.addAllAccelerator(this.getScene().getAccelerators(), properties.getAccelerator().getAccelerators());
                }
            }
        }

        getCenter().getChildren().add((screenStack.push(new StackedScreenWrapper(node, properties)).getNode()));
    }

    /**
     * Removes the screen from the stack pane and old screen is brought forward.
     *
     * All the parent and child tagged accelerator of the removed screen is deactivated and old screen child accelerator is activated back.
     */
    public void removeScreen(){

        if(!screenStack.empty() && screenStack.size() > 1) {

            StackedScreenWrapper poppedActiveScreen = screenStack.pop();

             /**
             * Detach All accelerators.
             */
            if(poppedActiveScreen.getProperties().getAccelerator().getAccelerators() != null) {
                if(this.getScene() != null) {
                    IAccelerator.removeAllAccelerator(this.getScene().getAccelerators(), poppedActiveScreen.getProperties().getAccelerator().getAccelerators());
                }
            }

            /**
             * Attach child accelerators back of the current active screen
             */

            StackedScreenWrapper nextActiveScreen = getCurrentActiveScreen(); // change the current active screen

            /**
             * Assign child accelerators
             */
            if(nextActiveScreen.getProperties().getAccelerator().getAccelerators() != null) {
                if(this.getScene() != null) {
                    IAccelerator.addChildAccelerators(this.getScene().getAccelerators(), nextActiveScreen.getProperties().getAccelerator().getAccelerators());
                }
            }
            /**
             * Remove the active screen from the scene
             */
            getCenter().getChildren().remove(poppedActiveScreen.getNode());


        }
    }

    public StackPane getCenter() {
        return center;
    }

    public void setCenter(StackPane center) {
        this.center = center;
    }


    public IStackedScreenProperties getActiveScreenProperties(){
       return getCurrentActiveScreen().getProperties();
    }

}

/**
 *
 * Bean for storing details of the screen.

 */

class StackedScreenWrapper {
    private String screenName;
    private IStackedScreenProperties properties;
    private Node node;


    public StackedScreenWrapper(String screenName, Node node, IStackedScreenProperties properties) {
        this.screenName = screenName;
        this.properties = properties;
        this.node = node;
    }

    public StackedScreenWrapper(Node node, IStackedScreenProperties properties) {
        this.properties = properties;
        this.node = node;
    }

    public StackedScreenWrapper(Node node) {
        this.node = node;
    }

    public IStackedScreenProperties getProperties() {
        return properties;
    }

    public void setProperties(IStackedScreenProperties properties) {
        this.properties = properties;
    }

    public Node getNode() {
        return node;
    }

    public void setNode(Node node) {
        this.node = node;
    }

    public String getScreenName() {
        return screenName;
    }

    public void setScreenName(String screenName) {
        this.screenName = screenName;
    }


    @Override
    public String toString() {
        return "StackedScreenWrapper{" +
                "screenName='" + screenName + '\'' +
                '}';
    }
}