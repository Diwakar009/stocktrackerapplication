/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.util;

import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ValidationUtil {
	
		public static final Pattern VALID_EMAIL_ADDRESS_REGEX = 
			    Pattern.compile("(([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)(\\s*(;|,)\\s*|\\s*$))*", Pattern.CASE_INSENSITIVE);
		
		public static final Pattern VALID_NUMERIC_REGEX = Pattern.compile("[0-9]*");
		
		public static final Pattern VALID_URL_REGEX = Pattern.compile("([h]|[H])([t]|[T])([t]|[T])([p]|[P])([s]|[S]){0,1}://([^:/]+)(:([0-9]+))?(/\\S*)*");
	
	
		/**
		 * Validation for email address format (Supports muliple email address format)
		 * 
		 * @param email
		 * @return
		 */
		public static boolean isEmailFormatValid(String email){
			Matcher matcher = VALID_EMAIL_ADDRESS_REGEX .matcher(email);
	        return matcher.matches();
		}
	
	
	   /**
		 * Validation method for Number Format (decimal is excluded)
		 * 
		 * @param text
		 * @return Return true if validation ok, otherwise return false.
		 */
		public static boolean isNumeric(String text)
		{
			if( text == null )
				return false;
			
			return VALID_NUMERIC_REGEX.matcher(text.trim()).matches();
		}
		
		
		/**
		 * Validation method for date. It is a very strict date validator.
		 * 
		 * @param text Input date to be validated.
		 * @param pattern Inputed date pattern.
		 * @return Return true if validation ok, otherwise return false.
		 */
		public static boolean isDate(String text, String pattern)
		{
			if (text == null || text.trim().equals(""))
				return false;
			
			try
			{
				SimpleDateFormat formatter = new SimpleDateFormat(pattern);
				formatter.setLenient(false);
				formatter.parse(text);
			}
			catch (Exception e)
			{
				return false;
			}
			try
			{
				//date pattern: MM/dd/yyyy
				int mPos, dPos, yPos, mVal, dVal, yVal;
				String _pattern = pattern.toUpperCase();
				String separator = String.valueOf(getSeparator(_pattern));
				String[] _patternArr = _pattern.split(separator);
				mPos = getPosition(_patternArr, "[M]{1,}");
				dPos = getPosition(_patternArr, "[D]{1,}");
				yPos = getPosition(_patternArr, "[Y]{1,}");

				String[] dateVals = text.trim().split(separator);
				mVal = Integer.parseInt(dateVals[mPos]);
				dVal = Integer.parseInt(dateVals[dPos]);
				yVal = Integer.parseInt(dateVals[yPos]);

				if (mVal < 1 || mVal > 12)
				{
					return false;
				}
				if (dVal < 1 || dVal > 31)
				{
					return false;
				}
				if (yVal < 0 || yVal > 9999)
				{
					return false;
				}
				int[] num_of_days_in_monthes = getNumberOfDaysInMonthes(yVal);
				if (dVal > num_of_days_in_monthes[mVal - 1])
				{
					return false;
				}
			}
			catch (Exception eg)
			{
				return false;
			}
			return true;
		}
		private static int getPosition(String[] patternArr, String regex)
		{
			for (int i = 0; i < patternArr.length; i++)
			{
				if (Pattern.compile(regex).matcher(patternArr[i]).matches())
					return i;
			}
			throw new java.lang.IllegalStateException("Cannot get the position by the RegExp");
		}

		private static char getSeparator(String pattern)
		{
			for (int i = 0; i < pattern.length(); i++)
			{
				char c = pattern.charAt(i);
				if (c < 65 || c > 90)
				{
					return c;
				}
			}
			throw new java.lang.IllegalArgumentException("Inputted date pattern is fatal wrong.");
		}
		private static int[] getNumberOfDaysInMonthes(int theyear)
		{
			return new int[] { 31, getLeapYear(theyear), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
		}

		private static int getLeapYear(int theyear)
		{
			return ((theyear % 4 == 0 && theyear % 100 != 0) || theyear % 400 == 0) ? 29 : 28;
		}
		
		 /**
	     * this method is moved from ValidatorUtil
	     * @author rocket.he
	     * @param url
	     * @return
	     */
	    public static boolean isValidURLAddress(String url)
		{
	    	Matcher matcher = VALID_URL_REGEX .matcher(url);
	        return matcher.matches();
		}
	    
	    
	
}
