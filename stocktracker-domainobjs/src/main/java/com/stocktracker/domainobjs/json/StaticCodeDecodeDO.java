/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.domainobjs.json;

import java.io.Serializable;

import com.stocktracker.model.BaseEntity;
import com.stocktracker.model.StaticCodeDecode;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;


/**
 * The persistent class for the static_code_decode database table.
 * 
 */
@JsonIgnoreProperties (ignoreUnknown=true)
@JsonInclude(Include.NON_EMPTY)
public class StaticCodeDecodeDO extends BaseDomainObject implements Serializable {
	private static final long serialVersionUID = 1L;

	public static String CLASS_NAME = StaticCodeDecodeDO.class.getName();

	@JsonProperty("id")
	private StaticCodeDecodeDOPK id;

	@JsonProperty("codeDesc")
	private String codeDesc;


	public StaticCodeDecodeDO() {
		super();
	}

	public StaticCodeDecodeDO(StaticCodeDecode codeDecode) {
		copyEntity2DomainObject(codeDecode);
	}

	public String getCodeName() {
		if(getId() == null){
			this.id = new StaticCodeDecodeDOPK();
		}
		
		return this.id.getCodeName();
	}
	
	public void setCodeName(String codeName) {
		
		if(getId() == null){
			this.id = new StaticCodeDecodeDOPK();
		}
		
		this.id.setCodeName(codeName);
	}
	public String getLanguage() {
		if(getId() == null){
			this.id = new StaticCodeDecodeDOPK();
		}
		
		return this.id.getLanguage();
	}
	public void setLanguage(String language) {
		
		if(getId() == null){
			this.id = new StaticCodeDecodeDOPK();
		}
		
		this.id.setLanguage(language);
	}
	public String getCodeValue() {
		if(getId() == null){
			this.id = new StaticCodeDecodeDOPK();
		}
		
		return this.id.getCodeValue();
	}
	public void setCodeValue(String codeValue) {
		
		if(getId() == null){
			this.id = new StaticCodeDecodeDOPK();
		}
		
		this.id.setCodeValue(codeValue);
	}

	public String getCodeValueFilter() {
		if(getId() == null){
			this.id = new StaticCodeDecodeDOPK();
		}

		return this.id.getCodeValueFilter();
	}
	public void setCodeValueFilter(String codeValue) {

		if(getId() == null){
			this.id = new StaticCodeDecodeDOPK();
		}

		this.id.setCodeValueFilter(codeValue);
	}




	@Override
	public StaticCodeDecodeDOPK getId() {
		return this.id;
	}

	public void setId(StaticCodeDecodeDOPK id) {
		this.id = id;
	}

	public String getCodeDesc() {
		return this.codeDesc;
	}

	public void setCodeDesc(String codeDesc) {
		this.codeDesc = codeDesc;
	}

	@JsonIgnore
	@Override
	public Object getDomainObjectKey() {
		return getId();
	}

	/*
	@Override
	public String toString() {
		return "IndustryDO [codeDesc=" + codeDesc + "]";
	}
	*/


	@Override
	public StaticCodeDecode copyDomainObject2Entity(BaseEntity entity) {
		StaticCodeDecode staticCodeDecode = (StaticCodeDecode)entity;
		staticCodeDecode.setCreatedBy(this.getCreatedBy());
		staticCodeDecode.setUpdatedBy(this.getUpdatedBy());
		staticCodeDecode.setCreatedAt(this.getCreatedAt());
		staticCodeDecode.setUpdatedAt(this.getUpdatedAt());
		staticCodeDecode.setCodeName(this.getCodeName());
		staticCodeDecode.setLanguage(this.getLanguage());
		staticCodeDecode.setCodeValue(this.getCodeValue());
		staticCodeDecode.setCodeDesc(this.getCodeDesc());
		staticCodeDecode.setCodeValueFilter(this.getCodeValueFilter());

		return staticCodeDecode;
	}
	@Override
	public void copyEntity2DomainObject(BaseEntity entity) {
		StaticCodeDecode staticCodeDecode = (StaticCodeDecode)entity;
		this.setCreatedBy(staticCodeDecode.getCreatedBy());
		this.setUpdatedBy(staticCodeDecode.getUpdatedBy());
		this.setCreatedAt(staticCodeDecode.getCreatedAt());
		this.setUpdatedAt(staticCodeDecode.getUpdatedAt());
		this.setCodeName(staticCodeDecode.getCodeName());
		this.setLanguage(staticCodeDecode.getLanguage());
		this.setCodeValue(staticCodeDecode.getCodeValue());
		this.setCodeDesc(staticCodeDecode.getCodeDesc());
		this.setCodeValueFilter(staticCodeDecode.getCodeValueFilter());
	}

}