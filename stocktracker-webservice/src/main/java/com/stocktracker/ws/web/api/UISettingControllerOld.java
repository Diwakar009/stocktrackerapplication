package com.stocktracker.ws.web.api;

import java.util.List;


import com.stocktracker.domainobjs.json.DOPage;
import com.stocktracker.domainobjs.json.TableRowMapDO;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value="/api/uisetting")
public class UISettingControllerOld extends GenericBaseControllerOld {

	@Override
	public DOPage<TableRowMapDO<String,String>> getAll(Pageable pageRequest) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TableRowMapDO<String, String> save(
			TableRowMapDO<String, String> rowMapDO) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean delete(TableRowMapDO<String, String> rowMapDO) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean delete(List<TableRowMapDO<String, String>> rowMapDO) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public DOPage<TableRowMapDO<String, String>> getWithFilter(
			TableRowMapDO<String, String> filterMap, Pageable pageRequest) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TableRowMapDO<String, String> find(TableRowMapDO<String, String> map) {
		// TODO Auto-generated method stub
		return null;
	}


}
