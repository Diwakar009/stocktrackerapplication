/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.template.exception;

public abstract class TemplateException extends RuntimeException {

	public TemplateException(String exceptionMessage, Throwable cause) {
		super(exceptionMessage, cause);
		// TODO Auto-generated constructor stub
	}

	public TemplateException(String exceptionMessage) {
		super(exceptionMessage);
		// TODO Auto-generated constructor stub
	}
	
}
