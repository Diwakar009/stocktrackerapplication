/*
 * Copyright (C) 2017 Cem Ikta
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 *
 */

package com.desktopapp.fx.control;

import com.desktopapp.fx.framework.TableColumnCodeDecodeField;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Table column builder.
 *
 * <p>Code snippet for table columns:
 * <pre>{@code List<TableColumn<User, ?>> columns = new ArrayList<>();
 * columns.add(TableColumnBuilder.<User, Long> create()
 *      .fieldName("id")
 *      .title("ID")
 *      .prefWidth(100)
 *      .build());
 * columns.add(TableColumnBuilder.<User, String> create()
 *      .fieldName("username")
 *      .title("Username")
 *      .prefWidth(200)
 *      .build());}</pre>
 *
 * @see TableColumn
 * @see TableView
 *
 * @author Diwakar C
 */
public class TableColumnBuilder<S, T> {

    public static final String DATATYPE_DATE = "DT_DATE";
    public static final String DATATYPE_DATETIME = "DT_DATETIME";
    public static final String DATATYPE_AMOUNT="DT_AMOUNT";
    public static final String DATATYPE_STATICCODEDECODE = "DT_STATICCODEDECODE";
    public static final String DATE_FORMAT="E, dd MMMM yyyy";
    public static final String DATETIME_FORMAT="E, dd MMMM yyyy HH:mm:ss";
    public static final NumberFormat AMOUNT_CCY_FORMATTER = NumberFormat.getCurrencyInstance(new Locale("hi", "IN")); // For Rupee
    public static  final DecimalFormat DECIMAL_FORMAT = new DecimalFormat("0.00");

    private String fieldName;
    private String title;
    private double prefWidth;
    private boolean visible = true;
    private boolean sortable = true;
    private boolean resizable = true;

    private String dataType;
    private TableColumnCodeDecodeField tableColumnCodeDecodeField;

    private TableColumnBuilder() {
        this.dataType="String";
    }

    public static <S, T> TableColumnBuilder<S, T> create() {
        return new TableColumnBuilder<>();
    }

    public TableColumnBuilder<S, T> fieldName(String fieldName) {
        this.fieldName = fieldName;
        return this;
    }

    public TableColumnBuilder<S, T> title(String title) {
        this.title = title;
        return this;
    }

    public TableColumnBuilder<S, T> prefWidth(double prefWidth) {
        this.prefWidth = prefWidth;
        return this;
    }

    public TableColumnBuilder<S, T> visible(boolean visible) {
        this.visible = visible;
        return this;
    }

    public TableColumnBuilder<S, T> sortable(boolean sortable) {
        this.sortable = sortable;
        return this;
    }

    public TableColumnBuilder<S, T> resizable(boolean resizable) {
        this.resizable = resizable;
        return this;
    }

    public TableColumnBuilder<S, T> dataType(String dataType) {
        this.dataType = dataType;
        return this;
    }

    public TableColumnBuilder<S, T> decodeCode(String decodeCode) {
        this.dataType=DATATYPE_STATICCODEDECODE;
        tableColumnCodeDecodeField = new TableColumnCodeDecodeField(decodeCode);
        return this;
    }

    public TableColumn<S, T> build() {
        TableColumn<S, T> column = new TableColumn<>(title);
        if(fieldName != null) {
            column.setCellValueFactory(new PropertyValueFactory<>(fieldName));
        }

        column.setStyle( "-fx-alignment: CENTER;");

        if(DATATYPE_DATE.equals(this.dataType)){
            setDateCellFactory((TableColumn<S, Date>)column);
        }else if(DATATYPE_DATETIME.equals(this.dataType)){
            setDateTimeCellFactory((TableColumn<S, Date>)column);
        }else if(DATATYPE_STATICCODEDECODE.equals(this.dataType)){
            setCodeDecodeCellFactory((TableColumn<S, String>)column,this.tableColumnCodeDecodeField);
        }else if(DATATYPE_AMOUNT.equals(this.dataType)){
            setAmountCellFactory((TableColumn<S, Double>)column);
        }

        column.setPrefWidth(prefWidth);
        column.setSortable(sortable);
        column.setResizable(resizable);
        column.setVisible(visible);
        return column;
    }


    /**
     * Attach cell factory for the Amount
     *
     * @param columnAmount
     */
    private void setAmountCellFactory(TableColumn<S, Double> columnAmount){
        columnAmount.setCellFactory(column -> {
            TableCell<S, Double> cell = new TableCell<S, Double>() {
                @Override
                protected void updateItem(Double item, boolean empty) {
                    super.updateItem(item, empty);
                    if(empty) {
                        setText(null);
                    }
                    else {
                        if(item != null) {
                            setText(AMOUNT_CCY_FORMATTER.format(item.doubleValue()));
                        }else{
                            setText(null);
                        }
                    }
                }
            };

            return cell;
        });
    }


    public static String getNoOfPacketsUnit(String stockCode,String stockVarietyCode){
        if(GunnyBagQuantityField.GUNNY_BAGS_CODE.equals(stockCode)){
            return GunnyBagQuantityField.GUNNY_BAG_UNIT;
        }else if(HuskQuantityField.HUSK_CODE.equals(stockCode)){
            if(HuskQuantityField.HUSK_TRIP_CODE.equals(stockVarietyCode)){
                return HuskQuantityField.HUSK_TRIP_UNIT;
            }else {
                return HuskQuantityField.HUSK_BAG_UNIT;
            }
        }else {
            return HuskQuantityField.QUANTITY_PKT_UNIT;
        }
    }

    public static String getWeightmentUnit(String stockCode,String stockVarietyCode){
        if(GunnyBagQuantityField.GUNNY_BAGS_CODE.equals(stockCode)){
            return GunnyBagQuantityField.GUNNY_BAG_WEIGHTMENT_UNIT;
        }else if(HuskQuantityField.HUSK_CODE.equals(stockCode)){
            return HuskQuantityField.HUSK_WEIGHTMENT_UNIT;
        }else {
            return QuantityField.QUANTITY_WEIGHTMENT_UNIT;
        }
    }

    /**
     * Attach cell factory for the date.
     *
     * @param columnDate
     */
    private void setDateCellFactory(TableColumn<S, Date> columnDate){
        columnDate.setCellFactory(column -> {
            TableCell<S, Date> cell = new TableCell<S, Date>() {
                private SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT);

                @Override
                protected void updateItem(Date item, boolean empty) {
                    super.updateItem(item, empty);
                    if(empty) {
                        setText(null);
                    }
                    else {
                        setText(format.format(item));
                    }
                }
            };
            return cell;
        });
    }


    /**
     * Attach data time format to the table cell
     *
     * @param columnDateTime
     */
    private void setDateTimeCellFactory(TableColumn<S, Date> columnDateTime){
        columnDateTime.setCellFactory(column -> {
            TableCell<S, Date> cell = new TableCell<S, Date>() {
                private SimpleDateFormat format = new SimpleDateFormat(DATETIME_FORMAT);

                @Override
                protected void updateItem(Date item, boolean empty) {
                    super.updateItem(item, empty);
                    if(empty) {
                        setText(null);
                    }
                    else {
                        setText(format.format(item));
                    }
                }
            };
            return cell;
        });
    }

    /**
     * Code decode table column field
     *
     * @param columnCodeDecode
     * @param tableColumnCodeDecodeField
     */
    private void setCodeDecodeCellFactory(TableColumn<S, String> columnCodeDecode,TableColumnCodeDecodeField tableColumnCodeDecodeField){
        columnCodeDecode.setCellFactory(column -> {
            TableCell<S, String> cell = new TableCell<S, String>() {
                @Override
                protected void updateItem(String codeValue, boolean empty) {
                    super.updateItem(codeValue, empty);
                    TableRow<?> currentRow = getTableRow();
                    if(empty) {
                        setText(null);
                    }
                    else {
                        String description = tableColumnCodeDecodeField.reteriveCodeDescription(codeValue);
                        if("TOTL".equals(description)){
                            description = "Total";
                            currentRow.setStyle("-fx-font-weight: bold");
                        }else if("MLAMS".equals(description)){
                            description = "Lamp Total's";
                            //currentRow.setStyle("-fx-font-weight: bold");
                        }else{
                            currentRow.setStyle("");
                        }
                        setText(description);
                    }
                }
            };
            return cell;
        });
    }




}
