/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.configuration;

import java.util.List;
import java.util.Map;

public class GlobalProperties {
	
	private String defaultUser;
	private String defaultPasswordHash;
	private String defaultUserAccess;
	private Map<String,String> mailProperties;
	private List<String> fullFunctionList;
	private Map<String,List<String>> functionAccessMap;
	private boolean offline;
	private boolean embededService;


    public boolean isEmbededService() {
        return embededService;
    }

    public void setEmbededService(boolean embededService) {
        this.embededService = embededService;
    }

    public String getDefaultUser() {
		return defaultUser;
	}
	public void setDefaultUser(String defaultUser) {
		this.defaultUser = defaultUser;
	}
	public String getDefaultPasswordHash() {
		return defaultPasswordHash;
	}
	public void setDefaultPasswordHash(String defaultPasswordHash) {
		this.defaultPasswordHash = defaultPasswordHash;
	}
	public String getDefaultUserAccess() {
		return defaultUserAccess;
	}
	public void setDefaultUserAccess(String defaultUserAccess) {
		this.defaultUserAccess = defaultUserAccess;
	}
	
	public Map<String, String> getMailProperties() {
		return mailProperties;
	}
	public void setMailProperties(Map<String, String> mailProperties) {
		this.mailProperties = mailProperties;
	}
	
	public List<String> getFullFunctionList() {
		return fullFunctionList;
	}
	public void setFullFunctionList(List<String> fullFunctionList) {
		this.fullFunctionList = fullFunctionList;
	}
	
	
	public Map<String, List<String>> getFunctionAccessMap() {
		return functionAccessMap;
	}
	public void setFunctionAccessMap(Map<String, List<String>> functionAccessMap) {
		this.functionAccessMap = functionAccessMap;
	}

	public boolean isOffline() {
		return offline;
	}

	public void setOffline(boolean offline) {
		this.offline = offline;
	}

	@Override
	public String toString() {
		return "GlobalProperties [defaultUser=" + defaultUser + "]";
	}

}
