package com.desktopapp.fx.manager;

import com.desktopapp.fx.DesktopFxApplication;
import com.desktopapp.fx.common.StageViewBean;
import com.desktopapp.fx.control.MessageBox;
import com.desktopapp.fx.util.I18n;
import javafx.application.Platform;
import javafx.scene.control.ButtonType;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import com.stocktracker.domainobjs.json.PreferencesDO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import static javafx.scene.control.ButtonBar.ButtonData;

import java.util.Map;
import java.util.Optional;

/**
 * Created by diwakar009 on 13/12/2018.
 */
@Component
public class FXBootManager implements IBootManager{

    public static final long DEFAULT_PREFERENCE_ID = 1l;

    private final Logger logger = LoggerFactory.getLogger(FXBootManager.class);

    @Autowired
    private PreferenceManager preferenceManager;

    @Autowired
    private LoginManager loginManager;

    @Autowired
    private SplashManager splashManager;

    @Autowired
    private ApplicationViewManager applicationViewManager;


    public PreferenceManager getPreferenceManager() {
        return preferenceManager;
    }

    public void setPreferenceManager(PreferenceManager preferenceManager) {
        this.preferenceManager = preferenceManager;
    }

    public LoginManager getLoginManager() {
        return loginManager;
    }

    public void setLoginManager(LoginManager loginManager) {
        this.loginManager = loginManager;
    }

    public SplashManager getSplashManager() {
        return splashManager;
    }

    public void setSplashManager(SplashManager splashManager) {
        this.splashManager = splashManager;
    }

    public ApplicationViewManager getApplicationViewManager() {
        return applicationViewManager;
    }

    public void setApplicationViewManager(ApplicationViewManager applicationViewManager) {
        this.applicationViewManager = applicationViewManager;
    }

    public void startFXApplication(Stage initStage){
        getSplashManager().startSplash(initStage,this::showMainStage);
    }
    /**
     * Show the login view
     */
    private void showMainStage() {
        StageViewBean stageView = getLoginManager().showLoginView(this::onAppExit);
        DesktopFxApplication.getLoginSession().setStageView(stageView);
        stageView.getStage().show();
    }

    public void onAppExit(WindowEvent windowEvent) {
        if (DesktopFxApplication.getLoginSession().getPreferences().isShowMessageDialog()) {
            Optional<ButtonType> result = MessageBox.create()
                    .owner(DesktopFxApplication.get().getMainStage())
                    .contentText(I18n.COMMON.getString("confirm.appExit"))
                    .showExitConfirmation();
            if (result.isPresent() && result.get().getButtonData() == ButtonData.OK_DONE) {
                exitApplication();
            } else {
                if (windowEvent != null) {
                    windowEvent.consume();
                }
            }
        } else {
            exitApplication();
        }
    }

    private void exitApplication() {
        logger.info("exit application");
        Platform.exit();
    }

    @Override
    public void startApplication(Map<String, Object> appProperties) {
        readInitialPreferences(appProperties);
        Stage initialStage = (Stage)appProperties.get(INITIAL_STAGE);
        startFXApplication(initialStage);
    }

    @Override
    public void exitApplication(Map<String, Object> appProperties) {
        Object windowEvent = appProperties.get(IBootManager.WINDOW_EVENT);

        if(windowEvent == null){
            onAppExit(null);
        }else{
            onAppExit((WindowEvent) windowEvent);
        }

    }

    public void readInitialPreferences(Map<String, Object> appProperties){
        PreferencesDO preferences = getPreferenceManager().find(DEFAULT_PREFERENCE_ID);
		DesktopFxApplication.getLoginSession().setPreferences(preferences);
	}
}
