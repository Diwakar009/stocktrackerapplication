-- Stock Tracker Database Schema
-- Version 1.0.0
-- DDL Script

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

USE stocktrackerdb_uat;

DELETE FROM STATIC_CODE_DECODE;
DELETE FROM DOC_HOLDER;
DELETE FROM DOC_CONTENT;
DELETE FROM DOC_CONTENT_BLOB;
DELETE FROM APP_USER;
DELETE FROM APP_USER_LOGIN;
DELETE FROM app_preferences;
DELETE FROM TBL_INDUSTRY;
DELETE FROM TBL_STOCK;
DELETE FROM TBL_STOCK_DTLS;
DELETE FROM TBL_PARTY_DTLS;
DELETE FROM TBL_TRANSPORT_DTLS;
DELETE FROM TBL_DELIVERY_DTLS;
DELETE FROM TBL_CONTRACT;
DELETE FROM TBL_COSTPRICE_DTLS;

COMMIT;




