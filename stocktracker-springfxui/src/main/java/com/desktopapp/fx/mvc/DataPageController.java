/*
 * Copyright (C) 2017 Cem Ikta
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 *
 */

package com.desktopapp.fx.mvc;



import com.desktopapp.fx.common.PageDetails;
import com.desktopapp.service.AbstractService;
import com.stocktracker.domainobjs.json.ResultDO;
import javafx.scene.control.TableView;
import com.stocktracker.domainobjs.json.BaseDomainObject;
import com.stocktracker.domainobjs.json.DOPage;
import com.stocktracker.domainobjs.json.TableRowMapDO;

/**
 * Data page controller
 *
 * @param <T> entity
 *
 * @author Cem Ikta
 */
public interface DataPageController<T extends BaseDomainObject> extends Controller {

    /**
     * Gets service of this controller
     *
     * @return service of the controller
     */
    AbstractService<T> getService();

    /**
     * Gets data page view of the controller
     *
     * @return data page view
     */
    DataPageView<T> getDataPageView();

    /**
     * Gets data list for {@link TableView}
     *
     * @param filter
     * @param details
     * @return
     */
    DOPage<TableRowMapDO<String,String>> getData(String filter, PageDetails details);

    /**
     *
     * Load latest data.
     *
     * @param entity
     * @return
     *
     */
    T loadData(T entity);

    /**
     * Open form view at center
     *
     * @param entity The entity object for form to edit.
     */
    void openFormView(T entity);

    /**
     * Add new action of the controller
     */
    void onAddNew();


    /**
     * Edit action of the controller
     */
    void onEdit();

    /**
     * Delete action of the controller
     */
    void onDelete();

    /**
     * Refresh action of the controller
     */
    void onRefresh();

    /**
     * Print preview action of the controller
     */
    void onPrintPreview();

    /**
     * Print action of the controller
     */
    void onPrint();

    /**
     * Pdf export action of the controller
     */
    void onPdf();

    /**
     * Form print preview action of the controller
     *
     * @param id The id parameter for query
     */
    void onFormPrintPreview(Long id);

    /**
     * Form print action of the controller
     *
     * @param id The id parameter for query
     */
    void onFormPrint(Long id);

    /**
     * Form pdf action of the controller
     *
     * @param id The id parameter for query
     */
    void onFormPdf(Long id);

    /**
     * Save action of the controller
     *
     * @param entity to save
     */
    ResultDO onSave(T entity);

}