/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.desktopapp.fx.view.stockmaintenance;

import com.desktopapp.fx.DesktopFxApplication;
import com.desktopapp.fx.common.IGenericComponent;
import com.desktopapp.fx.common.Quantity;
import com.desktopapp.fx.control.*;
import com.desktopapp.fx.control.listeners.ValueChangeListener;
import com.desktopapp.fx.util.I18n;
import com.stocktracker.component.constants.CodeDecodeConstants;
import com.stocktracker.domainobjs.json.sms.CostPriceDetailsDO;
import com.stocktracker.domainobjs.json.sms.StockDetailsDO;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.*;
import org.controlsfx.validation.Validator;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by diwakar009 on 10/11/2019.
 */
public class StockDetailsMainForm extends StackPane {

    public final static ArrayList<String> PARTY_FORM_ENABLING_CODES = new ArrayList<String>(List.of("MDAJ","MDRE","PURC","SALE","RECV","TRAN","MDRT","RETN","MDEN","MDEP","PYIS","PYRE"));
    public final static ArrayList<String> TRANSPORT_FORM_ENABLING_CODES = new ArrayList<String>(List.of("MDRE","PURC","SALE","RECV","TRAN","MDRT","RETN"));
    public final static ArrayList<String> DELIVERY_FORM_ENABLING_CODES = new ArrayList<String>(List.of("KMDEL"));
    public final static List<String> MANDI_ENTRY_IN_QTL = new ArrayList<String>(List.of("MDEN"));
    public final static List<String> PURCHASE_SALE = new ArrayList<String>(List.of("PURC","SALE","MDEP","MDAJ"));
    public final static List<String> PAYMENT_RECV_ISSUE = new ArrayList<String>(List.of("PYRE","PYIS","PYDU"));

    List<IGenericComponent> components = new ArrayList<>();

    /**
     * Stock details controls
     */
    private DateTimeField dfTransactionDate;
    private CodeDecodeField cdIndustry;
    private CodeDecodeField cdStock;
    private CodeDecodeField cdStockVareity;
    private CodeDecodeField cdTransactionType;
    private QuantityField quantityField;
    private GunnyBagQuantityField qfGunnyBag;
    private HuskQuantityField qfHusk;
    private StackPane spQuantityPanel;
    private Label tlblQuantity;

    private BigDecimalField bfLampEntryInQtls;
    private BigDecimalField bfAppliedRate;
    private BigDecimalField bfDerivedAmount;
    private BigDecimalField bfActualAmount;

    private BigDecimalField bfLampEntryAdjInQtls;
    private BigDecimalField bfLampEntryAftAdjInQtls;


    private StockDetailsTransactionForm stockDetailsTransactionForm;

    public StockDetailsMainForm(StockDetailsTransactionForm stockDetailsTransactionForm) {
        this.stockDetailsTransactionForm=stockDetailsTransactionForm;
        build();
        registerComponents();
    }

    public void build(){
        this.cdIndustry = new CodeDecodeField(CodeDecodeConstants.CD_INDUSTRY_CODE,CodeDecodeConstants.DEFAULT_LANGUAGE);

        this.cdStock = new CodeDecodeField(CodeDecodeConstants.CD_STOCK_CODE,CodeDecodeConstants.DEFAULT_LANGUAGE);
        this.cdStockVareity = new CodeDecodeField(CodeDecodeConstants.CD_STOCK_VARIETY,CodeDecodeConstants.DEFAULT_LANGUAGE);
        this.cdTransactionType = new CodeDecodeField(CodeDecodeConstants.CD_TRANSACTION_TYPE,CodeDecodeConstants.DEFAULT_LANGUAGE);
        this.cdStock.registerLinkedCodeDecodeField(cdStockVareity);
        this.cdStock.registerLinkedCodeDecodeField(cdTransactionType);


        /**
         *
         *  Flip Quantity entries in case of gunny bags.
         *
         *  if gunny Bags is selected then Bags & Bell should be displayed
         *  other then gunny bags then Packets and Weightment should be displayed
         *
         */
        this.spQuantityPanel = new StackPane();

        this.quantityField = new QuantityField();
        this.qfGunnyBag = new GunnyBagQuantityField();
        this.qfHusk = new HuskQuantityField();

        this.tlblQuantity = new Label("Packets & WMT");

        ValueChangeListener vclCdStock = new ValueChangeListener() {
            @Override
            public void valueChanged() {
                manageQuantityFieldPanel();
                defaultValuesOnStockChange();
            }
        };

        ValueChangeListener vclCdStock1 = new ValueChangeListener() {
            @Override
            public void valueChanged() {
                manageQuantityFieldPanel();
                defaultDeliveryControls();

            }
        };

        this.cdStock.addValueChangeListener(vclCdStock);
        this.cdStockVareity.addValueChangeListener(vclCdStock1);

        this.spQuantityPanel.getChildren().add(quantityField.asControl());



        this.bfLampEntryInQtls = new BigDecimalField();
        this.bfLampEntryInQtls.setAsReadOnly(true);
        this.bfLampEntryInQtls.setPromptText(QuantityField.QUANTITY_WEIGHTMENT_UNIT);

        this.bfAppliedRate = new BigDecimalField(2);
        this.bfAppliedRate.setAsReadOnly(true);

        this.bfDerivedAmount = new BigDecimalField(2);
        this.bfDerivedAmount.setAsReadOnly(true);

        this.bfActualAmount = new BigDecimalField(2);
        this.bfActualAmount.setAsReadOnly(true);

        this.bfLampEntryAdjInQtls = new BigDecimalField(2);;
        this.bfLampEntryAdjInQtls.setAsReadOnly(true);
        this.bfLampEntryAdjInQtls.setPromptText(QuantityField.QUANTITY_WEIGHTMENT_UNIT);

        this.bfLampEntryAftAdjInQtls = new BigDecimalField(2);;
        this.bfLampEntryAftAdjInQtls.setAsReadOnly(true);
        this.bfLampEntryAftAdjInQtls.setPromptText(QuantityField.QUANTITY_WEIGHTMENT_UNIT);

       this.quantityField.getBfWeightment().addValueChangeListener(() -> {
           bfAppliedRate.clearDataValue();
           bfActualAmount.clearDataValue();
           bfDerivedAmount.clearDataValue();
       });

       this.qfHusk.getBfWeightment().addValueChangeListener(() -> {
           bfAppliedRate.clearDataValue();
           bfActualAmount.clearDataValue();
           bfDerivedAmount.clearDataValue();
       });

       this.qfGunnyBag.getBfWeightment().addValueChangeListener(() -> {
           bfAppliedRate.clearDataValue();
           bfActualAmount.clearDataValue();
           bfDerivedAmount.clearDataValue();
       });

        this.bfAppliedRate.addValueChangeListener(() -> {

              BigDecimal quantity = null;

              if(PURCHASE_SALE.contains(cdTransactionType.getDataValue())){

                  if("HUSK".equals(cdStock.getDataValue())){
                      quantity = this.qfHusk.getQuantity().getNumberOfPackets();
                  }else{
                      quantity = this.quantityField.getQuantity().getWeightment();
                  }
             }else if(MANDI_ENTRY_IN_QTL.contains(cdTransactionType.getDataValue())){
                  quantity = bfLampEntryInQtls.getValue();
             }

            if(quantity != null && bfAppliedRate.getValue() != null) {
                bfDerivedAmount.setDataValue(quantity.multiply(bfAppliedRate.getValue()));
                bfActualAmount.setDataValue(quantity.multiply(bfAppliedRate.getValue()));
            }else{
                bfDerivedAmount.setDataValue("");
                bfActualAmount.setDataValue("");
            }

        });

        ValueChangeListener vclCdTransactionType = new ValueChangeListener() {
            @Override
            public void valueChanged() {
                manageSectionsOnTransactionTypeChange(cdTransactionType.getDataValue());
                manageControlsOnTransactionTypeChange(cdTransactionType.getDataValue());

                defaultDeliveryControls();
            }
        };
        this.cdTransactionType.addValueChangeListener(vclCdTransactionType);

        this.dfTransactionDate = new DateTimeField(LocalDate.now());

        int columnIndex = 0;
        int rowIndex = 0;

        BorderPane borderPane = new BorderPane();
        Label tlblTransactionDtls = new Label();
        tlblTransactionDtls.setStyle("-fx-font-weight: bold");

        tlblTransactionDtls.setText("Transaction Details");
        HBox lblHBox = new HBox();
        lblHBox.getChildren().add(tlblTransactionDtls);
        lblHBox.setAlignment(Pos.CENTER);
        borderPane.setTop(lblHBox);

        GridPane formPane = new GridPane();
        formPane.setPadding(new Insets(10, 10, 10, 10));
        formPane.setHgap(10);
        formPane.setVgap(10);

        formPane.setGridLinesVisible(false);

        ColumnConstraints column1 = new ColumnConstraints();
        column1.setPercentWidth(50);

        ColumnConstraints column2 = new ColumnConstraints();
        column2.setPercentWidth(100);

        ColumnConstraints column3 = new ColumnConstraints();
        column3.setPercentWidth(2);

        formPane.getColumnConstraints().addAll(column1,column2,column3,column1,column2);

        formPane.add(new Label("Mill Name"), columnIndex, rowIndex);
        formPane.add(this.cdIndustry.asControl(), columnIndex + 1, rowIndex);

        this.stockDetailsTransactionForm.getValidationSupport().registerValidator(this.cdIndustry.getTfCodeValue(), true,
                Validator.createEmptyValidator(I18n.COMMON.getString("validation.required")));

        formPane.add(new Label("Transaction Date"), columnIndex+3, rowIndex);
        formPane.add(this.dfTransactionDate, columnIndex + 4, rowIndex);

        this.stockDetailsTransactionForm.getValidationSupport().registerValidator(this.dfTransactionDate, true,
                Validator.createEmptyValidator(I18n.COMMON.getString("validation.required")));


        formPane.add(new Label("Stock"), columnIndex, rowIndex+1);
        formPane.add(this.cdStock.asControl(), columnIndex + 1, rowIndex+1);

        this.stockDetailsTransactionForm.getValidationSupport().registerValidator(this.cdStock.getTfCodeValue(), true,
                Validator.createEmptyValidator(I18n.COMMON.getString("validation.required")));

        formPane.add(new Label("Stock Variety"), columnIndex+3, rowIndex+1);
        formPane.add(this.cdStockVareity.asControl(), columnIndex + 4, rowIndex+1);

        this.stockDetailsTransactionForm.getValidationSupport().registerValidator(this.cdStockVareity.getTfCodeValue(), true,
                Validator.createEmptyValidator(I18n.COMMON.getString("validation.required")));


        formPane.add(new Label("Transaction Type"), columnIndex, rowIndex+2);
        formPane.add(this.cdTransactionType.asControl(), columnIndex+1, rowIndex+2);

        this.stockDetailsTransactionForm.getValidationSupport().registerValidator(this.cdTransactionType.getTfCodeValue(), true,
                Validator.createEmptyValidator(I18n.COMMON.getString("validation.required")));

        formPane.add(tlblQuantity, columnIndex+3, rowIndex+2);
        formPane.add(spQuantityPanel, columnIndex + 4, rowIndex+2);

        formPane.add(new Label("Lamp Entry"),  columnIndex, rowIndex+3);
        formPane.add(this.bfLampEntryInQtls, columnIndex+1, rowIndex+3);

        formPane.add(new Label("Lamp Entry Adj"),columnIndex+3, rowIndex+3);
        formPane.add(this.bfLampEntryAdjInQtls, columnIndex + 4, rowIndex+3);

        formPane.add(new Label("Final Entry"),  columnIndex, rowIndex+4);
        formPane.add(this.bfLampEntryAftAdjInQtls, columnIndex+1, rowIndex+4);

        formPane.add(new Label("Rate / QTL"),columnIndex+3, rowIndex+4);
        formPane.add(this.bfAppliedRate, columnIndex + 4, rowIndex+4);

        formPane.add(new Label("Derived Amount"), columnIndex, rowIndex+5);
        formPane.add(this.bfDerivedAmount, columnIndex+1, rowIndex+5);

        formPane.add(new Label("Actual Amount"), columnIndex+3, rowIndex+5);
        formPane.add(this.bfActualAmount, columnIndex+4, rowIndex+5);

      /*  formPane.add(new Text("P Or M Amt"), columnIndex, rowIndex+5);
        formPane.add(this.cdPlusOrMinusOnAmtTotal.asControl(), columnIndex+1, rowIndex+5);*/

        VBox vbox = new VBox();
        HBox formPaneHBox = new HBox();
        formPaneHBox.getChildren().add(formPane);
        //formPaneHBox.setAlignment(Pos.CENTER);

        vbox.getChildren().add(formPaneHBox);

        borderPane.setCenter(vbox);

        getChildren().add(borderPane);

        setStyle("-fx-border-color: gray");

    }

    private void defaultDeliveryControls() {
        if("KMDEL".equals(cdTransactionType.getDataValue())) {
            if ("PPC".equals(cdStockVareity.getDataValue())) {
                stockDetailsTransactionForm.getStockDetailsDeliveryForm().getCdDeliveryType().setDataValue("PPCD");
            } else if ("FCI".equals(cdStockVareity.getDataValue())) {
                stockDetailsTransactionForm.getStockDetailsDeliveryForm().getCdDeliveryType().setDataValue("FCID");
            } else {
                stockDetailsTransactionForm.getStockDetailsDeliveryForm().getCdDeliveryType().clearDataValue();
            }
        }
    }

    private void defaultValuesOnStockChange() {

        switch (cdStock.getDataValue()){
            case "BRRI":
                cdStockVareity.setDataValue("SBR");

                break;
            case "PADD":
                cdStockVareity.setDataValue("FODPY");
                break;
            case "HUSK":
                cdStockVareity.setDataValue("TRIP");
                break;
            case "GUBA":
                cdStockVareity.setDataValue("GUBA");
                break;
            case "RICE":
                cdStockVareity.setDataValue("PPC");
                break;
            case "EXPE":
                cdStockVareity.setDataValue("EOTR");
                break;
            case "BRBR":
                cdStockVareity.setDataValue("BRBR");
                break;
            case "BRAN":
                cdStockVareity.setDataValue("BRAN");
                break;

        }

    }

    /**
     * Manage controls on transaction type change
     *
     * @param transactionType
     */
    private void manageControlsOnTransactionTypeChange(String transactionType){

        manageQuantityFieldsOnTransactionTypeChange(transactionType);

        bfAppliedRate.clearDataValue();
        bfActualAmount.clearDataValue();
        bfDerivedAmount.clearDataValue();

        if("PURC".equals(transactionType)
                || "SALE".equals(transactionType)
                || "MDAJ".equals(transactionType)
                || "MDEP".equals(transactionType)
                || PAYMENT_RECV_ISSUE.contains(transactionType)){

            if(PAYMENT_RECV_ISSUE.contains(transactionType)) {
                bfAppliedRate.setAsReadOnly(true);
            }else{
                bfAppliedRate.setAsReadOnly(false);
            }
            bfActualAmount.setAsReadOnly(false);
        }else{
            bfAppliedRate.setAsReadOnly(true);
            bfActualAmount.setAsReadOnly(true);
        }

        if(MANDI_ENTRY_IN_QTL.contains(transactionType)){
            bfLampEntryInQtls.setDisable(false);
        }else {
            bfLampEntryInQtls.setDisable(true);
            bfLampEntryInQtls.clearDataValue();
        }

        if("MDEN".equals(transactionType)) {

            bfLampEntryAdjInQtls.setDisable(false);
            bfLampEntryAftAdjInQtls.setDisable(false);

            bfLampEntryInQtls.addValueChangeListener(() -> {
                bfLampEntryAdjInQtls.setDataValue(new BigDecimal(0.0));
                bfLampEntryAftAdjInQtls.setDataValue(bfLampEntryInQtls.getDataValue());
            });

            bfLampEntryAdjInQtls.addValueChangeListener(() -> {
                BigDecimal adjustmentEntry = bfLampEntryAdjInQtls.getValue();
                BigDecimal lampEntry = bfLampEntryInQtls.getValue();
                if(lampEntry != null && adjustmentEntry != null) {
                    BigDecimal finalEntry = lampEntry.add(lampEntry.multiply(adjustmentEntry).setScale(2, RoundingMode.DOWN));
                    bfLampEntryAftAdjInQtls.setDataValue(finalEntry);
                }
            });

        }else{
            bfLampEntryAdjInQtls.setDisable(true);
            bfLampEntryAdjInQtls.clearDataValue();

            bfLampEntryAftAdjInQtls.setDisable(true);
            bfLampEntryAftAdjInQtls.clearDataValue();
        }

    }

    private void manageQuantityFieldsOnTransactionTypeChange(String transactionType){

        if(/*"MDAJ".equals(transactionType) ||*/ "MDEN".equals(transactionType) || PAYMENT_RECV_ISSUE.contains(transactionType)){
            /*quantityField.setAsReadOnly(true);
            quantityField.setDataValue(new Quantity(BigDecimal.ZERO,BigDecimal.ZERO));*/
            if(quantityField != null) {
                quantityField.setAsReadOnly(true);
                quantityField.setDataValue(new Quantity(BigDecimal.ZERO,BigDecimal.ZERO));
            }

            if(qfGunnyBag != null) {
                qfGunnyBag.setAsReadOnly(true);
                qfGunnyBag.setDataValue(new Quantity(BigDecimal.ZERO,BigDecimal.ZERO));
            }

            if(qfHusk != null) {
                qfHusk.setAsReadOnly(true);
                qfHusk.setDataValue(new Quantity(BigDecimal.ZERO,BigDecimal.ZERO));
            }
        }else{

            if (quantityField != null) {
                quantityField.setDataValue(new Quantity(BigDecimal.ZERO, BigDecimal.ZERO));
                quantityField.setAsReadOnly(false);
                if("KMDEL".equals(transactionType)){
                    quantityField.getIfNoOfPackets().setDataValue(580);
                }

                if("MDAJ".equals(transactionType)){
                    quantityField.getIfNoOfPackets().setAsReadOnly(true);
                }else{
                    quantityField.getIfNoOfPackets().setAsReadOnly(false);
                }
            }

            if(qfGunnyBag != null) {
                qfGunnyBag.setAsReadOnly(false);
                qfGunnyBag.setDataValue(new Quantity(BigDecimal.ZERO,BigDecimal.ZERO));
            }

            if(qfHusk != null) {
                qfHusk.setAsReadOnly(false);
                qfHusk.setDataValue(new Quantity(BigDecimal.ZERO,BigDecimal.ZERO));
            }

        }

    }


    /**
     * Manage the sections for enabling and disabling.
     *
     *
     */
    private void manageSectionsOnTransactionTypeChange(String transactionType){

        this.stockDetailsTransactionForm.getStockDetailsPartyForm().clearAll();
        this.stockDetailsTransactionForm.getStockDetailsTransportForm().clearAll();
        this.stockDetailsTransactionForm.getStockDetailsDeliveryForm().clearAll();
        this.stockDetailsTransactionForm.getStockDetailsExtraForm().clearAll();

        if(PARTY_FORM_ENABLING_CODES.contains(transactionType) ){
            this.stockDetailsTransactionForm.getStockDetailsPartyForm().setDisable(false);
        }else {
            this.stockDetailsTransactionForm.getStockDetailsPartyForm().setDisable(true);
        }

        if(TRANSPORT_FORM_ENABLING_CODES.contains(transactionType) ){
            this.stockDetailsTransactionForm.getStockDetailsTransportForm().setDisable(false);
        }else {
            this.stockDetailsTransactionForm.getStockDetailsTransportForm().setDisable(true);
        }

        if(DELIVERY_FORM_ENABLING_CODES.contains(transactionType) ){
            this.stockDetailsTransactionForm.getStockDetailsDeliveryForm().setDisable(false);
        }else {
            this.stockDetailsTransactionForm.getStockDetailsDeliveryForm().setDisable(true);
        }

    }


    /**
     * Manage Quantity Field
     *
     */
    private void manageQuantityFieldPanel(){

        if(GunnyBagQuantityField.GUNNY_BAGS_CODE.equals(cdStock.getDataValue())
                || GunnyBagQuantityField.GUNNY_BAGS_CODE.equals(cdStockVareity.getDataValue())){

            tlblQuantity.setText("Bags & Bail");
            qfGunnyBag.clearDataValue();
            spQuantityPanel.getChildren().clear();
            spQuantityPanel.getChildren().add(qfGunnyBag.asControl());
            qfGunnyBag.getIfNoOfPackets().setDataValue(500);

        }else if(HuskQuantityField.HUSK_CODE.equals(cdStock.getDataValue())){

            String stockVarietyCode = cdStockVareity.getDataValue();
            if(HuskQuantityField.HUSK_TRIP_CODE.equals(stockVarietyCode)){
                qfHusk.setTripIndicator(true);
                tlblQuantity.setText("Trip & WMT");
            }else{
                qfHusk.setTripIndicator(false);
                tlblQuantity.setText("Packets & WMT");
            }
            qfHusk.clearDataValue();
            spQuantityPanel.getChildren().clear();
            spQuantityPanel.getChildren().add(qfHusk.asControl());

        }else{

            tlblQuantity.setText("Packets & WMT");
            quantityField.clearDataValue();
            spQuantityPanel.getChildren().clear();
            spQuantityPanel.getChildren().add(quantityField.asControl());

        }
    }




    /**
     * Get Generic Components
     *
     * @return
     */
    private List<IGenericComponent> registerComponents(){

        components.add(dfTransactionDate);
        components.add(cdIndustry);
        components.add(cdStock);
        components.add(cdStockVareity);
        components.add(cdTransactionType);
        components.add(quantityField);
        components.add(qfGunnyBag);
        components.add(qfHusk);
        components.add(bfAppliedRate);
        components.add(bfDerivedAmount);
        components.add(bfActualAmount);
        components.add(bfLampEntryInQtls);
        components.add(bfLampEntryAdjInQtls);
        components.add(bfLampEntryAftAdjInQtls);
        return components;
    }

    /**
     * Clear all
     */
    public void clearAll(){
        components.forEach(component -> component.clearDataValue());
    }

    /**
     * Set DO to Controls
     *
     * @param stockDetailsDO
     */
    public void setDOToControls(StockDetailsDO stockDetailsDO){

        if(stockDetailsDO != null) {

            this.dfTransactionDate.setDataValue(stockDetailsDO.getTransactionDate());
            this.cdIndustry.setDataValue(stockDetailsDO.getIndustryCode());
            this.cdStock.setDataValue(stockDetailsDO.getStockCode());
            this.cdStockVareity.setDataValue(stockDetailsDO.getStockVarietyCode());
            this.cdTransactionType.setDataValue(stockDetailsDO.getTransactionType());

            Long noOfPackets = stockDetailsDO.getNoOfPackets();
            Double weightment = stockDetailsDO.getTotalWeightment();

            Quantity quantity = new Quantity();
            if(noOfPackets != null){
                quantity.setNumberOfPackets(BigDecimal.valueOf(noOfPackets));
            }
            if(weightment != null){
                quantity.setWeightment(BigDecimal.valueOf(weightment));
            }

            if(GunnyBagQuantityField.GUNNY_BAGS_CODE.equals(stockDetailsDO.getStockCode())
                    || GunnyBagQuantityField.GUNNY_BAGS_CODE.equals(stockDetailsDO.getStockVarietyCode())) {
                this.qfGunnyBag.setDataValue(quantity);
            }else if (HuskQuantityField.HUSK_CODE.equals(cdStock.getDataValue())
                    || HuskQuantityField.HUSK_TRIP_CODE.equals(stockDetailsDO.getStockVarietyCode())
                    || HuskQuantityField.HUSK_BAG_CODE.equals(stockDetailsDO.getStockVarietyCode())){
                this.qfHusk.setDataValue(quantity);

                if(stockDetailsDO.getCostPriceDetailsDO() != null && stockDetailsDO.getCostPriceDetailsDO().getActualAmount() != null){
                    bfActualAmount.setDataValue(stockDetailsDO.getCostPriceDetailsDO().getActualAmount());
                }

            }else{
                this.quantityField.setDataValue(quantity);
            }

            if (stockDetailsDO.getLampEntryInQtl() != null) {
                bfLampEntryInQtls.setDataValue(stockDetailsDO.getLampEntryInQtl());
                if(stockDetailsDO.getLampEntryAdjInQtl() != null) {
                    bfLampEntryAdjInQtls.setDataValue(stockDetailsDO.getLampEntryAdjInQtl());
                    bfLampEntryAftAdjInQtls.setDataValue(stockDetailsDO.getLampEntryAfterAdjInQtl());
                }
            }

            if(stockDetailsDO.getCostPriceDetailsDO() != null){
                if(stockDetailsDO.getCostPriceDetailsDO().getAppliedRate() != null) {
                    bfAppliedRate.setDataValue(new BigDecimal(stockDetailsDO.getCostPriceDetailsDO().getAppliedRate()));
                }
                if(stockDetailsDO.getCostPriceDetailsDO().getDerivedAmount() != null) {
                    bfDerivedAmount.setDataValue(new BigDecimal(stockDetailsDO.getCostPriceDetailsDO().getDerivedAmount()));
                }
                if(stockDetailsDO.getCostPriceDetailsDO().getActualAmount() != null) {
                    bfActualAmount.setDataValue(new BigDecimal(stockDetailsDO.getCostPriceDetailsDO().getActualAmount()));
                }
            }




        }

    }

    /**
     * Set controls to DO
     *
     * @param stockDetailsDO
     */
    public void setControlsToDO(StockDetailsDO stockDetailsDO){

        if(stockDetailsDO != null) {

            try {
                Date transactionDate = this.dfTransactionDate.getDataValueAsDate();
                stockDetailsDO.setTransactionDate(transactionDate);
            }catch(Exception e){
                e.printStackTrace();
            }

            stockDetailsDO.setIndustryCode(this.cdIndustry.getDataValue());
            stockDetailsDO.setStockCode(this.cdStock.getDataValue());
            stockDetailsDO.setStockVarietyCode(this.cdStockVareity.getDataValue());
            stockDetailsDO.setTransactionType(this.cdTransactionType.getDataValue());

            if(GunnyBagQuantityField.GUNNY_BAGS_CODE.equals(this.cdStock.getDataValue())
                    || GunnyBagQuantityField.GUNNY_BAGS_CODE.equals(this.cdStockVareity.getDataValue())) {

                if(this.qfGunnyBag.getQuantity().getNumberOfPackets() != null && this.qfGunnyBag.getQuantity().getWeightment() != null) {
                    stockDetailsDO.setNoOfPackets(this.qfGunnyBag.getQuantity().getNumberOfPackets().longValue());
                    stockDetailsDO.setTotalWeightment(this.qfGunnyBag.getQuantity().getWeightment().doubleValue());
                }

            }else if (HuskQuantityField.HUSK_CODE.equals(cdStock.getDataValue())
                    || HuskQuantityField.HUSK_TRIP_CODE.equals(stockDetailsDO.getStockVarietyCode())
                    || HuskQuantityField.HUSK_BAG_CODE.equals(stockDetailsDO.getStockVarietyCode())) {
                if(this.qfHusk.getQuantity().getNumberOfPackets() != null && this.qfHusk.getQuantity().getWeightment() != null) {
                    stockDetailsDO.setNoOfPackets(this.qfHusk.getQuantity().getNumberOfPackets().longValue());
                    stockDetailsDO.setTotalWeightment(this.qfHusk.getQuantity().getWeightment().doubleValue());
                }

            }else{
                if(this.quantityField.getQuantity().getNumberOfPackets() != null && this.quantityField.getQuantity().getWeightment() != null) {
                    stockDetailsDO.setNoOfPackets(this.quantityField.getQuantity().getNumberOfPackets().longValue());
                    stockDetailsDO.setTotalWeightment(this.quantityField.getQuantity().getWeightment().doubleValue());
                }
            }

            if(this.bfLampEntryInQtls.getValue() != null) {
                stockDetailsDO.setLampEntryInQtl(this.bfLampEntryInQtls.getValue().doubleValue());
            }else{
                stockDetailsDO.setLampEntryInQtl(null);
            }

            if(this.bfLampEntryAdjInQtls.getValue() != null) {
                stockDetailsDO.setLampEntryAdjInQtl(this.bfLampEntryAdjInQtls.getValue().doubleValue());
            }else{
                stockDetailsDO.setLampEntryAdjInQtl(null);
            }

            if(this.bfLampEntryAftAdjInQtls.getValue() != null) {
                stockDetailsDO.setLampEntryAfterAdjInQtl(this.bfLampEntryAftAdjInQtls.getValue().doubleValue());
            }else{
                stockDetailsDO.setLampEntryAfterAdjInQtl(null);
            }

            CostPriceDetailsDO costPriceDetailsDO = new CostPriceDetailsDO();

            if(this.bfAppliedRate.getValue() != null){
                costPriceDetailsDO.setAppliedRate(bfAppliedRate.getValue().doubleValue());
            }else {
                costPriceDetailsDO.setAppliedRate(null);
            }

            if(this.bfDerivedAmount.getValue() != null){
                costPriceDetailsDO.setDerivedAmount(bfDerivedAmount.getValue().doubleValue());
            }else {
                costPriceDetailsDO.setDerivedAmount(null);
            }

            if(this.bfActualAmount.getValue() != null){
                costPriceDetailsDO.setActualAmount(bfActualAmount.getValue().doubleValue());
            }else {
                costPriceDetailsDO.setActualAmount(null);
            }
            stockDetailsDO.setCostPriceDetailsDO(costPriceDetailsDO);


            if (stockDetailsDO.isNew()) {
                stockDetailsDO.setCreatedBy(DesktopFxApplication.getLoginSession().getUserName());
            } else {
                stockDetailsDO.setUpdatedBy(DesktopFxApplication.getLoginSession().getUserName());
            }

        }
    }

    public CodeDecodeField getCdStock() {
        return cdStock;
    }

    public DateTimeField getDfTransactionDate() {
        return dfTransactionDate;
    }

    public CodeDecodeField getCdIndustry() {
        return cdIndustry;
    }

    public CodeDecodeField getCdTransactionType() {
        return cdTransactionType;
    }

    public CodeDecodeField getCdStockVareity() {
        return cdStockVareity;
    }

    public BigDecimalField getBfAppliedRate() {
        return bfAppliedRate;
    }

    public void setBfAppliedRate(BigDecimalField bfAppliedRate) {
        this.bfAppliedRate = bfAppliedRate;
    }

    public BigDecimalField getBfDerivedAmount() {
        return bfDerivedAmount;
    }

    public void setBfDerivedAmount(BigDecimalField bfDerivedAmount) {
        this.bfDerivedAmount = bfDerivedAmount;
    }

    public BigDecimalField getBfActualAmount() {
        return bfActualAmount;
    }

    public void setBfActualAmount(BigDecimalField bfActualAmount) {
        this.bfActualAmount = bfActualAmount;
    }

    public List<IGenericComponent> getComponents() {
        return components;
    }

    public void setComponents(List<IGenericComponent> components) {
        this.components = components;
    }
}
