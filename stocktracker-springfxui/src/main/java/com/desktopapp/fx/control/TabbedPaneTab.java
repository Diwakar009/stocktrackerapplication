/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.desktopapp.fx.control;

import javafx.scene.Node;
import javafx.scene.control.Tab;

/**
 * Tabbed tab
 *
 * @author R Diwakar C
 */
public class TabbedPaneTab extends Tab {

    private static final String DEFAULT_STYLE_CLASS = "tabbed-pane-tab";

    public TabbedPaneTab(String title) {
        this(title, null);
    }

    public TabbedPaneTab(String text, Node content) {
        super(text, content);
        getStyleClass().add(DEFAULT_STYLE_CLASS);
        setClosable(false);
    }

}
