/*
 * Copyright (c) 2020.
 *
 * All Rights are with Diwakar C
 */

package com.desktopapp.service.reports;

import com.desktopapp.service.StaticCodeDecodeService;
import com.desktopapp.service.config.AppConfig;
import com.desktopapp.exceptions.InvalidTemplateException;
import com.desktopapp.exceptions.NoTemplateFoundException;
import com.desktopapp.service.stockmaintenance.StockDetailsService;
import com.stocktracker.domainobjs.json.CodeDataItemDO;
import com.stocktracker.domainobjs.json.sms.MandiReconcilationDataDO;
import com.stocktracker.exceptions.BusinessException;
import com.stocktracker.exceptions.TechnicalException;
import com.stocktracker.jpa.spring.extended.config.ExtendedRepositoryConfig;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.*;
import java.util.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { AppConfig.class, ExtendedRepositoryConfig.class})
public class ExcelReportServiceTest {

    @Autowired
    private ExcelReportService excelReportService;

    @InjectMocks
    private ExcelReportService excelReportServiceMock;

    @Mock
    private StockDetailsService stockDetailsServiceMock;

    @Mock
    private StaticCodeDecodeService staticCodeDecodeServiceMock;


    @Before
    public void setup(){

        List<MandiReconcilationDataDO> mandiReconcilationDataDOList = new ArrayList<>();
        MandiReconcilationDataDO mandiReconcilationDataDO = new MandiReconcilationDataDO();
        mandiReconcilationDataDO.setIndustryCode("KMR");
        mandiReconcilationDataDO.setOnBehalf("KMR");
        mandiReconcilationDataDO.setStockCode("PADD");
        mandiReconcilationDataDO.setTransactionType("MDRV");
        mandiReconcilationDataDO.setLampNameCode("LA1");
        mandiReconcilationDataDO.setMandiNameCode("MA1");
        mandiReconcilationDataDO.setPartyName("Party Name 1");
        mandiReconcilationDataDO.setTotalReceived(10.0);
        mandiReconcilationDataDO.setTotalAdjusted(20.0);
        mandiReconcilationDataDO.setTotalReceivedNAdjustedNPurchased(30.0);
        mandiReconcilationDataDO.setTotalEntered(100.0);
        mandiReconcilationDataDO.setLampEntryAdjustment(0.03);
        mandiReconcilationDataDO.setTotalLampEntryAfterAdjustment(86.0);
        mandiReconcilationDataDO.setDifference(40.0);
        mandiReconcilationDataDO.setVehicleNumber("OR10 J 7477");
        mandiReconcilationDataDO.setRstNumber("1234");
        mandiReconcilationDataDOList.add(mandiReconcilationDataDO);

        mandiReconcilationDataDO = new MandiReconcilationDataDO();
        mandiReconcilationDataDO.setIndustryCode("KMR");
        mandiReconcilationDataDO.setOnBehalf("KMR");
        mandiReconcilationDataDO.setStockCode("PADD");
        mandiReconcilationDataDO.setTransactionType("MDAJ");
        mandiReconcilationDataDO.setLampNameCode("LA2");
        mandiReconcilationDataDO.setMandiNameCode("MA2");
        mandiReconcilationDataDO.setPartyName("Party Name 2");
        mandiReconcilationDataDO.setTotalAdjusted(20.0);
        mandiReconcilationDataDO.setTotalReceivedNAdjustedNPurchased(30.0);
        mandiReconcilationDataDO.setTotalEntered(100.0);
        mandiReconcilationDataDO.setLampEntryAdjustment(0.03);
        mandiReconcilationDataDO.setTotalLampEntryAfterAdjustment(86.0);
        mandiReconcilationDataDO.setDifference(40.0);
        mandiReconcilationDataDO.setVehicleNumber("OR10 J 7477");
        mandiReconcilationDataDO.setRstNumber("1234");
        mandiReconcilationDataDOList.add(mandiReconcilationDataDO);

        Mockito.when(stockDetailsServiceMock.getMandiReconciliationData("KMR",null,null))
                .thenReturn(mandiReconcilationDataDOList);

        Mockito.when(stockDetailsServiceMock.getMandiReconciliationData("KMR","LA1",null))
                .thenReturn(mandiReconcilationDataDOList);

        // test on null check

        List<MandiReconcilationDataDO> multiIndustryReconList = new ArrayList<>();

        mandiReconcilationDataDO = new MandiReconcilationDataDO();
        mandiReconcilationDataDO.setIndustryCode("SMR");
        mandiReconcilationDataDO.setOnBehalf("SMR");
        mandiReconcilationDataDO.setStockCode("PADD");
        mandiReconcilationDataDO.setTransactionType("MDRV");
        mandiReconcilationDataDO.setLampNameCode("LA1");
        mandiReconcilationDataDO.setMandiNameCode("MA1");
        mandiReconcilationDataDO.setPartyName("Party Name 1");
        mandiReconcilationDataDO.setTotalReceived(10.0);
        mandiReconcilationDataDO.setTotalAdjusted(20.0);
        mandiReconcilationDataDO.setTotalReceivedNAdjustedNPurchased(30.0);
        mandiReconcilationDataDO.setTotalEntered(100.0);
        mandiReconcilationDataDO.setLampEntryAdjustment(0.03);
        mandiReconcilationDataDO.setTotalLampEntryAfterAdjustment(86.0);
        mandiReconcilationDataDO.setDifference(40.0);
        mandiReconcilationDataDO.setTransactionDate(new Date());
        mandiReconcilationDataDO.setVehicleNumber("OR10 J 7477");
        mandiReconcilationDataDO.setRstNumber("1234");
        multiIndustryReconList.add(mandiReconcilationDataDO);

        mandiReconcilationDataDO = new MandiReconcilationDataDO();
        mandiReconcilationDataDO.setIndustryCode("SMR");
        mandiReconcilationDataDO.setOnBehalf("SMR");
        mandiReconcilationDataDO.setStockCode("PADD");
        mandiReconcilationDataDO.setTransactionType("MDAJ");
        mandiReconcilationDataDO.setLampNameCode("LA2");
        mandiReconcilationDataDO.setMandiNameCode("MA2");
        mandiReconcilationDataDO.setPartyName("Party Name 2");
        mandiReconcilationDataDO.setTotalAdjusted(20.0);
        mandiReconcilationDataDO.setTotalReceivedNAdjustedNPurchased(30.0);
        mandiReconcilationDataDO.setTotalEntered(100.0);
        mandiReconcilationDataDO.setLampEntryAdjustment(0.03);
        mandiReconcilationDataDO.setTotalLampEntryAfterAdjustment(86.0);
        mandiReconcilationDataDO.setDifference(40.0);
        mandiReconcilationDataDO.setTransactionDate(new Date());
        mandiReconcilationDataDO.setVehicleNumber("OR10 J 7477");
        mandiReconcilationDataDO.setRstNumber("1234");
        multiIndustryReconList.add(mandiReconcilationDataDO);

        Mockito.when(stockDetailsServiceMock.getMandiReconciliationData("SMR",null,null))
                .thenReturn(multiIndustryReconList);

        Mockito.when(staticCodeDecodeServiceMock.getCodeValueDescription(ExcelReportService.CD_INDUSTRY_CODE,"SMR",ExcelReportService.DEFAULT_LANGUAGE))
                .thenReturn(Optional.of("Sriya Modern Rice Mill"));

        Mockito.when(staticCodeDecodeServiceMock.getCodeValueDescription(ExcelReportService.CD_INDUSTRY_CODE,"KMR",ExcelReportService.DEFAULT_LANGUAGE))
                .thenReturn(Optional.of("Krishna Modern Rice Mill"));

        Mockito.when(staticCodeDecodeServiceMock.getCodeValueDescription(ExcelReportService.CD_MANDI_NAME,"MA1",ExcelReportService.DEFAULT_LANGUAGE))
                .thenReturn(Optional.of("Mandi Name 1"));

        Mockito.when(staticCodeDecodeServiceMock.getCodeValueDescription(ExcelReportService.CD_MANDI_NAME,"MA2",ExcelReportService.DEFAULT_LANGUAGE))
                .thenReturn(Optional.of("Mandi Name 2"));

        Mockito.when(staticCodeDecodeServiceMock.getCodeValueDescription(ExcelReportService.CD_LAMP_CODE,"LA1",ExcelReportService.DEFAULT_LANGUAGE))
                .thenReturn(Optional.of("Lamp Name 1"));

        Mockito.when(staticCodeDecodeServiceMock.getCodeValueDescription(ExcelReportService.CD_LAMP_CODE,"LA2",ExcelReportService.DEFAULT_LANGUAGE))
                .thenReturn(Optional.of("Lamp Name 2"));

        List<CodeDataItemDO> codeDataItemDOS = new ArrayList<>();
        CodeDataItemDO codeDataItemDO = new CodeDataItemDO();
        codeDataItemDO.setCodeName(ExcelReportService.CD_INDUSTRY_CODE);
        codeDataItemDO.setCodeValue("KMR");
        codeDataItemDO.setCodeDescription("Krishna Modern Rice Mill");
        codeDataItemDO.setLanguage("EN_US");
        codeDataItemDOS.add(codeDataItemDO);

        codeDataItemDO = new CodeDataItemDO();
        codeDataItemDO.setCodeName(ExcelReportService.CD_INDUSTRY_CODE);
        codeDataItemDO.setCodeValue("SMR");
        codeDataItemDO.setCodeDescription("Sriya Modern Rice Mill");
        codeDataItemDO.setLanguage("EN_US");
        codeDataItemDOS.add(codeDataItemDO);

        Mockito.when(staticCodeDecodeServiceMock.getCodeDataItemList(ExcelReportService.CD_INDUSTRY_CODE, ExcelReportService.DEFAULT_LANGUAGE, true))
                .thenReturn(Optional.of(codeDataItemDOS));


        List<CodeDataItemDO> codeDataItemDOS1 = new ArrayList<>();
        CodeDataItemDO codeDataItemDO1 = new CodeDataItemDO();
        codeDataItemDO1.setCodeName(ExcelReportService.CD_INDUSTRY_CODE);
        codeDataItemDO1.setCodeValue("KMR");
        codeDataItemDO1.setCodeDescription("Krishna Modern Rice Mill");
        codeDataItemDO1.setLanguage("EN_US");
        codeDataItemDOS1.add(codeDataItemDO1);
        Mockito.when(staticCodeDecodeServiceMock.getCodeDataItemList(ExcelReportService.CD_INDUSTRY_CODE,"KMR", ExcelReportService.DEFAULT_LANGUAGE, true))
                .thenReturn(Optional.of(codeDataItemDOS1));


    }

    @Test
    public void testAutowireExcelReportGenerator(){
        Assert.assertEquals(true, excelReportService != null);
    }

    @Test
    public void testReadExcelTemplate() throws NoTemplateFoundException, InvalidTemplateException {
        String templateFIleName = "../templates/MandiReconReportTemplate.xls";
        Workbook workbook = excelReportService.readExcelTemplate(templateFIleName);
        Assert.assertEquals(true,workbook != null);
    }

    @Test (expected = NoTemplateFoundException.class)
    public void testReadExcelTemplate_Nofile() throws NoTemplateFoundException, InvalidTemplateException {
        String templateFIleName = "../templates/MandiReconReportTemplate_Dummy.xls";
        Workbook workbook = excelReportService.readExcelTemplate(templateFIleName);
    }

    @Test (expected = InvalidTemplateException.class)
    public void testReadExcelTemplate_InvalidFile() throws NoTemplateFoundException, InvalidTemplateException {
        String templateFIleName = "../templates/MandiReconReportTemplate.txt";
        Workbook workbook = excelReportService.readExcelTemplate(templateFIleName);
    }

    @Test
    public void testDisplayTextFromTemplate() throws NoTemplateFoundException, InvalidTemplateException {
        String templateFIleName = "../templates/MandiReconReportTemplate.xls";
        Workbook workbook = excelReportService.readExcelTemplate(templateFIleName);
        Sheet sheet = workbook.getSheet("Template");
        Row row = sheet.getRow(0);
        Assert.assertEquals("Mandi Recon Report:",row.getCell(0).getStringCellValue());
    }

    @Test
    public void testReteriveMandiReconReport() throws BusinessException, TechnicalException {
        OutputStream report = excelReportServiceMock.reteriveMandiReconReport("KMR");
        File file = new File("MandiReconReport.xls");
        OutputStream os  = null;
        try {
            os = new FileOutputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        // Starts writing the bytes in it
        try {
            os.write(((ByteArrayOutputStream)report).toByteArray());
            os.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Assert.assertEquals(true,report != null);
    }

    @Test
    public void testReteriveMandiReconReport_NullParameter() throws BusinessException, TechnicalException {
        OutputStream report = excelReportServiceMock.reteriveMandiReconReport(null);
        File file = new File("MandiReconReport.xls");
        OutputStream os  = null;
        try {
            os = new FileOutputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        // Starts writing the bytes in it
        try {
            os.write(((ByteArrayOutputStream)report).toByteArray());
            os.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testReteriveMandiReconReport_LampParameter() throws BusinessException, TechnicalException {
        OutputStream report = excelReportServiceMock.reteriveMandiReconReport("KMR","LA1",null);
        File file = new File("MandiReconReport.xls");
        OutputStream os  = null;
        try {
            os = new FileOutputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        // Starts writing the bytes in it
        try {
            os.write(((ByteArrayOutputStream)report).toByteArray());
            os.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    @Test
    public void testReteriveMandiReconReport_NullParameterNonMock() throws BusinessException, TechnicalException {
        OutputStream report = excelReportService.reteriveMandiReconReport(null);
        File file = new File("MandiReconReport.xls");
        OutputStream os  = null;
        try {
            os = new FileOutputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        // Starts writing the bytes in it
        try {
            os.write(((ByteArrayOutputStream)report).toByteArray());
            os.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Test
    public void testIndustryOutturnReport() throws TechnicalException, BusinessException {
        Map<String, Object> parameterMap = new HashMap<>();
        OutputStream exportedExcel = excelReportService.reteriveIndustryOutturnReport(null);
        File file = new File("IndustryOutturnReport.xls");
        OutputStream os  = null;
        try {
            os = new FileOutputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        // Starts writing the bytes in it
        try {
            os.write(((ByteArrayOutputStream)exportedExcel).toByteArray());
            os.close();
        } catch (IOException e) {
            e.printStackTrace();
        }




    }

    @Test
    public void testExportStockDetailsTransactions() throws TechnicalException, BusinessException {
        Map<String, Object> parameterMap = new HashMap<>();
        OutputStream exportedExcel = excelReportService.reteriveStockDetailTransactionsReport(parameterMap);
        File file = new File("StockTransactionDetails.xls");
        OutputStream os  = null;
        try {
            os = new FileOutputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        // Starts writing the bytes in it
        try {
            os.write(((ByteArrayOutputStream)exportedExcel).toByteArray());
            os.close();
        } catch (IOException e) {
            e.printStackTrace();
        }




    }



}
