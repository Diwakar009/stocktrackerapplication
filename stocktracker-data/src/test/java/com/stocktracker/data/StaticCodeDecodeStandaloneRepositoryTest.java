package com.stocktracker.data;

import com.stocktracker.jpa.spring.extended.StaticCodeDecodeRepository;
import com.stocktracker.jpa.spring.extended.config.ExtendedRepositoryConfig;
import com.stocktracker.model.StaticCodeDecode;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

/**
 * Created by diwakar009 on 17/12/2018.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = { ExtendedRepositoryConfig.class })
public class StaticCodeDecodeStandaloneRepositoryTest {

    @Resource
    private StaticCodeDecodeRepository staticCodeDecodeRepository;

    @Before
    public void setup() {

    }

    @Test
    public void findUniqueAll(){
        Page<StaticCodeDecode> staticCodeDecodes = staticCodeDecodeRepository.findUniqueAll(PageRequest.of(1, 10));
        for (StaticCodeDecode staticCodeDecode:staticCodeDecodes){
            System.out.println(staticCodeDecode.toString());
        }
    }


    @Test
    public void findUniqueAllByFilter(){
        Page<StaticCodeDecode> staticCodeDecodes = staticCodeDecodeRepository.findUniqueAllByFilter("%Admin%",PageRequest.of(0, 10));
        for(StaticCodeDecode codeDecode : staticCodeDecodes){
            Assert.assertEquals(codeDecode.getCodeName(),"CD_ACCESSLEVEL");
            Assert.assertEquals(codeDecode.getLanguage(),"EN_US");
        }
    }
    @Test
    public void listByCodeNameNLangNFilter(){
        Page<StaticCodeDecode> staticCodeDecodes = staticCodeDecodeRepository.listByCodeNameNLangNFilter("CD_CURRENCY","EN_US","%",PageRequest.of(0,Integer.MAX_VALUE));
        for(StaticCodeDecode codeDecode : staticCodeDecodes){
            System.out.println(codeDecode.getCodeDesc());
        }
    }

}
