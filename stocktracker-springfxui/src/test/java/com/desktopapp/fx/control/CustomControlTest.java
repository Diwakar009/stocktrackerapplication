package com.desktopapp.fx.control;

import com.desktopapp.fx.objectfactory.ObjectFactory;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Control;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import com.stocktracker.domainobjs.json.CodeDataItemDO;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by diwakar009 on 3/12/2018.
 */
/*@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {AppConfigTest.class,JPAConfiguration.class})*/

@SpringBootTest
@ComponentScan({"com.desktopapp.fx.controller","com.desktopapp.service","com.stocktracker.jpa.spring.extended"})
public class CustomControlTest extends Application {

    public CustomControlTest() {
        super();
    }

    @Override
    public void init() throws Exception {
        super.init();
    }

    @Override
    public void stop() throws Exception {
        super.stop();
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        //testCodeDecodeField(primaryStage);
        //testCodeDecodeFieldGrid(primaryStage);
        //testCodeDecodeServiceCall(primaryStage);
        //testCodeDecodeSetDataValue(primaryStage);
        //testCodeDecodeLinkedCombo(primaryStage);
        testCustomTextFeild(primaryStage);
    }

    public void testCodeDecodeSetDataValue(Stage primaryStage){
        CodeDecodeField codeDecodeField1 = new CodeDecodeField("CD_CURRENCY","EN_US");
        CodeDecodeField codeDecodeField2 = new CodeDecodeField("CD_DOCTYPE","EN_US");

        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));
        grid.add(codeDecodeField1.asControl(),0,0);
        grid.add(codeDecodeField2.asControl(),0,1);

        codeDecodeField1.setDataValue("GBP");

        Scene scene = new Scene( grid, 300, 275);


        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public void testCustomTextFeild(Stage primaryStage){

        TextFieldExt textField = new TextFieldExt();

        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));
        grid.add(textField,0,0);

        Scene scene = new Scene( grid, 300, 275);
        primaryStage.setScene(scene);
        primaryStage.show();
    }


    public void testCodeDecodeLinkedCombo(Stage primaryStage){

        CodeDecodeField codeDecodeField1 = new CodeDecodeField("CD_RICEMILLSTOCK","EN_US");
        CodeDecodeField codeDecodeField2 = new CodeDecodeField("CD_STOCKVARIETY","EN_US");
        codeDecodeField1.registerLinkedCodeDecodeField(codeDecodeField2);

        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));
        grid.add(codeDecodeField1.asControl(),0,0);
        grid.add(codeDecodeField2.asControl(),0,1);

        Scene scene = new Scene( grid, 300, 275);
        primaryStage.setScene(scene);
        primaryStage.show();
    }



    public void testCodeDecodeServiceCall(Stage primaryStage){
        List<Control> controls = new ArrayList<Control>();


        /*ComboBoxExt<CodeDataItemDO> combo1 = new ComboBoxExt<CodeDataItemDO>() ;
        ComboBoxExt<CodeDataItemDO> combo2 = new ComboBoxExt<CodeDataItemDO>() ;

        TextFieldExt textFieldExt1 = new TextFieldExt(5);
        TextFieldExt textFieldExt2 = new TextFieldExt(5);


        CodeDecodeField codeDecodeField1 = new CodeDecodeField(textFieldExt1,combo1,"CD_CURRENCY","EN_US");
        CodeDecodeField codeDecodeField2 = new CodeDecodeField(textFieldExt2,combo2,"CD_DOCTYPE","EN_US");*/

        CodeDecodeField codeDecodeField1 = new CodeDecodeField("CD_CURRENCY","EN_US");
        CodeDecodeField codeDecodeField2 = new CodeDecodeField("CD_DOCTYPE","EN_US");

        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));
        grid.add(codeDecodeField1.asControl(),0,0);
        grid.add(codeDecodeField2.asControl(),0,1);

        Scene scene = new Scene( grid, 300, 275);
        primaryStage.setScene(scene);
        primaryStage.show();

    }

    public void displayScene(Stage primaryStage, List<Control> controls){
        primaryStage.setTitle("Testing of controls...");
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));
        final AtomicInteger rowCount = new AtomicInteger(0);

        controls.forEach(control -> {
            int rownum = rowCount.incrementAndGet();
            grid.add(new Label("Control " + rownum),0,rownum);
            grid.add(control,1,rownum);
        });
        Scene scene = new Scene(grid, 300, 275);
        scene.getStylesheets().add(getClass().getResource("/styles/light-theme.css").toExternalForm());

        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public void testCodeDecodeFieldGrid(Stage primaryStage){
        List<Control> controls = new ArrayList<Control>();
        ArrayList<CodeDataItemDO> comboData = new ArrayList<CodeDataItemDO>() {{
            add(new CodeDataItemDO("NAME","Diw", "", "EN_US", "Diwakar"));
            add(new CodeDataItemDO("NAME","Diwak", "", "EN_US", "Diwa"));
            add(new CodeDataItemDO("NAME","Deb", "", "EN_US", "Debasis"));
            add(new CodeDataItemDO("NAME","Gir", "", "EN_US", "Girish"));
            add(new CodeDataItemDO("NAME","Che", "", "EN_US", "Chetana"));
            add(new CodeDataItemDO("NAME","Rash", "", "EN_US", "Rashmi"));
            add(new CodeDataItemDO("NAME","Medh", "", "EN_US", "Medhansh"));
        }};

        ArrayList<CodeDataItemDO> comboData1 = new ArrayList<CodeDataItemDO>() {{
            add(new CodeDataItemDO("NAME","", "", "EN_US", ""));
            add(new CodeDataItemDO("NAME","AAA", "", "EN_US", "AAAAAAAAAAAAAAAAAAAAAAA"));
            add(new CodeDataItemDO("NAME","BBB", "", "EN_US", "BBBBBBBBBBBBBBBBBBBB"));
            add(new CodeDataItemDO("NAME","CCC", "", "EN_US", "CCCCCCCCCCCCCCCCCCCCC"));
            add(new CodeDataItemDO("NAME","DDD", "", "EN_US", "DDDDDDDDDDDDDDDDDDD"));
            add(new CodeDataItemDO("NAME","EEE", "", "EN_US", "EEEEEEEEEE"));
            add(new CodeDataItemDO("NAME","FFFFF", "", "EN_US", "FFFF"));
            add(new CodeDataItemDO("NAME","GGGGG", "", "EN_US", "GGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGG"));
        }};



        ComboBoxExt<CodeDataItemDO> combo1 = new ComboBoxExt<CodeDataItemDO>(comboData) ;
        ComboBoxExt<CodeDataItemDO> combo2 = new ComboBoxExt<CodeDataItemDO>(comboData1) ;

        TextFieldExt textFieldExt1 = new TextFieldExt(5);
        TextFieldExt textFieldExt2 = new TextFieldExt(5);


        CodeDecodeField codeDecodeField1 = new CodeDecodeField(textFieldExt1,combo1);
        CodeDecodeField codeDecodeField2 = new CodeDecodeField(textFieldExt2,combo2);


        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));
        grid.add(codeDecodeField1.asControl(),0,0);
        grid.add(codeDecodeField2.asControl(),0,1);

        Scene scene = new Scene( grid, 300, 275);
        scene.getStylesheets().add(getClass().getResource("/styles/light-theme.css").toExternalForm());
        primaryStage.setScene(scene);
        primaryStage.show();

    }

    public void testCodeDecodeField(Stage primaryStage){
        List<Control> controls = new ArrayList<Control>();
        ArrayList<CodeDataItemDO> comboData = new ArrayList<CodeDataItemDO>() {{
            add(new CodeDataItemDO("NAME","Diw", "", "EN_US", "Diwakar"));
            add(new CodeDataItemDO("NAME","Diwa", "", "EN_US", "Diwa"));
            add(new CodeDataItemDO("NAME","Deb", "", "EN_US", "Debasis"));
            add(new CodeDataItemDO("NAME","Gir", "", "EN_US", "Girish"));
            add(new CodeDataItemDO("NAME","Che", "", "EN_US", "Chetana"));
            add(new CodeDataItemDO("NAME","Rash", "", "EN_US", "Rashmi"));
            add(new CodeDataItemDO("NAME","Medh", "", "EN_US", "Medhansh"));
        }};


        ComboBoxExt<CodeDataItemDO> combo1 = new ComboBoxExt<CodeDataItemDO>(comboData) ;
        TextFieldExt textFieldExt = new TextFieldExt(3);
        CodeDecodeField codeDecodeField = new CodeDecodeField(textFieldExt,combo1);
        controls.add(combo1);
        controls.add(textFieldExt);

        displayScene(primaryStage,controls);
    }

    // @Test
    public void launchFx(){

        launch(null);
    }

    public static void main(String args[])
    {
        ApplicationContext context = SpringApplication.run(CodeDecodeFieldTest.class);
        ObjectFactory.setContext(context);
        launch(null);
    }


}
