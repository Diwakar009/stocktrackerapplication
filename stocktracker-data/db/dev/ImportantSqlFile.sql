select fn_codedecode_desc('CD_INDUSTRY_CODE','EN_US', result.MillName) AS 'Mill Name',
       fn_codedecode_desc('CD_STOCK_CODE','EN_US', result.StockCode) AS 'Stock Name',
       fn_codedecode_desc('CD_STOCK_VARIETY','EN_US', result.StockVarietyCode) AS 'Stock Variety',
       fn_codedecode_desc('CD_LAMP_CODE','EN_US', result.LampName) AS 'Lamp Name',
       fn_codedecode_desc('CD_MANDI_NAME','EN_US', result.MandiName) AS 'Mandi Name',
       result.PartyName as 'PartyName',
       result.Total_Received,
       result.Total_Adjusted,
       ROUND(result.Total_Received + result.Total_Adjusted,2) as 'Recv And Adjusted',
       result.Total_Entried,
       (ROUND(result.Total_Received + result.Total_Adjusted,2) - result.Total_Entried) as 'Difference',
       result.OnBehalf as 'OnBehalf'
from (

(select stockdetai0_.INDUSTRY_CODE as 'MillName',stockdetai0_.stock_code as 'StockCode',stockdetai0_.STOCK_VARIETIES_CODE as 'StockVarietyCode',stockdetai0_.LAMP_NAME_CODE as 'LampName',stockdetai0_.MANDI_NAME_CODE as 'MandiName' ,stockdetai0_.TRANSACTION_TYPE as 'transactionType',partydetai2_.PARTY_NAME_1 as 'PartyName',
ROUND(sum(concat(stockdetai0_.PLUS_OR_MINUS_ON_TOTAL,stockdetai0_.TOTAL_WEIGHTMENT)),2) as 'Total_Received',ROUND(sum(stockdetai0_.LAMP_ENTRY_IN_QTL),2) as 'Total_Entried',ROUND(sum(IF(TRANSACTION_TYPE = 'MDAJ', stockdetai0_.LAMP_ENTRY_IN_QTL, 0)),2) as 'Total_Adjusted',
stockdetai0_.ONBEHALF as 'OnBehalf'

from
        TBL_STOCK_DTLS stockdetai0_
    left outer join
        TBL_TRANSPORT_DTLS transportd1_
            on stockdetai0_.TRANSPORT_ID=transportd1_.ID
    left outer join
        TBL_PARTY_DTLS partydetai2_
            on stockdetai0_.PARTY_ID=partydetai2_.ID
where TRANSACTION_TYPE in ('MDRE','MDAJ','MDRT','MDEN')
and OnBehalf = 'KMR'
group by  LampName,MandiName,PARTY_NAME_1 with rollup) as result

);


DELIMITER $$

CREATE FUNCTION fn_codedecode_desc(
codename varchar(50),
lang varchar(5),
codevalue varchar(50)
)
RETURNS VARCHAR(1000)
DETERMINISTIC
BEGIN
DECLARE description varchar(1000);

select CODE_DESC into description from static_code_decode
where CODE_NAME = codename and
language = lang and
code_value = codevalue LIMIT 0,1;

RETURN (description);

END$$
DELIMITER ;
/