/*
 * Copyright (c) 2020.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.jpa.spring.extended.constants;

public class NativeQueryConstants {
    // Mandi Report Query Start
    public static final String MANDI_RECONCILIATION_QUERY_1 =
            new StringBuilder(NativeQueryConstants.MANDI_RECONCILIATION_OUTER_QUERY_1)
            .append("")
            .append(NativeQueryConstants.MANDI_RECONCILIATION_INNER_QUERY_1)
            .append("").toString();

    public static final String MANDI_RECONCILIATION_OUTER_QUERY_1 =
            "SELECT result.industry_code,result.stock_code,result.stock_variety_code,result.transaction_type," +
                    "result.lamp_name_code,result.mandi_name_code,result.party_name_1,result.id," +
                    "result.tot_received,result.tot_adjusted," +
                    "ROUND((result.tot_received + result.tot_adjusted) - result.tot_excess_purchase,2) as 'tot_recv_and_adj_minus_pur'," +
                    "result.tot_entered," +
                    "result.lamp_entry_adj," +
                    "result.tot_lamp_entry_aft_adj," +
                    "(ifnull(result.tot_lamp_entry_aft_adj,0.00) - ROUND((result.tot_received + result.tot_adjusted) - result.tot_excess_purchase,2)) as difference," +
                    "result.OnBehalf as onbehalf, result.transaction_date as transaction_date," +
                    "if(result.id is not null, result.vehicle_no,'') as vehicle_no,if(result.id is not null, result.rst_no,'') as rst_no,result.tot_excess_purchase FROM ";

    //'MDRE','MDAJ','MDRT','MDEN'
    public static final String MANDI_RECONCILIATION_INNER_QUERY_1 =
            "(SELECT sd.INDUSTRY_CODE as industry_code,sd.STOCK_CODE as stock_code,sd.STOCK_VARIETIES_CODE as stock_variety_code,sd.TRANSACTION_TYPE as transaction_type, " +
            "sd.LAMP_NAME_CODE as lamp_name_code,sd.MANDI_NAME_CODE as mandi_name_code,pd.PARTY_NAME_1 as party_name_1,sd.ID as id," +
            "td.VEHICLE_NUMBER as vehicle_no, td.rst_number as  rst_no," +
            /*"ROUND(SUM(concat(sd.PLUS_OR_MINUS_ON_TOTAL,sd.TOTAL_WEIGHTMENT)),2) as tot_received, " +*/
            "ROUND(SUM(IF(sd.TRANSACTION_TYPE = 'MDRE', sd.TOTAL_WEIGHTMENT, 0)),2) as tot_received, " +
            "ROUND(SUM(sd.LAMP_ENTRY_IN_QTL),2) as tot_entered, " +
            "ROUND(SUM(IF(sd.TRANSACTION_TYPE = 'MDAJ', sd.TOTAL_WEIGHTMENT, 0)),2) as tot_adjusted, " +
            "ROUND(SUM(IF(sd.TRANSACTION_TYPE = 'MDEP', sd.TOTAL_WEIGHTMENT, 0)),2) as tot_excess_purchase, " +
            "sd.LAMP_ENTRY_ADJ_IN_QTL as lamp_entry_adj, " +
            "ROUND(SUM(sd.LAMP_ENTRY_AFT_ADJ_IN_QTL),2) as tot_lamp_entry_aft_adj, " +
            "sd.ONBEHALF as onbehalf, " +
            "sd.TRANSACTION_DATE as transaction_date " +
            "FROM TBL_STOCK_DTLS sd " +
            "LEFT OUTER JOIN TBL_PARTY_DTLS pd ON sd.PARTY_ID = pd.ID " +
            "LEFT OUTER JOIN TBL_TRANSPORT_DTLS td ON sd.TRANSPORT_ID = td.ID " +
            "WHERE sd.STOCK_CODE = 'PADD' and sd.TRANSACTION_TYPE IN (?1) " +
            "AND sd.ONBEHALF = ?2 " +
            "AND sd.LAMP_NAME_CODE like ?3 " +
            "AND sd.MANDI_NAME_CODE like ?4 " +
            "GROUP BY sd.ONBEHALF,sd.LAMP_NAME_CODE,sd.MANDI_NAME_CODE,pd.PARTY_NAME_1,sd.transaction_type,sd.id WITH ROLLUP) as result";
    // Mandi Report Query End
}



