package com.desktopapp.fx.control;

import com.desktopapp.fx.common.IGenericComponent;
import com.desktopapp.fx.common.StringUtils;
import com.desktopapp.fx.control.listeners.ValueChangeListener;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.event.Event;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Window;
import javafx.util.Callback;
import com.stocktracker.domainobjs.IComboBoxAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

/**
 * Created by diwakar009 on 1/12/2018.
 */
public class ComboBoxExt<T> extends ComboBox<T> implements IGenericComponent {

    public static final double COMBOBOX_POPUP_HEIGHT = 100;

    private List<ValueChangeListener> valueChangeListeners;

    private FilteredComboBoxDecorator filteredComboBoxDecorator;

    public ComboBoxExt() {
        valueChangeListeners = new ArrayList<ValueChangeListener>();
    }


    public ComboBoxExt(ObservableList<T> items) {
        super(items);
        valueChangeListeners = new ArrayList<ValueChangeListener>();
        filteredComboBoxDecorator = new FilteredComboBoxDecorator(this,FilteredComboBoxDecorator.CONTAINS_CHARS_IN_ORDER);
        registerListener();
    }

    public ComboBoxExt(ObservableList<T> items,FilteredComboBoxDecorator.AutoCompleteComparator<T> autoCompleteComparator) {
        super(FXCollections.observableList(items));
        valueChangeListeners = new ArrayList<ValueChangeListener>();
        filteredComboBoxDecorator = new FilteredComboBoxDecorator(this,autoCompleteComparator);
        registerListener();
    }

    public ComboBoxExt(ArrayList<T> items,FilteredComboBoxDecorator.AutoCompleteComparator<T> autoCompleteComparator) {
       super(FXCollections.observableList(items));
       valueChangeListeners = new ArrayList<ValueChangeListener>();
       filteredComboBoxDecorator = new FilteredComboBoxDecorator(this,autoCompleteComparator);
       registerListener();
    }

    public ComboBoxExt(ArrayList<T> items) {
        super(FXCollections.observableList(items));
        valueChangeListeners = new ArrayList<ValueChangeListener>();
        filteredComboBoxDecorator = new FilteredComboBoxDecorator(this,FilteredComboBoxDecorator.CONTAINS_CHARS_IN_ORDER);
        registerListener();
    }

    public void registerListener(){

        getSelectionModel().selectedItemProperty().addListener( (options, oldValue, newValue) -> {
            fireListeners();
        });

       setCellFactory(new Callback<ListView<T>, ListCell<T>>() {
            @Override public ListCell<T> call(ListView<T> p) {
                return new ListCell<T>() {
                    @Override
                    protected void updateItem(T item, boolean empty) {
                        super.updateItem(item, empty);
                        setPrefWidth(getPrefWidth() - 15);
                        if (item == null || empty) {
                            setGraphic(null);
                        } else {
                            if(item !=null) {
                                if(item instanceof IComboBoxAdapter){
                                    setText(((IComboBoxAdapter)item).getSelectedItemText(item));
                                }else{
                                    setText(item.toString());
                                }
                            }else{
                                setText(null);
                            }
                             //setText(item.getCodeDescription());
                        }
                    }
                };
            }
        });


        hoverProperty().addListener((observable, oldValue, newValue) -> {
            if(newValue){
                T t = getSelectionModel().getSelectedItem();
                if(t !=null) {
                    if(t instanceof IComboBoxAdapter){
                        getTooltip().setText(((IComboBoxAdapter)t).getSelectedItemText(t));
                    }else{
                        getTooltip().setText(t.toString());
                    }
                 }
            }else{
                if(getTooltip().isShowing()){
                   getTooltip().hide();
                }
            }
        });

    }

    public void addItems(ObservableList<T> items){
        addItems(items,false);
    }

    public void addItems(ObservableList<T> items,boolean override){

        if(override) {
            valueProperty().set(null);
            setItems(items);
        }else {
            getItems().addAll(items);
        }

        getFilteredComboBoxDecorator().refreshComboItems();

        getSelectionModel().selectedItemProperty().addListener( (options, oldValue, newValue) -> {
            fireListeners();
        });
    }

    public void fireListeners(){
        valueChangeListeners.forEach(listener -> {listener.valueChanged();});
    }

    public void addValueChangeListener(ValueChangeListener listener){
        valueChangeListeners.add(listener);
    }

    @Override
    public void setAsReadOnly(boolean readOnly) {
        //setEditable(!readOnly);
        setDisable(readOnly);
    }

    @Override
    public void clearDataValue() {
        this.getSelectionModel().clearSelection();
    }

    @Override
    public boolean isDataExchangeApplicable() {
        return false;
    }

    @Override
    public void destroyComponent() {
        // TODO Auto-generated method stub

    }

    @Override
    public boolean isReadonly() {
        return isDisabled();
    }

    public void showTooltip() {
        if (!getTooltip().isShowing()) {
            Window stage = getScene().getWindow();
            double posX = stage.getX() + localToScene(getBoundsInLocal()).getMinX();
            double posY = stage.getY() + localToScene(getBoundsInLocal()).getMinY();
            getTooltip().show(stage, posX, posY);
        }
    }


    public List<ValueChangeListener> getValueChangeListeners() {
        return valueChangeListeners;
    }

    public void setValueChangeListeners(List<ValueChangeListener> valueChangeListeners) {
        this.valueChangeListeners = valueChangeListeners;
    }

    public FilteredComboBoxDecorator getFilteredComboBoxDecorator() {
        return filteredComboBoxDecorator;
    }

    public void setFilteredComboBoxDecorator(FilteredComboBoxDecorator filteredComboBoxDecorator) {
        this.filteredComboBoxDecorator = filteredComboBoxDecorator;
    }

    @Override
    public boolean isComponentShowing() {
        return isVisible();
    }
}

/**
 * Filtered combobox
 *
 * @param <T>
 */
class FilteredComboBoxDecorator<T>  {

    private final Logger log = LoggerFactory.getLogger(getClass());
    private static final String EMPTY = "";
    private StringProperty filter = new SimpleStringProperty(EMPTY);
    private AutoCompleteComparator<T> comparator = (typedText, objectToCompare) -> false;
    private volatile FilteredList<T> filteredItems;
    private final ComboBoxExt<T> comboBox;
    private ObservableList<T> originalComboData;
    private T selectedItem = null;
    private boolean skipFilterChanged = false;

    public FilteredComboBoxDecorator(final ComboBoxExt<T> comboBox,
                                     AutoCompleteComparator<T> comparator) {
        this.comboBox = comboBox;
        this.comparator = comparator;

        originalComboData = comboBox.getItems();
        filteredItems = new FilteredList<>(comboBox.getItems());
        this.comboBox.setItems(filteredItems);

        Tooltip tooltip = new Tooltip();
        tooltip.getStyleClass().add("tooltip-combobox");
        this.comboBox.setTooltip(tooltip);
        filter.addListener((observable, oldValue, newValue) -> handleFilterChanged(oldValue,newValue));
        this.comboBox.setOnKeyPressed(this::handleOnKeyPressed);
        //comboBox.addEventFilter(KeyEvent.KEY_RELEASED,this::handleOnKeyPressed);
        this.comboBox.setOnHidden(this::handleOnHiding);

        this.comboBox.itemsProperty().addListener((obs, oldV, newV) -> {
            if (newV != filteredItems) {
                if (!(newV instanceof FilteredList)) {
                    filteredItems = new FilteredList<>(newV);
                }else {
                    filteredItems = (FilteredList<T>) newV;
                }

                this.comboBox.setItems(filteredItems);
            }
        });
    }

    /*private void displayFilteredItems(FilteredList<T> tFilteredList){
        System.out.println("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&");
        tFilteredList.forEach(fl -> {
            System.out.print(fl.toString());
        });
        System.out.println("#########################################");
    }*/

    private void handleFilterChanged(String oldValue,String newValue) {

        if(!skipFilterChanged) {

            if (!oldValue.equals(newValue)) {

                if (filteredItems != null) {
                    Predicate<T> p = filter.get().isEmpty() ? null :
                            s -> this.comparator.matches(filter.get(), s);
                    filteredItems.setPredicate(p);
                }

                if (!StringUtils.isBlank(newValue)) {
                    comboBox.show();

                    if (StringUtils.isBlank(filter.get())) {
                        restoreOriginalItems();
                    } else {
                        comboBox.showTooltip();
                        SingleSelectionModel<T> selectionModel = comboBox.getSelectionModel();
                        if (filteredItems.isEmpty()) {
                            selectionModel.clearSelection();
                            comboBox.fireListeners();
                            selectedItem = null;
                            restoreOriginalItems();
                        } else {
                            selectionModel.select(0);
                            selectedItem = selectionModel.getSelectedItem();
                            //selectionModel.select(selectedItem);
                        }
                    }
                } else {
                    comboBox.getTooltip().hide();
                    restoreOriginalItems();
                }
            }
        }

        if(skipFilterChanged){
            skipFilterChanged=false;
            //comboBox.setItems(originalComboData);
            filteredItems = new FilteredList<>(originalComboData);
            comboBox.setItems(filteredItems);
        }
    }

    private void handleOnHiding(Event e) {
        skipFilterChanged = true;
        filter.setValue(EMPTY);
        comboBox.getTooltip().setText("");
        comboBox.getTooltip().hide();
        restoreOriginalItems();

    }

    private void handleOnKeyPressed(KeyEvent keyEvent) {
        KeyCode code = keyEvent.getCode();

        if (!keyEvent.isMetaDown()) {
            String filterValue = filter.get();
            //log.debug("Handling KeyCode = " + code);
            if (code.isLetterKey() || code.isDigitKey() || code == KeyCode.MINUS) {
                filterValue += keyEvent.getText();
            }else if ((code == KeyCode.BACK_SPACE) && (filterValue.length() > 0)) {
                filterValue = filterValue.substring(0, filterValue.length() - 1);
            } else if (code == KeyCode.ESCAPE) {
                filterValue = EMPTY;
            } else if ((code == KeyCode.DOWN) || (code == KeyCode.UP)) {
               comboBox.show();
            }

            filter.set(filterValue);
            comboBox.getTooltip().textProperty().set(filterValue);
        }
    }

    public void setComparator(AutoCompleteComparator<T> comparator) {
        this.comparator = comparator;
        handleFilterChanged("",filter.get());
    }

    public void refreshComboItems(){
        if(comboBox != null) {
            this.originalComboData = comboBox.getItems();
            filteredItems = new FilteredList<>(comboBox.getItems());
            this.comboBox.setItems(filteredItems);
        }
    }

    private void restoreOriginalItems() {
        T s = comboBox.getSelectionModel().getSelectedItem();
        if(s != null) {
            comboBox.getSelectionModel().select(s);
        }else{
            comboBox.getSelectionModel().select(selectedItem);
        }
      /*  if(comboBox.isShowing())
            comboBox.hide();*/
    }




    /**
     *
     * @param <T>
     */
    public interface AutoCompleteComparator<T> {
        boolean matches(String typedText, T objectToCompare);
    }

    public static AutoCompleteComparator<String> STARTSWITH =
            (txt, obj) -> obj.toUpperCase()
                    .startsWith(txt.toUpperCase());

    public static AutoCompleteComparator<String> STARTSWITH_IGNORE_SPACES =
            (txt, obj) -> obj.replace(" ", "")
                    .toUpperCase()
                    .startsWith(txt.replace(" ", "").toUpperCase());

    public static AutoCompleteComparator<String> CONTAINS_CHARS_IN_ORDER =
            (txt, obj) -> StringUtils.containsOrderedChars(txt.toUpperCase(), obj.toUpperCase());

    public static AutoCompleteComparator<String> STARTSWITH_FIRST_THEN_CONTAINS_CHARS_IN_ORDER =
            (txt, obj) -> obj.substring(0, 1).equalsIgnoreCase(txt.substring(0, 1)) &&
                    StringUtils.containsOrderedChars(txt.toUpperCase(), obj.toUpperCase());


}

