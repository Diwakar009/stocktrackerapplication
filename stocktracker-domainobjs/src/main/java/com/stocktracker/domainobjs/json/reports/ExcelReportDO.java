/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.domainobjs.json.reports;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.stocktracker.domainobjs.IDomainObject;
import com.stocktracker.domainobjs.json.BaseDomainObject;
import com.stocktracker.model.BaseEntity;
import com.stocktracker.model.Preferences;

import java.io.Serializable;
import java.util.Date;

/**
 * Excel Report Domain Object
 *
 */

public class ExcelReportDO extends BaseDomainObject implements Serializable {
    private static final long serialVersionUID = 1L;

    public static String CLASS_NAME = ExcelReportDO.class.getName();

    private byte[] reportContent;

    public byte[] getReportContent() {
        return reportContent;
    }

    public void setReportContent(byte[] reportContent) {
        this.reportContent = reportContent;
    }

    @Override
    public Object getDomainObjectKey() {
        return null;
    }
}