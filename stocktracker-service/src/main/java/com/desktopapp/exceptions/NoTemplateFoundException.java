/*
 * Copyright (c) 2020.
 *
 * All Rights are with Diwakar C
 */

package com.desktopapp.exceptions;

import com.stocktracker.exceptions.BusinessException;

public class NoTemplateFoundException extends BusinessException {

    public NoTemplateFoundException(String s) {
        super(s);
    }

    @Override
    public String toString() {
        return "NoTemplateFoundException{}";
    }
}
