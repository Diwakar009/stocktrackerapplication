package com.desktopapp.fx.control;

import com.desktopapp.fx.DesktopFxApplication;
import com.desktopapp.fx.app.AppFontIcons;
import com.desktopapp.fx.common.ITableDataViewBehaviour;
import com.desktopapp.fx.common.PageDetails;
import com.desktopapp.fx.control.fonticon.FontIconFactory;
import com.desktopapp.fx.util.I18n;
import com.desktopapp.fx.util.ViewHelpers;
import com.stocktracker.domainobjs.json.sms.StockDetailsDO;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.concurrent.Worker;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.input.*;
import javafx.scene.layout.*;
import com.stocktracker.domainobjs.json.DOPage;
import com.stocktracker.domainobjs.json.TableRowMapDO;
import javafx.scene.paint.Color;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * Created by diwakar009 on 24/9/2019.
 */
public class TableDataView<T> extends BorderPane{

    private final Logger logger = LoggerFactory.getLogger(TableDataView.class);

    private SearchField searchField;
    private ProgressIndicator progressIndicator;
    private TableView tableView;
    private Pager pager;

    protected ActionCommand btnAddNew;
    protected ActionCommand btnEdit;
    protected ActionCommand btnDelete;

    protected ActionCommand btnPrintPreview;
    protected ActionCommand btnPrint;
    protected ActionCommand btnPdf;

    protected ActionCommand btnExportToExcel;

    private ITableDataViewBehaviour<T> behaviour;

    public TableDataView(ITableDataViewBehaviour behaviour) {
        super();
        this.behaviour = behaviour;
        init();
    }

    public TableDataView(ITableDataViewBehaviour behaviour,Double prefHeight) {
        super();
        this.behaviour = behaviour;
        init();
    }

    public void init(){
        buildView();
    }

    public void buildView(){
        getStyleClass().addAll("data-page-view");
        setTop(getHeaderBar());
        setCenter(getTablePane());
    }

    private HBox getHeaderBar() {
        logger.debug("get data page header");
        HBox headerBar = new HBox();
        headerBar.getStyleClass().add("data-page-header");

        Label title = ViewHelpers.createFontIconLabel(behaviour.getTitle(), behaviour.getFontIcon(), "");
        title.getStyleClass().add("page-title");

        progressIndicator = new ProgressIndicator();
        progressIndicator.setMaxSize(50, 50);

        Region spacer = new Region();
        HBox.setHgrow(spacer, Priority.ALWAYS);

        // actions
        btnAddNew = new ActionCommand("", FontIconFactory.createIcon(AppFontIcons.ADD_NEW));
        btnAddNew.setTooltip(new Tooltip(I18n.COMMON.getString("action.addNew")));
        btnAddNew.getStyleClass().add("left-pill");
        btnAddNew.setOnAction(event -> behaviour.onAddNew());

        btnEdit = new ActionCommand("", FontIconFactory.createIcon(AppFontIcons.EDIT));
        btnEdit.setTooltip(new Tooltip(I18n.COMMON.getString("action.edit")));
        btnEdit.getStyleClass().add("right-pill");
        btnEdit.setOnAction(event -> behaviour.onEdit());
        HBox.setMargin(btnEdit, new Insets(0, 10, 0, 0));

        btnDelete = new ActionCommand("", FontIconFactory.createIcon(AppFontIcons.DELETE));
        btnDelete.setTooltip(new Tooltip(I18n.COMMON.getString("action.delete")));
        btnDelete.setOnAction(event -> behaviour.onDelete());
        HBox.setMargin(btnDelete, new Insets(0, 10, 0, 0));


        btnExportToExcel = new ActionCommand("", FontIconFactory.createIcon(AppFontIcons.PDF));
        btnExportToExcel.setTooltip(new Tooltip("Export To Excel"));
        btnExportToExcel.getStyleClass().add("right-pill");
        btnExportToExcel.setOnAction(event -> behaviour.onExportToExcel());

        headerBar.getChildren().addAll(title, progressIndicator, spacer);

        if(behaviour.hasAddNewActions()) {
            headerBar.getChildren().add(btnAddNew);
        }

        if(behaviour.hasEditActions()) {
            headerBar.getChildren().add(btnEdit);
        }

        if(behaviour.hasDeleteActions()) {
            headerBar.getChildren().add(btnDelete);
        }

        if(behaviour.hasExportToExcelAction()){
            headerBar.getChildren().addAll(btnExportToExcel);
        }

        //headerBar.getChildren().addAll(title, progressIndicator, spacer, btnAddNew,btnAddNew, btnDelete);
        if (behaviour.hasPrintActions()) {

            btnPrintPreview = new ActionCommand("", FontIconFactory.createIcon(AppFontIcons.PRINT_PREVIEW));
            btnPrintPreview.setTooltip(new Tooltip(I18n.COMMON.getString("action.printPreview")));
            btnPrintPreview.getStyleClass().add("left-pill");
            btnPrintPreview.setOnAction(event -> behaviour.onPrintPreview());

            btnPrint = new ActionCommand("", FontIconFactory.createIcon(AppFontIcons.PRINT));
            btnPrint.setTooltip(new Tooltip(I18n.COMMON.getString("action.print")));
            btnPrint.getStyleClass().add("center-pill");
            btnPrint.setOnAction(event -> behaviour.onPrint());

            btnPdf = new ActionCommand("", FontIconFactory.createIcon(AppFontIcons.PDF));
            btnPdf.setTooltip(new Tooltip(I18n.COMMON.getString("action.pdf")));
            btnPdf.getStyleClass().add("right-pill");
            btnPdf.setOnAction(event -> behaviour.onPdf());
            HBox.setMargin(btnPdf, new Insets(0, 10, 0, 0));

            headerBar.getChildren().addAll(btnPrintPreview, btnPrint, btnPdf);
        }

        // search field
        searchField = new SearchField(300);
        searchField.textProperty().addListener((observable, oldValue, newValue) -> {
            pager.setCurrentPageIndex(1);
            refreshData();
        });
        HBox.setMargin(searchField, new Insets(0, 0, 0, 100));

        if(this.behaviour.hasSearchActions()) {
            headerBar.getChildren().add(searchField);
        }
        return headerBar;
    }

    private VBox getTablePane() {
        logger.debug("get table pane");
        tableView = new TableView<>();
        tableView.setPlaceholder(new Label(I18n.COMMON.getString("tableView.dataNotFound")));
        tableView.setEditable(false);
        tableView.setTableMenuButtonVisible(true);

        tableView.setRowFactory(tv -> new TableRow<T>() {
            @Override
            public void updateItem(T item, boolean empty) {
                super.updateItem(item, empty) ;
                if(!empty) {
                    if (behaviour.hasRowHighlighting(item))
                        setStyle("-fx-background-color: tomato;-fx-selection-bar: blue;");
                    else
                        setStyle("");
                }else{
                    setStyle("");
                }
            }
        });
        //tableView.getSelectionModel().setCellSelectionEnabled(true);

        /*************************************/
        //TableView tableView = new TableView();

        tableView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        MenuItem item = new MenuItem("Copy");
        item.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                    copySelectionToClipboard(tableView);
            }
        });
        ContextMenu menu = new ContextMenu();
        menu.getItems().add(item);

        List<MenuItem> customizedMenuItems = new ArrayList<>();
        customizedMenuItems = behaviour.getFloatingMenuItems();

        if(customizedMenuItems != null) {
            for (MenuItem menu_item : customizedMenuItems) {
                menu.getItems().add(menu_item);
            }
        }


        tableView.setContextMenu(menu);

        final KeyCodeCombination keyCodeCopy = new KeyCodeCombination(KeyCode.C, KeyCombination.CONTROL_ANY);
        tableView.setOnKeyPressed(event -> {
            if (keyCodeCopy.match(event)) {
                copySelectionToClipboard(tableView);
            }
        });

        /******************************************/
        List<TableColumn<T, ?>> columns = behaviour.getTableViewColumns();
        columns.forEach(col -> {
          col.prefWidthProperty().bind(getTableView().widthProperty().divide(columns.size()));
        });

        tableView.getColumns().addAll(columns);

        tableView.setOnMouseClicked((MouseEvent event) -> {
            if (event.getClickCount() > 1) {
                behaviour.onEdit();
            }
        });

        tableView.setOnKeyPressed((KeyEvent event) ->{
            // Get the Type of the Event
            String type = event.getEventType().getName();
            // Get the KeyCode of the Event
            KeyCode keyCode = event.getCode();
            if (event.getEventType() == KeyEvent.KEY_PRESSED && event.getCode() == KeyCode.ENTER)
            {
                behaviour.onEdit();
                event.consume();
            }
        });

        tableView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            behaviour.onSelectedItemChanged();
        });




        pager = new Pager(DesktopFxApplication.getLoginSession().getPreferences().getItemsPerPage(),this::refreshData);

        VBox tablePanel = new VBox();

        VBox tablePane = new VBox();
        tablePane.getStyleClass().add("data-page-center");
        //tablePane.getChildren().addAll(tableView, pager);
        tablePane.getChildren().addAll(tableView);
        tablePanel.getChildren().addAll(tableView,pager);
        VBox.setVgrow(tableView, Priority.ALWAYS);
        VBox.setVgrow(pager, Priority.NEVER);
        VBox.setMargin(pager, new Insets(10, 10, 10, 10));
        return tablePanel;
    }

    @SuppressWarnings("rawtypes")
    public void copySelectionToClipboard(final TableView<?> table) {
        final Set<Integer> rows = new TreeSet<>();
        for (final TablePosition tablePosition : table.getSelectionModel().getSelectedCells()) {
            rows.add(tablePosition.getRow());
        }
        final StringBuilder strb = new StringBuilder();
        boolean firstRow = true;
        for (final Integer row : rows) {
            if (!firstRow) {
                strb.append('\n');
            }
            firstRow = false;
            boolean firstCol = true;
            for (final TableColumn<?, ?> column : table.getColumns()) {
                if (!firstCol) {
                    strb.append('\t');
                }
                firstCol = false;
                final Object cellData = column.getCellData(row);
                strb.append(cellData == null ? "" : cellData.toString());
            }
        }
        final ClipboardContent clipboardContent = new ClipboardContent();
        clipboardContent.putString(strb.toString());
        Clipboard.getSystemClipboard().setContent(clipboardContent);
    }

    /**
     * Refreshs the data of the {@link TableView} and {@link Pager}
     */
    public void refreshData() {
        logger.debug("refresh data");
        Task<DOPage<T>> task = new Task<DOPage<T>>() {
            @Override
            protected DOPage<T> call() throws Exception {
                PageDetails details = new PageDetails();
                details.setStart(pager.getStartPosition());
                details.setEnd(pager.getEndPosition());
                details.setPageNumber(pager.getCurrentPageIndex() - 1);
                details.setSize(pager.getItemsPerPage());
                //return getController().getData(searchField.getText(),details);
                return (DOPage<T>)behaviour.getData(searchField.getText(),details);
            }
        };
        task.stateProperty().addListener((source, oldState, newState) -> {
            if (newState.equals(Worker.State.SUCCEEDED)) {
                tableView.getItems().clear();
                if (task.getValue() != null) {
                    DOPage<T> page = task.getValue();
                    List<T> tableRowMapDOList = page.getList();
                    tableRowMapDOList.forEach(tableRowMapDO -> {
                        tableView.getItems().addAll(behaviour.mapTableRowToDO((TableRowMapDO<String, String>)tableRowMapDO));
                    });
                    tableView.getSelectionModel().selectFirst();
                    pager.setRowCount(page.getTotalElements().intValue());
                }
            }
        });

        progressIndicator.visibleProperty().bind(task.runningProperty());
        new Thread(task).start();
    }

    public ITableDataViewBehaviour<T> getBehaviour() {
        return behaviour;
    }

    public void setBehaviour(ITableDataViewBehaviour<T> behaviour) {
        this.behaviour = behaviour;
    }

    public TableView getTableView() {
        return tableView;
    }

    public void setTableView(TableView tableView) {
        this.tableView = tableView;
    }

    public ActionCommand getBtnAddNew() {
        return btnAddNew;
    }

    public void setBtnAddNew(ActionCommand btnAddNew) {
        this.btnAddNew = btnAddNew;
    }

    public ActionCommand getBtnEdit() {
        return btnEdit;
    }

    public void setBtnEdit(ActionCommand btnEdit) {
        this.btnEdit = btnEdit;
    }

    public ActionCommand getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(ActionCommand btnDelete) {
        this.btnDelete = btnDelete;
    }

    public ActionCommand getBtnPrintPreview() {
        return btnPrintPreview;
    }

    public void setBtnPrintPreview(ActionCommand btnPrintPreview) {
        this.btnPrintPreview = btnPrintPreview;
    }

    public ActionCommand getBtnPrint() {
        return btnPrint;
    }

    public void setBtnPrint(ActionCommand btnPrint) {
        this.btnPrint = btnPrint;
    }

    public ActionCommand getBtnPdf() {
        return btnPdf;
    }

    public void setBtnPdf(ActionCommand btnPdf) {
        this.btnPdf = btnPdf;
    }

    public void resetPager(){
        pager.setCurrentPageIndex(1);
   }

}
