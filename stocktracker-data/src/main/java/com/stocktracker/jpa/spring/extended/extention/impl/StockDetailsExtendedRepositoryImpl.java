/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.jpa.spring.extended.extention.impl;

import com.stocktracker.jpa.spring.extended.constants.NativeQueryConstants;
import com.stocktracker.jpa.spring.extended.bean.MandiReconcilationData;
import com.stocktracker.jpa.spring.extended.bean.StockDetailsSummary;
import com.stocktracker.jpa.spring.extended.extention.StockDetailsExtendedRepository;
import com.stocktracker.model.sms.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.*;
import java.util.Date;
import java.util.stream.Collectors;

/**
 * Created by diwakar009 on 22/10/2019.
 */
@Repository
public class StockDetailsExtendedRepositoryImpl implements StockDetailsExtendedRepository{

    private final Logger logger = LoggerFactory.getLogger(StockDetailsExtendedRepositoryImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Page<StockDetails> findAllStockDetailsByFilter(String industryCodeFilter, String stockCodeFilter, String stockVarietyCodeFilter,
                                                          String transTypeFilter,String rstNumberFilter,
                                                          String vehicleNumberFilter, Date startDateFilter, Date endDateFilter,
                                                          String partyNameFilter,String onBehalfFilter,
                                                          String lampNameFilter,String mandiNameFilter,String revisitFlagFilter, Pageable pageRequest) {
        logger.info("Executing findallstockdetailsByFilter...");
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();

        CriteriaQuery<StockDetails> mainCreteriaQuery = criteriaBuilder.createQuery(StockDetails.class);
        CriteriaQuery<Long> countCriteriaQuery = criteriaBuilder.createQuery(Long.class);
        /*CriteriaQuery<StockDetailsSummary> summaryCriteriaQuery = criteriaBuilder.createQuery(StockDetailsSummary.class);*/

        Root<StockDetails> mainRoot = mainCreteriaQuery.from(StockDetails.class);
        Root<StockDetails> countRoot = countCriteriaQuery.from(StockDetails.class);
        /*Root<StockDetails> summaryRoot = summaryCriteriaQuery.from(StockDetails.class);*/

        Join<StockDetails,TransportDetails> transportDetailsJoin = null;
        Join<StockDetails,PartyDetails> partyDetailsJoin = null;
        Join<StockDetails,CostPriceDetails> costPriceDetailsJoin= null;
        Join<StockDetails,DeliveryDetails> deliveryDetailsJoin= null;

        transportDetailsJoin = mainRoot.join(StockDetails_.transportDetails, JoinType.LEFT); // Join with transport details to main query
        partyDetailsJoin = mainRoot.join(StockDetails_.partyDetails, JoinType.LEFT); // Join with party details to main query
        costPriceDetailsJoin = mainRoot.join(StockDetails_.costPriceDetails, JoinType.LEFT); // Join with cost price details to main query
        deliveryDetailsJoin = mainRoot.join(StockDetails_.deliveryDetails, JoinType.LEFT); // Join with delivery details to main query

        countRoot.join(StockDetails_.transportDetails, JoinType.LEFT); // Join with transport details to count query
        countRoot.join(StockDetails_.partyDetails, JoinType.LEFT); // Join with transport details to count query
        countRoot.join(StockDetails_.costPriceDetails, JoinType.LEFT); // Join with transport details to count query
        countRoot.join(StockDetails_.deliveryDetails, JoinType.LEFT); // Join with transport details to count query

        mainCreteriaQuery.select(criteriaBuilder.construct(StockDetails.class,
                    mainRoot.get("id"), mainRoot.get("industryCode"), mainRoot.get("stockCode"), mainRoot.get("stockVarietyCode"),
                    mainRoot.get("transactionType"), mainRoot.get("transactionDate"), mainRoot.get("noOfPackets"), mainRoot.get("totalWeightment"),
                    mainRoot.get("plusOrMinusOnTotal"),mainRoot.get("lampEntryInQtl"),mainRoot.get("lampEntryAfterAdjInQtl"), mainRoot.get("remarks"), mainRoot.get("onBeHalf"),
                    (transportDetailsJoin.get("rstNumber")).alias("rstNumber"),
                    (transportDetailsJoin.get("vehicleNumber")).alias("vehicleNumber"),
                    (partyDetailsJoin.get("partyName1")).alias("partyName"),mainRoot.get("lampNameCode"),mainRoot.get("mandiNameCode"),
                    (costPriceDetailsJoin.get(CostPriceDetails_.appliedRate)).alias("appliedRate"),
                    (costPriceDetailsJoin.get(CostPriceDetails_.actualAmount)).alias("actualAmount"),
                    (deliveryDetailsJoin.get(DeliveryDetails_.deliveryLatNo)).alias("latNo"), mainRoot.get("revisitFlag")
                ));

        /*summaryCriteriaQuery.select(
                criteriaBuilder.construct(StockDetailsSummary.class,
                        criteriaBuilder.sum(summaryRoot.get("noOfPackets")),
                        criteriaBuilder.sum(summaryRoot.get("totalWeightment"))));*/

        Optional<ParameterExpression<String>> icFilter = Optional.empty();
        Optional<ParameterExpression<String>> scFilter = Optional.empty();
        Optional<ParameterExpression<String>> svcFilter = Optional.empty();
        Optional<ParameterExpression<String>> tyFilter = Optional.empty();
        Optional<ParameterExpression<String>> rstNoFilter = Optional.empty();
        Optional<ParameterExpression<String>> vehicleNoFilter = Optional.empty();
        Optional<ParameterExpression<String>> pnFilter = Optional.empty();
        Optional<ParameterExpression<Date>> sdFilter = Optional.empty();
        Optional<ParameterExpression<Date>> edFilter = Optional.empty();
        Optional<ParameterExpression<String>> obFilter = Optional.empty();
        Optional<ParameterExpression<String>> lnFilter = Optional.empty();
        Optional<ParameterExpression<String>> mnFilter = Optional.empty();
        Optional<ParameterExpression<String>> rvFilter = Optional.empty();


        List<Predicate> predicates = new ArrayList<Predicate>();

        if(industryCodeFilter != null && !"".equals(industryCodeFilter)) {
            icFilter = Optional.ofNullable(criteriaBuilder.parameter(String.class));
            predicates.add(criteriaBuilder.equal(mainRoot.get("industryCode"), icFilter.get()));
        }

        if(stockCodeFilter != null && !"".equals(stockCodeFilter)) {
            scFilter = Optional.ofNullable(criteriaBuilder.parameter(String.class));
            predicates.add(criteriaBuilder.equal(mainRoot.get("stockCode"),scFilter.get()));
        }

        if(stockVarietyCodeFilter != null && !"".equals(stockVarietyCodeFilter)) {
            svcFilter = Optional.ofNullable(criteriaBuilder.parameter(String.class));
            predicates.add(criteriaBuilder.equal(mainRoot.get("stockVarietyCode"),svcFilter.get()));
        }

        if(transTypeFilter != null && !"".equals(transTypeFilter)) {
            tyFilter = Optional.ofNullable(criteriaBuilder.parameter(String.class));
            predicates.add(criteriaBuilder.equal(mainRoot.get("transactionType"),tyFilter.get()));
        }

        if(rstNumberFilter != null && !"".equals(rstNumberFilter)) {
            rstNoFilter = Optional.ofNullable(criteriaBuilder.parameter(String.class));
            predicates.add(criteriaBuilder.like(transportDetailsJoin.get(TransportDetails_.rstNumber),rstNoFilter.get()));
        }

        if(vehicleNumberFilter != null && !"".equals(vehicleNumberFilter)) {
            vehicleNoFilter = Optional.ofNullable(criteriaBuilder.parameter(String.class));
            predicates.add(criteriaBuilder.like(transportDetailsJoin.get(TransportDetails_.vehicleNumber),vehicleNoFilter.get()));
        }

        if(startDateFilter != null && endDateFilter != null){
            sdFilter = Optional.ofNullable(criteriaBuilder.parameter(Date.class));
            edFilter = Optional.ofNullable(criteriaBuilder.parameter(Date.class));
            predicates.add(criteriaBuilder.between(mainRoot.get("transactionDate").as(java.sql.Date.class),sdFilter.get(),edFilter.get()));

        }else {
            if (startDateFilter != null) {
                sdFilter = Optional.ofNullable(criteriaBuilder.parameter(Date.class));
                predicates.add(criteriaBuilder.equal(mainRoot.get("transactionDate").as(java.sql.Date.class), sdFilter.get()));
            }

            if (endDateFilter != null) {
                edFilter = Optional.ofNullable(criteriaBuilder.parameter(Date.class));
                predicates.add(criteriaBuilder.lessThanOrEqualTo(mainRoot.get("transactionDate").as(java.sql.Date.class), edFilter.get()));
            }
        }

        if(partyNameFilter != null && !"".equals(partyNameFilter)) {
            pnFilter = Optional.ofNullable(criteriaBuilder.parameter(String.class));
            predicates.add(criteriaBuilder.like(partyDetailsJoin.get(PartyDetails_.partyName1),pnFilter.get()));
        }

        if(onBehalfFilter != null && !"".equals(onBehalfFilter)) {
            obFilter = Optional.ofNullable(criteriaBuilder.parameter(String.class));
            predicates.add(criteriaBuilder.equal(mainRoot.get("onBeHalf"), obFilter.get()));
        }

        if(lampNameFilter != null && !"".equals(lampNameFilter)) {
            lnFilter = Optional.ofNullable(criteriaBuilder.parameter(String.class));
            predicates.add(criteriaBuilder.equal(mainRoot.get("lampNameCode"), lnFilter.get()));
        }

        if(mandiNameFilter != null && !"".equals(mandiNameFilter)) {
            mnFilter = Optional.ofNullable(criteriaBuilder.parameter(String.class));
            predicates.add(criteriaBuilder.equal(mainRoot.get("mandiNameCode"), mnFilter.get()));
        }

        if(revisitFlagFilter != null && !"".equals(revisitFlagFilter)) {
            if("Y".equals(revisitFlagFilter)) {
                rvFilter = Optional.ofNullable(criteriaBuilder.parameter(String.class));
                predicates.add(criteriaBuilder.equal(mainRoot.get("revisitFlag"), rvFilter.get()));
            }else{
//                rvFilter = Optional.ofNullable(criteriaBuilder.parameter(String.class));
                predicates.add(criteriaBuilder.or(criteriaBuilder.equal(mainRoot.get("revisitFlag"),"N"),
                        criteriaBuilder.isNull(mainRoot.get("revisitFlag"))));
            }
        }

        mainCreteriaQuery.where(predicates.toArray(new Predicate[predicates.size()])); // Apply where clause
        mainCreteriaQuery.orderBy(criteriaBuilder.desc(mainRoot.get("transactionDate")));
        TypedQuery<StockDetails> mainQuery = entityManager.createQuery(mainCreteriaQuery);

        // Count Query
        countCriteriaQuery.select(criteriaBuilder.count(countRoot));
        countCriteriaQuery.where(predicates.toArray(new Predicate[predicates.size()])); // Apply where clause
        TypedQuery<Long> countQuery = entityManager.createQuery(countCriteriaQuery);

       if(icFilter.isPresent()) {
           mainQuery.setParameter(icFilter.get(), industryCodeFilter);
           countQuery.setParameter(icFilter.get(), industryCodeFilter);
       }

       if(scFilter.isPresent()) {
           mainQuery.setParameter(scFilter.get(), stockCodeFilter);
           countQuery.setParameter(scFilter.get(), stockCodeFilter);
       }

       if(svcFilter.isPresent()) {
            mainQuery.setParameter(svcFilter.get(), stockVarietyCodeFilter);
            countQuery.setParameter(svcFilter.get(), stockVarietyCodeFilter);
       }

        if(tyFilter.isPresent()) {
            mainQuery.setParameter(tyFilter.get(), transTypeFilter);
            countQuery.setParameter(tyFilter.get(), transTypeFilter);
        }

        if(rstNoFilter.isPresent()) {
            mainQuery.setParameter(rstNoFilter.get(), "%"+ rstNumberFilter + "%");
            countQuery.setParameter(rstNoFilter.get(), "%"+ rstNumberFilter + "%");
        }

        if(vehicleNoFilter.isPresent()) {
            mainQuery.setParameter(vehicleNoFilter.get(), "%"+ vehicleNumberFilter + "%");
            countQuery.setParameter(vehicleNoFilter.get(), "%"+ vehicleNumberFilter + "%");
        }

        if(sdFilter.isPresent()) {
            mainQuery.setParameter(sdFilter.get(), startDateFilter);
            countQuery.setParameter(sdFilter.get(), startDateFilter);
        }

        if(edFilter.isPresent()) {
            mainQuery.setParameter(edFilter.get(), endDateFilter);
            countQuery.setParameter(edFilter.get(), endDateFilter);
        }

       /*
       if(pnFilter.isPresent()) {
            mainQuery.setParameter(pnFilter.get(), "%"+ partyNameFilter + "%");
            countQuery.setParameter(pnFilter.get(), "%"+ partyNameFilter + "%");
        }
        */
        if(pnFilter.isPresent()) {
            mainQuery.setParameter(pnFilter.get(), partyNameFilter);
            countQuery.setParameter(pnFilter.get(),partyNameFilter);
        }

        if(obFilter.isPresent()) {
            mainQuery.setParameter(obFilter.get(), onBehalfFilter);
            countQuery.setParameter(obFilter.get(), onBehalfFilter);
        }

        if(lnFilter.isPresent()) {
            mainQuery.setParameter(lnFilter.get(), lampNameFilter);
            countQuery.setParameter(lnFilter.get(), lampNameFilter);
        }

        if(mnFilter.isPresent()) {
            mainQuery.setParameter(mnFilter.get(), mandiNameFilter);
            countQuery.setParameter(mnFilter.get(), mandiNameFilter);
        }

        if(rvFilter.isPresent()) {
            mainQuery.setParameter(rvFilter.get(), revisitFlagFilter);
            countQuery.setParameter(rvFilter.get(), revisitFlagFilter);
        }

        long total = countQuery.getSingleResult();
        return (Page)(pageRequest == null?new PageImpl(mainQuery.getResultList()):this.readPage(mainQuery,pageRequest, total));
    }

    @Override
    public Page<StockDetailsSummary> findStockDetailsSummaryByFilter(String industryCodeFilter, String stockCodeFilter,
                                                                     String stockVarietyCodeFilter, String transTypeFilter,
                                                                     String rstNumberFilter, String vehicleNumberFilter,
                                                                     Date startDateFilter, Date endDateFilter,
                                                                     String partyNameFilter,String onBehalfFilter,
                                                                     String lampNameFilter,String mandiNameFilter,String revisitFlagFilter, Pageable pageRequest) {

        logger.info("Executing findallstockdetailsByFilter...");
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();

        CriteriaQuery<StockDetailsSummary> summaryCriteriaQuery = criteriaBuilder.createQuery(StockDetailsSummary.class);
        Root<StockDetails> summaryRoot = summaryCriteriaQuery.from(StockDetails.class);

        Join<StockDetails,TransportDetails> transportDetailsJoin = summaryRoot.join(StockDetails_.transportDetails, JoinType.LEFT); // Join with transport details to main query
        Join<StockDetails,PartyDetails> partyDetailsJoin = summaryRoot.join(StockDetails_.partyDetails, JoinType.LEFT); // Join with party details to main query
        Join<StockDetails,CostPriceDetails> costPriceDetailsJoin = summaryRoot.join(StockDetails_.costPriceDetails, JoinType.LEFT); // Join with party details to main query
        //Join<StockDetails,DeliveryDetails> deliveryDetailsJoin= deliveryDetailsJoin = summaryRoot.join(StockDetails_.deliveryDetails, JoinType.LEFT); // Join with delivery details to main query;

        summaryCriteriaQuery.select(
                criteriaBuilder.construct(StockDetailsSummary.class,summaryRoot.get("industryCode"),
                        summaryRoot.get(StockDetails_.stockCode),summaryRoot.get(StockDetails_.stockVarietyCode),
                        summaryRoot.get(StockDetails_.transactionType),
                        criteriaBuilder.sum(summaryRoot.get(StockDetails_.noOfPackets)),
                        criteriaBuilder.sum(summaryRoot.get(StockDetails_.totalWeightment)),
                        criteriaBuilder.sum(summaryRoot.get(StockDetails_.lampEntryInQtl)),
                        criteriaBuilder.sum(summaryRoot.get(StockDetails_.lampEntryAfterAdjInQtl)),
                        criteriaBuilder.sum(costPriceDetailsJoin.get(CostPriceDetails_.actualAmount))
                        ));

        Optional<ParameterExpression<String>> icFilter = Optional.empty();
        Optional<ParameterExpression<String>> scFilter = Optional.empty();
        Optional<ParameterExpression<String>> svcFilter = Optional.empty();
        Optional<ParameterExpression<String>> tyFilter = Optional.empty();
        Optional<ParameterExpression<String>> rstNoFilter = Optional.empty();
        Optional<ParameterExpression<String>> vehicleNoFilter = Optional.empty();
        Optional<ParameterExpression<String>> pnFilter = Optional.empty();
        Optional<ParameterExpression<Date>> sdFilter = Optional.empty();
        Optional<ParameterExpression<Date>> edFilter = Optional.empty();
        Optional<ParameterExpression<String>> obFilter = Optional.empty();
        Optional<ParameterExpression<String>> lnFilter = Optional.empty();
        Optional<ParameterExpression<String>> mnFilter = Optional.empty();
        Optional<ParameterExpression<String>> rvFilter = Optional.empty();


        List<Predicate> predicates = new ArrayList<Predicate>();

        if(industryCodeFilter != null && !"".equals(industryCodeFilter)) {
            icFilter = Optional.ofNullable(criteriaBuilder.parameter(String.class));
            predicates.add(criteriaBuilder.equal(summaryRoot.get("industryCode"), icFilter.get()));
        }

        if(stockCodeFilter != null && !"".equals(stockCodeFilter)) {
            scFilter = Optional.ofNullable(criteriaBuilder.parameter(String.class));
            predicates.add(criteriaBuilder.equal(summaryRoot.get("stockCode"),scFilter.get()));
        }

        if(stockVarietyCodeFilter != null && !"".equals(stockVarietyCodeFilter)) {
            svcFilter = Optional.ofNullable(criteriaBuilder.parameter(String.class));
            predicates.add(criteriaBuilder.equal(summaryRoot.get("stockVarietyCode"),svcFilter.get()));
        }

        if(transTypeFilter != null && !"".equals(transTypeFilter)) {
            tyFilter = Optional.ofNullable(criteriaBuilder.parameter(String.class));
            predicates.add(criteriaBuilder.equal(summaryRoot.get("transactionType"),tyFilter.get()));
        }

        if(rstNumberFilter != null && !"".equals(rstNumberFilter)) {
            rstNoFilter = Optional.ofNullable(criteriaBuilder.parameter(String.class));
            predicates.add(criteriaBuilder.like(transportDetailsJoin.get(TransportDetails_.rstNumber),rstNoFilter.get()));
        }

        if(vehicleNumberFilter != null && !"".equals(vehicleNumberFilter)) {
            vehicleNoFilter = Optional.ofNullable(criteriaBuilder.parameter(String.class));
            predicates.add(criteriaBuilder.like(transportDetailsJoin.get(TransportDetails_.vehicleNumber),vehicleNoFilter.get()));
        }

        if(startDateFilter != null && endDateFilter != null){
            sdFilter = Optional.ofNullable(criteriaBuilder.parameter(Date.class));
            edFilter = Optional.ofNullable(criteriaBuilder.parameter(Date.class));
            predicates.add(criteriaBuilder.between(summaryRoot.get("transactionDate").as(java.sql.Date.class),sdFilter.get(),edFilter.get()));

        }else {
            if (startDateFilter != null) {
                sdFilter = Optional.ofNullable(criteriaBuilder.parameter(Date.class));
                predicates.add(criteriaBuilder.equal(summaryRoot.get("transactionDate").as(java.sql.Date.class), sdFilter.get()));
            }

            if (endDateFilter != null) {
                edFilter = Optional.ofNullable(criteriaBuilder.parameter(Date.class));
                predicates.add(criteriaBuilder.lessThanOrEqualTo(summaryRoot.get("transactionDate").as(java.sql.Date.class), edFilter.get()));
            }
        }

        if(partyNameFilter != null && !"".equals(partyNameFilter)) {
            pnFilter = Optional.ofNullable(criteriaBuilder.parameter(String.class));
            predicates.add(criteriaBuilder.like(partyDetailsJoin.get(PartyDetails_.partyName1),pnFilter.get()));
        }

        if(onBehalfFilter != null && !"".equals(onBehalfFilter)) {
            obFilter = Optional.ofNullable(criteriaBuilder.parameter(String.class));
            predicates.add(criteriaBuilder.equal(summaryRoot.get("onBeHalf"), obFilter.get()));
        }

        if(lampNameFilter != null && !"".equals(lampNameFilter)) {
            lnFilter = Optional.ofNullable(criteriaBuilder.parameter(String.class));
            predicates.add(criteriaBuilder.equal(summaryRoot.get("lampNameCode"), lnFilter.get()));
        }

        if(mandiNameFilter != null && !"".equals(mandiNameFilter)) {
            mnFilter = Optional.ofNullable(criteriaBuilder.parameter(String.class));
            predicates.add(criteriaBuilder.equal(summaryRoot.get("mandiNameCode"), mnFilter.get()));
        }

        if(revisitFlagFilter != null && !"".equals(revisitFlagFilter)) {
            if("Y".equals(revisitFlagFilter)) {
                rvFilter = Optional.ofNullable(criteriaBuilder.parameter(String.class));
                predicates.add(criteriaBuilder.equal(summaryRoot.get("revisitFlag"), rvFilter.get()));
            }else{
//                rvFilter = Optional.ofNullable(criteriaBuilder.parameter(String.class));
                predicates.add(criteriaBuilder.or(criteriaBuilder.equal(summaryRoot.get("revisitFlag"),"N"),
                        criteriaBuilder.isNull(summaryRoot.get("revisitFlag"))));
            }
        }

      // Summary Query

        summaryCriteriaQuery.where(predicates.toArray(new Predicate[predicates.size()])); // Apply where clause
        summaryCriteriaQuery.groupBy(summaryRoot.get("industryCode"),
                summaryRoot.get("stockCode"),summaryRoot.get("stockVarietyCode"),
                summaryRoot.get("transactionType"));
        summaryCriteriaQuery.orderBy(criteriaBuilder.desc(summaryRoot.get("industryCode")),
                criteriaBuilder.desc(summaryRoot.get("stockCode")),
                criteriaBuilder.desc(summaryRoot.get("stockVarietyCode")),
                criteriaBuilder.desc(summaryRoot.get("transactionType"))
        );

        TypedQuery<StockDetailsSummary> summaryQuery = entityManager.createQuery(summaryCriteriaQuery);

        if(icFilter.isPresent()) {
            summaryQuery.setParameter(icFilter.get(), industryCodeFilter);
        }

        if(scFilter.isPresent()) {
            summaryQuery.setParameter(scFilter.get(), stockCodeFilter);
        }

        if(svcFilter.isPresent()) {
            summaryQuery.setParameter(svcFilter.get(), stockVarietyCodeFilter);
        }

        if(tyFilter.isPresent()) {
            summaryQuery.setParameter(tyFilter.get(), transTypeFilter);
        }

        if(rstNoFilter.isPresent()) {
            summaryQuery.setParameter(rstNoFilter.get(), "%"+ rstNumberFilter + "%");
        }

        if(vehicleNoFilter.isPresent()) {
            summaryQuery.setParameter(vehicleNoFilter.get(), "%"+ vehicleNumberFilter + "%");
        }

        if(sdFilter.isPresent()) {
            summaryQuery.setParameter(sdFilter.get(), startDateFilter);
        }

        if(edFilter.isPresent()) {
            summaryQuery.setParameter(edFilter.get(), endDateFilter);
        }

        /*if(pnFilter.isPresent()) {
            summaryQuery.setParameter(pnFilter.get(), "%"+ partyNameFilter + "%");
        }*/

        if(pnFilter.isPresent()) {
            summaryQuery.setParameter(pnFilter.get(), partyNameFilter);
        }

        if(obFilter.isPresent()) {
            summaryQuery.setParameter(obFilter.get(), onBehalfFilter);
        }

        if(lnFilter.isPresent()) {
            summaryQuery.setParameter(lnFilter.get(), lampNameFilter);
        }

        if(mnFilter.isPresent()) {
            summaryQuery.setParameter(mnFilter.get(), mandiNameFilter);
        }

        if(rvFilter.isPresent()) {
            summaryQuery.setParameter(rvFilter.get(), revisitFlagFilter);
        }


        //long total = countQuery.getSingleResult();
        //return (Page)(pageRequest == null?new PageImpl(summaryQuery.getResultList()):this.readPage(summaryQuery,pageRequest, total));
        pageRequest = null; // Always return all records

        return (Page)(pageRequest == null?new PageImpl(summaryQuery.getResultList()):this.readPage(summaryQuery,pageRequest, 0l));
    }

    @Override
    public List<MandiReconcilationData> getMandiReconciliationReportData(String industryCodeFilter, String lampNameCodeFilter, String mandiNameCodeFilter) {
        logger.info("Executing getMandiReconciliationReportData...");
        Query mainQuery = entityManager.createNativeQuery(NativeQueryConstants.MANDI_RECONCILIATION_QUERY_1);
        mainQuery.setParameter(1,Arrays.asList("MDRE","MDAJ","MDRT","MDEN","MDEP"));
        mainQuery.setParameter(2, industryCodeFilter);
        if(lampNameCodeFilter == null || lampNameCodeFilter.isEmpty()){
            mainQuery.setParameter(3,"%");
        }else{
            mainQuery.setParameter(3,"%" + lampNameCodeFilter + "%");
        }

        if(mandiNameCodeFilter == null || mandiNameCodeFilter.isEmpty()){
            mainQuery.setParameter(4,"%");
        }else{
            mainQuery.setParameter(4,"%" + mandiNameCodeFilter + "%");
        }

        List<Object[]> resultList = ( List<Object[]>)mainQuery.getResultList();
        List<MandiReconcilationData> mandiReconcilationDataList = resultList.stream()
                .map(objects -> {
                     MandiReconcilationData mandiReconcilationData = new MandiReconcilationData();
                     mandiReconcilationData.setIndustryCode((String)objects[0]);
                     mandiReconcilationData.setStockCode((String)objects[1]);
                     mandiReconcilationData.setStockVarietyCode((String)objects[2]);
                     mandiReconcilationData.setTransactionType((String)objects[3]);
                     mandiReconcilationData.setLampNameCode((String)objects[4]);
                     mandiReconcilationData.setMandiNameCode((String)objects[5]);
                     mandiReconcilationData.setPartyName((String)objects[6]);
                     mandiReconcilationData.setId((Long) cast(objects[7]));
                     mandiReconcilationData.setTotalReceived((Double) cast(objects[8]));
                     mandiReconcilationData.setTotalAdjusted((Double) cast(objects[9]));
                     mandiReconcilationData.setTotalReceivedNAdjustedNPurchased((Double) cast(objects[10]));
                     mandiReconcilationData.setTotalEntered((Double) cast(objects[11]));
                     mandiReconcilationData.setLampEntryAdjustment((Double) cast(objects[12]));
                     mandiReconcilationData.setTotalLampEntryAfterAdjustment((Double) cast(objects[13]));
                     mandiReconcilationData.setDifference((Double) cast(objects[14]));
                     mandiReconcilationData.setOnBehalf((String)objects[15]);
                     mandiReconcilationData.setTransactionDate((Date)cast(objects[16]));
                     mandiReconcilationData.setVehicleNumber((String)objects[17]);
                     mandiReconcilationData.setRstNumber((String)objects[18]);
                     mandiReconcilationData.setTotalExcessPurchased((Double) cast(objects[19]));
                     return mandiReconcilationData;
                })
                .collect(Collectors.toList());
        return mandiReconcilationDataList;
    }

    private Object cast(Object object){
        if(object != null){
            if(object instanceof BigInteger){
                return ((BigInteger) object).longValue();
            }else if(object instanceof BigDecimal){
                return ((BigDecimal) object).doubleValue();
            }else if(object instanceof Date){
                return ((Date) object);
            }
        }
        return object;
    }

    protected Page<?> readPage(TypedQuery<?> query, Pageable pageable, Long total) {
        query.setFirstResult((int) pageable.getOffset());
        query.setMaxResults(pageable.getPageSize());
        List<?> content = total.longValue() > (long)pageable.getOffset()?query.getResultList(): Collections.emptyList();
        return new PageImpl(content, pageable, total.longValue());
    }

}
