/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.domainobjs.json;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonIgnoreProperties (ignoreUnknown=true)
@JsonInclude(Include.NON_EMPTY)
public class DomainObjectList<T> {

	List<T> list;

	public List<T> getList() {
		return list;
	}

	public void setList(List<T> list) {
		this.list = list;
	}
	
}
