package com.desktopapp.fx.manager;

import com.desktopapp.fx.DesktopFxApplication;
import com.desktopapp.fx.app.AppFeatures;
import com.desktopapp.fx.commands.factory.CommandFactory;
import com.desktopapp.fx.commands.factory.ICommandFactory;
import com.desktopapp.fx.common.StageViewBean;
import com.desktopapp.fx.control.TipOfTheDay;
import com.desktopapp.fx.view.AppView;
import com.stocktracker.domainobjs.json.PreferencesDO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * Created by diwakar009 on 13/12/2018.
 */
@Component
@Scope("prototype")
public class ApplicationViewManager {

    @Autowired
    private PreferenceManager preferenceManager;

    private AppView appView;

    private StageViewBean stageViewBean;

    public void showAppView(StageViewBean stageViewBean) {
        /*Initiate login session*/
        this.stageViewBean = stageViewBean;
        appView = new AppView(this);
        this.stageViewBean.getScene().setRoot(appView);
        appView.activateRibbonAccelerators();
        CommandFactory.getInstance().getCommand(appView,ICommandFactory.CMD_STOCKMAINTENANCE).executeCommand();
        if (DesktopFxApplication.get().getLoginSession().getPreferences().isShowTipOfTheDay()) {
            showTipOfTheDay();
        }
    }

    public void showTipOfTheDay() {
        TipOfTheDay tipOfTheDay = new TipOfTheDay(this.stageViewBean.getStage(), getTipsProperties());
        tipOfTheDay.getShowOnStartupCheckBox().setSelected(DesktopFxApplication.get().getLoginSession().getPreferences().isShowTipOfTheDay());
        tipOfTheDay.getShowOnStartupCheckBox().selectedProperty().addListener((observable, oldValue, newValue) -> {
            if (DesktopFxApplication.get().getLoginSession().getPreferences().isShowTipOfTheDay() != newValue) {
                DesktopFxApplication.get().getLoginSession().getPreferences().setShowTipOfTheDay(newValue);
                PreferencesDO preferences = preferenceManager.updatePreferences(DesktopFxApplication.get().getLoginSession().getPreferences());
                DesktopFxApplication.get().getLoginSession().setPreferences(preferences);
            }
        });
        tipOfTheDay.showAndWait();
    }

    private String getTipsProperties() {
        String locale = Locale.getDefault().toString();
        switch (locale) {
            case "en_GB": return AppFeatures.TIPS_PROPERTIES;
            case "de_DE": return AppFeatures.TIPS_DE_PROPERTIES;
            case "tr_TR": return AppFeatures.TIPS_TR_PROPERTIES;
            default: throw new IllegalArgumentException("Tips of the locale not found!");
        }
    }

    public PreferenceManager getPreferenceManager() {
        return preferenceManager;
    }

}
