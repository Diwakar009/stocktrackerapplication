package com.desktopapp.fx.view;

import com.desktopapp.fx.DesktopFxApplication;
import com.desktopapp.fx.app.AppAccelerator;
import com.desktopapp.fx.app.AppFontIcons;
import com.desktopapp.fx.common.ITableDataViewBehaviour;
import com.desktopapp.fx.common.PageDetails;
import com.desktopapp.fx.control.*;
import com.desktopapp.fx.control.fonticon.FontIcon;
import com.desktopapp.fx.control.multiscreen.StackedScreen;
import com.desktopapp.fx.controller.StaticCodeDecodeController;
import com.desktopapp.fx.mvc.AbstractFormView;
import com.desktopapp.fx.mvc.DataPageController;
import com.desktopapp.fx.util.I18n;
import com.desktopapp.fx.util.ViewHelpers;
import com.stocktracker.component.constants.DocStoreConstants;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.*;
import org.controlsfx.validation.Validator;
import com.stocktracker.domainobjs.json.DOPage;
import com.stocktracker.domainobjs.json.StaticCodeDecodeDO;
import com.stocktracker.domainobjs.json.TableRowMapDO;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by diwakar009 on 3/3/2019.
 */
public class StaticCodeDecodeForm extends AbstractFormView<StaticCodeDecodeDO> {

    private StaticCodeDecodeDO staticCodeDecodeDO;
    private TextFieldExt tfCodeDesc;
    private TextFieldExt tfCodeName;
    private TextFieldExt tfCodeValueFilter;
    private TextFieldExt tfCodeValue;
    private CodeDecodeField cdLanguage;
    private TableDataView<StaticCodeDecodeDO> tableDataView;

    public StaticCodeDecodeForm(DataPageController<StaticCodeDecodeDO> controller, StaticCodeDecodeDO staticCodeDecodeDO,Map<String,Object> intent) {
        super(controller,intent);
        this.staticCodeDecodeDO = staticCodeDecodeDO;
        loadLatestEntity();
        this.getLblformTitle().setText(getTitle());
        pop();
    }

    public StaticCodeDecodeForm(DataPageController<StaticCodeDecodeDO> controller, StaticCodeDecodeDO staticCodeDecodeDO,StackedScreen stackedScreen,Map<String,Object> intent) {
        super(controller,stackedScreen,intent);
        this.staticCodeDecodeDO = staticCodeDecodeDO;
        loadLatestEntity();
        this.getLblformTitle().setText(getTitle());
        pop();
    }


    @Override
    public Node buildFormView() {

        tfCodeName = ViewHelpers.createTextFieldExt(30, 200);
        getValidationSupport().registerValidator(tfCodeName, true,
                Validator.createEmptyValidator(I18n.COMMON.getString("validation.required")));

        tfCodeValueFilter = ViewHelpers.createTextFieldExt(5, 200);

        tfCodeValue = ViewHelpers.createTextFieldExt(5, 200);
        getValidationSupport().registerValidator(tfCodeValue, true,
                Validator.createEmptyValidator(I18n.COMMON.getString("validation.required")));

        cdLanguage = ViewHelpers.createCodeDecodeField(DocStoreConstants.CD_LANGUAGE,"EN_US",200);

        tfCodeDesc = ViewHelpers.createTextFieldExt(100, 200);
        getValidationSupport().registerValidator(tfCodeDesc, true,
                Validator.createEmptyValidator(I18n.COMMON.getString("validation.required")));


        registerComponents(tfCodeName,tfCodeValueFilter,tfCodeValue,cdLanguage,tfCodeDesc);

        GridPane formPane = new GridPane();
        formPane.setPadding(new Insets(20, 30, 10, 30));
        formPane.setHgap(10);
        formPane.setVgap(10);

        formPane.add(new Label(I18n.STATICCODEDECODE.getString("form.codeName")), 0, 0);
        formPane.add(tfCodeName, 1, 0);

        formPane.add(new Label(I18n.STATICCODEDECODE.getString("form.codeValue")), 0, 1);
        formPane.add(tfCodeValue, 1, 1);

        formPane.add(new Label(I18n.STATICCODEDECODE.getString("form.codeValueFilter")), 0, 2);
        formPane.add(tfCodeValueFilter, 1, 2);

        formPane.add(new Label(I18n.STATICCODEDECODE.getString("form.codeDesc")), 0, 3);
        formPane.add(tfCodeDesc, 1, 3);

        formPane.add(new Label(I18n.STATICCODEDECODE.getString("form.language")), 0, 4);
        formPane.add(cdLanguage.asControl(), 1, 4);

        tableDataView = new TableDataView(new StaticCodeDecodeTableBehaviour(this));

        VBox vbox = new VBox();

        HBox formPaneHBox = new HBox();
        formPaneHBox.getChildren().add(formPane);
        formPaneHBox.setAlignment(Pos.CENTER);

        HBox tablePaneHBox = new HBox();

        tablePaneHBox.getChildren().add(tableDataView);
        HBox.setHgrow(tableDataView,Priority.ALWAYS);
        VBox.setVgrow(tableDataView,Priority.SOMETIMES);
        vbox.getChildren().add(formPaneHBox);
        vbox.getChildren().add(tablePaneHBox);

        return vbox;
    }

    @Override
    public StaticCodeDecodeDO getCurrentEntity() {
        return staticCodeDecodeDO;
    }

    @Override
    public void loadLatestEntity() {

    }

    @Override
    public void pop() {

        if( !staticCodeDecodeDO.isNew() ){
            tfCodeName.setAsReadOnly(true);
            cdLanguage.setAsReadOnly(true);
        }

        setDOToControls(staticCodeDecodeDO);
        tableDataView.refreshData(); // refresh the table data
    }

    public void setDOToControls(StaticCodeDecodeDO staticCodeDecodeDO){
        if(staticCodeDecodeDO != null) {
            tfCodeName.setDataValue(staticCodeDecodeDO.getCodeName());
            tfCodeValue.setDataValue(staticCodeDecodeDO.getCodeValue());
            tfCodeDesc.setDataValue(staticCodeDecodeDO.getCodeDesc());
            tfCodeValueFilter.setDataValue(staticCodeDecodeDO.getCodeValueFilter());
            cdLanguage.setDataValue(staticCodeDecodeDO.getLanguage());
        }
    }

    @Override
    public void onDelete(){
        ((StaticCodeDecodeController)getController()).onDelete((StaticCodeDecodeDO) tableDataView.getTableView().getSelectionModel().getSelectedItem());
        refreshForm();
    }

    @Override
    public void push() {
        staticCodeDecodeDO.setCodeName(tfCodeName.getDataValue());
        staticCodeDecodeDO.setCodeValue(tfCodeValue.getDataValue());
        staticCodeDecodeDO.setCodeDesc(tfCodeDesc.getDataValue());
        staticCodeDecodeDO.setLanguage(cdLanguage.getDataValue());
        staticCodeDecodeDO.setCodeValueFilter(tfCodeValueFilter.getDataValue());

        if (staticCodeDecodeDO.isNew()) {
            staticCodeDecodeDO.setCreatedBy(DesktopFxApplication.getLoginSession().getUserName());
        } else {
            staticCodeDecodeDO.setUpdatedBy(DesktopFxApplication.getLoginSession().getUserName());
        }
    }

    @Override
    public void refreshForm() {
        getTableDataView().refreshData();
    }

    @Override
    public FontIcon getFontIcon() {
        return AppFontIcons.STATICCODEDECODE;
    }

    @Override
    public String getTitle() {
        return !isEditMode() ? I18n.STATICCODEDECODE.getString("form.newTitle") : I18n.STATICCODEDECODE.getString("form.editTitle");
    }

    @Override
    public Node asNode() {
        return this;
    }

    @Override
    public List<ActionCommand> addMoreActionCommands(List<ActionCommand> commands) {
        commands = new ArrayList<ActionCommand>();
        commands.add(new ActionButtonCommand(I18n.COMMON.getString("action.save"), AppAccelerator.getKeyCode("SCD-S"), event -> onSave()));
        commands.add(new ActionButtonCommand(I18n.COMMON.getString("action.saveAndClose"), AppAccelerator.getKeyCode("SCD-A"), event -> onSaveAndClose()));
        commands.add(new ActionButtonCommand(I18n.COMMON.getString("action.delete"), AppAccelerator.getKeyCode("SCD-D"), event -> onDelete()));
        commands.add(new ActionButtonCommand(I18n.COMMON.getString("action.clear"), AppAccelerator.getKeyCode("SCD-C"), event -> onClear()));
        commands.add(new ActionButtonCommand(I18n.COMMON.getString("action.close"),new KeyCodeCombination(KeyCode.ESCAPE, KeyCombination.SHIFT_DOWN), event -> onCloseForm()));

        return commands;
    }

    public StaticCodeDecodeDO getStaticCodeDecodeDO() {
        return staticCodeDecodeDO;
    }

    public void setStaticCodeDecodeDO(StaticCodeDecodeDO staticCodeDecodeDO) {
        this.staticCodeDecodeDO = staticCodeDecodeDO;
    }

    public TableDataView<StaticCodeDecodeDO> getTableDataView() {
        return tableDataView;
    }

    public void setTableDataView(TableDataView<StaticCodeDecodeDO> tableDataView) {
        this.tableDataView = tableDataView;
    }
}


