/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.domainobjs.json;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties (ignoreUnknown=true)
@JsonInclude(Include.NON_EMPTY)
@Deprecated
public class TableDataDO  extends BaseDomainObject implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@JsonProperty("rows")
	private List<TableRowMapDO<String,String>> rows = null;
	
	
	public TableDataDO() {
		super();
		rows = new ArrayList<TableRowMapDO<String,String>>();
	}

	@Override
	public Object getDomainObjectKey() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public void addRow(TableRowMapDO<String,String> row){
		getRows().add(row);
	}

	public List<TableRowMapDO<String, String>> getRows() {
		return rows;
	}

	public void setRows(List<TableRowMapDO<String, String>> rows) {
		this.rows = rows;
	}

	@Override
	public Object getId() {
		return null;
	}
}
