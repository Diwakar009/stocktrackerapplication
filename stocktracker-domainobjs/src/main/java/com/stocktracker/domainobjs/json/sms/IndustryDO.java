/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.domainobjs.json.sms;

import com.stocktracker.domainobjs.json.BaseDomainObject;
import com.stocktracker.model.BaseEntity;
import com.stocktracker.model.sms.Industry;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;


/**
 * IndustryDO class
 * 
 */
@JsonIgnoreProperties (ignoreUnknown=true)
@JsonInclude(Include.NON_EMPTY)
public class IndustryDO extends BaseDomainObject implements Serializable {
	private static final long serialVersionUID = 1L;

	public static String CLASS_NAME = IndustryDO.class.getName();

	@JsonProperty("id")
	private IndustryDOPK id;

	@JsonProperty("industryType")
	private String industryType;

	@JsonProperty("industryOwner")
	private String industryOwner;

	@JsonProperty("industryStatus")
	private String industryStatus;

	@JsonProperty("moreDetails")
	private String moreDetails;

	public void setIndustryCode(String industryCode){
		if(getId() == null){
			id = new IndustryDOPK();
		}
		id.setIndustryCode(industryCode);
	}

	public String getIndustryCode(){
		if(getId() == null){
			id = new IndustryDOPK();
		}
		return id.getIndustryCode();
	}

	public String getIndustryType() {
		return industryType;
	}

	public void setIndustryType(String industryType) {
		this.industryType = industryType;
	}

	public String getIndustryOwner() {
		return industryOwner;
	}

	public void setIndustryOwner(String industryOwner) {
		this.industryOwner = industryOwner;
	}

	public String getIndustryStatus() {
		return industryStatus;
	}

	public void setIndustryStatus(String industryStatus) {
		this.industryStatus = industryStatus;
	}

	public String getMoreDetails() {
		return moreDetails;
	}

	public void setMoreDetails(String moreDetails) {
		this.moreDetails = moreDetails;
	}

	public IndustryDO() {
		super();
	}

	public IndustryDO(Industry industry) {
		copyEntity2DomainObject(industry);
	}

	@Override
	public IndustryDOPK getId() {
		return this.id;
	}

	public void setId(IndustryDOPK id) {
		this.id = id;
	}

	@JsonIgnore
	@Override
	public Object getDomainObjectKey() {
		return getId();
	}

	@Override
	public Industry copyDomainObject2Entity(BaseEntity entity) {
		Industry industry = (Industry)entity;
		industry.setCreatedBy(this.getCreatedBy());
		industry.setUpdatedBy(this.getUpdatedBy());
		industry.setCreatedAt(this.getCreatedAt());
		industry.setUpdatedAt(this.getUpdatedAt());

		industry.setIndustryCode(this.getId().getIndustryCode());
		industry.setIndustryOwner(this.getIndustryOwner());
		industry.setIndustryType(this.getIndustryType());
		industry.setIndustryStatus(this.getIndustryStatus());
		industry.setMoreDetails(this.getMoreDetails());

		return industry;
	}
	@Override
	public void copyEntity2DomainObject(BaseEntity entity) {
		Industry industry = (Industry)entity;
		this.setCreatedBy(industry.getCreatedBy());
		this.setUpdatedBy(industry.getUpdatedBy());
		this.setCreatedAt(industry.getCreatedAt());
		this.setUpdatedAt(industry.getUpdatedAt());

		this.setIndustryCode(industry.getId().getIndustryCode());
		this.setIndustryOwner(industry.getIndustryOwner());
		this.setIndustryType(industry.getIndustryType());
		this.setIndustryStatus(industry.getIndustryStatus());
		this.setMoreDetails(industry.getMoreDetails());
	}

}