/*
 * Copyright (c) 2020.
 *
 * All Rights are with Diwakar C
 */

package com.desktopapp.service;

import com.stocktracker.domainobjs.json.*;
import org.springframework.stereotype.Service;

import java.util.*;


/**
 * Static Code Decode Service
 *
 * @author Ratnala Diwakar Choudhury
 */
@Service
public class PreferencesService extends AbstractService<PreferencesDO> {

	public PreferencesService() {
        super(PreferencesDO.class);
    }
    
	public static final String BASE_RESOURCE = "preference";

	public PreferencesDO find(Long preferenceId) {
		PreferencesDO preferencesDO = new PreferencesDO();
		preferencesDO.setId(preferenceId);
		return super.find(preferencesDO);
	}

	@Override
	protected String getBaseResource() {
		return BASE_RESOURCE;
	}

}
