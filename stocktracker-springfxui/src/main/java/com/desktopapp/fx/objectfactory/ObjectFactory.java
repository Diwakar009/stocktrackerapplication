package com.desktopapp.fx.objectfactory;

import com.desktopapp.fx.controller.*;
import com.desktopapp.fx.manager.FXBootManager;
import com.desktopapp.service.AppUserLoginService;
import com.desktopapp.service.AppUserService;
import com.desktopapp.service.PreferencesService;
import com.desktopapp.service.StaticCodeDecodeService;
import org.springframework.context.ApplicationContext;

public class ObjectFactory {


    public static ApplicationContext context;

	/**
	 * Controller Object
	 */
	public static final String STATIC_CODE_DECODE_CTRL = "staticCodeDecodeController";
	public static final String DOC_HOLDER_CTRL = "documentHolderController";
	public static final String DOC_CONTENT_CTRL = "documentContentController";
	public static final String APP_USER_CTRL ="appUserController";
	public static final String APP_USER_LOGIN_CTRL ="appUserLoginController";
	public static final String PREFERENCES_CTRL=" preferencesController";
	public static final String STOCK_MAINTENANCE_CTRL = "stockMaintenanceController";
	public static final String CONTRACT_MAINTENANCE_CTRL = "contractMaintenanceController";


	/**
	 * Service Object
	 */
	public static final String DOC_HOLDER_SERVICE=" documentHolderService";
	public static final String DOC_CONTENT_SERVICE=" documentContentService";
	public static final String STATIC_CODE_DECODE_SERVICE=" staticCodeDecodeService";
	public static final String APP_USER_SERVICE ="appUserService";
	public static final String APP_USER_LOGIN_SERVICE ="appUserLoginService";
	public static final String APP_USER_FUNCTION_ACC_SERVICE ="appUserFunctionAccessService";
	public static final String APP_USER_UI_SETTINGS_SERVICE = "appUserUISettingsService";
	public static final String EMAIL_GATEWAY_SERVICE=" emailGatewayService";
	public static final String PREFERENCES_SERVICE=" preferencesService";


	/**
	 * Return controller object
	 * 
	 * @param objectName
	 * @return
	 */
	public static Object getControllerObject(String objectName){
		if(STATIC_CODE_DECODE_CTRL.equals(objectName)){
			return getContext().getBean(StaticCodeDecodeController.class);
		}else if(DOC_HOLDER_CTRL.equals(objectName)){
			return getContext().getBean("documentHolderController");
		}else if(DOC_CONTENT_CTRL.equals(objectName)){
			return getContext().getBean("documentContentController");
		}else if(APP_USER_CTRL.equals(objectName)){
			return getContext().getBean(AppUserController.class);
		}else if(APP_USER_LOGIN_CTRL.equals(objectName)){
			return getContext().getBean(AppUserLoginController.class);
		}else if(PREFERENCES_CTRL.equals(objectName)){
			return getContext().getBean(PreferencesController.class);
		}else if(STOCK_MAINTENANCE_CTRL.equals(objectName)){
			return getContext().getBean(StockMaintenanceController.class);
		}else if(CONTRACT_MAINTENANCE_CTRL.equals(objectName)){
			return getContext().getBean(ContractMaintenanceController.class);
		}else{
			return null;
		}
	}
	
	
	/**
	 * Return service object
	 * 
	 * @param objectName
	 * @return
	 *//*
	public static Object getServiceObject(String objectName){
		if(DOC_HOLDER_SERVICE.equals(objectName)){
			return getContext().getBean("documentHolderService");
		}else if(DOC_CONTENT_SERVICE.equals(objectName)){
			return getContext().getBean("documentContentService");
		}else if(STATIC_CODE_DECODE_SERVICE.equals(objectName)){
			return getContext().getBean(StaticCodeDecodeService.class);
		}else if(APP_USER_SERVICE.equals(objectName)){
			return getContext().getBean(AppUserService.class);
		}else if(APP_USER_LOGIN_SERVICE.equals(objectName)){
			return getContext().getBean(AppUserLoginService.class);
		}else if(APP_USER_FUNCTION_ACC_SERVICE.equals(objectName)){
			return getContext().getBean("functionAccessService");
		}else if(APP_USER_UI_SETTINGS_SERVICE.equals(objectName)){
			return getContext().getBean("uiSettingsService");
		}else if(EMAIL_GATEWAY_SERVICE.equals(objectName)){
			return getContext().getBean("emailGatewayService");
		}else if(PREFERENCES_SERVICE.equals(objectName)){
			return getContext().getBean(PreferencesService.class);
		}else{
			return null;
		}
	}*/

	public static Object getProductBootManager(){
		return getContext().getBean(FXBootManager.class);
	}


	public static ApplicationContext getContext() {
		return context;
	}

	public static void setContext(ApplicationContext context) {
		ObjectFactory.context = context;
	}
}
