/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.domainobjs.json;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by diwakar009 on 13/11/2019.
 */
@JsonIgnoreProperties(ignoreUnknown=true)
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ResultDO implements Serializable{

    @JsonProperty("resultAttributes")
    private List<String> resultAttributes;

    @JsonProperty("isError")
    private Boolean isError;

    @JsonProperty("isWarning")
    private Boolean isWarning;

    @JsonProperty("errorList")
    private List<String> errorList;

    @JsonProperty("warningList")
    private List<String> warningList;

    public ResultDO() {
        this.isError = false;
        errorList = new ArrayList<String>();
        warningList = new ArrayList<String>();
        resultAttributes = new ArrayList<String>();
    }

    public ResultDO(Boolean isError, Boolean isWarning) {
        this.isError = isError;
        this.isWarning = isWarning;
        errorList = new ArrayList<String>();
        warningList = new ArrayList<String>();
        resultAttributes = new ArrayList<String>();
    }

    public Boolean isError() {
        return isError;
    }

    public void setError(Boolean error) {
        isError = error;
    }

    public List<String> getErrorList() {
        return errorList;
    }

    public void setErrorList(List<String> errorList) {
        this.errorList = errorList;
    }

    public List<String> getWarningList() {
        return warningList;
    }

    public void setWarningList(List<String> warningList) {
        this.warningList = warningList;
    }

    public Boolean getWarning() {
        return isWarning;
    }

    public void setWarning(Boolean warning) {
        isWarning = warning;
    }

    public List<String> getResultAttributes() {
        return resultAttributes;
    }

    public void setResultAttributes(List<String> resultAttributes) {
        this.resultAttributes = resultAttributes;
    }

    public Boolean getError() {
        return isError;
    }

    public void addResultIdentifier(String identifier){
        resultAttributes.add(identifier);
    }

}
