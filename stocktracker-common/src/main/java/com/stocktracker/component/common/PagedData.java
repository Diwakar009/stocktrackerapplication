/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.component.common;

import java.util.List;

/**
 * Created by diwakar009 on 10/11/2018.
 */
public class PagedData<T> {

    List<T> records;
    Long totalNoOfRecords;


    public List<T> getRecords() {
        return records;
    }

    public void setRecords(List<T> records) {
        this.records = records;
    }

    public Long getTotalNoOfRecords() {
        return totalNoOfRecords;
    }

    public void setTotalNoOfRecords(Long totalNoOfRecords) {
        this.totalNoOfRecords = totalNoOfRecords;
    }
}
