/*
 * Copyright (c) 2020.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.exceptions;

import com.stocktracker.exceptions.TechnicalException;

public class ExcelCellRenderingException extends TechnicalException {
    public ExcelCellRenderingException(String s) {
        super(s);
    }
}
