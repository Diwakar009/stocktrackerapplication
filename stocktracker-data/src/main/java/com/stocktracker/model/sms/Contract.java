/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.model.sms;

import com.stocktracker.framework.IEntity;
import com.stocktracker.model.BaseEntity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * The persistent class for the TBL_CONTRACT database table.
 * 
 */
@Entity
@Table(name = "TBL_CONTRACT")
@AttributeOverride(name = "id", column = @Column(name = "ID", nullable = false,
		columnDefinition = "BIGINT UNSIGNED"))
public class Contract extends BaseEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	public Contract() {
	}

	public Contract(Long id, String industryCode, Date startDate,
					Date endDate, String contractTypeCode,
					String contractName,
					String rawMaterial,
					Double approvedRawMaterialQuantity,
					Double receivedRawMaterialQuantity,
					String deliveryProduct,
					Double productDeliveryQuantity,
					Double productDeliveredQuantity,
					String status) {
		this.id = id;
		this.industryCode = industryCode;
		this.startDate = startDate;
		this.endDate = endDate;
		this.contractTypeCode = contractTypeCode;
		this.contractName = contractName;
		this.deliveryProduct = deliveryProduct;
		this.rawMaterial = rawMaterial;
		this.approvedRawMaterialQuantity = approvedRawMaterialQuantity;
		this.receivedRawMaterialQuantity = receivedRawMaterialQuantity;
		this.productDeliveryQuantity = productDeliveryQuantity;
		this.productDeliveredQuantity = productDeliveredQuantity;
		this.status = status;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "ID", nullable = false, columnDefinition = "BIGINT UNSIGNED")
	private Long id;

	@Column(name="INDUSTRY_CODE", length = 5)
	private String industryCode;

	@Column(name = "START_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date startDate;

	@Column(name = "END_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date endDate;

	@Column(name="CONTRACT_TYPE", length = 5)
	private String contractTypeCode;

	@Column(name="CONTRACT_NAME", length = 200)
	private String contractName;

	@Column(name="RAW_MATERIAL", length = 5)
	private String rawMaterial;

	@Column(name="APPR_RAW_MATRL_QTY", nullable = true, columnDefinition = "DECIMAL UNSIGNED")
	private Double approvedRawMaterialQuantity;

	@Column(name="RECV_RAW_MATRL_QTY", nullable = true, columnDefinition = "DECIMAL UNSIGNED")
	private Double receivedRawMaterialQuantity;

	@Column(name="DELIVERY_PRODUCT", length = 5)
	private String deliveryProduct;

	@Column(name="PRODUCT_DELIVERY_QTY", nullable = true, columnDefinition = "DECIMAL UNSIGNED")
	private Double productDeliveryQuantity;

	@Column(name="PRODUCT_DELIVERED_QTY", nullable = true, columnDefinition = "DECIMAL UNSIGNED")
	private Double productDeliveredQuantity;

	@Column(name="REMARKS", length = 1000)
	private String remarks;

	@Column(name="STATUS", length = 5)
	private String status;

	@Column(name="MORE_DETAILS", length = 65535)
	private String moreDetails;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIndustryCode() {
		return industryCode;
	}

	public void setIndustryCode(String industryCode) {
		this.industryCode = industryCode;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getContractTypeCode() {
		return contractTypeCode;
	}

	public void setContractTypeCode(String contractTypeCode) {
		this.contractTypeCode = contractTypeCode;
	}

	public String getContractName() {
		return contractName;
	}

	public void setContractName(String contractName) {
		this.contractName = contractName;
	}

	public String getDeliveryProduct() {
		return deliveryProduct;
	}

	public void setDeliveryProduct(String deliveryProduct) {
		this.deliveryProduct = deliveryProduct;
	}

	public String getRawMaterial() {
		return rawMaterial;
	}

	public void setRawMaterial(String rawMaterial) {
		this.rawMaterial = rawMaterial;
	}

	public Double getApprovedRawMaterialQuantity() {
		return approvedRawMaterialQuantity;
	}

	public void setApprovedRawMaterialQuantity(Double approvedRawMaterialQuantity) {
		this.approvedRawMaterialQuantity = approvedRawMaterialQuantity;
	}

	public Double getReceivedRawMaterialQuantity() {
		return receivedRawMaterialQuantity;
	}

	public void setReceivedRawMaterialQuantity(Double receivedRawMaterialQuantity) {
		this.receivedRawMaterialQuantity = receivedRawMaterialQuantity;
	}

	public Double getProductDeliveryQuantity() {
		return productDeliveryQuantity;
	}

	public void setProductDeliveryQuantity(Double productDeliveryQuantity) {
		this.productDeliveryQuantity = productDeliveryQuantity;
	}

	public Double getProductDeliveredQuantity() {
		return productDeliveredQuantity;
	}

	public void setProductDeliveredQuantity(Double productDeliveredQuantity) {
		this.productDeliveredQuantity = productDeliveredQuantity;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMoreDetails() {
		return moreDetails;
	}

	public void setMoreDetails(String moreDetails) {
		this.moreDetails = moreDetails;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	@Override
	public Serializable getEntityPK() {
		return getId();
	}

	@Override
	public void copyEntity(IEntity entity) {
		Contract source = (Contract)entity;
		this.setCreatedBy(source.getCreatedBy());
		this.setUpdatedBy(source.getUpdatedBy());
		this.setCreatedAt(source.getCreatedAt());
		this.setUpdatedAt(source.getUpdatedAt());
		this.setId(source.getId());
		this.setIndustryCode(source.getIndustryCode());
		this.setStartDate(source.getStartDate());
		this.setEndDate(source.getEndDate());
		this.setContractTypeCode(source.getContractTypeCode());
		this.setContractName(source.getContractName());
		this.setDeliveryProduct(source.getDeliveryProduct());
		this.setRawMaterial(source.getRawMaterial());
		this.setApprovedRawMaterialQuantity(source.getApprovedRawMaterialQuantity());
		this.setReceivedRawMaterialQuantity(source.getReceivedRawMaterialQuantity());
		this.setProductDeliveryQuantity(source.getProductDeliveryQuantity());
		this.setProductDeliveredQuantity(source.getProductDeliveredQuantity());
		this.setRemarks(source.getRemarks());
		this.setStatus(source.getStatus());
		this.setMoreDetails(source.getMoreDetails());
	}
}