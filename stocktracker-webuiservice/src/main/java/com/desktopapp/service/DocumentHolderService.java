package com.desktopapp.service;

import java.util.ArrayList;

import com.stocktracker.domainobjs.json.DocHolderDO;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

/**
 * Document Holder Service
 *
 * @author Ratnala Diwakar Choudhury
 */
@Service
public class DocumentHolderService extends AbstractService<DocHolderDO> {
	
    final static Logger logger = Logger.getLogger(DocumentHolderService.class);

	public DocumentHolderService() {
        super(DocHolderDO.class);
    }

	/**
	 * Remove entity.
	 *
	 * @param entity entity model
	 */
	public void removeHolder(DocHolderDO entity) {
		remove(entity);
	}

	/**
	 * Remove entities.
	 *
	 * @param entities entity model
	 */
	public void removeHolders(ArrayList<DocHolderDO> entities) {
		remove(entities);
	}
	
	public static final String BASE_RESOURCE = "documentHolder";
		
	@Override
	protected String getBaseResource() {
		return BASE_RESOURCE;
	}

}
