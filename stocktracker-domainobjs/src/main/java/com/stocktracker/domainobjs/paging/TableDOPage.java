/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.domainobjs.paging;

import com.stocktracker.domainobjs.json.DOPage;
import com.stocktracker.domainobjs.json.TableRowMapDO;

public class TableDOPage extends DOPage<TableRowMapDO<String,String>>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
