package com.desktopapp.jsonobject;

import com.stocktracker.framework.IEntity;
import com.stocktracker.model.BaseEntity;
import com.google.gson.annotations.Expose;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public abstract class BaseObjectJSON implements IJSONObject, Serializable {

    private static final long serialVersionUID = 8777540776806878929L;

    protected Map<String, Object> argumentMap = new HashMap<String, Object>();

    protected IEntity entity;

    @Expose
    private Date createdAt;

    @Expose
    private String createdBy;

    @Expose
    private Date updatedAt;

    @Expose
    private String updatedBy;


    public BaseObjectJSON(Map<String, Object> argumentMap, IEntity entity) {
        super();

        this.entity = entity;
        this.argumentMap = argumentMap;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }


    public void copyEntityToJSON() {
        if (entity instanceof BaseEntity) {
            BaseEntity baseEntity = (BaseEntity) entity;
            setCreatedAt(baseEntity.getCreatedAt());
            setCreatedBy(baseEntity.getCreatedBy());
            setUpdatedAt(baseEntity.getUpdatedAt());
            setUpdatedBy(baseEntity.getUpdatedBy());
        }
    }

    public void copyJSONToEntity() {

        if (entity instanceof BaseEntity) {
            BaseEntity baseEntity = (BaseEntity) entity;
            baseEntity.setCreatedAt(getCreatedAt());
            baseEntity.setCreatedBy(getCreatedBy());
            baseEntity.setUpdatedAt(getUpdatedAt());
            baseEntity.setUpdatedBy(getUpdatedBy());
        }

    }

    public IEntity getEntity() {
        return entity;
    }

    public void setEntity(IEntity entity) {
        this.entity = entity;
    }

    public Map<String, Object> getArgumentMap() {
        return argumentMap;
    }

    public void setArgumentMap(Map<String, Object> argumentMap) {
        this.argumentMap = argumentMap;
    }


}
