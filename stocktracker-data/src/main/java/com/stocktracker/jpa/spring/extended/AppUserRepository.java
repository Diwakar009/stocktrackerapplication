package com.stocktracker.jpa.spring.extended;

import com.stocktracker.jpa.spring.extended.custom.ExtendedRepository;
import com.stocktracker.model.AppUser;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface AppUserRepository extends ExtendedRepository<AppUser, Long> {
	
	@Query(value="select distinct d from AppUser as d left outer join d.appUserLogins as c where d.firstName LIKE :firstName OR d.lastName LIKE :lastName OR d.shortName like :shortName OR d.phone like :phone OR d.email like :email or c.userName LIKE :userName ORDER BY d.shortName")
	public List<AppUser> findByUserLikeAll(@Param("firstName") String firstName,
                                           @Param("lastName") String lastName,
                                           @Param("shortName") String shortName,
                                           @Param("phone") String phone,
                                           @Param("email") String email,
                                           @Param("userName") String userName);

	@Query(value="select distinct d from AppUser as d left outer join d.appUserLogins as c where d.firstName LIKE :firstName OR d.lastName LIKE :lastName OR d.shortName like :shortName OR d.phone like :phone OR d.email like :email or c.userName LIKE :userName ORDER BY d.shortName")
	public Page<AppUser> findByUserLikeAll(@Param("firstName") String firstName,
                                           @Param("lastName") String lastName,
                                           @Param("shortName") String shortName,
                                           @Param("phone") String phone,
                                           @Param("email") String email,
                                           @Param("userName") String userName
            , Pageable pagable);
}
