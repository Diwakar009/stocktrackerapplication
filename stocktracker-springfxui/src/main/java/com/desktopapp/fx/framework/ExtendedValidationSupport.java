/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.desktopapp.fx.framework;

import org.controlsfx.validation.ValidationSupport;

/**
 * Extended validation support for having more control on front end error's
 *
 */
public class ExtendedValidationSupport extends ValidationSupport {

    public ExtendedValidationSupport() {
        super();
        setErrorDecorationEnabled(false);
    }

    @Override
    public Boolean isInvalid() {
        setErrorDecorationEnabled(true);
        initInitialDecoration();
        return super.isInvalid();
    }
}
