/*
 * Copyright (c) 2020.
 *
 * All Rights are with Diwakar C
 */

package com.desktopapp.service;

import com.desktopapp.service.config.AppConfig;
import com.stocktracker.domainobjs.json.PreferencesDO;
import com.stocktracker.jpa.spring.extended.config.ExtendedRepositoryConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by diwakar009 on 17/12/2018.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = { AppConfig.class, ExtendedRepositoryConfig.class})
@Transactional
public class PreferenceServiceTest {
    @Autowired
    private PreferencesService preferencesService;

    @Test
    public void testSampleService() {
        PreferencesDO preferencesDO = preferencesService.find(1l);
        System.out.println("*************** " +preferencesDO.getAppTheme());
    }

    @Test
    @Rollback(true)
    public void uppdatePreferenceService() {
        PreferencesDO preferencesDO = preferencesService.find(1l);
        preferencesDO.setShowTipOfTheDay(true);
        preferencesService.update(preferencesDO);
        preferencesDO = preferencesService.find(1l);
        System.out.println("############ " +preferencesDO.isShowTipOfTheDay());
    }

}
