/*
 * Copyright (c) 2020.
 *
 * All Rights are with Diwakar C
 */

package com.desktopapp.fx.mvc;


import com.desktopapp.fx.DesktopFxApplication;
import com.desktopapp.fx.app.AppAccelerator;
import com.desktopapp.fx.common.IAccelerator;
import com.desktopapp.fx.common.IStackedScreenProperties;
import com.desktopapp.fx.common.ITableDataViewBehaviour;
import com.desktopapp.fx.common.PageDetails;
import com.desktopapp.fx.control.Pager;
import com.desktopapp.fx.control.TableDataView;
import com.desktopapp.fx.control.multiscreen.StackedScreen;
import com.stocktracker.domainobjs.json.BaseDomainObject;
import com.stocktracker.domainobjs.json.DOPage;
import com.stocktracker.domainobjs.json.TableRowMapDO;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.*;
import javafx.scene.layout.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * Abstract data page view
 *
 * @param <T> entity
 *
 * @author R Diwakar C
 */
public abstract class AbstractDataPageView<T extends BaseDomainObject> implements DataPageView<T> ,IStackedScreenProperties, ITableDataViewBehaviour<T> {

    private static final String FORM_NAME = "DataPage";
    private final Logger logger = LoggerFactory.getLogger(AbstractDataPageView.class);
    protected DataPageController<T> controller;
    private StackedScreen stackedScreen;
    private AbstractPreviewView previewView;
    private TableDataView tableDataView;
    /**
     * init process
     *
     * @param controller The data page controller
     */
    @Override
    public void init(DataPageController<T> controller) {
        logger.debug("init data page");
        this.controller = controller;
        buildView();
    }

    @Override
    public DataPageController<T> getController() {
        return controller;
    }

    private void buildView() {
        logger.debug("build data page view");
        stackedScreen = new StackedScreen();
        tableDataView = new TableDataView<>(this);

        StackPane stackPane = new StackPane();
        BorderPane borderPane = new BorderPane();
        /*borderPane.setTop(applicationInfoPane); // Error and Info Panel*/
        borderPane.setCenter(renderCenter());
        /* stackPane.getChildren().add(renderCenter());*/
        stackPane.getChildren().add(borderPane);
        stackedScreen.addScreen(stackPane, (IStackedScreenProperties) this);
    }

    public Boolean hasPrintActions() {
        return false;
    }

    protected Node renderCenter(){
        BorderPane dataPageBroderView = new BorderPane();
        dataPageBroderView.getStyleClass().addAll("data-page-view");
        /*dataPageBroderView.setTop(getHeaderBar());
        dataPageBroderView.setCenter(getTablePane());*/

        dataPageBroderView.setCenter(tableDataView);

        dataPageBroderView.setBottom(buildButtom());
        return dataPageBroderView;
    }


    protected Node buildButtom(){
        previewView = getPreviewView();
        if (previewView != null) {
            return  previewView.asNode();
        }
        return null;
    }

    public void setDataPageAccelerators() {
        logger.debug("set data page view accelerators");
        Scene scene = stackedScreen.getScene();
        if (scene == null) {
            throw new IllegalArgumentException("setDataPageAccelerators must be called when the DataPageView is " +
                    "attached to a scene");
        }

        IStackedScreenProperties properties = stackedScreen.getActiveScreenProperties();
        AppAccelerator appAccelerator = DesktopFxApplication.getLoginSession().getAppAccelerator();
        if(properties.getAccelerator().getAccelerators() != null){
            Map<String, Map<KeyCodeCombination, Runnable>> accelerators = properties.getAccelerator().getAccelerators();
            if(accelerators != null) {
                accelerators.entrySet().forEach((accelerator) -> {
                    accelerator.getValue().forEach((keyCodeCombination, runnable) -> {
                        //scene.getAccelerators().put(keyCodeCombination, runnable);
                        appAccelerator.activateAccelerator(scene,keyCodeCombination, runnable);
                    });
                });
            }
        }
    }

   /**
     * If the data page view has preview, this method should be overridden.
     * Data page view has no default preview.
     *
     * @return preview view if exist, otherwise null.
     */
    public AbstractPreviewView getPreviewView() {
        return null;
    }

    /**
     * If the selected model of the {@link TableView} changed, notifies the preview view and
     * preview view shows new data values.
     */
    private void notifyPreviewView() {
        logger.debug("notify preview view");
        if (getSelectedModel() != null) {
            previewView.setEntity(getSelectedModel());
        }
    }
    /**
     * Refreshs the data of the {@link TableView} and {@link Pager}
     */
    @Override
    public void refreshData() {
        tableDataView.refreshData();
    }

    protected Map<KeyCodeCombination,Runnable> getDataPageKeyCodeCombination() {
        /**
         * Add modify delete accelerators
         */
        Map<KeyCodeCombination,Runnable> pageKeyCodeCombinationMap = new HashMap<KeyCodeCombination,Runnable>(){{
            put(new KeyCodeCombination(KeyCode.N, KeyCombination.SHORTCUT_DOWN), tableDataView.getBtnAddNew()::fire);
            put(new KeyCodeCombination(KeyCode.E, KeyCombination.SHORTCUT_DOWN), tableDataView.getBtnEdit()::fire);
            put(new KeyCodeCombination(KeyCode.D, KeyCombination.SHORTCUT_DOWN), tableDataView.getBtnDelete()::fire);
        }};

        return pageKeyCodeCombinationMap;
    }

    protected Map<KeyCodeCombination,Runnable> getPrintKeyCodeCombination() {
        /**
         * Print key code accelerators
         */
        Map<KeyCodeCombination,Runnable> printKeyCodeCombinationMap = new HashMap<KeyCodeCombination,Runnable>(){{
            put(new KeyCodeCombination(KeyCode.R, KeyCombination.SHORTCUT_DOWN),tableDataView.getBtnPrintPreview()::fire);
            put(new KeyCodeCombination(KeyCode.P, KeyCombination.SHORTCUT_DOWN),tableDataView.getBtnPrint()::fire);
            put(new KeyCodeCombination(KeyCode.F, KeyCombination.SHORTCUT_DOWN),tableDataView.getBtnPdf()::fire);
        }};

        return printKeyCodeCombinationMap;
    }

    @Override
    public T getSelectedModel() {
        logger.debug("get tableview selected model");
        return (T)tableDataView.getTableView().getSelectionModel().getSelectedItem();
    }

    @Override
    public Node asNode() {
        return stackedScreen;
    }


    public StackedScreen getStackedScreen() {
        return stackedScreen;
    }

    @Override
    public IAccelerator getAccelerator() {
        return new IAccelerator() {
            @Override
            public Map<String, Map<KeyCodeCombination, Runnable>> getAccelerators() {
                Map<String,Map<KeyCodeCombination, Runnable>> combinations = new HashMap<String,Map<KeyCodeCombination, Runnable>>();
                Map<KeyCodeCombination,Runnable> childAccelerators = getDataPageKeyCodeCombination();
                if (hasPrintActions()) {
                    childAccelerators.putAll(getPrintKeyCodeCombination());
                }
                combinations.put(IAccelerator.CHILD_ACCELERATORS,childAccelerators);
                combinations.put(IAccelerator.PARENT_ACCELERATORS,new HashMap<KeyCodeCombination,Runnable>());

                return combinations;
            }
        };
    }

    @Override
    public void disposeScreen() {
        stackedScreen.removeScreen();
    }

    /**
     * Call on click on addNew
     *
     */
    public void onAddNew(){
        getController().onAddNew();
    }

    /**
     * Call on click onEdit
     *
     */
    public void onEdit(){
        getController().onEdit();
    }

    /**
     * Call on click onDelete
     *
     */
    public void onDelete(){
        getController().onDelete();
    }

    /**
     * Call on click onPrintPreview
     *
     */
    public void onPrintPreview(){
        getController().onPrintPreview();
    }

    /**
     * Call on click onPrint
     *
     */
    public void onPrint(){
        getController().onPrint();
    }

    /**
     * Call on click onPdf
     *
     */
    public void onPdf(){
        getController().onPdf();
    }


    @Override
    public DOPage<TableRowMapDO<String, String>> getData(String filter, PageDetails details) {
        return getController().getData(filter,details);
    }

    @Override
    public void onSelectedItemChanged() {
        if (previewView != null) {
            notifyPreviewView();
        }
    }


    @Override
    public Boolean hasAddNewActions() {
        return true;
    }

    @Override
    public Boolean hasEditActions() {
        return true;
    }

    @Override
    public Boolean hasDeleteActions() {
        return true;
    }

    @Override
    public Boolean hasSearchActions() {
        return true;
    }


    public TableDataView getTableDataView() {
        return tableDataView;
    }

}
