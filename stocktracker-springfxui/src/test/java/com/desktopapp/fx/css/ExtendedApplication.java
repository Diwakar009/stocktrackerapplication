package com.desktopapp.fx.css;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
 
/**
 *
 * @author jojorabbit
 */
public abstract class ExtendedApplication extends Application {
 
    protected double DEFAULT_APP_WIDTH = 500;
    protected double DEFAULT_APP_HEIGHT = 400;
    protected Stage stage;
    protected Pane root;
    protected Scene scene;
 
    @Override
    public void start(Stage primaryStage) {
        preSetup(primaryStage);
        setup();
        postSetup();
    }
 
    protected abstract void setup();
 
    protected void preSetup(Stage primaryStage) {
        stage = primaryStage;
        stage.setTitle(getAppTitle());
        root = new StackPane();
        root.getStyleClass().add("root-pane");
        scene = new Scene(root, getAppWidth(), getAppHeight());
        stage.setScene(scene);
        setupCss();
    }
 
    protected void postSetup() {
        stage.centerOnScreen();
        stage.show();
        // stage.setVisible(true);
    }
 
    // override to change APP WIDTH
    protected double getAppWidth() {
        return DEFAULT_APP_WIDTH;
    }
 
    // override to change APP HEIGHT
    protected double getAppHeight() {
        return DEFAULT_APP_HEIGHT;
    }
 
    // override to set App Title
    protected String getAppTitle() {
        return "Test";
    }
 
    // override to add CSS styles
    protected void setupCss() {
    }
}