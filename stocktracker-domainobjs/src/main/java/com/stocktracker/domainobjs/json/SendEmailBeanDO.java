/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.domainobjs.json;

import java.util.List;

public class SendEmailBeanDO {

	private List<String> toList;
	private List<String> ccList;
	private List<String> bccList;
	private String subject;
	private String body;
	private List<TableRowMapDO> ids;
	
	
	public SendEmailBeanDO() {
		super();
	}

	public SendEmailBeanDO(List<String> toList, String subject, String body,
			List<TableRowMapDO> ids) {
		super();
		this.toList = toList;
		this.subject = subject;
		this.body = body;
		this.ids = ids;
	}

	public SendEmailBeanDO(List<String> toList, List<String> ccList,
			List<String> bccList, String subject, String body,
			List<TableRowMapDO> ids) {
		super();
		this.toList = toList;
		this.ccList = ccList;
		this.bccList = bccList;
		this.subject = subject;
		this.body = body;
		this.ids = ids;
	}
	
	public List<String> getToList() {
		return toList;
	}
	public void setToList(List<String> toList) {
		this.toList = toList;
	}
	public List<String> getCcList() {
		return ccList;
	}
	public void setCcList(List<String> ccList) {
		this.ccList = ccList;
	}
	public List<String> getBccList() {
		return bccList;
	}
	public void setBccList(List<String> bccList) {
		this.bccList = bccList;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public List<TableRowMapDO> getIds() {
		return ids;
	}
	public void setIds(List<TableRowMapDO> ids) {
		this.ids = ids;
	}
	
	
}
