/*
 * Copyright (C) 2017 Cem Ikta
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 *
 */

package com.desktopapp.fx.control;

import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.util.StringConverter;
import javafx.util.converter.IntegerStringConverter;
import org.apache.commons.lang.StringUtils;

import java.util.function.UnaryOperator;
import java.util.regex.Pattern;

/**
 * Integer field control
 *
 * @see TextField
 * @see IntegerStringConverter
 * @see TextFormatter
 * @see UnaryOperator
 *
 * @author R Diwakar C
 */
public class IntegerField extends TextFieldExt {

    public IntegerField() {
        super();
        StringConverter<Integer> integerStringConverter = new IntegerStringConverter();
        setTextFormatter(new TextFormatter<>(integerStringConverter, null, new NumberUnaryOperator()));
        setOnAction(event -> integerStringConverter.fromString(this.getText()));
    }

    public void setDataValue(Integer value) {
        super.setDataValue(value.toString());
    }

    public Long getValue()
    {
        if(!StringUtils.isBlank(this.getText())){
            return Long.valueOf(this.getText());
        }
        return null;
    }
}
