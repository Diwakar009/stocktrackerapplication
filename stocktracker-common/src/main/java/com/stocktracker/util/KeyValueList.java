/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.util;

import java.util.AbstractCollection;
import java.util.AbstractMap;
import java.util.AbstractSet;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Set;


/**
 * Represents an immutable, singaly -linked list of (@link KeyValuePair)s.
 * This class is a very lightweight list impelemntation that exists primarily 
 * to provide immutable representation of key-value lists
 * 
 * @author "Diwakar Choudhury"
 *
 */
public final class KeyValueList extends AbstractCollection<KeyValuePair> {

	private final KeyValuePair head;
	private final KeyValueList tail;

	public static final KeyValueList EMPTY = new KeyValueList();

	@Deprecated
	public KeyValueList() {
		this.head = null;
		this.tail = null;
	}

	public KeyValueList(KeyValuePair head, KeyValueList tail) {
		this.head = Objects.requireNonNull(head,
				"Null 'head' argument passed to keyValueList constructor");
		this.tail = (tail == null ? EMPTY : tail);
	}

	public KeyValueList(String key, Object value, KeyValueList tail) {
		this(new KeyValuePair(key, value), tail);
	}

	public static KeyValueList fromArray(KeyValuePair... entries) {
		KeyValueList result = KeyValueList.EMPTY;

		for (int i = entries.length - 1; i >= 0; i--) {
			result = new KeyValueList(
					Objects.requireNonNull(entries[i],
							"Null entry in entry list passed to KeyValueList.from...()"),
					result);
		}

		return result;
	}

	public static KeyValueList fromList(List<KeyValuePair> entries) {
		KeyValueList result = KeyValueList.EMPTY;
		ListIterator<KeyValuePair> iter = entries.listIterator(entries.size());

		while (iter.hasPrevious()) {
			result = new KeyValueList(
					Objects.requireNonNull(iter.previous(),
							"Null entry in entry list passed to KeyValueList.from...()"),
					result);
		}

		return result;
	}

	public static KeyValueList fromCollection(Collection<KeyValuePair> entries) {
		return (entries instanceof List<?> ? fromList((List<KeyValuePair>) entries)
				: fromArray(entries.toArray(new KeyValuePair[entries.size()])));
	}

	public static KeyValueList fromMap(Map<String, ?> entries) {

		@SuppressWarnings("unchecked")
		Map.Entry<String, ?>[] entryArray = entries.entrySet().toArray(
				new Map.Entry[entries.size()]);
		KeyValueList result = KeyValueList.EMPTY;

		for (int i = entryArray.length - 1; i >= 0; i--) {
			result = new KeyValueList(new KeyValuePair(entryArray[i].getKey(),
					entryArray[i].getValue()), result);
		}

		return result;
	}

	@Override
	public boolean isEmpty() {
		return (tail == null);
	}

	@Override
	public int size() {
		int size = 0;
		for (KeyValueList kvl = this; !kvl.isEmpty(); kvl = kvl.tail) {
			size += 1;
		}
		return size;
	}

	@Override
	public Iterator<KeyValuePair> iterator() {
		// TODO Auto-generated method stub
		return new EntriesIterator(this);
	}

	private static abstract class AbstractKeyValueListIterator<T> implements
			Iterator<T> {

		private KeyValueList current;

		protected AbstractKeyValueListIterator(KeyValueList start) {
			this.current = Objects
					.requireNonNull(start,
							"Null 'start' argument passed to AbstractKeyValueListIterator constructor");
		}

		@Override
		public boolean hasNext() {
			return (!current.isEmpty());
		}

		@Override
		public T next() {
			KeyValueList current = this.current;
			if (current.isEmpty()) {
				throw new NoSuchElementException(
						"Attempt to iterate past the end of a KeyValueList");
			}

			this.current = current.tail;
			return access(current.head);
		}

		@Override
		public void remove() {
			throw new UnsupportedOperationException(
					"Unsupported call to Iterator.remove()");
			// Iterator.super.remove();
		}

		protected abstract T access(KeyValuePair entry);

	}

	private static final class KeysIterator extends
			AbstractKeyValueListIterator<String> {
		public KeysIterator(KeyValueList keyValueList) {
			super(keyValueList);
		}

		protected String access(KeyValuePair entry) {
			return entry.getKey();
		}
	}

	private static final class ValuesIterator extends
			AbstractKeyValueListIterator<Object> {
		public ValuesIterator(KeyValueList keyValueList) {
			super(keyValueList);
		}

		protected Object access(KeyValuePair entry) {
			return entry.getValue();
		}
	}

	private static final class EntriesIterator extends
			AbstractKeyValueListIterator<KeyValuePair> {

		public EntriesIterator(KeyValueList keyValueList) {
			super(keyValueList);
		}

		protected KeyValuePair access(KeyValuePair entry) {
			return entry;
		}

	}

	private static final class MapEntriesIterator extends
			AbstractKeyValueListIterator<Map.Entry<String, Object>> {

		public MapEntriesIterator(KeyValueList keyValueList) {
			super(keyValueList);
		}

		protected KeyValuePair access(KeyValuePair entry) {
			return entry;
		}

	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder(100 * size());

		sb.append("[");

		for (KeyValueList kvl = this; !kvl.isEmpty(); kvl = kvl.tail) {
			if (kvl != this) {
				sb.append(", ");
			}
			sb.append(kvl.head.getKey()).append('=')
					.append(kvl.head.getValue());
		}

		sb.append("]");

		return sb.toString();

	}

	public Map<String, ?> asMap() {
		return new AbstractMap<String, Object>() {

			@Override
			public Set<Map.Entry<String, Object>> entrySet() {
				// TODO Auto-generated method stub
				return new AbstractSet<Map.Entry<String, Object>>() {

					@Override
					public Iterator<java.util.Map.Entry<String, Object>> iterator() {
						return new MapEntriesIterator(KeyValueList.this);
					}

					@Override
					public int size() {
						// TODO Auto-generated method stub
						return KeyValueList.this.size();
					}

				};
			}
		};
	}

	public Collection<Object> getValues() {
		return new AbstractCollection<Object>() {

			@Override
			public Iterator<Object> iterator() {
				return new ValuesIterator(KeyValueList.this);
			}

			@Override
			public int size() {
				return KeyValueList.this.size();
			}
		};
	}

	public Collection<String> getKeys() {

		return new AbstractCollection<String>() {

			@Override
			public Iterator<String> iterator() {
				return new KeysIterator(KeyValueList.this);
			}

			@Override
			public int size() {
				return KeyValueList.this.size();
			}
		};
	}

	public <T> T get(String key, Class<T> expectedType) {
		Object value = get(key);

		try {
			return expectedType.cast(value);

		} catch (ClassCastException e) {
			throw new ClassCastException("key '" + key + "' has value  '"
					+ "wrong type");
		}

	}

	public Object get(String key) {
		for (KeyValueList kvl = this; !kvl.isEmpty(); kvl = kvl.tail) {
			if (kvl.head.getKey().equalsIgnoreCase(key)) {
				return kvl.head.getValue();
			}
		}
		return null;
	}

	
	public boolean containsKey(String key){
		for (KeyValueList kvl = this; !kvl.isEmpty(); kvl = kvl.tail) {
			if (kvl.head.getKey().equalsIgnoreCase(key)) {
				return true;
			}
		}	
		return false;
	}
	
	public boolean containsValue(Object value) {
		for (KeyValueList kvl = this; !kvl.isEmpty(); kvl = kvl.tail) {

			if (Objects.equals(kvl.head.getValue(), value)) {

				return true;
			}
		}
		return false;
	}

	public Boolean getBoolean(String key) {
		return get(key, Boolean.class);
	}

	public String getString(String key) {
		return get(key, String.class);
	}

	public Number getNumber(String key) {
		return get(key, Number.class);
	}

	public Date getDate(String key) {
		return get(key, Date.class);
	}

}
