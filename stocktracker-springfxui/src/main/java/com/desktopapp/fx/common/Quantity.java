/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.desktopapp.fx.common;

import java.math.BigDecimal;

/**
 * Created by diwakar009 on 1/11/2019.
 */
public class Quantity {

    private BigDecimal numberOfPackets;
    private BigDecimal weightment;

    public Quantity() {
    }

    public Quantity(BigDecimal numberOfPackets) {
        this.numberOfPackets = numberOfPackets;
        this.weightment = numberOfPackets.multiply(new BigDecimal(0.5));
    }

    public Quantity(BigDecimal numberOfPackets, BigDecimal weightment) {
        this.numberOfPackets = numberOfPackets;
        this.weightment = weightment;
    }

    public BigDecimal getNumberOfPackets() {
        return numberOfPackets;
    }

    public void setNumberOfPackets(BigDecimal numberOfPackets) {
        this.numberOfPackets = numberOfPackets;
    }

    public BigDecimal getWeightment() {
        return weightment;
    }

    public void setWeightment(BigDecimal weightment) {
        this.weightment = weightment;
    }

    @Override
    public String toString() {
        return "Quantity{" +
                "numberOfPackets=" + numberOfPackets +
                ", weightment=" + weightment +
                '}';
    }
}
