package com.stocktracker.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import com.stocktracker.framework.ICompositeEntityPK;

/**
 * The primary key class for the static_code_decode database table.
 * 
 */
@Embeddable
public class StaticCodeDecodePK implements Serializable,ICompositeEntityPK {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="CODE_NAME", length = 50)
	private String codeName;
	
	@Column(name="LANGUAGE", length = 5)
	private String language;

	@Column(name="CODE_VALUE", length = 50)
	private String codeValue;

	@Column(name="CODE_VALUE_FILTER", length = 50)
	private String codeValueFilter;


	public StaticCodeDecodePK() {
	}
	public String getCodeName() {
		return this.codeName;
	}
	public void setCodeName(String codeName) {
		this.codeName = codeName;
	}
	public String getLanguage() {
		return this.language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public String getCodeValue() {
		return this.codeValue;
	}
	public void setCodeValue(String codeValue) {
		this.codeValue = codeValue;
	}

	public String getCodeValueFilter() {
		return codeValueFilter;
	}

	public void setCodeValueFilter(String codeValueFilter) {
		this.codeValueFilter = codeValueFilter;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		StaticCodeDecodePK that = (StaticCodeDecodePK) o;

		if (!codeName.equals(that.codeName)) return false;
		if (!language.equals(that.language)) return false;
		if (!codeValue.equals(that.codeValue)) return false;
		return codeValueFilter != null ? codeValueFilter.equals(that.codeValueFilter) : that.codeValueFilter == null;
	}


}