package com.desktopapp.fx.manager;

import com.desktopapp.fx.DesktopFxApplication;
import com.desktopapp.fx.app.AppFeatures;
import com.desktopapp.fx.common.StageViewBean;
import com.desktopapp.fx.controller.AppUserLoginController;
import com.desktopapp.fx.util.I18n;
import com.desktopapp.fx.util.ResourceUtils;
import com.desktopapp.fx.view.LoginView;
import com.stocktracker.util.PasswordHash;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import org.controlsfx.control.Notifications;
import com.stocktracker.domainobjs.json.AppUserLoginDO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

/**
 * Created by diwakar009 on 13/12/2018.
 */
@Component
@Scope("prototype")
public class LoginManager {

    private final Logger logger = LoggerFactory.getLogger(LoginManager.class);

    private StageViewBean stageViewBean;

    @Autowired
    private AppUserLoginController appUserLoginController;

    @Autowired
    private PreferenceManager preferenceManager;

    @Autowired
    private ApplicationViewManager applicationViewManager;

    public enum LoginStatus
    {
        SUCCESSFUL,
        UNSUCCESSFUL,
        WRONG_PASSWORD,
        INACTIVE,
        DEFAULT_USER_LOGIN,
        USER_NOT_FOUND;
    }

    public LoginManager() {
    }

    /**
     * Check Credential and Login
     *
     * @param username
     * @param password
     */
    private LoginStatus validateCredentials(String username, String password) {
        logger.info("validateCredentials process " + preferenceManager);
        String defaultUser = ResourceUtils.getConfiguration().getGlobalProperties().getDefaultUser();
        String accessLevel = "";

        if (defaultUser.equals(username)) { // if the username is default username

            String passwordHash = ResourceUtils.getConfiguration().getGlobalProperties().getDefaultPasswordHash();
            try {
                if(PasswordHash.validatePassword(password, passwordHash)){
                    accessLevel = ResourceUtils.getConfiguration().getGlobalProperties().getDefaultUserAccess();
                    //DesktopFxApplication.get().getLoginSession().setLoggedInUser(appUserLogin);
                    DesktopFxApplication.get().getLoginSession().setUserName(username);
                    DesktopFxApplication.get().getLoginSession().setAccessType(accessLevel);

                    return LoginStatus.DEFAULT_USER_LOGIN;
                }
            } catch (NoSuchAlgorithmException e1) {
                logger.error(e1.getMessage());
            } catch (InvalidKeySpecException e1) {
                logger.error(e1.getMessage());
            }

        }else{

            AppUserLoginDO appUserLogin = appUserLoginController.getAppUserLoginByUserName(username);
            if (appUserLogin == null) {
                return LoginStatus.USER_NOT_FOUND;
            }else if(false) { // TODO User active or inactive
                return LoginStatus.INACTIVE;
        	}else {
                try {
                    if (PasswordHash.validatePassword(password, appUserLogin.getPassword())) {

                        DesktopFxApplication.get().getLoginSession().setLoggedInUser(appUserLogin);
                        DesktopFxApplication.get().getLoginSession().setUserName(username);
                        DesktopFxApplication.get().getLoginSession().setAccessType(appUserLogin.getFnAccessLevel());

                        return LoginStatus.SUCCESSFUL;
                    } else {
                        return LoginStatus.WRONG_PASSWORD;
                    }
                }catch (NoSuchAlgorithmException e1) {
                    logger.error(e1.getMessage());
                } catch (InvalidKeySpecException e1) {
                    logger.error(e1.getMessage());
                }
            }

        }
        return LoginStatus.UNSUCCESSFUL;
    }

    /**
     * Show validateCredentials view
     *
     * @param onAppExit
     */
    public StageViewBean showLoginView(EventHandler<WindowEvent> onAppExit){
        Scene mainScene = new Scene(new LoginView(this), AppFeatures.WIDTH, AppFeatures.HEIGHT);
		mainScene.getStylesheets().add(getClass().getResource(DesktopFxApplication.get().getCurrentAppTheme().getThemeStyle()).toExternalForm());

        Stage mainStage = new Stage();
		mainStage.setTitle(I18n.COMMON.getString("app.title") + "( " + DesktopFxApplication.getLoginSession().getPreferences().getEnvironment() + " )");
		mainStage.getIcons().add(new Image(DesktopFxApplication.class.getResourceAsStream(AppFeatures.APP_ICON))); // to override it Appfeature update in APP_icon
		mainStage.setOnCloseRequest(onAppExit);
		mainStage.setScene(mainScene);
		mainStage.setMaximized(true);

        stageViewBean = new StageViewBean(mainStage,mainScene);
		//mainStage.show();
		return stageViewBean;
    }

    /**
     * login with username and password
     *
     * @param username
     * @param password
     */
    public void login(String username, String password) {
        LoginManager.LoginStatus loginStatus = validateCredentials(username,password);

        switch (loginStatus){

            case DEFAULT_USER_LOGIN:
            case SUCCESSFUL:

                //applicationViewManager.showAppView(stageViewBean.getStage(),stageViewBean.getScene());
                applicationViewManager.showAppView(stageViewBean);
                break;

            case WRONG_PASSWORD:

                Notifications.create()
                        .text(I18n.COMMON.getString("login.wrongPassword"))
                        .position(Pos.TOP_RIGHT).showInformation();

                break;

            case INACTIVE:

                Notifications.create()
                        .text(I18n.COMMON.getString("login.userNotActive"))
                        .position(Pos.TOP_RIGHT).showInformation();

                break;

            case USER_NOT_FOUND:

                Notifications.create()
                        .text(I18n.COMMON.getString("login.userNotFound"))
                        .position(Pos.TOP_RIGHT).showInformation();

                break;

            default:

                Notifications.create()
                        .text(I18n.COMMON.getString("login.loginFailed"))
                        .position(Pos.TOP_RIGHT).showInformation();

        }
    }



}
