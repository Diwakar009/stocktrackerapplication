package com.desktopapp.fx.control;

import com.desktopapp.fx.common.IGenericComponent;
import com.desktopapp.fx.control.listeners.ValueChangeListener;
import com.desktopapp.fx.controller.StaticCodeDecodeController;
import com.desktopapp.fx.objectfactory.ObjectFactory;
import com.desktopapp.fx.util.ViewHelpers;
import javafx.collections.FXCollections;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import com.stocktracker.domainobjs.json.CodeDataItemDO;

import java.util.*;


/**
 * CodeDecode for static code decode
 *
 * Created by diwakar009 on 3/12/2018.
 */
public class CodeDecodeField implements IGenericComponent{

    private String codeName;
    private String language;
    private TextFieldExt tfCodeValue;
    private ComboBoxExt<CodeDataItemDO> cbCodeDesc;
    public static final int LENGTH_CODEVALUE = 5;
    public static final double DEFAULT_COMBOBOX_PREFWIDTH = 200;
    public static final double DEFAULT_TEXTBOX_PREFWIDTH = 50;
    private boolean required;
    private List<CodeDecodeField> linkedCodeDecodeList;
    private Map<String,List<CodeDataItemDO>> cachedCodeDecodeData = new HashMap<>();
    public static CodeDataItemDO EMPTY_CODE_DECODE_ITEM = new CodeDataItemDO();
    public boolean skipCache = false;

    public final FilteredComboBoxDecorator.AutoCompleteComparator<CodeDataItemDO> AUTOCOMPLETE_CONTAINS_CHARS_IN_ORDER =
            (txt, obj) -> {
                return FilteredComboBoxDecorator.CONTAINS_CHARS_IN_ORDER.matches(txt,obj.getCodeDescription());
            };


    /**
     *
     * @param tfCodeValue
     * @param cbCodeDesc
     */

    public CodeDecodeField(TextFieldExt tfCodeValue, ComboBoxExt<CodeDataItemDO> cbCodeDesc) {
        this.tfCodeValue = tfCodeValue;
        this.cbCodeDesc = cbCodeDesc;
        init();
        registerListeners();
    }


    /**
     *
     * @param codeName
     * @param language
     */
    public CodeDecodeField(String codeName,String language,int codeValueLength,int codeValuePrefWidth,int comboBoxPrefWidth) {
        this.cbCodeDesc = new ComboBoxExt<CodeDataItemDO>(FXCollections.observableList(reteriveLinkedCodeDecodeItems(codeName,language)), AUTOCOMPLETE_CONTAINS_CHARS_IN_ORDER);
        this.tfCodeValue = ViewHelpers.createTextFieldExt(codeValueLength, codeValuePrefWidth);
        this.cbCodeDesc.setPrefWidth(comboBoxPrefWidth);
        this.tfCodeValue.setPrefColumnCount(codeValueLength);
        init();
        registerListeners();
    }

    /**
     *
     * @param codeName
     * @param language
     */
    public CodeDecodeField(String codeName,String language) {
        this.codeName=codeName;
        this.language=language;
        this.cbCodeDesc = new ComboBoxExt<CodeDataItemDO>(FXCollections.observableList(reteriveLinkedCodeDecodeItems(codeName,language)), AUTOCOMPLETE_CONTAINS_CHARS_IN_ORDER);
        //this.tfCodeValue = new TextFieldExt(LENGTH_CODEVALUE);
        this.tfCodeValue = ViewHelpers.createTextFieldExt(LENGTH_CODEVALUE, DEFAULT_TEXTBOX_PREFWIDTH);
        this.cbCodeDesc.setPrefWidth(DEFAULT_COMBOBOX_PREFWIDTH);
        this.tfCodeValue.setPrefColumnCount(LENGTH_CODEVALUE);
        init();
        registerListeners();
    }

    /**
     *
     * @param tfCodeValue
     * @param cbCodeDesc
     * @param codeName
     * @param language
     */
    public CodeDecodeField(TextFieldExt tfCodeValue, ComboBoxExt<CodeDataItemDO> cbCodeDesc,String codeName,String language) {
        this.tfCodeValue = tfCodeValue;
        this.cbCodeDesc = cbCodeDesc;
        init();
        this.cbCodeDesc.addItems(FXCollections.observableList(reteriveLinkedCodeDecodeItems(codeName,language)),true);
        this.cbCodeDesc.setPrefWidth(DEFAULT_COMBOBOX_PREFWIDTH);
        this.tfCodeValue.setPrefColumnCount(LENGTH_CODEVALUE);
        registerListeners();
    }



    public void init(){
        this.tfCodeValue.setDisable(true);
        linkedCodeDecodeList = new ArrayList<CodeDecodeField>();

        /*
        ValidationSupport validationSupport = new ValidationSupport();
        validationSupport.setValidationDecorator(new GraphicValidationDecoration());
        validationSupport.registerValidator(tfCodeValue, false,
                Validator.createEmptyValidator(I18n.COMMON.getString("validation.required")));
        */

    }

    public void registerLinkedCodeDecodeField(CodeDecodeField codeDecodeField){
         //codeDecodeField.setAsReadOnly(true);
         linkedCodeDecodeList.add(codeDecodeField);
    }


    /**
     * Get code data items (StaticCodeDeode)
     * @param codeName
     * @param language
     * @return
     */
    public List<CodeDataItemDO> reteriveLinkedCodeDecodeItems(String codeName, String language){

        if(!isSkipCache()){
            if(cachedCodeDecodeData.keySet().contains("*")){
                return cachedCodeDecodeData.get("*");
            }
        }

        StaticCodeDecodeController controller = (StaticCodeDecodeController) ObjectFactory.getControllerObject(ObjectFactory.STATIC_CODE_DECODE_CTRL);
        Optional<List<CodeDataItemDO>> listOptional = controller.getStaticCodeDataItemList(codeName,language);
        if(listOptional.isPresent()) {
           cachedCodeDecodeData.put("*", listOptional.get());
        }

        return cachedCodeDecodeData.get("*");
    }

    /**
     * Reterive Linked items for sepecified codedecode
     *
     * @param codeDecodeField
     * @param filterCode
     * @return
     */
    public List<CodeDataItemDO> reteriveLinkedCodeDecodeItems(CodeDecodeField codeDecodeField, String filterCode){

        if(!isSkipCache()){
            if(codeDecodeField.cachedCodeDecodeData.keySet().contains(filterCode)){
                return codeDecodeField.cachedCodeDecodeData.get(filterCode);
            }
        }

        StaticCodeDecodeController controller = (StaticCodeDecodeController) ObjectFactory.getControllerObject(ObjectFactory.STATIC_CODE_DECODE_CTRL);
        Optional<List<CodeDataItemDO>> codeDataItemDOList = controller.getStaticCodeDataItemList(codeDecodeField.codeName,filterCode,codeDecodeField.language);
        if(codeDataItemDOList.isPresent()) {
            codeDecodeField.cachedCodeDecodeData.put(filterCode, codeDataItemDOList.get());
        }else{
            return null;
        }

        return codeDecodeField.cachedCodeDecodeData.get(filterCode);
    }

    public void addValueChangeListener(ValueChangeListener changeListener){
        tfCodeValue.addValueChangeListener(changeListener);
    }

    /**
     *
     * Register all listeners
     */

    public void registerListeners(){

        cbCodeDesc.addValueChangeListener(() -> {
            CodeDataItemDO dataItemDO = cbCodeDesc.getSelectionModel().getSelectedItem();
            if(dataItemDO != null) {
                tfCodeValue.setDataValue(dataItemDO.getCodeValue());
            }else{
                 clearDataValue();
            }
         });

        tfCodeValue.addValueChangeListener(() -> {
            String codeValue = tfCodeValue.getDataValue();
            List<CodeDataItemDO>  list = cbCodeDesc.getItems();
            CodeDataItemDO item =  list.stream().filter(cd -> codeValue.equals(cd.getCodeValue())).findAny().orElse(null);
            if(item != null){
                cbCodeDesc.getSelectionModel().select(item);
                filterLinkedCodeDecode(item);
            }else{
                clearDataValue();
            }
        });
    }

    private void filterLinkedCodeDecode(CodeDataItemDO item){
            linkedCodeDecodeList.forEach(codeDecodeField -> {
                if(item != null ) {
                    codeDecodeField.cbCodeDesc.addItems(FXCollections.observableList(reteriveLinkedCodeDecodeItems(codeDecodeField,item.getCodeValue())), true);
                }else{
                    codeDecodeField.cbCodeDesc.addItems(FXCollections.observableList(new ArrayList<>()), true);
                }
            });
    }

    /**
     * Return codedecodefield as single control
     *
     * @return gridPane
     */
    public GridPane asControl(){
        GridPane gridPane = new GridPane();
        StackPane tfStack = new StackPane();
        tfStack.getChildren().add(tfCodeValue);
        StackPane cbStack = new StackPane();
        cbStack.getChildren().add(cbCodeDesc);
        gridPane.add(tfStack,0,0);
        gridPane.add(cbStack,1,0);
        return gridPane;
    }

    /**
     * Set the width of the codedecodefield.
     *
     * @param width
     */
    public void setMaxWidth(double width){
        cbCodeDesc.setMaxWidth(width);
    }

    /**
     * Set the width of the codedecodefield.
     *
     * @param width
     */
    public void setPrefWidth(double width){
        cbCodeDesc.setPrefWidth(width);
    }

    public void getPrefWidth(double width){
        cbCodeDesc.getPrefWidth();
    }

    /**
     * Request focus
     *
     */
    public void requestFocus(){
        cbCodeDesc.requestFocus();
    }


    public void setVisible(boolean visible){
        tfCodeValue.setVisible(visible);
        cbCodeDesc.setVisible(visible);
    }

    @Override
    public void clearDataValue() {
        tfCodeValue.clearDataValue();
        cbCodeDesc.clearDataValue();
    }

    @Override
    public void setAsReadOnly(boolean readOnly) {
        //tfCodeValue.setAsReadOnly(readOnly);
        cbCodeDesc.setAsReadOnly(readOnly);
    }

    @Override
    public boolean isReadonly() {
        return cbCodeDesc.isReadonly();
    }

    @Override
    public void destroyComponent() {
        tfCodeValue = null;
        cbCodeDesc = null;
    }

    @Override
    public boolean isDataExchangeApplicable() {
        return true;
    }

    @Override
    public void setDataValue(String value) {
        tfCodeValue.setDataValue(value);
    }

    @Override
    public String getDataValue() {
        return tfCodeValue.getDataValue();
    }


    public String getDataValueDescription() {

        if(cbCodeDesc.getSelectionModel().getSelectedItem() != null) {
            CodeDataItemDO codeDataItemDO = cbCodeDesc.getSelectionModel().getSelectedItem();
            return codeDataItemDO.getCodeDescription();
        }

        return "";
    }

    @Override
    public boolean isComponentShowing() {
        return (getTfCodeValue().isComponentShowing() && getCbCodeDesc().isComponentShowing());
    }

    public TextFieldExt getTfCodeValue() {
        return tfCodeValue;
    }

    public void setTfCodeValue(TextFieldExt tfCodeValue) {
        this.tfCodeValue = tfCodeValue;
    }

    public ComboBoxExt<CodeDataItemDO> getCbCodeDesc() {
        return cbCodeDesc;
    }

    public void setCbCodeDesc(ComboBoxExt<CodeDataItemDO> cbCodeDesc) {
        this.cbCodeDesc = cbCodeDesc;
    }

    public boolean isSkipCache() {
        return skipCache;
    }

    public void setSkipCache(boolean skipCache) {
        this.skipCache = skipCache;
    }
}
