/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.domainobjs.jsonwrapper;


public interface IJSONObject {
	
	public void copyEntityToJSON();
	
	public void copyJSONToEntity();

}
