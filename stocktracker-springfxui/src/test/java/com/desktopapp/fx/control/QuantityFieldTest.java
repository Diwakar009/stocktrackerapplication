/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.desktopapp.fx.control;

import com.desktopapp.fx.common.Quantity;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import org.junit.Test;

import java.math.BigDecimal;


/**
 * Created by diwakar009 on 1/12/2018.
 */
public class QuantityFieldTest extends Application{
    public QuantityFieldTest() {
        super();
    }

    @Override
    public void init() throws Exception {
        super.init();
    }

    @Override
    public void stop() throws Exception {
        super.stop();
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        //testNumberOfPacketsField(primaryStage);
        //testQuantity(primaryStage);
        testGunnyBangQuantity(primaryStage);
    }

    @Test
    public void name() throws Exception {
    }

    public void testGunnyBangQuantity(Stage primaryStage){
        primaryStage.setTitle("Test...");

        GunnyBagQuantityField integerField = new GunnyBagQuantityField();
        GunnyBagQuantityField integerField1 = new GunnyBagQuantityField();
        GunnyBagQuantityField answerField1 = new GunnyBagQuantityField();

        Button button = new Button("Calculate");


        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));

        grid.add(integerField.asControl(),1,1);
        grid.add(integerField1.asControl(),1,2);
        grid.add(answerField1.asControl(),1,3);

        grid.add(button,1,4);

        Scene scene = new Scene(grid, 800, 700);
        primaryStage.setScene(scene);
        primaryStage.show();


    }

    public void testQuantity(Stage primaryStage){
        primaryStage.setTitle("Test...");

        QuantityField integerField = new QuantityField();
        QuantityField integerField1 = new QuantityField();
        QuantityField answerField1 = new QuantityField();

        Button button = new Button("Calculate");


        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));

        grid.add(integerField.asControl(),1,1);
        grid.add(integerField1.asControl(),1,2);
        grid.add(answerField1.asControl(),1,3);

        grid.add(button,1,4);

        Scene scene = new Scene(grid, 800, 700);
        primaryStage.setScene(scene);
        primaryStage.show();


    }


    public void testNumberOfPacketsField(Stage primaryStage){
        primaryStage.setTitle("Test...");

        IntegerField integerField = new IntegerField();
        IntegerField integerField1 = new IntegerField();
        IntegerField answerField1 = new IntegerField();

        BigDecimalField weightmentField = new BigDecimalField(2);
        BigDecimalField weightmentField1 = new BigDecimalField(2);
        BigDecimalField ansWeightmentFld = new BigDecimalField(2);

        Button button = new Button("Calculate");

        Button button1 = new Button("Calculate Weightment ");

        button.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Long int1 = (Long)integerField.getValue();
                Long int2 = (Long)integerField1.getValue();
                Long ansInt = int1.longValue()+ int2.longValue();
                answerField1.setDataValue(String.valueOf(ansInt.intValue()));
            }
        });

        button1.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                BigDecimal int1 = (BigDecimal)weightmentField.getValue();
                BigDecimal int2 = (BigDecimal)weightmentField1.getValue();
                BigDecimal ansInt = int1.add(int2);
                ansWeightmentFld.setDataValue(ansInt.toPlainString());
            }
        });



        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));

        grid.add(integerField,1,1);
        grid.add(integerField1,1,2);
        grid.add(answerField1,1,3);

        grid.add(button,1,4);

        grid.add(weightmentField,2,1);
        grid.add(weightmentField1,2,2);
        grid.add(ansWeightmentFld,2,3);

        grid.add(button1,2,4);


        Scene scene = new Scene(grid, 300, 275);
        primaryStage.setScene(scene);
        primaryStage.show();

    }

    public static void main(String[] args) {
        launch(args);
    }
}
