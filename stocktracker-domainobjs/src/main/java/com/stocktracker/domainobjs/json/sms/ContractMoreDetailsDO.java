/*
 * Copyright (c) 2020.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.domainobjs.json.sms;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.Gson;

import java.io.Serializable;

/**
 * ContractMoreDetailsDO .
 *
 */
@JsonIgnoreProperties(ignoreUnknown=true)
@JsonInclude(Include.NON_EMPTY)
public class ContractMoreDetailsDO implements Serializable {
	private static final long serialVersionUID = 1L;
	public static String CLASS_NAME = ContractMoreDetailsDO.class.getName();

	@JsonProperty("season")
	private String season;

	@JsonProperty("kmsYear")
	private String kmsYear;

	@JsonProperty("millerCode")
	private String millerCode;

	public ContractMoreDetailsDO() {
		season = new String();
		kmsYear = new String();
		millerCode = new String();
	}

	public String getSeason() {
		return season;
	}

	public void setSeason(String season) {
		this.season = season;
	}

	public String getKmsYear() {
		return kmsYear;
	}

	public void setKmsYear(String kmsYear) {
		this.kmsYear = kmsYear;
	}

	public String getMillerCode() {
		return millerCode;
	}

	public void setMillerCode(String millerCode) {
		this.millerCode = millerCode;
	}

	public String toJSON(){
		return  new Gson().toJson(this);
	}
	public ContractMoreDetailsDO fromJSON(String json){
		return  new Gson().fromJson(json, ContractMoreDetailsDO.class);
	}
}

