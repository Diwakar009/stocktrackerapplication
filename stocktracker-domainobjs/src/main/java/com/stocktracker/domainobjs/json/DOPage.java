/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.domainobjs.json;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.xml.crypto.dsig.keyinfo.KeyValue;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties (ignoreUnknown=true)
@JsonInclude(Include.NON_EMPTY)
public class DOPage<T> implements Serializable{
 
	
	public DOPage() {
		super();
		// TODO Auto-generated constructor stub
		list = new ArrayList<T>();
	}

	@JsonProperty("list")
	private List<T> list;
	
	@JsonProperty("last")
	private boolean last;
	
	@JsonProperty("totalElements")
	private Long totalElements;
	
	@JsonProperty("totalPages")
	private Integer totalPages;
	
	@JsonProperty("size")
	private Integer size;
	
	@JsonProperty("number")
	private Integer number;
	
	@JsonProperty("first")
	private boolean first;
	
	@JsonProperty("numberOfElements")
	private Integer numberOfElements;
	
	
	public List<T> getList() {
		return list;
	}

	public void setList(List<T> list) {
		this.list = list;
	}

	public boolean isLast() {
		return last;
	}

	public void setLast(boolean last) {
		this.last = last;
	}

	public Long getTotalElements() {
		return totalElements;
	}

	public void setTotalElements(Long totalElements) {
		this.totalElements = totalElements;
	}

	public Integer getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(Integer totalPages) {
		this.totalPages = totalPages;
	}

	public Integer getSize() {
		return size;
	}

	public void setSize(Integer size) {
		this.size = size;
	}

	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	public boolean isFirst() {
		return first;
	}

	public void setFirst(boolean first) {
		this.first = first;
	}

	public Integer getNumberOfElements() {
		return numberOfElements;
	}

	public void setNumberOfElements(Integer numberOfElements) {
		this.numberOfElements = numberOfElements;
	}

	@Override
	public String toString() {
		return "DOPage{" +
				"list=" + list +
				", last=" + last +
				", totalElements=" + totalElements +
				", totalPages=" + totalPages +
				", size=" + size +
				", number=" + number +
				", first=" + first +
				", numberOfElements=" + numberOfElements +
				'}';
	}
}
