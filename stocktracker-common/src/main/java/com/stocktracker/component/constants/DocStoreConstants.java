/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.component.constants;

public class DocStoreConstants {
	
	public static final String MAIN_BACKUP_FILE = "docstorebkfile.dat";
	public static final String MAIN_BACKUP_DOC_FOLDER = "docstoreFolder";
	
	public static final String CD_LANGUAGE = "CD_LANGUAGE";
	public static final String CD_DOCTYPE = "CD_DOCTYPE";
	public static final String CD_FILETYPE = "CD_FILETYPE";
	public static final String CD_ACCESSLEVEL="CD_ACCESSLEVEL";
	
	public static final String CD_YESNO = "CD_YESNO";

}
