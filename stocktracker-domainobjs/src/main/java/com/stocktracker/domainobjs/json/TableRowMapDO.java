/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.domainobjs.json;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties (ignoreUnknown=true)
@JsonInclude(Include.NON_EMPTY)
public class TableRowMapDO<K,V>  extends BaseDomainObject implements Serializable{
	
	private static final long serialVersionUID = 1L;
	public static String CLASS_NAME = TableRowMapDO.class.getName();

	/**
	 * Used for construction of the objects.
	 */
	@JsonProperty("clazzName")
	private String clazzName = "";
	
	@JsonProperty("row")
	private Map<K,V> row = null;

	public TableRowMapDO(Map<K,V> map,String clazzName) {
		super();
		this.row = map;
		this.clazzName = clazzName;
	}

	public TableRowMapDO() {
		super();
		row = new HashMap<K, V>();
	}

	public TableRowMapDO(Class clazz) {
		super();
		row = new HashMap<K, V>();
		clazzName = clazz.getName();
	}

	
	public void add(K key,V value){
		row.put(key, value);
	}
	
	public Map<K, V> getRow() {
		return row;
	}
	public void setRow(Map<K, V> keyValueMap) {
		this.row = keyValueMap;
	}

	@JsonIgnore
	@Override
	public Object getDomainObjectKey() {
		// TODO Auto-generated method stub
		return getId();
	}

	public String getClazzName() {
		return clazzName;
	}

	public void setClazzName(String clazzName) {
		this.clazzName = clazzName;
	}

	@JsonIgnore
	public Object getId(){
		return getRow().get("id");
	}
	
	@JsonIgnore
	public V getValue(K  key){
		return (V)row.get(key);
	}
	
	@JsonIgnore
	public  void addKeyValue(K key,V value){
		row.put(key, value);
	}


}
