/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.template.helper;

import java.io.StringReader;
import java.io.StringWriter;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.stocktracker.template.ParsedTemplate;
import com.stocktracker.template.exception.MalFormedTemplateException;
import com.stocktracker.template.exception.TemplateRenderingException;
import com.stocktracker.util.KeyValueList;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.event.EventCartridge;
import org.apache.velocity.app.event.IncludeEventHandler;
import org.apache.velocity.app.event.ReferenceInsertionEventHandler;
import org.apache.velocity.context.Context;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.RuntimeInstance;
import org.apache.velocity.runtime.RuntimeServices;
import org.apache.velocity.runtime.log.LogChute;
//import org.apache.velocity.tools.generic.DateTool;

import com.stocktracker.component.common.ContentType;


/**
 * Template util for merging the  
 * 
 * @author "Diwakar Choudhury"
 */
public final class TemplateHelper {
	
	public static ParsedTemplate parseTemplate(String templateName , String templateData, Locale templateLocale ){
		ContentType templateContentType = (templateData.regionMatches(true, 0, "<html>", 0,6)) ? ContentType.HTML : ContentType.PLAIN;
		
		try{
			RuntimeInstance ri = createVelocityRuntime();
			ri.getConfiguration().addProperty(RuntimeConstants.INPUT_ENCODING, "UTF-8");
			ri.getConfiguration().addProperty(RuntimeConstants.OUTPUT_ENCODING, "UTF-8");
			Template templateObject = new Template();
			templateObject.setEncoding("UTF-8");
			templateObject.setRuntimeServices(ri);
			templateObject.setName(templateName);
			templateObject.setData(ri.parse(new StringReader(templateData), templateName));
			templateObject.initDocument();
			return new ParsedTemplate(templateObject, templateContentType, templateLocale);
		}catch(Exception e){
			throw new MalFormedTemplateException("Failed to parse '" + templateName + "' - " + e, e);
		}
	}
	
	public static RuntimeInstance createVelocityRuntime() throws Exception{
		RuntimeInstance ri = new RuntimeInstance();
		//ri.setProperty(RuntimeConstants.RUNTIME_LOG_LOGSYSTEM,new VelocityLogAdapter(LOGGER));
		ri.setProperty(RuntimeConstants.VM_PERM_INLINE_LOCAL, true);
		/*
		ri.setProperty(RuntimeConstants.RESOURCE_LOADER, "file"); 
		ve.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath"); 
		ve.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());
		*/
		
		ri.init();
		return ri;
	}
	
	public static String renderTemplate(ParsedTemplate parsedTempalte,KeyValueList parameters){
		
		try{
			VelocityContext context = (parameters == null ? new VelocityContext() : new VelocityContext(new ContextAdapter(parameters)));
			context.put("dollar", "$");
			
			context.put("locale", parsedTempalte.getTempalteLocale());
			
			context.put("dateFormat", new SimpleDateFormat("dd.MM.yyyy"));
			context.put("timeFormat", DateFormat.getTimeInstance(DateFormat.SHORT, parsedTempalte.getTempalteLocale()));
			context.put("dateTimeFormat", DateFormat.getDateTimeInstance(DateFormat.SHORT,DateFormat.SHORT, parsedTempalte.getTempalteLocale()));
			
			//context.put("currencyFormat", DateFormat.getDateTimeInstance(DateFormat.SHORT,DateFormat.SHORT, parsedTempalte.getTempalteLocale()));
			
			//context.put("date",new DateTool()); // commented as the velocity tool is giving issues
			//context.put("MaskUtil",new MaskTool());
			
			EventCartridge ec = new EventCartridge();
			ec.addEventHandler(new ReferenceInsertionHandler(parsedTempalte.getTempalteLocale()));
		
			ec.addEventHandler(new SuppressIncludesHandler(parsedTempalte.getTemplateName()));
			ec.attachToContext(context);
			StringWriter resultWriter = new StringWriter(300);
			parsedTempalte.getTemplateObject().merge(context, resultWriter);
			return resultWriter.toString();
			
			
		}catch(MalFormedTemplateException e){
			throw e;
		}catch(TemplateRenderingException e){
			throw e;
		}catch(Exception e){
			throw new TemplateRenderingException("Caught unexpected exception '" + e + "'" + "when rendering" + parsedTempalte,e );
		}
	}
	private static final class ContextAdapter implements Context{
		private final KeyValueList wrappedKeyValueList;
		
		public ContextAdapter(KeyValueList keyValueList) {
			this.wrappedKeyValueList = Objects.requireNonNull(keyValueList,"Null 'KeyValueList' argument passed to Templatehelper.ContextAdapter constructor");
		}

		@Override
		public boolean containsKey(Object key) {
			// TODO Auto-generated method stub
			return (key instanceof String ? wrappedKeyValueList.containsKey((String)key) : false);
		}

		@Override
		public Object get(String key) {
			// TODO Auto-generated method stub
			return wrappedKeyValueList.get(key);
		}

		@Override
		public Object[] getKeys() {
			// TODO Auto-generated method stub
			return wrappedKeyValueList.getKeys().toArray();
		}

		@Override
		public Object put(String arg0, Object arg1) {
			throw new UnsupportedOperationException("Unexpected put() call on inner context during template rendering");
		}

		@Override
		public Object remove(Object arg0) {
			throw new UnsupportedOperationException("Unexpected remove() call on inner context during template rendering");
		}

		@Override
		public String toString() {
			return wrappedKeyValueList.toString();
		}
		
	}
	
	private static final class ReferenceInsertionHandler implements ReferenceInsertionEventHandler{
		
		private final Locale templateLocale;

		public ReferenceInsertionHandler(Locale templateLocale) {
			super();
			this.templateLocale = Objects.requireNonNull(templateLocale,"Null 'templateLocale' argument passed to Templatehelper.ContextAdapter constructor");
		}

		@Override
		public Object referenceInsert(String reference, Object value) {
			
			if(value == null){
				//throw new Exception ("Reference '" + reference + "' Resolved to NULL");
				return "";
			}
			
			if(value instanceof String){
				return value;
			}
			
			if(value instanceof Number){
				NumberFormat formater = NumberFormat.getNumberInstance(templateLocale);
				formater.setMaximumFractionDigits(6);
				return formater.format(value);
			}
			
			if(value instanceof Timestamp){
				return DateFormat.getDateTimeInstance(DateFormat.MEDIUM,DateFormat.MEDIUM,templateLocale).format(value);
			}
			
			if(value instanceof Date){
				return DateFormat.getDateInstance(DateFormat.MEDIUM,templateLocale).format(value);
			}
			
			if(value instanceof Boolean){
				return ((Boolean) value ? "1" : "0");
			}
			
			// TODO Auto-generated method stub
			return value.toString();
		}
		
		
		
	}
	
	
	private static final class SuppressIncludesHandler implements IncludeEventHandler{
		private final String templateName;
		
		public SuppressIncludesHandler(String templateName) {
			super();
			this.templateName = Objects.requireNonNull(templateName,"Null 'templateName' argument passed to Templatehelper.SupperessIncludeshandler constructor");;
		}

		@Override
		public String includeEvent(String arg0, String arg1, String directiveName) {
			// TODO Auto-generated method stub
			throw new MalFormedTemplateException("Unsupportive directive '" + directiveName + "' in template '" +  templateName + "'");
		}
		
	}
	
	private static final class VelocityLogAdapter implements LogChute{

		private final Logger delegate;
		
		public VelocityLogAdapter(Logger delegate) {
			
			super();
			this.delegate = Objects.requireNonNull(delegate,"Null 'delegate' argument passed to Templatehelper.VelocityLogAdapter constructor");;
		}
		
		@Override
		public void init(RuntimeServices rs) throws Exception {
			// Nothing to do
			
		}

		@Override
		public boolean isLevelEnabled(int level) {
			return delegate.isLoggable(toLevel(level));
		}

		@Override
		public void log(int level, String message) {
			delegate.log(toLevel(level),message);
		}

		@Override
		public void log(int level, String message, Throwable t) {
			delegate.log(toLevel(level),message,t);
			
		}
		
		private static Level toLevel (int velocityLevel){
				switch(velocityLevel){
				case LogChute.TRACE_ID:
					return Level.FINEST;
				case LogChute.DEBUG_ID:
					return Level.FINE;
				case LogChute.INFO_ID:
					return Level.INFO;
				case LogChute.WARN_ID:
					return Level.WARNING;
				case LogChute.ERROR_ID:
					return Level.SEVERE;
				default:
					return Level.SEVERE;
				}
		}
		
	}
	
	
}
