/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.domainobjs.json.sms;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.stocktracker.domainobjs.json.BaseDomainObject;
import com.stocktracker.model.BaseEntity;
import com.stocktracker.model.sms.CostPriceDetails;
import com.stocktracker.model.sms.PartyDetails;

import javax.persistence.Column;
import java.io.Serializable;

/**
 * CostPriceDetailsDO .
 * 
 */
@JsonIgnoreProperties(ignoreUnknown=true)
@JsonInclude(Include.NON_EMPTY)
public class CostPriceDetailsDO extends BaseDomainObject implements Serializable {
	private static final long serialVersionUID = 1L;

	public static String CLASS_NAME = CostPriceDetailsDO.class.getName();

	public CostPriceDetailsDO() {

	}

	public CostPriceDetailsDO(PartyDetails partyDetails) {
		copyEntity2DomainObject(partyDetails);
	}

	@JsonProperty("id")
	private Long id;

	@JsonProperty("appliedRate")
	private Double appliedRate;

	@JsonProperty("derivedAmount")
	private Double derivedAmount;

	@JsonProperty("actualAmount")
	private Double actualAmount;

	@JsonProperty("plusOrMinusOnTotal")
	private Character plusOrMinusOnTotal;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getAppliedRate() {
		return appliedRate;
	}

	public void setAppliedRate(Double appliedRate) {
		this.appliedRate = appliedRate;
	}

	public Double getDerivedAmount() {
		return derivedAmount;
	}

	public void setDerivedAmount(Double derivedAmount) {
		this.derivedAmount = derivedAmount;
	}

	public Double getActualAmount() {
		return actualAmount;
	}

	public void setActualAmount(Double actualAmount) {
		this.actualAmount = actualAmount;
	}

	public Character getPlusOrMinusOnTotal() {
		return plusOrMinusOnTotal;
	}

	public void setPlusOrMinusOnTotal(Character plusOrMinusOnTotal) {
		this.plusOrMinusOnTotal = plusOrMinusOnTotal;
	}

	@JsonIgnore
	@Override
	public Object getDomainObjectKey() {
		return getId();
	}

	@Override
	public CostPriceDetails copyDomainObject2Entity(BaseEntity entity) {
		CostPriceDetails costPriceDetails = null;

		if(entity == null){
			costPriceDetails = new CostPriceDetails();
		}else {
			costPriceDetails = (CostPriceDetails)entity;
		}

		costPriceDetails.setCreatedBy(this.getCreatedBy());
		costPriceDetails.setUpdatedBy(this.getUpdatedBy());
		costPriceDetails.setCreatedAt(this.getCreatedAt());
		costPriceDetails.setUpdatedAt(this.getUpdatedAt());

		costPriceDetails.setId(this.getId());
		costPriceDetails.setAppliedRate(this.getAppliedRate());
		costPriceDetails.setDerivedAmount(this.getDerivedAmount());
		costPriceDetails.setActualAmount(this.getActualAmount());
		costPriceDetails.setPlusOrMinusOnTotal(this.getPlusOrMinusOnTotal());

		return costPriceDetails;
	}
	@Override
	public void copyEntity2DomainObject(BaseEntity entity) {
		CostPriceDetails costPriceDetails = (CostPriceDetails) entity;
		this.setCreatedBy(costPriceDetails.getCreatedBy());
		this.setUpdatedBy(costPriceDetails.getUpdatedBy());
		this.setCreatedAt(costPriceDetails.getCreatedAt());
		this.setUpdatedAt(costPriceDetails.getUpdatedAt());

		this.setId(costPriceDetails.getId());
		this.setAppliedRate(costPriceDetails.getAppliedRate());
		this.setDerivedAmount(costPriceDetails.getDerivedAmount());
		this.setActualAmount(costPriceDetails.getActualAmount());
		this.setPlusOrMinusOnTotal(costPriceDetails.getPlusOrMinusOnTotal());

	}

	@Override
	public String toString() {
		return "CostPriceDetailsDO{" +
				"id=" + id +
				", appliedRate=" + appliedRate +
				", derivedAmount=" + derivedAmount +
				", actualAmount=" + actualAmount +
				", plusOrMinusOnTotal=" + plusOrMinusOnTotal +
				'}';
	}
}