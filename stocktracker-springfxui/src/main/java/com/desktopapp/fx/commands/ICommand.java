package com.desktopapp.fx.commands;

/**
 * Created by diwakar009 on 25/12/2018.
 */
public interface ICommand {
    public void executeCommand();
}
