/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.jpa.spring.extended;

import com.stocktracker.jpa.spring.extended.custom.ExtendedRepository;
import com.stocktracker.model.sms.DeliveryDetails;
import org.springframework.stereotype.Repository;

@Repository
public interface DeliveryDetailsRepository extends ExtendedRepository<DeliveryDetails, Long> {

}
