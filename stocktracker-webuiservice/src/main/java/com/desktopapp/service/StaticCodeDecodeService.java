package com.desktopapp.service;

import java.util.*;

import com.stocktracker.domainobjs.IDomainObject;
import com.stocktracker.domainobjs.json.CodeDataItemDO;
import com.stocktracker.domainobjs.json.DOPage;
import com.stocktracker.domainobjs.json.StaticCodeDecodeDO;
import com.stocktracker.domainobjs.json.TableRowMapDO;
import com.stocktracker.model.BaseEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;


/**
 * Static Code Decode Service
 *
 * @author Ratnala Diwakar Choudhury
 */
@Service
public class StaticCodeDecodeService extends AbstractService<StaticCodeDecodeDO> {
	
	public StaticCodeDecodeService() {
        super(StaticCodeDecodeDO.class);
    }
    
	public static final String BASE_RESOURCE = "staticCodeDecodes";
	
    @Override
	protected String getBaseResource() {
		return BASE_RESOURCE;
	}

    public Optional<List<CodeDataItemDO>> getStaticCodeDataItemList(String codeName, String language){
	   
    	List<CodeDataItemDO> codeDataItemList = null;
    	String getCallWebURL = getWebserviceURL() + "/{codeName}/{language}";
    	
    	Map<String, String> params = new HashMap<String, String>();
        params.put("codeName", codeName);
        params.put("language", language);
    	
    	//CodeDataItemDO[] codeDecodes = getRestTemplate().getForObject(getCallWebURL,CodeDataItemDO[].class,params);
		CodeDataItemDO[] codeDecodes = (CodeDataItemDO[])callService(getCallWebURL,CodeDataItemDO[].class,params);
    	
    	if(codeDecodes != null && codeDecodes.length != 0){
    		codeDataItemList = Arrays.asList(codeDecodes);
    	}else{
    		return Optional.of(codeDataItemList);
    	}
    	
    	return Optional.of(codeDataItemList);
    	
    }

	public Optional<List<CodeDataItemDO>> getStaticCodeDataItemList(String codeName,String filterCodeValue, String language){

		List<CodeDataItemDO> codeDataItemList = null;
		String getCallWebURL = getWebserviceURL() + "/{codeName}/{filterCodeValue}/{language}";

		Map<String, String> params = new HashMap<String, String>();
		params.put("codeName", codeName);
		params.put("filterCodeValue", filterCodeValue);
		params.put("language", language);

		//CodeDataItemDO[] codeDecodes = getRestTemplate().getForObject(getCallWebURL,CodeDataItemDO[].class,params);
		CodeDataItemDO[] codeDecodes = (CodeDataItemDO[])callService(getCallWebURL,CodeDataItemDO[].class,params);

		if(codeDecodes != null && codeDecodes.length != 0){
			codeDataItemList = Arrays.asList(codeDecodes);
		}else{
			return Optional.of(codeDataItemList);
		}

		return Optional.of(codeDataItemList);

	}

	/**
	 * list static code decodes using codename and lang
	 *
	 * @param parameters
	 * @return
	 */
	public DOPage<TableRowMapDO<String, String>> listByCodeNameNLang(Map<String, Object> parameters) {
		return getListWithPostQuery("listByCodeNameNLang",parameters);
	}

	/**
	 * Delete by code name and language
	 *
	 * @param codeName
	 * @param language
	 */
	public void deleteByCodeNameAndLanguage(String codeName,String language){
		StaticCodeDecodeDO staticCodeDecodeDO = new StaticCodeDecodeDO();
		staticCodeDecodeDO.setCodeName(codeName);
		staticCodeDecodeDO.setLanguage(language);
		this.postForRemove(getWebserviceURL() + "/" + "deleteByCodeNameAndLanguage",staticCodeDecodeDO);
	}
}
