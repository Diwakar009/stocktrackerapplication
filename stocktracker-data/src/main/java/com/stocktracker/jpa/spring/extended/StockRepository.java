/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.jpa.spring.extended;

import com.stocktracker.jpa.spring.extended.custom.ExtendedRepository;
import com.stocktracker.jpa.spring.extended.extention.StockDetailsExtendedRepository;
import com.stocktracker.jpa.spring.extended.extention.StockExtendedRepository;
import com.stocktracker.model.sms.Stock;
import com.stocktracker.model.sms.StockPK;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.persistence.LockModeType;
import java.util.List;

@Repository
public interface StockRepository extends ExtendedRepository<Stock, StockPK> , StockExtendedRepository {

    @Lock(LockModeType.PESSIMISTIC_READ)
    @Query(value="SELECT s FROM Stock s WHERE s.id.industryCode=:industryCode and s.id.stockCode=:stockCode and s.id.stockVarietyCode=:stockVariety")
    public Stock findStockNLock(@Param("industryCode") String industryCode, @Param("stockCode") String stockCode, @Param("stockVariety") String stockVariety);


}
