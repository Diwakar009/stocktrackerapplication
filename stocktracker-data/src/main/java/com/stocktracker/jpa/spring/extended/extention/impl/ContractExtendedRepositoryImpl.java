/*
 * Copyright (c) 2020.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.jpa.spring.extended.extention.impl;

import com.stocktracker.jpa.spring.extended.bean.MandiReconcilationData;
import com.stocktracker.jpa.spring.extended.bean.StockDetailsSummary;
import com.stocktracker.jpa.spring.extended.constants.NativeQueryConstants;
import com.stocktracker.jpa.spring.extended.extention.ContractExtendedRepository;
import com.stocktracker.jpa.spring.extended.extention.StockDetailsExtendedRepository;
import com.stocktracker.model.BaseEntity;
import com.stocktracker.model.sms.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by diwakar009 on 22/10/2019.
 */
@Repository
public class ContractExtendedRepositoryImpl implements ContractExtendedRepository {

    private final Logger logger = LoggerFactory.getLogger(ContractExtendedRepositoryImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Page<Contract> findAllContractByFilter(String industryCodeFilter,
                                                  Date startDateRangeStartFilter, Date startDateRangeEndFilter,
                                                  String contractTypeFilter, String contractNameFilter,
                                                  String contractStatusFilter, PageRequest pageRequest) {

        logger.info("Executing findAllContractByFilter...");
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();

        CriteriaQuery<Contract> mainCreteriaQuery = criteriaBuilder.createQuery(Contract.class);
        CriteriaQuery<Long> countCriteriaQuery = criteriaBuilder.createQuery(Long.class);

        Root<Contract> mainRoot = mainCreteriaQuery.from(Contract.class);
        Root<Contract> countRoot = countCriteriaQuery.from(Contract.class);

        mainCreteriaQuery.select(criteriaBuilder.construct(Contract.class,
                mainRoot.get("id"),
                mainRoot.get("industryCode"),
                mainRoot.get("startDate"),
                mainRoot.get("endDate"),
                mainRoot.get("contractTypeCode"),
                mainRoot.get("contractName"),
                mainRoot.get("rawMaterial"),
                mainRoot.get("approvedRawMaterialQuantity"),
                mainRoot.get("receivedRawMaterialQuantity"),
                mainRoot.get("deliveryProduct"),
                mainRoot.get("productDeliveryQuantity"),
                mainRoot.get("productDeliveredQuantity"),
                mainRoot.get("status")));


        Optional<ParameterExpression<String>> icFilter = Optional.empty();
        Optional<ParameterExpression<Date>> sdrsFilter = Optional.empty();
        Optional<ParameterExpression<Date>> edreFilter = Optional.empty();
        Optional<ParameterExpression<String>> ctFilter = Optional.empty();
        Optional<ParameterExpression<String>> caFilter = Optional.empty();
        Optional<ParameterExpression<String>> csFilter = Optional.empty();

        List<Predicate> predicates = new ArrayList<Predicate>();

        if(industryCodeFilter != null && !"".equals(industryCodeFilter)) {
            icFilter = Optional.ofNullable(criteriaBuilder.parameter(String.class));
            predicates.add(criteriaBuilder.equal(mainRoot.get("industryCode"), icFilter.get()));
        }

        if(startDateRangeStartFilter != null && startDateRangeEndFilter != null){
            sdrsFilter = Optional.ofNullable(criteriaBuilder.parameter(Date.class));
            edreFilter = Optional.ofNullable(criteriaBuilder.parameter(Date.class));
            predicates.add(criteriaBuilder.between(mainRoot.get("startDate").as(java.sql.Date.class),sdrsFilter.get(),edreFilter.get()));

        }else {
            if (startDateRangeStartFilter != null) {
                sdrsFilter = Optional.ofNullable(criteriaBuilder.parameter(Date.class));
                predicates.add(criteriaBuilder.equal(mainRoot.get("startDate").as(java.sql.Date.class), sdrsFilter.get()));
            }

            if (startDateRangeEndFilter != null) {
                edreFilter = Optional.ofNullable(criteriaBuilder.parameter(Date.class));
                predicates.add(criteriaBuilder.lessThanOrEqualTo(mainRoot.get("startDate").as(java.sql.Date.class), edreFilter.get()));
            }
        }

        if(contractTypeFilter != null && !"".equals(contractTypeFilter)) {
            ctFilter = Optional.ofNullable(criteriaBuilder.parameter(String.class));
            predicates.add(criteriaBuilder.equal(mainRoot.get("contractTypeCode"), ctFilter.get()));
        }

        if(contractNameFilter != null && !"".equals(contractNameFilter)) {
            caFilter = Optional.ofNullable(criteriaBuilder.parameter(String.class));
            predicates.add(criteriaBuilder.like(mainRoot.get("contractName"), caFilter.get()));
        }

        if(contractStatusFilter != null && !"".equals(contractStatusFilter)) {
            csFilter = Optional.ofNullable(criteriaBuilder.parameter(String.class));
            predicates.add(criteriaBuilder.equal(mainRoot.get("contractStatus"), csFilter.get()));
        }

        mainCreteriaQuery.where(predicates.toArray(new Predicate[predicates.size()])); // Apply where clause
        mainCreteriaQuery.orderBy(criteriaBuilder.desc(mainRoot.get("startDate")));
        TypedQuery<Contract> mainQuery = entityManager.createQuery(mainCreteriaQuery);

        // Count Query
        countCriteriaQuery.select(criteriaBuilder.count(countRoot));
        countCriteriaQuery.where(predicates.toArray(new Predicate[predicates.size()])); // Apply where clause
        TypedQuery<Long> countQuery = entityManager.createQuery(countCriteriaQuery);

        if(icFilter.isPresent()) {
            mainQuery.setParameter(icFilter.get(), industryCodeFilter);
            countQuery.setParameter(icFilter.get(), industryCodeFilter);
        }

        if(sdrsFilter.isPresent()) {
            mainQuery.setParameter(sdrsFilter.get(), startDateRangeStartFilter);
            countQuery.setParameter(sdrsFilter.get(), startDateRangeStartFilter);
        }

        if(edreFilter.isPresent()) {
            mainQuery.setParameter(edreFilter.get(), startDateRangeEndFilter);
            countQuery.setParameter(edreFilter.get(), startDateRangeEndFilter);
        }

        if(ctFilter.isPresent()) {
            mainQuery.setParameter(ctFilter.get(), contractTypeFilter);
            countQuery.setParameter(ctFilter.get(), contractTypeFilter);
        }

        if(caFilter.isPresent()) {
            mainQuery.setParameter(caFilter.get(), "%"+ contractNameFilter + "%");
            countQuery.setParameter(caFilter.get(), "%"+ contractNameFilter + "%");
        }

        if(csFilter.isPresent()) {
            mainQuery.setParameter(csFilter.get(), contractStatusFilter);
            countQuery.setParameter(caFilter.get(), contractStatusFilter);
        }

        long total = countQuery.getSingleResult();
        return (Page)(pageRequest == null?new PageImpl(mainQuery.getResultList()):this.readPage(mainQuery,pageRequest, total));
    }


    private Object cast(Object object){
        if(object != null){
            if(object instanceof BigInteger){
                return ((BigInteger) object).longValue();
            }else if(object instanceof BigDecimal){
                return ((BigDecimal) object).doubleValue();
            }else if(object instanceof Date){
                return ((Date) object);
            }
        }
        return object;
    }

    protected Page<?> readPage(TypedQuery<?> query, Pageable pageable, Long total) {
        query.setFirstResult((int) pageable.getOffset());
        query.setMaxResults(pageable.getPageSize());
        List<?> content = total.longValue() > (long)pageable.getOffset()?query.getResultList(): Collections.emptyList();
        return new PageImpl(content, pageable, total.longValue());
    }

}
