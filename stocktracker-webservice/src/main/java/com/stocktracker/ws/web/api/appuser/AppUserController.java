/*
 * Copyright (c) 2020.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.ws.web.api.appuser;

import com.desktopapp.service.AbstractService;
import com.desktopapp.service.AppUserService;
import com.stocktracker.domainobjs.json.AppUserDO;
import com.stocktracker.framework.IEntity;
import com.stocktracker.model.AppUser;


import com.stocktracker.ws.web.api.GenericBaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequestMapping(value="/api/appUser")
@RestController
public class AppUserController extends GenericBaseController<AppUserDO> {
	
	@Autowired
    AppUserService service;

    @Override
    public AppUserDO find(AppUserDO appUserDO) {
       return service.find(appUserDO);
    }

    @Override
	public AppUserDO save(AppUserDO userDO) {
      return (AppUserDO)service.create(userDO);
	}

	@Override
	public boolean delete(AppUserDO userDO) {
        service.remove(userDO);
        return true;
	}

	@Override
	public boolean deleteList(List<AppUserDO> doList) {
        service.remove(doList);
        return true;
	}


    @Override
    public AbstractService getService() {
        return service;
    }

}
