package com.desktopapp.fx.app;

import com.desktopapp.fx.DesktopFxApplication;
import com.desktopapp.fx.control.CodeDecodeFieldTest;
import com.desktopapp.fx.controller.StaticCodeDecodeController;
import com.desktopapp.fx.util.I18n;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;

/**
 * Created by diwakar009 on 12/12/2018.
 */
/*@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {AppConfigTest.class,JPAConfiguration.class})*/
@SpringBootTest
@ComponentScan({"com.desktopapp.fx.controller","com.desktopapp.service","com.stocktracker.jpa.spring.extended"})
public class StaticCodeDecodePageTest  extends Application {

    private Stage mainStage;
    private Scene mainScene;
    public StaticCodeDecodePageTest() {
        super();
    }

    @Override
    public void init() throws Exception {
        super.init();
    }

    @Override
    public void stop() throws Exception {
        super.stop();
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle(I18n.COMMON.getString("app.title"));
        primaryStage.getIcons().add(new Image(DesktopFxApplication.class.getResourceAsStream(AppFeatures.APP_ICON)));
        primaryStage.setOnCloseRequest(this::onAppExit);

        AnchorPane outerPane = new AnchorPane();
        outerPane.prefHeight(AnchorPane.USE_PREF_SIZE);
        outerPane.prefWidth(AnchorPane.USE_PREF_SIZE);

        /*TestingAppView testingAppView = new TestingAppView();
        outerPane.getChildren().addAll(testingAppView);*/

        mainScene = new Scene(outerPane, 300, 275);
        mainScene.setRoot(outerPane);

        /*testingAppView.addPageToCenter(new StaticCodeDecodeController().getDataPageView());*/
        primaryStage.setScene(mainScene);
        primaryStage.setMaximized(true);
        primaryStage.show();
    }


    public void onAppExit(WindowEvent windowEvent) {
        Platform.exit();
    }


    public static void main(String args[]){
        ApplicationContext context = SpringApplication.run(CodeDecodeFieldTest.class);
        com.desktopapp.fx.objectfactory.ObjectFactory.setContext(context);
        launch(null);
    }

}
