/*
 * Copyright (c) 2020.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.ws.web.api.preference;

import com.desktopapp.service.AbstractService;
import com.desktopapp.service.PreferencesService;
import com.desktopapp.service.StaticCodeDecodeService;
import com.stocktracker.domainobjs.json.*;
import com.stocktracker.ws.web.api.GenericBaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RequestMapping(value="/api/preference")
@RestController
public class PreferenceController extends GenericBaseController<PreferencesDO> {

	/**
	 * The PreferencesService business service.
	 */
	@Autowired
	private PreferencesService service;


	@Override
	public PreferencesDO save(PreferencesDO preferencesDO) {
		return (PreferencesDO)service.create(preferencesDO);
	}

	@Override
	public boolean delete(PreferencesDO preferencesDO){
		service.remove(preferencesDO);
		return true;
	}

	@Override
	public boolean deleteList(List<PreferencesDO> doList) {
		service.remove(doList);
		return true;
	}

	@Override
	public PreferencesDO find(PreferencesDO preferencesDO) {
		return service.find(preferencesDO);
	}

	@Override
	public AbstractService getService() {
		return service;
	}


}
