package com.desktopapp.fx.commands;

import com.desktopapp.fx.DesktopFxApplication;

/**
 * Created by diwakar009 on 24/12/2018.
 */
public class HelpCommand implements ICommand{

    private void onOpenHelp() {
        DesktopFxApplication.get().showAppHelp();
    }

    @Override
    public void executeCommand() {
        onOpenHelp();
    }
}
