package com.stocktracker.ws.web.api;

import java.util.List;


import com.stocktracker.domainobjs.json.DOPage;
import com.stocktracker.domainobjs.json.DomainObjectList;
import com.stocktracker.domainobjs.json.TableRowMapDO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 
 * Abstract class for the generic services exposed for the common operations like
 * 
 *  findAll
 *  findWithFilter
 *  save
 *  delete
 *  deleteList
 * 
 * @author "Diwakar Choudhury"
 *
 */
public abstract class GenericBaseControllerOld extends BaseController {
	
    /**
     * The Logger for this class.
     */
    protected Logger logger = LoggerFactory.getLogger(this.getClass());
    
    /**
     * Web service endpoint to fetch all StaticCodeDecode entities (Used)
     * 
     * @return A ResponseEntity containing a Collection of StaticCodeDecode domain objects.
     */
    @RequestMapping(
            value = "/listAll",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DOPage<TableRowMapDO<String,String>>> listAll(Pageable pageRequest) {
        logger.info("> listAll");
        
        DOPage<TableRowMapDO<String,String>> page = getAll(pageRequest);

        logger.info("< listAll");
        return  new ResponseEntity<DOPage<TableRowMapDO<String,String>>>(page,HttpStatus.OK);
    }

    public abstract DOPage<TableRowMapDO<String,String>> getAll(Pageable pageRequest);
    
    @RequestMapping(value="/save",method=RequestMethod.POST,consumes=MediaType.APPLICATION_JSON_VALUE,produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<TableRowMapDO<String,String>> saveObj(@RequestBody TableRowMapDO<String,String> rowMap){
		logger.info("< save");
		
		TableRowMapDO<String,String> tableRowMap = save(rowMap);
	
		logger.info("> save");
		return new ResponseEntity<TableRowMapDO<String,String>>(tableRowMap,HttpStatus.CREATED);
		
	}
    
    public abstract TableRowMapDO<String,String> save(TableRowMapDO<String,String> rowMap);
    
	@RequestMapping(value="/delete",method=RequestMethod.POST,consumes=MediaType.APPLICATION_JSON_VALUE,produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Boolean> deleteObj(@RequestBody TableRowMapDO<String,String> rowMap){
		
		logger.info("< delete");
		boolean isSuccess = delete(rowMap);
		logger.info("> delete");
		
		return new ResponseEntity<Boolean>(isSuccess,HttpStatus.OK);
	}
	
	public abstract boolean delete(TableRowMapDO<String,String> staticCodeDecodeDO);
	
	
	@RequestMapping(value="/deleteList",method=RequestMethod.POST,consumes=MediaType.APPLICATION_JSON_VALUE,produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Boolean> delete(@RequestBody DomainObjectList<TableRowMapDO<String,String>> domainObjList){
		logger.info("< deleteList");
		
		boolean isSuccess = delete(domainObjList.getList());
	
		logger.info("> deleteList");
		
		return new ResponseEntity<Boolean>(isSuccess,HttpStatus.OK);
		
	}
	public abstract boolean delete(List<TableRowMapDO<String,String>> staticCodeDecodeDO);
	
	
	@RequestMapping(
     		value = "/listWithFilter",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    
    public ResponseEntity<DOPage<TableRowMapDO<String,String>>> listWithFilter(@RequestBody TableRowMapDO<String,String> filterMap){
	    logger.info("> listWithFilter");
	    
	    Integer pageNumber =  1;
	    		
	    if(filterMap.getValue("page") != null){		
	    	pageNumber = Integer.valueOf(filterMap.getValue("page"));
	    }
	    Integer size = 0;
	    
	    if(filterMap.getRow().get("size") != null){		
	    	size = Integer.valueOf(filterMap.getRow().get("size"));
	    }

	    Pageable pageRequest = PageRequest.of(pageNumber, size);
       
	    DOPage<TableRowMapDO<String,String>> page = getWithFilter(filterMap,pageRequest);

        logger.info("< listWithFilter");
        return  new ResponseEntity<DOPage<TableRowMapDO<String,String>>>(page,HttpStatus.OK);
	
	}

	public abstract DOPage<TableRowMapDO<String, String>> getWithFilter(TableRowMapDO<String, String> filterMap, Pageable pageRequest); 
	
	/**
     * Web service endpoint to fetch all StaticCodeDecode entities (Used)
     * 
     * @return A ResponseEntity containing a Collection of StaticCodeDecode domain objects.
     */
    @RequestMapping(
            value = "/get",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TableRowMapDO<String,String>> get(@RequestBody TableRowMapDO<String, String> idmap) {
        logger.info("> get");
        
        TableRowMapDO<String,String> datamap = find(idmap);

        logger.info("< get");
        
        return  new ResponseEntity<TableRowMapDO<String,String>>(datamap,HttpStatus.OK);
    }

    public abstract TableRowMapDO<String,String> find(TableRowMapDO<String, String> idmap);
}
