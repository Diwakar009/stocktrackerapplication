/*
 * Copyright (c) 2020.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.jpa.spring.extended.extention.impl;

import com.stocktracker.jpa.spring.extended.bean.StockDetailsSummary;
import com.stocktracker.jpa.spring.extended.extention.StockDetailsExtendedRepository;
import com.stocktracker.jpa.spring.extended.extention.StockExtendedRepository;
import com.stocktracker.model.sms.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.*;

/**
 * Created by diwakar009 on 22/10/2019.
 */
@Repository
public class StockExtendedRepositoryImpl implements StockExtendedRepository {

    private final Logger logger = LoggerFactory.getLogger(StockExtendedRepositoryImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Page<Stock> getStockBalanceByFilter(String industryCodeFilter, String stockCodeFilter, String stockVarietyCodeFilter, Pageable pageRequest) {
        logger.info("Executing getStockBalanceByFilter...");
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();

        CriteriaQuery<Stock> mainCreteriaQuery = criteriaBuilder.createQuery(Stock.class);
        CriteriaQuery<Long> countCriteriaQuery = criteriaBuilder.createQuery(Long.class);

        Root<Stock> mainRoot = mainCreteriaQuery.from(Stock.class);
        Root<Stock> countRoot = countCriteriaQuery.from(Stock.class);

        mainCreteriaQuery.select(criteriaBuilder.construct(Stock.class,mainRoot.get(Stock_.id)
                , mainRoot.get(Stock_.totalNoOfPackets)
                , mainRoot.get(Stock_.totalWeightment)
                , mainRoot.get(Stock_.asOnDate)
        ));

        Optional<ParameterExpression<String>> icFilter = Optional.empty();
        Optional<ParameterExpression<String>> scFilter = Optional.empty();
        Optional<ParameterExpression<String>> svcFilter = Optional.empty();

        List<Predicate> predicates = new ArrayList<Predicate>();

        //predicates.add(criteriaBuilder.notEqual(mainRoot.get(Stock_.id).get(StockPK_.stockCode), "EXPE"));

        if(industryCodeFilter != null && !"".equals(industryCodeFilter)) {
            icFilter = Optional.ofNullable(criteriaBuilder.parameter(String.class));
            predicates.add(criteriaBuilder.equal(mainRoot.get(Stock_.id).get(StockPK_.industryCode), icFilter.get()));
        }

        if(stockCodeFilter != null && !"".equals(stockCodeFilter)) {
            scFilter = Optional.ofNullable(criteriaBuilder.parameter(String.class));
            predicates.add(criteriaBuilder.equal(mainRoot.get(Stock_.id).get(StockPK_.stockCode),scFilter.get()));
        }

        if(stockVarietyCodeFilter != null && !"".equals(stockVarietyCodeFilter)) {
            svcFilter = Optional.ofNullable(criteriaBuilder.parameter(String.class));
            predicates.add(criteriaBuilder.equal(mainRoot.get(Stock_.id).get(StockPK_.stockVarietyCode),svcFilter.get()));
        }

        mainCreteriaQuery.where(predicates.toArray(new Predicate[predicates.size()])); // Apply where clause
        mainCreteriaQuery.orderBy(criteriaBuilder.desc(mainRoot.get(Stock_.id).get(StockPK_.industryCode)),
                criteriaBuilder.desc(mainRoot.get(Stock_.id).get(StockPK_.stockCode)),
                criteriaBuilder.desc(mainRoot.get(Stock_.id).get(StockPK_.stockVarietyCode)));

        TypedQuery<Stock> mainQuery = entityManager.createQuery(mainCreteriaQuery);

        // Count Query
        countCriteriaQuery.select(criteriaBuilder.count(countRoot));
        countCriteriaQuery.where(predicates.toArray(new Predicate[predicates.size()])); // Apply where clause
        TypedQuery<Long> countQuery = entityManager.createQuery(countCriteriaQuery);

        if(icFilter.isPresent()) {
            mainQuery.setParameter(icFilter.get(), industryCodeFilter);
            countQuery.setParameter(icFilter.get(), industryCodeFilter);
        }

        if(scFilter.isPresent()) {
            mainQuery.setParameter(scFilter.get(), stockCodeFilter);
            countQuery.setParameter(scFilter.get(), stockCodeFilter);
        }

        if(svcFilter.isPresent()) {
            mainQuery.setParameter(svcFilter.get(), stockVarietyCodeFilter);
            countQuery.setParameter(svcFilter.get(), stockVarietyCodeFilter);
        }
        long total = countQuery.getSingleResult();
        return (Page)(pageRequest == null?new PageImpl(mainQuery.getResultList()):this.readPage(mainQuery,pageRequest, total));

    }

    protected Page<?> readPage(TypedQuery<?> query, Pageable pageable, Long total) {
        query.setFirstResult((int) pageable.getOffset());
        query.setMaxResults(pageable.getPageSize());
        List<?> content = total.longValue() > (long)pageable.getOffset()?query.getResultList(): Collections.emptyList();
        return new PageImpl(content, pageable, total.longValue());
    }

}
