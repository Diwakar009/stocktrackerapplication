package com.stocktracker.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.stocktracker.framework.IEntity;
import org.hibernate.annotations.Cascade;


/**
 * The persistent class for the doc_content database table.
 * 
 */
@Entity
@Table(name="doc_content")
@NamedQueries({
    @NamedQuery(name = DocContent.FIND_ALL,
            query="SELECT d FROM DocContent d"),
            @NamedQuery(name = DocContent.FIND_BY_DOCHOLDER_ID,
            query="SELECT d FROM DocContent d WHERE d.docHolder.id = :docHolderId"),
    @NamedQuery(name = DocContent.FIND_BY_DOCCONTENT_LIKE_ALL,
            query="SELECT d FROM DocContent d WHERE d.docHolder.id = :docHolderId and (d.docContentName LIKE :docContentName OR d.docContentKeywords LIKE :docContentKeywords) ORDER BY d.docContentName")
})
public class DocContent extends BaseEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	public static String PREFIX_NAMEDQUERY = "DocContent.";

	public static final String FIND_ALL = "DocContent.findAll";
	public static final String FIND_BY_DOCHOLDER_ID = "DocContent.findByDocHolderId";
	public static final String FIND_BY_DOCCONTENT_LIKE_ALL = "DocContent.findByDocHolderLikeAll";
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "DOC_CONTENT_ID", nullable = false, columnDefinition = "BIGINT UNSIGNED")
	private Long id;

	@Column(name="DOC_CONTENT_KEYWORDS", length = 1000)
	private String docContentKeywords;

	@Column(name="DOC_CONTENT_NAME", length = 100)
	private String docContentName;

	/*
	@Column(name="DOC_CONTENT_SEQ")
	private BigInteger docContentSeq;
	*/
	@Column(name="DOC_FILE_EXTENTION", length = 20)
	private String docFileExtension;
	
	@Column(name="DOC_FILE_NAME", length = 100)
	private String docFileName;

	@Column(name="DOC_DIR_PATH", length = 200)
	private String docDirPath;
	
	@Column(name = "DOC_LASTMODIFIED_TIME")
	@Temporal(TemporalType.TIMESTAMP)
	private Date docLastModifiedTime;
	
	//bi-directional many-to-one association to DocHolder
	@ManyToOne
	@JoinColumn(name="DOC_ID")
	private DocHolder docHolder;
	
	@OneToMany(mappedBy="docContent",fetch=FetchType.LAZY)
	@Cascade(value = org.hibernate.annotations.CascadeType.ALL)
	private List<DocContentBlob> docContentBlobs;
	
 
	public DocContent() {
		docContentBlobs = new ArrayList<DocContentBlob>();
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	public String getDocContentKeywords() {
		return this.docContentKeywords;
	}

	public void setDocContentKeywords(String docContentKeywords) {
		this.docContentKeywords = docContentKeywords;
	}

	public String getDocContentName() {
		return this.docContentName;
	}

	public void setDocContentName(String docContentName) {
		this.docContentName = docContentName;
	}
	
	/*
	public BigInteger getDocContentSeq() {
		return this.docContentSeq;
	}

	public void setDocContentSeq(BigInteger docContentSeq) {
		this.docContentSeq = docContentSeq;
	}
	*/
	

 	public String getDocFileExtension() {
		return docFileExtension;
	}

	public void setDocFileExtension(String docFileExtension) {
		this.docFileExtension = docFileExtension;
	}

	public String getDocFileName() {
		return docFileName;
	}

	public void setDocFileName(String docFileName) {
		this.docFileName = docFileName;
	}

	public String getDocDirPath() {
		return docDirPath;
	}

	public void setDocDirPath(String docFilePath) {
		this.docDirPath = docFilePath;
	}

	public DocHolder getDocHolder() {
		return this.docHolder;
	}

	public void setDocHolder(DocHolder docHolder) {
		this.docHolder = docHolder;
	}
	
	public List<DocContentBlob> getDocContentBlobs() {
		return docContentBlobs;
	}

	public void setDocContentBlobs(List<DocContentBlob> docContentBlobs) {
		this.docContentBlobs = docContentBlobs;
	}
	
	public Date getDocLastModifiedTime() {
	    return docLastModifiedTime;
	}

	public void setDocLastModifiedTime(Date docLastModifiedTime) {
	    this.docLastModifiedTime = docLastModifiedTime;
	}

	public DocContentBlob addDocContentBlob(DocContentBlob docContentBlob) {
		getDocContentBlobs().add(docContentBlob);
		docContentBlob.setDocContent(this);

		return docContentBlob;
	}
	
	public DocContentBlob removeDocContentBlob(DocContentBlob docContentBlob) {
		getDocContentBlobs().remove(docContentBlob);
		docContentBlob.setDocContent(null);

		return docContentBlob;
	}

	@Override
	public Serializable getEntityPK() {
		return getId();
	}

	@Override
	public void copyEntity(IEntity entity) {
		DocContent source = (DocContent)entity;
		this.setCreatedBy(source.getCreatedBy());
		this.setUpdatedBy(source.getUpdatedBy());
		this.setCreatedAt(source.getCreatedAt());
		this.setUpdatedAt(source.getUpdatedAt());
		this.setId(source.getId());

		this.setDocDirPath(source.getDocDirPath());
		this.setDocLastModifiedTime(source.getDocLastModifiedTime());
		this.setDocContentName(source.getDocContentName());
		this.setDocContentKeywords(source.getDocContentKeywords());
		this.setDocFileExtension(source.getDocFileExtension());
		this.setDocFileName(source.getDocFileName());

		DocHolder holder = new DocHolder();
		holder.setId(source.getDocHolder().getId());
		this.setDocHolder(holder);

	}



	
	
}