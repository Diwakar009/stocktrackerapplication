/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.domainobjs.json.sms;

import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import java.io.Serializable;


/**
 * IndustryOutturnReportDO class
 * 
 */
@JsonIgnoreProperties (ignoreUnknown=true)
@JsonInclude(Include.NON_EMPTY)
public class IndustryOutturnReportDO implements Serializable {
	private static final long serialVersionUID = 1L;
	public static String CLASS_NAME = IndustryOutturnReportDO.class.getName();

	@JsonProperty("industryName")
	private String industryName;

	@JsonProperty("stockCode")
	@JsonIgnore
	private String stockCode;

	@JsonProperty("weightmentPerLAT")
	private Long weightmentPerLAT = 290l;

	@JsonProperty("totalMandiRecvPaddyInWmt")
	private Double totalMandiRecvPaddyInWmt;

	@JsonProperty("totalMandiRecvPaddyInPkt")
	private Long totalMandiRecvPaddyInPkt;

	@JsonProperty("totalMandiAdjPaddyInWmt")
	private Double totalMandiAdjPaddyInWmt;

	@JsonProperty("totalMandiAdjPaddyAmt")
	private Double totalMandiAdjPaddyAmt;

	@JsonProperty("totalPrivateRecvPaddyInWmt")
	private Double totalPrivateRecvPaddyInWmt;

	@JsonProperty("totalPrivateRecvPaddyInPkt")
	private Long totalPrivateRecvPaddyInPkt;

	@JsonProperty("totalPaddyOfferInPkt")
	private Long totalPaddyOfferInPkt;

	@JsonProperty("totalPaddyOfferInWmt")
	private Double totalPaddyOfferInWmt;

	@JsonProperty("totalRiceToBeDeliveredInWmt")
	private Double totalRiceToBeDeliveredInWmt;

	@JsonProperty("totalRiceProducedInPkt")
	private Long totalRiceProducedInPkt;

	@JsonProperty("totalRiceProducedInWmt")
	private Double totalRiceProducedInWmt;

	@JsonProperty("totalBrokenRiceProducedInPkt")
	private Long totalBrokenRiceProducedInPkt;

	@JsonProperty("totalBrokenRiceProducedInWmt")
	private Double totalBrokenRiceProducedInWmt;

	@JsonProperty("totalBranBrokenProducedInPkt")
	private Long totalBranBrokenProducedInPkt;

	@JsonProperty("totalBranBrokenProducedInWmt")
	private Double totalBranBrokenProducedInWmt;

	@JsonProperty("totalBranProducedInPkt")
	private Long totalBranProducedInPkt;

	@JsonProperty("totalBranProducedInWmt")
	private Double totalBranProducedInWmt;

	@JsonProperty("totalRunningStockInPkt")
	private Long totalRunningStockInPkt;

	@JsonProperty("totalRunningStockInWmt")
	private Double totalRunningStockInWmt;

	@JsonProperty("totalHuskProducedInWmt")
	private Double totalHuskProducedInWmt;

	public String getIndustryName() {
		return industryName;
	}

	public void setIndustryName(String industryName) {
		this.industryName = industryName;
	}

	public Long getWeightmentPerLAT() {
		return weightmentPerLAT;
	}

	public void setWeightmentPerLAT(Long weightmentPerLAT) {
		this.weightmentPerLAT = weightmentPerLAT;
	}

	public Double getTotalMandiRecvPaddyInWmt() {
		return totalMandiRecvPaddyInWmt;
	}

	public void setTotalMandiRecvPaddyInWmt(Double totalMandiRecvPaddyInWmt) {
		this.totalMandiRecvPaddyInWmt = totalMandiRecvPaddyInWmt;
	}

	public Double getTotalPrivateRecvPaddyInWmt() {
		return totalPrivateRecvPaddyInWmt;
	}

	public void setTotalPrivateRecvPaddyInWmt(Double totalPrivateRecvPaddyInWmt) {
		this.totalPrivateRecvPaddyInWmt = totalPrivateRecvPaddyInWmt;
	}

	public Long getTotalPrivateRecvPaddyInPkt() {
		return totalPrivateRecvPaddyInPkt;
	}

	public void setTotalPrivateRecvPaddyInPkt(Long totalPrivateRecvPaddyInPkt) {
		this.totalPrivateRecvPaddyInPkt = totalPrivateRecvPaddyInPkt;
	}

	public Long getTotalPaddyOfferInPkt() {
		return totalPaddyOfferInPkt;
	}

	public void setTotalPaddyOfferInPkt(Long totalPaddyOfferInPkt) {
		this.totalPaddyOfferInPkt = totalPaddyOfferInPkt;
	}

	public Double getTotalPaddyOfferInWmt() {
		return totalPaddyOfferInWmt;
	}

	public void setTotalPaddyOfferInWmt(Double totalPaddyOfferInWmt) {
		this.totalPaddyOfferInWmt = totalPaddyOfferInWmt;
	}

	public Double getTotalRiceToBeDeliveredInWmt() {
		return totalRiceToBeDeliveredInWmt;
	}

	public void setTotalRiceToBeDeliveredInWmt(Double totalRiceToBeDeliveredInWmt) {
		this.totalRiceToBeDeliveredInWmt = totalRiceToBeDeliveredInWmt;
	}

	public Long getTotalRiceProducedInPkt() {
		return totalRiceProducedInPkt;
	}

	public void setTotalRiceProducedInPkt(Long totalRiceProducedInPkt) {
		this.totalRiceProducedInPkt = totalRiceProducedInPkt;
	}

	public Double getTotalRiceProducedInWmt() {
		return totalRiceProducedInWmt;
	}

	public void setTotalRiceProducedInWmt(Double totalRiceProducedInWmt) {
		this.totalRiceProducedInWmt = totalRiceProducedInWmt;
	}

	public Long getTotalBrokenRiceProducedInPkt() {
		return totalBrokenRiceProducedInPkt;
	}

	public void setTotalBrokenRiceProducedInPkt(Long totalBrokenRiceProducedInPkt) {
		this.totalBrokenRiceProducedInPkt = totalBrokenRiceProducedInPkt;
	}

	public Double getTotalBrokenRiceProducedInWmt() {
		return totalBrokenRiceProducedInWmt;
	}

	public void setTotalBrokenRiceProducedInWmt(Double totalBrokenRiceProducedInWmt) {
		this.totalBrokenRiceProducedInWmt = totalBrokenRiceProducedInWmt;
	}

	public Long getTotalBranBrokenProducedInPkt() {
		return totalBranBrokenProducedInPkt;
	}

	public void setTotalBranBrokenProducedInPkt(Long totalBranBrokenProducedInPkt) {
		this.totalBranBrokenProducedInPkt = totalBranBrokenProducedInPkt;
	}

	public Double getTotalBranBrokenProducedInWmt() {
		return totalBranBrokenProducedInWmt;
	}

	public void setTotalBranBrokenProducedInWmt(Double totalBranBrokenProducedInWmt) {
		this.totalBranBrokenProducedInWmt = totalBranBrokenProducedInWmt;
	}

	public Long getTotalBranProducedInPkt() {
		return totalBranProducedInPkt;
	}

	public void setTotalBranProducedInPkt(Long totalBranProducedInPkt) {
		this.totalBranProducedInPkt = totalBranProducedInPkt;
	}

	public Double getTotalBranProducedInWmt() {
		return totalBranProducedInWmt;
	}

	public void setTotalBranProducedInWmt(Double totalBranProducedInWmt) {
		this.totalBranProducedInWmt = totalBranProducedInWmt;
	}

	public Long getTotalRunningStockInPkt() {
		return totalRunningStockInPkt;
	}

	public void setTotalRunningStockInPkt(Long totalRunningStockInPkt) {
		this.totalRunningStockInPkt = totalRunningStockInPkt;
	}

	public Double getTotalRunningStockInWmt() {
		return totalRunningStockInWmt;
	}

	public void setTotalRunningStockInWmt(Double totalRunningStockInWmt) {
		this.totalRunningStockInWmt = totalRunningStockInWmt;
	}

	public String getStockCode() {
		return stockCode;
	}

	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}

	public Long getTotalMandiRecvPaddyInPkt() {
		return totalMandiRecvPaddyInPkt;
	}

	public void setTotalMandiRecvPaddyInPkt(Long totalMandiRecvPaddyInPkt) {
		this.totalMandiRecvPaddyInPkt = totalMandiRecvPaddyInPkt;
	}

	public Double getTotalHuskProducedInWmt() {
		return totalHuskProducedInWmt;
	}

	public void setTotalHuskProducedInWmt(Double totalHuskProducedInWmt) {
		this.totalHuskProducedInWmt = totalHuskProducedInWmt;
	}

	public Double getTotalMandiAdjPaddyInWmt() {
		return totalMandiAdjPaddyInWmt;
	}

	public void setTotalMandiAdjPaddyInWmt(Double totalMandiAdjPaddyInWmt) {
		this.totalMandiAdjPaddyInWmt = totalMandiAdjPaddyInWmt;
	}

	public Double getTotalMandiAdjPaddyAmt() {
		return totalMandiAdjPaddyAmt;
	}

	public void setTotalMandiAdjPaddyAmt(Double totalMandiAdjPaddyAmt) {
		this.totalMandiAdjPaddyAmt = totalMandiAdjPaddyAmt;
	}

	public void copyDo(IndustryOutturnReportDO reportDO){

		if(reportDO.getIndustryName() != null)
			setIndustryName(reportDO.getIndustryName());

		if(reportDO.getTotalMandiRecvPaddyInWmt() != null)
			setTotalMandiRecvPaddyInWmt(reportDO.getTotalMandiRecvPaddyInWmt());

		if(reportDO.getTotalMandiRecvPaddyInPkt() != null)
			setTotalMandiRecvPaddyInPkt(reportDO.getTotalMandiRecvPaddyInPkt());

		if(reportDO.getTotalPrivateRecvPaddyInWmt() != null)
			setTotalPrivateRecvPaddyInWmt(reportDO.getTotalPrivateRecvPaddyInWmt());

		if(reportDO.getTotalPrivateRecvPaddyInPkt() != null)
			setTotalPrivateRecvPaddyInPkt(reportDO.getTotalPrivateRecvPaddyInPkt());

		if(reportDO.getTotalPaddyOfferInPkt() != null)
			setTotalPaddyOfferInPkt(reportDO.getTotalPaddyOfferInPkt());

		if(reportDO.getTotalPaddyOfferInWmt() != null)
			setTotalPaddyOfferInWmt(reportDO.getTotalPaddyOfferInWmt());

		if(reportDO.getTotalRiceToBeDeliveredInWmt() != null)
			setTotalRiceToBeDeliveredInWmt(reportDO.getTotalRiceToBeDeliveredInWmt());

		if(reportDO.getTotalRiceProducedInPkt() != null)
			setTotalRiceProducedInPkt(reportDO.getTotalRiceProducedInPkt());

		if(reportDO.getTotalRiceProducedInWmt() != null)
			setTotalRiceProducedInWmt(reportDO.getTotalRiceProducedInWmt());

		if(reportDO.getTotalBrokenRiceProducedInPkt() != null)
			setTotalBrokenRiceProducedInPkt(reportDO.getTotalBrokenRiceProducedInPkt());

		if(reportDO.getTotalBrokenRiceProducedInWmt() != null)
			setTotalBrokenRiceProducedInWmt(reportDO.getTotalBrokenRiceProducedInWmt());

		if(reportDO.getTotalBranBrokenProducedInPkt() != null)
			setTotalBranBrokenProducedInPkt(reportDO.getTotalBranBrokenProducedInPkt());

		if(reportDO.getTotalBranBrokenProducedInWmt() != null)
			setTotalBranBrokenProducedInWmt(reportDO.getTotalBranBrokenProducedInWmt());

		if(reportDO.getTotalBranProducedInPkt() != null)
			setTotalBranProducedInPkt(reportDO.getTotalBranProducedInPkt());

		if(reportDO.getTotalBranProducedInWmt() != null)
			setTotalBranProducedInWmt(reportDO.getTotalBranProducedInWmt());

		if(reportDO.getTotalRunningStockInPkt() != null)
			setTotalRunningStockInPkt(reportDO.getTotalRunningStockInPkt());

		if(reportDO.getTotalRunningStockInWmt() != null)
			setTotalRunningStockInWmt(reportDO.getTotalRunningStockInWmt());

		if(reportDO.getTotalHuskProducedInWmt() != null)
			setTotalHuskProducedInWmt(reportDO.getTotalHuskProducedInWmt());

		if(reportDO.getTotalMandiAdjPaddyInWmt() != null)
			setTotalMandiAdjPaddyInWmt(reportDO.getTotalMandiAdjPaddyInWmt());

		if(reportDO.getTotalMandiAdjPaddyAmt() != null)
			setTotalMandiAdjPaddyAmt(reportDO.getTotalMandiAdjPaddyAmt());

	}

	@Override
	public String toString() {
		return "IndustryOutturnReportDO{" +
				"industryName='" + industryName + '\'' +
				", stockCode='" + stockCode + '\'' +
				", weightmentPerLAT=" + weightmentPerLAT +
				", totalMandiRecvPaddyInWmt=" + totalMandiRecvPaddyInWmt +
				", totalMandiRecvPaddyInPkt=" + totalMandiRecvPaddyInPkt +
				", totalMandiAdjPaddyInWmt=" + totalMandiAdjPaddyInWmt +
				", totalMandiAdjPaddyAmt=" + totalMandiAdjPaddyAmt +
				", totalPrivateRecvPaddyInWmt=" + totalPrivateRecvPaddyInWmt +
				", totalPrivateRecvPaddyInPkt=" + totalPrivateRecvPaddyInPkt +
				", totalPaddyOfferInPkt=" + totalPaddyOfferInPkt +
				", totalPaddyOfferInWmt=" + totalPaddyOfferInWmt +
				", totalRiceToBeDeliveredInWmt=" + totalRiceToBeDeliveredInWmt +
				", totalRiceProducedInPkt=" + totalRiceProducedInPkt +
				", totalRiceProducedInWmt=" + totalRiceProducedInWmt +
				", totalBrokenRiceProducedInPkt=" + totalBrokenRiceProducedInPkt +
				", totalBrokenRiceProducedInWmt=" + totalBrokenRiceProducedInWmt +
				", totalBranBrokenProducedInPkt=" + totalBranBrokenProducedInPkt +
				", totalBranBrokenProducedInWmt=" + totalBranBrokenProducedInWmt +
				", totalBranProducedInPkt=" + totalBranProducedInPkt +
				", totalBranProducedInWmt=" + totalBranProducedInWmt +
				", totalRunningStockInPkt=" + totalRunningStockInPkt +
				", totalRunningStockInWmt=" + totalRunningStockInWmt +
				", totalHuskProducedInWmt=" + totalHuskProducedInWmt +
				'}';
	}
}