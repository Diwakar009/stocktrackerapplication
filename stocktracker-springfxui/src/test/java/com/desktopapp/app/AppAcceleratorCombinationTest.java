package com.desktopapp.app;

import com.desktopapp.fx.app.AppAccelerator;
import com.desktopapp.fx.config.AppConfigTest;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Created by diwakar009 on 22/6/2019.
 */
/*@RunWith(SpringRunner.class)
@SpringBootTest(classes = {AppConfigTest.class })*/
public class AppAcceleratorCombinationTest {

    //@Test
    public void keyCodeTest() throws Exception {
        Assert.assertEquals("Alt+Q", AppAccelerator.getKeyCode("ALT-Q").getDisplayText());
        Assert.assertEquals("Alt+Z", AppAccelerator.getKeyCode("ALT-Z").getDisplayText());
        Assert.assertEquals("F2", AppAccelerator.getKeyCode("F2").getDisplayText());
        Assert.assertEquals("Ctrl+Z", AppAccelerator.getKeyCode("CTRL-Z").getDisplayText());
    }
}
