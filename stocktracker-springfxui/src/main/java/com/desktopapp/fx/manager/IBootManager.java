package com.desktopapp.fx.manager;

import com.desktopapp.fx.objectfactory.ObjectFactory;

import java.util.Map;

/**
 * Created by diwakar009 on 8/6/2019.
 */
public interface IBootManager {
    public final static String INITIAL_STAGE = "INITIAL_STAGE";
    public final static String WINDOW_EVENT = "WINDOW_EVENT";

    public void startApplication(Map<String,Object> appProperties);
    public void exitApplication(Map<String,Object> appProperties);

    public static FXBootManager getProductBootManager(){
        return (FXBootManager) ObjectFactory.getProductBootManager();
    }
}
