/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.model.sms;

import com.stocktracker.framework.IEntity;
import com.stocktracker.model.BaseEntity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * The persistent class for the TBL_PARTY_DTLS database table.
 * 
 */
@Entity
@Table(name = "TBL_PARTY_DTLS")
@AttributeOverride(name = "id", column = @Column(name = "ID", nullable = false,
		columnDefinition = "BIGINT UNSIGNED"))
public class PartyDetails extends BaseEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	public PartyDetails() {
	}

	public PartyDetails(String partyName1, String partyName2, String partyName3) {
		this.partyName1 = partyName1;
		this.partyName2 = partyName2;
		this.partyName3 = partyName3;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "ID", nullable = false, columnDefinition = "BIGINT UNSIGNED")
	private Long id;

	/*@Column(name="INDUSTRY_CODE", length = 5)
	private String industryCode;

	@Column(name="STOCK_CODE", length = 5)
	private String stockCode;

	@Column(name="STOCK_VARIETIES_CODE", length = 5)
	private String stockVarietyCode;*/

	@Column(name="PARTY_TYPE_CODE", length = 5)
	private String partyTypeCode;

	@Column(name="OWN_INDUSTRY_CODE", length = 5)
	private String ownIndustryCode;

	@Column(name="PARTY_NAME_1", length = 50)
	private String partyName1;

	@Column(name="PARTY_NAME_2", length = 50)
	private String partyName2;

	@Column(name="PARTY_NAME_3", length = 50)
	private String partyName3;

	/*@Column(name="ONBEHALF", length = 5)
	private String onBehalf;*/

	@Column(name="MORE_DETAILS", length = 65535)
	private String moreDetails;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	/*public String getIndustryCode() {
		return industryCode;
	}

	public void setIndustryCode(String industryCode) {
		this.industryCode = industryCode;
	}

	public String getStockCode() {
		return stockCode;
	}

	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}

	public String getStockVarietyCode() {
		return stockVarietyCode;
	}

	public void setStockVarietyCode(String stockVarietyCode) {
		this.stockVarietyCode = stockVarietyCode;
	}*/

	public String getPartyTypeCode() {
		return partyTypeCode;
	}

	public void setPartyTypeCode(String partyTypeCode) {
		this.partyTypeCode = partyTypeCode;
	}

	public String getOwnIndustryCode() {
		return ownIndustryCode;
	}

	public void setOwnIndustryCode(String ownIndustryCode) {
		this.ownIndustryCode = ownIndustryCode;
	}

	public String getPartyName1() {
		return partyName1;
	}

	public void setPartyName1(String partyName1) {
		this.partyName1 = partyName1;
	}

	public String getPartyName2() {
		return partyName2;
	}

	public void setPartyName2(String partyName2) {
		this.partyName2 = partyName2;
	}

	public String getPartyName3() {
		return partyName3;
	}

	public void setPartyName3(String partyName3) {
		this.partyName3 = partyName3;
	}

	/*public String getOnBehalf() {
		return onBehalf;
	}

	public void setOnBehalf(String onBehalf) {
		this.onBehalf = onBehalf;
	}*/

	public String getMoreDetails() {
		return moreDetails;
	}

	public void setMoreDetails(String moreDetails) {
		this.moreDetails = moreDetails;
	}

	@Override
	public Serializable getEntityPK() {
		return id;
	}

	@Override
	public void copyEntity(IEntity entity) {
		PartyDetails source = (PartyDetails)entity;
		this.setCreatedBy(source.getCreatedBy());
		this.setUpdatedBy(source.getUpdatedBy());
		this.setCreatedAt(source.getCreatedAt());
		this.setUpdatedAt(source.getUpdatedAt());

		this.setId(source.getId());
		/*this.setIndustryCode(source.getIndustryCode());
		this.setStockCode(source.getStockCode());
		this.setStockVarietyCode(source.getStockVarietyCode());*/
		this.setPartyTypeCode(source.getPartyTypeCode());
		this.setOwnIndustryCode(source.getOwnIndustryCode());
		this.setPartyName1(source.getPartyName1());
		this.setPartyName2(source.getPartyName2());
		this.setPartyName3(source.getPartyName3());
		/*this.setOnBehalf(source.getOnBehalf());*/
		this.setMoreDetails(source.getMoreDetails());
	}
}