/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.desktopapp.bean;

import com.stocktracker.domainobjs.json.sms.StockDetailsDO;

import java.util.Comparator;

/**
 * Created by diwakar009 on 5/11/2019.
 */
public class StockChangeIndentier implements Comparable <StockChangeIndentier> {

    private String industryCode;
    private String stockCode;
    private String stockVarietyCode;
    private String transactionType;
    private Long numberOfPackets;
    private Double weightment;

    public StockChangeIndentier(StockDetailsDO stockDetailsDO) {
        this.setIndustryCode(stockDetailsDO.getIndustryCode());
        this.setStockCode(stockDetailsDO.getStockCode());
        this.setStockVarietyCode(stockDetailsDO.getStockVarietyCode());
        this.setTransactionType(stockDetailsDO.getTransactionType());
        this.setNumberOfPackets(stockDetailsDO.getNoOfPackets());
        this.setWeightment(stockDetailsDO.getTotalWeightment());
    }

    public String getIndustryCode() {
        return industryCode;
    }

    public void setIndustryCode(String industryCode) {
        this.industryCode = industryCode;
    }

    public String getStockCode() {
        return stockCode;
    }

    public void setStockCode(String stockCode) {
        this.stockCode = stockCode;
    }

    public String getStockVarietyCode() {
        return stockVarietyCode;
    }

    public void setStockVarietyCode(String stockVarietyCode) {
        this.stockVarietyCode = stockVarietyCode;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public Long getNumberOfPackets() {
        return numberOfPackets;
    }

    public void setNumberOfPackets(Long numberOfPackets) {
        this.numberOfPackets = numberOfPackets;
    }

    public Double getWeightment() {
        return weightment;
    }

    public void setWeightment(Double weightment) {
        this.weightment = weightment;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        StockChangeIndentier that = (StockChangeIndentier) o;

        if (industryCode != null ? !industryCode.equals(that.industryCode) : that.industryCode != null) return false;
        if (stockCode != null ? !stockCode.equals(that.stockCode) : that.stockCode != null) return false;
        if (stockVarietyCode != null ? !stockVarietyCode.equals(that.stockVarietyCode) : that.stockVarietyCode != null)
            return false;
        if (transactionType != null ? !transactionType.equals(that.transactionType) : that.transactionType != null)
            return false;
        if (numberOfPackets != null ? !numberOfPackets.equals(that.numberOfPackets) : that.numberOfPackets != null)
            return false;
        return weightment != null ? weightment.equals(that.weightment) : that.weightment == null;
    }

    @Override
    public int hashCode() {
        int result = industryCode != null ? industryCode.hashCode() : 0;
        result = 31 * result + (stockCode != null ? stockCode.hashCode() : 0);
        result = 31 * result + (stockVarietyCode != null ? stockVarietyCode.hashCode() : 0);
        result = 31 * result + (transactionType != null ? transactionType.hashCode() : 0);
        result = 31 * result + (numberOfPackets != null ? numberOfPackets.hashCode() : 0);
        result = 31 * result + (weightment != null ? weightment.hashCode() : 0);
        return result;
    }

    @Override
    public int compareTo(StockChangeIndentier o) {

        if(this.equals(o)){
            return 0;
        }

        return -1;

    }
}
