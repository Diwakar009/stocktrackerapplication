/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.model.sms;

import com.stocktracker.framework.IEntity;
import com.stocktracker.model.BaseEntity;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;


/**
 * The persistent class for the TBL_INDUSTRY database table.
 * 
 */
@Entity
@Table(name="TBL_INDUSTRY")
public class Industry extends BaseEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private IndustryPK id;

	@Column(name="INDUSTRY_TYPE", length = 5)
	private String industryType;

	@Column(name="INDUSTRY_OWNER", length = 40)
	private String industryOwner;

	@Column(name="INDUSTRY_STATUS", length = 5)
	private String industryStatus;

	@Column(name="MORE_DETAILS", length = 65535)
	private String moreDetails;

	public IndustryPK getId() {
		return id;
	}

	public void setId(IndustryPK id) {
		this.id = id;
	}

	public void setIndustryCode(String industryCode){
		if(getId() == null){
			this.id = new IndustryPK();
		}
		this.id.setIndustryCode(industryCode);
	}

	public String getIndustryCode() {
		if(getId() == null){
			this.id = new IndustryPK();
		}
		return this.id.getIndustryCode();
	}

	public String getIndustryType() {
		return industryType;
	}

	public void setIndustryType(String industryType) {
		this.industryType = industryType;
	}

	public String getIndustryOwner() {
		return industryOwner;
	}

	public void setIndustryOwner(String industryOwner) {
		this.industryOwner = industryOwner;
	}

	public String getIndustryStatus() {
		return industryStatus;
	}

	public void setIndustryStatus(String industryStatus) {
		this.industryStatus = industryStatus;
	}

	public String getMoreDetails() {
		return moreDetails;
	}

	public void setMoreDetails(String moreDetails) {
		this.moreDetails = moreDetails;
	}

	@Override
	public Serializable getEntityPK() {
		return id;
	}

	@Override
	public void copyEntity(IEntity entity) {
		Industry source = (Industry)entity;
		this.setCreatedBy(source.getCreatedBy());
		this.setUpdatedBy(source.getUpdatedBy());
		this.setCreatedAt(source.getCreatedAt());
		this.setUpdatedAt(source.getUpdatedAt());
		this.setIndustryOwner(source.getIndustryOwner());
		this.setIndustryType(source.getIndustryType());
		this.setIndustryStatus(source.getIndustryStatus());
		this.setMoreDetails(source.getMoreDetails());
	}

	@Override
	public String toString() {
		return "Industry{" +
				"id=" + id +
				", industryType='" + industryType + '\'' +
				", industryOwner='" + industryOwner + '\'' +
				", industryStatus='" + industryStatus + '\'' +
				", moreDetails='" + moreDetails + '\'' +
				'}';
	}
}