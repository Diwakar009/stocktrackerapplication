/*
 * Copyright (C) 2017 Cem Ikta
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 *
 */

package com.desktopapp.fx.controller;


import com.desktopapp.fx.mvc.AbstractDataPageController;
import com.desktopapp.fx.mvc.DataPageView;
import com.desktopapp.fx.view.AppUserForm;
import com.desktopapp.fx.view.AppUserPage;
import com.desktopapp.service.AbstractService;
import com.desktopapp.service.AppUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.stocktracker.domainobjs.json.AppUserDO;

/**
 * User controller
 *
 * @author diwakar009
 */
@Component
public class AppUserController extends AbstractDataPageController<AppUserDO> {

    @Autowired
    private AppUserService appUserService;

    @Override
    protected AbstractService<AppUserDO> createService() {
        return appUserService;
    }

    @Override
    protected DataPageView<AppUserDO> createDataPageView() {
        return new AppUserPage();
    }

    @Override
    public void openFormView(AppUserDO user) {
        new AppUserForm(this, user).showDialog();
    }

    @Override
    public void onAddNew() {
        openFormView(new AppUserDO());
    }

    @Override
    public String getName() {
        return "AppUserController";
    }

    @Override
    public AppUserDO loadData(AppUserDO entity) {
        return appUserService.find(entity);
    }
}