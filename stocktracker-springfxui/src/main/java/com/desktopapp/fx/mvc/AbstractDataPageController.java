/*
 * Copyright (C) 2017 R Diwakar C
 *
 */

package com.desktopapp.fx.mvc;


import com.desktopapp.fx.DesktopFxApplication;
import com.desktopapp.fx.common.PageDetails;
import com.desktopapp.fx.control.MessageBox;
import com.desktopapp.fx.control.multiscreen.StackedScreen;
import com.desktopapp.fx.util.I18n;
import com.desktopapp.service.AbstractService;
import com.stocktracker.domainobjs.json.ResultDO;
import javafx.geometry.Pos;
import javafx.scene.control.ButtonType;
import org.controlsfx.control.Notifications;
import com.stocktracker.domainobjs.json.BaseDomainObject;
import com.stocktracker.domainobjs.json.DOPage;
import com.stocktracker.domainobjs.json.TableRowMapDO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;

import static com.desktopapp.service.QueryParameter.with;
import static javafx.scene.control.ButtonBar.ButtonData;


/**
 * Abstract data page controller.
 *
 * @param <T> entity
 *
 * @author diwakar009
 */
public abstract class AbstractDataPageController<T extends BaseDomainObject> implements DataPageController<T> {

    private final Logger logger = LoggerFactory.getLogger(AbstractDataPageController.class);
    private AbstractService<T> service;
    protected DataPageView<T> dataPageView;

    public AbstractDataPageController() {
    }

    @Override
    public AbstractService<T> getService() {
        logger.debug("get service");
        if (service == null) {
            service = createService();
        }
        return service;
    }

    protected abstract AbstractService<T> createService();

    @Override
    public DataPageView<T> getDataPageView() {
        logger.debug("get data page view");
        if (dataPageView == null) {
            dataPageView = createDataPageView();
            dataPageView.init(this);
            dataPageView.refreshData();
        } else {
            dataPageView.refreshData();
        }
        return dataPageView;
    }

    protected StackedScreen getMultScreenPane(){
        return (StackedScreen) dataPageView.getStackedScreen();
    }

    protected abstract DataPageView<T> createDataPageView();

    @Override
    public void onEdit() {
        logger.debug("on edit action");
        if (dataPageView == null) {
            return;
        }
        if (dataPageView.getSelectedModel() == null) {
            return;
        }
        openFormView(dataPageView.getSelectedModel());
    }

    @Override
    public void onDelete() {
        logger.debug("on delete action");
        if (dataPageView == null) {
            return;
        }
        if (dataPageView.getSelectedModel() == null) {
            return;
        }

        Optional<ButtonType> result = MessageBox.create()
                .owner(DesktopFxApplication.get().getMainStage())
                .contentText(I18n.COMMON.getString("confirm.delete"))
                .showDeleteConfirmation();

        if (result.isPresent() && result.get().getButtonData() == ButtonData.OK_DONE) {
            try {
                getService().remove(dataPageView.getSelectedModel());
                onRefresh();
                if (DesktopFxApplication.get().getLoginSession().getPreferences().isShowInfoPopups()) {
                    Notifications.create()
                            .text(I18n.COMMON.getString("notification.deleted"))
                            .position(Pos.TOP_RIGHT).showInformation();
                }
            } catch (Exception e) {
                MessageBox.create()
                        .owner(DesktopFxApplication.get().getMainStage())
                        .contentText(I18n.COMMON.getString("exception.delete"))
                        .showError(e);
            }
        }
    }

    @Override
    public void onRefresh() {
        logger.debug("on refresh action");
        if (dataPageView != null) {
            dataPageView.refreshData();
        }
    }

    @Override
    public ResultDO onSave(T entity) {
        logger.debug("on save action {}", entity);
        try {
            if (entity.isNew()) {
                entity.setCreatedBy(DesktopFxApplication.get().getLoginSession().getLoggedInUser().getUserName());
                getService().create(entity);
            } else {
                entity.setUpdatedBy(DesktopFxApplication.get().getLoginSession().getLoggedInUser().getUserName());
                getService().update(entity);
            }
            onRefresh();
            if (DesktopFxApplication.get().getLoginSession().getPreferences().isShowInfoPopups()) {
                Notifications.create()
                        .text(I18n.COMMON.getString("notification.saved"))
                        .position(Pos.TOP_RIGHT).showInformation();
            }
        } catch (Exception e) {
            logger.error("save is not successful", e);
            MessageBox.create()
                    .owner(DesktopFxApplication.get().getMainStage())
                    .contentText(I18n.COMMON.getString("exception.save"))
                    .showError(e);
        }

        return entity.getResultDO();
    }

    @Override
    public DOPage<TableRowMapDO<String,String>> getData(String filter, PageDetails pageDetails) {
        if (filter.equals("")) {
            return getService().listAll(with("page", String.valueOf(pageDetails.getPageNumber())).
                    and("size", String.valueOf(pageDetails.getSize())).
                    and("start",pageDetails.getStart()).
                    and("end",pageDetails.getEnd()).parameters());
        } else {
            return getService().listByFilter(with("search", "%" + filter + "%").
                    and("page", String.valueOf(pageDetails.getPageNumber())).
                    and("size", String.valueOf(pageDetails.getSize())).
                    and("start",pageDetails.getStart()).
                    and("end",pageDetails.getEnd()).
                    parameters());

        }
    }

    @Override
    public void onPrintPreview() {
    }

    @Override
    public void onPrint(){
    }

    @Override
    public void onPdf(){
    }

    @Override
    public void onFormPrintPreview(Long id) {
    }

    @Override
    public void onFormPrint(Long id){
    }

    @Override
    public void onFormPdf(Long id){
    }

    @Override
    public T loadData(T entity) {
        return getService().find(entity);
    }
}
