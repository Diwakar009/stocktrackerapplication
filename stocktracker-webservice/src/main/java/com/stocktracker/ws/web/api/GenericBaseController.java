package com.stocktracker.ws.web.api;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.desktopapp.service.AbstractService;
import com.stocktracker.domainobjs.json.DOPage;
import com.stocktracker.domainobjs.json.DomainObjectList;
import com.stocktracker.domainobjs.json.TableRowMapDO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 *  Abstract class for the generic services exposed for the common operations like
 *
 *  findAll
 *  findWithFilter
 *  save
 *  delete
 *  deleteList
 *
 * @author "Diwakar Choudhury"
 *
 */
public abstract class GenericBaseController<T> extends BaseController {

    /**
     * The Logger for this class.
     */
    protected Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * Web service endpoint to fetch all StaticCodeDecode entities (Used)
     *
     * @return A ResponseEntity containing a Collection of StaticCodeDecode domain objects.
     */
    @RequestMapping(
            value = "/listAll",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DOPage<TableRowMapDO<String,String>>> listAll(Pageable pageRequest) {
        logger.info("> listAll");

        /*DOPage<TableRowMapDO<String,String>> page = getService().getAllEntries(pageRequest);*/
        Map<String,String> parameters = new HashMap<String,String>();
        parameters.put("page",String.valueOf(pageRequest.getPageNumber()));
        parameters.put("size",String.valueOf(pageRequest.getPageSize()));

        DOPage<TableRowMapDO<String,String>> page = getService().listAll(parameters);

        logger.info("< listAll");
        return  new ResponseEntity<DOPage<TableRowMapDO<String,String>>>(page,HttpStatus.OK);
    }

    @RequestMapping(value="/save",method=RequestMethod.POST,consumes=MediaType.APPLICATION_JSON_VALUE,produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<T> saveObj(@RequestBody T domainObj){
        logger.info("< save");

        T tableRowMap = save(domainObj);

        logger.info("> save");
        return new ResponseEntity<T>(tableRowMap,HttpStatus.CREATED);

    }

    public abstract T save(T domainObj);

    @RequestMapping(value="/delete",method=RequestMethod.POST,consumes=MediaType.APPLICATION_JSON_VALUE,produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Boolean> deleteList(@RequestBody T rowMap){

        logger.info("< delete");
        boolean isSuccess = delete(rowMap);
        logger.info("> delete");

        if(!isSuccess){
            return new ResponseEntity<Boolean>(isSuccess,HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<Boolean>(isSuccess,HttpStatus.OK);

    }

    /**
     * Single delete
     * @param domainObj
     * @return
     */
    public abstract boolean delete(T domainObj);


    @RequestMapping(value="/deleteList",method=RequestMethod.POST,consumes=MediaType.APPLICATION_JSON_VALUE,produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Boolean> deleteObjList(@RequestBody DomainObjectList<T> domainObjList){
        logger.info("< deleteList");

        boolean isSuccess = deleteList(domainObjList.getList());

        logger.info("> deleteList");

        return new ResponseEntity<Boolean>(isSuccess,HttpStatus.OK);

    }
    public abstract boolean deleteList(List<T> domainObjList);


    @RequestMapping(
            value = "/listWithFilter",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)

    public ResponseEntity<DOPage<TableRowMapDO<String,String>>> listWithFilter(@RequestBody TableRowMapDO<String,String> filterMap){
        logger.info("> listWithFilter");

        Integer pageNumber =  1;

        if(filterMap.getValue("page") != null){
            pageNumber = Integer.valueOf(filterMap.getValue("page"));
        }else{
            if(filterMap.getPage() != null){
                pageNumber = Integer.valueOf(filterMap.getPage());
            }
        }

        Integer size = 0;

        if(filterMap.getRow().get("size") != null){
            size = Integer.valueOf(filterMap.getRow().get("size"));
        }else{
            if(filterMap.getSize() != null){
                size = Integer.valueOf(filterMap.getSize());
            }
        }
        Pageable pageRequest = PageRequest.of(pageNumber, size);
        DOPage<TableRowMapDO<String,String>> page = getService().listByFilter(getFilterCriteriaMap(filterMap,pageRequest));
        /*DOPage<TableRowMapDO<String,String>> page = getService().getFilteredEntries(filterMap,pageRequest);*/

        logger.info("< listWithFilter");
        return  new ResponseEntity<DOPage<TableRowMapDO<String,String>>>(page,HttpStatus.OK);
    }

    /**
     * Web service endpoint to fetch all StaticCodeDecode entities (Used)
     *
     * @return A ResponseEntity containing a Collection of StaticCodeDecode domain objects.
     */
    @RequestMapping(
            value = "/get",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<T> get(@RequestBody T domainObjId) {
        logger.info("> get");

        T datamap = find(domainObjId);

        logger.info("< get");

        return  new ResponseEntity<T>(datamap,HttpStatus.OK);
    }

    public Map<String,Object> getFilterCriteriaMap(TableRowMapDO<String,String> filterMap, Pageable pageRequest){
        Map<String,Object> parameters = new HashMap<String,Object>();
        parameters.put("page",String.valueOf(pageRequest.getPageNumber()));
        parameters.put("size",String.valueOf(pageRequest.getPageSize()));
        return parameters;
    }

    public abstract T find(T domainObjId);
    public abstract AbstractService getService();


}
