/*
 * Copyright (c) 2020.
 *
 * All Rights are with Diwakar C
 */

package com.desktopapp.service;

/**
 * Created by diwakar009 on 1/11/2018.
 */

import com.desktopapp.service.config.AppConfig;
import com.desktopapp.service.stockmaintenance.PartyDetailsService;
import com.stocktracker.domainobjs.json.sms.PartyDetailsDO;
import com.stocktracker.jpa.spring.extended.config.ExtendedRepositoryConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;




@RunWith(SpringRunner.class)
@SpringBootTest(classes = { AppConfig.class, ExtendedRepositoryConfig.class})
@Transactional
public class PartyDetailsServiceTest {

    @Autowired
    private PartyDetailsService partyDetailsService;

    @Test
    public void testReterivePartyNames() {
        List<PartyDetailsDO> partyNames = partyDetailsService.getPartyNameSuggestions("Beh");
        partyNames.forEach(e -> System.out.println(e.getPartyName1()));
    }



}
