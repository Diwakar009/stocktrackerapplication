/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.domainobjs.json.sms;

import com.stocktracker.domainobjs.json.BaseDomainObject;
import com.stocktracker.domainobjs.json.ResultDO;
import com.stocktracker.model.BaseEntity;
import com.stocktracker.model.sms.StockDetails;
import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import org.apache.commons.lang.StringUtils;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * StockDetailsDO .
 *
 */
@JsonIgnoreProperties(ignoreUnknown=true)
@JsonInclude(Include.NON_EMPTY)
public class StockDetailsDO extends BaseDomainObject implements Serializable {
	private static final long serialVersionUID = 1L;
	public static String CLASS_NAME = StockDetailsDO.class.getName();


	public StockDetailsDO() {
		init();
	}

	public StockDetailsDO(StockDetails stock) {
		init();
		copyEntity2DomainObject(stock);
	}

	@JsonProperty("id")
	private Long id;

	@JsonProperty("industryCode")
	private String industryCode;

	@JsonProperty("stockCode")
	private String stockCode;

	@JsonProperty("stockVarietyCode")
	private String stockVarietyCode;

	@JsonProperty("transactionType")
	private String transactionType;

	@JsonProperty("transactionDate")
	@JsonFormat(shape = JsonFormat.Shape.STRING,pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
	private Date transactionDate;

	@JsonProperty("noOfPackets")
	private Long noOfPackets;

	@JsonProperty("totalWeightment")
	private Double totalWeightment;

	@JsonProperty("plusOrMinusOnTotal")
	private Character plusOrMinusOnTotal;

	@JsonProperty("lampEntryInQtl")
	private Double lampEntryInQtl;

	@JsonProperty("lampNameCode")
	private String lampNameCode;

	@JsonProperty("mandiNameCode")
	private String mandiNameCode;

	@JsonProperty("revisitFlag")
	private String revisitFlag;


	/*@JsonProperty("packetOpeningBalance")
	private Long packetOpeningBalance;

	@JsonProperty("weighmentOpeningBalance")
	private Double weighmentOpeningBalance;

	@JsonProperty("packetClosingBalance")
	private Long packetClosingBalance;

	@JsonProperty("weightmentClosingBalance")
	private Double weightmentClosingBalance;*/

	@JsonProperty("remarks")
	private String remarks;

	@JsonProperty("onBeHalf")
	private String onBeHalf;

	@JsonProperty("moreDetails")
	private String moreDetails;

	@JsonProperty("lampEntryAdjInQtl")
	private Double lampEntryAdjInQtl;

	@JsonProperty("lampEntryAfterAdjInQtl")
	private Double lampEntryAfterAdjInQtl;

	@JsonProperty("settlementStatus")
	private String settlementStatus;

	@JsonProperty("partyDetailsDO")
	private PartyDetailsDO partyDetailsDO;

	@JsonProperty("transportDetailsDO")
	private TransportDetailsDO transportDetailsDO;

	@JsonProperty("deliveryDetailsDO")
	private DeliveryDetailsDO deliveryDetailsDO;

	@JsonProperty("costPriceDetailsDO")
	private CostPriceDetailsDO costPriceDetailsDO;

	@JsonProperty("stockDO")
	private StockDO stockDO;

	@JsonProperty("contractDO")
	private ContractDO contractDO;


	public void init(){

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getStockCode() {
		return stockCode;
	}

	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}

	public String getStockVarietyCode() {
		return stockVarietyCode;
	}

	public void setStockVarietyCode(String stockVarietyCode) {
		this.stockVarietyCode = stockVarietyCode;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public Long getNoOfPackets() {
		return noOfPackets;
	}

	public void setNoOfPackets(Long noOfPackets) {
		this.noOfPackets = noOfPackets;
	}

	public Double getTotalWeightment() {
		return totalWeightment;
	}

	public void setTotalWeightment(Double totalWeightment) {
		this.totalWeightment = totalWeightment;
	}

	public Character getPlusOrMinusOnTotal() {
		return plusOrMinusOnTotal;
	}

	public void setPlusOrMinusOnTotal(Character plusOrMinusOnTotal) {
		this.plusOrMinusOnTotal = plusOrMinusOnTotal;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getOnBeHalf() {
		return onBeHalf;
	}

	public void setOnBeHalf(String onBeHalf) {
		this.onBeHalf = onBeHalf;
	}

	public String getIndustryCode() {
		return industryCode;
	}

	public void setIndustryCode(String industryCode) {
		this.industryCode = industryCode;
	}

	public String getMoreDetails() {
		return moreDetails;
	}

	public void setMoreDetails(String moreDetails) {
		this.moreDetails = moreDetails;
	}

	public String getLampNameCode() {
		return lampNameCode;
	}

	public void setLampNameCode(String lampNameCode) {
		this.lampNameCode = lampNameCode;
	}

	public PartyDetailsDO getPartyDetailsDO() {
		return partyDetailsDO;
	}

	public void setPartyDetailsDO(PartyDetailsDO partyDetailsDO) {
		this.partyDetailsDO = partyDetailsDO;
	}

	public TransportDetailsDO getTransportDetailsDO() {
		return transportDetailsDO;
	}

	public void setTransportDetailsDO(TransportDetailsDO transportDetailsDO) {
		this.transportDetailsDO = transportDetailsDO;
	}

	public Double getLampEntryInQtl() {
		return lampEntryInQtl;
	}

	public void setLampEntryInQtl(Double lampEntryInQtl) {
		this.lampEntryInQtl = lampEntryInQtl;
	}

	public DeliveryDetailsDO getDeliveryDetailsDO() {
		return deliveryDetailsDO;
	}

	public void setDeliveryDetailsDO(DeliveryDetailsDO deliveryDetailsDO) {
		this.deliveryDetailsDO = deliveryDetailsDO;
	}

	public StockDO getStockDO() {
		return stockDO;
	}

	public void setStockDO(StockDO stockDO) {
		this.stockDO = stockDO;
	}

	public CostPriceDetailsDO getCostPriceDetailsDO() {
		return costPriceDetailsDO;
	}

	public void setCostPriceDetailsDO(CostPriceDetailsDO costPriceDetailsDO) {
		this.costPriceDetailsDO = costPriceDetailsDO;
	}

	public String getMandiNameCode() {
		return mandiNameCode;
	}

	public void setMandiNameCode(String mandiNameCode) {
		this.mandiNameCode = mandiNameCode;
	}

	public String getRevisitFlag() {
		return revisitFlag;
	}

	public void setRevisitFlag(String revisitFlag) {
		this.revisitFlag = revisitFlag;
	}

	public ContractDO getContractDO() {
		return contractDO;
	}

	public void setContractDO(ContractDO contractDO) {
		this.contractDO = contractDO;
	}

	@JsonIgnore
	@Override
	public Object getDomainObjectKey() {
		return getId();
	}

	@Override
	public StockDetails copyDomainObject2Entity(BaseEntity entity) {

		StockDetails stockDetails = null;

		if(entity == null){
			stockDetails = new StockDetails();
		}else {
			stockDetails = (StockDetails)entity;
		}

		stockDetails.setCreatedBy(this.getCreatedBy());
		stockDetails.setUpdatedBy(this.getUpdatedBy());
		stockDetails.setCreatedAt(this.getCreatedAt());
		stockDetails.setUpdatedAt(this.getUpdatedAt());

		stockDetails.setId(this.getId());
		stockDetails.setStockCode(this.getStockCode());
		stockDetails.setStockVarietyCode(this.getStockVarietyCode());
		stockDetails.setTransactionType(this.getTransactionType());
		stockDetails.setIndustryCode(this.getIndustryCode());
		stockDetails.setNoOfPackets(this.getNoOfPackets());
		stockDetails.setTotalWeightment(this.getTotalWeightment());
		stockDetails.setPlusOrMinusOnTotal(this.getPlusOrMinusOnTotal());
		stockDetails.setLampEntryInQtl(this.getLampEntryInQtl());
		stockDetails.setLampNameCode(this.getLampNameCode());
		stockDetails.setMandiNameCode(this.getMandiNameCode());
		stockDetails.setRevisitFlag(this.getRevisitFlag());


		stockDetails.setRemarks(this.getRemarks());
		stockDetails.setOnBeHalf(this.getOnBeHalf());
		stockDetails.setMoreDetails(this.getMoreDetails());
		stockDetails.setTransactionDate(this.getTransactionDate());

		stockDetails.setLampEntryAdjInQtl(this.getLampEntryAdjInQtl());
		stockDetails.setLampEntryAfterAdjInQtl(this.getLampEntryAfterAdjInQtl());
		stockDetails.setSettlementStatus(this.getSettlementStatus());

		if(this.getPartyDetailsDO() != null) {
			stockDetails.setPartyDetails(this.getPartyDetailsDO().copyDomainObject2Entity(stockDetails.getPartyDetails()));
		}

		if(this.getDeliveryDetailsDO() != null) {
			stockDetails.setDeliveryDetails(this.getDeliveryDetailsDO().copyDomainObject2Entity(stockDetails.getDeliveryDetails()));
		}

		if(this.getTransportDetailsDO() != null) {
			stockDetails.setTransportDetails(this.getTransportDetailsDO().copyDomainObject2Entity(stockDetails.getTransportDetails()));
		}

		if(this.getStockDO() != null) {
			stockDetails.setStock(this.getStockDO().copyDomainObject2Entity(stockDetails.getStock()));
		}

		if(this.getCostPriceDetailsDO() != null) {
			stockDetails.setCostPriceDetails(this.getCostPriceDetailsDO().copyDomainObject2Entity(stockDetails.getCostPriceDetails()));
		}

		if(this.getContractDO() != null){
			stockDetails.setContractDetails(this.getContractDO().copyDomainObject2Entity(stockDetails.getContractDetails()));
		}

		return stockDetails;
	}

	@Override
	public void copyEntity2DomainObject(BaseEntity entity) {
		StockDetails stockDetails = (StockDetails)entity;
		this.setCreatedBy(stockDetails.getCreatedBy());
		this.setUpdatedBy(stockDetails.getUpdatedBy());
		this.setCreatedAt(stockDetails.getCreatedAt());
		this.setUpdatedAt(stockDetails.getUpdatedAt());

		this.setCreatedBy(stockDetails.getCreatedBy());
		this.setUpdatedBy(stockDetails.getUpdatedBy());
		this.setCreatedAt(stockDetails.getCreatedAt());
		this.setUpdatedAt(stockDetails.getUpdatedAt());

		this.setId(stockDetails.getId());
		this.setStockCode(stockDetails.getStockCode());
		this.setStockVarietyCode(stockDetails.getStockVarietyCode());
		this.setTransactionType(stockDetails.getTransactionType());
		this.setIndustryCode(stockDetails.getIndustryCode());
		this.setNoOfPackets(stockDetails.getNoOfPackets());
		this.setTotalWeightment(stockDetails.getTotalWeightment());
		this.setPlusOrMinusOnTotal(stockDetails.getPlusOrMinusOnTotal());
		this.setLampEntryInQtl(stockDetails.getLampEntryInQtl());
		this.setLampNameCode(stockDetails.getLampNameCode());
		this.setMandiNameCode(stockDetails.getMandiNameCode());
		this.setRevisitFlag(stockDetails.getRevisitFlag());

		this.setRemarks(stockDetails.getRemarks());
		this.setOnBeHalf(stockDetails.getOnBeHalf());
		this.setMoreDetails(stockDetails.getMoreDetails());
		this.setTransactionDate(stockDetails.getTransactionDate());

		this.setLampEntryAdjInQtl(stockDetails.getLampEntryAdjInQtl());
		this.setLampEntryAfterAdjInQtl(stockDetails.getLampEntryAfterAdjInQtl());
		this.setSettlementStatus(stockDetails.getSettlementStatus());

        if(stockDetails.getPartyDetails() != null) {
			this.setPartyDetailsDO(new PartyDetailsDO());
			this.getPartyDetailsDO().copyEntity2DomainObject(stockDetails.getPartyDetails());
		}

		if(stockDetails.getDeliveryDetails() != null){
			this.setDeliveryDetailsDO(new DeliveryDetailsDO());
			this.getDeliveryDetailsDO().copyEntity2DomainObject(stockDetails.getDeliveryDetails());
		}

		if(stockDetails.getTransportDetails() != null){
			this.setTransportDetailsDO(new TransportDetailsDO());
			this.getTransportDetailsDO().copyEntity2DomainObject(stockDetails.getTransportDetails());
		}

		if(stockDetails.getStock() != null){
			this.setStockDO(new StockDO());
			this.getStockDO().copyEntity2DomainObject(stockDetails.getStock());
		}

		if(stockDetails.getCostPriceDetails() != null){
			this.setCostPriceDetailsDO(new CostPriceDetailsDO());
			this.getCostPriceDetailsDO().copyEntity2DomainObject(stockDetails.getCostPriceDetails());
		}

		if(stockDetails.getContractDetails() != null){
			this.setContractDO(new ContractDO());
			this.getContractDO().copyEntity2DomainObject(stockDetails.getContractDetails());
		}

	}

	/**
	 * Validate Stock Details DO
	 *
	 * @return
	 */
	public Optional<ResultDO> validate(){

		ResultDO resultDO = new ResultDO();
		List<String> errorList = new ArrayList<>();

		if(getIndustryCode() == null || StringUtils.isBlank(getIndustryCode())){
			errorList.add("Mill Name Is Mandatory");
		}

		if(getStockCode() == null || StringUtils.isBlank(getStockCode())){
			errorList.add("Stock Is Mandatory");
		}

		if(getStockVarietyCode() == null || StringUtils.isBlank(getStockVarietyCode())){
			errorList.add("Stock Variety Is Mandatory");
		}

		if(getTransactionDate() == null){
			errorList.add("Transaction Date Is Mandatory");
		}

		if(getTransactionType() == null || StringUtils.isBlank(getTransactionType())){
			errorList.add("Transaction Type Is Mandatory");
		}

		if(getNoOfPackets() == null || getTotalWeightment() == null){
			errorList.add("Quantity Is Mandatory");
		}

		if(errorList.size() > 0){
			/*resultDO = new ResultDO();*/
			resultDO.setError(true);
			resultDO.setErrorList(errorList);
		}

		return Optional.of(resultDO);
	}

	public Double getLampEntryAdjInQtl() {
		return lampEntryAdjInQtl;
	}

	public void setLampEntryAdjInQtl(Double lampEntryAdjInQtl) {
		this.lampEntryAdjInQtl = lampEntryAdjInQtl;
	}

	public Double getLampEntryAfterAdjInQtl() {
		return lampEntryAfterAdjInQtl;
	}

	public void setLampEntryAfterAdjInQtl(Double lampEntryAfterAdjInQtl) {
		this.lampEntryAfterAdjInQtl = lampEntryAfterAdjInQtl;
	}

	public String getSettlementStatus() {
		return settlementStatus;
	}

	public void setSettlementStatus(String settlementStatus) {
		this.settlementStatus = settlementStatus;
	}

	@Override
	public String toString() {
		return "StockDetailsDO{" +
				"id=" + id +
				", industryCode='" + industryCode + '\'' +
				", stockCode='" + stockCode + '\'' +
				", stockVarietyCode='" + stockVarietyCode + '\'' +
				", transactionType='" + transactionType + '\'' +
				", transactionDate=" + transactionDate +
				", noOfPackets=" + noOfPackets +
				", totalWeightment=" + totalWeightment +
				", plusOrMinusOnTotal=" + plusOrMinusOnTotal +
				", lampEntryInQtl=" + lampEntryInQtl +
				", lampNameCode='" + lampNameCode + '\'' +
				", mandiNameCode='" + mandiNameCode + '\'' +
				", revisitFlag='" + revisitFlag + '\'' +
				", remarks='" + remarks + '\'' +
				", onBeHalf='" + onBeHalf + '\'' +
				", moreDetails='" + moreDetails + '\'' +
				", partyDetailsDO=" + partyDetailsDO +
				", transportDetailsDO=" + transportDetailsDO +
				", deliveryDetailsDO=" + deliveryDetailsDO +
				", costPriceDetailsDO=" + costPriceDetailsDO +
				", contractDO=" + contractDO +
				", stockDO=" + stockDO +
				'}';
	}
}

