/*
 * Customers Java Swing Application Demo
 *
 * Copyright(c) 2013, devsniper.com
 */
package com.desktopapp.service;

import com.stocktracker.domainobjs.json.AppUserDO;
import org.springframework.stereotype.Service;


/**
 * AppUser Service
 *
 * @author Ratnala Diwakar Choudhury
 */
@Service
public class AppUserService extends AbstractService<AppUserDO> {

    public AppUserService() {
        super(AppUserDO.class);
    }
    
   public static final String BASE_RESOURCE = "appUser";
	
    @Override
	protected String getBaseResource() {
		return BASE_RESOURCE;
	}

}
