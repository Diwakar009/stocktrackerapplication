package com.desktopapp.fx.config.manager;

import com.desktopapp.fx.app.AppAccelerator;
import com.desktopapp.fx.app.AppFontIcons;
import com.desktopapp.fx.commands.HelpCommand;
import com.desktopapp.fx.commands.factory.CommandFactory;
import com.desktopapp.fx.commands.factory.ICommandFactory;
import com.desktopapp.fx.config.ApplicationViewConfig;
import com.desktopapp.fx.config.RibbonMenuItem;
import com.desktopapp.fx.control.ActionRibbonCommand;
import com.desktopapp.fx.control.RibbonGroup;
import com.desktopapp.fx.util.I18n;
import com.desktopapp.fx.view.AppView;
import javafx.scene.Node;

import java.util.*;

/**
 * Created by diwakar009 on 11/3/2019.
 */
public class ApplicationViewConfigManager extends CommonConfigManager<ApplicationViewConfig>{

    private AppView appView;
    private Map<String,RibbonMenuItem> ribbonMenuItemsMap = new LinkedHashMap<String,RibbonMenuItem>();

     /* Map<String, String> map = Stream.of(new String[][] {
            { "Hello", "World" },
            { "John", "Doe" },
    }).collect(Collectors.collectingAndThen(
            Collectors.toMap(data -> data[0], data -> data[1]),
            Collections::<String, String> unmodifiableMap));*/


    public static final String HOME_RIBBON = "HOME";
    public static final String VIEW_RIBBON = "VIEW";
    public static final String EXTRATAB_RIBBON = "EXTRA";
    public static final String HELPTAB_RIBBON = "HELP";

    public ApplicationViewConfigManager(AppView appView) {
        super(ApplicationViewConfig.class);
        this.appView=appView;
    }

    @Override
    public String getConfigJSONResource() {
        return "productconfig/applicationView.json";
    }

    @Override
    public void overrideProperties() {
        appView.setMenuRibbonMap(getRibbonMenuItems());
    }

    private String getRibbonLabel(String key){
        switch (key){
            case HOME_RIBBON:
                return  I18n.COMMON.getString("app.ribbon.home");

            case VIEW_RIBBON:
                return  I18n.COMMON.getString("app.ribbon.view");

            case EXTRATAB_RIBBON:
                return  I18n.COMMON.getString("app.ribbon.extras");

            case HELPTAB_RIBBON:
                return  I18n.COMMON.getString("app.ribbon.help");

            default:
                return "";

        }
    }

    public  Map<String,RibbonMenuItem> getRibbonMenuItems(){

        constructHomeMenuRibbons();
        constructViewMenuRibbons();
        constructExtraTabMenuRibbons();
        //constructHelpTabMenuRibbons(); // Not yet Implemented

        return ribbonMenuItemsMap;

    }

    private void constructHomeMenuRibbons(){

        List<Node> homeRibbons = new ArrayList<Node>();

        homeRibbons.add(new ActionRibbonCommand(CommandFactory.getInstance().getCommand(appView, ICommandFactory.CMD_STATICCODEDECODE),
                I18n.COMMON.getString("app.menu.staticcodedecode"), AppFontIcons.STATICCODEDECODE, AppAccelerator.getKeyCode("ALT-C")));

        homeRibbons.add(RibbonGroup.addSeparator());

        homeRibbons.add(new ActionRibbonCommand(CommandFactory.getInstance().getCommand(appView, ICommandFactory.CMD_CONTRACTMAINTENANCE),
                I18n.COMMON.getString("app.menu.contractmaintenance"), AppFontIcons.CONTRACTMANAGEMENT, AppAccelerator.getKeyCode("ALT-T")));

        homeRibbons.add(new ActionRibbonCommand(CommandFactory.getInstance().getCommand(appView, ICommandFactory.CMD_STOCKMAINTENANCE),
                I18n.COMMON.getString("app.menu.stockmaintenance"), AppFontIcons.STOCKMANAGEMENT, AppAccelerator.getKeyCode("ALT-M")));

        homeRibbons.add(RibbonGroup.addSeparator());

        homeRibbons.add(new ActionRibbonCommand(CommandFactory.getInstance().getCommand(appView, ICommandFactory.CMD_EXIT),
                I18n.COMMON.getString("app.menu.exit"), AppFontIcons.EXIT, AppAccelerator.getKeyCode("ALT-Q")));

        ribbonMenuItemsMap.put(HOME_RIBBON,new RibbonMenuItem(getRibbonLabel(HOME_RIBBON),homeRibbons));

    }

    private void constructViewMenuRibbons(){
        List<Node> viewRibbons = new ArrayList<Node>();

        viewRibbons.add(new ActionRibbonCommand(CommandFactory.getInstance().getCommand(appView, ICommandFactory.CMD_STATUSBAR),
                I18n.COMMON.getString("app.menu.statusbar"), AppFontIcons.STATUS_BAR, AppAccelerator.getKeyCode("ALT-S")));

        viewRibbons.add(new ActionRibbonCommand(CommandFactory.getInstance().getCommand(appView, ICommandFactory.CMD_THEMES),
                I18n.COMMON.getString("app.menu.themes"), AppFontIcons.THEMES, AppAccelerator.getKeyCode("ALT-T")));

        ribbonMenuItemsMap.put(VIEW_RIBBON,new RibbonMenuItem(getRibbonLabel(VIEW_RIBBON),viewRibbons));

    }

    private void constructExtraTabMenuRibbons(){
        List<Node> extraTabRibbons = new ArrayList<Node>();

        extraTabRibbons.add(new ActionRibbonCommand(CommandFactory.getInstance().getCommand(appView, ICommandFactory.CMD_PREFERENCE),
                I18n.COMMON.getString("app.menu.preferences"), AppFontIcons.PREFERENCES, AppAccelerator.getKeyCode("ALT-P")));

        ribbonMenuItemsMap.put(EXTRATAB_RIBBON,new RibbonMenuItem(getRibbonLabel(EXTRATAB_RIBBON),extraTabRibbons));

    }

    private void constructHelpTabMenuRibbons(){

        List<Node> helpTabRibbons = new ArrayList<Node>();

        helpTabRibbons.add(new ActionRibbonCommand(new HelpCommand(),I18n.COMMON.getString("app.menu.help"), AppFontIcons.HELP, AppAccelerator.getKeyCode("F1")));

        helpTabRibbons.add(new ActionRibbonCommand(CommandFactory.getInstance().getCommand(appView, ICommandFactory.CMD_KEYBOARDSHORTCUT),
                I18n.COMMON.getString("app.menu.keyboardShortcuts"), AppFontIcons.KEYBOARD_SHORTCUTS, AppAccelerator.getKeyCode("ALT-K")));

        helpTabRibbons.add(new ActionRibbonCommand(CommandFactory.getInstance().getCommand(appView, ICommandFactory.CMD_TIPOFTHEDAY),
                I18n.COMMON.getString("app.menu.tipOfTheDay"), AppFontIcons.TIP_OF_THE_DAY, AppAccelerator.getKeyCode("ALT-I")));

        helpTabRibbons.add(RibbonGroup.addSeparator());

        helpTabRibbons.add(new ActionRibbonCommand(CommandFactory.getInstance().getCommand(appView, ICommandFactory.CMD_ABOUT),
                I18n.COMMON.getString("app.menu.about"), AppFontIcons.ABOUT, AppAccelerator.getKeyCode("ALT-B")));

        ribbonMenuItemsMap.put(HELPTAB_RIBBON,new RibbonMenuItem(getRibbonLabel(HELPTAB_RIBBON),helpTabRibbons));

    }
}
