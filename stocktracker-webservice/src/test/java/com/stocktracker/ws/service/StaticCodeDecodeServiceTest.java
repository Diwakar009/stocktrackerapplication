package com.stocktracker.ws.service;

import java.util.List;
import java.util.Optional;

import com.desktopapp.service.StaticCodeDecodeService;
import com.stocktracker.domainobjs.json.CodeDataItemDO;
import com.stocktracker.ws.BaseUnitTest;
import com.stocktracker.ws.config.AppConfig;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.transaction.annotation.Transactional;

import com.stocktracker.model.StaticCodeDecode;

/**
 * Unit test methods for the GreetingService and GreetingServiceBean.
 * 
 * @author Diwakar Choudhury
 */



@Transactional
public class StaticCodeDecodeServiceTest extends BaseUnitTest {

	@Autowired
	private StaticCodeDecodeService staticCodeDecodeService;

    @Before
    public void setUp() {}

    @After
    public void tearDown() {}

    @Test
    public void testGetStaticCodeDecodeListWithLang(){
    	
    	Optional<List<CodeDataItemDO>> codeDataItems = staticCodeDecodeService.getCodeDataItemList("EN_US");

    	Assert.assertTrue(
                "Number of code decode Retrevived :",
                codeDataItems.get().size() > 0);

        codeDataItems.get().forEach(codeDataItemDO -> {displayMessage(codeDataItemDO.toString());});
    }

    @Test
    public void testGetStaticCodeDecodeListWithCodeNameNLang(){

        Optional<List<CodeDataItemDO>> codeDataItems = staticCodeDecodeService.getStaticCodeDataItemList("CD_ACCESSLEVEL","EN_US");

        Assert.assertTrue(
                "Number of code decode Retrevived :",
                codeDataItems.get().size() > 0);

        codeDataItems.get().forEach(codeDataItemDO -> {displayMessage(codeDataItemDO.toString());});

    }

    @Test
    public void testGetStaticCodeDecodeListWithCodeNameCodeValueNDesc(){

        Optional<List<CodeDataItemDO>> codeDataItems = staticCodeDecodeService.getStaticCodeDataItemList("CD_ACCESSLEVEL","EN_US");

        Assert.assertTrue(
                "Number of code decode Retrevived :",
                codeDataItems.get().size() > 0);

        codeDataItems.get().forEach(codeDataItemDO -> {displayMessage(codeDataItemDO.toString());});

    }

    @Test
    public void testGetStaticCodeDecodeListCodeNameNLang4rDropDown(){

        Optional<List<CodeDataItemDO>> codeDataItems = staticCodeDecodeService.getStaticCodeDataItemList("CD_ACCESSLEVEL","EN_US");

        Assert.assertTrue(
                "Number of code decode Retrevived :",
                codeDataItems.get().size() > 0);

        codeDataItems.get().forEach(codeDataItemDO -> {displayMessage(codeDataItemDO.toString());});

    }
}

