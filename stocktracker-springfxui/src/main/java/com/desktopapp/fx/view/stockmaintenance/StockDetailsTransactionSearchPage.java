/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.desktopapp.fx.view.stockmaintenance;


import com.desktopapp.fx.app.AppFontIcons;
import com.desktopapp.fx.common.IAccelerator;
import com.desktopapp.fx.common.IGenericComponent;
import com.desktopapp.fx.common.PageDetails;
import com.desktopapp.fx.control.*;
import com.desktopapp.fx.control.TableColumnBuilder;
import com.desktopapp.fx.control.fonticon.FontIcon;
import com.desktopapp.fx.controller.StockMaintenanceController;
import com.desktopapp.fx.mvc.AbstractDataPageView;
import com.desktopapp.fx.util.ViewHelpers;
import com.stocktracker.component.constants.CodeDecodeConstants;
import com.stocktracker.domainobjs.json.sms.*;
import com.stocktracker.exceptions.BusinessException;
import com.stocktracker.exceptions.TechnicalException;
import com.stocktracker.util.DateTimeUtil;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.layout.*;
import com.stocktracker.domainobjs.json.DOPage;
import com.stocktracker.domainobjs.json.TableRowMapDO;
import javafx.scene.text.Text;
import javafx.util.Callback;
import org.controlsfx.control.textfield.AutoCompletionBinding;
import org.controlsfx.control.textfield.TextFields;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.util.*;

/**
 * Stock Transaction Search page
 * @author diwakar009
 */
public class StockDetailsTransactionSearchPage extends AbstractDataPageView<StockDetailsDO> {

    private final Logger logger = LoggerFactory.getLogger(StockDetailsTransactionSearchPage.class);

    private CodeDecodeField cdIndustry;
    private TextFieldExt tfPartyName;
    private CodeDecodeField cdStock;
    private CodeDecodeField cdStockVariety;
    private CodeDecodeField cdTransactionType;
    private DateTimeField dfStartDate;
    private DateTimeField dfEndDate;
    private TextFieldExt tfRstNumber;
    private TextFieldExt tfVehicleNumber;
    private CodeDecodeField cdOnBehalf;
    private CodeDecodeField cdLampName;
    private CodeDecodeField cdMandiName;
    private CodeDecodeField cdRevisitFlag;

    private ActionButtonCommand btnSearch;
    private ActionButtonCommand btnClear;
    private ActionButtonCommand btnDownloadMandiReport;
    private ProgressIndicator progressIndicator;

    private TableDataView tvStockSummary;
    private TableDataView tvStockBalance;

    private List<IGenericComponent> inputComponents = new ArrayList<>();

    private ScrollPane scrollPane;

    @Override
    protected Node renderCenter(){
        scrollPane = new ScrollPane();
        scrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
        scrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
        scrollPane.fitToWidthProperty().set(true);

        BorderPane dataPageBroderView = new BorderPane();
        dataPageBroderView.getStyleClass().addAll("search-data-page-view");
        dataPageBroderView.setTop(getScreenHeaderBar());
        scrollPane.setContent(buildCenter());
        dataPageBroderView.setCenter(scrollPane);
        dataPageBroderView.setBottom(buildButtom());
        return dataPageBroderView;
    }

    private Node buildCenter(){

        BorderPane searchPageCenterView = new BorderPane();
        searchPageCenterView.getStyleClass().addAll("search-data-page-center");
        searchPageCenterView.setTop(buildFilterPane());

        BorderPane tablePaneBorder = new BorderPane();
        /*tablePaneBorder.getStyleClass().addAll("search-data-table");
        tablePaneBorder.setTop(getHeaderBar());*/
        VBox centerBox = new VBox();

        VBox stockDtlTable = new VBox();
        stockDtlTable.setPrefHeight(300);
        //stockDtlTable.getChildren().add(getTablePane());
        stockDtlTable.getChildren().add(getTableDataView());
        centerBox.getChildren().add(stockDtlTable);

        VBox summaryTablePaneBorder = new VBox();
        summaryTablePaneBorder.setPrefHeight(300);
        summaryTablePaneBorder.getChildren().add(getSummaryTablePane());
        centerBox.getChildren().add(summaryTablePaneBorder);

        VBox stockBalanceTablePaneBorder = new VBox();
        stockBalanceTablePaneBorder.setPrefHeight(300);
        stockBalanceTablePaneBorder.getChildren().add(getStockBalanceTablePane());
        centerBox.getChildren().add(stockBalanceTablePaneBorder);

        tablePaneBorder.setCenter(centerBox);
        searchPageCenterView.setCenter(tablePaneBorder);
        return searchPageCenterView;
    }

    protected VBox getStockBalanceTablePane() {
        tvStockBalance = new TableDataView(new StockBalanceTableBehaviour(this));
        VBox tablePane = new VBox();
        tablePane.getChildren().addAll(tvStockBalance);
        VBox.setVgrow(tvStockBalance, Priority.ALWAYS);
        tvStockBalance.refreshData(); // refresh data after loading
        return tablePane;
    }

    protected VBox getSummaryTablePane() {
        tvStockSummary = new TableDataView(new StockDetialsSummaryTableBehaviour(this));
        VBox tablePane = new VBox();
        tablePane.getChildren().addAll(tvStockSummary);
        VBox.setVgrow(tvStockSummary, Priority.ALWAYS);
        tvStockSummary.refreshData(); // refresh data after loading
        return tablePane;
    }

    private Node buildFilterPane(){

        this.cdIndustry = new CodeDecodeField(CodeDecodeConstants.CD_INDUSTRY_CODE,CodeDecodeConstants.DEFAULT_LANGUAGE);
        this.cdIndustry.setPrefWidth(200);
        this.tfPartyName = new TextFieldExt(100);

        TextFields.bindAutoCompletion(this.tfPartyName, new Callback<AutoCompletionBinding.ISuggestionRequest, Collection<String>>() {
            @Override
            public Collection<String> call(AutoCompletionBinding.ISuggestionRequest iSuggestionRequest) {
                return getPartyNameSuggestions(tfPartyName.getDataValue());
            }
        });

        this.cdStock = new CodeDecodeField(CodeDecodeConstants.CD_STOCK_CODE,CodeDecodeConstants.DEFAULT_LANGUAGE);
        this.cdStockVariety = new CodeDecodeField(CodeDecodeConstants.CD_STOCK_VARIETY,CodeDecodeConstants.DEFAULT_LANGUAGE);
        this.cdTransactionType = new CodeDecodeField(CodeDecodeConstants.CD_TRANSACTION_TYPE,CodeDecodeConstants.DEFAULT_LANGUAGE);
        this.dfStartDate = new DateTimeField();
        this.dfEndDate = new DateTimeField();
        this.tfRstNumber = new TextFieldExt();
        this.tfVehicleNumber =new TextFieldExt();

        this.tfRstNumber.setPrefWidth(50);
        this.tfVehicleNumber.setPrefWidth(50);

        this.cdOnBehalf = new CodeDecodeField(CodeDecodeConstants.CD_INDUSTRY_CODE,CodeDecodeConstants.DEFAULT_LANGUAGE);
        this.cdOnBehalf.setPrefWidth(200);

        this.cdLampName = new CodeDecodeField(CodeDecodeConstants.CD_LAMP_CODE,CodeDecodeConstants.DEFAULT_LANGUAGE);

        this.cdMandiName = new CodeDecodeField(CodeDecodeConstants.CD_MANDI_NAME,CodeDecodeConstants.DEFAULT_LANGUAGE);
        this.cdLampName.registerLinkedCodeDecodeField(cdMandiName);

        this.cdRevisitFlag = new CodeDecodeField(CodeDecodeConstants.CD_YESNO,CodeDecodeConstants.DEFAULT_LANGUAGE);
        this.cdRevisitFlag.setPrefWidth(200);


        this.inputComponents.add(cdIndustry);
        this.inputComponents.add(tfPartyName);
        this.inputComponents.add(cdStock);
        this.inputComponents.add(cdStockVariety);
        this.inputComponents.add(cdTransactionType);
        this.inputComponents.add(dfStartDate);
        this.inputComponents.add(dfEndDate);
        this.inputComponents.add(tfRstNumber);
        this.inputComponents.add(tfVehicleNumber);
        this.inputComponents.add(cdOnBehalf);
        this.inputComponents.add(cdLampName);
        this.inputComponents.add(cdMandiName);
        this.inputComponents.add(cdRevisitFlag);

        this.cdStock.registerLinkedCodeDecodeField(cdStockVariety);
        this.cdStock.registerLinkedCodeDecodeField(cdTransactionType);


        this.btnSearch= new ActionButtonCommand("Search",event -> onSearch());
        this.btnClear= new ActionButtonCommand("Clear",event -> onClear());



        this.btnSearch.setTooltip(new Tooltip("Search"));
        this.btnSearch.getStyleClass().add("left-pill");

        this.btnClear.setTooltip(new Tooltip("Clear"));
        this.btnClear.getStyleClass().add("left-pill");


        this.btnDownloadMandiReport = new ActionButtonCommand("Mandi Report", progressIndicator, event -> onDownloadMandiReport());
        this.btnDownloadMandiReport.setTooltip(new Tooltip("Download Mandi Report"));
        this.btnDownloadMandiReport.getStyleClass().add("left-pill");

        BorderPane borderPane = new BorderPane();

        GridPane formPane = new GridPane();
        formPane.setPadding(new Insets(20, 30, 10, 30));
        formPane.setHgap(10);
        formPane.setVgap(10);

        formPane.setAlignment(Pos.CENTER);

        formPane.add(new Label("Mill Name"), 0, 0);
        formPane.add(cdIndustry.asControl(), 1, 0);

        formPane.add(new Label("Party Name"), 2, 0);
        formPane.add(tfPartyName, 3, 0);

        formPane.add(new Label("Stock Name"), 0, 1);
        formPane.add(cdStock.asControl(), 1, 1);

        formPane.add(new Label("Stock Variety"), 2, 1);
        formPane.add(cdStockVariety.asControl(), 3, 1);

        formPane.add(new Label("Transaction Type"), 0, 2);
        formPane.add(cdTransactionType.asControl(), 1, 2);

        formPane.add(new Label("Transaction Date"), 2, 2);
        HBox startDateEndDateHBox = new HBox();
        startDateEndDateHBox.getChildren().add(dfStartDate);
        startDateEndDateHBox.getChildren().add(new Text(" __ "));
        startDateEndDateHBox.getChildren().add(dfEndDate);
        formPane.add(startDateEndDateHBox,3 , 2);

        formPane.add(new Label("RST Number"), 0, 3);
        formPane.add(tfRstNumber, 1, 3);

        formPane.add(new Label("Vehicle Number"), 2, 3);
        formPane.add(tfVehicleNumber, 3, 3);

        formPane.add(new Label("OnBehalf"), 0, 4);
        formPane.add(cdOnBehalf.asControl(), 1, 4);

        formPane.add(new Label("Lamp Name"), 0, 5);
        formPane.add(cdLampName.asControl(), 1, 5);

        formPane.add(new Label("Mandi Name"), 2, 5);
        formPane.add(cdMandiName.asControl(), 3, 5);

        formPane.add(new Label("Revisit Flag"), 0, 6);
        formPane.add(cdRevisitFlag.asControl(), 1, 6);

        HBox buttonBox = new HBox();
        buttonBox.setStyle("-fx-pref-height: 50px;");
        buttonBox.getChildren().add(btnSearch);
        buttonBox.getChildren().add(btnClear);

       /* HBox mandiReportHBox = new HBox();
        mandiReportHBox.setAlignment(Pos.CENTER);
        mandiReportHBox.getChildren().add(btnDownloadMandiReport);
        mandiReportHBox.getChildren().add(piDownloadMandiReport);
        buttonBox.getChildren().add(mandiReportHBox);*/
        buttonBox.getChildren().add(btnDownloadMandiReport);

        buttonBox.setAlignment(Pos.CENTER);
        buttonBox.setSpacing(40);

        borderPane.setTop(formPane);
        borderPane.setCenter(buttonBox);
        return borderPane;
    }

    private void onDownloadMandiReport() {
        try {
            ((StockMaintenanceController) getController()).onDownloadMandiReport(getCdIndustry().getDataValue(), getCdLampName().getDataValue(), getCdMandiName().getDataValue());
        }catch (BusinessException | TechnicalException e) {
            logger.error("Exception in generating mandi recon report", e);
        }
    }

    @Override
    public void onExportToExcel() {

        String industryCode = this.cdIndustry.getDataValue();
        String partyName = this.tfPartyName.getDataValue();
        String stockCode = this.cdStock.getDataValue();
        String stockVarietyCode = this.cdStockVariety.getDataValue();
        String transactionType = this.cdTransactionType.getDataValue();
        Date transStartDate = this.dfStartDate.getDataValueAsDate();
        Date transEndDate = this.dfEndDate.getDataValueAsDate();
        String rstNumber = this.tfRstNumber.getDataValue();
        String vehicleNumber = this.tfVehicleNumber.getDataValue();
        String onBehalf = this.cdOnBehalf.getDataValue();
        String lampName = this.cdLampName.getDataValue();
        String mandiName = this.cdMandiName.getDataValue();
        String revisitFlag = this.cdRevisitFlag.getDataValue();

        try {
            ((StockMaintenanceController) getController()).onDownloadStockDetailTransaction(industryCode,stockCode,stockVarietyCode,transactionType,transStartDate,transEndDate,rstNumber,vehicleNumber,partyName,onBehalf,lampName,mandiName,revisitFlag);
        }catch (BusinessException | TechnicalException e) {
            logger.error("Exception in generating stock details report", e);
        }
    }

    /**
     * Get Party Names.
     *
     * @param partyName
     * @return
     */
    public List<String> getPartyNameSuggestions(String partyName){
        Optional<List<String>> suggestedPartyNames = ((StockMaintenanceController)this.getController()).getSuggestedPartyNames(partyName);
        return suggestedPartyNames.orElseGet(Arrays::asList);
    }

    @Override
    public DOPage<TableRowMapDO<String, String>> getData(String filter, PageDetails details) {
        String industryCode = this.cdIndustry.getDataValue();
        String partyName = this.tfPartyName.getDataValue();
        String stockCode = this.cdStock.getDataValue();
        String stockVarietyCode = this.cdStockVariety.getDataValue();
        String transactionType = this.cdTransactionType.getDataValue();
        Date transStartDate = this.dfStartDate.getDataValueAsDate();
        Date transEndDate = this.dfEndDate.getDataValueAsDate();
        String rstNumber = this.tfRstNumber.getDataValue();
        String vehicleNumber = this.tfVehicleNumber.getDataValue();
        String onBehalf = this.cdOnBehalf.getDataValue();
        String lampName = this.cdLampName.getDataValue();
        String mandiName = this.cdMandiName.getDataValue();
        String revisitFlag = this.cdRevisitFlag.getDataValue();

        return ((StockMaintenanceController)getController()).getData(industryCode,stockCode,stockVarietyCode,transactionType,transStartDate,transEndDate,rstNumber,vehicleNumber,partyName,onBehalf,lampName,mandiName,revisitFlag,details);

    }



    /**
     * Event on click of onSearch
     *
     */
    public void onSearch(){
       getTableDataView().resetPager();
       refreshData();
    }

    @Override
    public void refreshData() {
        super.refreshData();
        this.tvStockSummary.refreshData();
        this.tvStockBalance.refreshData();
    }

    /**
     * Event on click onSummary
     *
     * Summary the filter criteria
     *
     */
    public void onSummary(){
        this.inputComponents.forEach(
                component -> {component.clearDataValue(); });
    }

    /**
     * Event on click onClear
     *
     * Clear the filter criteria
     *
     */
    public void onClear(){
        this.inputComponents.forEach(
                component -> {component.clearDataValue();
                });
    }

    /**
     * Screen Header
     *
     * @return
     */
    protected HBox getScreenHeaderBar() {
        logger.debug("get data page header");
        HBox headerBar = new HBox();
        headerBar.getStyleClass().add("search-data-page-header");

        Label title = ViewHelpers.createFontIconLabel(getTitle(), getFontIcon(), "");
        title.getStyleClass().add("search-data-page-title");
        this.progressIndicator = new ProgressIndicator();
        Region spacer = new Region();
        HBox.setHgrow(spacer, Priority.ALWAYS);
        headerBar.getChildren().addAll(title, progressIndicator, spacer);
        return headerBar;
    }

    protected HBox getSummaryHeaderBar() {
        logger.debug("get data page header");
        HBox headerBar = new HBox();
        headerBar.getStyleClass().add("search-data-table-header");
        Label title = ViewHelpers.createFontIconLabel("Stock Summary", getFontIcon(), "");
        title.getStyleClass().add("search-data-table-title");
        ProgressIndicator progressIndicator = new ProgressIndicator();
        progressIndicator.setMaxSize(50, 25);
        Region spacer = new Region();
        HBox.setHgrow(spacer, Priority.ALWAYS);
        headerBar.getChildren().addAll(title, progressIndicator, spacer);
        return headerBar;
    }

    /*protected HBox getHeaderBar() {
        logger.debug("get data page header");
        HBox headerBar = new HBox();
        headerBar.getStyleClass().add("search-data-table-header");

        Label title = ViewHelpers.createFontIconLabel("Stock Transaction's", getFontIcon(), "");
        title.getStyleClass().add("search-data-table-title");

        progressIndicator = new ProgressIndicator();
        progressIndicator.setMaxSize(50, 25);

        Region spacer = new Region();
        HBox.setHgrow(spacer, Priority.ALWAYS);

        // actions
        btnAddNew = new ActionCommand("", FontIconFactory.createIcon(AppFontIcons.ADD_NEW));
        btnAddNew.setTooltip(new Tooltip(I18n.COMMON.getString("action.addNew")));
        btnAddNew.getStyleClass().add("left-pill");
        //btnAddNew.setOnAction(event -> onAddNew());
        btnAddNew.setOnActionEvent(event -> onAddNew());

        btnEdit = new ActionCommand("", FontIconFactory.createIcon(AppFontIcons.EDIT));
        btnEdit.setTooltip(new Tooltip(I18n.COMMON.getString("action.edit")));
        btnEdit.getStyleClass().add("right-pill");
        btnEdit.setOnActionEvent(event -> onEdit());

        HBox.setMargin(btnEdit, new Insets(0, 10, 0, 0));

        btnDelete = new ActionCommand("", FontIconFactory.createIcon(AppFontIcons.DELETE));
        btnDelete.setTooltip(new Tooltip(I18n.COMMON.getString("action.delete")));
        btnDelete.setOnActionEvent(event -> onDelete());

        HBox.setMargin(btnDelete, new Insets(0, 10, 0, 0));

        headerBar.getChildren().addAll(title, progressIndicator, spacer, btnAddNew, btnEdit, btnDelete);

        if (hasPrintActions()) {
            btnPrintPreview = new ActionCommand("", FontIconFactory.createIcon(AppFontIcons.PRINT_PREVIEW));
            btnPrintPreview.setTooltip(new Tooltip(I18n.COMMON.getString("action.printPreview")));
            btnPrintPreview.getStyleClass().add("left-pill");
            btnPrintPreview.setOnActionEvent(event -> onPrintPreview());

            btnPrint = new ActionCommand("", FontIconFactory.createIcon(AppFontIcons.PRINT));
            btnPrint.setTooltip(new Tooltip(I18n.COMMON.getString("action.print")));
            btnPrint.getStyleClass().add("center-pill");
            btnPrint.setOnActionEvent(event -> onPrint());

            btnPdf = new ActionCommand("", FontIconFactory.createIcon(AppFontIcons.PDF));
            btnPdf.setTooltip(new Tooltip(I18n.COMMON.getString("action.pdf")));
            btnPdf.getStyleClass().add("right-pill");
            btnPdf.setOnActionEvent(event -> onPdf());
            HBox.setMargin(btnPdf, new Insets(0, 10, 0, 0));

            headerBar.getChildren().addAll(btnPrintPreview, btnPrint, btnPdf);
        }

        return headerBar;
    }*/

    @Override
    public IAccelerator getAccelerator() {
        return new IAccelerator() {
            @Override
            public Map<String, Map<KeyCodeCombination, Runnable>> getAccelerators() {
                Map<String,Map<KeyCodeCombination, Runnable>> combinations = new HashMap<String,Map<KeyCodeCombination, Runnable>>();
                Map<KeyCodeCombination,Runnable> childAccelerators = getDataPageKeyCodeCombination();
                if (hasPrintActions()) {
                    childAccelerators.putAll(getPrintKeyCodeCombination());
                }
                combinations.put(IAccelerator.CHILD_ACCELERATORS,childAccelerators);
                combinations.put(IAccelerator.PARENT_ACCELERATORS,new HashMap<KeyCodeCombination,Runnable>());

                return combinations;
            }
        };
    }

    @Override
    public List<TableColumn<StockDetailsDO, ?>> getTableViewColumns() {
        List<TableColumn<StockDetailsDO, ?>> columns = new ArrayList<>();

        TableColumn<StockDetailsDO, Date> transactionDateCol = TableColumnBuilder.<StockDetailsDO, Date> create()
                .fieldName("transactionDate")
                .title("Transaction Date")
                .dataType(TableColumnBuilder.DATATYPE_DATE)
                .build();

        columns.add(transactionDateCol);

        TableColumn<StockDetailsDO, String> industryCodeCol = TableColumnBuilder.<StockDetailsDO, String> create()
                .fieldName("industryCode")
                .title("Industry Code")
                .decodeCode(CodeDecodeConstants.CD_INDUSTRY_CODE)
                .build();

        columns.add(industryCodeCol);

        TableColumn<StockDetailsDO, String> stockCodeCol = TableColumnBuilder.<StockDetailsDO, String> create()
                .fieldName("stockCode")
                .title("Stock")
                .decodeCode(CodeDecodeConstants.CD_STOCK_CODE)
                .build();

        columns.add(stockCodeCol);

        TableColumn<StockDetailsDO, String> stockVarietyCodeCol = TableColumnBuilder.<StockDetailsDO, String> create()
                .fieldName("stockVarietyCode")
                .title("Stock Varierty")
                .decodeCode(CodeDecodeConstants.CD_STOCK_VARIETY)
                .build();

        columns.add(stockVarietyCodeCol);

        TableColumn<StockDetailsDO, String> transactionTypeCol = TableColumnBuilder.<StockDetailsDO, String> create()
                .fieldName("transactionType")
                .title("Transaction Type")
                .decodeCode(CodeDecodeConstants.CD_TRANSACTION_TYPE)
                .build();

        columns.add(transactionTypeCol);

        TableColumn<StockDetailsDO, Long> noOfPacketsCol = TableColumnBuilder.<StockDetailsDO, Long> create()
                .title("No of Packets/Bags")
                .build();

        noOfPacketsCol.setCellValueFactory(cellvaluefac -> {
            if(cellvaluefac.getValue().getNoOfPackets() != null) {
                return new ReadOnlyObjectWrapper(cellvaluefac.getValue().getNoOfPackets() + " " + TableColumnBuilder.getNoOfPacketsUnit(cellvaluefac.getValue().getStockCode(),
                        cellvaluefac.getValue().getStockVarietyCode()));
            }
            return new ReadOnlyObjectWrapper("");
        });


        columns.add(noOfPacketsCol);

        TableColumn<StockDetailsDO, Double> weightmentCol = TableColumnBuilder.<StockDetailsDO, Double> create()
                .title("Weightment/Bail")
                .build();

        weightmentCol.setCellValueFactory(cellvaluefac -> {
            if(cellvaluefac.getValue().getTotalWeightment() != null) {
                return new ReadOnlyObjectWrapper(cellvaluefac.getValue().getTotalWeightment() + " " + TableColumnBuilder.getWeightmentUnit(cellvaluefac.getValue().getStockCode(),
                        cellvaluefac.getValue().getStockVarietyCode()));
            }
            return new ReadOnlyObjectWrapper("");
        });

        columns.add(weightmentCol);

        TableColumn<StockDetailsDO, Double> appliedRateCol = TableColumnBuilder.<StockDetailsDO, Double> create()
                .title("Applied Rate")
                .build();

        appliedRateCol.setCellValueFactory(cellvaluefac -> {
            if(cellvaluefac.getValue().getCostPriceDetailsDO() != null && cellvaluefac.getValue().getCostPriceDetailsDO().getAppliedRate() != null) {
                return new ReadOnlyObjectWrapper(TableColumnBuilder.AMOUNT_CCY_FORMATTER.format(cellvaluefac.getValue().getCostPriceDetailsDO().getAppliedRate()));
            }
            return new ReadOnlyObjectWrapper("");
        });

        columns.add(appliedRateCol);

        TableColumn<StockDetailsDO, Long> actualAmounttCol = TableColumnBuilder.<StockDetailsDO, Long> create()
                .title("Amount")
                .build();

        actualAmounttCol.setCellValueFactory(cellvaluefac -> {
            if(cellvaluefac.getValue().getCostPriceDetailsDO() != null) {
                return new ReadOnlyObjectWrapper(TableColumnBuilder.AMOUNT_CCY_FORMATTER.format(cellvaluefac.getValue().getCostPriceDetailsDO().getActualAmount()));
            }
            return new ReadOnlyObjectWrapper("");
        });

        columns.add(actualAmounttCol);


        TableColumn<StockDetailsDO, String> lampNameCol = TableColumnBuilder.<StockDetailsDO, String> create()
                .fieldName("lampNameCode")
                .title("Lamp Name")
                .decodeCode(CodeDecodeConstants.CD_LAMP_CODE)
                .build();


        columns.add(lampNameCol);

        TableColumn<StockDetailsDO, String> mandiNameCol = TableColumnBuilder.<StockDetailsDO, String> create()
                .fieldName("mandiNameCode")
                .title("Mandi Name")
                .decodeCode(CodeDecodeConstants.CD_MANDI_NAME)
                .build();

        columns.add(mandiNameCol);


        TableColumn<StockDetailsDO, String> partyNameCol = TableColumnBuilder.<StockDetailsDO, String> create()
                .title("Party Name")
                .build();

        partyNameCol.setCellValueFactory(cellvaluefac -> {
            if(cellvaluefac.getValue().getPartyDetailsDO() != null) {
                return new ReadOnlyObjectWrapper(cellvaluefac.getValue().getPartyDetailsDO().getPartyName1());
            }
            return new ReadOnlyObjectWrapper("");
        });

        columns.add(partyNameCol);

        TableColumn<StockDetailsDO, Double> lampEntryCol = TableColumnBuilder.<StockDetailsDO, Double> create()
                .title("Lamp Entry")
                .build();

        lampEntryCol.setCellValueFactory(cellvaluefac -> {
            if(cellvaluefac.getValue().getLampEntryInQtl() != null) {
                return new ReadOnlyObjectWrapper( cellvaluefac.getValue().getLampEntryInQtl() + " " + QuantityField.QUANTITY_WEIGHTMENT_UNIT);
            }
            return new ReadOnlyObjectWrapper("");
        });

        columns.add(lampEntryCol);

        TableColumn<StockDetailsDO, Double> lampEntryAfterAdjInQtlCol = TableColumnBuilder.<StockDetailsDO, Double> create()
                .title("Final Entry")
                .build();

        lampEntryAfterAdjInQtlCol.setCellValueFactory(cellvaluefac -> {
            if(cellvaluefac.getValue().getLampEntryAfterAdjInQtl() != null) {
                return new ReadOnlyObjectWrapper( cellvaluefac.getValue().getLampEntryAfterAdjInQtl() + " " + QuantityField.QUANTITY_WEIGHTMENT_UNIT);
            }
            return new ReadOnlyObjectWrapper("");
        });

        columns.add(lampEntryAfterAdjInQtlCol);



        TableColumn<StockDetailsDO, String> rstNumberCol = TableColumnBuilder.<StockDetailsDO, String> create()
                .title("RST Number")
                .build();

        rstNumberCol.setCellValueFactory(cellvaluefac -> {
                if(cellvaluefac.getValue().getTransportDetailsDO() != null) {
                    return new ReadOnlyObjectWrapper(cellvaluefac.getValue().getTransportDetailsDO().getRstNumber());
                }
                return new ReadOnlyObjectWrapper("");
        });


        columns.add(rstNumberCol);

        TableColumn<StockDetailsDO, String> vehicleNumberCol = TableColumnBuilder.<StockDetailsDO, String> create()
                .title("Vehicle Number")
                .build();

        vehicleNumberCol.setCellValueFactory(cellvaluefac -> {
                if(cellvaluefac.getValue().getTransportDetailsDO() != null) {
                    return new ReadOnlyObjectWrapper(cellvaluefac.getValue().getTransportDetailsDO().getVehicleNumber());
                }
                return new ReadOnlyObjectWrapper("");
        });

        columns.add(vehicleNumberCol);

        TableColumn<StockDetailsDO, String> onBehalfCol = TableColumnBuilder.<StockDetailsDO, String> create()
                .fieldName("onBeHalf")
                .title("On Behalf")
                .decodeCode(CodeDecodeConstants.CD_INDUSTRY_CODE)
                .build();

        columns.add(onBehalfCol);

        TableColumn<StockDetailsDO, DeliveryDetailsDO> deliveryLatCol = TableColumnBuilder.<StockDetailsDO, DeliveryDetailsDO> create()
                .fieldName("deliveryDetailsDO")
                .title("Delivery Lat No.")
                .build();

        deliveryLatCol.setCellValueFactory(cellvaluefac -> {
            if(cellvaluefac.getValue().getDeliveryDetailsDO() != null) {
                return new ReadOnlyObjectWrapper(cellvaluefac.getValue().getDeliveryDetailsDO().getDeliveryLatNo());
            }
            return new ReadOnlyObjectWrapper("");
        });

        //deliveryLatCol.setVisible(false);

        columns.add(deliveryLatCol);

        TableColumn<StockDetailsDO, String> remarksCol = TableColumnBuilder.<StockDetailsDO, String> create()
                .fieldName("remarks")
                .title("Remarks")
                .build();

        columns.add(remarksCol);

        TableColumn<StockDetailsDO, Long> idCol = TableColumnBuilder.<StockDetailsDO, Long> create()
                .fieldName("id")
                .title("Id")
                .visible(false)
                .prefWidth(20)
                .build();

        columns.add(idCol);

        return columns;
    }

    @Override
    public void onEdit() {
       getController().onEdit();
    }

    @Override
    public Boolean hasRowHighlighting(StockDetailsDO item) {

        if (item != null && item.getRevisitFlag() != null && item.getRevisitFlag().equals("Y"))
            return true;

        return false;
    }

    @Override
    public void onDelete() {
        ((StockMaintenanceController)getController()).onDelete();
    }

    @Override
    public StockDetailsDO mapTableRowToDO(TableRowMapDO<String, String> tableRowMapDO) {
        StockDetailsDO stockDetailsDO = new StockDetailsDO();

        stockDetailsDO.setId(Long.parseLong(tableRowMapDO.getValue("id")));
        stockDetailsDO.setIndustryCode(tableRowMapDO.getValue("industryCode"));

        stockDetailsDO.setStockCode(tableRowMapDO.getValue("stockCode"));


        stockDetailsDO.setStockVarietyCode(tableRowMapDO.getValue("stockVarietyCode"));

        if(tableRowMapDO.getValue("numberOfPackets") != null) {
            stockDetailsDO.setNoOfPackets(Long.parseLong(tableRowMapDO.getValue("numberOfPackets")));
        }

        if(tableRowMapDO.getValue("weightment") != null) {
            stockDetailsDO.setTotalWeightment(Double.parseDouble(tableRowMapDO.getValue("weightment")));
        }

        stockDetailsDO.setLampNameCode(tableRowMapDO.getValue("lampNameCode"));
        stockDetailsDO.setMandiNameCode(tableRowMapDO.getValue("mandiNameCode"));

        if(tableRowMapDO.getValue("lampEntry") != null) {
            stockDetailsDO.setLampEntryInQtl(Double.parseDouble(tableRowMapDO.getValue("lampEntry")));
        }

        if(tableRowMapDO.getValue("lampEntryAfterAdjInQtl") != null) {
            stockDetailsDO.setLampEntryAfterAdjInQtl(Double.parseDouble(tableRowMapDO.getValue("lampEntryAfterAdjInQtl")));
        }

        stockDetailsDO.setTransactionType(tableRowMapDO.getValue("transactionType"));

        if(tableRowMapDO.getValue("rstNumber") != null || tableRowMapDO.getValue("vehicleNumber") != null){
            stockDetailsDO.setTransportDetailsDO(new TransportDetailsDO());
            stockDetailsDO.getTransportDetailsDO().setRstNumber(tableRowMapDO.getValue("rstNumber"));
            stockDetailsDO.getTransportDetailsDO().setVehicleNumber(tableRowMapDO.getValue("vehicleNumber"));
        }

        if(tableRowMapDO.getValue("partyName") != null || tableRowMapDO.getValue("partyName") != null){
            stockDetailsDO.setPartyDetailsDO(new PartyDetailsDO());
            stockDetailsDO.getPartyDetailsDO().setPartyName1(tableRowMapDO.getValue("partyName"));
        }

        try {
            Date transactionDate = DateTimeUtil.convertUTCStringToDate(tableRowMapDO.getValue("tranactionDate"));
            stockDetailsDO.setTransactionDate(transactionDate);
        }catch (ParseException e){
            logger.error("Transaction date parse error ' " + tableRowMapDO.getValue("tranactionDate") + "'");
        }
        stockDetailsDO.setOnBeHalf(tableRowMapDO.getValue("onBehalf"));

        stockDetailsDO.setRevisitFlag(tableRowMapDO.getValue("revisitFlag"));

        if(tableRowMapDO.getValue("actualAmount") != null) {
            stockDetailsDO.setCostPriceDetailsDO(new CostPriceDetailsDO());
            if( tableRowMapDO.getValue("appliedRate") != null) {
                stockDetailsDO.getCostPriceDetailsDO().setAppliedRate(Double.parseDouble(tableRowMapDO.getValue("appliedRate")));
            }
            stockDetailsDO.getCostPriceDetailsDO().setActualAmount(Double.parseDouble(tableRowMapDO.getValue("actualAmount")));
        }

        if(tableRowMapDO.getValue("deliveryLatNo") != null) {
            stockDetailsDO.setDeliveryDetailsDO(new DeliveryDetailsDO());
            stockDetailsDO.getDeliveryDetailsDO().setDeliveryLatNo(Long.parseLong(tableRowMapDO.getValue("deliveryLatNo")));
        }

        stockDetailsDO.setRemarks(tableRowMapDO.getValue("remarks"));

        return stockDetailsDO;
    }

    @Override
    public boolean hasExportToExcelAction() {
        return true;
    }

    @Override
    public FontIcon getFontIcon() {
        return AppFontIcons.STOCKMANAGEMENT;
    }

    @Override
    public String getTitle() {
        return "Stock Maintenance";
    }

    public CodeDecodeField getCdIndustry() {
        return cdIndustry;
    }

    public TextFieldExt getTfPartyName() {
        return tfPartyName;
    }

    public CodeDecodeField getCdStock() {
        return cdStock;
    }

    public CodeDecodeField getCdStockVariety() {
        return cdStockVariety;
    }

    public CodeDecodeField getCdTransactionType() {
        return cdTransactionType;
    }

    public DateTimeField getDfStartDate() {
        return dfStartDate;
    }

    public DateTimeField getDfEndDate() {
        return dfEndDate;
    }

    public TextFieldExt getTfRstNumber() {
        return tfRstNumber;
    }

    public TextFieldExt getTfVehicleNumber() {
        return tfVehicleNumber;
    }

    public CodeDecodeField getCdOnBehalf() {
        return cdOnBehalf;
    }

    public CodeDecodeField getCdLampName() {
        return cdLampName;
    }

    public CodeDecodeField getCdMandiName() {
        return cdMandiName;
    }

    public CodeDecodeField getCdRevisitFlag() {
        return cdRevisitFlag;
    }

    public void setCdRevisitFlag(CodeDecodeField cdRevisitFlag) {
        this.cdRevisitFlag = cdRevisitFlag;
    }

    @Override
    public Boolean hasSearchActions() {
        return false;
    }

    @Override
    public List<MenuItem> getFloatingMenuItems() {
        List<MenuItem> list = new ArrayList<>();
        MenuItem item = new MenuItem("Toggle Revisit Flag");
        item.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                ((StockMaintenanceController)getController()).toggleRevisitFlag();
            }
        });
        list.add(item);

        MenuItem item1 = new MenuItem("Delete");
        item1.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                ((StockMaintenanceController)getController()).onDelete();
            }
        });
        list.add(item1);
        return list;
    }
}

