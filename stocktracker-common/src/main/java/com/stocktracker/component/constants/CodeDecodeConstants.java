/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.component.constants;

/**
 * Created by diwakar009 on 21/10/2019.
 */
public class CodeDecodeConstants {
    public static final String CD_INDUSTRY_CODE = "CD_INDUSTRY_CODE";
    public static final String CD_STOCK_CODE = "CD_STOCK_CODE";
    public static final String CD_STOCK_VARIETY = "CD_STOCK_VARIETY";
    public static final String CD_TRANSACTION_TYPE = "CD_TRANS_TYPE";


    public static final String CD_LANGUAGE="CD_LANGUAGE";
    public static final String CD_PLUS_OR_MINUS_ON_STOCK_TOTAL = "PLUS_OR_MINUS_ON_TOTAL";
    public static final String CD_PLUS_OR_MINUS_ON_AMT_TOTAL = "PLUS_OR_MINUS_ON_AMT_TOTAL";

    public static final String CD_OWN_VEHICLE= "CD_OWN_VEHICLE";

    public static final String CD_PARTY_NAMES = "CD_PARTY_NAMES";

    public static final String CD_DELIVERY_TYPE = "CD_DELIVERY_TYPE";

    public static final String CD_DELIVERY_DEPO= "CD_DELIVERY_DEPO";

    public static final String CD_PARTY_TYPE = "CD_PARTY_TYPE";

    public static final String CD_TRANSPORT_TYPE = "CD_TRANSPORT_TYPE";

    public static final String CD_LAMP_CODE = "CD_LAMP_CODE";
    public static final String CD_MANDI_NAME = "CD_MANDI_NAME";

    public static final String DEFAULT_LANGUAGE = "EN_US";

    public static final String CD_CONTRACT_TYPE = "CD_CONTRACT_TYPE";
    public static final String CD_DELIVERY_PRODUCT = "CD_DELIVERY_PRODUCT";
    public static final String CD_RAW_MATERIAL = "CD_RAW_MATERIAL";
    public static final String CD_CONTRACT_STATUS = "CD_CONTRACT_STATUS";
    public static final String CD_YESNO = "CD_YESNO";
}
