/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.domainobjs.jsonwrapper;

import com.google.gson.annotations.Expose;
import com.stocktracker.domainobjs.IDomainObject;
import com.stocktracker.domainobjs.json.BaseDomainObject;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public abstract class BaseObjectJSON implements IJSONObject,Serializable{
	
	    private static final long serialVersionUID = 8777540776806878929L;
	    
	    protected Map<String,Object> argumentMap = new HashMap<String, Object>();

	    protected IDomainObject entity;
	
	    @Expose
	    private Date createdAt;

	    @Expose
	    private String createdBy;

	    @Expose
	    private Date updatedAt; 

	    @Expose
	    private String updatedBy;
		
		
		public BaseObjectJSON(Map<String,Object> argumentMap,IDomainObject entity) {
			super();
			
			this.entity = entity;
			this.argumentMap= argumentMap;
		}

		public Date getCreatedAt() {
			return createdAt;
		}

		public void setCreatedAt(Date createdAt) {
			this.createdAt = createdAt;
		}

		public String getCreatedBy() {
			return createdBy;
		}

		public void setCreatedBy(String createdBy) {
			this.createdBy = createdBy;
		}

		public Date getUpdatedAt() {
			return updatedAt;
		}

		public void setUpdatedAt(Date updatedAt) {
			this.updatedAt = updatedAt;
		}

		public String getUpdatedBy() {
			return updatedBy;
		}

		public void setUpdatedBy(String updatedBy) {
			this.updatedBy = updatedBy;
		}

		
		public void copyEntityToJSON() {
			if(entity instanceof BaseDomainObject){
				BaseDomainObject baseEntity = (BaseDomainObject) entity;
				setCreatedAt(baseEntity.getCreatedAt());
				setCreatedBy(baseEntity.getCreatedBy());
				setUpdatedAt(baseEntity.getUpdatedAt());
				setUpdatedBy(baseEntity.getUpdatedBy());
			}
		}

		public void copyJSONToEntity() {
			
			if(entity instanceof BaseDomainObject){
				BaseDomainObject baseEntity = (BaseDomainObject) entity;
				baseEntity.setCreatedAt(getCreatedAt());
				baseEntity.setCreatedBy(getCreatedBy());
				baseEntity.setUpdatedAt(getUpdatedAt());
				baseEntity.setUpdatedBy(getUpdatedBy());
			}
			
		}

		public IDomainObject getEntity() {
			return entity;
		}

		public void setEntity(IDomainObject entity) {
			this.entity = entity;
		}

		public Map<String, Object> getArgumentMap() {
			return argumentMap;
		}

		public void setArgumentMap(Map<String, Object> argumentMap) {
			this.argumentMap = argumentMap;
		}
		
		
			    
}
