/*
 * Copyright (c) 2020.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.ws.web.api.documentmanagement;

import java.util.List;


import com.desktopapp.service.AbstractService;
import com.stocktracker.domainobjs.json.DOPage;
import com.stocktracker.domainobjs.json.TableRowMapDO;
import com.stocktracker.ws.web.api.GenericBaseController;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value="/api/functionAccess")
public class FunctionAccessController extends GenericBaseController {


	@Override
	public Object save(Object domainObj) {
		return null;
	}

	@Override
	public boolean delete(Object domainObj) {
		return false;
	}

	@Override
	public boolean deleteList(List domainObjList) {
		return false;
	}

	@Override
	public Object find(Object domainObjId) {
		return null;
	}

	@Override
	public AbstractService getService() {
		return null;
	}
}
