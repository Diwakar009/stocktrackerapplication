/*
 * Copyright (c) 2020.
 *
 * All Rights are with Diwakar C
 */

package com.desktopapp.service;

/**
 * Created by diwakar009 on 1/11/2018.
 */

import com.desktopapp.service.config.AppConfig;
import com.desktopapp.service.stockmaintenance.StockDetailsService;
import com.desktopapp.service.stockmaintenance.StockService;
import com.stocktracker.domainobjs.json.sms.*;
import com.stocktracker.jpa.spring.extended.bean.StockDetailsSummary;
import com.stocktracker.jpa.spring.extended.config.ExtendedRepositoryConfig;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { AppConfig.class, ExtendedRepositoryConfig.class})
public class StockDetailsServiceTest{

    @Resource
    private StockDetailsService stockDetailsService;

    @Resource
    private StockService stockService;

    @Test
    @Rollback(true)
    public void testSaveStockDetails() {
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Calendar calobj = Calendar.getInstance();

        StockDetailsDO stockDetailsDO = new StockDetailsDO();
        stockDetailsDO.setIndustryCode("KMR");
        stockDetailsDO.setStockCode("PADD");
        stockDetailsDO.setStockVarietyCode("ODPY");


        try {
            calobj.setTime(df.parse("20/10/2019 00:00:00"));
            stockDetailsDO.setTransactionDate(calobj.getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }


        /*stockDetailsDO.setTransactionDate(calobj.getTime());*/

        stockDetailsDO.setTransactionType("RECV"); // RETN
        stockDetailsDO.setPlusOrMinusOnTotal('-');
        stockDetailsDO.setNoOfPackets(100l);
        stockDetailsDO.setTotalWeightment(100l * 0.50d);
        stockDetailsDO.setRemarks("Test Save");

        PartyDetailsDO partyDetailsDO = new PartyDetailsDO();
        partyDetailsDO.setPartyTypeCode("MANDI");
        partyDetailsDO.setPartyName1("Mandi Name");
        partyDetailsDO.setPartyName2("Broker Name");
        partyDetailsDO.setPartyName3("Farmer Name");

        stockDetailsDO.setPartyDetailsDO(partyDetailsDO);

        TransportDetailsDO transportDetailsDO = new TransportDetailsDO();
        transportDetailsDO.setTransportTypeCode("OWN");
        transportDetailsDO.setRstNumber("1245");
        transportDetailsDO.setVehicleNumber("OR 10 H 7477");
        transportDetailsDO.setVehicleCaptain("Buchi");

        stockDetailsDO.setTransportDetailsDO(transportDetailsDO);
        stockDetailsDO = stockDetailsService.saveOrUpdateStockDetails(stockDetailsDO);
        StockDO stockDO = stockDetailsDO.getStockDO();

        System.out.println("*****************************************************************");
        System.out.println(stockDetailsDO.toString());
        System.out.println(stockDO.toString());
        System.out.println("*****************************************************************");

    }

    @Test
    @Rollback(true)
    public void testSaveStockDetails_MDRV_Contract() {
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Calendar calobj = Calendar.getInstance();

        StockDetailsDO stockDetailsDO = new StockDetailsDO();
        stockDetailsDO.setIndustryCode("KMR");
        stockDetailsDO.setStockCode("PADD");
        stockDetailsDO.setStockVarietyCode("ODPY");


        try {
            calobj.setTime(df.parse("20/10/2019 00:00:00"));
            stockDetailsDO.setTransactionDate(calobj.getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }


        /*stockDetailsDO.setTransactionDate(calobj.getTime());*/

        stockDetailsDO.setTransactionType("MDRE"); // RETN
        stockDetailsDO.setPlusOrMinusOnTotal('-');
        stockDetailsDO.setNoOfPackets(100l);
        stockDetailsDO.setTotalWeightment(100l * 0.50d);
        stockDetailsDO.setRemarks("Test Save");

        stockDetailsDO.setOnBeHalf("KMR");

        PartyDetailsDO partyDetailsDO = new PartyDetailsDO();
        partyDetailsDO.setPartyTypeCode("MANDI");
        partyDetailsDO.setPartyName1("Mandi Name");
        partyDetailsDO.setPartyName2("Broker Name");
        partyDetailsDO.setPartyName3("Farmer Name");

        stockDetailsDO.setPartyDetailsDO(partyDetailsDO);

        TransportDetailsDO transportDetailsDO = new TransportDetailsDO();
        transportDetailsDO.setTransportTypeCode("OWN");
        transportDetailsDO.setRstNumber("1245");
        transportDetailsDO.setVehicleNumber("OR 10 H 7477");
        transportDetailsDO.setVehicleCaptain("Buchi");

        stockDetailsDO.setTransportDetailsDO(transportDetailsDO);
        stockDetailsDO = stockDetailsService.saveOrUpdateStockDetails(stockDetailsDO);
        StockDO stockDO = stockDetailsDO.getStockDO();

        System.out.println("*****************************************************************");
        System.out.println(stockDetailsDO.toString());
        //System.out.println(stockDO.toString());
        System.out.println("*****************************************************************");

    }



    @Test
    @Rollback(true)
    public void testGetStockDetails(){
        StockDetailsDO stockDetailsDO = new StockDetailsDO();
        stockDetailsDO.setIndustryCode("KMR");
        stockDetailsDO.setStockCode("PADD");
        stockDetailsDO.setStockVarietyCode("ODPY");
        stockDetailsDO.setId(40l);
        StockDetailsDO stockDetailsDO1 = stockDetailsService.find(stockDetailsDO);
        if(stockDetailsDO1 != null) {
            StockDO stockDO = stockDetailsDO1.getStockDO();
            PartyDetailsDO partyDetailsDO = stockDetailsDO1.getPartyDetailsDO();
            TransportDetailsDO transportDetailsDO = stockDetailsDO1.getTransportDetailsDO();
            DeliveryDetailsDO deliveryDetailsDO = stockDetailsDO1.getDeliveryDetailsDO();
            System.out.println("*****************************************************************");
            System.out.println(stockDO != null ? stockDO.toString() : "No Stock");
            System.out.println(partyDetailsDO != null ? partyDetailsDO.toString() : "No Party");
            System.out.println(transportDetailsDO != null ? transportDetailsDO.toString() : "No Transport");
            System.out.println(deliveryDetailsDO != null ? deliveryDetailsDO.toString() : "No Delivery Details");
            System.out.println("*****************************************************************");
        }
    }

    @Test
    @Rollback(true)
    public void testUpdateStockDetails() {

        StockDetailsDO stockDetailsDO = new StockDetailsDO();
        stockDetailsDO.setId(12l);
        stockDetailsDO = stockDetailsService.find(stockDetailsDO);

        stockDetailsDO.setNoOfPackets(150l);
        stockDetailsDO.setTotalWeightment(150l * 0.50d);
        stockDetailsDO.setRemarks("Test Update");

        PartyDetailsDO partyDetailsDO = stockDetailsDO.getPartyDetailsDO();
        partyDetailsDO.setPartyName1("Mandi Name1");
        partyDetailsDO.setPartyName2("Broker Name1");
        partyDetailsDO.setPartyName3("Farmer Name1");
        stockDetailsDO.setPartyDetailsDO(partyDetailsDO);


        TransportDetailsDO transportDetailsDO = stockDetailsDO.getTransportDetailsDO();
        transportDetailsDO.setRstNumber("12347");
        transportDetailsDO.setVehicleNumber("OR 10 H 17477");
        transportDetailsDO.setVehicleCaptain("Buchi1");
        stockDetailsDO.setTransportDetailsDO(transportDetailsDO);

        stockDetailsDO = stockDetailsService.saveOrUpdateStockDetails(stockDetailsDO);
        StockDO stockDO = stockDetailsDO.getStockDO();

        System.out.println("*****************************************************************");
        System.out.println(stockDetailsDO.toString());
        System.out.println(stockDO.toString());
        System.out.println("*****************************************************************");

    }

    @Test
    @Rollback(true)
    public void testDeleteStockDetail(){
        StockDetailsDO stockDetailsDO = new StockDetailsDO();
        stockDetailsDO.setId(13l);
        stockDetailsService.deleteStockDetails(stockDetailsDO);
        System.out.println("*****************************************************************");
        //System.out.println(isDeleted);
        System.out.println("*****************************************************************");
    }

    @Test
    public void testMandiReconciliationData(){
        List<MandiReconcilationDataDO> mandiReconDO =
                stockDetailsService.getMandiReconciliationData("KMR",null,null);

        for (MandiReconcilationDataDO m:mandiReconDO) {
            //System.out.println(mandiReconDO.toString());
        }

        Assert.assertEquals(true,mandiReconDO != null);
    }

    @Test
    @Rollback(true)
    public void testGetIndustryOutturnReportData(){
        Map<String,IndustryOutturnReportDO>  outturnReportDOS = stockDetailsService.getIndustryOutturnReportData();
        outturnReportDOS.entrySet().forEach(
                e -> {
                    System.out.println("Key -> " + e.getKey());
                    System.out.println("Value -> ");
                    System.out.println("Paddy..................");
                    System.out.println("Paddy in Offer Till Date (PKT) - " + e.getValue().getTotalPaddyOfferInPkt());
                    System.out.println("Paddy in Offer Till Date (QTL) - " + e.getValue().getTotalPaddyOfferInWmt());
                    System.out.println("Running Paddy Stock in PKT " + e.getValue().getTotalRunningStockInPkt());
                    System.out.println("Running Paddy Stock in QTL " + e.getValue().getTotalRunningStockInWmt());
                    System.out.println("Paddy Adjustment in QTl " + e.getValue().getTotalMandiAdjPaddyInWmt());
                    System.out.println("Rice..................");
                    System.out.println("Total Rice Produced Till Date (PKT) - " + e.getValue().getTotalRiceProducedInPkt());
                    System.out.println("Actual Rice Output - " + e.getValue().getTotalRiceProducedInWmt());
                    System.out.println("Broken Rice..................");
                    System.out.println("Total Broken Rice Till Date (PKT) - " + e.getValue().getTotalBrokenRiceProducedInPkt());
                    System.out.println("Actual Broken Rice Output - " + e.getValue().getTotalBrokenRiceProducedInWmt());
                    System.out.println("Bran Broken..................");
                    System.out.println("Total Bran Broken Till Date (PKT) - " + e.getValue().getTotalBranBrokenProducedInPkt());
                    System.out.println("Actual Broken Broken Output - " + e.getValue().getTotalBranBrokenProducedInWmt());
                    System.out.println("Bran..................");
                    System.out.println("Total Bran Till Date (PKT) - " + e.getValue().getTotalBranProducedInPkt());
                    System.out.println("Actual Bran Output - " + e.getValue().getTotalBranProducedInWmt());
                    System.out.println("Husk..................");
                    System.out.println("Actual Husk Output - " + e.getValue().getTotalHuskProducedInWmt());
                });
    }

}
