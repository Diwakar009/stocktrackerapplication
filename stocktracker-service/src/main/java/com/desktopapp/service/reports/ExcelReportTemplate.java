/*
 * Copyright (c) 2020.
 *
 * All Rights are with Diwakar C
 */

package com.desktopapp.service.reports;

import org.apache.poi.ss.usermodel.Workbook;

public interface ExcelReportTemplate {
    Workbook getWorkbook();
}
