package com.desktopapp.service.reports;

import com.stocktracker.exceptions.BusinessException;
import com.stocktracker.exceptions.TechnicalException;

import java.io.OutputStream;

public interface ReportService {
    OutputStream reteriveMandiReconReport(String industryCode) throws BusinessException,TechnicalException;
    OutputStream reteriveMandiReconReport(String industryCode,String lampName,String mandiName) throws BusinessException,TechnicalException;
    OutputStream reteriveIndustryOutturnReport(String industryCode) throws BusinessException,TechnicalException;

}
