/*
 * Copyright (C) 2017 Cem Ikta 
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 *
 */

package com.desktopapp.fx.view;


import com.desktopapp.fx.app.AppFontIcons;
import com.desktopapp.fx.control.TableColumnBuilder;
import com.desktopapp.fx.control.fonticon.FontIcon;
import com.desktopapp.fx.mvc.AbstractDataPageView;
import com.desktopapp.fx.util.I18n;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.geometry.Pos;
import javafx.scene.control.TableColumn;
import javafx.scene.control.cell.CheckBoxTableCell;
import com.stocktracker.domainobjs.json.AppUserDO;
import com.stocktracker.domainobjs.json.TableRowMapDO;

import java.util.ArrayList;
import java.util.List;

/**
 * StaticCodeDecode page view
 *
 * @author diwakar009
 */
public class AppUserPage extends AbstractDataPageView<AppUserDO> {

    @Override
    public List<TableColumn<AppUserDO, ?>> getTableViewColumns() {
        List<TableColumn<AppUserDO, ?>> columns = new ArrayList<>();


        TableColumn<AppUserDO, String> titleCol = TableColumnBuilder.<AppUserDO, String> create()
                .fieldName("title")
                .title(I18n.USER.getString("table.title"))
                .build();

        columns.add(titleCol);

        TableColumn<AppUserDO, String> firstNameCol = TableColumnBuilder.<AppUserDO, String> create()
                .fieldName("firstName")
                .title(I18n.USER.getString("table.firstName"))
                .build();

        columns.add(firstNameCol);

        TableColumn<AppUserDO, String> lastNameCol = TableColumnBuilder.<AppUserDO, String> create()
                .fieldName("lastName")
                .title(I18n.USER.getString("table.lastName"))
                .build();

        columns.add(lastNameCol);


        TableColumn<AppUserDO, String> shortNameCol = TableColumnBuilder.<AppUserDO, String> create()
                .fieldName("shortName")
                .title(I18n.USER.getString("table.shortName"))
                .build();

        columns.add(shortNameCol);

        TableColumn<AppUserDO, String> mobileCol = TableColumnBuilder.<AppUserDO, String> create()
                .fieldName("mobile")
                .title(I18n.USER.getString("table.mobile"))
                .build();

        columns.add(mobileCol);

        TableColumn<AppUserDO, String> emailCol = TableColumnBuilder.<AppUserDO, String> create()
                .fieldName("email")
                .title(I18n.USER.getString("table.email"))
                .build();

        columns.add(emailCol);

        TableColumn<AppUserDO, Boolean> activeCol = TableColumnBuilder.<AppUserDO, Boolean> create()
                .fieldName("active")
                .title(I18n.USER.getString("table.active"))
                .build();
        setActiveColumnCheckBox(activeCol);


        columns.add(activeCol);


        return columns;
    }
    private void setActiveColumnCheckBox(TableColumn<AppUserDO, Boolean> activeCol) {
        activeCol.setCellValueFactory(param -> {
            AppUserDO user = param.getValue();
            SimpleBooleanProperty activeProperty = new SimpleBooleanProperty(user.getActive());
            activeProperty.addListener((observable, oldValue, newValue) -> user.setActive(newValue));
            return activeProperty;
        });

        activeCol.setCellFactory(param -> {
            CheckBoxTableCell<AppUserDO, Boolean> cell = new CheckBoxTableCell<>();
            cell.setAlignment(Pos.CENTER);
            return cell;
        });
    }

    @Override
    public AppUserDO mapTableRowToDO(TableRowMapDO<String, String> row) {
        AppUserDO userDO = new AppUserDO();
        userDO.setFirstName(row.getValue("firstName"));
        userDO.setLastName(row.getValue("lastName"));
        userDO.setShortName(row.getValue("shortName"));
        userDO.setMobile(row.getValue("mobile"));
        userDO.setEmail(row.getValue("email"));
        userDO.setActive(new Boolean(row.getValue("active")));
        userDO.setTitle(row.getValue("title"));
        userDO.setPhone(row.getValue("phone"));
        userDO.setCity(row.getValue("city"));
        userDO.setHomepage(row.getValue("homepage"));
        userDO.setId(new Long(row.getValue("id")));
        return userDO;
    }


    @Override
    public FontIcon getFontIcon() {
        return AppFontIcons.APP_USER;
    }

    @Override
    public String getTitle() {
        return I18n.USER.getString("title");
    }
}
