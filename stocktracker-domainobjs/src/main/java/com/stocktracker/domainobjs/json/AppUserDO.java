/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.domainobjs.json;

import java.io.Serializable;

import com.stocktracker.model.AppUser;
import com.stocktracker.model.BaseEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * The persistent class for the app_user database table.
 * 
 */
@JsonIgnoreProperties (ignoreUnknown=true)
@JsonInclude(Include.NON_EMPTY)
public class AppUserDO extends BaseDomainObject implements Serializable {
	private static final long serialVersionUID = 1L;

	public static String CLASS_NAME = AppUserDO.class.getName();

	@JsonProperty("id")
	private Long id;

	@JsonProperty("active")
	private boolean active;

	@JsonProperty("address")
	private String address;

	@JsonProperty("city")
	private String city;

	@JsonProperty("firstName")
	private String firstName;

	@JsonProperty("lastName")
	private String lastName;

	@JsonProperty("title")
	private String title;

	@JsonProperty("fax")
	private String fax;

	@JsonProperty("mobile")
	private String mobile;

	@JsonProperty("email")
	private String email;

	@JsonProperty("homepage")
	private String homepage;

	@JsonProperty("notes")
	private String notes;

	@JsonProperty("phone")
	private String phone;

	@JsonProperty("postalCode")
	private String postalCode;

	@JsonProperty("region")
	private String region;

	@JsonProperty("shortName")
	private String shortName;

	@JsonProperty("skype")
	private String skype;

	//private List<AppUserLoginDO> appUserLogins;

	public AppUserDO() {
	}

	public AppUserDO(AppUser appUser) {
		copyEntity2DomainObject(appUser);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public boolean getActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFax() {
		return this.fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getHomepage() {
		return this.homepage;
	}

	public void setHomepage(String homepage) {
		this.homepage = homepage;
	}

	public String getMobile() {
		return this.mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getNotes() {
		return this.notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPostalCode() {
		return this.postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getRegion() {
		return this.region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getShortName() {
		return this.shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public String getSkype() {
		return this.skype;
	}

	public void setSkype(String skype) {
		this.skype = skype;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Override
	@JsonIgnore
	public Object getDomainObjectKey() {
		// TODO Auto-generated method stub
		return getId();
	}


	@Override
	public AppUser copyDomainObject2Entity(BaseEntity entity){

		AppUser appUser = (AppUser)entity;

		appUser.setCreatedBy(this.getCreatedBy());
		appUser.setUpdatedBy(this.getUpdatedBy());
		appUser.setCreatedAt(this.getCreatedAt());
		appUser.setUpdatedAt(this.getUpdatedAt());
		appUser.setId(this.getId());
		appUser.setLastName(this.getLastName());
		appUser.setFirstName(this.getFirstName());
		appUser.setShortName(this.getShortName());
		appUser.setMobile(this.getMobile());
		appUser.setActive(new Boolean(this.getActive()));
		appUser.setEmail(this.getEmail());
		appUser.setPhone(this.getPhone());
		appUser.setTitle(this.getTitle());
		appUser.setCity(this.getCity());
		appUser.setHomepage(this.getHomepage());
		appUser.setAddress(this.getAddress());
		appUser.setFax(this.getFax());
		appUser.setNotes(this.getNotes());
		appUser.setPostalCode(this.getPostalCode());
		appUser.setRegion(this.getRegion());
		appUser.setSkype(this.getSkype());

		return appUser;

	}

	@Override
	public void copyEntity2DomainObject(BaseEntity entity){

		AppUser appUser = (AppUser)entity;

		this.setCreatedBy(appUser.getCreatedBy());
		this.setUpdatedBy(appUser.getUpdatedBy());
		this.setCreatedAt(appUser.getCreatedAt());
		this.setUpdatedAt(appUser.getUpdatedAt());
		this.setId(appUser.getId());
		this.setLastName(appUser.getLastName());
		this.setFirstName(appUser.getFirstName());
		this.setShortName(appUser.getShortName());
		this.setMobile(appUser.getMobile());
		this.setActive(new Boolean(appUser.getActive()));
		this.setEmail(appUser.getEmail());
		this.setPhone(appUser.getPhone());
		this.setTitle(appUser.getTitle());
		this.setCity(appUser.getCity());
		this.setHomepage(appUser.getHomepage());
		this.setAddress(appUser.getAddress());
		this.setFax(appUser.getFax());
		this.setNotes(appUser.getNotes());
		this.setPostalCode(appUser.getPostalCode());
		this.setRegion(appUser.getRegion());
		this.setSkype(appUser.getSkype());

	}



}