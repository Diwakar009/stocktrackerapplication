/*
 * Copyright (c) 2020.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.exceptions;

public class BusinessException extends Exception{
    public BusinessException(String s) {
        super(s);
    }
}
