/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.domainobjs.json;

import java.io.Serializable;
import java.util.Date;

import com.stocktracker.model.BaseEntity;
import com.fasterxml.jackson.annotation.*;
import com.stocktracker.domainobjs.IDomainObject;

import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * 
 */
@JsonIgnoreProperties (ignoreUnknown=true)
@JsonInclude(Include.NON_EMPTY)
public abstract class BaseDomainObject implements Serializable,IDomainObject {

    public BaseDomainObject() {
        this.isNew = new Boolean(true);
        this.resultDO = new ResultDO();
    }

    @JsonProperty("page")
    private String page;

    @JsonProperty("size")
    private String size;

    @JsonProperty("createdAt")
    @JsonFormat(shape = JsonFormat.Shape.STRING,pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
    private Date createdAt;

	@JsonProperty("createdBy")
    private String createdBy;

	@JsonProperty("updatedAt")
    @JsonFormat(shape = JsonFormat.Shape.STRING,pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
    private Date updatedAt;

	@JsonProperty("updatedBy")
    private String updatedBy;

	@JsonProperty("isNew")
	private Boolean isNew;

    @JsonProperty("resultDO")
    @JsonIgnore
	private ResultDO resultDO;


    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    /**
     * Sets createdAt before insert
     */
    public void setCreationDate() {
        this.createdAt = new Date();
    }

    /**
     * Sets updatedAt before update
     */
    public void setChangeDate() {
        this.updatedAt = new Date();
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public Boolean isNew() {
        if(getId() == null){
            isNew = true;
        }else{
            isNew = false;
        }
        return isNew;
    }

    public void setNew(Boolean aNew) {isNew = aNew;}

    @Override
    public BaseEntity copyDomainObject2Entity(BaseEntity entity) {
        return null;
    }

    @Override
    public void copyEntity2DomainObject(BaseEntity entity) {

    }

    public Object getId(){
        return null;
    }

    public ResultDO getResultDO() {
        return resultDO;
    }

    public void setResultDO(ResultDO resultDO) {
        this.resultDO = resultDO;
    }


}
