/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.domainobjs.json.sms;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.stocktracker.domainobjs.ICompositeDomainObjectKey;

import java.io.Serializable;

/**
 * The primary key class for the IndustryDOPK
 * 
 */
@JsonIgnoreProperties (ignoreUnknown=true)
@JsonInclude(Include.NON_EMPTY)
public class IndustryDOPK implements Serializable,ICompositeDomainObjectKey {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@JsonProperty("industryCode")
	private String industryCode;

	public String getIndustryCode() {
		return industryCode;
	}

	public void setIndustryCode(String industryCode) {
		this.industryCode = industryCode;
	}

	public IndustryDOPK() {
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		IndustryDOPK that = (IndustryDOPK) o;

		return industryCode != null ? industryCode.equals(that.industryCode) : that.industryCode == null;
	}

	@Override
	public int hashCode() {
		return industryCode != null ? industryCode.hashCode() : 0;
	}
}