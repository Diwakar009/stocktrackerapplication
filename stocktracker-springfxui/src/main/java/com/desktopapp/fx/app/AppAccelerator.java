package com.desktopapp.fx.app;

import com.desktopapp.fx.control.ActionCommand;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by diwakar009 on 22/6/2019.
 */
public class AppAccelerator {

    public AppAccelerator() {

    }


    /**
     * Activate Accelerator
     *
     * @param scene
     * @param keyCodeCombination
     * @param action
     */
    public void activateAccelerator(Scene scene, KeyCodeCombination keyCodeCombination,Runnable action) {
        scene.getAccelerators().put(keyCodeCombination, action);
    }

    /**
     * Key code combination for the ribbon menu
     *
     * ALT Key down is for the ribbon menu
     */
    private static Map<String,KeyCodeCombination> ribbonMenuKeyCodeCombination = new HashMap<String,KeyCodeCombination>(){{
        put("ALT-C", new KeyCodeCombination(KeyCode.C, KeyCombination.ALT_DOWN)); // Static Decode Menu
        put("ALT-U", new KeyCodeCombination(KeyCode.U, KeyCombination.ALT_DOWN)); // App User
        put("ALT-Q", new KeyCodeCombination(KeyCode.Q, KeyCombination.ALT_DOWN)); // Quit
        put("ALT-S", new KeyCodeCombination(KeyCode.S, KeyCombination.ALT_DOWN)); // Status Bar
        put("ALT-T", new KeyCodeCombination(KeyCode.T, KeyCombination.ALT_DOWN)); //Themes
        put("ALT-P",  new KeyCodeCombination(KeyCode.P, KeyCombination.ALT_DOWN)); //Preference
        put("ALT-K",  new KeyCodeCombination(KeyCode.K, KeyCombination.ALT_DOWN)); // Keyboard
        put("ALT-I",  new KeyCodeCombination(KeyCode.I, KeyCombination.ALT_DOWN)); //Tip of the Day
        put("ALT-B",  new KeyCodeCombination(KeyCode.B, KeyCombination.ALT_DOWN)); // About
        put("F1",  new KeyCodeCombination(KeyCode.F1)); // Help
    }};


    public static Map<String,KeyCodeCombination> getRibbonMenuKeyCodeCombination(){
        return ribbonMenuKeyCodeCombination;
    }

    /**
     * Returns keycode
     *
     * @param keyCode
     * @return
     */
    public static KeyCodeCombination getKeyCode(String keyCode){

        String key = null;
        String keyCombination = null;
        KeyCodeCombination combination = null;

        String[] keyCodeSplit = keyCode.split("-");
        if(keyCodeSplit.length == 2){
            key = keyCodeSplit[1].trim();
            keyCombination = keyCodeSplit[0].trim();
        }else if (keyCodeSplit.length == 1){
            key = keyCodeSplit[0];
        }

        if(keyCombination == null) {
            return (combination = new KeyCodeCombination(KeyCode.getKeyCode(key)));
        }else {
            return (combination = new KeyCodeCombination(KeyCode.getKeyCode(key),getKeyCombination(keyCombination)));
        }
    }

    private static KeyCombination.Modifier getKeyCombination(String keyCombination){
            if("ALT".equals(keyCombination)){
                return  KeyCombination.ALT_DOWN;
            }else if("SHIFT".equals(keyCombination)){
                return  KeyCombination.SHIFT_DOWN;
            }else if("CTRL".equals(keyCombination)){
                return  KeyCombination.CONTROL_DOWN;
            }
            return KeyCombination.SHORTCUT_DOWN;
    }

}
