package com.desktopapp.fx.commands;

import com.desktopapp.fx.controller.AppUserController;
import com.desktopapp.fx.controller.AppUserLoginController;
import com.desktopapp.fx.objectfactory.ObjectFactory;
import com.desktopapp.fx.view.AppView;

/**
 * Created by diwakar009 on 24/12/2018.
 */
public class AppUserLoginCommand implements ICommand {

    private AppView view;

    private AppUserLoginController appUserLoginController;

    public AppUserLoginCommand(AppView view) {
        this.view = view;
        this.appUserLoginController = (AppUserLoginController) ObjectFactory.getControllerObject(ObjectFactory.APP_USER_LOGIN_CTRL);
    }

    @Override
    public void executeCommand() {
        this.view.addPageToCenter(appUserLoginController.getDataPageView());
    }

}
