/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.domainobjs.json;

import java.io.Serializable;

import com.stocktracker.domainobjs.ICompositeDomainObjectKey;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * The primary key class for the static_code_decode database table.
 * 
 */
@JsonIgnoreProperties (ignoreUnknown=true)
@JsonInclude(Include.NON_EMPTY)
public class StaticCodeDecodeDOPK implements Serializable,ICompositeDomainObjectKey {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@JsonProperty("codeName")
	private String codeName;
	
	@JsonProperty("language")
	private String language;

	@JsonProperty("codeValue")
	private String codeValue;

	@JsonProperty("codeValueFilter")
	private String codeValueFilter;




	public StaticCodeDecodeDOPK() {
	}
	public String getCodeName() {
		return this.codeName;
	}
	public void setCodeName(String codeName) {
		this.codeName = codeName;
	}
	public String getLanguage() {
		return this.language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public String getCodeValue() {
		return this.codeValue;
	}
	public void setCodeValue(String codeValue) {
		this.codeValue = codeValue;
	}
	public String getCodeValueFilter() {return codeValueFilter;}
	public void setCodeValueFilter(String codeValueFilter) {this.codeValueFilter = codeValueFilter;}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		StaticCodeDecodeDOPK that = (StaticCodeDecodeDOPK) o;

		if (!codeName.equals(that.codeName)) return false;
		if (!language.equals(that.language)) return false;
		if (!codeValue.equals(that.codeValue)) return false;
		return codeValueFilter != null ? codeValueFilter.equals(that.codeValueFilter) : that.codeValueFilter == null;
	}

}