package com.desktopapp.service;

import java.io.ByteArrayInputStream;

import com.desktopapp.service.common.IEmailGatewayService;
import com.stocktracker.domainobjs.json.DocContentDO;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;


/**
 * Document Content Service
 *
 * @author Ratnala Diwakar Choudhury
 */
@SuppressWarnings("rawtypes")
@Service
public class DocumentContentService extends AbstractService<DocContentDO> {

    private final static Logger LOGGER = Logger
            .getLogger(DocumentContentService.class);

    IEmailGatewayService emailGatewayService;


    public DocumentContentService() {
        super(DocContentDO.class);
    }

    public ByteArrayInputStream downloadDocument (DocContentDO docContentDO){
         String getCallWebURL = getWebserviceURL() + "/downloadDocument";
         byte[] response = getRestTemplate().postForObject(getCallWebURL, docContentDO, byte[].class);

         return new ByteArrayInputStream(response);
    }

    public IEmailGatewayService getEmailGatewayService() {
        return emailGatewayService;
    }

    public void setEmailGatewayService(IEmailGatewayService emailGatewayService) {
        this.emailGatewayService = emailGatewayService;
    }


    public static final String BASE_RESOURCE = "documentContent";

    @Override
    protected String getBaseResource() {
        return BASE_RESOURCE;
    }

}
