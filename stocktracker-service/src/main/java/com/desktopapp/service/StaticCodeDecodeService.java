package com.desktopapp.service;


import com.stocktracker.jpa.spring.extended.StaticCodeDecodeRepository;
import com.stocktracker.model.BaseEntity;
import com.stocktracker.model.StaticCodeDecode;
import com.stocktracker.domainobjs.IDomainObject;
import com.stocktracker.domainobjs.json.CodeDataItemDO;
import com.stocktracker.domainobjs.json.DOPage;
import com.stocktracker.domainobjs.json.StaticCodeDecodeDO;
import com.stocktracker.domainobjs.json.TableRowMapDO;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Static COde Decode Service
 *
 * @author Ratnala Diwakar Choudhury
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS,readOnly = true)
public class StaticCodeDecodeService extends AbstractService<StaticCodeDecodeDO> {

    @Resource
    private StaticCodeDecodeRepository staticCodeDecodeRepository;

    public StaticCodeDecodeService() {
        super(StaticCodeDecodeDO.class);
    }

    /**
     * Get code decode item list with default en_us
     *
     * @param codeName
     * @return
     */
    public Optional<List<CodeDataItemDO>> getCodeDataItemList(String codeName) {
        return getStaticCodeDataItemList(codeName, "EN_US");
    }

    public Optional<List<CodeDataItemDO>> getStaticCodeDataItemList(String codeName, String langauage) {
        return getCodeDataItemList(codeName, langauage, false);
    }

    public Optional<List<CodeDataItemDO>> getStaticCodeDataItemList(String codeName, String filterCodeValue, String langauage) {
        return getCodeDataItemList(codeName, filterCodeValue, langauage, false);
    }

    /**
     * Get code Data item List
     *
     * @param codeName
     * @param langauage
     * @return
     */
    public Optional<List<CodeDataItemDO>> getCodeDataItemList(String codeName, String filterCodeValue, String langauage, boolean emptyCode) {

        List<CodeDataItemDO> list = new ArrayList<CodeDataItemDO>();
        CodeDataItemDO EMPTY_CODE_DECODE_ITEM = new CodeDataItemDO("", "", "", "EN_US", "");
        if (!emptyCode) {
            list.add(EMPTY_CODE_DECODE_ITEM);
        }
        List<StaticCodeDecodeDO> codeDecodeList =
                returnDomainObjects(((StaticCodeDecodeRepository) staticCodeDecodeRepository).findByCodeNameAndCodeValueFilter(codeName, filterCodeValue, langauage));

        if (codeDecodeList != null) {
            codeDecodeList.forEach(codeDecode -> {
                CodeDataItemDO item = new CodeDataItemDO(codeDecode.getCodeName(), codeDecode.getCodeValue(), "", "EN_US", codeDecode.getCodeDesc());
                list.add(item);
            });
        }
        return Optional.ofNullable(list);
    }

    /**
     * Get code Data item List
     *
     * @param codeName
     * @param langauage
     * @return
     */
    public Optional<List<CodeDataItemDO>> getCodeDataItemList(String codeName, String langauage, boolean emptyCode) {

        List<CodeDataItemDO> list = new ArrayList<CodeDataItemDO>();
        CodeDataItemDO EMPTY_CODE_DECODE_ITEM = new CodeDataItemDO("", "", "", "EN_US", "");
        if (!emptyCode) {
            list.add(EMPTY_CODE_DECODE_ITEM);
        }
        List<StaticCodeDecodeDO> codeDecodeList =
                returnDomainObjects(((StaticCodeDecodeRepository) staticCodeDecodeRepository).findByCodeNameNLang(codeName, langauage));

        if (codeDecodeList != null) {
            codeDecodeList.forEach(codeDecode -> {
                CodeDataItemDO item = new CodeDataItemDO(codeDecode.getCodeName(), codeDecode.getCodeValue(), "", "EN_US", codeDecode.getCodeDesc());
                list.add(item);
         });
        }
        return Optional.ofNullable(list);
    }


    /**
     * list static code decodes using codename and lang
     *
     * @param parameters
     * @return
     */
    @Transactional
    public DOPage<TableRowMapDO<String, String>> listByCodeNameNLang(Map<String, Object> parameters) {
        DOPage<TableRowMapDO<String, String>> doPage = new DOPage<TableRowMapDO<String, String>>();
        int page = Integer.valueOf((String) parameters.get("page"));
        int size = Integer.valueOf((String) parameters.get("size"));
        String codeName = (String) parameters.get("codeName");
        String language = (String) parameters.get("language");
        String filter = (String) parameters.get("search");

        Page<? extends BaseEntity> pagedData = getRepository().listByCodeNameNLangNFilter(codeName,language,(filter == null || "".equals(filter)) ? "%" : filter,PageRequest.of(page,size));
        copyPageToDOPageSettings(pagedData, doPage);
        List<? extends IDomainObject> domainObjects = returnDomainObjects(pagedData.getContent());

        if (domainObjects != null) {
            for (IDomainObject domainObject : domainObjects) {
                doPage.getList().add(copyDomainObjToTableRowMap(domainObject));
            }
        }

        return doPage;
    }

    @Transactional
    public void deleteByCodeNameAndLanguage(String codeName,String language){
        staticCodeDecodeRepository.deleteByCodeNameAndLanguage(codeName,language);
    }

    @Override
    public Class<StaticCodeDecode> getEntityClass() {
        return StaticCodeDecode.class;
    }

    @Override
    public StaticCodeDecodeDO returnDomainObject(BaseEntity entity) {

        if (entity == null) {
            return null;
        }

        StaticCodeDecodeDO domainObject = new StaticCodeDecodeDO((StaticCodeDecode) entity);
        return domainObject;
    }

    @Override
    public List<StaticCodeDecodeDO> returnDomainObjects(List<? extends BaseEntity> entities) {
        if (entities == null) {
            return null;
        }
        List<StaticCodeDecodeDO> domainObjects = new ArrayList<StaticCodeDecodeDO>();

        if (entities != null) {
            entities.forEach(entity -> {
                StaticCodeDecodeDO domainObject = new StaticCodeDecodeDO((StaticCodeDecode) entity);
                domainObjects.add(domainObject);
            });
        }
        return domainObjects;
    }

    @Override
    public BaseEntity returnEntity(StaticCodeDecodeDO domainObject) {

        StaticCodeDecodeDO staticCodeDecodeDO = (StaticCodeDecodeDO) domainObject;
        StaticCodeDecode staticCodeDecode = new StaticCodeDecode();
        staticCodeDecodeDO.copyDomainObject2Entity(staticCodeDecode);

        return staticCodeDecode;
    }

    @Override
    public Page<? extends BaseEntity> findByFilter(Map<String, Object> parameters, PageRequest pageRequest) {
        String searchFilter = (String)parameters.get("search");
        return getRepository().findUniqueAllByFilter(searchFilter,pageRequest);
    }

    @Override
    public Page<? extends BaseEntity> findAll(Map<String, Object> parameters,PageRequest pageRequest){
        return getRepository().findUniqueAll(pageRequest);
    }

    @Override
    public List<? extends BaseEntity> returnEntities(List<StaticCodeDecodeDO> domainObjects) {
        List<StaticCodeDecode> entities = new ArrayList();
        for (IDomainObject domainObject : domainObjects) {
            StaticCodeDecodeDO staticCodeDecodeDO = (StaticCodeDecodeDO) domainObject;
            StaticCodeDecode staticCodeDecode = new StaticCodeDecode();
            staticCodeDecodeDO.copyDomainObject2Entity(staticCodeDecode);
            entities.add(staticCodeDecode);
        }
        return entities;

    }

    @Override
    public TableRowMapDO<String, String> copyDomainObjToTableRowMap(IDomainObject domainObject) {
        StaticCodeDecodeDO code = (StaticCodeDecodeDO) domainObject;
        TableRowMapDO<String, String> tableRowMapDO = new TableRowMapDO<String, String>();
        tableRowMapDO.add("id", code.getId().toString());
        tableRowMapDO.add("codeName", code.getCodeName());
        tableRowMapDO.add("codeValueFilter", code.getCodeValueFilter());
        tableRowMapDO.add("codeValue", code.getCodeValue());
        tableRowMapDO.add("language", code.getLanguage());
        tableRowMapDO.add("codeDesc", code.getCodeDesc());
        tableRowMapDO.setClazzName(StaticCodeDecodeDO.CLASS_NAME);

        return tableRowMapDO;
    }

    public String getCodeDescription(String codeName,String codeValue,String language){
        Optional<String> codeDescription = getCodeValueDescription(codeName,codeValue,language);
        if(codeDescription.isPresent()){
            return codeDescription.get();
        }
        return codeValue;
    }

    public Optional<String> getCodeValueDescription(String codeName, String codeValue, String language) {
        List<StaticCodeDecode> codeValueDescriptions = staticCodeDecodeRepository.getCodeValueDescription(codeName,codeValue,language);
        if(!codeValueDescriptions.isEmpty() && codeValueDescriptions.size() > 0){
            return Optional.ofNullable(codeValueDescriptions.get(0).getCodeDesc());
        }
        return Optional.empty();
    }

    @Override
    public StaticCodeDecodeRepository getRepository() {
        return this.staticCodeDecodeRepository;
    }
}
