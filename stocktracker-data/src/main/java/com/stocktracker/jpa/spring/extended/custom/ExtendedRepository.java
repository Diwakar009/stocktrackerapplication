package com.stocktracker.jpa.spring.extended.custom;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.List;
import java.util.Map;

@NoRepositoryBean
public interface ExtendedRepository<T, ID>
  extends JpaRepository<T, ID> {
  
    public List<T> findByAttributeContainsText(String attributeName, String text);
    public Page<T> findByNamedQuery(String namedQuery, Pageable pageable, Map<String,Object> parameters);
}