package com.desktopapp.service;

import com.stocktracker.domainobjs.json.AppUserLoginDO;
import org.springframework.stereotype.Service;


/**
 * App Login User Service
 *
 * @author Ratnala Diwakar Choudhury
 */
@SuppressWarnings("rawtypes")
@Service
public class AppUserLoginService extends AbstractService<AppUserLoginDO> {

    public AppUserLoginService() {
        super(AppUserLoginDO.class);
    }
    
    public AppUserLoginDO getAppUserLoginByUserName(String userName) {

		AppUserLoginDO appUserLoginDO = (AppUserLoginDO) callGetWebService("appUserLoginByUserName", AppUserLoginDO.class, QueryParameter.with("userName",userName).parameters());
    	/*
    	TableRowMapDO rowMap = getRestTemplate().getForObject(constructQueryParams(getWebserviceURL() + "/" + "appUserLoginByUserName",QueryParameter.with("userName",userName).parameters())
    						,TableRowMapDO.class
    						, QueryParameter.with("userName",userName).parameters());
    	*/					
    	return appUserLoginDO;
    }
    
	public static final String BASE_RESOURCE = "appUserLogin";
		
	@Override
	protected String getBaseResource() {
		return BASE_RESOURCE;
	}

}
