package com.desktopapp.fx.view;

import com.desktopapp.fx.DesktopFxApplication;
import com.desktopapp.fx.control.CodeDecodeField;
import com.desktopapp.fx.control.TextFieldExt;
import com.desktopapp.fx.controller.AppUserController;
import com.desktopapp.fx.mvc.AbstractFormViewDialog;
import com.desktopapp.fx.mvc.DataPageController;
import com.desktopapp.fx.util.I18n;
import com.desktopapp.fx.util.ViewHelpers;
import com.stocktracker.component.constants.DocStoreConstants;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import org.controlsfx.validation.Validator;
import com.stocktracker.domainobjs.json.AppUserDO;

/**
 * Created by diwakar009 on 3/3/2019.
 */
public class AppUserForm extends AbstractFormViewDialog<AppUserDO> {

    private AppUserDO appUserDO;
    private TextFieldExt tfCodeDesc;
    private TextFieldExt tfCodeName;
    private TextFieldExt tfCodeValue;
    private CodeDecodeField cdLanguage;

    private AppUserController appUserController;

    public AppUserForm(DataPageController<AppUserDO> controller, AppUserDO appUserDO) {
        super(DesktopFxApplication.get().getMainStage(), controller);
        this.appUserDO = appUserDO;
        this.appUserController = (AppUserController)controller;
        setTitle(getFormTitle());
    }

    @Override
    public String getFormTitle() {
        return appUserDO.isNew() ? I18n.USER.getString("form.newTitle") : I18n.USER.getString("form.editTitle");
    }

    @Override
    public Node buildFormView() {

        tfCodeName = ViewHelpers.createTextFieldExt(30, 200);
        getValidationSupport().registerValidator(tfCodeName, true,
                Validator.createEmptyValidator(I18n.COMMON.getString("validation.required")));

        tfCodeValue = ViewHelpers.createTextFieldExt(5, 200);
        getValidationSupport().registerValidator(tfCodeValue, true,
                Validator.createEmptyValidator(I18n.COMMON.getString("validation.required")));

        cdLanguage = ViewHelpers.createCodeDecodeField(DocStoreConstants.CD_LANGUAGE,"EN_US",200);

        tfCodeDesc = ViewHelpers.createTextFieldExt(100, 200);
        getValidationSupport().registerValidator(tfCodeDesc, true,
                Validator.createEmptyValidator(I18n.COMMON.getString("validation.required")));


        GridPane formPane = new GridPane();
        formPane.setPadding(new Insets(20, 30, 10, 30));
        formPane.setHgap(10);
        formPane.setVgap(5);

        formPane.add(new Label(I18n.STATICCODEDECODE.getString("form.codeName")), 0, 0);
        formPane.add(tfCodeName, 1, 0);

        formPane.add(new Label(I18n.STATICCODEDECODE.getString("form.codeValue")), 0, 1);
        formPane.add(tfCodeValue, 1, 1);

        formPane.add(new Label(I18n.STATICCODEDECODE.getString("form.codeDesc")), 0, 2);
        formPane.add(tfCodeDesc, 1, 2);

        formPane.add(new Label(I18n.STATICCODEDECODE.getString("form.language")), 0, 3);
        formPane.add(cdLanguage.asControl(), 1, 3);

        return formPane;
    }

    @Override
    public AppUserDO getCurrentEntity() {
        return appUserDO;
    }

    @Override
    public void loadLatestEntity() {
        appUserController.loadData(appUserDO);
    }

    @Override
    public void pop() {

    }

    @Override
    public void push() {

        if (appUserDO.isNew()) {
            appUserDO.setCreatedBy(DesktopFxApplication.getLoginSession().getUserName());
        } else {
            appUserDO.setUpdatedBy(DesktopFxApplication.getLoginSession().getUserName());
        }
    }
}
