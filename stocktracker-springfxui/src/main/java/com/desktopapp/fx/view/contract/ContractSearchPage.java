/*
 * Copyright (c) 2020.
 *
 * All Rights are with Diwakar C
 */

package com.desktopapp.fx.view.contract;


import com.desktopapp.fx.app.AppFontIcons;
import com.desktopapp.fx.common.IAccelerator;
import com.desktopapp.fx.common.IGenericComponent;
import com.desktopapp.fx.common.PageDetails;
import com.desktopapp.fx.control.*;
import com.desktopapp.fx.control.fonticon.FontIcon;
import com.desktopapp.fx.controller.ContractMaintenanceController;
import com.desktopapp.fx.mvc.AbstractDataPageView;
import com.desktopapp.fx.util.ViewHelpers;
import com.stocktracker.component.constants.CodeDecodeConstants;
import com.stocktracker.domainobjs.json.DOPage;
import com.stocktracker.domainobjs.json.TableRowMapDO;
import com.stocktracker.domainobjs.json.sms.ContractDO;
import com.stocktracker.domainobjs.json.sms.CostPriceDetailsDO;
import com.stocktracker.domainobjs.json.sms.StockDetailsDO;
import com.stocktracker.util.DateTimeUtil;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.Tooltip;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.layout.*;
import javafx.scene.text.Text;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.util.*;

/**
 * Contract Search page
 *
 * @author diwakar009
 */
public class ContractSearchPage extends AbstractDataPageView<ContractDO> {

    private final Logger logger = LoggerFactory.getLogger(ContractSearchPage.class);

    private CodeDecodeField cdIndustry;
    private DateTimeField dfStartDateRangeStart;
    private DateTimeField dfStartDateRangeEnd;
    private CodeDecodeField cdContractType;
    private TextFieldExt tfContractName;
    private CodeDecodeField cdDeliveryProduct;
    private CodeDecodeField cdContractStatus;

    private ActionButtonCommand btnSearch;
    private ActionButtonCommand btnClear;

    private List<IGenericComponent> inputComponents = new ArrayList<>();

    private ScrollPane scrollPane;

    @Override
    protected Node renderCenter(){
        scrollPane = new ScrollPane();
        scrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
        scrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
        scrollPane.fitToWidthProperty().set(true);

        BorderPane dataPageBroderView = new BorderPane();
        dataPageBroderView.getStyleClass().addAll("search-data-page-view");
        dataPageBroderView.setTop(getScreenHeaderBar());
        scrollPane.setContent(buildCenter());
        dataPageBroderView.setCenter(scrollPane);
        dataPageBroderView.setBottom(buildButtom());
        return dataPageBroderView;
    }

    private Node buildCenter(){
        BorderPane searchPageCenterView = new BorderPane();
        searchPageCenterView.getStyleClass().addAll("search-data-page-center");
        searchPageCenterView.setTop(buildFilterPane());
        BorderPane tablePaneBorder = new BorderPane();
        VBox centerBox = new VBox();

        VBox contractTable = new VBox();
        contractTable.getChildren().add(getTableDataView());
        centerBox.getChildren().add(contractTable);

        tablePaneBorder.setCenter(centerBox);
        searchPageCenterView.setCenter(tablePaneBorder);
        return searchPageCenterView;
    }


    private Node buildFilterPane(){

        this.cdIndustry = new CodeDecodeField(CodeDecodeConstants.CD_INDUSTRY_CODE,CodeDecodeConstants.DEFAULT_LANGUAGE);
        this.cdIndustry.setPrefWidth(200);
        this.dfStartDateRangeStart = new DateTimeField();
        this.dfStartDateRangeEnd = new DateTimeField();
        cdContractType = new CodeDecodeField(CodeDecodeConstants.CD_CONTRACT_TYPE,CodeDecodeConstants.DEFAULT_LANGUAGE);
        tfContractName = new TextFieldExt(200);
        cdDeliveryProduct = new CodeDecodeField(CodeDecodeConstants.CD_DELIVERY_PRODUCT,CodeDecodeConstants.DEFAULT_LANGUAGE);
        cdContractStatus = new CodeDecodeField(CodeDecodeConstants.CD_CONTRACT_STATUS,CodeDecodeConstants.DEFAULT_LANGUAGE);

        this.inputComponents.add(cdIndustry);
        this.inputComponents.add(dfStartDateRangeStart);
        this.inputComponents.add(dfStartDateRangeEnd);
        this.inputComponents.add(cdContractType);
        this.inputComponents.add(tfContractName);
        this.inputComponents.add(cdDeliveryProduct);
        this.inputComponents.add(cdContractStatus);

        this.btnSearch= new ActionButtonCommand("Search",event -> onSearch());
        this.btnClear= new ActionButtonCommand("Clear",event -> onClear());

        this.btnSearch.setTooltip(new Tooltip("Search"));
        this.btnSearch.getStyleClass().add("left-pill");

        this.btnClear.setTooltip(new Tooltip("Clear"));
        this.btnClear.getStyleClass().add("left-pill");

        BorderPane borderPane = new BorderPane();

            GridPane formPane = new GridPane();
        formPane.setPadding(new Insets(20, 30, 10, 30));
        formPane.setHgap(10);
        formPane.setVgap(10);

        formPane.setAlignment(Pos.CENTER);

        formPane.add(new Label("Mill Name"), 0, 0);
        formPane.add(cdIndustry.asControl(), 1, 0);

        formPane.add(new Label("Contract Name"), 2, 0);
        formPane.add(tfContractName, 3, 0);

        formPane.add(new Label("Contract Type"), 0, 1);
        formPane.add(cdContractType.asControl(), 1, 1);

        formPane.add(new Label("Contract Status"), 2, 1);
        formPane.add(cdContractStatus.asControl(), 3, 1);

        formPane.add(new Label("Delivery Product"), 0, 2);
        formPane.add(cdDeliveryProduct.asControl(), 1, 2);

        formPane.add(new Label("Start Date"), 2, 2);
        HBox startDateEndDateHBox = new HBox();
        startDateEndDateHBox.getChildren().add(dfStartDateRangeStart);
        startDateEndDateHBox.getChildren().add(new Text(" __ "));
        startDateEndDateHBox.getChildren().add(dfStartDateRangeEnd);
        formPane.add(startDateEndDateHBox,3 , 2);

        HBox buttonBox = new HBox();
        buttonBox.setStyle("-fx-pref-height: 50px;");
        buttonBox.getChildren().add(btnSearch);
        buttonBox.getChildren().add(btnClear);
        buttonBox.setAlignment(Pos.CENTER);
        buttonBox.setSpacing(40);

        borderPane.setTop(formPane);
        borderPane.setCenter(buttonBox);
        return borderPane;
    }

    @Override
    public DOPage<TableRowMapDO<String, String>> getData(String filter, PageDetails details) {
        String industryCode = this.cdIndustry.getDataValue();
        Date startDateRangeStart = this.dfStartDateRangeStart.getDataValueAsDate();
        Date startDateRangeEnd = this.dfStartDateRangeEnd.getDataValueAsDate();
        String contractType = this.cdContractType.getDataValue();
        String contractName = this.tfContractName.getDataValue();
        String contractStatus = this.cdContractStatus.getDataValue();
        return ((ContractMaintenanceController)getController()).getData(industryCode,startDateRangeStart,startDateRangeEnd,contractType,contractName,contractStatus,details);
    }

    /**
     * Event on click of onSearch
     *
     */
    public void onSearch(){
       getTableDataView().resetPager();
       refreshData();
    }

    @Override
    public void refreshData() {
        super.refreshData();
    }

    /**
     * Event on click onSummary
     *
     * Summary the filter criteria
     *
     */
    public void onSummary(){
        this.inputComponents.forEach(
                component -> {component.clearDataValue(); });
    }

    /**
     * Event on click onClear
     *
     * Clear the filter criteria
     *
     */
    public void onClear(){
        this.inputComponents.forEach(
                component -> {component.clearDataValue();
                });
    }

    /**
     * Screen Header
     *
     * @return
     */
    protected HBox getScreenHeaderBar() {
        logger.debug("get data page header");
        HBox headerBar = new HBox();
        headerBar.getStyleClass().add("search-data-page-header");

        Label title = ViewHelpers.createFontIconLabel(getTitle(), getFontIcon(), "");
        title.getStyleClass().add("search-data-page-title");

        Region spacer = new Region();
        HBox.setHgrow(spacer, Priority.ALWAYS);
        headerBar.getChildren().addAll(title, spacer);
        return headerBar;
    }

    @Override
    public IAccelerator getAccelerator() {
        return new IAccelerator() {
            @Override
            public Map<String, Map<KeyCodeCombination, Runnable>> getAccelerators() {
                Map<String,Map<KeyCodeCombination, Runnable>> combinations = new HashMap<String,Map<KeyCodeCombination, Runnable>>();
                Map<KeyCodeCombination,Runnable> childAccelerators = getDataPageKeyCodeCombination();
                if (hasPrintActions()) {
                    childAccelerators.putAll(getPrintKeyCodeCombination());
                }
                combinations.put(IAccelerator.CHILD_ACCELERATORS,childAccelerators);
                combinations.put(IAccelerator.PARENT_ACCELERATORS,new HashMap<KeyCodeCombination,Runnable>());

                return combinations;
            }
        };
    }

    @Override
    public List<TableColumn<ContractDO, ?>> getTableViewColumns() {
        List<TableColumn<ContractDO, ?>> columns = new ArrayList<>();

        TableColumn<ContractDO, Date> startDateCol = TableColumnBuilder.<ContractDO, Date> create()
                .fieldName("startDate")
                .title("Start Date")
                .dataType(TableColumnBuilder.DATATYPE_DATE)
                .build();

        columns.add(startDateCol);

        TableColumn<ContractDO, String> industryCodeCol = TableColumnBuilder.<ContractDO, String> create()
                .fieldName("industryCode")
                .title("Industry Code")
                .decodeCode(CodeDecodeConstants.CD_INDUSTRY_CODE)
                .build();

        columns.add(industryCodeCol);

        TableColumn<ContractDO, String> contractTypeCol = TableColumnBuilder.<ContractDO, String> create()
                .fieldName("contractTypeCode")
                .title("Contract Type")
                .decodeCode(CodeDecodeConstants.CD_CONTRACT_TYPE)
                .build();

        columns.add(contractTypeCol);

        TableColumn<ContractDO, String> contractNameCol = TableColumnBuilder.<ContractDO, String> create()
                .fieldName("contractName")
                .title("Contract Name")
                .build();

        columns.add(contractNameCol);

        TableColumn<ContractDO, String> deliveryProductCol = TableColumnBuilder.<ContractDO, String> create()
                .fieldName("deliveryProduct")
                .title("Delivery Product")
                .decodeCode(CodeDecodeConstants.CD_DELIVERY_PRODUCT)
                .build();

        columns.add(deliveryProductCol);

        TableColumn<ContractDO, String> rawMaterialCol = TableColumnBuilder.<ContractDO, String> create()
                .fieldName("rawMaterial")
                .title("Raw Material")
                .decodeCode(CodeDecodeConstants.CD_RAW_MATERIAL)
                .build();

        columns.add(rawMaterialCol);


        TableColumn<ContractDO, Double> approvedRawMaterialQuantity = TableColumnBuilder.<ContractDO, Double> create()
                .title("Approved Quantity")
                .build();

        approvedRawMaterialQuantity.setCellValueFactory(cellvaluefac -> {
            if(cellvaluefac.getValue().getApprovedRawMaterialQuantity() != null) {
                return new ReadOnlyObjectWrapper(cellvaluefac.getValue().getApprovedRawMaterialQuantity() + " " + TableColumnBuilder.getWeightmentUnit(cellvaluefac.getValue().getRawMaterial(),""));
            }
            return new ReadOnlyObjectWrapper("");
        });
        columns.add(approvedRawMaterialQuantity);

        TableColumn<ContractDO, Double> receivedRawMaterialQuantity = TableColumnBuilder.<ContractDO, Double> create()
                .title("Received Quantity")
                .build();

        receivedRawMaterialQuantity.setCellValueFactory(cellvaluefac -> {
            if(cellvaluefac.getValue().getReceivedRawMaterialQuantity() != null) {
                return new ReadOnlyObjectWrapper(cellvaluefac.getValue().getReceivedRawMaterialQuantity()+ " " + TableColumnBuilder.getWeightmentUnit(cellvaluefac.getValue().getRawMaterial(),""));
            }
            return new ReadOnlyObjectWrapper("");
        });
        columns.add(receivedRawMaterialQuantity);

        TableColumn<ContractDO, Double> productDeliveryQuantityCol = TableColumnBuilder.<ContractDO, Double> create()
                .title("Delivery Quantity")
                .build();

        productDeliveryQuantityCol.setCellValueFactory(cellvaluefac -> {
            if(cellvaluefac.getValue().getProductDeliveryQuantity() != null) {
                return new ReadOnlyObjectWrapper(cellvaluefac.getValue().getProductDeliveryQuantity() + " " + TableColumnBuilder.getWeightmentUnit(cellvaluefac.getValue().getDeliveryProduct(),""));
            }
            return new ReadOnlyObjectWrapper("");
        });
        columns.add(productDeliveryQuantityCol);

        TableColumn<ContractDO, Double> productDeliveredQuantityCol = TableColumnBuilder.<ContractDO, Double> create()
                .title("Pending Product Wmt")
                .build();

        productDeliveredQuantityCol.setCellValueFactory(cellvaluefac -> {
            if(cellvaluefac.getValue().getProductDeliveredQuantity() != null) {
                return new ReadOnlyObjectWrapper(cellvaluefac.getValue().getProductDeliveredQuantity() + " " + TableColumnBuilder.getWeightmentUnit(cellvaluefac.getValue().getDeliveryProduct(),""));
            }
            return new ReadOnlyObjectWrapper("");
        });
        columns.add(productDeliveredQuantityCol);

        TableColumn<ContractDO, String> statusCol = TableColumnBuilder.<ContractDO, String> create()
                .fieldName("status")
                .title("Contract Status")
                .decodeCode(CodeDecodeConstants.CD_CONTRACT_STATUS)
                .build();

        columns.add(statusCol);


        TableColumn<ContractDO, String> remarksCol = TableColumnBuilder.<ContractDO, String> create()
                .fieldName("remarks")
                .title("Remarks")
                .build();

        columns.add(remarksCol);

        TableColumn<ContractDO, Long> idCol = TableColumnBuilder.<ContractDO, Long> create()
                .fieldName("id")
                .title("Id")
                .visible(false)
                .prefWidth(20)
                .build();

        columns.add(idCol);

        return columns;
    }

    @Override
    public void onEdit() {
       getController().onEdit();
    }


    @Override
    public ContractDO mapTableRowToDO(TableRowMapDO<String, String> tableRowMapDO) {
        ContractDO contractDO = new ContractDO();
        contractDO.setId(Long.parseLong(tableRowMapDO.getValue("id")));
        contractDO.setIndustryCode(tableRowMapDO.getValue("industryCode"));
        contractDO.setContractName(tableRowMapDO.getValue("contactName"));
        contractDO.setContractTypeCode(tableRowMapDO.getValue("contactTypeCode"));
        contractDO.setDeliveryProduct(tableRowMapDO.getValue("deliveryProduct"));
        contractDO.setRawMaterial(tableRowMapDO.getValue("rawMaterial"));

        try {
            Date startDate = DateTimeUtil.convertUTCStringToDate(tableRowMapDO.getValue("startDate"));
            contractDO.setStartDate(startDate);
        }catch (ParseException e){
            logger.error("Start date parse error ' " + tableRowMapDO.getValue("startDate") + "'");
        }

        try {
            Date endDate = DateTimeUtil.convertUTCStringToDate(tableRowMapDO.getValue("endDate"));
            contractDO.setEndDate(endDate);
        }catch (ParseException e){
            logger.error("End date parse error ' " + tableRowMapDO.getValue("endDate") + "'");
        }


        if(tableRowMapDO.getValue("approvedRawMaterialQuantity") != null) {
            contractDO.setApprovedRawMaterialQuantity(Double.parseDouble(tableRowMapDO.getValue("approvedRawMaterialQuantity")));
        }

        if(tableRowMapDO.getValue("receivedRawMaterialQuantity") != null) {
            contractDO.setReceivedRawMaterialQuantity(Double.parseDouble(tableRowMapDO.getValue("receivedRawMaterialQuantity")));
        }


        if(tableRowMapDO.getValue("productDeliveryQuantity") != null) {
            contractDO.setProductDeliveryQuantity(Double.parseDouble(tableRowMapDO.getValue("productDeliveryQuantity")));
        }

        if(tableRowMapDO.getValue("productDeliveredQuantity") != null) {
            contractDO.setProductDeliveredQuantity(Double.parseDouble(tableRowMapDO.getValue("productDeliveredQuantity")));
        }

        contractDO.setStatus(tableRowMapDO.getValue("status"));
        contractDO.setRemarks(tableRowMapDO.getValue("remarks"));

        return contractDO;
    }

    @Override
    public FontIcon getFontIcon() {
        return AppFontIcons.CONTRACTMANAGEMENT;
    }

    @Override
    public String getTitle() {
        return "Contract Maintenance";
    }

    @Override
    public Boolean hasSearchActions() {
        return false;
    }

}

