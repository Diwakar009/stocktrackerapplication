/*
 * Copyright (c) 2020.
 *
 * All Rights are with Diwakar C
 */

package com.desktopapp.exceptions;

import com.stocktracker.exceptions.BusinessException;

public class NoOpenContractAvailableException extends BusinessException {

    public NoOpenContractAvailableException(String s) {
        super(s);
    }

    public NoOpenContractAvailableException() {
        super("No open contract available for selected industry");
    }

    @Override
    public String toString() {
        return "No open contract available for selected industry";
    }
}

