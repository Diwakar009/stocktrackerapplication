package com.stocktracker.jpa.spring.extended;

import com.stocktracker.jpa.spring.extended.custom.ExtendedRepository;
import com.stocktracker.model.AppUserLogin;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface AppUserLoginRepository extends ExtendedRepository<AppUserLogin, Long> {
	
	@Query(value="SELECT d FROM AppUserLogin d WHERE d.appUser.id = :appUserId")
	public Page<AppUserLogin> findByAppUserId(@Param("appUserId") Long appUserId
            , Pageable pagable);

	@Query(value="SELECT d FROM AppUserLogin d WHERE d.appUser.id = :appUserId and d.userName LIKE :userName ORDER BY d.userName")
	public Page<AppUserLogin> findByAppUserLoginLikeAll(@Param("appUserId") Long appUserId, @Param("userName") String userName
            , Pageable pagable);
	 
	@Query(value="SELECT d FROM AppUserLogin d WHERE d.userName=:userName")
	public List<AppUserLogin> findByAppUserLogin(@Param("userName") String userName);

	
}
