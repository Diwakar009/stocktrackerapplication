/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.data;

import com.stocktracker.jpa.spring.extended.IndustryRepository;
import com.stocktracker.jpa.spring.extended.config.ExtendedRepositoryConfig;
import com.stocktracker.model.sms.Industry;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by diwakar009 on 17/12/2018.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = { ExtendedRepositoryConfig.class })
public class IndustryStandaloneRepositoryTest {

    @Resource
    private IndustryRepository industryRepository;

    @Before
    public void setup() {

    }

    @Test
    public void findAllTest(){
        List<Industry> industries = industryRepository.findAll();
        for(Industry industry : industries){
            System.out.println(industry.toString());
        }
    }

    //@Test
    public void saveStaticDataSetup(){
        List<Industry> industryList = new ArrayList<>();
        Industry industry = new Industry();
        industry.setIndustryCode("KMR");
        industry.setIndustryStatus("OP");
        industry.setIndustryType("RM");
        industry.setIndustryOwner("R Padmavati");
        industryList.add(industry);

        industry = new Industry();
        industry.setIndustryCode("SMR");
        industry.setIndustryStatus("OP");
        industry.setIndustryType("RM");
        industry.setIndustryOwner("G Banajalata Choudhury");
        industryList.add(industry);

        industry = new Industry();
        industry.setIndustryCode("PBSMR");
        industry.setIndustryStatus("CL");
        industry.setIndustryType("RM");
        industry.setIndustryOwner("G Banajalata Choudhury");
        industryList.add(industry);

        industryRepository.saveAll(industryList);
    }



}
