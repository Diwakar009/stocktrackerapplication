package com.stocktracker.ws.web.api;

import com.stocktracker.domainobjs.json.DocContentDO;
import com.stocktracker.domainobjs.json.DomainObjectList;
import com.stocktracker.domainobjs.json.TableRowMapDO;
import com.stocktracker.util.Base64Util;
import com.stocktracker.util.CommonFileUtil;
import com.stocktracker.util.DateTimeUtil;
import com.stocktracker.util.JSONUtil;
import com.stocktracker.ws.BaseUnitTest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Unit tests for the GreetingController using Spring MVC Mocks.
 * 
 * These tests utilize the Spring MVC Mock objects to simulate sending actual
 * HTTP requests to the Controller component. This test ensures that the
 * RequestMappings are configured correctly. Also, these tests ensure that the
 * request and response bodies are serialized as expected.
 * 
 * @author Matt Warman
 */
@Transactional
public class DocumentContentControllerTest extends BaseUnitTest {


    @Test
    public void testGet() throws Exception{

        String uri = "/api/documentContent/get/";

        DocContentDO docContentDO = new DocContentDO();
        docContentDO.setId(new Long(10));


        String jsonString = JSONUtil.serializeToJSON(docContentDO);

        MvcResult result = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON).content(jsonString)).andReturn();

        String content = result.getResponse().getContentAsString();
        int status = result.getResponse().getStatus();
        Gson gson = new GsonBuilder().setDateFormat(DateTimeUtil.UTC_FORMAT).setPrettyPrinting().create();
        JsonParser jp = new JsonParser();
        JsonElement je = jp.parse(content);
        String prettyJsonString = gson.toJson(je);

       // Assert.assertEquals("failure - expected HTTP status", 200, status);

        displayMessage(prettyJsonString);

        DocContentDO sd =  (DocContentDO)JSONUtil.deserializeToObject(content,DocContentDO.class);

        DocContentDO stdo = gson.fromJson(je,DocContentDO.class);

        //Assert.assertEquals("Document Content Name :", "Personal details", stdo.getDocContentName());

    }

    @Test
    public void testDownloadDocument() throws Exception{

        String uri = "/api/documentContent/downloadDocument/";

        DocContentDO docContentDO = new DocContentDO();
        docContentDO.setId(new Long(10));


        String jsonString = JSONUtil.serializeToJSON(docContentDO);

        MvcResult result = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON).content(jsonString)).andReturn();

        String content = result.getResponse().getContentAsString();
        int status = result.getResponse().getStatus();
        /*
        Gson gson = new GsonBuilder().setDateFormat(DateTimeUtil.UTC_FORMAT).setPrettyPrinting().create();
        JsonParser jp = new JsonParser();
        JsonElement je = jp.parse(content);
        String prettyJsonString = gson.toJson(je);

        Assert.assertEquals("failure - expected HTTP status", 200, status);

        displayMessage(prettyJsonString);

        DocContentDO sd =  (DocContentDO)JSONUtil.deserializeToObject(content,DocContentDO.class);

        DocContentDO stdo = gson.fromJson(je,DocContentDO.class);

        Assert.assertEquals("Document Content Name :", "Personal details", stdo.getDocContentName());
        */

    }

    //@Test
    public void testUpdate() throws  Exception{


        String uri = "/api/documentContent/get/";

        DocContentDO docContentDO = new DocContentDO();
        docContentDO.setId(new Long(2));

        String jsonString = JSONUtil.serializeToJSON(docContentDO);

        MvcResult result = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON).content(jsonString)).andReturn();

        String content = result.getResponse().getContentAsString();
        int status = result.getResponse().getStatus();
        Gson gson = new GsonBuilder().setDateFormat(DateTimeUtil.UTC_FORMAT).setPrettyPrinting().create();
        JsonParser jp = new JsonParser();
        JsonElement je = jp.parse(content);
        String prettyJsonString = gson.toJson(je);

        Assert.assertEquals("failure - expected HTTP status", 200, status);

        displayMessage(prettyJsonString);

        DocContentDO sd =  (DocContentDO)JSONUtil.deserializeToObject(content,DocContentDO.class);

        DocContentDO stdo = gson.fromJson(je,DocContentDO.class);

        stdo.setDocContentName("SamplePage");

        uri = "/api/documentContent/save/";

        jsonString = JSONUtil.serializeToJSON(stdo);

        result = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON).content(jsonString)).andReturn();

        content = result.getResponse().getContentAsString();
        status = result.getResponse().getStatus();
        gson = new GsonBuilder().setDateFormat(DateTimeUtil.UTC_FORMAT).setPrettyPrinting().create();
        jp = new JsonParser();
        je = jp.parse(content);
        prettyJsonString = gson.toJson(je);

        //Assert.assertEquals("failure - expected HTTP status", 201, status);

        displayMessage(content);

    }



    @Test
    public void testSave() throws Exception{

        String uri = "/api/documentContent/save/";

        DocContentDO contentDO = new DocContentDO();
        contentDO.setDocContentName("SampleName");
        contentDO.setDocContentKeywords("Sample Keyword");
        //contentDO.setDocDirPath(Base64Util.encodeString("D:\\\\Diwakar\\\\Documents\\\\Documents\\\\UOB CC Documents\\\\Diwakar New Passport"));
        contentDO.setDocDirPath(("D:\\\\Diwakar\\\\Documents\\\\Documents\\\\UOB CC Documents\\\\Diwakar New Passport"));
        contentDO.setDocFileName("SampleFileName.txt");
        contentDO.setDocFileExtension("txt");
        contentDO.setDocHolderId(2l);

        String jsonString = JSONUtil.serializeToJSON(contentDO);

        MvcResult result = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON).content(jsonString)).andReturn();

        String content = result.getResponse().getContentAsString();
        int status = result.getResponse().getStatus();
        Gson gson = new GsonBuilder().setDateFormat(DateTimeUtil.UTC_FORMAT).setPrettyPrinting().create();
        JsonParser jp = new JsonParser();
        JsonElement je = jp.parse(content);
        String prettyJsonString = gson.toJson(je);

        //Assert.assertEquals("failure - expected HTTP status", 201, status);

        displayMessage(content);

        DocContentDO sd =  (DocContentDO)JSONUtil.deserializeToObject(content,DocContentDO.class);

        DocContentDO stdo = gson.fromJson(je,DocContentDO.class);


        uri = "/api/documentContent/get/";


        jsonString = JSONUtil.serializeToJSON(stdo);

        result = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON).content(jsonString)).andReturn();

        content = result.getResponse().getContentAsString();
        status = result.getResponse().getStatus();
        gson = new GsonBuilder().setPrettyPrinting().create();
        jp = new JsonParser();
        je = jp.parse(content);
        prettyJsonString = gson.toJson(je);

        //Assert.assertEquals("failure - expected HTTP status", 200, status);

        displayMessage(content);



    }

    @Test
    public void testDelete() throws Exception{
        String uri = "/api/documentContent/delete/";

        DocContentDO docContentDo = new DocContentDO();
        docContentDo.setId(new Long(2));

        String jsonString = JSONUtil.serializeToJSON(docContentDo);

        MvcResult result = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON).content(jsonString)).andReturn();

        String content = result.getResponse().getContentAsString();
        int status = result.getResponse().getStatus();
        Gson gson = new GsonBuilder().setDateFormat(DateTimeUtil.UTC_FORMAT).setPrettyPrinting().create();
        JsonParser jp = new JsonParser();
        JsonElement je = jp.parse(content);
        String prettyJsonString = gson.toJson(je);

        //Assert.assertEquals("failure - expected HTTP status", 200, status);

        displayMessage(content);

    }

    @Test
    public void testDeleteList() throws Exception{
        String uri = "/api/documentContent/deleteList/";

        DocContentDO docContentDo1 = new DocContentDO();
        docContentDo1.setId(new Long(2));

        DocContentDO docContentDo2 = new DocContentDO();
        docContentDo2.setId(new Long(3));

        List<DocContentDO> docContentDOArrayList = new ArrayList<DocContentDO>();
        docContentDOArrayList.add(docContentDo1);
        docContentDOArrayList.add(docContentDo2);


        DomainObjectList objectList = new DomainObjectList();
        objectList.setList(docContentDOArrayList);

        //staticCodeDecodeDO.setCodeDesc("Sample Access Level");

        String jsonString = JSONUtil.serializeToJSON(objectList);

        MvcResult result = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON).content(jsonString)).andReturn();

        String content = result.getResponse().getContentAsString();
        int status = result.getResponse().getStatus();
        Gson gson = new GsonBuilder().setDateFormat(DateTimeUtil.UTC_FORMAT).setPrettyPrinting().create();
        JsonParser jp = new JsonParser();
        JsonElement je = jp.parse(content);
        String prettyJsonString = gson.toJson(je);

        //Assert.assertEquals("failure - expected HTTP status", 200, status);

        displayMessage(content);

    }
    @Test
    public void testListAll() throws Exception{

        String uri = "/api/documentContent/listWithFilter/";

        TableRowMapDO<String, String> rowMap = new TableRowMapDO<String, String>();
        rowMap.setSize("10");
        rowMap.setPage("1");
        rowMap.addKeyValue("docHolderId","1");
        rowMap.addKeyValue("docContentName","%");
        rowMap.addKeyValue("docContentKeywords","%");



        String jsonString = JSONUtil.serializeToJSON(rowMap);

/*        MvcResult result = mvc.perform(MockMvcRequestBuilders.get(uri)
                .accept(MediaType.APPLICATION_JSON)).andReturn();*/

        MvcResult result = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON).content(jsonString)).andReturn();

        String content = result.getResponse().getContentAsString();
        int status = result.getResponse().getStatus();
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        JsonParser jp = new JsonParser();
        JsonElement je = jp.parse(content);
        String prettyJsonString = gson.toJson(je);

        //Assert.assertEquals("failure - expected HTTP status", 200, status);

        displayMessage(content);

    }
/*

    @Test
    public void testListAllTableRowObjectConstruction() throws Exception{

        String uri = "/api/appUser/listAll/";

        TableRowMapDO<String, String> rowMap = new TableRowMapDO<String, String>();
        rowMap.setSize("2");
        rowMap.setPage("1");

        String jsonString = JSONUtil.serializeToJSON(rowMap);

        MvcResult result = mvc.perform(MockMvcRequestBuilders.get(uri)
                .accept(MediaType.APPLICATION_JSON)).andReturn();

        String content = result.getResponse().getContentAsString();
        int status = result.getResponse().getStatus();
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        JsonParser jp = new JsonParser();
        JsonElement je = jp.parse(content);
        String prettyJsonString = gson.toJson(je);

        Assert.assertEquals("failure - expected HTTP status", 200, status);

        displayMessage(content);
        TableDOPage page =  (TableDOPage)JSONUtil.deserializeToObject(content,TableDOPage.class);
        List<TableRowMapDO<String,String>> rows = page.getList();

        for(TableRowMapDO<String,String> row : rows){

            String clazzName = row.getClazzName();
            Class<?> clazz = Class.forName(clazzName);
            Constructor<?> ctor = clazz.getConstructor();
            Object object = ctor.newInstance(new Object[] {});

            AppUserDO userDO = new AppUserDO();
            userDO.setFirstName(row.getValue("firstName"));
            userDO.setLastName(row.getValue("lastName"));
            userDO.setShortName(row.getValue("shortName"));
            userDO.setMobile(row.getValue("mobile"));
            userDO.setEmail(row.getValue("email"));
            userDO.setActive(new Boolean(row.getValue("active")));
            userDO.setId(new Long(row.getValue("id")));

            displayMessage(userDO.getId().toString());
        }
    }
    */

    @Test
    public void testListWithFilter() throws Exception {

        String uri = "/api/documentContent/listWithFilter/";

        TableRowMapDO<String, String> rowMap = new TableRowMapDO<String, String>();
        rowMap.setSize("10");
        rowMap.setPage("1");
        rowMap.addKeyValue("docHolderId","1");
        rowMap.addKeyValue("docContentName","%Pass%");
        rowMap.addKeyValue("docContentKeywords","%Pass%");

        String jsonString = JSONUtil.serializeToJSON(rowMap);

        MvcResult result = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON).content(jsonString)).andReturn();

        String content = result.getResponse().getContentAsString();
        int status = result.getResponse().getStatus();
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        JsonParser jp = new JsonParser();
        JsonElement je = jp.parse(content);
        String prettyJsonString = gson.toJson(je);



        Assert.assertEquals("failure - expected HTTP status", 200, status);

        displayMessage(prettyJsonString);
    }


}
