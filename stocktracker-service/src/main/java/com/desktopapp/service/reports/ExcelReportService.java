/*
 * Copyright (c) 2020.
 *
 * All Rights are with Diwakar C
 */

package com.desktopapp.service.reports;
import com.stocktracker.domainobjs.json.sms.IndustryOutturnReportDO;
import com.stocktracker.domainobjs.json.sms.StockDetailsDO;
import com.stocktracker.exceptions.ExcelCellRenderingException;
import com.desktopapp.service.StaticCodeDecodeService;
import com.desktopapp.exceptions.InvalidTemplateException;
import com.desktopapp.exceptions.NoTemplateFoundException;
import com.desktopapp.service.stockmaintenance.StockDetailsService;
import com.stocktracker.excel.ExcelUtils;
import com.stocktracker.domainobjs.json.CodeDataItemDO;
import com.stocktracker.domainobjs.json.sms.MandiReconcilationDataDO;
import com.stocktracker.exceptions.BusinessException;
import com.stocktracker.exceptions.TechnicalException;
import com.stocktracker.model.BaseEntity;
import com.stocktracker.util.CommonFileUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import java.io.*;
import java.sql.Array;
import java.util.*;
import java.util.stream.Collectors;

import static com.stocktracker.component.constants.CodeDecodeConstants.CD_STOCK_CODE;
import static com.stocktracker.component.constants.CodeDecodeConstants.CD_STOCK_VARIETY;

@Component
public class ExcelReportService implements ReportService{

    private static final Logger logger = LoggerFactory.getLogger(ExcelReportService.class);

    @Autowired
    private StockDetailsService stockDetailsService;

    @Autowired
    private StaticCodeDecodeService staticCodeDecodeService;

    private Map<CodeDataItemDO,String> codeDescriptionCache = new HashMap<>();

    public static final String DEFAULT_LANGUAGE = "EN_US";
    public static final String CD_INDUSTRY_CODE = "CD_INDUSTRY_CODE";
    public static final String CD_LAMP_CODE = "CD_LAMP_CODE";
    public static final String CD_MANDI_NAME = "CD_MANDI_NAME";
    public static final String CD_TRANSACTION_TYPE = "CD_TRANS_TYPE";
    public static final List<String> VALID_EXT_LIST = Arrays.asList("xls","xlsx");


    public Workbook readExcelTemplate(String templateFileName) throws NoTemplateFoundException, InvalidTemplateException {
        logger.info("Method readExcelTemplate Started");
        String fileExtension = CommonFileUtil.getFileExtension(templateFileName);
        if(!VALID_EXT_LIST.contains(fileExtension)){
            throw new InvalidTemplateException("Invalid File Exception -" +  fileExtension);
        }

        File file = new File(templateFileName);
        if(file.exists()){
        try {
                return WorkbookFactory.create(file);
            } catch (IOException e) {
                throw new InvalidTemplateException("IO Exception -" + e.getMessage());
            }
        }
        throw new NoTemplateFoundException("Excel Template Not Found");
    }

    public OutputStream reteriveStockDetailTransactionsReport(Map<String, Object> parameters)  throws BusinessException ,TechnicalException{
        try {

            StockDetailTransactionsExcelTemplate template = new StockDetailTransactionsExcelTemplate(readExcelTemplate(StockDetailTransactionsExcelTemplate.TEMPLATE_FILE_NAME));
            Workbook workbook = template.getWorkbook();
            Page<? extends BaseEntity> pagedData = stockDetailsService.findByFilter(parameters, null);
            List<StockDetailsDO> stockDetailsDOList = stockDetailsService.returnDomainObjects(pagedData.getContent());

            int i = 1;
            Sheet sheet = workbook.cloneSheet(0);
            workbook.setSheetName(i, "Stock Transactions");
            Integer row = StockDetailTransactionsExcelTemplate.INDEX_OF_DATA_ROW;
            Integer col = 0;

            Map<Integer, CellStyle> styleMap = template.getCellStyleMap();
            for(StockDetailsDO detailsDO : stockDetailsDOList){
                col = 0;
                Row currentRow = sheet.getRow(row);

                if(currentRow == null){
                    currentRow = sheet.createRow(row);
                }
                ExcelUtils.setCellValue(currentRow, col++, detailsDO.getTransactionDate(), styleMap.get(0));
                //ExcelUtils.setCellValue(currentRow, col++, detailsDO.getIndustryCode() != null ? getCodeDescription(CD_INDUSTRY_CODE, detailsDO.getIndustryCode(), DEFAULT_LANGUAGE) : "", styleMap.get(1));
                ExcelUtils.setCellValue(currentRow, col++, detailsDO.getIndustryCode(), styleMap.get(1));
                ExcelUtils.setCellValue(currentRow, col++, detailsDO.getStockCode() != null ? getCodeDescription(CD_STOCK_CODE, detailsDO.getStockCode(), DEFAULT_LANGUAGE) : "", styleMap.get(2));
                ExcelUtils.setCellValue(currentRow, col++, detailsDO.getStockVarietyCode() != null ? getCodeDescription(CD_STOCK_VARIETY, detailsDO.getStockVarietyCode(), DEFAULT_LANGUAGE) : "", styleMap.get(3));
                ExcelUtils.setCellValue(currentRow, col++, detailsDO.getTransactionType() != null ? getCodeDescription(CD_TRANSACTION_TYPE, detailsDO.getTransactionType(), DEFAULT_LANGUAGE) : "", styleMap.get(4));
                ExcelUtils.setCellValue(currentRow, col++, detailsDO.getNoOfPackets(), styleMap.get(5));
                ExcelUtils.setCellValue(currentRow, col++, detailsDO.getTotalWeightment(), styleMap.get(6));

                if(detailsDO.getCostPriceDetailsDO() != null) {
                    ExcelUtils.setCellValue(currentRow, col++, detailsDO.getCostPriceDetailsDO().getAppliedRate(), styleMap.get(7));
                }else {
                    col++;
                }

                if(detailsDO.getCostPriceDetailsDO() != null) {
                    ExcelUtils.setCellValue(currentRow, col++, detailsDO.getCostPriceDetailsDO().getActualAmount(), styleMap.get(8));
                }else {
                    col++;
                }

                ExcelUtils.setCellValue(currentRow, col++, detailsDO.getLampNameCode() != null ? getCodeDescription(CD_LAMP_CODE, detailsDO.getLampNameCode(), DEFAULT_LANGUAGE) : "", styleMap.get(9));
                ExcelUtils.setCellValue(currentRow, col++, detailsDO.getMandiNameCode() != null ? getCodeDescription(CD_MANDI_NAME, detailsDO.getMandiNameCode(), DEFAULT_LANGUAGE) : "", styleMap.get(10));

                if(detailsDO.getPartyDetailsDO() != null) {
                    ExcelUtils.setCellValue(currentRow, col++, detailsDO.getPartyDetailsDO().getPartyName1(), styleMap.get(11));
                }else{
                    col++;
                }

                ExcelUtils.setCellValue(currentRow, col++, detailsDO.getLampEntryInQtl(), styleMap.get(12));
                ExcelUtils.setCellValue(currentRow, col++, detailsDO.getLampEntryAfterAdjInQtl(), styleMap.get(13));

                if(detailsDO.getTransportDetailsDO() != null) {
                    ExcelUtils.setCellValue(currentRow, col++, detailsDO.getTransportDetailsDO().getRstNumber(), styleMap.get(14));
                }else{
                    col++;
                }

                if(detailsDO.getTransportDetailsDO() != null) {
                    ExcelUtils.setCellValue(currentRow, col++, detailsDO.getTransportDetailsDO().getVehicleNumber(), styleMap.get(15));
                }else{
                    col++;
                }
                //ExcelUtils.setCellValue(currentRow, col++, detailsDO.getOnBeHalf() != null ? getCodeDescription(CD_INDUSTRY_CODE, detailsDO.getOnBeHalf(), DEFAULT_LANGUAGE) : "", styleMap.get(15));
                ExcelUtils.setCellValue(currentRow, col++, detailsDO.getOnBeHalf(), styleMap.get(16));
                if(detailsDO.getDeliveryDetailsDO() != null) {
                    ExcelUtils.setCellValue(currentRow, col++, detailsDO.getDeliveryDetailsDO().getDeliveryLatNo(), styleMap.get(17));
                }else{
                    col++;
                }
                ExcelUtils.setCellValue(currentRow, col++, detailsDO.getRemarks(), styleMap.get(18));

                row = row + 1;
            }
            ExcelUtils.setAutoFilter(sheet,StockDetailTransactionsExcelTemplate.INDEX_OF_DATA_ROW - 1, row - 1 , 0, StockDetailTransactionsExcelTemplate.NO_OF_COLUMNS-1);
            template.applySummaryRow(sheet,row);

            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            try {
                if (workbook.getSheet("Template") != null) {
                    workbook.removeSheetAt(0);
                }
                workbook.write(outputStream);
            } catch (IOException e) {
                throw new TechnicalException(e.getMessage());
            }

            return outputStream;
        }catch (NoTemplateFoundException | InvalidTemplateException | ExcelCellRenderingException e){
            throw e;
        }catch (Exception ex){
            throw new TechnicalException(ex.getMessage());
        }
    }

    @Override
    public OutputStream reteriveMandiReconReport(String industryCode) throws BusinessException ,TechnicalException{
        return reteriveMandiReconReport(industryCode,null,null);
    }

    @Override
    public OutputStream reteriveMandiReconReport(String industryCode, String lampName, String mandiName) throws BusinessException,TechnicalException {

        try {
            MandiReconExcelReportTemplate template = new MandiReconExcelReportTemplate(readExcelTemplate(MandiReconExcelReportTemplate.MANDI_RECON_TEMPLATE));
            Workbook workbook = template.getWorkbook();
            List<CodeDataItemDO> industryCodeItemList = new ArrayList<>();
            Optional<List<CodeDataItemDO>> codeDataItemList = staticCodeDecodeService.getCodeDataItemList(CD_INDUSTRY_CODE, DEFAULT_LANGUAGE, true);
            if (industryCode == null || industryCode.isEmpty()) {
                if (codeDataItemList.isPresent()) {
                    industryCodeItemList = codeDataItemList.get();
                }
            } else {
                if (codeDataItemList.isPresent()) {
                    industryCodeItemList = codeDataItemList.get().stream().filter(codeDataItemDO -> industryCode.equals(codeDataItemDO.getCodeValue())).collect(Collectors.toList());
                }
            }

            int i = 1;

            for (CodeDataItemDO industryCodeItem : industryCodeItemList) {

                List<MandiReconcilationDataDO> reconciliationDataDOList = stockDetailsService.getMandiReconciliationData(industryCodeItem.getCodeValue(), lampName, mandiName);
                Sheet sheet = null;
                if ((reconciliationDataDOList != null && !reconciliationDataDOList.isEmpty()) || industryCodeItemList.size() == 1) {
                    sheet = workbook.cloneSheet(0);
                    workbook.setSheetName(i++, industryCodeItem.getCodeDescription());
                    String cellValue = sheet.getRow(0).getCell(0).getStringCellValue();
                    sheet.getRow(0).getCell(0).setCellValue(cellValue + " " + industryCodeItem.getCodeDescription());
                } else {
                    continue;
                }

                Integer row = MandiReconExcelReportTemplate.INDEX_OF_DATA_ROW;
                Integer col = 0;

                Map<Integer, CellStyle> styleMap = null;

                for (MandiReconcilationDataDO reconcilationDataDO : reconciliationDataDOList) {
                    boolean differenceIndicator = false;
                    col = 0;
                    Row currentRow = sheet.getRow(row);

                    if(currentRow == null){
                        currentRow = sheet.createRow(row);
                    }

                    if(reconcilationDataDO.getId() == null){
                        if(StringUtils.isEmpty(reconcilationDataDO.getTransactionType())){
                            if(reconcilationDataDO.getTransactionDate() != null
                                    && !StringUtils.isEmpty(reconcilationDataDO.getOnBehalf())
                                    && !StringUtils.isEmpty(reconcilationDataDO.getLampNameCode())
                                    && !StringUtils.isEmpty(reconcilationDataDO.getMandiNameCode())
                                    && !StringUtils.isEmpty(reconcilationDataDO.getPartyName())){
                                    differenceIndicator = true;
                                    styleMap = template.getGreenHighlightedCellStyleMap();
                            }else {
                                    styleMap = template.getHighlightedCellStyleMap();
                            }
                        }else{
                            styleMap = template.getHighlightedCellStyleMap();
                        }
                    }else{
                        if ( reconcilationDataDO.getTransactionDate() == null
                                || StringUtils.isEmpty(reconcilationDataDO.getOnBehalf())
                                || StringUtils.isEmpty(reconcilationDataDO.getLampNameCode())
                                || StringUtils.isEmpty(reconcilationDataDO.getMandiNameCode())
                                || StringUtils.isEmpty(reconcilationDataDO.getTransactionType())
                                || StringUtils.isEmpty(reconcilationDataDO.getPartyName())){
                            styleMap = template.getWarningCellStyleMap();
                        }else{
                            styleMap = template.getCellStyleMap();
                        }
                    }

                    ExcelUtils.setCellValue(currentRow, col++, reconcilationDataDO.getIndustryCode(), styleMap.get(0));
                    ExcelUtils.setCellValue(currentRow, col++, reconcilationDataDO.getTransactionDate(), styleMap.get(1));
                    ExcelUtils.setCellValue(currentRow, col++, reconcilationDataDO.getOnBehalf(), styleMap.get(2));
                    ExcelUtils.setCellValue(currentRow, col++, reconcilationDataDO.getLampNameCode() != null ? getCodeDescription(CD_LAMP_CODE, reconcilationDataDO.getLampNameCode(), DEFAULT_LANGUAGE) : "", styleMap.get(3));
                    ExcelUtils.setCellValue(currentRow, col++, reconcilationDataDO.getMandiNameCode() != null ? getCodeDescription(CD_MANDI_NAME, reconcilationDataDO.getMandiNameCode(), DEFAULT_LANGUAGE) : "", styleMap.get(4));
                    ExcelUtils.setCellValue(currentRow, col++, reconcilationDataDO.getPartyName(), styleMap.get(5));
                    ExcelUtils.setCellValue(currentRow, col++, reconcilationDataDO.getTransactionType() != null ? getCodeDescription(CD_TRANSACTION_TYPE, reconcilationDataDO.getTransactionType(), DEFAULT_LANGUAGE) : "", styleMap.get(6));
                    ExcelUtils.setCellValue(currentRow, col++, reconcilationDataDO.getId(), styleMap.get(7));
                    ExcelUtils.setCellValue(currentRow, col++, reconcilationDataDO.getVehicleNumber(), styleMap.get(8));
                    ExcelUtils.setCellValue(currentRow, col++, reconcilationDataDO.getRstNumber(), styleMap.get(9));
                    ExcelUtils.setCellValue(currentRow, col++, reconcilationDataDO.getTotalReceived() != null ? reconcilationDataDO.getTotalReceived().doubleValue() : 0.0d, styleMap.get(10));
                    ExcelUtils.setCellValue(currentRow, col++, reconcilationDataDO.getTotalAdjusted() != null ? reconcilationDataDO.getTotalAdjusted().doubleValue() : 0.0d, styleMap.get(11));
                    ExcelUtils.setCellValue(currentRow, col++, reconcilationDataDO.getTotalExcessPurchased() != null ? reconcilationDataDO.getTotalExcessPurchased().doubleValue() : 0.0d, styleMap.get(12));
                    ExcelUtils.setCellValue(currentRow, col++, reconcilationDataDO.getTotalReceivedNAdjustedNPurchased() != null ? reconcilationDataDO.getTotalReceivedNAdjustedNPurchased().doubleValue() : 0.0d, styleMap.get(13));
                    ExcelUtils.setCellValue(currentRow, col++, reconcilationDataDO.getTotalEntered() != null ? reconcilationDataDO.getTotalEntered().doubleValue() : 0.0d, styleMap.get(14));
                    ExcelUtils.setCellValue(currentRow, col++, reconcilationDataDO.getLampEntryAdjustment() != null ? reconcilationDataDO.getLampEntryAdjustment().doubleValue() : 0.0d, styleMap.get(15));
                    ExcelUtils.setCellValue(currentRow, col++, reconcilationDataDO.getTotalLampEntryAfterAdjustment() != null ? reconcilationDataDO.getTotalLampEntryAfterAdjustment().doubleValue() : 0.0d, styleMap.get(16));

                    if(differenceIndicator){
                          ExcelUtils.setCellValue(currentRow, col++, reconcilationDataDO.getDifference() != null ? reconcilationDataDO.getDifference().doubleValue() : 0.0d, styleMap.get(17));
                    }else{
                        ExcelUtils.setCellValue(currentRow, col++, null, styleMap.get(17));
                    }

                    row = row + 1;

                }
                ExcelUtils.setAutoFilter(sheet,MandiReconExcelReportTemplate.INDEX_OF_DATA_ROW - 1, row - 1 , 0, MandiReconExcelReportTemplate.NO_OF_COLUMNS-1);
                template.applySummaryRow(sheet,row);
            }
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            try {
                if (workbook.getSheet("Template") != null) {
                    workbook.removeSheetAt(0);
                }
                workbook.write(outputStream);
            } catch (IOException e) {
                throw new TechnicalException(e.getMessage());
            }

            return outputStream;
        }catch (NoTemplateFoundException | InvalidTemplateException | ExcelCellRenderingException e){
            throw e;
        }catch (Exception ex){
            throw new TechnicalException(ex.getMessage());
        }
    }

    @Override
    public OutputStream reteriveIndustryOutturnReport(String industryCode) throws BusinessException, TechnicalException {
        try {
                IndustryOutturnReportTemplate template = new IndustryOutturnReportTemplate(readExcelTemplate(IndustryOutturnReportTemplate.INDUSTRY_OUTTURN_TEMPLATE));
                Workbook workbook = template.getWorkbook();
                Sheet sheet = workbook.cloneSheet(0);
                Map<Integer, CellStyle> styleMap = template.getCellStyleMap();
                Row currentRow = sheet.getRow(2);
                // Industry Code --> Stock Code --> IndustryOutturnReportDO
                Map<String,IndustryOutturnReportDO>  dataMap = stockDetailsService.getIndustryOutturnReportData();

                int row = 1;
                int col = 1;
                int column = 0;

                for(Map.Entry<String, IndustryOutturnReportDO> es:dataMap.entrySet()){
                    String industryCode_1 = es.getKey();
                    IndustryOutturnReportDO  outturnReportDO = es.getValue();
                    row = 1;
                    Character COL_NAME = IndustryOutturnReportTemplate.COLUMNS[column];
                    try {
                            currentRow = sheet.getRow(row++);
                            ExcelUtils.setCellValue(currentRow, col, industryCode_1);
                            currentRow = sheet.getRow(row++);
                            ExcelUtils.setCellValue(currentRow, col, outturnReportDO.getTotalPaddyOfferInPkt());
                            currentRow = sheet.getRow(row++);
                            ExcelUtils.setCellValue(currentRow, col, outturnReportDO.getTotalPaddyOfferInWmt());
                            currentRow = sheet.getRow(row++);
                            ExcelUtils.setCellFormula(currentRow, col, COL_NAME + "4*67%");
                            row++;
                            currentRow = sheet.getRow(row++);
                            ExcelUtils.setCellValue(currentRow, col, outturnReportDO.getTotalRiceProducedInPkt());
                            currentRow = sheet.getRow(row++);
                            ExcelUtils.setCellValue(currentRow, col, outturnReportDO.getTotalBrokenRiceProducedInPkt());
                            currentRow = sheet.getRow(row++);
                            ExcelUtils.setCellValue(currentRow, col, outturnReportDO.getTotalBranBrokenProducedInPkt());
                            currentRow = sheet.getRow(row++);
                            ExcelUtils.setCellValue(currentRow, col, outturnReportDO.getTotalBranProducedInPkt());
                            row++;
                            row++;
                            row++;
                            currentRow = sheet.getRow(row++);
                            ExcelUtils.setCellValue(currentRow, col, outturnReportDO.getTotalRiceProducedInWmt());
                            currentRow = sheet.getRow(row++);
                            ExcelUtils.setCellValue(currentRow, col, outturnReportDO.getTotalBrokenRiceProducedInWmt());
                            currentRow = sheet.getRow(row++);
                            ExcelUtils.setCellValue(currentRow, col, outturnReportDO.getTotalBranBrokenProducedInWmt());
                            currentRow = sheet.getRow(row++);
                            ExcelUtils.setCellValue(currentRow, col, outturnReportDO.getTotalBranProducedInWmt());

                            currentRow = sheet.getRow(row++);
                            ExcelUtils.setCellFormula(currentRow, col, COL_NAME + "14" + "/" + COL_NAME + "4");
                            currentRow = sheet.getRow(row++);
                            ExcelUtils.setCellFormula(currentRow, col, COL_NAME + "15" + "/" + COL_NAME + "4");
                            currentRow = sheet.getRow(row++);
                            ExcelUtils.setCellFormula(currentRow, col, COL_NAME + "16" + "/" + COL_NAME + "4");
                            currentRow = sheet.getRow(row++);
                            ExcelUtils.setCellFormula(currentRow, col, COL_NAME + "17" + "/" + COL_NAME + "4");

                            row++;

                            currentRow = sheet.getRow(row++);
                            ExcelUtils.setCellFormula(currentRow, col, COL_NAME + "18" + "+" + COL_NAME + "19");
                            //B18+B19+B20+B21
                            currentRow = sheet.getRow(row++);
                            ExcelUtils.setCellFormula(currentRow, col, COL_NAME + "18" +
                                    "+" + COL_NAME + "19" +
                                    "+" + COL_NAME + "20" +
                                    "+" + COL_NAME + "21" );

                            row++;
                            currentRow = sheet.getRow(row++);
                            ExcelUtils.setCellValue(currentRow, col, outturnReportDO.getTotalRunningStockInPkt());
                            currentRow = sheet.getRow(row++);
                            ExcelUtils.setCellValue(currentRow, col, outturnReportDO.getTotalRunningStockInWmt());

                            col = col + 2;
                            column++;
                    } catch (ExcelCellRenderingException excelCellRenderingException) {
                        excelCellRenderingException.printStackTrace();
                    }
                }

                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                    try {
                        if (workbook.getSheet("Template") != null) {
                            workbook.removeSheetAt(0);
                        }
                        workbook.write(outputStream);
                    } catch (IOException e) {
                        throw new TechnicalException(e.getMessage());
                    }

                    return outputStream;

        }catch (NoTemplateFoundException | InvalidTemplateException | ExcelCellRenderingException e){
            throw e;
        }catch (Exception ex){
            throw new TechnicalException(ex.getMessage());
        }
    }

    public String getCodeDescription(String codeName,String codeValue,String language){
        CodeDataItemDO codeDataItemDO = new CodeDataItemDO(codeName,codeValue,language);
        if(!codeDescriptionCache.containsKey(codeDataItemDO)) {
            Optional<String> codeDescription = staticCodeDecodeService.getCodeValueDescription(codeName, codeValue, language);
            if (codeDescription.isPresent()) {
                codeDescriptionCache.put(codeDataItemDO, codeDescription.get());
            }else {
                codeDescriptionCache.put(codeDataItemDO, codeValue);
            }
        }
        return codeDescriptionCache.get(codeDataItemDO);
    }
}
