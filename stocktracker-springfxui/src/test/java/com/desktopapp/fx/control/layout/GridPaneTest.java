/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.desktopapp.fx.control.layout;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * Created by diwakar009 on 13/11/2019.
 */
public class GridPaneTest  extends Application {

    @Override
    public void start( final Stage primaryStage )
    {

        ColumnConstraints col1 = new ColumnConstraints();
        col1.setHgrow( Priority.ALWAYS );

        ColumnConstraints col2 = new ColumnConstraints();
        col2.setHgrow( Priority.ALWAYS );

        ColumnConstraints col3 = new ColumnConstraints();
        col3.setHgrow( Priority.ALWAYS );

        ColumnConstraints col4 = new ColumnConstraints();
        col4.setHgrow( Priority.ALWAYS );

        GridPane gridPane = new GridPane();
        gridPane.setGridLinesVisible( true );
        gridPane.getColumnConstraints().addAll( col1,col2,col3,col4 );

        gridPane.addColumn( 0, new Button( "col 1" ) );
        gridPane.addColumn( 1, new Button( "col 2" ) );
        gridPane.addColumn( 2, new Button( "col 3" ) );
        //gridPane.addColumn( 3, new Button( "col 4" ) );

        final Scene scene = new Scene( new VBox( gridPane ), 400, 300 );
        primaryStage.setScene( scene );
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
