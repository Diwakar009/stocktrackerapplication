/*
 * Copyright (c) 2020.
 *
 * All Rights are with Diwakar C
 */

package com.desktopapp.service;

import com.desktopapp.exceptions.NoOpenContractAvailableException;
import com.desktopapp.service.config.AppConfig;
import com.desktopapp.service.stockmaintenance.ContractService;
import com.stocktracker.domainobjs.json.sms.ContractDO;
import com.stocktracker.jpa.spring.extended.config.ExtendedRepositoryConfig;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by diwakar009 on 17/12/2018.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = { AppConfig.class, ExtendedRepositoryConfig.class})
@Transactional
public class ContractServiceTest {
    @Autowired
    private ContractService contractService;

    private List<ContractDO> contractList;

    private List<String> transactionTypeList;

    @Before
    public void setUp(){
        transactionTypeList = new ArrayList<>();
        transactionTypeList.add("MDRE");
        transactionTypeList.add("MDAJ");
        transactionTypeList.add("MDRT");
        transactionTypeList.add("KMDEL");
        transactionTypeList.add("KMRET");
        transactionTypeList.add("MDEN");
    }


    @Test
    public void isContractApplicable_List() {
        for (String tranType : transactionTypeList)
            Assert.assertEquals(true,contractService.isContractApplicable(tranType));
    }

    @Test
    @Rollback(true)
    public void updateOpenContract_RecvRawMat() {
        ContractDO contract = new ContractDO();
        contract.setIndustryCode("KMR1");
        contract.setContractName("KMR_KMSP_2019_2020"); // Auto Generated - Industrycode_ContractType_Year
        contract.setContractTypeCode("KMSP");
        contract.setStartDate(new Date());
        contract.setStatus("OPN");
        contract.setDeliveryProduct("RICE");
        contract.setRawMaterial("PADD");
        contract.setApprovedRawMaterialQuantity(23000.00);
        contractService.create(contract);
        ContractDO contract1 = contractService.updateOpenContract("KMR1","MDRE",100.00);
        Assert.assertEquals(100.00,contract1.getReceivedRawMaterialQuantity().doubleValue(),00);
        contract1 = contractService.updateOpenContract("KMR1","MDRE",200.00);
        Assert.assertEquals(300.00,contract1.getReceivedRawMaterialQuantity().doubleValue(),00);
        contract1 = contractService.updateOpenContract("KMR1","MDAJ",200.00);
        Assert.assertEquals(500.00,contract1.getReceivedRawMaterialQuantity().doubleValue(),00);
        contract1 = contractService.updateOpenContract("KMR1","MDRT",200.00);
        Assert.assertEquals(300.00,contract1.getReceivedRawMaterialQuantity().doubleValue(),00);
    }

    @Test
    @Rollback(true)
    public void updateOpenContract_ProductDel(){
        ContractDO contract = new ContractDO();
        contract.setIndustryCode("KMR1");
        contract.setContractName("KMR_KMSP_2019_2020"); // Auto Generated - Industrycode_ContractType_Year
        contract.setContractTypeCode("KMSP");
        contract.setStartDate(new Date());
        contract.setStatus("OPN");
        contract.setDeliveryProduct("RICE");
        contract.setRawMaterial("PADD");
        contract.setProductDeliveryQuantity(4000.00);
        contractService.create(contract);
        ContractDO contract1 = contractService.updateOpenContract("KMR1","KMDEL",100.00);
        Assert.assertEquals(100.00,contract1.getProductDeliveredQuantity().doubleValue(),00);
        contract1 = contractService.updateOpenContract("KMR1","KMDEL",200.00);
        Assert.assertEquals(300.00,contract1.getProductDeliveredQuantity().doubleValue(),00);
    }

    @Test
    @Rollback(true)
    public void reviseContract() {
        ContractDO contract = new ContractDO();
        contract.setIndustryCode("KMR1");
        contract.setContractName("KMR_KMSP_2019_2020"); // Auto Generated - Industrycode_ContractType_Year
        contract.setContractTypeCode("KMSP");
        contract.setStartDate(new Date());
        contract.setStatus("OPN");
        contract.setDeliveryProduct("RICE");
        contract.setRawMaterial("PADD");
        contract.setApprovedRawMaterialQuantity(23000.00);
        contractService.create(contract);

    }

    @Test
    @Rollback(true)
    public void updateContract_Test() throws NoOpenContractAvailableException {
        ContractDO contractDO = contractService.updateContract(324l,"MDRE",100.00,false);
        Assert.assertEquals(400.00,contractDO.getReceivedRawMaterialQuantity().doubleValue(),00);
    }
}
