package com.desktopapp.fx.view;

import com.desktopapp.fx.DesktopFxApplication;
import com.desktopapp.fx.control.CodeDecodeField;
import com.desktopapp.fx.control.TextFieldExt;
import com.desktopapp.fx.mvc.AbstractFormViewDialog;
import com.desktopapp.fx.mvc.DataPageController;
import com.desktopapp.fx.util.I18n;
import com.desktopapp.fx.util.ViewHelpers;
import com.stocktracker.component.constants.DocStoreConstants;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import org.controlsfx.validation.Validator;
import com.stocktracker.domainobjs.json.StaticCodeDecodeDO;

/**
 * Created by diwakar009 on 3/3/2019.
 */
public class StaticCodeDecodeFormDialog extends AbstractFormViewDialog<StaticCodeDecodeDO> {

    private StaticCodeDecodeDO staticCodeDecodeDO;
    private TextFieldExt tfCodeDesc;
    private TextFieldExt tfCodeName;
    private TextFieldExt tfCodeValueFilter;
    private TextFieldExt tfCodeValue;
    private CodeDecodeField cdLanguage;

    public StaticCodeDecodeFormDialog(DataPageController<StaticCodeDecodeDO> controller, StaticCodeDecodeDO staticCodeDecodeDO) {
        super(DesktopFxApplication.get().getMainStage(), controller);
        this.staticCodeDecodeDO = staticCodeDecodeDO;
        setTitle(getFormTitle());
    }

    @Override
    public String getFormTitle() {
        return staticCodeDecodeDO.isNew() ? I18n.STATICCODEDECODE.getString("form.newTitle") : I18n.STATICCODEDECODE.getString("form.editTitle");
    }

    @Override
    public Node buildFormView() {

        tfCodeName = ViewHelpers.createTextFieldExt(30, 200);
        getValidationSupport().registerValidator(tfCodeName, true,
                Validator.createEmptyValidator(I18n.COMMON.getString("validation.required")));

        tfCodeValueFilter = ViewHelpers.createTextFieldExt(5, 200);

        tfCodeValue = ViewHelpers.createTextFieldExt(5, 200);
        getValidationSupport().registerValidator(tfCodeValue, true,
                Validator.createEmptyValidator(I18n.COMMON.getString("validation.required")));

        cdLanguage = ViewHelpers.createCodeDecodeField(DocStoreConstants.CD_LANGUAGE,"EN_US",200);

        tfCodeDesc = ViewHelpers.createTextFieldExt(100, 200);
        getValidationSupport().registerValidator(tfCodeDesc, true,
                Validator.createEmptyValidator(I18n.COMMON.getString("validation.required")));


        GridPane formPane = new GridPane();
        formPane.setPadding(new Insets(20, 30, 10, 30));
        formPane.setHgap(10);
        formPane.setVgap(5);

        formPane.add(new Label(I18n.STATICCODEDECODE.getString("form.codeName")), 0, 0);
        formPane.add(tfCodeName, 1, 0);

        formPane.add(new Label(I18n.STATICCODEDECODE.getString("form.codeValue")), 0, 1);
        formPane.add(tfCodeValue, 1, 1);

        formPane.add(new Label(I18n.STATICCODEDECODE.getString("form.codeValueFilter")), 0, 2);
        formPane.add(tfCodeValueFilter, 1, 2);

        formPane.add(new Label(I18n.STATICCODEDECODE.getString("form.codeDesc")), 0, 3);
        formPane.add(tfCodeDesc, 1, 3);

        formPane.add(new Label(I18n.STATICCODEDECODE.getString("form.language")), 0, 4);
        formPane.add(cdLanguage.asControl(), 1, 4);

        return formPane;
    }

    @Override
    public StaticCodeDecodeDO getCurrentEntity() {
        return staticCodeDecodeDO;
    }

    @Override
    public void loadLatestEntity() {

    }

    @Override
    public void pop() {
        tfCodeName.setDataValue(staticCodeDecodeDO.getCodeName());
        tfCodeValue.setDataValue(staticCodeDecodeDO.getCodeValue());
        tfCodeDesc.setDataValue(staticCodeDecodeDO.getCodeDesc());
        tfCodeValueFilter.setDataValue(staticCodeDecodeDO.getCodeValueFilter());
        cdLanguage.setDataValue(staticCodeDecodeDO.getLanguage());
    }

    @Override
    public void push() {
        staticCodeDecodeDO.setCodeName(tfCodeName.getDataValue());
        staticCodeDecodeDO.setCodeValue(tfCodeValue.getDataValue());
        staticCodeDecodeDO.setCodeDesc(tfCodeDesc.getDataValue());
        staticCodeDecodeDO.setLanguage(cdLanguage.getDataValue());
        staticCodeDecodeDO.setCodeValueFilter(tfCodeValueFilter.getDataValue());

        if (staticCodeDecodeDO.isNew()) {
            staticCodeDecodeDO.setCreatedBy(DesktopFxApplication.getLoginSession().getUserName());
        } else {
            staticCodeDecodeDO.setUpdatedBy(DesktopFxApplication.getLoginSession().getUserName());
        }
    }
}
