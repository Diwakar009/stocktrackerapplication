package com.desktopapp.fx.control;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.util.Callback;
import com.stocktracker.domainobjs.json.CodeDataItemDO;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by diwakar009 on 3/12/2018.
 */
public class ComoBoxExtTest extends Application {

    public ComoBoxExtTest() {
        super();
    }

    @Override
    public void init() throws Exception {
        super.init();
    }

    @Override
    public void stop() throws Exception {
        super.stop();
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        testComboBox(primaryStage);
    }

    public void displayScene(Stage primaryStage, List<Control> controls){
        primaryStage.setTitle("Testing of controls...");


        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));
        final AtomicInteger rowCount = new AtomicInteger(0);


        controls.forEach(control -> {
            int rownum = rowCount.incrementAndGet();
            grid.add(new Label("Control " + rownum),0,rownum);
            grid.add(control,1,rownum);
        });


        Scene scene = new Scene(grid, 300, 275);
        scene.getStylesheets().add(getClass().getResource("/styles/light-theme.css").toExternalForm());

        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public void testComboBox(Stage primaryStage){
       List<Control> controls = new ArrayList<Control>();
       ArrayList<CodeDataItemDO> comboData = new ArrayList<CodeDataItemDO>() {{
            add(new CodeDataItemDO("NAME","Diw", "", "EN_US", "Diwakar"));
            add(new CodeDataItemDO("NAME","Diwa", "", "EN_US", "Diwa"));
            add(new CodeDataItemDO("NAME","Deb", "", "EN_US", "Debasis"));
            add(new CodeDataItemDO("NAME","Gir", "", "EN_US", "Girish"));
            add(new CodeDataItemDO("NAME","Che", "", "EN_US", "Chetana"));
            add(new CodeDataItemDO("NAME","Rash", "", "EN_US", "Rashmi"));
            add(new CodeDataItemDO("NAME","Medh", "", "EN_US", "Medhansh"));
        }};

        FilteredComboBoxDecorator.AutoCompleteComparator<CodeDataItemDO> AUTOCOMPLETE_STARTSWITH =
                (txt, obj) -> {
                    return obj.getCodeDescription().toUpperCase()
                            .startsWith(txt.toUpperCase());
                };

       ComboBoxExt<CodeDataItemDO> combo1 = new ComboBoxExt<CodeDataItemDO>(comboData,AUTOCOMPLETE_STARTSWITH) ;

       combo1.setVisibleRowCount(5);
       combo1.setCellFactory(new Callback<ListView<CodeDataItemDO>, ListCell<CodeDataItemDO>>() {
            @Override public ListCell<CodeDataItemDO> call(ListView<CodeDataItemDO> p) {
                return new ListCell<CodeDataItemDO>() {
                        @Override
                        protected void updateItem(CodeDataItemDO item, boolean empty) {
                            super.updateItem(item, empty);
                            if (item == null || empty) {
                                setGraphic(null);
                            } else {
                                setText(item.getCodeDescription());
                            }
                        }
                };
            }
        });

        combo1.addValueChangeListener(() -> {
           //System.out.println(combo1.getSelectionModel().getSelectedItem().getCodeValue());
       });


       combo1.getSelectionModel().selectFirst();




       controls.add(combo1);
       displayScene(primaryStage,controls);
    }

    public static void main(String args[]){
        launch(args);
    }

}
