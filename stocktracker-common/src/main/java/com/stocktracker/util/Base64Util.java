/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.util;

import java.nio.charset.Charset;

import org.apache.commons.codec.binary.Base64;
import org.springframework.util.StringUtils;

public class Base64Util {
    
    
    /**
     * Returns the Base64 Decoded String
     * 
     * @param encodedStr
     * @return
     */
    public static String decode(String encodedStr){
	String decodedStr = null;
	if(!StringUtils.isEmpty(encodedStr)){
	    try{
		decodedStr = new String (Base64.decodeBase64(encodedStr.getBytes()));
	    }catch(Exception e){
		e.printStackTrace();
	    }
	}
	
	return decodedStr;
    }
    
    /**
     * Returns the Base64 Encoded String
     * 
     * @param byteArr
     * @return
     */
    public static String encodeString(byte[] byteArr){
	
	String encodedStr = null;
	try{
	    byte[] bytes = Base64.encodeBase64(byteArr);
	    encodedStr = new String (bytes);
	}catch(Exception e){
	    e.printStackTrace();
	}
	
	return encodedStr;
    }
    
    /**
     * Returns the Base64 Encoded String
     * 
     * @param input
     * @return
     */
    public static String encodeString (String input){
	byte[] bytes = input.getBytes(Charset.defaultCharset());
	return encodeString(bytes);
    }
    

}
