package com.stocktracker.jpa.spring.extended.custom;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.query.QueryUtils;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import java.util.Map;


public class ExtendedRepositoryImpl<T, ID extends Serializable>
  extends CustomJpaRepository<T, ID> implements ExtendedRepository<T, ID> {

   private EntityManager entityManager;
 
    public ExtendedRepositoryImpl(JpaEntityInformation<T, ?> entityInformation, EntityManager entityManager) {
        super(entityInformation, entityManager);
        this.entityManager = entityManager;
    }

    @Override
    @Transactional
    public List<T> findByAttributeContainsText(String attributeName, String text) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<T> cQuery = builder.createQuery(getDomainClass());
        Root<T> root = cQuery.from(getDomainClass());
        cQuery
                .select(root)
                .where(builder
                        .like(root.<String>get(attributeName), "%" + text + "%"));
        TypedQuery<T> query = entityManager.createQuery(cQuery);
        return query.getResultList();
      /* return new ArrayList<>();*/
    }

    @Transactional
    public Page<T> findByNamedQuery(String namedQuery, Pageable pageable, Map<String,Object> parameters) {
        TypedQuery<T> countQuuery = this.entityManager.createNamedQuery(namedQuery,getDomainClass());
        String namedQueryString = countQuuery.unwrap(org.hibernate.Query.class).getQueryString();
        String countQueryString = QueryUtils.createCountQueryFor(namedQueryString);

        Query countQuery = this.entityManager.createQuery(countQueryString);
        for (Map.Entry<String, Object> entry : parameters.entrySet()) {
            countQuery.setParameter(entry.getKey(), entry.getValue());
        }

        Long total = (Long)countQuery.getSingleResult();
        TypedQuery<T> query = this.entityManager.createNamedQuery(namedQuery,getDomainClass());
        for (Map.Entry<String, Object> entry : parameters.entrySet()) {
            query.setParameter(entry.getKey(), entry.getValue());
        }

        return (Page)(pageable == null?new PageImpl(query.getResultList()):this.readPage(query, pageable,total));
    }

    protected Page<T> readPage(TypedQuery<T> query, Pageable pageable, Long total) {
        query.setFirstResult((int) pageable.getOffset());
        query.setMaxResults(pageable.getPageSize());
        List<T> content = total.longValue() > (long)pageable.getOffset()?query.getResultList(): Collections.emptyList();
        return new PageImpl(content, pageable, total.longValue());
    }

}