/*
 * Copyright (c) 2020.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.exceptions;

public class TechnicalException extends Exception{
    public TechnicalException(String s) {
        super(s);
    }

    @Override
    public String toString() {
        return "TechnicalException{} " + getMessage();
    }
}
