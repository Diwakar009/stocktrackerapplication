/*
 * Copyright (C) 2017 Cem Ikta
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 *
 */

package com.desktopapp.fx.controller;


import com.desktopapp.fx.DesktopFxApplication;
import com.desktopapp.fx.app.AppTheme;
import com.desktopapp.fx.control.MessageBox;
import com.desktopapp.fx.mvc.Controller;
import com.desktopapp.fx.util.I18n;
import com.desktopapp.fx.view.PreferencesDialog;
import com.desktopapp.fx.view.ThemesDialog;

import com.desktopapp.service.PreferencesService;
import com.stocktracker.domainobjs.json.PreferencesDO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Preferences controller
 *
 * @author diwakar009
 */
@Component
public class PreferencesController implements Controller {

    private final Logger logger = LoggerFactory.getLogger(PreferencesController.class);

    private PreferencesDO preferences;

    @Autowired
    private PreferencesService preferencesService;

      public void showPreferences() {
        preferences = preferencesService.find(1L);
        new PreferencesDialog(DesktopFxApplication.get().getMainStage(), this, preferences).showAndWait();
    }

    public void showThemes() {
        preferences = preferencesService.find(1L);
        new ThemesDialog(DesktopFxApplication.get().getMainStage(), this, preferences).showAndWait();
    }

    public void onSave(PreferencesDO entity) {
        logger.info("on save action");
        try {
            entity.setUpdatedBy(DesktopFxApplication.get().getLoginSession().getLoggedInUser().getId().toString());
            preferences = (PreferencesDO)preferencesService.update(entity);
            DesktopFxApplication.get().getLoginSession().setPreferences(preferences);
        } catch (Exception e) {
            logger.error("Preferences save error {}", e);
            MessageBox.create()
                    .owner(DesktopFxApplication.get().getMainStage())
                    .contentText(I18n.COMMON.getString("notification.saveException"))
                    .showError(e);
        }
    }

    public PreferencesDO update(PreferencesDO entity) {
        logger.info("on update action");
        try {
            entity.setUpdatedBy(DesktopFxApplication.get().getLoginSession().getLoggedInUser().getId().toString());
            preferences = (PreferencesDO)preferencesService.update(entity);
            DesktopFxApplication.get().getLoginSession().setPreferences(preferences);
        } catch (Exception e) {
            logger.error("Preferences save error {}", e);
            MessageBox.create()
                    .owner(DesktopFxApplication.get().getMainStage())
                    .contentText(I18n.COMMON.getString("notification.saveException"))
                    .showError(e);
        }

        return preferences;
    }

    public PreferencesDO find(Long id){
        return preferencesService.find(id);
    }


    public void changeAppTheme(AppTheme appTheme) {
        DesktopFxApplication.get().changeAppTheme(appTheme);
    }

    @Override
    public String getName() {
        return "PreferencesController";
    }

}
