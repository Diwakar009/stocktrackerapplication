/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.util;

import java.io.IOException;
import java.util.Collection;

import org.apache.commons.lang.StringUtils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;

public class JSONUtil {
	
	public static <T>T deserializeToObject(String jsonMsg,Class<T> classz) throws IOException{
		
		T obj = null;
		if(StringUtils.isNotEmpty(jsonMsg.trim())){
			ObjectMapper mapper = new ObjectMapper();
			obj = mapper.readValue(jsonMsg, classz);
		}
		
		return obj;
	}
	
	
	public static <T>T deserializeToObject(String jsonMsg,TypeReference<T> typeReference) throws IOException{
		
		T obj = null;
		if(StringUtils.isNotEmpty(jsonMsg.trim())){
			ObjectMapper mapper = new ObjectMapper();
			obj = mapper.readValue(jsonMsg, typeReference);
		}
		
		return obj;
	}
	
	
	public static <T>T deserializeToCollectionObject(String jsonMsg,Class<? extends Object> collectionClass,Class<? extends Object> targetClass) throws IOException{
		
		T obj = null;
		if(StringUtils.isNotEmpty(jsonMsg.trim())){
			ObjectMapper mapper = new ObjectMapper();
			obj = mapper.readValue(jsonMsg, TypeFactory.defaultInstance().constructCollectionType((Class<? extends Collection<T>>)collectionClass,targetClass));
		}
		
		return obj;
	}
	
	public static String serializeToJSON(Object object) throws IOException{
		
		ObjectMapper mapper = new ObjectMapper();
		String json = mapper.writeValueAsString(object);
		
		return json;
	}
	

}
