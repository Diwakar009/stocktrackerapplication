/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.domainobjs.json;

import com.stocktracker.model.BaseEntity;
import com.stocktracker.model.Preferences;

import java.io.Serializable;
import java.util.Date;

/**
 * Preference Domain Object
 *
 */
public class PreferencesDO extends BaseDomainObject implements Serializable {


    public static String CLASS_NAME = PreferencesDO.class.getName();

    private Long id;

    private Integer itemsPerPage;

    private Boolean showMessageDialog;

    private Boolean showInfoPopups;

    private Boolean showTipOfTheDay;

    private String reportExportDirectory;

    private String appTheme;

    private String environment;

    private Date currentDate;

    public PreferencesDO() {
    }

    public PreferencesDO(Preferences preferences) {
        copyEntity2DomainObject(preferences);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public Integer getItemsPerPage() {
        return itemsPerPage;
    }

    public void setItemsPerPage(Integer itemsPerPage) {
        this.itemsPerPage = itemsPerPage;
    }

    public Boolean isShowMessageDialog() {
        return showMessageDialog;
    }

    public void setShowMessageDialog(Boolean showMessageDialog) {
        this.showMessageDialog = showMessageDialog;
    }

    public Boolean isShowInfoPopups() {
        return showInfoPopups;
    }

    public void setShowInfoPopups(Boolean showInfoPopups) {
        this.showInfoPopups = showInfoPopups;
    }

    public Boolean isShowTipOfTheDay() {
        return showTipOfTheDay;
    }

    public void setShowTipOfTheDay(Boolean showTipOfTheDay) {
        this.showTipOfTheDay = showTipOfTheDay;
    }

    public String getReportExportDirectory() {
        return reportExportDirectory;
    }

    public void setReportExportDirectory(String reportExportDirectory) {
        this.reportExportDirectory = reportExportDirectory;
    }

    public String getAppTheme() {
        return appTheme;
    }

    public void setAppTheme(String appTheme) {
        this.appTheme = appTheme;
    }

    public String getEnvironment() {
        return environment;
    }

    public void setEnvironment(String environment) {
        this.environment = environment;
    }

    public Date getCurrentDate() {
        return currentDate;
    }

    public void setCurrentDate(Date currentDate) {
        this.currentDate = currentDate;
    }

    @Override
    public String toString() {
        return "PreferencesDO{" +
                "id=" + id +
                ", itemsPerPage=" + itemsPerPage +
                ", showMessageDialog=" + showMessageDialog +
                ", showInfoPopups=" + showInfoPopups +
                ", showTipOfTheDay=" + showTipOfTheDay +
                ", reportExportDirectory='" + reportExportDirectory + '\'' +
                ", appTheme='" + appTheme + '\'' +
                ", environment='" + environment + '\'' +
                ", currentDate=" + currentDate +
                '}';
    }

    @Override
    public Object getDomainObjectKey() {
        return id;
    }

    @Override
    public Preferences copyDomainObject2Entity(BaseEntity entity){
        Preferences preferences = (Preferences)entity;
        preferences.setCreatedBy(this.getCreatedBy());
        preferences.setUpdatedBy(this.getUpdatedBy());
        preferences.setCreatedAt(this.getCreatedAt());
        preferences.setUpdatedAt(this.getUpdatedAt());
        preferences.setId(this.getId());
        preferences.setItemsPerPage(this.getItemsPerPage());
        preferences.setShowMessageDialog(this.isShowMessageDialog());
        preferences.setShowInfoPopups(this.isShowInfoPopups());
        preferences.setShowTipOfTheDay(this.isShowTipOfTheDay());
        preferences.setReportExportDirectory(this.getReportExportDirectory());
        preferences.setAppTheme(this.getAppTheme());
        preferences.setEnvironment(this.getEnvironment());
        preferences.setCurrentDate(this.getCurrentDate());
        return preferences;
    }

    @Override
    public void copyEntity2DomainObject(BaseEntity entity){
        Preferences preferences = (Preferences) entity;
        this.setCreatedBy(preferences.getCreatedBy());
        this.setUpdatedBy(preferences.getUpdatedBy());
        this.setCreatedAt(preferences.getCreatedAt());
        this.setUpdatedAt(preferences.getUpdatedAt());
        this.setId(preferences.getId());
        this.setItemsPerPage(preferences.getItemsPerPage());
        this.setShowMessageDialog(preferences.isShowMessageDialog());
        this.setShowInfoPopups(preferences.isShowInfoPopups());
        this.setShowTipOfTheDay(preferences.isShowTipOfTheDay());
        this.setReportExportDirectory(preferences.getReportExportDirectory());
        this.setAppTheme(preferences.getAppTheme());
        this.setEnvironment(preferences.getEnvironment());
        this.setCurrentDate(preferences.getCurrentDate());
    }
}