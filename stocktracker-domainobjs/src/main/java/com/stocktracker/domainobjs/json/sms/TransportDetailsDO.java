/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.domainobjs.json.sms;

import com.stocktracker.domainobjs.json.BaseDomainObject;
import com.stocktracker.model.BaseEntity;
import com.stocktracker.model.sms.TransportDetails;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang.StringUtils;

import java.io.Serializable;

/**
 * TransportDetailsDO .
 * 
 */
@JsonIgnoreProperties(ignoreUnknown=true)
@JsonInclude(Include.NON_EMPTY)
public class TransportDetailsDO extends BaseDomainObject implements Serializable {
	private static final long serialVersionUID = 1L;

	public static String CLASS_NAME = TransportDetailsDO.class.getName();

	public TransportDetailsDO() {
		moreDetails= new TransportDetailsMoreDetailsDO();
	}

	public TransportDetailsDO(TransportDetails transportDetails) {
		copyEntity2DomainObject(transportDetails);
	}

	@JsonProperty("id")
	private Long id;

	@JsonProperty("transportTypeCode")
	private String transportTypeCode;

	@JsonProperty("rstNumber")
	private String rstNumber;

	@JsonProperty("vehicleNumber")
	private String vehicleNumber;

	@JsonProperty("vehicleCaptain")
	private String vehicleCaptain;

	@JsonProperty("moreDetails")
	private TransportDetailsMoreDetailsDO moreDetails;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TransportDetailsMoreDetailsDO getMoreDetails() {
		return moreDetails;
	}

	public String getTransportTypeCode() {
		return transportTypeCode;
	}

	public void setTransportTypeCode(String transportTypeCode) {
		this.transportTypeCode = transportTypeCode;
	}

	public String getRstNumber() {
		return rstNumber;
	}

	public void setRstNumber(String rstNumber) {
		this.rstNumber = rstNumber;
	}

	public String getVehicleNumber() {
		return vehicleNumber;
	}

	public void setVehicleNumber(String vehicleNumber) {
		this.vehicleNumber = vehicleNumber;
	}

	public String getVehicleCaptain() {
		return vehicleCaptain;
	}

	public void setVehicleCaptain(String vehicleCaptain) {
		this.vehicleCaptain = vehicleCaptain;
	}

	public void setMoreDetails(TransportDetailsMoreDetailsDO moreDetails) {
		this.moreDetails = moreDetails;
	}


	@JsonIgnore
	@Override
	public Object getDomainObjectKey() {
		return getId();
	}

	@Override
	public TransportDetails copyDomainObject2Entity(BaseEntity entity) {

		TransportDetails transportDetails = null;

		if(entity == null){
			transportDetails = new TransportDetails();
		}else {
			transportDetails = (TransportDetails)entity;
		}
		transportDetails.setCreatedBy(this.getCreatedBy());
		transportDetails.setUpdatedBy(this.getUpdatedBy());
		transportDetails.setCreatedAt(this.getCreatedAt());
		transportDetails.setUpdatedAt(this.getUpdatedAt());

		transportDetails.setId(this.getId());
		transportDetails.setTransportTypeCode(this.getTransportTypeCode());
		transportDetails.setVehicleCaptain(this.getVehicleCaptain());
		transportDetails.setVehicleNumber(this.getVehicleNumber());
		transportDetails.setRstNumber(this.getRstNumber());
		transportDetails.setMoreDetails(this.getMoreDetails().toJSON());
		return transportDetails;
	}

	@Override
	public void copyEntity2DomainObject(BaseEntity entity) {

		TransportDetails transportDetails = (TransportDetails)entity;

		this.setCreatedBy(transportDetails.getCreatedBy());
		this.setUpdatedBy(transportDetails.getUpdatedBy());
		this.setCreatedAt(transportDetails.getCreatedAt());
		this.setUpdatedAt(transportDetails.getUpdatedAt());

		this.setId(transportDetails.getId());
		this.setTransportTypeCode(transportDetails.getTransportTypeCode());

		this.setVehicleCaptain(transportDetails.getVehicleCaptain());
		this.setVehicleNumber(transportDetails.getVehicleNumber());
		this.setRstNumber(transportDetails.getRstNumber());

		if(StringUtils.isNotBlank(transportDetails.getMoreDetails())) {
			this.setMoreDetails(new TransportDetailsMoreDetailsDO().fromJSON(transportDetails.getMoreDetails()));
		}
	}

	@Override
	public String toString() {
		return "TransportDetailsDO{" +
				"id=" + id +
				", transportTypeCode='" + transportTypeCode + '\'' +
				", rstNumber='" + rstNumber + '\'' +
				", vehicleNumber='" + vehicleNumber + '\'' +
				", vehicleCaptain='" + vehicleCaptain + '\'' +
				", moreDetails='" + moreDetails + '\'' +
				'}';
	}
}