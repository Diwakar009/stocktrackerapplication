/*
 * Copyright (C) 2017 Cem Ikta
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 *
 */

package com.desktopapp.fx.controller;


import com.desktopapp.fx.mvc.AbstractDataPageController;
import com.desktopapp.fx.mvc.DataPageView;
import com.desktopapp.service.AbstractService;
import com.desktopapp.service.AppUserLoginService;
import com.stocktracker.domainobjs.json.AppUserLoginDO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * User Login controller
 *
 * @author diwakar009
 */
@Component
public class AppUserLoginController extends AbstractDataPageController<AppUserLoginDO> {

    @Autowired
    private AppUserLoginService appUserLoginService;

    /**
     * Get appuser login by user name
     *
     * @param userName
     * @return
     */

    public AppUserLoginDO getAppUserLoginByUserName(String userName) {
        return ((AppUserLoginService) getService()).getAppUserLoginByUserName(userName);
    }

    @Override
    protected AbstractService<AppUserLoginDO> createService() {
        return appUserLoginService;
    }

    @Override
    protected DataPageView<AppUserLoginDO> createDataPageView() {
        return null;
    }

    @Override
    public void openFormView(AppUserLoginDO user) {

    }

    @Override
    public void onAddNew() {
        openFormView(new AppUserLoginDO());
    }

    @Override
    public String getName() {
        return "AppUserLoginController";
    }

}