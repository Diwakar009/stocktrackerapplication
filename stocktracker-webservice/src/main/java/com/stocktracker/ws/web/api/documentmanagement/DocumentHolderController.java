/*
 * Copyright (c) 2020.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.ws.web.api.documentmanagement;

import java.util.ArrayList;
import java.util.List;


import com.desktopapp.service.AbstractService;
import com.desktopapp.service.DocumentHolderService;
import com.stocktracker.domainobjs.json.DocHolderDO;

import com.stocktracker.ws.web.api.GenericBaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.stocktracker.model.DocHolder;

@RestController
@RequestMapping(value="/api/documentHolder")
public class DocumentHolderController extends GenericBaseController<DocHolderDO> {

	@Autowired
	DocumentHolderService service;

	@Override
	public DocHolderDO find(DocHolderDO docHolderDO) {
		return service.find(docHolderDO);
	}

	@Override
	public DocHolderDO save(DocHolderDO docHolderDO) {
		return (DocHolderDO)service.create(docHolderDO);
	}

	@Override
	public boolean delete(DocHolderDO docHolderDO) {
		service.remove(docHolderDO);
		return true;
	}

	@Override
	public boolean deleteList(List<DocHolderDO> doList) {
		service.remove(doList);
		return true;
	}


	@Override
	public AbstractService getService() {
		return service;
	}

	

}

