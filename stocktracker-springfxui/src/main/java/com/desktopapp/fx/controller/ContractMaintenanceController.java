/*
 * Copyright (c) 2020.
 *
 * All Rights are with Diwakar C
 */

package com.desktopapp.fx.controller;


import com.desktopapp.fx.common.PageDetails;
import com.desktopapp.fx.control.multiscreen.StackedScreen;
import com.desktopapp.fx.mvc.AbstractDataPageController;
import com.desktopapp.fx.mvc.DataPageView;
import com.desktopapp.fx.view.contract.ContractForm;
import com.desktopapp.fx.view.contract.ContractSearchPage;
import com.desktopapp.fx.view.stockmaintenance.StockDetailsTransactionForm;
import com.desktopapp.fx.view.stockmaintenance.StockDetailsTransactionSearchPage;
import com.desktopapp.service.AbstractService;
import com.desktopapp.service.stockmaintenance.ContractService;
import com.stocktracker.domainobjs.json.DOPage;
import com.stocktracker.domainobjs.json.TableRowMapDO;
import com.stocktracker.domainobjs.json.sms.ContractDO;
import com.stocktracker.domainobjs.json.sms.StockDetailsDO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static com.desktopapp.service.QueryParameter.with;

/**
 * Contract Maintenance controller
 *
 * @author diwakar009
 */
@Component
public class ContractMaintenanceController extends AbstractDataPageController<ContractDO> {
    private final Logger logger = LoggerFactory.getLogger(ContractMaintenanceController.class);

    @Autowired
    ContractService contractService;

    @Override
    protected AbstractService<ContractDO> createService() {
        return contractService;
    }

    @Override
    protected DataPageView<ContractDO> createDataPageView() {
        return new ContractSearchPage();
    }

    @Override
    public void openFormView(ContractDO contractDO) {

        Map<String,Object> intent = new HashMap<String,Object>();

        if(!contractDO.isNew()){
            intent.put("is_edit",true);
        }

        new ContractForm(this,contractDO,(StackedScreen) this.getDataPageView().getStackedScreen(),intent);
    }

    @Override
    public void onAddNew() {
        this.openFormView(new ContractDO());
    }

    @Override
    public void onEdit() {

        logger.debug("on edit action");
        if (dataPageView == null) {
            return;
        }

        if (dataPageView.getSelectedModel() == null) {
            return;
        }
        this.openFormView(dataPageView.getSelectedModel());
    }


    /**
     * Get contract data
     *
     * @param industryCode
     * @param startDateRangeStart
     * @param startDateRangeEnd
     * @param contractType
     * @param contractName
     * @param contractStatus
     * @param pageDetails
     * @return
     */

    public DOPage<TableRowMapDO<String,String>> getData(String industryCode,
                                                        Date startDateRangeStart,
                                                        Date startDateRangeEnd,
                                                        String contractType, String contractName, String contractStatus, PageDetails pageDetails) {

        boolean filterEnabled = false;

        if(!"".equals(industryCode)
                || startDateRangeStart != null
                || startDateRangeEnd != null
                || !"".equals(contractType)
                || !"".equals(contractName)
                || !"".equals(contractStatus)){
            filterEnabled = true;
        }

        if (!filterEnabled) {
            return getService().listAll(with("page", String.valueOf(pageDetails.getPageNumber())).
                    and("size", String.valueOf(pageDetails.getSize())).
                    and("start",pageDetails.getStart()).
                    and("end",pageDetails.getEnd()).parameters());
        } else {
            return getService().listByFilter(with("industryCode", industryCode).
                    and("startDateRangeStart", startDateRangeStart).
                    and("startDateRangeEnd", startDateRangeEnd).
                    and("contractType", contractType).
                    and("contractName", contractName).
                    and("contractStatus", contractStatus).
                    and("page", String.valueOf(pageDetails.getPageNumber())).
                    and("size", String.valueOf(pageDetails.getSize())).
                    and("start",pageDetails.getStart()).
                    and("end",pageDetails.getEnd()).
                    parameters());
        }
    }

    @Override
    public String getName() {
        return "ContractMaintenanceController";
    }


}