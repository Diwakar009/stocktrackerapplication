package com.desktopapp.fx.commands;

import com.desktopapp.fx.DesktopFxApplication;

/**
 * Created by diwakar009 on 24/12/2018.
 */
public class ExitCommand implements ICommand{

    public void executeCommand() {
        DesktopFxApplication.get().onAppExit(null);
    }

}
