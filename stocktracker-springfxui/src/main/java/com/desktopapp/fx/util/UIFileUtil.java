/*
 * Copyright (c) 2020.
 *
 * All Rights are with Diwakar C
 */

package com.desktopapp.fx.util;

import com.desktopapp.fx.DesktopFxApplication;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DecimalFormat;

public class UIFileUtil {
	
	/**
	 * Opens bytedata as file.
	 * 
	 * @param data
	 * @param fileWithoutExtension
	 * @param extension
	 * @return
	 */
	public static boolean openByteDataAsFile(byte[] data,String fileWithoutExtension,String extension){
		
		boolean isOpened = false;
		File file = null;
		try {
		    	if(fileWithoutExtension.length() < 3){
		    	    fileWithoutExtension = fileWithoutExtension + "XXX";
		    	}
		    	
			file = File.createTempFile(fileWithoutExtension, "." + extension);
			file.deleteOnExit();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Path path = Paths.get(file.getAbsolutePath());
		if(path != null){
			try {
					if(file.exists()){
						file.delete();	
					}
					Files.write(path, data);
					isOpened = openFile(file.getAbsolutePath());;
				} catch (IOException e) {
					e.printStackTrace();
				}
			
		}
		return isOpened;
	}
	
	
	/**
	 * Print bytedata as file.
	 * 
	 * @param data
	 * @param fileWithoutExtension
	 * @param extension
	 * @return
	 */
	public static boolean printByteDataAsFile(byte[] data,String fileWithoutExtension,String extension){
		
		boolean isOpened = false;
		File file = null;
		try {
		    	if(fileWithoutExtension.length() < 3){
		    	    fileWithoutExtension = fileWithoutExtension + "XXX";
		    	}
		    	
			file = File.createTempFile(fileWithoutExtension, "." + extension);
			file.deleteOnExit();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Path path = Paths.get(file.getAbsolutePath());
		if(path != null){
			try {
					if(file.exists()){
						file.delete();	
					}
					Files.write(path, data);
					isOpened = printFile(file.getAbsolutePath());;
				} catch (IOException e) {
					e.printStackTrace();
				}
			
		}
		return isOpened;
	}
	
	/**
	 * Print any file (pdf,doc)
	 * 
	 * @param filePath
	 * @throws IOException
	 */
	public static boolean printFile(String filePath) throws IOException{
		
		boolean filePrinted = false;
		File file = new File(filePath);
		if (Desktop.isDesktopSupported()) {
	        Desktop desktop = Desktop.getDesktop();
	        desktop.print(file);
	        filePrinted = true;
    	}else{
    		//MessageBox.showInfo("Desktop Not Supported, So could not print the file!");
    		//isFileOpened = false;
    	}
    
	  
	    return filePrinted;
	}
	
	/**
	 * Open any file (pdf,doc)
	 * 
	 * @param filePath
	 * @throws IOException
	 */
	public static boolean openFile(String filePath) throws IOException{
		boolean isFileOpened = false;
		File file = new File(filePath);

		try{
			DesktopFxApplication.get().getHostServices().showDocument(file.toURI().toURL().toExternalForm());
			isFileOpened = true;
		}catch (Exception anyException){
			isFileOpened = false;
			anyException.printStackTrace();
		}
		return isFileOpened;
	}
	
	
	/**
	 * Get file Size in MB
	 * 
	 * @param file
	 * @return
	 */
	public long getFileSizeInMB(File file){
		// Get length of file in bytes
		long fileSizeInBytes = file.length();
		// Convert the bytes to Kilobytes (1 KB = 1024 Bytes)
		long fileSizeInKB = fileSizeInBytes / 1024;
		// Convert the KB to MegaBytes (1 MB = 1024 KBytes)
		long fileSizeInMB = fileSizeInKB / 1024;
		
		return fileSizeInMB;
	}

	/**
	 * Returns file size in String.
	 *  
	 * @param size
	 * @return
	 */
	
	public static String getStringSizeLengthFile(long size) {

	    DecimalFormat df = new DecimalFormat("0.00");

	    float sizeKb = 1024.0f;
	    float sizeMo = sizeKb * sizeKb;
	    float sizeGo = sizeMo * sizeKb;
	    float sizeTerra = sizeGo * sizeKb;


	    if(size < sizeMo)
	        return df.format(size / sizeKb)+ " KB";
	    else if(size < sizeGo)
	        return df.format(size / sizeMo) + " MB";
	    else if(size < sizeTerra)
	        return df.format(size / sizeGo) + " GB";

	    return "";
	}
	

}
