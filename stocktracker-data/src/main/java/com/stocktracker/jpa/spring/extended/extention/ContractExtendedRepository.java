/*
 * Copyright (c) 2020.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.jpa.spring.extended.extention;

import com.stocktracker.model.BaseEntity;
import com.stocktracker.model.sms.Contract;
import com.stocktracker.model.sms.Stock;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.Date;

/**
 * Created by diwakar009 on 22/10/2019.
 */
public interface ContractExtendedRepository {
    Page<Contract> findAllContractByFilter(String industryCodeFilter, Date startDateRangeStartFilter, Date startDateRangeEndFilter, String contractTypeFilter, String contractNameFilter, String contractStatusFilter, PageRequest pageRequest);

}
