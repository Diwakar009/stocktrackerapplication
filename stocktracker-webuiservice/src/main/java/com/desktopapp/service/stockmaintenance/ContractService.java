/*
 * Copyright (c) 2020.
 *
 * All Rights are with Diwakar C
 */

package com.desktopapp.service.stockmaintenance;

import com.desktopapp.service.AbstractService;
import com.stocktracker.domainobjs.json.DOPage;
import com.stocktracker.domainobjs.json.ResultDO;
import com.stocktracker.domainobjs.json.TableRowMapDO;
import com.stocktracker.domainobjs.json.sms.ContractDO;
import com.stocktracker.domainobjs.json.sms.PartyDetailsDO;
import com.stocktracker.domainobjs.json.sms.StockDetailsDO;
import org.springframework.stereotype.Service;

import java.util.*;


/**
 * Stock Details Service
 *
 * @author Ratnala Diwakar Choudhury
 */
@Service
public class ContractService extends AbstractService<ContractDO> {

	public ContractService() {
        super(ContractDO.class);
    }
    
	public static final String BASE_RESOURCE = "stockMaintenance";

    @Override
	protected String getBaseResource() {
		return BASE_RESOURCE;
	}

}
