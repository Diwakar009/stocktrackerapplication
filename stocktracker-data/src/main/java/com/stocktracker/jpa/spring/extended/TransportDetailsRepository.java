/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.jpa.spring.extended;

import com.stocktracker.jpa.spring.extended.custom.ExtendedRepository;
import com.stocktracker.model.sms.TransportDetails;
import org.springframework.stereotype.Repository;

@Repository
public interface TransportDetailsRepository extends ExtendedRepository<TransportDetails, Long> {

}
