/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.domainobjs.json.sms;

import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.google.gson.Gson;

import java.io.Serializable;

/**
 * PartyDetailsExtraInfoDO .
 *
 */
@JsonIgnoreProperties(ignoreUnknown=true)
@JsonInclude(Include.NON_EMPTY)
public class PartyDetailsMoreDetailsDO implements Serializable {
	private static final long serialVersionUID = 1L;
	public static String CLASS_NAME = PartyDetailsMoreDetailsDO.class.getName();

	@JsonProperty("ownIndustryCode")
	private String ownIndustryCode;

	@JsonProperty("partyNameCode")
	private String partyNameCode;

	public PartyDetailsMoreDetailsDO() {
		ownIndustryCode = new String();
		partyNameCode = new String();
	}

	public String getOwnIndustryCode() {
		return ownIndustryCode;
	}

	public void setOwnIndustryCode(String ownIndustryCode) {
		this.ownIndustryCode = ownIndustryCode;
	}

	public String getPartyNameCode() {
		return partyNameCode;
	}

	public void setPartyNameCode(String partyNameCode) {
		this.partyNameCode = partyNameCode;
	}

	public String toJSON(){
		return  new Gson().toJson(this);
	}
	public PartyDetailsMoreDetailsDO fromJSON(String json){
		return  new Gson().fromJson(json, PartyDetailsMoreDetailsDO.class);
	}
}

