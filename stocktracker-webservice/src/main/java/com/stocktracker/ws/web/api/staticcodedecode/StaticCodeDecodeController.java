/*
 * Copyright (c) 2020.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.ws.web.api.staticcodedecode;

import java.util.*;

import com.desktopapp.service.AbstractService;
import com.desktopapp.service.StaticCodeDecodeService;
import com.stocktracker.domainobjs.json.CodeDataItemDO;
import com.stocktracker.domainobjs.json.DOPage;
import com.stocktracker.domainobjs.json.StaticCodeDecodeDO;
import com.stocktracker.domainobjs.json.TableRowMapDO;
import com.stocktracker.ws.web.api.GenericBaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequestMapping(value="/api/staticCodeDecodes")
@RestController
public class StaticCodeDecodeController extends GenericBaseController<StaticCodeDecodeDO> {


	/**
	 * The StaticCodeDecodeService business service.
	 */
	@Autowired
	private StaticCodeDecodeService service;




	@RequestMapping(
			value = "/listByCodeNameNLang",
			method = RequestMethod.POST,
			produces = MediaType.APPLICATION_JSON_VALUE)

	public ResponseEntity<DOPage<TableRowMapDO<String,String>>> listByCodeNameNLang(@RequestBody TableRowMapDO<String,String> filterMap){
		logger.info("> listByCodeNameNLang");

		Integer pageNumber =  1;

		if(filterMap.getValue("page") != null){
			pageNumber = Integer.valueOf(filterMap.getValue("page"));
		}else{
			if(filterMap.getPage() != null){
				pageNumber = Integer.valueOf(filterMap.getPage());
			}
		}

		Integer size = 0;

		if(filterMap.getRow().get("size") != null){
			size = Integer.valueOf(filterMap.getRow().get("size"));
		}else{
			if(filterMap.getSize() != null){
				size = Integer.valueOf(filterMap.getSize());
			}
		}

		Pageable pageRequest = PageRequest.of(pageNumber, size);

		Map<String,Object> parameters = new HashMap<String,Object>();
		parameters.put("page",String.valueOf(pageRequest.getPageNumber()));
		parameters.put("size",String.valueOf(pageRequest.getPageSize()));
		parameters.put("codeName",filterMap.getRow().get("codeName") );
		parameters.put("language",filterMap.getRow().get("language") );
		parameters.put("search",filterMap.getRow().get("filter") );

		DOPage<TableRowMapDO<String,String>> page = ((StaticCodeDecodeService)getService()).listByCodeNameNLang(parameters);

		logger.info("< listByCodeNameNLang");
		return  new ResponseEntity<DOPage<TableRowMapDO<String,String>>>(page,HttpStatus.OK);
	}


	/**
	 * Web service endpoint to fetch CodeDataItems entities. The service returns
	 * the collection of StaticCodeDecode entities as JSON (Used).
	 *
	 * @return A ResponseEntity containing a Collection of CodeDataItemDO objects.
	 */
	@RequestMapping(
			value = "/{codeName}/{language}",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)


	public ResponseEntity<List<CodeDataItemDO>> getStaticCodeDataItemList(@PathVariable("codeName")String codeName
			, @PathVariable("language")String language) {

		logger.info("> getStaticCodeDataItemList ");

		Optional<List<CodeDataItemDO>> staticCodeDecodes = service.getStaticCodeDataItemList(codeName,language);

		logger.info("< getStaticCodeDataItemList ");

		if(staticCodeDecodes.isPresent()){
			return new ResponseEntity<List<CodeDataItemDO>>(staticCodeDecodes.get(),HttpStatus.OK);
		}else{
			return new ResponseEntity<List<CodeDataItemDO>>(new ArrayList<>(),HttpStatus.OK);
		}
	}

	/**
	 * Web service endpoint to fetch CodeDataItems entities. The service returns
	 * the collection of StaticCodeDecode entities as JSON (Used).
	 *
	 * @return A ResponseEntity containing a Collection of CodeDataItemDO objects.
	 */
	@RequestMapping(
			value = "/{codeName}/{filterCodeValue}/{language}",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)


	public ResponseEntity<List<CodeDataItemDO>> getStaticCodeDataItemList(@PathVariable("codeName")String codeName,@PathVariable("filterCodeValue")String filterCodeValue
			, @PathVariable("language")String language) {

		logger.info("> getStaticCodeDataItemList ");

		Optional<List<CodeDataItemDO>> staticCodeDecodes = service.getStaticCodeDataItemList(codeName,filterCodeValue,language);

		logger.info("< getStaticCodeDataItemList ");

		if(staticCodeDecodes.isPresent()){
			return new ResponseEntity<List<CodeDataItemDO>>(staticCodeDecodes.get(),HttpStatus.OK);
		}else{
			return new ResponseEntity<List<CodeDataItemDO>>(new ArrayList<>(),HttpStatus.OK);
		}
	}

	@RequestMapping(value="/deleteByCodeNameAndLanguage",
			method=RequestMethod.POST,
			consumes=MediaType.APPLICATION_JSON_VALUE,
			produces=MediaType.APPLICATION_JSON_VALUE)

	public ResponseEntity<Boolean> deleteByCodeNameAndLanguage(@RequestBody StaticCodeDecodeDO staticCodeDecodeDO){
		logger.info("< delete");
		boolean isSuccess = false;
		service.deleteByCodeNameAndLanguage(staticCodeDecodeDO.getCodeName(),staticCodeDecodeDO.getLanguage());
		logger.info("> delete");

		isSuccess = true;

		if(!isSuccess){
			return new ResponseEntity<Boolean>(isSuccess,HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Boolean>(isSuccess,HttpStatus.OK);

	}

	@Override
	public Map<String, Object> getFilterCriteriaMap(TableRowMapDO<String, String> filterMap, Pageable pageRequest) {
		return super.getFilterCriteriaMap(filterMap, pageRequest);
	}

	@Override
	public StaticCodeDecodeDO save(StaticCodeDecodeDO staticCodeDecodeDO) {
		return (StaticCodeDecodeDO)service.create(staticCodeDecodeDO);
	}

	@Override
	public boolean delete(StaticCodeDecodeDO staticCodeDecodeDO){
		service.remove(staticCodeDecodeDO);
		return true;
	}

	@Override
	public boolean deleteList(List<StaticCodeDecodeDO> doList) {
		service.remove(doList);
		return true;
	}

	@Override
	public StaticCodeDecodeDO find(StaticCodeDecodeDO staticCodeDecodeDO) {
		return service.find(staticCodeDecodeDO);
	}

	@Override
	public AbstractService getService() {
		return service;
	}


}
