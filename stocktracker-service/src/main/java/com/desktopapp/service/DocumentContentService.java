package com.desktopapp.service;


import com.desktopapp.bean.SendEmailBean;
import com.desktopapp.jsonobject.DocContentJSON;
import com.desktopapp.service.common.IEmailGatewayService;
import com.stocktracker.component.common.Attachment;
import com.stocktracker.framework.IEntity;
import com.stocktracker.jpa.spring.extended.DocContentRepository;
import com.stocktracker.model.BaseEntity;
import com.stocktracker.model.DocContent;
import com.stocktracker.model.DocContentBlob;
import com.stocktracker.model.DocHolder;
import com.stocktracker.resource.util.ResourceUtil;
import com.stocktracker.template.ParsedTemplate;
import com.stocktracker.template.helper.TemplateHelper;
import com.stocktracker.util.*;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import com.stocktracker.domainobjs.IDomainObject;
import com.stocktracker.domainobjs.json.DocContentDO;
import com.stocktracker.domainobjs.json.DocHolderDO;
import com.stocktracker.domainobjs.json.TableRowMapDO;
import com.stocktracker.domainobjs.jsonwrapper.ArgumentConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.io.ByteArrayInputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;


/**
 * Document Content Service
 *
 * @author Ratnala Diwakar Choudhury
 */
@Service
@Transactional(
        propagation = Propagation.SUPPORTS,
        readOnly = false)
public class DocumentContentService extends AbstractService<DocContentDO> {


    private final static Logger LOGGER = Logger
            .getLogger(DocumentContentService.class);

    @Resource
    protected DocContentRepository docContentRepository;


    @Autowired
    IEmailGatewayService emailGatewayService;


    public DocumentContentService() {

        super(DocContentDO.class);
        //repository = new DocContentStandaloneRepository(DocContent.class);
    }

    /**
     * Download documents
     *
     * @param exportDirPath
     * @param ids
     * @throws Exception
     */
    public void downloadDocContents(String exportDirPath, ArrayList<DocContentDO> ids) throws Exception {
        LOGGER.debug("downloadDocContents started ");
        Map<String, Object> argumentMap = new HashMap<String, Object>();
        argumentMap.put(ArgumentConstants.ARG_MASKDOCMENTS, "N");

        try {

            ArrayList<DocContentDO> docContents = null;
            if (ids == null || ids.size() == 0) {
                ids = (ArrayList<DocContentDO>)returnDomainObjects(docContentRepository.findAll());
            }
            ids.forEach(docContentDO -> {
                DocContentDO contentDO = (DocContentDO) docContentDO;
                DocContent content = (DocContent) getRepository().getOne(docContentDO.getId());
                DocContentJSON contentJSON = new DocContentJSON(argumentMap, content, exportDirPath);
            });
        } catch (Exception anyException) {
            anyException.printStackTrace();
            LOGGER.error("Exception Occoured " + anyException.getMessage());
            throw anyException;
        }
        LOGGER.debug("downloadDocContents ended ");
    }


    /**
     * Upload documents in the stocktracker tables automatically scaning it.
     * <p>
     * Folder Name would be name
     * File Name would be document content name
     * Document search key words would be parent folder to actual folder
     *
     * @param docHolder
     * @param folderPath
     * @throws Exception
     */

    public void uploadFolder(DocHolderDO docHolder, String folderPath, String docOwner) throws Exception {

        LOGGER.debug("uploadFolder started ");
        Map<String, List<Path>> fileMap = CommonFileUtil.getDirectoryFileMap(folderPath);

        fileMap.forEach((directory, fileList) -> {
            fileList.forEach(filePath -> {
                DocContent content = new DocContent();
                content.setDocContentName(CommonFileUtil.getFileNameWithoutExtention(filePath.getFileName().toString()));
                content.setDocFileName(filePath.getFileName().toString());
                content.setDocDirPath(filePath.getParent().toString());
                content.setDocFileExtension(FilenameUtils.getExtension(filePath.getFileName().toString()));
                try {
                    content.setDocLastModifiedTime(new Date(Files.getLastModifiedTime(filePath).toMillis()));
                } catch (Exception e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
                content.setDocHolder(docHolder.copyDomainObject2Entity(new DocHolder()));

                if ("Y".equals(docHolder.getDocPersist())) {
                    ArrayList<DocContentBlob> contentBlobs = new ArrayList<DocContentBlob>();

                    DocContentBlob contentBlob = new DocContentBlob();
                    contentBlob.setDocContent(content);
                    try {
                        contentBlob.setDocContentBlob(FileUtils.readFileToByteArray(filePath.toFile()));
                        //contentBlob.setDocContentBlob(CommonFileUtil.getBlobFromByteArray(FileUtils.readFileToByteArray(filePath.toFile())));
                    } catch (Exception e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    contentBlobs.add(contentBlob);

                    content.setDocContentBlobs(contentBlobs);
                }
                //contents.add(content);
                docContentRepository.save(content);

            });
        });
        LOGGER.debug("uploadFolder ended ");
    }


    public void emailAttachments(SendEmailBean email) {
        LOGGER.debug("emailAttachments started ");
        try {

            List<String> toList = email.getToList();
            List<String> ccList = email.getCcList();
            List<String> bccList = email.getBccList();
            String subject = email.getSubject();
            String body = email.getBody();
            List<DocContent> docContents = email.getIds();

            List<Attachment> attachments = new ArrayList<Attachment>();

            String convertedToHtmlTxt = CommonUtil.convertTxtToHtml(body);
            KeyValueList parameters = KeyValueList.EMPTY;
            parameters = new KeyValueList("bodyHtml", convertedToHtmlTxt, parameters);

            // templateData Get the template data from the VM file
            String templateData = ResourceUtil.getResourceContent("EmailTemplate.vm");
            ParsedTemplate template = TemplateHelper.parseTemplate("EMAIL_TEMPLATE", templateData, Locale.US);
            String emailContent = TemplateHelper.renderTemplate(template, parameters);

            for (DocContent content : docContents) {
                List<DocContentBlob> contentBlob = content.getDocContentBlobs();
                if (contentBlob != null) {
                    DocContentBlob blob = contentBlob.get(0);
                    // TODO: file type should be changed based on extension
                    Attachment attachment = new Attachment(false, Long.toString(content.getId()), content.getDocFileName(), "application/pdf", Base64Util.encodeString(blob.getDocContentBlob()));
                    attachments.add(attachment);
                }
            }


            emailGatewayService.send(toList, ccList, bccList, subject, emailContent, attachments);

        } catch (Exception anyException) {
            anyException.printStackTrace();
            LOGGER.error("Exception Occoured " + anyException.getMessage());
            throw anyException;
        }
    }

    public IEmailGatewayService getEmailGatewayService() {
        return emailGatewayService;
    }

    public void setEmailGatewayService(IEmailGatewayService emailGatewayService) {
        this.emailGatewayService = emailGatewayService;
    }

    public ByteArrayInputStream downloadDocument(DocContentDO docContentDO) {
       /* return ((DocContentRepository) repository).fetchDocument(docContentDO.copyDomainObject2Entity(new DocContent()));*/
       return null;
    }

    @Override
    public Class<DocContent> getEntityClass() {
        return DocContent.class;
    }

    @Override
    public DocContentDO returnDomainObject(BaseEntity entity) {
        if (entity == null) {
            return null;
        }
        DocContentDO domainObject = new DocContentDO((DocContent) entity);
        return domainObject;
    }

    @Override
    public List<DocContentDO> returnDomainObjects(List<? extends BaseEntity> entities) {
        if (entities == null) {
            return null;
        }
        List<DocContentDO> domainObjects = new ArrayList<DocContentDO>();
        if (entities != null) {
            for (IEntity entity : entities) {
                DocContentDO domainObject = new DocContentDO((DocContent) entity);
                domainObjects.add(domainObject);
            }
        }
        return domainObjects;
    }

    @Override
    public BaseEntity returnEntity(DocContentDO domainObject) {
        DocContentDO docContentDO = (DocContentDO) domainObject;
        DocContent docContent = new DocContent();
        docContentDO.copyDomainObject2Entity(docContent);
        return docContent;
    }

    @Override
    public List<? extends BaseEntity> returnEntities(List<DocContentDO> domainObjects) {
        List<DocContent> entities = new ArrayList();
        for (IDomainObject domainObject : domainObjects) {
            DocContentDO docContentDO = (DocContentDO) domainObject;
            DocContent docContent = new DocContent();
            docContentDO.copyDomainObject2Entity(docContent);
            entities.add(docContent);
        }
        return entities;
    }

    @Override
    public TableRowMapDO<String, String> copyDomainObjToTableRowMap(IDomainObject domainObject) {
        DocContentDO docContentDO = (DocContentDO) domainObject;
        TableRowMapDO<String, String> tableRowMapDO = new TableRowMapDO<String, String>();
        tableRowMapDO.add("id", docContentDO.getId().toString());
        tableRowMapDO.add("docContentName", docContentDO.getDocContentName());
        tableRowMapDO.add("docFileName", docContentDO.getDocFileName());
        tableRowMapDO.add("docFileExtension", docContentDO.getDocFileExtension());

        if (docContentDO.getDocLastModifiedTime() != null) {
            tableRowMapDO.add("docLastModifiedTime", DateTimeUtil.convertDateToUTCString(docContentDO.getDocLastModifiedTime()));
        } else {
            tableRowMapDO.add("docLastModifiedTime", "");
        }

        tableRowMapDO.setClazzName(DocContentDO.CLASS_NAME);

        return tableRowMapDO;

    }

    @Override
    public DocContentRepository getRepository() {
        return this.docContentRepository;
    }

}
