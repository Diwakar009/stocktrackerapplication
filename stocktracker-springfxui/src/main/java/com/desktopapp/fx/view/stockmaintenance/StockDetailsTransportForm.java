/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.desktopapp.fx.view.stockmaintenance;

import com.desktopapp.fx.common.IGenericComponent;
import com.desktopapp.fx.control.CodeDecodeField;
import com.desktopapp.fx.control.TextFieldExt;
import com.desktopapp.fx.control.listeners.ValueChangeListener;
import com.stocktracker.component.constants.CodeDecodeConstants;
import com.stocktracker.domainobjs.json.sms.StockDetailsDO;
import com.stocktracker.domainobjs.json.sms.TransportDetailsDO;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.*;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.List;

    /**
     * Created by diwakar009 on 10/11/2019.
     */
    public class StockDetailsTransportForm extends StackPane{

    public final static String TRANSPORT_TYPE_OWN = "OWN";

    private CodeDecodeField cdTransportType;
    private CodeDecodeField cdOwnVehicle;
    private TextFieldExt tfRstNumber;
    private TextFieldExt tfVehicleNumber;
    private StockDetailsTransactionForm stockDetailsTransactionForm;
    List<IGenericComponent> components = new ArrayList<>();


    public StockDetailsTransportForm(StockDetailsTransactionForm stockDetailsTransactionForm) {
        this.stockDetailsTransactionForm=stockDetailsTransactionForm;
        this.cdTransportType = new CodeDecodeField(CodeDecodeConstants.CD_TRANSPORT_TYPE,CodeDecodeConstants.DEFAULT_LANGUAGE);
        this.cdOwnVehicle= new CodeDecodeField(CodeDecodeConstants.CD_OWN_VEHICLE,CodeDecodeConstants.DEFAULT_LANGUAGE);
        this.cdOwnVehicle.setSkipCache(true);

        this.tfRstNumber = new TextFieldExt();
        this.tfVehicleNumber = new TextFieldExt();

        build();

        registerComponents();
    }

    public void build(){
        int columnIndex = 0;
        int rowIndex = 0;

        BorderPane borderPane = new BorderPane();
        Label transactionDtlsLbl = new Label();
        transactionDtlsLbl.setStyle("-fx-font-weight: bold");

        transactionDtlsLbl.setText("Transport Details");
        HBox lblHBox = new HBox();
        lblHBox.getChildren().add(transactionDtlsLbl);
        lblHBox.setAlignment(Pos.CENTER);
        borderPane.setTop(lblHBox);

        GridPane formPane = new GridPane();
        formPane.setPadding(new Insets(10, 10, 10, 10));
        formPane.setHgap(10);
        formPane.setVgap(10);

        formPane.setGridLinesVisible(false);

        ColumnConstraints column1 = new ColumnConstraints();
        column1.setPercentWidth(50);

        ColumnConstraints column2 = new ColumnConstraints();
        column2.setPercentWidth(100);

        ColumnConstraints column3 = new ColumnConstraints();
        column3.setPercentWidth(2);

        formPane.getColumnConstraints().addAll(column1,column2,column3,column1,column2);

        ValueChangeListener transChangeListener = new ValueChangeListener() {
            @Override
            public void valueChanged() {
                if(TRANSPORT_TYPE_OWN.equals(cdTransportType.getDataValue())){
                    cdOwnVehicle.setAsReadOnly(false);
                    tfVehicleNumber.setAsReadOnly(true);
                }else{
                    cdOwnVehicle.setAsReadOnly(true);
                    tfVehicleNumber.setAsReadOnly(false);
                    tfVehicleNumber.clearDataValue();
                    cdOwnVehicle.clearDataValue();
                }
            }
        };
        this.cdTransportType.addValueChangeListener(transChangeListener);

        formPane.add(new Label("Transport Type"), columnIndex, rowIndex);
        formPane.add(this.cdTransportType.asControl(), columnIndex + 1, rowIndex);

        this.cdOwnVehicle.addValueChangeListener(() -> {
            this.tfVehicleNumber.setDataValue(cdOwnVehicle.getDataValueDescription());
        });

        formPane.add(new Label("Own Vehicle"), columnIndex+ 3, rowIndex);
        formPane.add(this.cdOwnVehicle.asControl(), columnIndex + 4, rowIndex++);
        this.cdOwnVehicle.setAsReadOnly(true);

        formPane.add(new Label("Vehicle Number"), columnIndex, rowIndex);
        formPane.add(this.tfVehicleNumber, columnIndex + 1, rowIndex++);

        formPane.add(new Label("RST Number"), columnIndex, rowIndex);
        formPane.add(this.tfRstNumber, columnIndex + 1, rowIndex++);

        VBox vbox = new VBox();
        HBox formPaneHBox = new HBox();
        formPaneHBox.getChildren().add(formPane);
        //formPaneHBox.setAlignment(Pos.CENTER);

        vbox.getChildren().add(formPaneHBox);

        borderPane.setCenter(vbox);

        getChildren().add(borderPane);

        setStyle("-fx-border-color: gray");
    }

    /**
     * Get Generic Components
     *
     * @return
     */
    private List<IGenericComponent> registerComponents(){
        components.add(cdOwnVehicle);
        components.add(cdTransportType);
        components.add(tfRstNumber);
        components.add(tfVehicleNumber);
        return components;
    }

    /**
     * Clear all
     */
    public void clearAll(){
        components.forEach(component -> component.clearDataValue());
    }

    /**
     * Set DO to Controls
     *
     * @param stockDetailsDO
     */
    public void setDOToControls(StockDetailsDO stockDetailsDO){
        if(stockDetailsDO != null) {
            TransportDetailsDO transportDetailsDO = null;
            if(stockDetailsDO.getTransportDetailsDO() != null){
                transportDetailsDO = stockDetailsDO.getTransportDetailsDO();
            }else{
                transportDetailsDO = new TransportDetailsDO();
            }
            cdTransportType.setDataValue(transportDetailsDO.getTransportTypeCode());
            tfRstNumber.setDataValue(transportDetailsDO.getRstNumber());
            tfVehicleNumber.setDataValue(transportDetailsDO.getVehicleNumber());

            cdOwnVehicle.setDataValue(transportDetailsDO.getMoreDetails().getOwnVehicleCode());
            stockDetailsDO.setTransportDetailsDO(transportDetailsDO);
        }
    }

    /**
     * Set controls to DO
     *
     * @param stockDetailsDO
     */
    public void setControlsToDO(StockDetailsDO stockDetailsDO){
        if(stockDetailsDO != null) {
            TransportDetailsDO transportDetailsDO = null;
            if(stockDetailsDO.getTransportDetailsDO() != null){
                transportDetailsDO = stockDetailsDO.getTransportDetailsDO();
            }else{
                transportDetailsDO = new TransportDetailsDO();
            }
            if(StringUtils.isNotBlank(this.cdOwnVehicle.getDataValue())) {
                transportDetailsDO.getMoreDetails().setOwnVehicleCode(this.cdOwnVehicle.getDataValue());
            }

            transportDetailsDO.setTransportTypeCode(this.cdTransportType.getDataValue());
            transportDetailsDO.setRstNumber(this.tfRstNumber.getDataValue());
            transportDetailsDO.setVehicleNumber(this.tfVehicleNumber.getDataValue());

            stockDetailsDO.setTransportDetailsDO(transportDetailsDO);
        }
    }

    public List<IGenericComponent> getComponents() {
        return components;
    }

    public void setComponents(List<IGenericComponent> components) {
        this.components = components;
    }
}
