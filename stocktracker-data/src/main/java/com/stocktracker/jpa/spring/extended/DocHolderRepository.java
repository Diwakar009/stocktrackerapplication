package com.stocktracker.jpa.spring.extended;

import com.stocktracker.jpa.spring.extended.custom.ExtendedRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.stocktracker.model.DocHolder;


@Repository
public interface DocHolderRepository extends ExtendedRepository<DocHolder, Long> {
	
	@Query(value="select distinct d from DocHolder as d left outer join d.docContents as c where d.docOwner LIKE :docOwner OR d.docCoowner LIKE :docCoowner OR d.docName like :docName OR d.docKeywords like :docKeywords or c.docContentName LIKE :docKeywords OR c.docContentKeywords LIKE :docKeywords ORDER BY d.docName")
	public Page<DocHolder> findByDocHolderLikeAll(@Param("docOwner") String docOwner,
                                                  @Param("docCoowner") String docCoowner,
                                                  @Param("docName") String docName,
                                                  @Param("docKeywords") String docKeywords, Pageable pagable);
	
	@Modifying
	@Query(value="delete from DocHolder where id =:id")
	public void deleteDocHolder(@Param("id") Long id);

}
