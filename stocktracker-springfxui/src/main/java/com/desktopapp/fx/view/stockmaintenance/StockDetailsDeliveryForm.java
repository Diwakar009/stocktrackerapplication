/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.desktopapp.fx.view.stockmaintenance;

import com.desktopapp.fx.common.IGenericComponent;
import com.desktopapp.fx.control.CodeDecodeField;
import com.desktopapp.fx.control.IntegerField;
import com.desktopapp.fx.control.TextAreaExt;
import com.stocktracker.component.constants.CodeDecodeConstants;
import com.stocktracker.domainobjs.json.sms.DeliveryDetailsDO;
import com.stocktracker.domainobjs.json.sms.StockDetailsDO;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.*;
import javafx.scene.text.Text;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by diwakar009 on 10/11/2019.
 */
public class StockDetailsDeliveryForm extends StackPane{

    private CodeDecodeField cdDeliveryType;
    private CodeDecodeField cdDeliveryDepo;
    private IntegerField ifLatNumber;
    private TextAreaExt taDeliveryAddress;

    private StockDetailsTransactionForm stockDetailsTransactionForm;
    List<IGenericComponent> components = new ArrayList<>();


    public StockDetailsDeliveryForm(StockDetailsTransactionForm stockDetailsTransactionForm) {
        this.stockDetailsTransactionForm=stockDetailsTransactionForm;
        this.cdDeliveryType = new CodeDecodeField(CodeDecodeConstants.CD_DELIVERY_TYPE,CodeDecodeConstants.DEFAULT_LANGUAGE);
        this.cdDeliveryType.setSkipCache(true);

        this.cdDeliveryDepo = new CodeDecodeField(CodeDecodeConstants.CD_DELIVERY_DEPO,CodeDecodeConstants.DEFAULT_LANGUAGE);
        this.cdDeliveryDepo.setSkipCache(true);
        this.cdDeliveryType.registerLinkedCodeDecodeField(this.cdDeliveryDepo);

        this.ifLatNumber= new IntegerField();
        this.taDeliveryAddress = new TextAreaExt(100);
        this.taDeliveryAddress.setPrefRowCount(2);

        this.cdDeliveryType.addValueChangeListener(() -> {
            if("PRIV".equals(cdDeliveryType.getDataValue())){

                taDeliveryAddress.setAsReadOnly(false);
                cdDeliveryDepo.setAsReadOnly(true);
                ifLatNumber.setAsReadOnly(true);

                taDeliveryAddress.clearDataValue();
                cdDeliveryDepo.clearDataValue();
                ifLatNumber.clearDataValue();

            }else{
                taDeliveryAddress.setAsReadOnly(true);
                cdDeliveryDepo.setAsReadOnly(false);
                ifLatNumber.setAsReadOnly(false);

                taDeliveryAddress.clearDataValue();
                cdDeliveryDepo.clearDataValue();
                ifLatNumber.clearDataValue();
            }
        });

        build();
        registerComponents();
    }

    public void build(){
        int columnIndex = 0;
        int rowIndex = 0;

        BorderPane borderPane = new BorderPane();
        Label transactionDtlsLbl = new Label();
        transactionDtlsLbl.setStyle("-fx-font-weight: bold");

        transactionDtlsLbl.setText("Delivery Details");
        HBox lblHBox = new HBox();
        lblHBox.getChildren().add(transactionDtlsLbl);
        lblHBox.setAlignment(Pos.CENTER);
        borderPane.setTop(lblHBox);

        GridPane formPane = new GridPane();
        formPane.setPadding(new Insets(10, 10, 10, 10));
        formPane.setHgap(10);
        formPane.setVgap(10);

        formPane.setGridLinesVisible(false);

        ColumnConstraints column1 = new ColumnConstraints();
        column1.setPercentWidth(50);

        ColumnConstraints column2 = new ColumnConstraints();
        column2.setPercentWidth(100);

        ColumnConstraints column3 = new ColumnConstraints();
        column3.setPercentWidth(2);

        formPane.getColumnConstraints().addAll(column1,column2,column3,column1,column2);

        formPane.add(new Label("Delivery Type"), columnIndex, rowIndex);
        formPane.add(this.cdDeliveryType.asControl(), columnIndex + 1, rowIndex++);

        formPane.add(new Label("Delivery Depo"), columnIndex, rowIndex);
        formPane.add(this.cdDeliveryDepo.asControl(), columnIndex + 1, rowIndex);

        formPane.add(new Label("LAT Number"), columnIndex+ 3, rowIndex);
        formPane.add(this.ifLatNumber, columnIndex + 4, rowIndex++);

        formPane.add(new Label("Delivery Address"), columnIndex, rowIndex);
        formPane.add(this.taDeliveryAddress, columnIndex + 1, rowIndex++);

        VBox vbox = new VBox();
        HBox formPaneHBox = new HBox();
        formPaneHBox.getChildren().add(formPane);
        //formPaneHBox.setAlignment(Pos.CENTER);

        vbox.getChildren().add(formPaneHBox);

        borderPane.setCenter(vbox);

        getChildren().add(borderPane);

        setStyle("-fx-border-color: gray");
    }

    /**
     * Get Generic Components
     *
     * @return
     */
    private List<IGenericComponent> registerComponents(){
        components.add(cdDeliveryType);
        components.add(ifLatNumber);
        components.add(taDeliveryAddress);
        components.add(cdDeliveryDepo);
        return components;
    }

    /**
     * Clear all
     */
    public void clearAll(){
        components.forEach(component -> component.clearDataValue());
    }

    /**
     * Set DO to Controls
     *
     * @param stockDetailsDO
     */
    public void setDOToControls(StockDetailsDO stockDetailsDO){

        if(stockDetailsDO != null) {
            DeliveryDetailsDO deliveryDetailsDO = null;
            if(stockDetailsDO.getDeliveryDetailsDO() != null){
                deliveryDetailsDO = stockDetailsDO.getDeliveryDetailsDO();
            }else{
                deliveryDetailsDO = new DeliveryDetailsDO();
            }
            cdDeliveryType.setDataValue(deliveryDetailsDO.getDeliveryTypeCode());
            cdDeliveryDepo.setDataValue(deliveryDetailsDO.getDeliveryDepo());
            if(deliveryDetailsDO.getDeliveryLatNo() != null) {
                ifLatNumber.setDataValue(Long.toString(deliveryDetailsDO.getDeliveryLatNo()));
            }
            taDeliveryAddress.setDataValue(deliveryDetailsDO.getDeliveryAddress());

        }
    }

    /**
     * Set controls to DO
     *
     * @param stockDetailsDO
     */
    public void setControlsToDO(StockDetailsDO stockDetailsDO){
        if(stockDetailsDO != null) {
            DeliveryDetailsDO deliveryDetailsDO = null;

            if(stockDetailsDO.getDeliveryDetailsDO() != null){
                deliveryDetailsDO = stockDetailsDO.getDeliveryDetailsDO();
            }else{
                deliveryDetailsDO = new DeliveryDetailsDO();
            }

            deliveryDetailsDO.setDeliveryTypeCode(this.cdDeliveryType.getDataValue());
            deliveryDetailsDO.setDeliveryDepo(this.cdDeliveryDepo.getDataValue());

            if(!StringUtils.isEmpty(this.ifLatNumber.getDataValue())) {
                deliveryDetailsDO.setDeliveryLatNo(Long.parseLong(this.ifLatNumber.getDataValue()));
            }else{
                deliveryDetailsDO.setDeliveryLatNo(null);
            }

            deliveryDetailsDO.setDeliveryAddress(this.taDeliveryAddress.getDataValue());

            stockDetailsDO.setDeliveryDetailsDO(deliveryDetailsDO);

        }
    }

    public List<IGenericComponent> getComponents() {
        return components;
    }

    public void setComponents(List<IGenericComponent> components) {
        this.components = components;
    }

    public CodeDecodeField getCdDeliveryType() {
        return cdDeliveryType;
    }

    public CodeDecodeField getCdDeliveryDepo() {
        return cdDeliveryDepo;
    }
}
