/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.model.sms;

import com.stocktracker.framework.IEntity;
import com.stocktracker.model.BaseEntity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the TBL_STOCK database table.
 * 
 */
@Entity
@Table(name="TBL_STOCK")
public class Stock extends BaseEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private StockPK id;

	public Stock(StockPK id) {
		this.id = id;
		init();
	}

	public Stock(StockPK id, Long totalNoOfPackets, Double totalWeightment, Date asOnDate) {
		this.id = id;
		this.totalNoOfPackets = totalNoOfPackets;
		this.totalWeightment = totalWeightment;
		this.asOnDate = asOnDate;
	}

	public Stock() {
		init();
	}

	public void init(){
		setTotalNoOfPackets(0l);
		setTotalWeightment(0.0d);
		setAsOnDateAsNow();
	}

	@Basic(optional = false)
	@Column(name = "TOTAL_NO_OF_PKTS", nullable = false, columnDefinition = "BIGINT UNSIGNED")
	private Long totalNoOfPackets;

	@Basic(optional = false)
	@Column(name = "TOTAL_WEIGHTMENT", nullable = false, columnDefinition = "DECIMAL UNSIGNED",precision=10, scale=2)
	private Double totalWeightment;

	@Column(name = "AS_ON_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date asOnDate;

	@Column(name="MORE_DETAILS", length = 65535)
	private String moreDetails;

	public StockPK getId() {
		return id;
	}

	public void setId(StockPK id) {
		this.id = id;
	}

	public String getIndustryCode() {
		if(getId() == null){
			this.id = new StockPK();
		}
		return this.id.getIndustryCode();
	}

	public void setIndustryCode(String industryCode) {
		if(getId() == null){
			this.id = new StockPK();
		}
		this.id.setIndustryCode(industryCode);
	}

	public String getStockCode() {
		if(getId() == null){
			this.id = new StockPK();
		}
		return this.id.getStockCode();
	}

	public void setStockCode(String stockCode) {
		if(getId() == null){
			this.id = new StockPK();
		}
		this.id.setStockCode(stockCode);
	}

	public String getStockVarietyCode() {
		if(getId() == null){
			this.id = new StockPK();
		}
		return this.id.getStockVarietyCode();
	}

	public void setStockVarietyCode(String stockVarietyCode) {
		if(getId() == null){
			this.id = new StockPK();
		}
		this.id.setStockVarietyCode(stockVarietyCode);
	}

	public Long getTotalNoOfPackets() {
		return totalNoOfPackets;
	}

	public void setTotalNoOfPackets(Long totalNoOfPackets) {
		this.totalNoOfPackets = totalNoOfPackets;
	}

	public Double getTotalWeightment() {
		return totalWeightment;
	}

	public void setTotalWeightment(Double totalWeightment) {
		this.totalWeightment = totalWeightment;
	}

	public Date getAsOnDate() {
		return asOnDate;
	}

	public void setAsOnDate(Date asOnDate) {
		this.asOnDate = asOnDate;
	}

	public String getMoreDetails() {
		return moreDetails;
	}

	public void setMoreDetails(String moreDetails) {
		this.moreDetails = moreDetails;
	}

	/**
	 * Add the total no of packets
	 *
	 * @param totalNoOfPackets
	 * @return
	 */
	public Long addTotalNoofPackets(Long totalNoOfPackets){
		setTotalNoOfPackets(getTotalNoOfPackets() + totalNoOfPackets);
		setAsOnDateAsNow();
		return getTotalNoOfPackets();
	}

	/**
	 * Add total weightments in quintals
	 *
	 * @param totalWeightment
	 * @return
	 */
	public Double addTotalWeightment(Double totalWeightment){
		setTotalWeightment(getTotalWeightment() + totalWeightment);
		setAsOnDateAsNow();
		return getTotalWeightment();
	}


	/**
	 * Substruct the total no of packets
	 *
	 * @param totalNoOfPackets
	 * @return
	 */
	public Long substractTotalNoofPackets(Long totalNoOfPackets){
		Long result = getTotalNoOfPackets() - totalNoOfPackets;
		/*if(result < 0){
			setTotalNoOfPackets(0l);
		}else{*/
			setTotalNoOfPackets(result);
		/*}*/
		setAsOnDateAsNow();
		return getTotalNoOfPackets();
	}

	/**
	 * Add total weightments in quintals
	 *
	 * @param totalWeightment
	 * @return
	 */
	public Double substractTotalWeightment(Double totalWeightment){

		Double result = getTotalWeightment() - totalWeightment;
		/*if(result < 0.0d){
			setTotalWeightment(0.0d);
		}else{*/
			setTotalWeightment(result);
		/*}*/
		setAsOnDateAsNow();
		return getTotalWeightment();
	}



	/**
	 * Set as on date as now.
	 *
	 */
	public void setAsOnDateAsNow(){
		setAsOnDate(new Date());
	}

	@Override
	public Serializable getEntityPK() {
		return null;
	}

	@Override
	public void copyEntity(IEntity entity) {

		Stock source = (Stock)entity;
		this.setCreatedBy(source.getCreatedBy());
		this.setUpdatedBy(source.getUpdatedBy());
		this.setCreatedAt(source.getCreatedAt());
		this.setUpdatedAt(source.getUpdatedAt());

		this.setTotalNoOfPackets(source.getTotalNoOfPackets());
		this.setAsOnDate(source.getAsOnDate());
		this.setTotalWeightment(source.getTotalWeightment());
		this.setMoreDetails(source.getMoreDetails());
	}

	@Override
	public String toString() {
		return "Stock{" +
				"id=" + id.toString() +
				", totalNoOfPackets=" + totalNoOfPackets +
				", totalWeightment=" + totalWeightment +
				", asOnDate=" + asOnDate +
				", moreDetails='" + moreDetails + '\'' +
				'}';
	}
}