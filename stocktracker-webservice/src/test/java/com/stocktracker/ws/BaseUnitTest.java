package com.stocktracker.ws;

import org.junit.After;
import org.junit.Before;

/**
 * Created by diwakar009 on 21/11/2017.
 */
public abstract class BaseUnitTest extends AbstractControllerTest {

    @Before
    public void setUp() {
        System.out.println("*****************START TEST*******************");
        super.setUp();
    }

    @After
    public void tearDown() {
        System.out.println("*****************END TEST*******************");
    }


    public void displayMessage(String content){
        System.out.println(content);
    }

}
