package com.stocktracker.util;

import com.stocktracker.resource.util.ResourceUtil;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * JPA Util
 *
 * @author Ratala Diwakar Choudhury
 */
public class JpaUtil {

    private static EntityManagerFactory emf = null;

    
    public JpaUtil() {
    }

    /**
     * Create entity manager factory.
     *
     * @return entity manager factory from persistence unit
     */
    public static EntityManagerFactory getEntityManagerFactory() {
        if (emf == null) {
        	String persistentUnit = ResourceUtil.getPropertyValue("DB_PU");
        	
        	if("MYSQL".equals(persistentUnit)){
        		emf = Persistence.createEntityManagerFactory("desktopMysqlPU");
        	}else if("DERBY".equals(persistentUnit)){
        		emf = Persistence.createEntityManagerFactory("desktopDerbyPU");
        	}else if("MANGODB".equals(persistentUnit)){
    			emf = Persistence.createEntityManagerFactory("desktopMangoDBPU");
        	}else if("NEO4J_EMBEDED".equals(persistentUnit)){
    			emf = Persistence.createEntityManagerFactory("desktopNeo4jPU");
        	}else{
        	    emf = Persistence.createEntityManagerFactory("desktopEmbedDerbyPU");
        	}
        }
        return emf;
    }

    /**
     * Close the entity manager factory.
     */
    public static void closeEntityManagerFactory() {
        if (emf != null) {
            emf.close();
        }
    }

    /**
     * Gets new entity manager instance.
     *
     * @return new entity manager instance
     */
    public static EntityManager getEntityManager() {
        return getEntityManagerFactory().createEntityManager();
    }

}
