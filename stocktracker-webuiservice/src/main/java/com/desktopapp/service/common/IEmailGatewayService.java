package com.desktopapp.service.common;

import java.util.Collection;
import java.util.List;

import com.stocktracker.component.common.Attachment;

public interface IEmailGatewayService {
	
	public void send(String to, List<String> ccList, List<String> bccList, String subject, String body, Collection<Attachment> attachments);
	public void send(List<String> toList, List<String> ccList, List<String> bccList, String subject, String body, Collection<Attachment> attachments);

}
