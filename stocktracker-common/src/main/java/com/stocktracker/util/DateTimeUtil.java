/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.util;


import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.util.StringUtils;

public class DateTimeUtil {
    
    public static final String UTC_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
    
    public static final String FORMAT_dd_MMM_yyyy="dd MMM yyyy";
    public static final String FORMAT_yyyy_MM_dd="yyyyMMdd";
    public static final String FORMAT_yyyyMMdd="yyyy-MM-dd";
    public static final String FORMAT_dd_MMMM_yyyy="dd MMMM yyyy";
    public static final String FORMAT_dd_MM_yy="dd.MM.yy";
    public static final String FORMAT_dd_MMM_yyyy_HH_mm_ss="dd MMM yyyy HH:mm:ss";
    public static final String FORMAT_yyyyMMddHHmm="yyyyMMddHHmm";

    public static final String FORMAT_dd_MM_yyyy="dd/MM/yyyy";


    /**
     * get time stamp in service format
     * 
     * @return
     */
    public static String getTimeStampinServiceFormat(){
	Date current = new Date(System.currentTimeMillis());
	SimpleDateFormat sdf = new SimpleDateFormat(UTC_FORMAT);	
	return sdf.format(current);
    }
    
   
    /**
     * returns the formatted date in string
     * 
     * @param date
     * @param dateFormat
     * @return
     */
    public static String formatDate(final Date date,String dateFormat){
		String formatedDate = "";
		if((dateFormat != null) && (!dateFormat.trim().isEmpty()) && (date != null )){
		    formatedDate = new SimpleDateFormat(dateFormat).format(date);
		}
		return formatedDate;
    }
    
    /**
     * Convert Date to UTC Format String
     * 
     * @param date
     * @return
     */
    public static String convertDateToUTCString(Date date){
    	return formatDate(date,UTC_FORMAT);
    }
    
    /**
     * Convert UTC Format String to Date
     * 
     * @param date
     * @return
     */
    public static Date convertUTCStringToDate(String date) throws ParseException{
    	return getDateFromString(date,UTC_FORMAT);
    }
    
    
    /**
     * Convert UTC String format to other format
     * 
     * @param dateString
     * @param format
     * @return
     * @throws ParseException
     */
    public static String covertUTCStringToOtherFormat(String dateString,String format) throws ParseException {
    	Date parsed = convertUTCStringToDate(dateString);
    	return formatDate(parsed,format);
    }
    
    /**
     * Convert UTC String format to other format
     * 
     * @param dateString
     * @param format
     * @return
     * @throws ParseException
     */
//    public static Date covertUTCDateToOtherDateFormat(Date date,String format) throws ParseException {
//    	Date parsed = formatDate(date,format);
//    	return parsed;
//    }
    
    
    /**
     * returns date from string 
     * 
     * @param dateString
     * @param dateFormat
     * @return
     * @throws ParseException
     */
    public static Date getDateFromString (final String dateString, String dateFormat) throws ParseException{
	
	Date date = null;
	if(!StringUtils.isEmpty(dateString)){
	    DateFormat dateFormatter = new SimpleDateFormat(dateFormat);
	    date = (Date)dateFormatter.parse(dateString);
	}
	return date;
	
    }
	    
    
    

}
