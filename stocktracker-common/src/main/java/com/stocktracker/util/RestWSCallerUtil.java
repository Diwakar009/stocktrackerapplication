/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.lang.exception.ExceptionUtils;


public class RestWSCallerUtil {
	
	@Deprecated
	public static <T>T callGetWebService(String urlString,Map<String,String> nameValuePairMap,Map<String,String> headerMap,Class<T> targerClass){
		
		URL url = null;
		T output = null;
		GetMethod request = null;
		List<NameValuePair> nameValuePairs = null;
		org.apache.commons.httpclient.util.HttpURLConnection conn = null;
		
		try{
			
			url = new URL(urlString);
			// helper  new HttpServiceHelper(spid,url);
			request = new GetMethod(url.getPath());
			
			if(nameValuePairMap != null && !nameValuePairMap.isEmpty()){
				nameValuePairs = new ArrayList<NameValuePair>();
				for(Entry<String,String> entry : nameValuePairMap.entrySet()){
					nameValuePairs.add(new NameValuePair(entry.getKey(),entry.getValue()));
				}
				request.setQueryString(nameValuePairs.toArray(new NameValuePair[nameValuePairs.size()]));
			}
			
			// setting headers in the request
			
			if(headerMap != null && !headerMap.isEmpty()){
				for(Entry<String,String> entry : headerMap.entrySet()){
					request.addRequestHeader(entry.getKey(),entry.getValue());
				}
			}
			
			
		}catch(MalformedURLException malformedURLExceptio){
			throw new RuntimeException("Malformed webservice URL [" + urlString +"] supplied.",malformedURLExceptio);
		}catch(Exception anyException){
			ExceptionUtils.printRootCauseStackTrace(anyException);
			throw new RuntimeException("Error during call to webservice URL [" + urlString +"] supplied.",anyException);
		}finally{
			
			
			
			if(request != null){
				request.releaseConnection();
			}
		}
		
		return output;
		
	}
	
	
	
	
	/**
	 * Rest Get Call URL String 
	 * 
	 * @param urlStr
	 * @return
	 * @throws IOException
	 */
	public static <T>T callHttpGet(String urlStr,Class<T> tagetClass) throws IOException {
		  BufferedReader rd = null;
		  HttpURLConnection conn = null;
		  StringBuilder sb = new StringBuilder();
		  T output = null;
		  try{
			  
			  URL url = new URL(urlStr);
			  conn = (HttpURLConnection) url.openConnection();
	
			  if (conn.getResponseCode() != 200) {
			    throw new IOException(conn.getResponseMessage());
			  }
	
			  // Buffer the result into a string
			  rd = new BufferedReader(
			      new InputStreamReader(conn.getInputStream()));
			  String line;
			  while ((line = rd.readLine()) != null) {
			    sb.append(line);
			  }
			  
			  output = JSONUtil.deserializeToObject(sb.toString(), tagetClass);
			 
		  }catch(IOException exception){
			  throw exception;
		  }finally{
			  if(rd != null)
				  rd.close();
				
			  if(conn != null)
				  conn.disconnect();
		  }
		  
		  return output;
	}
	
	/**
	 * Rest Get Call URL String 
	 * 
	 * @param urlStr
	 * @return
	 * @throws IOException
	 */
	public static <T>List<T> callHttpGet(String urlStr,Class<? extends Object> collectionClass,Class<T> tagetClass) throws IOException {
		  BufferedReader rd = null;
		  HttpURLConnection conn = null;
		  StringBuilder sb = new StringBuilder();
		  List<T> output = null;
		  try{
			  
			  URL url = new URL(urlStr);
			  conn = (HttpURLConnection) url.openConnection();
	
			  if (conn.getResponseCode() != 200) {
			    throw new IOException(conn.getResponseMessage());
			  }
	
			  // Buffer the result into a string
			  rd = new BufferedReader(
			      new InputStreamReader(conn.getInputStream()));
			  String line;
			  while ((line = rd.readLine()) != null) {
			    sb.append(line);
			  }
			  
			  if(collectionClass != null){
				  output = JSONUtil.deserializeToCollectionObject(sb.toString(),collectionClass, tagetClass);
			  }else{
				  T output1 = JSONUtil.deserializeToObject(sb.toString(), tagetClass);
				  output = new ArrayList<T>();
				  output.add(output1);
			  }
			 
		  }catch(IOException exception){
			  throw exception;
		  }finally{
			  if(rd != null)
				  rd.close();
				
			  if(conn != null)
				  conn.disconnect();
		  }
		  
		  return output;
	}
	
	
	
	/**
	 * Rest Post Call URL String 
	 * 
	 * @param urlStr
	 * @return
	 * @throws IOException
	 */
	public static String callHttpPost(String urlStr, String[] paramName,
				String[] paramVal) throws Exception {
				HttpURLConnection conn = null;
				BufferedReader rd = null;
				OutputStream out = null;
				Writer writer = null;
				StringBuilder sb = null;
		
				try{
				  URL url = new URL(urlStr);
				  conn = (HttpURLConnection) url.openConnection();
				  conn.setRequestMethod("POST");
				  conn.setDoOutput(true);
				  conn.setDoInput(true);
				  conn.setUseCaches(false);
				  conn.setAllowUserInteraction(false);
				  conn.setRequestProperty("Content-Type",
				      "application/x-www-form-urlencoded");
	
				  // Create the form content
				  out = conn.getOutputStream();
				  writer = new OutputStreamWriter(out, "UTF-8");
				  for (int i = 0; i < paramName.length; i++) {
				    writer.write(paramName[i]);
				    writer.write("=");
				    writer.write(URLEncoder.encode(paramVal[i], "UTF-8"));
				    writer.write("&");
				  }
				  writer.close();
				  out.close();
	
				  if (conn.getResponseCode() != 200) {
				    throw new IOException(conn.getResponseMessage());
				  }
	
				  // Buffer the result into a string
				  rd = new BufferedReader(
				      new InputStreamReader(conn.getInputStream()));
				  sb = new StringBuilder();
				  String line;
				  while ((line = rd.readLine()) != null) {
				    sb.append(line);
				  }
				
				}catch(Exception e){
					throw e;
				}finally{
					if(out != null){
						out.close();
					}
					if(writer != null){
						writer.close();
					}
					if(rd != null){  
						rd.close();
					}
					if(conn != null){	
					  conn.disconnect();
					}
				}
				  
				  
			  return sb.toString();
			}

}
