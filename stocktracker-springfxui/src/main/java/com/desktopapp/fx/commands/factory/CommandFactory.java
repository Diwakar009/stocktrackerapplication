package com.desktopapp.fx.commands.factory;

import com.desktopapp.fx.commands.*;
import com.desktopapp.fx.view.AppView;
import javafx.scene.Node;

/**
 * Created by diwakar009 on 10/3/2019.
 */
public class CommandFactory implements ICommandFactory{

    private static ICommandFactory INSTANCE;

    private CommandFactory(){
    }

    public static ICommandFactory getInstance(){
        if (INSTANCE == null)
        {
            synchronized(CommandFactory.class)
            {
                if (INSTANCE == null)
                    INSTANCE = new CommandFactory();
            }
        }
        return INSTANCE;
    }

    @Override
    public ICommand getCommand(Node view, String commandName) {
      switch (commandName){
            case CMD_ABOUT:
              return new AboutCommand();

            case CMD_EXIT:
              return new ExitCommand();

            case CMD_HELP:
              return new HelpCommand();

            case CMD_KEYBOARDSHORTCUT:
              return new KeyboardShortcutsCommand();

            case CMD_STATUSBAR:
              return new StatusBarCommand((AppView)view);

            case CMD_THEMES:
              return new ThemesCommand((AppView)view);

            case CMD_TIPOFTHEDAY:
              return new TipOfTheDayCommand((AppView)view);

            case CMD_PREFERENCE:
              return new PreferencesCommand((AppView)view);

             case CMD_STATICCODEDECODE:
              return new StaticCodeDecodeCommand((AppView)view);

             case CMD_APPUSER:
                  return new AppUserCommand((AppView)view);

             case CMD_STOCKMAINTENANCE:
                  return new StockMaintenanceCommand((AppView)view);

          case CMD_CONTRACTMAINTENANCE:
              return new ContractMaintenanceCommand((AppView)view);




             default: return new DefaultCommand((AppView)view);
      }
    }
}
