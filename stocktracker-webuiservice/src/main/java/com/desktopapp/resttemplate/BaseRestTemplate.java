package com.desktopapp.resttemplate;


import com.stocktracker.resource.util.ResourceUtil;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.Map;

/**
 * Created by diwakar009 on 24/11/2017.
 */
public class BaseRestTemplate extends RestTemplate{

    private boolean offline = false;

    public BaseRestTemplate() {
        super();
        offline = ResourceUtil.getConfiguration().getGlobalProperties().isOffline();
    }

    @Override
    public <T> T getForObject(String url, Class<T> responseType, Map<String, ?> urlVariables) throws RestClientException {

        if (offline) {
            System.out.println(url);
        }

        return super.getForObject(url, responseType, urlVariables);
    }

    @Override
    public <T> T postForObject(String url, Object request, Class<T> responseType, Object... uriVariables)
            throws RestClientException {

        if (offline) {
            // mock the call with the dummy data
            System.out.println(url);
        }

        return super.postForObject(url, request, responseType,uriVariables);
    }

    @Override
    public <T> T postForObject(URI url, Object request, Class<T> responseType) throws RestClientException {

        if (offline) {
            // mock the call with the dummy data
            System.out.println(url);
        }

        return super.postForObject(url, request, responseType);
    }
}
