package com.stocktracker.ws.web.api;

import com.stocktracker.domainobjs.json.DomainObjectList;
import com.stocktracker.domainobjs.json.StaticCodeDecodeDO;
import com.stocktracker.domainobjs.json.TableDOPage;
import com.stocktracker.domainobjs.json.TableRowMapDO;
import com.stocktracker.util.DateTimeUtil;
import com.stocktracker.ws.BaseUnitTest;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;

import com.stocktracker.util.JSONUtil;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.lang.reflect.Constructor;
import java.util.*;

/**
 * Unit tests for the GreetingController using Spring MVC Mocks.
 * 
 * These tests utilize the Spring MVC Mock objects to simulate sending actual
 * HTTP requests to the Controller component. This test ensures that the
 * RequestMappings are configured correctly. Also, these tests ensure that the
 * request and response bodies are serialized as expected.
 * 
 * @author Matt Warman
 */
@Transactional
public class StaticCodeDecodeControllerTest extends BaseUnitTest {

    @Test
    public void testGetCodeDataItemsList() throws Exception {

        String uri = "/api/staticCodeDecodes/CD_FILETYPE/EN_US";

        MvcResult result = mvc.perform(MockMvcRequestBuilders.get(uri)
                .accept(MediaType.APPLICATION_JSON)).andReturn();

        String content = result.getResponse().getContentAsString();
        int status = result.getResponse().getStatus();

        Assert.assertEquals("failure - expected HTTP status", 200, status);

        Assert.assertTrue(
                "failure - expected HTTP response body to have a value",
                content.trim().length() > 0);

        displayMessage(content);
    }

    @Test
    public void testGet() throws Exception{

        String uri = "/api/staticCodeDecodes/get/";

       StaticCodeDecodeDO staticCodeDecodeDO = new StaticCodeDecodeDO();
       staticCodeDecodeDO.setCodeName("CD_ACCESSLEVEL");
       staticCodeDecodeDO.setCodeValue("AD");
       staticCodeDecodeDO.setLanguage("EN_US");

       String jsonString = JSONUtil.serializeToJSON(staticCodeDecodeDO);

        MvcResult result = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON).content(jsonString)).andReturn();

        String content = result.getResponse().getContentAsString();
        int status = result.getResponse().getStatus();
        Gson gson = new GsonBuilder().setDateFormat(DateTimeUtil.UTC_FORMAT).setPrettyPrinting().create();
        JsonParser jp = new JsonParser();
        JsonElement je = jp.parse(content);
        String prettyJsonString = gson.toJson(je);

        Assert.assertEquals("failure - expected HTTP status", 200, status);

        displayMessage(prettyJsonString);

        StaticCodeDecodeDO sd =  (StaticCodeDecodeDO)JSONUtil.deserializeToObject(content,StaticCodeDecodeDO.class);

        StaticCodeDecodeDO stdo = gson.fromJson(je,StaticCodeDecodeDO.class);

        if(stdo != null) {
            Assert.assertEquals("Static Code Description :", "Admin", stdo.getCodeDesc());
        }

    }

    @Test
    public void testUpdate() throws  Exception{

        String uri = "/api/staticCodeDecodes/save/";

        StaticCodeDecodeDO staticCodeDecodeDO = new StaticCodeDecodeDO();
        staticCodeDecodeDO.setCodeName("CD_ACCESSLEVEL");
        staticCodeDecodeDO.setCodeValue("AD");
        staticCodeDecodeDO.setLanguage("EN_US");
        staticCodeDecodeDO.setCodeDesc("Sample Access Level");

        String jsonString = JSONUtil.serializeToJSON(staticCodeDecodeDO);

        MvcResult result = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON).content(jsonString)).andReturn();

        String content = result.getResponse().getContentAsString();
        int status = result.getResponse().getStatus();
        Gson gson = new GsonBuilder().setDateFormat(DateTimeUtil.UTC_FORMAT).setPrettyPrinting().create();
        JsonParser jp = new JsonParser();
        JsonElement je = jp.parse(content);
        String prettyJsonString = gson.toJson(je);

        Assert.assertEquals("failure - expected HTTP status", 201, status);

        displayMessage(content);

    }



    @Test
    public void testSave() throws Exception{

        String uri = "/api/staticCodeDecodes/save/";

        StaticCodeDecodeDO staticCodeDecodeDO = new StaticCodeDecodeDO();
        staticCodeDecodeDO.setCodeName("CD_ACCESSLEVEL1");
        staticCodeDecodeDO.setCodeValue("AD");
        staticCodeDecodeDO.setLanguage("EN_US");
        staticCodeDecodeDO.setCodeDesc("Sample Access Level");

        String jsonString = JSONUtil.serializeToJSON(staticCodeDecodeDO);

        MvcResult result = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON).content(jsonString)).andReturn();

        String content = result.getResponse().getContentAsString();
        int status = result.getResponse().getStatus();
        Gson gson = new GsonBuilder().setDateFormat(DateTimeUtil.UTC_FORMAT).setPrettyPrinting().create();
        JsonParser jp = new JsonParser();
        JsonElement je = jp.parse(content);
        String prettyJsonString = gson.toJson(je);

        Assert.assertEquals("failure - expected HTTP status", 201, status);

        displayMessage(content);

        uri = "/api/staticCodeDecodes/get/";

        staticCodeDecodeDO = new StaticCodeDecodeDO();
        staticCodeDecodeDO.setCodeName("CD_ACCESSLEVEL1");
        staticCodeDecodeDO.setCodeValue("AD");
        staticCodeDecodeDO.setLanguage("EN_US");

        jsonString = JSONUtil.serializeToJSON(staticCodeDecodeDO);

        result = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON).content(jsonString)).andReturn();

        content = result.getResponse().getContentAsString();
        status = result.getResponse().getStatus();
        gson = new GsonBuilder().setPrettyPrinting().create();
        jp = new JsonParser();
        je = jp.parse(content);
        prettyJsonString = gson.toJson(je);

        Assert.assertEquals("failure - expected HTTP status", 200, status);

        displayMessage(content);



    }

    @Test
    public void testDelete() throws Exception{
        String uri = "/api/staticCodeDecodes/delete/";

        StaticCodeDecodeDO staticCodeDecodeDO = new StaticCodeDecodeDO();
        staticCodeDecodeDO.setCodeName("CD_ACCESSLEVEL");
        staticCodeDecodeDO.setCodeValue("AD");
        staticCodeDecodeDO.setLanguage("EN_US");
        //staticCodeDecodeDO.setCodeDesc("Sample Access Level");

        String jsonString = JSONUtil.serializeToJSON(staticCodeDecodeDO);

        MvcResult result = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON).content(jsonString)).andReturn();

        String content = result.getResponse().getContentAsString();
        int status = result.getResponse().getStatus();
        Gson gson = new GsonBuilder().setDateFormat(DateTimeUtil.UTC_FORMAT).setPrettyPrinting().create();
        JsonParser jp = new JsonParser();
        JsonElement je = jp.parse(content);
        String prettyJsonString = gson.toJson(je);

        Assert.assertEquals("failure - expected HTTP status", 200, status);

        displayMessage(content);

    }

    @Test
    public void testDeleteList() throws Exception{
        String uri = "/api/staticCodeDecodes/deleteList/";

        StaticCodeDecodeDO staticCodeDecodeDO = new StaticCodeDecodeDO();
        staticCodeDecodeDO.setCodeName("CD_ACCESSLEVEL");
        staticCodeDecodeDO.setCodeValue("AD");
        staticCodeDecodeDO.setLanguage("EN_US");

        StaticCodeDecodeDO staticCodeDecodeDO1 = new StaticCodeDecodeDO();
        staticCodeDecodeDO1.setCodeName("CD_ACCESSLEVEL");
        staticCodeDecodeDO1.setCodeValue("RO");
        staticCodeDecodeDO1.setLanguage("EN_US");

        List<StaticCodeDecodeDO> staticCodeDecodeDOList = new ArrayList<StaticCodeDecodeDO>();
        staticCodeDecodeDOList.add(staticCodeDecodeDO);
        staticCodeDecodeDOList.add(staticCodeDecodeDO1);


        DomainObjectList objectList = new DomainObjectList();
        objectList.setList(staticCodeDecodeDOList);

        //staticCodeDecodeDO.setCodeDesc("Sample Access Level");

        String jsonString = JSONUtil.serializeToJSON(objectList);

        MvcResult result = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON).content(jsonString)).andReturn();

        String content = result.getResponse().getContentAsString();
        int status = result.getResponse().getStatus();
        Gson gson = new GsonBuilder().setDateFormat(DateTimeUtil.UTC_FORMAT).setPrettyPrinting().create();
        JsonParser jp = new JsonParser();
        JsonElement je = jp.parse(content);
        String prettyJsonString = gson.toJson(je);

        Assert.assertEquals("failure - expected HTTP status", 200, status);

        displayMessage(content);

    }

    @Test
    public void testListAll() throws Exception{

        String uri = "/api/staticCodeDecodes/listAll/";

        TableRowMapDO<String, String> rowMap = new TableRowMapDO<String, String>();
        rowMap.setSize("5");
        rowMap.setPage("1");

        String jsonString = JSONUtil.serializeToJSON(rowMap);

        MvcResult result = mvc.perform(MockMvcRequestBuilders.get(uri)
                .accept(MediaType.APPLICATION_JSON)).andReturn();

        String content = result.getResponse().getContentAsString();
        int status = result.getResponse().getStatus();
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        JsonParser jp = new JsonParser();
        JsonElement je = jp.parse(content);
        String prettyJsonString = gson.toJson(je);

        Assert.assertEquals("failure - expected HTTP status", 200, status);

        displayMessage(content);

    }

    @Test
    public void testListAllTableRowObjectConstruction() throws Exception{

        String uri = "/api/staticCodeDecodes/listAll/";

        TableRowMapDO<String, String> rowMap = new TableRowMapDO<String, String>();
        rowMap.setSize("2");
        rowMap.setPage("1");

        String jsonString = JSONUtil.serializeToJSON(rowMap);

        MvcResult result = mvc.perform(MockMvcRequestBuilders.get(uri)
                .accept(MediaType.APPLICATION_JSON)).andReturn();

        String content = result.getResponse().getContentAsString();
        int status = result.getResponse().getStatus();
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        JsonParser jp = new JsonParser();
        JsonElement je = jp.parse(content);
        String prettyJsonString = gson.toJson(je);

        Assert.assertEquals("failure - expected HTTP status", 200, status);

        displayMessage(content);
        TableDOPage page =  (TableDOPage)JSONUtil.deserializeToObject(content,TableDOPage.class);
        List<TableRowMapDO<String,String>> rows = page.getList();

        for(TableRowMapDO<String,String> row : rows){

            String clazzName = row.getClazzName();
            Class<?> clazz = Class.forName(clazzName);
            Constructor<?> ctor = clazz.getConstructor();
            Object object = ctor.newInstance(new Object[] {});

            StaticCodeDecodeDO stdo = (StaticCodeDecodeDO) object;
            stdo.setCodeName(row.getValue("codeName"));
            stdo.setLanguage(row.getValue("language"));
            stdo.setCodeDesc(row.getValue("codeDesc"));
            stdo.setCodeValue(row.getValue("codeValue"));

            displayMessage(stdo.getId().toString());
        }
    }

    @Test
    public void testListWithFilter() throws Exception {
        String uri = "/api/staticCodeDecodes/listWithFilter/";

        TableRowMapDO<String, String> rowMap = new TableRowMapDO<String, String>();
        rowMap.setSize("5");
        rowMap.setPage("1");

        rowMap.addKeyValue("codeName", "%A%");
        rowMap.addKeyValue("codeValue", "%A%");
        rowMap.addKeyValue("codeDesc", "%A%");

        String jsonString = JSONUtil.serializeToJSON(rowMap);

        MvcResult result = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON).content(jsonString)).andReturn();

        String content = result.getResponse().getContentAsString();
        int status = result.getResponse().getStatus();
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        JsonParser jp = new JsonParser();
        JsonElement je = jp.parse(content);
        String prettyJsonString = gson.toJson(je);

        Assert.assertEquals("failure - expected HTTP status", 200, status);
    }



}
