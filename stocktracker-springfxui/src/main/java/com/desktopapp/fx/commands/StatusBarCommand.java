package com.desktopapp.fx.commands;

import com.desktopapp.fx.control.StatusBar;
import com.desktopapp.fx.view.AppView;

/**
 * Created by diwakar009 on 24/12/2018.
 */
public class StatusBarCommand implements ICommand{

    private AppView view;

    public StatusBarCommand(AppView view){
       this.view = view;
    }

    public void onToggleStatusBar() {
        StatusBar statusBar = view.getStatusBar();
        statusBar.setVisible(!statusBar.isVisible());
        if (statusBar.isVisible()) {
            view.setBottom(statusBar);
        } else {
            view.setBottom(null);
        }
    }

    @Override
    public void executeCommand() {
        onToggleStatusBar();
    }
}
