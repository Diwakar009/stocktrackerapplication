/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.jpa.spring.extended;

import com.stocktracker.jpa.spring.extended.custom.ExtendedRepository;
import com.stocktracker.model.sms.Industry;
import com.stocktracker.model.sms.IndustryPK;
import org.springframework.stereotype.Repository;

@Repository
public interface IndustryRepository extends ExtendedRepository<Industry, IndustryPK> {

}
