/*
 * Copyright (C) 2017 Cem Ikta
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 *
 */

package com.desktopapp.fx.view;


import com.desktopapp.fx.DesktopFxApplication;
import com.desktopapp.fx.app.AppAccelerator;
import com.desktopapp.fx.app.AppFontIcons;
import com.desktopapp.fx.config.RibbonMenuItem;
import com.desktopapp.fx.config.manager.ApplicationViewConfigManager;
import com.desktopapp.fx.control.*;
import com.desktopapp.fx.control.fonticon.FontIconFactory;
import com.desktopapp.fx.manager.ApplicationViewManager;
import com.desktopapp.fx.mvc.DataPageView;
import com.desktopapp.fx.mvc.View;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Application view
 *
 * @author diwakar009
 */
public class AppView extends BorderPane {

    private final static Logger logger = LoggerFactory.getLogger(AppView.class);
    private BorderPane appCenter;
    private StatusBar statusBar;
    private Map<String,RibbonMenuItem> menuRibbonMap = new LinkedHashMap<String,RibbonMenuItem>();
    private ApplicationViewConfigManager applicationViewConfigManager;
    private ApplicationViewManager applicationViewManager;
    private static ApplicationInfoPane applicationInfoPane;

    public AppView(ApplicationViewManager applicationViewManager) {
        super();
        this.applicationViewManager=applicationViewManager;
        applicationViewConfigManager = new ApplicationViewConfigManager(this);
        applicationInfoPane = new ApplicationInfoPane();
        buildView();
    }

    private void buildView() {
        getStyleClass().add("app-view");
        setPadding(new Insets(10, 0, 0, 0));
        applicationViewConfigManager.overrideProperties();
        setTop(buildRibbonMenu());
        setCenter(buildAppCenter());
        setBottom(buildStatusBar());
    }

    public StatusBar getStatusBar() {
        return statusBar;
    }

    public void addPageToCenter(View page) {
        logger.info("Add page to center with page {}", page);
        appCenter.setLeft(null);
        appCenter.setRight(null);
        appCenter.setTop(applicationInfoPane);
        appCenter.setCenter(page.asNode());
        appCenter.setBottom(null);

        if (page instanceof DataPageView) {
            ((DataPageView) page).setDataPageAccelerators();
        }
        page.asNode().requestFocus();
    }

    private BorderPane buildAppCenter() {
        appCenter = new BorderPane();
        appCenter.setId("app-center");
        return appCenter;
    }

    private RibbonPane buildRibbonMenu() {
        List<RibbonTab> tabs = new ArrayList<RibbonTab>();
        menuRibbonMap.entrySet().forEach(entry -> {
            RibbonGroup group = new RibbonGroup();
            entry.getValue().getRibbonItems().forEach(ribbon -> {
                group.addRibbon(ribbon);
            });
            tabs.add(new RibbonTab(entry.getValue().getMenuDisplayText(),group));
        });

       RibbonPane pane = new RibbonPane(tabs.toArray(new RibbonTab[tabs.size()]));
       return pane;

    }

    private StatusBar buildStatusBar() {
        Label loggedInUser = new Label(DesktopFxApplication.get().getLoginSession().getLoggedInUser().getUserName(),
                FontIconFactory.createIcon(AppFontIcons.APP_USER));

        SimpleDateFormat sdf = new SimpleDateFormat("EEEE', 'dd. MMMM yyyy");
        Label currentDate = new Label(sdf.format(new Date()), FontIconFactory.createIcon(AppFontIcons.APP_DATE));

        statusBar = new StatusBar();
        statusBar.getLeftItems().addAll(loggedInUser, currentDate);
        return statusBar;
    }

    public void activateRibbonAccelerators() {

        AppAccelerator appAccelerator = DesktopFxApplication.get().getLoginSession().getAppAccelerator();
        menuRibbonMap.forEach((key, value) -> {
            value.getRibbonItems().forEach(ribbon -> {
                if(ribbon instanceof ActionCommand){
                    //getScene().getAccelerators().put(((ActionCommand) ribbon).getKeyCodeCombination(), ((ActionCommand) ribbon)::fire);
                    appAccelerator.activateAccelerator(getScene(),((ActionCommand) ribbon).getKeyCodeCombination(), ((ActionCommand) ribbon)::fire);
                }
            });
        });
    }

     public void setMenuRibbonMap(Map<String, RibbonMenuItem> menuRibbonMap) {
        this.menuRibbonMap = menuRibbonMap;
    }

    public ApplicationViewManager getApplicationViewManager() {
        return applicationViewManager;
    }

    public static ApplicationInfoPane getApplicationInfoPane() {
        return applicationInfoPane;
    }
}
