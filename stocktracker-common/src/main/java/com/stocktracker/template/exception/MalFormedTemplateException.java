/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.template.exception;

public class MalFormedTemplateException extends TemplateException{

	public MalFormedTemplateException(String exceptionMessage) {
		super(exceptionMessage);
		// TODO Auto-generated constructor stub
	}

	public MalFormedTemplateException(String exceptionMessage, Throwable cause) {
		super(exceptionMessage, cause);
		// TODO Auto-generated constructor stub
	}
	

}
