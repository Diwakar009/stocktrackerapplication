/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.model.sms;

import com.stocktracker.framework.IEntity;
import com.stocktracker.model.BaseEntity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * The persistent class for the TBL_TRANSPORT_DTLS database table.
 * 
 */
@Entity
@Table(name = "TBL_DELIVERY_DTLS")
@AttributeOverride(name = "id", column = @Column(name = "ID", nullable = false,
		columnDefinition = "BIGINT UNSIGNED"))
public class DeliveryDetails extends BaseEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "ID", nullable = false, columnDefinition = "BIGINT UNSIGNED")
	private Long id;

	/*@Column(name="INDUSTRY_CODE", length = 5)
	private String industryCode;

	@Column(name="STOCK_CODE", length = 5)
	private String stockCode;

	@Column(name="STOCK_VARIETIES_CODE", length = 5)
	private String stockVarietyCode;*/

	@Column(name="DELIVERY_TYPE_CODE", length = 5)
	private String deliveryTypeCode;

	@Column(name="DELIVERY_DEPO", length = 5)
	private String deliveryDepo;

	@Column(name="DELIVERY_LAT_NO", nullable = true, columnDefinition = "BIGINT UNSIGNED")
	private Long deliveryLatNo;

	@Column(name="DELIVERY_ADDRESS",length = 100)
	private String deliveryAddress;

	@Column(name="MORE_DETAILS", length = 65535)
	private String moreDetails;

	@Override
	public Serializable getEntityPK() {
		return getId();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	/*public String getIndustryCode() {
		return industryCode;
	}

	public void setIndustryCode(String industryCode) {
		this.industryCode = industryCode;
	}

	public String getStockCode() {
		return stockCode;
	}

	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}

	public String getStockVarietyCode() {
		return stockVarietyCode;
	}

	public void setStockVarietyCode(String stockVarietyCode) {
		this.stockVarietyCode = stockVarietyCode;
	}*/

	public String getDeliveryTypeCode() {
		return deliveryTypeCode;
	}

	public void setDeliveryTypeCode(String deliveryTypeCode) {
		this.deliveryTypeCode = deliveryTypeCode;
	}

	public String getDeliveryDepo() {
		return deliveryDepo;
	}

	public void setDeliveryDepo(String deliveryDepo) {
		this.deliveryDepo = deliveryDepo;
	}

	public Long getDeliveryLatNo() {
		return deliveryLatNo;
	}

	public void setDeliveryLatNo(Long deliveryLatNo) {
		this.deliveryLatNo = deliveryLatNo;
	}

	public String getDeliveryAddress() {
		return deliveryAddress;
	}

	public void setDeliveryAddress(String deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}

	public String getMoreDetails() {
		return moreDetails;
	}

	public void setMoreDetails(String moreDetails) {
		this.moreDetails = moreDetails;
	}

	@Override
	public void copyEntity(IEntity entity) {
		DeliveryDetails source = (DeliveryDetails)entity;
		this.setCreatedBy(source.getCreatedBy());
		this.setUpdatedBy(source.getUpdatedBy());
		this.setCreatedAt(source.getCreatedAt());
		this.setUpdatedAt(source.getUpdatedAt());

		this.setId(source.getId());
		/*this.setIndustryCode(source.getIndustryCode());
		this.setStockCode(source.getStockCode());
		this.setStockVarietyCode(source.getStockVarietyCode());*/
		this.setDeliveryTypeCode(source.getDeliveryTypeCode());
		this.setDeliveryDepo(source.getDeliveryDepo());
		this.setDeliveryLatNo(source.getDeliveryLatNo());
		this.setDeliveryAddress(source.getDeliveryAddress());
		this.setMoreDetails(source.getMoreDetails());
	}
}