/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.desktopapp.fx.control;

import com.desktopapp.fx.common.IGenericComponent;
import com.desktopapp.fx.common.Quantity;
import javafx.scene.Node;
import javafx.scene.layout.HBox;

import java.math.BigDecimal;


/**
 * Quantity field for number of bags and total weightment.
 *
 * Created by diwakar009 on 3/12/2018.
 */
public class GunnyBagQuantityField extends QuantityField implements IGenericComponent{

    public static BigDecimal ONE_BELL_EQUALS_BAGS = new BigDecimal(500);
    public static String GUNNY_BAGS_CODE = "GUBA";
    public static String GUNNY_BAG_UNIT = "BAG";
    public static String GUNNY_BAG_WEIGHTMENT_UNIT = "BAIL";

    public GunnyBagQuantityField() {
        super();
        init();
    }

    public GunnyBagQuantityField(Quantity quantity) {
        super(quantity);
        init();
    }

    public void init(){
        this.ifNoOfPackets = new IntegerField();
        this.ifNoOfPackets.setPrefWidth(100);

        this.bfWeightment = new BigDecimalField(2);
        this.bfWeightment.setPrefWidth(100);

        ifNoOfPackets.addValueChangeListener(() -> {
            Long noOfPackets = ifNoOfPackets.getValue();

            if(noOfPackets != null) {
                bfWeightment.setDataValue(new BigDecimal(noOfPackets.longValue()).divide(ONE_BELL_EQUALS_BAGS));
            }
        });
    }

    public Node asControl(){
        HBox hBox = new HBox();
        this.ifNoOfPackets.setPromptText(GUNNY_BAG_UNIT);
        hBox.getChildren().add(this.ifNoOfPackets);
        this.bfWeightment.setPromptText(GUNNY_BAG_WEIGHTMENT_UNIT);
        hBox.getChildren().add(this.bfWeightment);
        hBox.setSpacing(0);
        return hBox;
    }

}
