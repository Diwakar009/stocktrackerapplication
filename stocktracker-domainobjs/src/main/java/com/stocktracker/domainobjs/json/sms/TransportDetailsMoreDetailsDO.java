/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.domainobjs.json.sms;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.Gson;

import java.io.Serializable;

/**
 * TransportDetailsExtraInfoDO .
 *
 */
@JsonIgnoreProperties(ignoreUnknown=true)
@JsonInclude(Include.NON_EMPTY)
public class TransportDetailsMoreDetailsDO implements Serializable {
	private static final long serialVersionUID = 1L;
	public static String CLASS_NAME = TransportDetailsMoreDetailsDO.class.getName();

	@JsonProperty("ownVehicleCode")
	private String ownVehicleCode;


	public TransportDetailsMoreDetailsDO() {
		ownVehicleCode = new String();
	}

	public String getOwnVehicleCode() {
		return ownVehicleCode;
	}

	public void setOwnVehicleCode(String ownVehicleCode) {
		this.ownVehicleCode = ownVehicleCode;
	}

	public String toJSON(){
		return  new Gson().toJson(this);
	}
	public TransportDetailsMoreDetailsDO fromJSON(String json){
		return  new Gson().fromJson(json, TransportDetailsMoreDetailsDO.class);
	}
}

