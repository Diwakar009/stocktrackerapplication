package com.desktopapp.fx.css;

import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
/**
 *
 * https://docs.oracle.com/cd/E17802_01/javafx/javafx/1.3/docs/api/javafx.scene/doc-files/cssref.html#typecolor
 *
 * @author jojorabbit
 */
public class CssTest001 extends ExtendedApplication {
    StackPane cssNode;
 
    public static void main(final String[] args) {
        launch(CssTest001.class, args);
    }
    @Override
    protected void setup() {
 
        cssNode = new StackPane();
        cssNode.getStyleClass().add("custom-node");
        cssNode.setPrefSize(200, 200);

        Button button1 = new Button("Press Me...");
        button1.getStyleClass().add("button");

        cssNode.getChildren().add(button1);

        root.getChildren().add(new Group(cssNode)); // adding to group so root does not resize custom node
    }
    @Override
    protected String getAppTitle() {
        return "CSS Test 001";
    }
    @Override
    protected void setupCss() {
        scene.getStylesheets().add("csstest001.css");
    }
 
}