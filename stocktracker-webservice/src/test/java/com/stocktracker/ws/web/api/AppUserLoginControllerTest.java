package com.stocktracker.ws.web.api;

import com.stocktracker.domainobjs.json.AppUserDO;
import com.stocktracker.domainobjs.json.AppUserLoginDO;
import com.stocktracker.domainobjs.json.DomainObjectList;
import com.stocktracker.domainobjs.json.TableRowMapDO;
import com.stocktracker.model.AppUserLogin;
import com.stocktracker.util.DateTimeUtil;
import com.stocktracker.util.JSONUtil;
import com.stocktracker.util.PasswordHash;
import com.stocktracker.ws.BaseUnitTest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;

/**
 * Unit tests for the GreetingController using Spring MVC Mocks.
 * 
 * These tests utilize the Spring MVC Mock objects to simulate sending actual
 * HTTP requests to the Controller component. This test ensures that the
 * RequestMappings are configured correctly. Also, these tests ensure that the
 * request and response bodies are serialized as expected.
 * 
 * @author Matt Warman
 */
@Transactional
public class AppUserLoginControllerTest extends BaseUnitTest {


    @Test
    public void testGet() throws Exception{

        String uri = "/api/appUserLogin/get/";

        AppUserLoginDO appUserLoginDO = new AppUserLoginDO();
        appUserLoginDO.setId(new Long(2));


        String jsonString = JSONUtil.serializeToJSON(appUserLoginDO);

        MvcResult result = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON).content(jsonString)).andReturn();

        String content = result.getResponse().getContentAsString();
        int status = result.getResponse().getStatus();
        Gson gson = new GsonBuilder().setDateFormat(DateTimeUtil.UTC_FORMAT).setPrettyPrinting().create();
        JsonParser jp = new JsonParser();
        JsonElement je = jp.parse(content);
        String prettyJsonString = gson.toJson(je);

        //Assert.assertEquals("failure - expected HTTP status", 200, status);

        displayMessage(prettyJsonString);

        AppUserLoginDO sd =  (AppUserLoginDO)JSONUtil.deserializeToObject(content,AppUserLoginDO.class);

        AppUserLoginDO stdo = gson.fromJson(je,AppUserLoginDO.class);

        //Assert.assertEquals("User Name :", "varsha009", stdo.getUserName());

    }

    @Test
    public void testUpdate() throws  Exception{


        String uri = "/api/appUserLogin/get/";

        AppUserLoginDO appUserLoginDO = new AppUserLoginDO();
        appUserLoginDO.setId(new Long(2));


        String jsonString = JSONUtil.serializeToJSON(appUserLoginDO);

        MvcResult result = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON).content(jsonString)).andReturn();

        String content = result.getResponse().getContentAsString();
        int status = result.getResponse().getStatus();
        Gson gson = new GsonBuilder().setDateFormat(DateTimeUtil.UTC_FORMAT).setPrettyPrinting().create();
        JsonParser jp = new JsonParser();
        JsonElement je = jp.parse(content);
        String prettyJsonString = gson.toJson(je);

        //Assert.assertEquals("failure - expected HTTP status", 200, status);

        displayMessage(prettyJsonString);

        AppUserLoginDO sd =  (AppUserLoginDO)JSONUtil.deserializeToObject(content,AppUserLoginDO.class);

        AppUserLoginDO stdo = gson.fromJson(je,AppUserLoginDO.class);

        stdo.setUserName("Chetana009");

        uri = "/api/appUserLogin/save/";

        jsonString = JSONUtil.serializeToJSON(stdo);

        result = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON).content(jsonString)).andReturn();

        content = result.getResponse().getContentAsString();
        status = result.getResponse().getStatus();
        gson = new GsonBuilder().setDateFormat(DateTimeUtil.UTC_FORMAT).setPrettyPrinting().create();
        jp = new JsonParser();
        je = jp.parse(content);
        prettyJsonString = gson.toJson(je);

        //Assert.assertEquals("failure - expected HTTP status", 201, status);

        displayMessage(content);

    }



    @Test
    public void testSave() throws Exception{

        String uri = "/api/appUserLogin/save/";

        AppUserDO appUserDO = new AppUserDO();
        appUserDO.setId(2l);

        AppUserLoginDO appUserLoginDO = new AppUserLoginDO();
        appUserLoginDO.setUserName("Sample009");
        appUserLoginDO.setPassword(PasswordHash.createHash("sample009"));
        appUserLoginDO.setFnAccessLevel("AD");
        appUserLoginDO.setSecQuestion("XYZ");
        appUserLoginDO.setSecQuestionAns("XYZ");
        appUserLoginDO.setAppUser(appUserDO);

        String jsonString = JSONUtil.serializeToJSON(appUserLoginDO);

        MvcResult result = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON).content(jsonString)).andReturn();

        String content = result.getResponse().getContentAsString();
        int status = result.getResponse().getStatus();
        Gson gson = new GsonBuilder().setDateFormat(DateTimeUtil.UTC_FORMAT).setPrettyPrinting().create();
        JsonParser jp = new JsonParser();
        JsonElement je = jp.parse(content);
        String prettyJsonString = gson.toJson(je);

        //Assert.assertEquals("failure - expected HTTP status", 201, status);

        displayMessage(content);

        AppUserLoginDO sd =  (AppUserLoginDO)JSONUtil.deserializeToObject(content,AppUserLoginDO.class);

        AppUserLoginDO stdo = gson.fromJson(je,AppUserLoginDO.class);


        uri = "/api/appUserLogin/get/";


        jsonString = JSONUtil.serializeToJSON(stdo);

        result = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON).content(jsonString)).andReturn();

        content = result.getResponse().getContentAsString();
        status = result.getResponse().getStatus();
        gson = new GsonBuilder().setPrettyPrinting().create();
        jp = new JsonParser();
        je = jp.parse(content);
        prettyJsonString = gson.toJson(je);

        //Assert.assertEquals("failure - expected HTTP status", 200, status);

        displayMessage(content);

    }

    @Test
    public void testDelete() throws Exception{
        String uri = "/api/appUserLogin/delete/";

        AppUserLoginDO appUserLoginDO = new AppUserLoginDO();
        appUserLoginDO.setId(new Long(2));

        String jsonString = JSONUtil.serializeToJSON(appUserLoginDO);

        MvcResult result = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON).content(jsonString)).andReturn();

        String content = result.getResponse().getContentAsString();
        int status = result.getResponse().getStatus();
        Gson gson = new GsonBuilder().setDateFormat(DateTimeUtil.UTC_FORMAT).setPrettyPrinting().create();
        JsonParser jp = new JsonParser();
        JsonElement je = jp.parse(content);
        String prettyJsonString = gson.toJson(je);

        //Assert.assertEquals("failure - expected HTTP status", 200, status);

        displayMessage(content);

    }

    @Test
    public void testDeleteList() throws Exception{
        String uri = "/api/appUserLogin/deleteList/";

        AppUserLoginDO appUserLoginDO1 = new AppUserLoginDO();
        appUserLoginDO1.setId(new Long(2));


        AppUserLoginDO appUserLoginDO2 = new AppUserLoginDO();
        appUserLoginDO2.setId(new Long(3));


        List<AppUserLoginDO> appUserLoginDOArrayList = new ArrayList<AppUserLoginDO>();
        appUserLoginDOArrayList.add(appUserLoginDO1);
        appUserLoginDOArrayList.add(appUserLoginDO2);


        DomainObjectList objectList = new DomainObjectList();
        objectList.setList(appUserLoginDOArrayList);

        //staticCodeDecodeDO.setCodeDesc("Sample Access Level");

        String jsonString = JSONUtil.serializeToJSON(objectList);

        MvcResult result = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON).content(jsonString)).andReturn();

        String content = result.getResponse().getContentAsString();
        int status = result.getResponse().getStatus();
        Gson gson = new GsonBuilder().setDateFormat(DateTimeUtil.UTC_FORMAT).setPrettyPrinting().create();
        JsonParser jp = new JsonParser();
        JsonElement je = jp.parse(content);
        String prettyJsonString = gson.toJson(je);

        //Assert.assertEquals("failure - expected HTTP status", 200, status);

        displayMessage(content);

    }

    @Test
    public void testListAll() throws Exception{

        String uri = "/api/appUserLogin/listAll/";

        TableRowMapDO<String, String> rowMap = new TableRowMapDO<String, String>();
        rowMap.setSize("5");
        rowMap.setPage("1");

        String jsonString = JSONUtil.serializeToJSON(rowMap);

        MvcResult result = mvc.perform(MockMvcRequestBuilders.get(uri)
                .accept(MediaType.APPLICATION_JSON)).andReturn();

        String content = result.getResponse().getContentAsString();
        int status = result.getResponse().getStatus();
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        JsonParser jp = new JsonParser();
        JsonElement je = jp.parse(content);
        String prettyJsonString = gson.toJson(je);

        //Assert.assertEquals("failure - expected HTTP status", 200, status);

        displayMessage(content);

    }



    @Test
    public void testListWithFilter() throws Exception {
        String uri = "/api/appUser/listWithFilter/";

        TableRowMapDO<String, String> rowMap = new TableRowMapDO<String, String>();
        rowMap.setSize("5");
        rowMap.setPage("1");

        rowMap.addKeyValue("firstName", "%di%");
        rowMap.addKeyValue("lastName", "%di%");
        rowMap.addKeyValue("shortName", "%di%");
        rowMap.addKeyValue("phone", "%di%");
        rowMap.addKeyValue("email", "%di%");
        rowMap.addKeyValue("userName", "%di%");

        String jsonString = JSONUtil.serializeToJSON(rowMap);

        MvcResult result = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON).content(jsonString)).andReturn();

        String content = result.getResponse().getContentAsString();
        int status = result.getResponse().getStatus();
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        JsonParser jp = new JsonParser();
        JsonElement je = jp.parse(content);
        String prettyJsonString = gson.toJson(je);



        //Assert.assertEquals("failure - expected HTTP status", 200, status);

        displayMessage(prettyJsonString);
    }



}
