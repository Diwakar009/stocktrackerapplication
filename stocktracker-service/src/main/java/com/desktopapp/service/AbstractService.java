package com.desktopapp.service;


import com.stocktracker.jpa.spring.extended.custom.ExtendedRepository;
import com.stocktracker.model.BaseEntity;
import com.stocktracker.domainobjs.IDomainObject;
import com.stocktracker.domainobjs.json.DOPage;
import com.stocktracker.domainobjs.json.TableRowMapDO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

import java.io.OutputStream;
import java.util.*;

/**
 * Abstract Service for all services.
 *
 * @param <T> entity
 * @author Ratnala Diwakar Choudhury
 */
@Service
@EnableTransactionManagement
@Transactional
public abstract class AbstractService<T extends IDomainObject> {

    private final Class<T> domainObjectClass;

    /**
     * Create default service
     */
    public AbstractService(Class<T> domainObjectClass) {
        this.domainObjectClass = domainObjectClass;
        //repository = new StandaloneRepository(getEntityClass());
    }


    /**
     * Create entity.
     *
     * @param domainObject entity model.
     * @return created entity
     */
    /*public IDomainObject create(T domainObject) {
        Optional storedEntity = getRepository().findById(returnEntity(domainObject));
        BaseEntity entity = null;
        if(storedEntity.isPresent()){
            ((BaseEntity)storedEntity.get()).copyEntity(returnEntity(domainObject));
            entity = (BaseEntity) getRepository().save(storedEntity);
        }else {
            entity = (BaseEntity) getRepository().save(returnEntity(domainObject));
        }
        return returnDomainObject(entity);
    }*/
    @Transactional
    public IDomainObject create(T domainObject) {
        BaseEntity entity = (BaseEntity)getRepository().save(returnEntity(domainObject));
        return returnDomainObject(entity);
    }

    /**
     * Create entities.
     *
     * @param domainObjects entity model.
     * @return created entity
     */
    @Transactional
    public List<IDomainObject> create(List<T> domainObjects) {

        List<IDomainObject> iDomainObjects = new ArrayList<IDomainObject>();
        domainObjects.forEach((domainObject) -> {
            BaseEntity entity = (BaseEntity)getRepository().save(returnEntity(domainObject));
            iDomainObjects.add(returnDomainObject(entity));
        });

        return iDomainObjects;
    }

    /**
     * Update entity.
     *
     * @param domainObject entity model
     * @return updated entity
     */
    @Transactional
    public IDomainObject update(T domainObject) {

      /*  Preferences entity = (Preferences)getRepository().getOne(returnEntity(domainObject));
        entity.copyEntity(returnEntity(domainObject));*/
        BaseEntity updatedEntity = (BaseEntity)getRepository().save(returnEntity(domainObject));
        return returnDomainObject(updatedEntity);
    }

    /**
     * Remove entity.
     *
     * @param domainObject entity model
     */
    @Transactional
    public void remove(T domainObject) {
        getRepository().delete(returnEntity(domainObject));
    }

    /**
     * Remove entities.
     *
     * @param domainObjects entity model
     */
    @Transactional
    public void remove(List<T> domainObjects) {
        getRepository().deleteAll(returnEntities(domainObjects));
    }

    /**
     * Find entity.
     *
     * @param id entity id
     * @return entity
     */
    @Transactional
    public T find(Long id) {
       /* return returnDomainObject((BaseEntity) getRepository().getOne(id));*/

        Optional<BaseEntity> updatedEntity = (Optional<BaseEntity>)getRepository().findById(id);
        if(updatedEntity.isPresent()){
            return returnDomainObject(updatedEntity.get());
        }

        return null;
    }

    @Transactional
    public T find(T domainObject) {
        BaseEntity entity = returnEntity(domainObject);
        if (entity != null && entity.getEntityPK() != null) {
            //BaseEntity updatedEntity = (BaseEntity)getRepository().getOne(entity.getEntityPK());
            Optional<BaseEntity> updatedEntity = (Optional<BaseEntity>)getRepository().findById(entity.getEntityPK());
            if(updatedEntity.isPresent()){
                return returnDomainObject(updatedEntity.get());
            }
        }

        return null;
    }

    @Transactional
    public DOPage<TableRowMapDO<String, String>> listAll(Map<String, Object> parameters) {
        DOPage<TableRowMapDO<String, String>> doPage = new DOPage<TableRowMapDO<String, String>>();
        int page = Integer.valueOf((String) parameters.get("page"));
        int size = Integer.valueOf((String) parameters.get("size"));
        boolean isPagination;
        PageRequest pageRequest = null;
        if(parameters.get("isPagination") != null){
            isPagination = (Boolean) parameters.get("isPagination");
        }else{
            isPagination = true;
        }
        if(isPagination){
            pageRequest = PageRequest.of(page,size);
        }
        Page<? extends BaseEntity> pagedData = findAll(parameters,pageRequest);//getRepository().findAll(new PageRequest(page,size));
        copyPageToDOPageSettings(pagedData, doPage);
        List<? extends IDomainObject> domainObjects = returnDomainObjects(pagedData.getContent());

        if (domainObjects != null) {
            for (IDomainObject domainObject : domainObjects) {
                doPage.getList().add(copyDomainObjToTableRowMap(domainObject));
            }
        }

        return doPage;
    }

    @Transactional
    public DOPage<TableRowMapDO<String, String>> listByFilter(Map<String, Object> parameters) {
        DOPage<TableRowMapDO<String, String>> doPage = new DOPage<TableRowMapDO<String, String>>();
        int page = Integer.valueOf((String) parameters.get("page"));
        int size = Integer.valueOf((String) parameters.get("size"));
        boolean isPagination;
        PageRequest pageRequest = null;
        if(parameters.get("isPagination") != null){
            isPagination = (Boolean) parameters.get("isPagination");
        }else{
            isPagination = true;
        }
        if(isPagination){
            pageRequest = PageRequest.of(page,size);
        }
        Page<? extends BaseEntity> pagedData = findByFilter(parameters,pageRequest); // getRepository().findAll(new PageRequest(page,size));
        copyPageToDOPageSettings(pagedData, doPage);
        List<? extends IDomainObject> domainObjects = returnDomainObjects(pagedData.getContent());
        if (domainObjects != null) {
            for (IDomainObject domainObject : domainObjects) {
                doPage.getList().add(copyDomainObjToTableRowMap(domainObject));
            }
        }
        return doPage;
    }

    /**
     * Copy Page Settings to DOPage
     *
     * @param sourcePage
     * @param destPage
     */
    protected void copyPageToDOPageSettings(Page<?> sourcePage, DOPage<?> destPage) {
        destPage.setFirst(sourcePage.isFirst());
        destPage.setLast(sourcePage.isLast());
        destPage.setNumber(sourcePage.getNumber());
        destPage.setNumberOfElements(sourcePage.getNumberOfElements());
        destPage.setSize(sourcePage.getSize());
        destPage.setTotalPages(sourcePage.getTotalPages());
        destPage.setTotalElements(sourcePage.getTotalElements());
    }

    /**
     * Returns list of objects using Ids
     *
     * @param ids
     * @return
     */
    @Transactional
    public List<IDomainObject> getListWithIds(List<String> ids) {
        List<IDomainObject> entityList = new ArrayList<IDomainObject>();

        for (String id : ids) {

            IDomainObject e = find(Long.valueOf(id));

            if (e != null) {
                entityList.add(e);
            }
        }
        return entityList;
    }



    public abstract Class<? extends BaseEntity> getEntityClass();

    public abstract T returnDomainObject(BaseEntity entity);

    public abstract List<T> returnDomainObjects(List<? extends BaseEntity> entities);

    public abstract BaseEntity returnEntity(T domainObject);

    public abstract List<? extends BaseEntity> returnEntities(List<T> domainObjects);

    public abstract TableRowMapDO<String, String> copyDomainObjToTableRowMap(IDomainObject domainObject);

    public abstract ExtendedRepository getRepository();

    public Page<? extends BaseEntity> findByFilter(Map<String, Object> parameters,PageRequest pageRequest){
        return getRepository().findAll(pageRequest);
    }

    public Page<? extends BaseEntity> findAll(Map<String, Object> parameters,PageRequest pageRequest){
        return getRepository().findAll(pageRequest);
    }

}
