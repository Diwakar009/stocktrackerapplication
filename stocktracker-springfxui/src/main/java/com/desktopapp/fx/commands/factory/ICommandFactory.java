package com.desktopapp.fx.commands.factory;

import com.desktopapp.fx.commands.AppUserCommand;
import com.desktopapp.fx.commands.ICommand;
import javafx.scene.Node;

/**
 * Created by diwakar009 on 10/3/2019.
 */
public interface ICommandFactory {
    String CMD_ABOUT = "AboutCommand";
    String CMD_EXIT = "ExitCommand";
    String CMD_HELP = "HelpCommand";
    String CMD_KEYBOARDSHORTCUT = "KeyBoardShortCutCommand";
    String CMD_PREFERENCE = "PreferenceCommand";
    String CMD_STATUSBAR = "StatusBarCommand";
    String CMD_THEMES = "ThemesCommand";
    String CMD_TIPOFTHEDAY = "TipOfTheDayCommand";

    String CMD_APPUSER = "AppUserCommand";
    String CMD_STATICCODEDECODE = "StaticCodeDecodeCommand";
    String CMD_STOCKMAINTENANCE="StockMaintenanceCommand";
    String CMD_CONTRACTMAINTENANCE = "ContractMaintenanceCommand";

    String CMD_DEFAULT = "DefaultCommand";

    ICommand getCommand(Node node, String commandName);
}

