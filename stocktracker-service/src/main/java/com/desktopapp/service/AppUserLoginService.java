package com.desktopapp.service;

import com.stocktracker.framework.IEntity;
import com.stocktracker.jpa.spring.extended.AppUserLoginRepository;
import com.stocktracker.model.AppUserLogin;
import com.stocktracker.model.BaseEntity;
import com.stocktracker.domainobjs.IDomainObject;
import com.stocktracker.domainobjs.json.AppUserLoginDO;
import com.stocktracker.domainobjs.json.TableRowMapDO;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;


/**
 * App Login User Service
 *
 * @author Ratnala Diwakar Choudhury
 */
@Service
@Transactional(
        propagation = Propagation.SUPPORTS,
        readOnly = false)
public class AppUserLoginService extends AbstractService<AppUserLoginDO> {

    @Resource
    protected AppUserLoginRepository appUserLoginRepository;

    public AppUserLoginService() {
        super(AppUserLoginDO.class);
        //repository = new AppUserLoginStandaloneRepository(AppUserLogin.class);
    }


    /**
     * Find AppUserLogin
     *
     * @param userName
     * @return
     */
    public AppUserLoginDO getAppUserLoginByUserName(String userName) {

        List<AppUserLogin> appUserLogin = appUserLoginRepository.findByAppUserLogin(userName);

        if(appUserLogin.size() > 0){
            return returnDomainObject(appUserLogin.get(0));
        }else{
            return null;
        }
    }


    @Override
    public Class<AppUserLogin> getEntityClass() {
        return AppUserLogin.class;
    }

    @Override
    public AppUserLoginDO returnDomainObject(BaseEntity entity) {

        if (entity == null) {
            return null;
        }

        AppUserLoginDO domainObject = new AppUserLoginDO((AppUserLogin) entity);
        return domainObject;
    }

    @Override
    public List<AppUserLoginDO> returnDomainObjects(List<? extends BaseEntity> entities) {

        if (entities == null) {
            return null;
        }

        List<AppUserLoginDO> domainObjects = new ArrayList<AppUserLoginDO>();
        if (entities != null) {
            for (IEntity entity : entities) {
                AppUserLoginDO domainObject = new AppUserLoginDO((AppUserLogin) entity);
                domainObjects.add(domainObject);
            }
        }
        return domainObjects;
    }

    @Override
    public BaseEntity returnEntity(AppUserLoginDO domainObject) {

        AppUserLoginDO appUserLoginDO = (AppUserLoginDO) domainObject;
        AppUserLogin appUserLogin = new AppUserLogin();
        appUserLoginDO.copyDomainObject2Entity(appUserLogin);

        return appUserLogin;
    }

    @Override
    public List<? extends BaseEntity> returnEntities(List<AppUserLoginDO> domainObjects) {
        List<AppUserLogin> entities = new ArrayList();
        for (IDomainObject domainObject : domainObjects) {
            AppUserLoginDO appUserLoginDO = (AppUserLoginDO) domainObject;
            AppUserLogin appUserLogin = new AppUserLogin();
            appUserLoginDO.copyDomainObject2Entity(appUserLogin);
            entities.add(appUserLogin);
        }
        return entities;

    }

    @Override
    public TableRowMapDO<String, String> copyDomainObjToTableRowMap(IDomainObject docHolderDO) {
        AppUserLoginDO appuserloginDO = (AppUserLoginDO) docHolderDO;
        TableRowMapDO<String, String> tableRowMapDO = new TableRowMapDO<String, String>();
        tableRowMapDO.add("id", String.valueOf(appuserloginDO.getId()));
        tableRowMapDO.add("userName", appuserloginDO.getUserName());
        tableRowMapDO.add("fnAccessLevel", appuserloginDO.getFnAccessLevel());
        tableRowMapDO.setClazzName(AppUserLoginDO.CLASS_NAME);
        return tableRowMapDO;
    }

    @Override
    public AppUserLoginRepository getRepository() {
        return this.appUserLoginRepository;
    }
}
