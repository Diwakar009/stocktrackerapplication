/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.desktopapp.fx.view.stockmaintenance;

import com.desktopapp.fx.DesktopFxApplication;
import com.desktopapp.fx.app.AppFontIcons;
import com.desktopapp.fx.common.IGenericComponent;
import com.desktopapp.fx.control.*;
import com.desktopapp.fx.control.fonticon.FontIcon;
import com.desktopapp.fx.control.multiscreen.StackedScreen;
import com.desktopapp.fx.controller.StockMaintenanceController;
import com.desktopapp.fx.mvc.AbstractFormView;
import com.desktopapp.fx.mvc.DataPageController;
import com.stocktracker.domainobjs.json.ResultDO;
import com.stocktracker.domainobjs.json.sms.StockDetailsDO;

import javafx.concurrent.Task;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.*;

import java.util.List;
import java.util.Map;

/**
 * Created by diwakar009 on 3/3/2019.
 */
public class StockDetailsTransactionForm extends AbstractFormView<StockDetailsDO> {

    private StockDetailsDO stockDetailsDO;

    /**
     * Main Form
     *
     */
    private StockDetailsMainForm stockDetailsMainForm;

    /*
     *   Party Details
     *
     */
    private StockDetailsPartyForm stockDetailsPartyForm;

    /*
     *   Transport Details
     *
     */
    private StockDetailsTransportForm stockDetailsTransportForm;


    /*
     * Delivery Details
     *
     */
    private StockDetailsDeliveryForm stockDetailsDeliveryForm;


    /*
     * Extra Information
     *
    */
    private StockDetailsExtraForm stockDetailsExtraForm;

    /**
     *
     */
    private ScrollPane scrollPane;


    public StockDetailsTransactionForm(DataPageController<StockDetailsDO> controller, StockDetailsDO stockDetailsDO,Map<String,Object> intent) {
        super(controller,intent);

        this.stockDetailsDO = stockDetailsDO;
        loadLatestEntity();
        this.getLblformTitle().setText(getTitle());
        pop();
    }

    public StockDetailsTransactionForm(DataPageController<StockDetailsDO> controller, StockDetailsDO stockDetailsDO, StackedScreen stackedScreen,Map<String,Object> intent) {
        super(controller,stackedScreen,intent);
        this.stockDetailsDO = stockDetailsDO;
        loadLatestEntity();
        this.getLblformTitle().setText(getTitle());
        pop();
    }

    @Override
    public Node buildFormView() {

        scrollPane = new ScrollPane();
        scrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
        scrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
        scrollPane.fitToWidthProperty().set(true);

        stockDetailsMainForm = new StockDetailsMainForm(this);
        stockDetailsPartyForm = new StockDetailsPartyForm(this,stockDetailsMainForm.getCdStock(),stockDetailsMainForm.getCdStockVareity(),stockDetailsMainForm.getCdTransactionType());
        stockDetailsTransportForm = new StockDetailsTransportForm(this);
        stockDetailsDeliveryForm = new StockDetailsDeliveryForm(this);
        stockDetailsExtraForm= new StockDetailsExtraForm(this);

        registerComponents(stockDetailsMainForm.getComponents());
        registerComponents(stockDetailsPartyForm.getComponents());
        registerComponents(stockDetailsTransportForm.getComponents());
        registerComponents(stockDetailsDeliveryForm.getComponents());
        registerComponents(stockDetailsExtraForm.getComponents());

        int columnIndex = 0;
        int rowIndex = 0;


        BorderPane borderPane = new BorderPane();

        GridPane gridPane = new GridPane();

        ColumnConstraints col1 = new ColumnConstraints();
        col1.setPercentWidth(50);

        ColumnConstraints col2 = new ColumnConstraints();
        col2.setPercentWidth(50);

        gridPane.getColumnConstraints().addAll( col1,col2);

        gridPane.setGridLinesVisible(false);
        gridPane.add(stockDetailsMainForm,columnIndex,rowIndex);
        gridPane.add(stockDetailsPartyForm,columnIndex,rowIndex+1);
        gridPane.add(stockDetailsTransportForm,columnIndex+1,rowIndex);
        gridPane.add(stockDetailsDeliveryForm,columnIndex+1,rowIndex+1);
        //gridPane.add(stockDetailsExtraForm,columnIndex,rowIndex+2);

        HBox hBox = new HBox();
        hBox.getChildren().add(stockDetailsExtraForm);
        hBox.setAlignment(Pos.CENTER);
        gridPane.add(hBox,columnIndex,rowIndex+2,2,rowIndex+2);
        GridPane.setHgrow(hBox,Priority.ALWAYS);

        scrollPane.setContent(gridPane);
        borderPane.setCenter(scrollPane);
        //borderPane.setCenter(gridPane);
        return borderPane;
    }



    @Override
    public StockDetailsDO getCurrentEntity() {
        return stockDetailsDO;
    }

    @Override
    public void loadLatestEntity() {
        stockDetailsDO = getController().loadData(stockDetailsDO);

        if(stockDetailsDO == null){
            stockDetailsDO = new StockDetailsDO();
        }
    }

    /**
     * Clears all the data from the formComponents
     *
     */
    public void onClear(){

        for(IGenericComponent component: this.getFormComponents()){
            component.clearDataValue();
        }

        requestFocus();
    }


    public void requestFocus(){
        // Hack code as due to scrollpane request focus has issues.
        new Thread( new Task<Void>()
        {
            @Override
            public Void call() throws Exception // This is NOT on FX thread
            {
                Thread.sleep(200);
                return null;
            }
            @Override
            public void succeeded()  // This is called on FX thread.
            {
                getStockDetailsMainForm().getCdIndustry().requestFocus();
            }

        }).start();

    }

    @Override
    public void pop() {
        this.getStockDetailsTransportForm().setDisable(true);
        this.getStockDetailsDeliveryForm().setDisable(true);
        this.getStockDetailsPartyForm().setDisable(true);
        this.setDOToControls(stockDetailsDO);
        this.requestFocus();
        //getStockDetailsMainForm().getCdIndustry().requestFocus();

    }

    /**
     * Set DO to Controls
     *
     * @param stockDetailsDO
     */
    public void setDOToControls(StockDetailsDO stockDetailsDO){
        this.stockDetailsMainForm.setDOToControls(stockDetailsDO);
        this.stockDetailsPartyForm.setDOToControls(stockDetailsDO);
        this.stockDetailsTransportForm.setDOToControls(stockDetailsDO);
        this.stockDetailsDeliveryForm.setDOToControls(stockDetailsDO);
        this.stockDetailsExtraForm.setDOToControls(stockDetailsDO);
    }

    /**
     * Set controls to DO
     *
     * @param stockDetailsDO
     */
    public void setControlsToDO(StockDetailsDO stockDetailsDO){

        this.stockDetailsMainForm.setControlsToDO(stockDetailsDO);
        this.stockDetailsPartyForm.setControlsToDO(stockDetailsDO);
        this.stockDetailsTransportForm.setControlsToDO(stockDetailsDO);
        this.stockDetailsDeliveryForm.setControlsToDO(stockDetailsDO);
        this.stockDetailsExtraForm.setControlsToDO(stockDetailsDO);
    }

    @Override
    public void onDelete(){
        ResultDO resultDO = ((StockMaintenanceController)getController()).onDelete(stockDetailsDO);

        if(resultDO == null){
            return; // dont do any thing
        }

        if(!resultDO.isError()){
            DesktopFxApplication.showSuccessMessage("Record Deleted Successfully.");
            onCloseForm();
        }else {
            DesktopFxApplication.showErrorMessageList(resultDO.getErrorList());
        }
    }

    @Override
    public void push() {
        setControlsToDO(stockDetailsDO);
    }

    @Override
    public void refreshForm() {
        this.stockDetailsMainForm.getCdIndustry().requestFocus();
    }

    @Override
    public FontIcon getFontIcon() {
        return AppFontIcons.STOCKMANAGEMENT;
    }

    @Override
    public String getTitle() {
        return !isEditMode() ? "Add New Stock Transaction" : "Edit Stock Transaction";
    }

    @Override
    public Node asNode() {
        return this;
    }

    @Override
    public List<ActionCommand> addMoreActionCommands(List<ActionCommand> commands) {
        return commands;
    }

    public StockDetailsDO getStockDetailsDO() {
        return stockDetailsDO;
    }

    public void setStockDetailsDO(StockDetailsDO stockDetailsDO) {
        this.stockDetailsDO = stockDetailsDO;
    }

    public StockDetailsMainForm getStockDetailsMainForm() {
        return stockDetailsMainForm;
    }

    public void setStockDetailsMainForm(StockDetailsMainForm stockDetailsMainForm) {
        this.stockDetailsMainForm = stockDetailsMainForm;
    }

    public StockDetailsPartyForm getStockDetailsPartyForm() {
        return stockDetailsPartyForm;
    }

    public void setStockDetailsPartyForm(StockDetailsPartyForm stockDetailsPartyForm) {
        this.stockDetailsPartyForm = stockDetailsPartyForm;
    }

    public StockDetailsTransportForm getStockDetailsTransportForm() {
        return stockDetailsTransportForm;
    }

    public void setStockDetailsTransportForm(StockDetailsTransportForm stockDetailsTransportForm) {
        this.stockDetailsTransportForm = stockDetailsTransportForm;
    }

    public StockDetailsDeliveryForm getStockDetailsDeliveryForm() {
        return stockDetailsDeliveryForm;
    }

    public void setStockDetailsDeliveryForm(StockDetailsDeliveryForm stockDetailsDeliveryForm) {
        this.stockDetailsDeliveryForm = stockDetailsDeliveryForm;
    }

    public StockDetailsExtraForm getStockDetailsExtraForm() {
        return stockDetailsExtraForm;
    }

    public void setStockDetailsExtraForm(StockDetailsExtraForm stockDetailsExtraForm) {
        this.stockDetailsExtraForm = stockDetailsExtraForm;
    }
}
