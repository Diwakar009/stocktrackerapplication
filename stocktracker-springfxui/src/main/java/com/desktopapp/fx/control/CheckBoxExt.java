package com.desktopapp.fx.control;

import com.desktopapp.fx.common.IGenericComponent;
import javafx.scene.control.CheckBox;

@SuppressWarnings("serial")
public class CheckBoxExt<E> extends CheckBox implements IGenericComponent {


	public CheckBoxExt(String paramString) {
		super(paramString);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void setAsReadOnly(boolean readOnly) {
		// TODO Auto-generated method stub
		setDisable(readOnly);
	}

	@Override
	public void clearDataValue() {
		//setSelectedIndex(0);
	}

	@Override
	public void destroyComponent() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setDataValue(String value) {
		 setSelected(new Boolean(value));
	}

	@Override
	public String getDataValue() {
		// TODO Auto-generated method stub
		return Boolean.toString(isSelected());
	}
	
	
	@Override
	public boolean isDataExchangeApplicable() {
		return true;
	}
}
