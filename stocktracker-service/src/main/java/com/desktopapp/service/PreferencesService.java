package com.desktopapp.service;


import com.stocktracker.jpa.spring.extended.PreferencesRepository;
import com.stocktracker.model.BaseEntity;
import com.stocktracker.model.Preferences;
import com.stocktracker.domainobjs.IDomainObject;
import com.stocktracker.domainobjs.json.PreferencesDO;
import com.stocktracker.domainobjs.json.TableRowMapDO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Static COde Decode Service
 *
 * @author Ratnala Diwakar Choudhury
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS)
public class PreferencesService extends AbstractService<PreferencesDO> {

    @Autowired
    protected PreferencesRepository preferencesRepository;

    public PreferencesService() {
        super(PreferencesDO.class);
    }


    @Override
    public Class<Preferences> getEntityClass() {
        return Preferences.class;
    }

    @Override
    public PreferencesDO returnDomainObject(BaseEntity entity) {

        if (entity == null) {
            return null;
        }

        PreferencesDO domainObject = new PreferencesDO((Preferences) entity);
        return domainObject;
    }

    @Override
    public List<PreferencesDO> returnDomainObjects(List<? extends BaseEntity> entities) {
        if (entities == null) {
            return null;
        }
        List<PreferencesDO> domainObjects = new ArrayList<PreferencesDO>();

        if (entities != null) {
            entities.forEach(entity -> {
                PreferencesDO domainObject = new PreferencesDO((Preferences) entity);
                domainObjects.add(domainObject);
            });
        }
        return domainObjects;
    }

    @Override
    public BaseEntity returnEntity(PreferencesDO domainObject) {

        PreferencesDO preferencesDO = (PreferencesDO) domainObject;
        Preferences preferences = new Preferences();
        preferencesDO.copyDomainObject2Entity(preferences);

        return preferences;
    }

    @Override
    public List<? extends BaseEntity> returnEntities(List<PreferencesDO> domainObjects) {
        List<Preferences> entities = new ArrayList();
        for (IDomainObject domainObject : domainObjects) {
            PreferencesDO preferencesDO = (PreferencesDO) domainObject;
            Preferences preferences = new Preferences();
            preferencesDO.copyDomainObject2Entity(preferences);
            entities.add(preferences);
        }
        return entities;

    }

    @Override
    public TableRowMapDO<String, String> copyDomainObjToTableRowMap(IDomainObject domainObject) {
        PreferencesDO code = (PreferencesDO) domainObject;
        TableRowMapDO<String, String> tableRowMapDO = new TableRowMapDO<String, String>();
        tableRowMapDO.add("id", code.getId().toString());
        tableRowMapDO.setClazzName(PreferencesDO.CLASS_NAME);

        return tableRowMapDO;
    }

    @Override
    public PreferencesRepository getRepository() {
        return this.preferencesRepository;
    }

}
