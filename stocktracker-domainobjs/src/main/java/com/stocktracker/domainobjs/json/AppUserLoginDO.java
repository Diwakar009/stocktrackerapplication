/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.domainobjs.json;

import com.stocktracker.model.AppUser;
import com.stocktracker.model.AppUserLogin;
import com.stocktracker.model.BaseEntity;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import java.io.Serializable;


/**
 * The persistent class for the app_user_login database table.
 * 
 */

@JsonIgnoreProperties(ignoreUnknown=true)
@JsonInclude(Include.NON_EMPTY)
public class AppUserLoginDO extends BaseDomainObject implements Serializable {

	public static String CLASS_NAME = AppUserLoginDO.class.getName();

	private static final long serialVersionUID = 1L;

	@JsonProperty("id")
	private Long id;

	@JsonProperty("fnAccessLevel")
	private String fnAccessLevel;

	@JsonProperty("loginAttempts")
	private int loginAttempts;

	@JsonProperty("userName")
	private String userName;

	@JsonProperty("password")
	private String password;

	@JsonProperty("secQuestion")
	private String secQuestion;

	@JsonProperty("secQuestionAns")
	private String secQuestionAns;

	@JsonProperty("appUser")
	private AppUserDO appUser;

	public AppUserLoginDO() {
	}

	public AppUserLoginDO(AppUserLogin appUserLogin) {
		copyEntity2DomainObject(appUserLogin);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFnAccessLevel() {
		return this.fnAccessLevel;
	}

	public void setFnAccessLevel(String fnAccessLevel) {
		this.fnAccessLevel = fnAccessLevel;
	}

	public int getLoginAttempts() {
		return this.loginAttempts;
	}

	public void setLoginAttempts(int loginAttempts) {
		this.loginAttempts = loginAttempts;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getSecQuestion() {
		return this.secQuestion;
	}

	public void setSecQuestion(String secQuestion) {
		this.secQuestion = secQuestion;
	}

	public String getSecQuestionAns() {
		return this.secQuestionAns;
	}

	public void setSecQuestionAns(String secQuestionAns) {
		this.secQuestionAns = secQuestionAns;
	}
	

	public AppUserDO getAppUser() {
		return this.appUser;
	}

	public void setAppUser(AppUserDO appUser) {
		this.appUser = appUser;
	}

	@Override
	public Object getDomainObjectKey() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AppUserLogin copyDomainObject2Entity(BaseEntity entity) {
		AppUserLogin appUserLogin = (AppUserLogin)entity;

		appUserLogin.setCreatedBy(getCreatedBy());
		appUserLogin.setUpdatedBy(getUpdatedBy());
		appUserLogin.setCreatedAt(getCreatedAt());
		appUserLogin.setUpdatedAt(getUpdatedAt());
		appUserLogin.setUserName(getUserName());
		appUserLogin.setFnAccessLevel(getFnAccessLevel());
		appUserLogin.setPassword(getPassword());
		appUserLogin.setSecQuestion(getSecQuestion());
		appUserLogin.setSecQuestionAns(getSecQuestionAns());
		appUserLogin.setLoginAttempts(getLoginAttempts());

		AppUser appUser = new AppUser();
		if(getAppUser() != null) {
			getAppUser().copyDomainObject2Entity(appUser);
		}

		appUserLogin.setAppUser(appUser);
		appUserLogin.setId(getId());

		return appUserLogin;

	}

	@Override
	public void copyEntity2DomainObject(BaseEntity entity) {

		AppUserLogin appUserLogin = (AppUserLogin)entity;

		setCreatedBy(appUserLogin.getCreatedBy());
		setUpdatedBy(appUserLogin.getUpdatedBy());
		setCreatedAt(appUserLogin.getCreatedAt());
		setUpdatedAt(appUserLogin.getUpdatedAt());
		setUserName(appUserLogin.getUserName());
		setFnAccessLevel(appUserLogin.getFnAccessLevel());
		setPassword(appUserLogin.getPassword());
		setSecQuestion(appUserLogin.getSecQuestion());
		setSecQuestionAns(appUserLogin.getSecQuestionAns());
		setLoginAttempts(appUserLogin.getLoginAttempts());

		AppUserDO appUserDO = new AppUserDO();
		appUserDO.copyEntity2DomainObject(appUserLogin.getAppUser());
		setAppUser(appUserDO);
		setId(appUserLogin.getId());

	}



}