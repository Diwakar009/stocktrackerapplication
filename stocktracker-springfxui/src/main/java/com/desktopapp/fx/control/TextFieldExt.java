/*
 * Copyright (C) 2017 Cem Ikta
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 *
 */

package com.desktopapp.fx.control;

import com.desktopapp.fx.common.IGenericComponent;
import com.desktopapp.fx.control.listeners.ValueChangeListener;
import com.stocktracker.util.CustomStringUtils;
import javafx.animation.FadeTransition;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.property.ObjectProperty;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.util.Duration;
import org.controlsfx.control.textfield.CustomTextField;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.UnaryOperator;

/**
 * Text field control with limited character length
 *
 * @see TextField
 * @see TextFormatter
 * @see UnaryOperator
 *
 * @author Diwakar C
 */
public class TextFieldExt extends CustomTextField implements IGenericComponent{


    /**
     * max lenght property
     */

    private String name = "";

    private int maxLength;

    private List<ValueChangeListener> valueChangeListenerList = null;

    private boolean disableValueChangeListener = false;

    private String prevTextValue = "";

    public boolean mandatory = false;

    private static final Duration FADE_DURATION = Duration.millis(350);

    public TextFieldExt() {
        super();
        valueChangeListenerList = new ArrayList<ValueChangeListener>();
        registerListener(valueChangeListenerList);


    }

    /**
     * Constructor with max lenght property
     *
     * @param maxLength max lenght property
     */
    public TextFieldExt(String name,int maxLength) {
        super();
        this.name = name;
        this.maxLength = maxLength;
        setTextFormatter(new TextFormatter<>(new LengthUnaryOperator(maxLength)));

        valueChangeListenerList = new ArrayList<ValueChangeListener>();
        registerListener(valueChangeListenerList);
    }

    /**
     * Creates text field with string length
     *
     * @param length maximale string length
     */
    public TextFieldExt(int length) {
        super();
        setTextFormatter(new TextFormatter<>(new LengthUnaryOperator(length)));
        valueChangeListenerList = new ArrayList<ValueChangeListener>();
        registerListener(valueChangeListenerList);
    }


    /**
     * Constructor with max lenght property and readonly
     *
     * @param maxLength max lenght property
     */
    public TextFieldExt(String name,int maxLength,boolean readonly) {
        super();
        this.name = name;
        this.maxLength = maxLength;
        setTextFormatter(new TextFormatter<>(new LengthUnaryOperator(maxLength)));
        valueChangeListenerList = new ArrayList<ValueChangeListener>();
        setAsReadOnly(readonly);
        registerListener(valueChangeListenerList);
    }

    /**
     * Constructor with max lenght property and readonly
     *
     * @param maxLength max lenght property
     */
    public TextFieldExt(int maxLength,boolean readonly) {
        super();
        this.maxLength = maxLength;
        setTextFormatter(new TextFormatter<>(new LengthUnaryOperator(maxLength)));
        valueChangeListenerList = new ArrayList<ValueChangeListener>();
        setAsReadOnly(readonly);
        registerListener(valueChangeListenerList);
    }

    /**
     * Gets max length
     *
     * @return maxLength
     */
    public int getMaxLength() {
        return maxLength;
    }




    /**
     * Sets max length
     *
     * @param maxLength max length property
     */
    public void setMaxLength(int maxLength) {
        this.maxLength = maxLength;
        setTextFormatter(new TextFormatter<>(new LengthUnaryOperator(maxLength)));
    }

    /**
     * Set Data Value
     * @param value
     */
    public void setDataValue(String value){
        if(value == null)
            value = "";

        setTextValue(value);
    }

    /**
     * Get Data Value (Do any formating if necessary)
     * @return
     */
    public String getDataValue() {
        return super.getText();
    }

    public boolean isDisableValueChangeListener() {
        return disableValueChangeListener;
    }

    public void setDisableValueChangeListener(boolean disableValueChangeListener) {
        this.disableValueChangeListener = disableValueChangeListener;
    }

    public void addValueChangeListener(ValueChangeListener valueChangeListener){
        valueChangeListenerList.add(valueChangeListener);
    }

    public void setTextValue(String t) {
        // TODO Auto-generated method stub
        super.setText(t);

        if(t != null){
            prevTextValue = t;
        }else{
            prevTextValue = "";
        }
    }


    public boolean isMandatory() {
        return mandatory;
    }

    public void setMandatory(boolean mandatory) {
        this.mandatory = mandatory;
    }

    @Override
    public void setAsReadOnly(boolean readOnly) {
        //setEditable(!readOnly);
        setDisable(readOnly);
    }

    @Override
    public void clearDataValue() {
        setText("");
    }

    @Override
    public void destroyComponent() {
        valueChangeListenerList = null;
    }

    @Override
    public boolean validateInput() {
        if(this.mandatory){
            if(CustomStringUtils.isEmpty(getDataValue())){
                MessageBox.create()
                        .contentText(this.name + " Field is mandatory!")
                        .title("registerMandatoryValidation Error Alert")
                        .headerText("registerMandatoryValidation Alert")
                        .showError();


                this.requestFocus();
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean isComponentShowing(){
        return isVisible();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean isDataExchangeApplicable() {
        return true;
    }

    @Override
    public boolean isReadonly() {
        return isDisabled();
    }

    /**
     * Register value change listeners.
     *
     * @param valueChangeListeners
     */
    public void registerListener(List<ValueChangeListener> valueChangeListeners){
        this.textProperty().addListener((observable,oldText,newtext) -> {
           if(!Objects.equals(oldText,newtext)) {
                if (!isDisableValueChangeListener()) {
                    disableValueChangeListener = true; // temporary disable for infinite calling of value change listner
                    for (ValueChangeListener listener : valueChangeListenerList) {
                        listener.valueChanged();
                    }
                    disableValueChangeListener = false;
                }
            }
        });

        //setupClearButtonField(this,rightProperty());
    }

    private void setupClearButtonField(TextField inputField, ObjectProperty<Node> rightProperty) {
        inputField.getStyleClass().add("clearable-field"); //$NON-NLS-1$

        Region clearButton = new Region();
        clearButton.getStyleClass().addAll("graphic"); //$NON-NLS-1$
        StackPane clearButtonPane = new StackPane(clearButton);
        clearButtonPane.getStyleClass().addAll("clear-button"); //$NON-NLS-1$
        clearButtonPane.setOpacity(0.0);
        clearButtonPane.setCursor(Cursor.DEFAULT);
        clearButtonPane.setOnMouseReleased(e -> setDataValue(""));
        clearButtonPane.managedProperty().bind(inputField.editableProperty());
        clearButtonPane.visibleProperty().bind(inputField.editableProperty());

        rightProperty.set(clearButtonPane);

        final FadeTransition fader = new FadeTransition(FADE_DURATION, clearButtonPane);
        fader.setCycleCount(1);

        inputField.textProperty().addListener(new InvalidationListener() {
            @Override public void invalidated(Observable arg0) {
                String text = inputField.getText();
                boolean isTextEmpty = text == null || text.isEmpty();
                boolean isButtonVisible = fader.getNode().getOpacity() > 0;

                if (isTextEmpty && isButtonVisible) {
                    setButtonVisible(false);
                } else if (!isTextEmpty && !isButtonVisible) {
                    setButtonVisible(true);
                }
            }

            private void setButtonVisible( boolean visible ) {
                fader.setFromValue(visible? 0.0: 1.0);
                fader.setToValue(visible? 1.0: 0.0);
                fader.play();
            }
        });
    }
}
