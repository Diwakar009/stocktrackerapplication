package com.desktopapp.fx.control;

import com.desktopapp.fx.common.IGenericComponent;
import javafx.scene.control.DatePicker;

import java.time.LocalDate;

/**
 * Created by diwakar009 on 21/9/2019.
 */
public class DataPickerExt extends DatePicker implements IGenericComponent{


    @Override
    public void setAsReadOnly(boolean readOnly) {
         setDisable(readOnly);
    }

    @Override
    public void destroyComponent() {

    }

    @Override
    public boolean isDataExchangeApplicable() {
        return true;
    }

    @Override
    public void setDataValue(String value) {

    }

    @Override
    public String getDataValue() {
        return null;
    }

    @Override
    public boolean isComponentShowing() {
        return isShowing();
    }

    @Override
    public boolean validateInput() {
        return false;
    }

    @Override
    public void clearDataValue() {

    }

    @Override
    public boolean isReadonly() {
        return isDisabled();
    }
}
