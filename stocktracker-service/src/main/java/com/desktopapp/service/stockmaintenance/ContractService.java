/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.desktopapp.service.stockmaintenance;


import com.desktopapp.exceptions.NoOpenContractAvailableException;
import com.desktopapp.service.AbstractService;
import com.stocktracker.domainobjs.json.sms.StockDetailsDO;
import com.stocktracker.jpa.spring.extended.ContractRepository;
import com.stocktracker.model.BaseEntity;
import com.stocktracker.model.sms.Contract;
import com.stocktracker.domainobjs.IDomainObject;
import com.stocktracker.domainobjs.json.TableRowMapDO;
import com.stocktracker.domainobjs.json.sms.ContractDO;
import com.stocktracker.util.DateTimeUtil;
import com.stocktracker.util.MathUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Contract Service
 *
 * @author Ratnala Diwakar Choudhury
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS,readOnly = true)
public class ContractService extends AbstractService<ContractDO> {

    @Resource
    protected ContractRepository contractRepository;

    public ContractService() {
        super(ContractDO.class);
    }

    public boolean isContractApplicable(String transactionType){
        boolean isApplicable = false;
        switch (transactionType) {
            case "MDRE":
            case "MDAJ":
            case "MDRT":
            case "KMDEL":
            case "KMRET": // Not there in the system yet
            case "MDEN": // Mandi Entry
                isApplicable = true;
                break;
        }
        return isApplicable;
    }

    public ContractDO updateOpenContract(String industryCode,String transactionType, Double quantity){
        if(isContractApplicable(transactionType)){
            ContractDO contractDO = returnDomainObject(contractRepository.findOpenContract(industryCode));
            if(contractDO != null) {
                return updateContract(contractDO.getId(), transactionType, quantity,false);
            }
        }
        return null;
     }


    public ContractDO updateContract(Long contractId,String transactionType, Double quantity,boolean isReverse){
        ContractDO contractDO = find(contractId);;
        switch (transactionType){
            case "MDRE":
            case "MDAJ":
                if(!isReverse)
                    contractDO.setReceivedRawMaterialQuantity(MathUtil.add(contractDO.getReceivedRawMaterialQuantity(),quantity));
                else
                    contractDO.setReceivedRawMaterialQuantity(MathUtil.substract(contractDO.getReceivedRawMaterialQuantity(),quantity));
                break;
            case "MDRT":
                if(!isReverse)
                    contractDO.setReceivedRawMaterialQuantity(MathUtil.substract(contractDO.getReceivedRawMaterialQuantity(),quantity));
                else
                    contractDO.setReceivedRawMaterialQuantity(MathUtil.add(contractDO.getReceivedRawMaterialQuantity(),quantity));
                break;
            case "KMDEL":
                if(!isReverse)
                    contractDO.setProductDeliveredQuantity(MathUtil.add(contractDO.getProductDeliveredQuantity() ,quantity));
                else
                    contractDO.setProductDeliveredQuantity(MathUtil.substract(contractDO.getProductDeliveredQuantity() ,quantity));
                break;
            case "KMRET": // Not there in the system yet
                if(!isReverse)
                    contractDO.setProductDeliveredQuantity(MathUtil.substract(contractDO.getProductDeliveredQuantity() ,quantity));
                else
                    contractDO.setProductDeliveredQuantity(MathUtil.add(contractDO.getProductDeliveredQuantity() ,quantity));
                break;
            case "MDEN": // Mandi Entry
                break;
            default:
                return null;
        }
        return (ContractDO) update(contractDO);
    }

    public ContractDO reviseContract(StockDetailsDO newStockDetails,StockDetailsDO oldStockDetails) throws NoOpenContractAvailableException {
        // check old stock details, and rollback the contract the totals
        // check the new stock details and applu to the contract totals
        if(isContractApplicable(oldStockDetails.getTransactionType())){
            updateContract(oldStockDetails.getContractDO().getId(),oldStockDetails.getTransactionType(),oldStockDetails.getTotalWeightment(),true);
        }

        if(newStockDetails != null) {
            if (isContractApplicable(newStockDetails.getTransactionType())) {
                return updateOpenContract(newStockDetails.getOnBeHalf(), newStockDetails.getTransactionType(), newStockDetails.getTotalWeightment());
            }
        }

        return null;
    }

    @Override
    public Page<? extends BaseEntity> findByFilter(Map<String, Object> parameters, PageRequest pageRequest) {
        String industryCodeFilter = (String)parameters.get("industryCode");
        Date startDateRangeStart = (Date)parameters.get("startDateRangeStart");
        Date startDateRangeEnd = (Date)parameters.get("startDateRangeEnd");
        String contractTypeFilter = (String)parameters.get("contractType");
        String contractNameFilter = (String)parameters.get("contractName");
        String contractStatusFilter = (String)parameters.get("contractStatus");
        return getRepository().findAllContractByFilter(industryCodeFilter,startDateRangeStart,startDateRangeEnd,contractTypeFilter,contractNameFilter,contractStatusFilter,pageRequest);
    }

    @Override
    public Class<Contract> getEntityClass() {
        return Contract.class;
    }

    @Override
    public ContractDO returnDomainObject(BaseEntity entity) {

        if (entity == null) {
            return null;
        }

        ContractDO domainObject = new ContractDO((Contract) entity);
        return domainObject;
    }

    @Override
    public List<ContractDO> returnDomainObjects(List<? extends BaseEntity> entities) {
        if (entities == null) {
            return null;
        }
        List<ContractDO> domainObjects = new ArrayList<ContractDO>();

        if (entities != null) {
            entities.forEach(entity -> {
                ContractDO domainObject = new ContractDO((Contract) entity);
                domainObjects.add(domainObject);
            });
        }
        return domainObjects;
    }

    @Override
    public BaseEntity returnEntity(ContractDO domainObject) {
        ContractDO contractDO = (ContractDO) domainObject;
        Contract contract = new Contract();
        contractDO.copyDomainObject2Entity(contract);
        return contract;
    }

   @Override
    public List<? extends BaseEntity> returnEntities(List<ContractDO> domainObjects) {
        List<Contract> entities = new ArrayList();
        for (IDomainObject domainObject : domainObjects) {
            ContractDO contractDO = (ContractDO) domainObject;
            Contract contract = new Contract();
            contractDO.copyDomainObject2Entity(contract);
            entities.add(contract);
        }
        return entities;
    }




    @Override
    public TableRowMapDO<String, String> copyDomainObjToTableRowMap(IDomainObject domainObject) {
        ContractDO contractDO = (ContractDO) domainObject;
        TableRowMapDO<String, String> tableRowMapDO = new TableRowMapDO<String, String>();
        tableRowMapDO.add("id", contractDO.getId().toString());
        tableRowMapDO.add("industryCode",contractDO.getIndustryCode());
        tableRowMapDO.add("contactName",contractDO.getContractName());
        tableRowMapDO.add("contactTypeCode",contractDO.getContractTypeCode());
        tableRowMapDO.add("deliveryProduct",contractDO.getDeliveryProduct());
        tableRowMapDO.add("rawMaterial",contractDO.getRawMaterial());
        if(contractDO.getStartDate() != null) {
            tableRowMapDO.add("startDate", String.valueOf(DateTimeUtil.convertDateToUTCString(contractDO.getStartDate())));
        }

        if(contractDO.getEndDate() != null) {
            tableRowMapDO.add("endDate", String.valueOf(DateTimeUtil.convertDateToUTCString(contractDO.getEndDate())));
        }

        if(contractDO.getApprovedRawMaterialQuantity() != null){
            tableRowMapDO.add("approvedRawMaterialQuantity",String.valueOf(contractDO.getApprovedRawMaterialQuantity()));
        }

        if(contractDO.getReceivedRawMaterialQuantity() != null){
            tableRowMapDO.add("receivedRawMaterialQuantity",String.valueOf(contractDO.getReceivedRawMaterialQuantity()));
        }

        if(contractDO.getProductDeliveryQuantity() != null){
            tableRowMapDO.add("productDeliveryQuantity",String.valueOf(contractDO.getProductDeliveryQuantity()));
        }

        if(contractDO.getProductDeliveredQuantity() != null){
            tableRowMapDO.add("productDeliveredQuantity",String.valueOf(contractDO.getProductDeliveredQuantity()));
        }

        tableRowMapDO.add("status",contractDO.getStatus());
        tableRowMapDO.add("remarks",contractDO.getRemarks());

        tableRowMapDO.setClazzName(ContractDO.CLASS_NAME);
        return tableRowMapDO;
    }

    @Override
    public ContractRepository getRepository() {
        return this.contractRepository;
    }
}
