/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.desktopapp.fx.control;

import javafx.scene.control.TextFormatter;

import java.util.function.UnaryOperator;

/**
 * FloatNumberUnaryOperator unary operator
 *
 * @author R Diwakar C
 */
public class FloatNumberUnaryOperator implements UnaryOperator<TextFormatter.Change> {

    private Integer scale;

    public FloatNumberUnaryOperator(Integer scale) {
        this.scale = scale;
    }

    public FloatNumberUnaryOperator() {
        this.scale = 2;
    }

    @Override
    public TextFormatter.Change apply(TextFormatter.Change change) {

        String text = change.getText();

        if (!change.isContentChange()) {
            return change;
        }

        if (text.matches("[0-9]*")) {
            return change;
        }

        return change;
    }

}
