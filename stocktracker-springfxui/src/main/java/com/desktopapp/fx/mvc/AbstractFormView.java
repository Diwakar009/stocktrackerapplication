/*
 * Copyright (C) 2019 R Diwakar C
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 *
 */

package com.desktopapp.fx.mvc;


import com.desktopapp.fx.DesktopFxApplication;
import com.desktopapp.fx.app.AppAccelerator;
import com.desktopapp.fx.common.IAccelerator;
import com.desktopapp.fx.common.IGenericComponent;
import com.desktopapp.fx.common.IStackedScreenProperties;
import com.desktopapp.fx.control.*;
import com.desktopapp.fx.control.multiscreen.StackedScreen;
import com.desktopapp.fx.framework.ExtendedValidationSupport;
import com.desktopapp.fx.util.I18n;
import com.desktopapp.fx.util.ViewHelpers;
import com.stocktracker.domainobjs.json.ResultDO;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.*;
import org.controlsfx.control.Notifications;
import org.controlsfx.validation.decoration.GraphicValidationDecoration;
import com.stocktracker.domainobjs.json.BaseDomainObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * Abstract form view for data editing in {@link Dialog} forms.
 *
 * @param <T> entity
 * @see DataPageController
 *
 * @author R Diwakar C
 */
public abstract class AbstractFormView<T extends BaseDomainObject> extends StackPane implements IStackedScreenProperties,FormPageView {

    private final Logger logger = LoggerFactory.getLogger(AbstractFormView.class);
    public static final String FORM_PAGES = "form-pages";

    protected DataPageController<T> controller;
    private ExtendedValidationSupport validationSupport;


    private StackedScreen stackedScreen;
    private List<IGenericComponent> formComponents = new ArrayList<IGenericComponent>();
    private List<ActionCommand> commandButtons = new ArrayList<ActionCommand>();
    protected Map<String,Object> intent = new HashMap<String,Object>();

     public final int BUTTON_SPACING = 25;
    // commandButtons
    protected Label lblformTitle;
    public boolean alternateColorInd;



    /**
     * Creates form view
     *
     * @param controller form's controller
     */
    public AbstractFormView(DataPageController<T> controller,Map<String,Object> intent) {
        super();
        this.intent = intent;
        this.controller = controller;
        this.stackedScreen = new StackedScreen();
        init();
    }

    public AbstractFormView(DataPageController<T> controller,StackedScreen stackedScreen,Map<String,Object> intent) {
        super();
        this.intent=intent;
        this.controller = controller;
        this.stackedScreen = stackedScreen;
        init();
    }

    public void init(){
        buildView();
    }

    public DataPageController<T> getController() {
        return this.controller;
    }

    private void buildView() {
        logger.debug("build form view");
        BorderPane borderPane = new BorderPane();
        borderPane.getStyleClass().add("form-view");
        borderPane.setTop(getHeaderBar());
        Node centerNode = buildFormView();
        borderPane.setCenter(centerNode);
        borderPane.getCenter().getStyleClass().add("form-center");
        borderPane.setBottom(buildButtonPane());

        getChildren().add(borderPane);

        stackedScreen.addScreen(this, (IStackedScreenProperties) this);
    }

    private HBox getHeaderBar() {
        logger.debug("get header bar");
        HBox headerBar = new HBox();
        headerBar.getStyleClass().add("form-header");

        lblformTitle = ViewHelpers.createFontIconLabel(getFontIcon(), "");
        lblformTitle.getStyleClass().add("form-title");

        Region spacer = new Region();
        HBox.setHgrow(spacer, Priority.ALWAYS);

        headerBar.getChildren().addAll(lblformTitle, spacer);

        return headerBar;
    }

    private HBox buildButtonPane() {

        if(isEditMode()) {
            commandButtons.add(new ActionButtonCommand(I18n.COMMON.getString("action.saveAndClose"), AppAccelerator.getKeyCode("SCD-A"), event -> onSaveAndClose()));
            commandButtons.add(new ActionButtonCommand(I18n.COMMON.getString("action.delete"), AppAccelerator.getKeyCode("SCD-D"), event -> onDelete()));
        }else{
            commandButtons.add(new ActionButtonCommand(I18n.COMMON.getString("action.save"), AppAccelerator.getKeyCode("SCD-S"), event -> onSave()));
            commandButtons.add(new ActionButtonCommand(I18n.COMMON.getString("action.saveAndClose"), AppAccelerator.getKeyCode("SCD-A"), event -> onSaveAndClose()));
        }
        commandButtons.add(new ActionButtonCommand(I18n.COMMON.getString("action.clear"), AppAccelerator.getKeyCode("SCD-C"), event -> onClear()));
        commandButtons.add(new ActionButtonCommand(I18n.COMMON.getString("action.close"),new KeyCodeCombination(KeyCode.ESCAPE, KeyCombination.SHIFT_DOWN), event -> onCloseForm()));

        commandButtons = addMoreActionCommands(commandButtons);

        HBox buttonBar = new HBox();
        buttonBar.getStyleClass().add("form-button-bar");
        buttonBar.setAlignment(Pos.CENTER);
        buttonBar.setSpacing(BUTTON_SPACING);
        for(ActionCommand command : commandButtons){
            buttonBar.getChildren().add(command);
        }
        return buttonBar;
    }

    /**
     * Register the generic component.
     *
     * @param components
     */
    public void registerComponents(IGenericComponent... components){
        for(IGenericComponent component: components) {
            this.formComponents.add(component);
        }
    }

    /**
     * Register the generic component.
     *
     * @param components
     */
    public void registerComponents(List<IGenericComponent> components){
        for(IGenericComponent component: components) {
            this.formComponents.add(component);
        }
    }

    /**
     * Add more commandButtons if required by the derived classes
     *
     * @param commands
     */
    public abstract List<ActionCommand> addMoreActionCommands(List<ActionCommand> commands);

    /**
     * Builds form view control
     *
     * @return center node of the form view
     */
    public abstract Node buildFormView();

    /**
     * If the form view has print actions, this method should be overridden.
     * Form view has no default print actions.
     *
     * @return true if the form view has print actions, otherwise false.
     */
    public Boolean hasPrintActions() {
        return false;
    }

    /**
     * Gets the current entity object
     *
     * @return entity of the form view
     */
    public abstract T getCurrentEntity();

    /**
     * Pop data model values to form view
     */
    public abstract void pop();

    /**
     * Retrieve latest entity from the datasource
     */
    public abstract void loadLatestEntity();

    /**
     * Push form view values to data model
     */
    public abstract void push();

    public void showSuccessMessage(String message){
        alternateColorInd = !alternateColorInd;
        DesktopFxApplication.showSuccessMessage(message,alternateColorInd);
    }

    /**
     * If the fields of the form view is valid, save the data of the form view.
     */
    public void onSave() {
        logger.debug("form view save action");
        if (isFormValid()) {
            push();
            ResultDO resultDO = controller.onSave(getCurrentEntity());
            if(!resultDO.isError()){
                refreshForm();

                if(isEditMode()){
                    if( resultDO.getResultAttributes() != null && resultDO.getResultAttributes().size() > 0) {
                        showSuccessMessage(String.format("Record Updated Successfully : ID - %S ", resultDO.getResultAttributes().get(0)));
                    }else {
                        showSuccessMessage("Record Updated Successfully");
                    }
                }else{
                    if( resultDO.getResultAttributes() != null && resultDO.getResultAttributes().size() > 0) {
                        showSuccessMessage(String.format("Record Created Successfully : ID - %S", resultDO.getResultAttributes().get(0)));
                    }else {
                        showSuccessMessage("Record Created Successfully");
                    }
                }

            }else {
                DesktopFxApplication.showErrorMessageList(resultDO.getErrorList());
            }
        }
    }

    /**
     * If the fields of the form view are valid, save the data of the form view and close the form view.
     */
    public void onSaveAndClose() {
        logger.debug("form view save and close action");
        if (isFormValid()) {
            push();

            ResultDO resultDO = controller.onSave(getCurrentEntity());
            if(!resultDO.isError()){
                refreshForm();

                if(isEditMode()){
                    if( resultDO.getResultAttributes() != null && resultDO.getResultAttributes().size() > 0) {
                        DesktopFxApplication.showSuccessMessage(String.format("Record Updated Successfully : ID - %S ", resultDO.getResultAttributes().get(0)));
                    }else {
                        DesktopFxApplication.showSuccessMessage("Record Updated Successfully");
                    }
                }else{
                    if( resultDO.getResultAttributes() != null && resultDO.getResultAttributes().size() > 0) {
                        DesktopFxApplication.showSuccessMessage(String.format("Record Created Successfully : ID - %S", resultDO.getResultAttributes().get(0)));
                    }else {
                        DesktopFxApplication.showSuccessMessage("Record Created Successfully");
                    }
                }
                onCloseForm();
            }else {
                DesktopFxApplication.showErrorMessageList(resultDO.getErrorList());
            }
        }
    }

    /**
     * If the fields of the form view are valid, save the data of the form view and close the form view.
     */
    public void onDelete() {
         logger.debug("form view on delete action");
    }

    /**
     * Clears all the data from the formComponents
     *
     */
    public void onClear(){
        for(IGenericComponent component: formComponents){
            if(!component.isReadonly()) {
                component.clearDataValue();
            }
        }
    }

    private boolean isFormValid() {

        logger.debug("form view validation");
        if (getValidationSupport().isInvalid()) {

            getValidationSupport().initInitialDecoration();

            Notifications.create()
                        .text(I18n.COMMON.getString("notification.saveValidationError"))
                        .position(Pos.TOP_RIGHT).showWarning();
        }
        return !getValidationSupport().isInvalid();
    }

    /**
     * Gets validation support of the form view.
     *
     * @return validation support
     */
    public ExtendedValidationSupport getValidationSupport() {
        if (validationSupport == null) {
            validationSupport = new ExtendedValidationSupport();
            validationSupport.setValidationDecorator(new GraphicValidationDecoration());
           /* validationSupport.setErrorDecorationEnabled(false);*/
        }
        return validationSupport;
    }

    public void showDialog() {
        loadLatestEntity();
        pop();
    }

    private boolean checkFormForReport() {
        /*if (getCurrentEntity().getId() == null) {
            MessageBox.create()
                    .owner(DesktopFxApplication.get().getMainStage())
                    .contentText(I18n.COMMON.getString("reports.formNotSaved"))
                    .showWarning();
            return false;
        }*/
        return true;
    }

    protected void onCloseForm() {
        disposeScreen();
    }

    /**
     * Is the form is opened in edit mode.
     *
     * @return
     */
    public boolean isEditMode()
    {
        if(this.intent.get("is_edit") != null) {
            return  (Boolean)this.intent.get("is_edit");
        }

        return false;
    }

    public List<IGenericComponent> getFormComponents() {
        return formComponents;
    }

    public void setFormComponents(List<IGenericComponent> formComponents) {
        this.formComponents = formComponents;
    }

    @Override
    public IAccelerator getAccelerator() {
        return new IAccelerator() {
            @Override
            public Map<String, Map<KeyCodeCombination, Runnable>> getAccelerators() {
                Map<String,Map<KeyCodeCombination, Runnable>> combinations = new HashMap<String,Map<KeyCodeCombination, Runnable>>();
                Map<KeyCodeCombination,Runnable> childAccelerators = new HashMap<KeyCodeCombination,Runnable>();
                Map<KeyCodeCombination,Runnable> parentAccelerators = new HashMap<KeyCodeCombination,Runnable>();
                for(ActionCommand command: commandButtons){
                    if(!command.isChildAccelarator()) {
                        parentAccelerators.put(command.getKeyCodeCombination(), command::fire);
                    }else{
                        childAccelerators.put(command.getKeyCodeCombination(), command::fire);
                    }
                }
                combinations.put(IAccelerator.CHILD_ACCELERATORS,childAccelerators);
                combinations.put(IAccelerator.PARENT_ACCELERATORS,parentAccelerators);
                return combinations;
            }
        };
    }

    @Override
    public void disposeScreen() {
        stackedScreen.removeScreen();
    }

    @Override
    public String getScreenTitle() {
        return getTitle();
    }

    public abstract void refreshForm();

    public Label getLblformTitle() {
        return lblformTitle;
    }

    public Map<String, Object> getIntent() {
        return intent;
    }

    public void setIntent(Map<String, Object> intent) {
        this.intent = intent;
    }
}
