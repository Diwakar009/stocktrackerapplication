/*
 * Copyright (c) 2020.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.jpa.spring.extended.extention.impl;

import com.stocktracker.jpa.spring.extended.ContractRepository;
import com.stocktracker.jpa.spring.extended.config.ExtendedRepositoryConfig;
import com.stocktracker.model.sms.Contract;
import junit.framework.TestCase;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { ExtendedRepositoryConfig.class })
@Transactional
public class ContractExtendedRepositoryImplTest extends TestCase {

    @Autowired
    private ContractRepository contractRepository;

    private List<Contract> contractList;

    @Before
    public void setUp(){
        contractList = new ArrayList<>();
        Contract contract = new Contract();
        contract.setIndustryCode("KMR1");
        contract.setContractName("KMR_KMSP_2019_2020"); // Auto Generated - Industrycode_ContractType_Year
        contract.setContractTypeCode("KMSP");
        contract.setStartDate(new Date());
        contract.setStatus("OPN");
        contract.setDeliveryProduct("RICE");
        contract.setRawMaterial("PADD");
        contract.setProductDeliveryQuantity(8000d);
        contract.setProductDeliveredQuantity(8000d);
        contract.setApprovedRawMaterialQuantity(7000d);
        contract.setReceivedRawMaterialQuantity(8000d);
        contractList.add(contract);

        contract = new Contract();
        contract.setIndustryCode("SMR");
        contract.setContractName("SMR_KMSP_2019_2020");
        contract.setContractTypeCode("KMSP");
        contract.setStartDate(new Date());
        contract.setStatus("OPN");
        contract.setDeliveryProduct("RICE");
        contract.setRawMaterial("PADD");
        contract.setProductDeliveryQuantity(8000d);
        contract.setProductDeliveredQuantity(8000d);
        contract.setApprovedRawMaterialQuantity(7000d);
        contract.setReceivedRawMaterialQuantity(8000d);
        contractList.add(contract);
    }

    @Test
    public void contactRepositoryTest(){
        Assert.assertEquals(true,contractRepository != null);
    }

    @Test
    @Rollback(true)
    public void testSaveContract(){
        Contract contract = new Contract();
        contract.setIndustryCode("KMR");
        contract.setContractName("KMR_KMSP_2019_2020"); // Auto Generated - Industrycode_ContractType_Year
        contract.setContractTypeCode("KMSP");
        contract.setStartDate(new Date());
        contract.setStatus("OPN");
        contract.setDeliveryProduct("RICE");
        contract.setRawMaterial("PADD");
        contract.setProductDeliveryQuantity(8000d);
        contract.setProductDeliveredQuantity(8000d);
        contract.setApprovedRawMaterialQuantity(7000d);
        contract.setReceivedRawMaterialQuantity(1000d);
        contract.setRemarks("Some Remarks");

        Contract savedContract = contractRepository.save(contract);
        Contract contract1 = contractRepository.getOne(savedContract.getId());
        Assert.assertEquals("KMR_KMSP_2019_2020",savedContract.getContractName());
        Assert.assertEquals("KMSP",savedContract.getContractTypeCode());
        Assert.assertEquals("KMR",savedContract.getIndustryCode());
        Assert.assertEquals("OPN",savedContract.getStatus());
        Assert.assertEquals(contract.getStartDate(),savedContract.getStartDate());
        Assert.assertEquals("RICE",savedContract.getDeliveryProduct());
        Assert.assertEquals("PADD",savedContract.getRawMaterial());
        Assert.assertEquals(new Double(8000d),savedContract.getProductDeliveryQuantity());
        Assert.assertEquals(new Double(8000d),savedContract.getProductDeliveredQuantity());
        Assert.assertEquals(new Double(7000d),savedContract.getApprovedRawMaterialQuantity());
        Assert.assertEquals(new Double(1000d),savedContract.getReceivedRawMaterialQuantity());
        Assert.assertEquals("Some Remarks",savedContract.getRemarks());
    }

    @Test
    @Rollback(true)
    public void testUpdateContract(){
        Contract contract = new Contract();
        contract.setIndustryCode("KMR");
        contract.setContractName("KMR_KMSP_2019_2020"); // Auto Generated - Industrycode_ContractType_Year
        contract.setContractTypeCode("KMSP");
        contract.setStartDate(new Date());
        contract.setStatus("OPN");
        contract.setDeliveryProduct("RICE");
        contract.setRawMaterial("PADD");
        contract.setProductDeliveryQuantity(8000d);
        contract.setProductDeliveredQuantity(8000d);
        contract.setApprovedRawMaterialQuantity(7000d);
        contract.setReceivedRawMaterialQuantity(1000d);
        contract.setRemarks("Some Remarks");
       Contract savedContract = contractRepository.save(contract);
        Contract contract1 = contractRepository.getOne(savedContract.getId());
        contract1.setStatus("CLO");
        savedContract = contractRepository.save(contract1);

        Assert.assertEquals("KMR_KMSP_2019_2020",savedContract.getContractName());
        Assert.assertEquals("KMSP",savedContract.getContractTypeCode());
        Assert.assertEquals("KMR",savedContract.getIndustryCode());
        Assert.assertEquals("CLO",savedContract.getStatus());
        Assert.assertEquals(contract.getStartDate(),savedContract.getStartDate());
        Assert.assertEquals("RICE",savedContract.getDeliveryProduct());
        Assert.assertEquals("PADD",savedContract.getRawMaterial());
        Assert.assertEquals(new Double(8000d),savedContract.getProductDeliveryQuantity());
        Assert.assertEquals(new Double(8000d),savedContract.getProductDeliveredQuantity());
        Assert.assertEquals(new Double(7000d),savedContract.getApprovedRawMaterialQuantity());
        Assert.assertEquals(new Double(1000d),savedContract.getReceivedRawMaterialQuantity());
        Assert.assertEquals("Some Remarks",savedContract.getRemarks());

    }

    @Test(expected = Exception.class)
    @Rollback(true)
    public void testDeleteContract() {
        Contract contract = new Contract();
        contract.setIndustryCode("KMR");
        contract.setContractName("KMR_KMSP_2019_2020"); // Auto Generated - Industrycode_ContractType_Year
        contract.setContractTypeCode("KMSP");
        contract.setStartDate(new Date());
        contract.setStatus("OPN");
        contract.setDeliveryProduct("RICE");
        contract.setRawMaterial("PADD");
        contract.setProductDeliveryQuantity(8000d);
        contract.setProductDeliveredQuantity(8000d);
        contract.setApprovedRawMaterialQuantity(7000d);
        contract.setReceivedRawMaterialQuantity(1000d);
        contract.setRemarks("Some Remarks");

        Contract savedContract = contractRepository.save(contract);
        Contract contract1 = contractRepository.getOne(savedContract.getId());
        contractRepository.delete(contract1);
        contractRepository.getOne(savedContract.getId());
    }


    @Test
    @Rollback(true)
    public void findAllContractByFilter_OnlyIndustryCode() {
        contractRepository.saveAll(contractList);
        Page<Contract> contracts = contractRepository.findAllContractByFilter("KMR",null,null,null,null,null, PageRequest.of(0,10));
        Assert.assertEquals(1,contracts.getTotalElements());
    }

    @Test
    @Rollback(true)
    public void findAllContractByFilter_ByStartDate() {
        contractRepository.saveAll(contractList);
        Page<Contract> contracts = contractRepository.findAllContractByFilter(null,new Date(),null,null,null,null, PageRequest.of(0,10));
        Assert.assertEquals(2,contracts.getTotalElements());
    }

    @Test
    @Rollback(true)
    public void findAllContractByFilter_ByStartDateNStartEndDate() {
        contractRepository.saveAll(contractList);
        Page<Contract> contracts = contractRepository.findAllContractByFilter(null,new Date(),new Date(),null,null,null, PageRequest.of(0,10));
        Assert.assertEquals(2,contracts.getTotalElements());
    }

    @Test
    @Rollback(true)
    public void findAllContractByFilter_OnlyContractName() {
        contractRepository.saveAll(contractList);
        Page<Contract> contracts = contractRepository.findAllContractByFilter(null,null,null,null,"%_KMSP_2019_2020%",null, PageRequest.of(0,10));
        Assert.assertEquals(2,contracts.getTotalElements());
    }

    @Test
    @Rollback(true)
    public void findOpenContract(){
        Contract contract = new Contract();
        contract.setIndustryCode("KMR1");
        contract.setContractName("KMR_KMSP_2019_2020"); // Auto Generated - Industrycode_ContractType_Year
        contract.setContractTypeCode("KMSP");
        contract.setStartDate(new Date());
        contract.setStatus("OPN");
        contract.setDeliveryProduct("RICE");
        contract.setRawMaterial("PADD");
        contract.setProductDeliveryQuantity(8000d);
        contract.setProductDeliveredQuantity(8000d);
        contract.setApprovedRawMaterialQuantity(7000d);
        contract.setReceivedRawMaterialQuantity(1000d);
        contract.setRemarks("Some Remarks");
        Contract savedContract = contractRepository.save(contract);
        Contract contract1 = contractRepository.findOpenContract("KMR1");
        Assert.assertEquals("KMR_KMSP_2019_2020",contract1.getContractName());
    }
}