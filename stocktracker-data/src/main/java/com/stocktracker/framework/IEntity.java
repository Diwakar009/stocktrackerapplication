package com.stocktracker.framework;

import java.io.Serializable;

public interface IEntity {
	public Serializable getEntityPK();

	public void copyEntity(IEntity entity);
}
