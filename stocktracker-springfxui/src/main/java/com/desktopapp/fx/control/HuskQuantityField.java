/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.desktopapp.fx.control;

import com.desktopapp.fx.common.IGenericComponent;
import com.desktopapp.fx.common.Quantity;
import javafx.scene.Node;
import javafx.scene.layout.HBox;

import java.math.BigDecimal;


/**
 * Quantity field for Husk.
 *
 * 1 PKT = 22 Kg
 * 1 Trip = 30 QTL
 *
 * Add an extra check box for the trip identification
 *
 * Created by diwakar009 on 3/12/2018.
 */
public class HuskQuantityField extends QuantityField implements IGenericComponent{

    public static BigDecimal ONE_BAG_TO_QTL = new BigDecimal(0.22); // 22 Kg
    public static BigDecimal ONE_TRIP_TO_QTL = new BigDecimal(30); // 30 QTL

    public static String HUSK_CODE = "HUSK";
    public static String HUSK_TRIP_CODE = "TRIP";
    public static String HUSK_BAG_CODE = "BAG";

    public static String HUSK_WEIGHTMENT_UNIT = "QTL";

    public static String HUSK_TRIP_UNIT = "TRIP";
    public static String HUSK_BAG_UNIT = "BAG";

    private Boolean tripIndicator = false;

    public HuskQuantityField() {
        super();
        init();
    }

    public HuskQuantityField(Boolean tripIndicator) {
        super();
        this.tripIndicator = tripIndicator;
        init();

    }

    public HuskQuantityField(Quantity quantity,Boolean tripIndicator) {
        super(quantity);
        this.tripIndicator = tripIndicator;
        init();
    }

    public void init(){
        this.ifNoOfPackets = new IntegerField();
        this.ifNoOfPackets.setPrefWidth(100);

        this.bfWeightment = new BigDecimalField(2);
        this.bfWeightment.setPrefWidth(100);

        ifNoOfPackets.addValueChangeListener(() -> {
            Long noOfPackets = ifNoOfPackets.getValue();

            if(noOfPackets != null) {
                bfWeightment.setDataValue(new BigDecimal(noOfPackets.longValue()).multiply(!tripIndicator ? ONE_BAG_TO_QTL : ONE_TRIP_TO_QTL));
            }
        });
    }

    public Node asControl(){
        HBox hBox = new HBox();
        this.ifNoOfPackets.setPromptText((!tripIndicator ? HUSK_BAG_UNIT : HUSK_TRIP_UNIT));
        hBox.getChildren().add(this.ifNoOfPackets);
        this.bfWeightment.setPromptText(HUSK_WEIGHTMENT_UNIT);
        hBox.getChildren().add(this.bfWeightment);
        hBox.setSpacing(0);
        return hBox;
    }


    public Boolean getTripIndicator() {
        return tripIndicator;
    }

    public void setTripIndicator(Boolean tripIndicator) {
        this.tripIndicator = tripIndicator;
    }
}
