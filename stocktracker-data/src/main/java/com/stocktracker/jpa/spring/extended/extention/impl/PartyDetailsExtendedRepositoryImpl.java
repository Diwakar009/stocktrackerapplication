/*
 * Copyright (c) 2020.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.jpa.spring.extended.extention.impl;

import com.stocktracker.jpa.spring.extended.extention.PartyDetailsExtendedRepository;
import com.stocktracker.jpa.spring.extended.extention.StockDetailsExtendedRepository;
import com.stocktracker.model.sms.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by diwakar009 on 22/10/2019.
 */
@Repository
public class PartyDetailsExtendedRepositoryImpl implements PartyDetailsExtendedRepository {

    private final Logger logger = LoggerFactory.getLogger(PartyDetailsExtendedRepositoryImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<PartyDetails> findPartyNames(String partyName) {
        logger.info("Executing findPartyNames...");
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<PartyDetails> mainCreteriaQuery = criteriaBuilder.createQuery(PartyDetails.class);
        Root<PartyDetails> mainRoot = mainCreteriaQuery.from(PartyDetails.class);

        mainCreteriaQuery.select(criteriaBuilder.construct(PartyDetails.class,mainRoot.get("partyName1")
                , mainRoot.get("partyName2"), mainRoot.get("partyName3"))).distinct(true);

        Optional<ParameterExpression<String>> partyNameFilter = Optional.empty();

        List<Predicate> predicates = new ArrayList<Predicate>();

        if(partyNameFilter != null && !"".equals(partyNameFilter)) {
            partyNameFilter = Optional.ofNullable(criteriaBuilder.parameter(String.class));
            predicates.add(criteriaBuilder.like(mainRoot.get("partyName1"), partyNameFilter.get()));
        }

        mainCreteriaQuery.where(predicates.toArray(new Predicate[predicates.size()])); // Apply where clause
        TypedQuery<PartyDetails> mainQuery = entityManager.createQuery(mainCreteriaQuery);

        if(partyNameFilter.isPresent()) {
            mainQuery.setParameter(partyNameFilter.get(), "%"+ partyName + "%");
        }

        List<PartyDetails> partyDetailsList = mainQuery.setMaxResults(10).getResultList();
        //List<String> partyNames = partyNameList.stream().map(partyDetails -> partyDetails.getPartyName1()).collect(Collectors.toList());
        return partyDetailsList;
    }

    protected Page<StockDetails> readPage(TypedQuery<StockDetails> query, Pageable pageable, Long total) {
        query.setFirstResult((int) pageable.getOffset());
        query.setMaxResults(pageable.getPageSize());
        List<StockDetails> content = total.longValue() > (long)pageable.getOffset()?query.getResultList(): Collections.emptyList();
        return new PageImpl(content, pageable, total.longValue());
    }

}
