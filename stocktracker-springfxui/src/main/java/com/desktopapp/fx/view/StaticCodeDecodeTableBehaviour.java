/*
 * Copyright (c) 2020.
 *
 * All Rights are with Diwakar C
 */

package com.desktopapp.fx.view;

import com.desktopapp.fx.app.AppFontIcons;
import com.desktopapp.fx.common.ITableDataViewBehaviour;
import com.desktopapp.fx.common.PageDetails;
import com.desktopapp.fx.control.TableColumnBuilder;
import com.desktopapp.fx.control.fonticon.FontIcon;
import com.desktopapp.fx.controller.StaticCodeDecodeController;
import com.desktopapp.fx.util.I18n;
import com.stocktracker.domainobjs.json.DOPage;
import com.stocktracker.domainobjs.json.StaticCodeDecodeDO;
import com.stocktracker.domainobjs.json.TableRowMapDO;
import javafx.scene.Node;
import javafx.scene.control.TableColumn;

import java.util.ArrayList;
import java.util.List;

/**
 * Static code decode Table properties and behaviour
 */
 class StaticCodeDecodeTableBehaviour implements ITableDataViewBehaviour<StaticCodeDecodeDO> {

    private  StaticCodeDecodeForm form;

    public StaticCodeDecodeTableBehaviour(StaticCodeDecodeForm form) {
        this.form = form;
    }

    @Override
    public List<TableColumn<StaticCodeDecodeDO, ?>> getTableViewColumns() {
        List<TableColumn<StaticCodeDecodeDO, ?>> columns = new ArrayList<>();

        TableColumn<StaticCodeDecodeDO, String> codeNameCol = TableColumnBuilder.<StaticCodeDecodeDO, String> create()
                .fieldName("codeName")
                .prefWidth(100)
                .title(I18n.STATICCODEDECODE.getString("table.codeName"))
                .build();

        columns.add(codeNameCol);

        TableColumn<StaticCodeDecodeDO, String> languageCol = TableColumnBuilder.<StaticCodeDecodeDO, String> create()
                .fieldName("language")
                .prefWidth(100)
                .title(I18n.STATICCODEDECODE.getString("table.language"))
                .build();

        columns.add(languageCol);

        TableColumn<StaticCodeDecodeDO, String> codeValueFilterCol = TableColumnBuilder.<StaticCodeDecodeDO, String> create()
                .fieldName("codeValueFilter")
                .prefWidth(100)
                .title(I18n.STATICCODEDECODE.getString("table.codeValueFilter"))
                .build();

        columns.add(codeValueFilterCol);

        TableColumn<StaticCodeDecodeDO, String> codeValueCol = TableColumnBuilder.<StaticCodeDecodeDO, String> create()
                .fieldName("codeValue")
                .prefWidth(100)
                .title(I18n.STATICCODEDECODE.getString("table.codeValue"))
                .build();
        columns.add(codeValueCol);

        TableColumn<StaticCodeDecodeDO, String> codeDescCol = TableColumnBuilder.<StaticCodeDecodeDO, String> create()
                .fieldName("codeDesc")
                .title(I18n.STATICCODEDECODE.getString("table.codeDesc"))
                .build();

        columns.add(codeDescCol);
        return columns;
    }

    @Override
    public DOPage<TableRowMapDO<String, String>> getData(String filter, PageDetails details) {
        return ((StaticCodeDecodeController)getForm().getController()).listByCodeNameNLang(form.getStaticCodeDecodeDO().getCodeName(), filter, form.getStaticCodeDecodeDO().getLanguage(), details);
    }

    @Override
    public StaticCodeDecodeDO mapTableRowToDO(TableRowMapDO<String,String> tableRowMapDO) {
        StaticCodeDecodeDO staticCodeDecodeDO = new StaticCodeDecodeDO();
        staticCodeDecodeDO.setCodeName(tableRowMapDO.getValue("codeName"));
        staticCodeDecodeDO.setCodeValue(tableRowMapDO.getValue("codeValue"));
        staticCodeDecodeDO.setCodeDesc(tableRowMapDO.getValue("codeDesc"));
        staticCodeDecodeDO.setLanguage(tableRowMapDO.getValue("language"));
        staticCodeDecodeDO.setCodeValueFilter(tableRowMapDO.getValue("codeValueFilter"));
        return staticCodeDecodeDO;
    }

    @Override
    public void onSelectedItemChanged() {
        StaticCodeDecodeDO selectedItem = (StaticCodeDecodeDO) getForm().getTableDataView().getTableView().getSelectionModel().getSelectedItem();
        if(selectedItem != null) {
            getForm().setDOToControls(selectedItem);
        }
    }

    @Override
    public Node asNode() {
        return null;
    }

    @Override
    public FontIcon getFontIcon() {
        return AppFontIcons.STATICCODEDECODE;
    }

    @Override
    public String getTitle() {
        return I18n.STATICCODEDECODE.getString("title");
    }

    public StaticCodeDecodeForm getForm() {
        return form;
    }

    public void setForm(StaticCodeDecodeForm form) {
        this.form = form;
    }

    @Override
    public Boolean hasSearchActions() {
        return true;
    }
}