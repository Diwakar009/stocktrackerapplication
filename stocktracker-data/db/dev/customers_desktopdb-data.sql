-- Customers Desktop Database Data
-- Version 1.0

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

USE customers_desktopdb;

--
-- Dumping data for table country
--
SET AUTOCOMMIT=0;
INSERT INTO country 
VALUES (1, 'TR', 'Turkey', NULL, 'Admin', '2013-10-12 14:34:29', NULL, NULL),
(2, 'DE', 'Germany', NULL, 'Admin', '2013-10-12 14:34:30', NULL, NULL),
(3, 'GB', 'England', NULL, 'Admin', '2013-10-12 14:34:31', NULL, NULL);
COMMIT;

--
-- Dumping data for table category
--
SET AUTOCOMMIT=0;
INSERT INTO category 
VALUES (1, 'Discounter', NULL, 'Admin', '2013-10-12 14:34:32', NULL, NULL),
(2, 'Reseller', NULL, 'Admin', '2013-10-12 14:34:33', NULL, NULL),
(3, 'End user', NULL, 'Admin', '2013-10-12 14:34:34', NULL, NULL);
COMMIT;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
