/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.desktopapp.fx.control;

import javafx.geometry.Pos;
import javafx.scene.control.Separator;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by diwakar009 on 12/11/2019.
 */
public class ApplicationInfoPane extends VBox {


    public ApplicationInfoPane() {

    }

    /**
     * Add and show error messages
     *
     * @param errorMessages
     */
    public void showErrorMessage(List<String> errorMessages){

        clearMessages();
        errorMessages.forEach(errorMessage -> {
            HBox box = new HBox();
            box.setStyle("-fx-background-color : #e47c68");
            box.setPrefHeight(20);
            Text errorText = new Text("ERROR:" + errorMessage);
            errorText.setStyle(" -fx-padding: 3px 0px 0px 0px;-fx-font-weight: bold");
            HBox.setHgrow(box, Priority.ALWAYS);
            box.getChildren().add(errorText);
            getChildren().add(box);

            getChildren().add(new Separator());
        });

    }


    /**
     * Add and show success messages
     *
     * @param successMessages
     */
    public void showSuccessMessage(List<String> successMessages){

        clearMessages();
        successMessages.forEach(successMessage -> {
            HBox box = new HBox();
            box.setStyle("-fx-background-color : #4cbe83");
            box.setPrefHeight(20);
            //box.setAlignment(Pos.CENTER);
            Text successText = new Text("SUCCESS: " + successMessage);
            successText.setStyle("-fx-padding: 3px 0px 0px 0px;-fx-font-weight: bold");
            HBox.setHgrow(box, Priority.ALWAYS);
            box.getChildren().add(successText);
            getChildren().add(box);

            getChildren().add(new Separator());
        });

    }

    /**
     * Add and show success messages
     *
     * alernateColorInd 0 or 1 to show the difference in update in single screen.
     *
     * @param successMessages
     * @param alternateColorInd
     */
    public void showSuccessMessage(List<String> successMessages,boolean alternateColorInd){

        clearMessages();
        successMessages.forEach(successMessage -> {
            HBox box = new HBox();
            if (alternateColorInd) {
                box.setStyle("-fx-background-color : #4cbe83");
            } else {
                box.setStyle("-fx-background-color : #4cbeaf");
            }
            box.setPrefHeight(20);
            //box.setAlignment(Pos.CENTER);
            Text successText = new Text("SUCCESS: " + successMessage);
            successText.setStyle("-fx-padding: 3px 0px 0px 0px;-fx-font-weight: bold");
            HBox.setHgrow(box, Priority.ALWAYS);
            box.getChildren().add(successText);
            getChildren().add(box);

            getChildren().add(new Separator());
        });

    }

    /**
     * Add and show success messages
     *
     * @param warningMessages
     */
    public void showWarningMessage(List<String> warningMessages){

        clearMessages();
        warningMessages.forEach(warningMessage -> {
            HBox box = new HBox();
            box.setStyle("-fx-background-color : #feb742");
            box.setPrefHeight(20);
            Text errorText = new Text("WARNING: "  + warningMessage);
            errorText.setStyle("-fx-padding: 3px 0px 0px 0px;-fx-font-weight: bold");
            HBox.setHgrow(box, Priority.ALWAYS);
            box.getChildren().add(errorText);
            getChildren().add(box);
            getChildren().add(new Separator());
        });
        // #77d3e0 Notice background color
    }

    public void showMessages(List<String> errorMessages, List<String> warningMessages){

        clearMessages();

        errorMessages.forEach(errorMessage -> {
            HBox box = new HBox();
            box.setStyle("-fx-background-color : #e47c68;");
            box.setPrefHeight(20);
            Text errorText = new Text("ERROR: "  + errorMessage);
            errorText.setStyle("-fx-padding: 3px 0px 0px 0px;-fx-font-weight: bold");
            HBox.setHgrow(box, Priority.ALWAYS);
            box.getChildren().add(errorText);
            getChildren().add(box);

            getChildren().add(new Separator());
        });

        warningMessages.forEach(warningMessage -> {
            HBox box = new HBox();
            box.setStyle("-fx-background-color : #feb742");
            box.setPrefHeight(20);
            Text errorText = new Text("WARNING: " + warningMessage);
            errorText.setStyle("-fx-padding: 3px 0px 0px 0px;-fx-font-weight: bold");
            HBox.setHgrow(box, Priority.ALWAYS);
            box.getChildren().add(errorText);
            getChildren().add(box);
            getChildren().add(new Separator());
        });

    }

    /**
     * add single error messages
     *
     * @param errorMessages
     */
    public void showErrorMessage(String errorMessages){
        List<String> errorMesssages = new ArrayList<String>();
        errorMesssages.add(errorMessages);

        showErrorMessage(errorMesssages);
    }

    /**
     * Success messages
     *
     * @param successMessages
     */
    public void showSuccessMessage(String successMessages){
        List<String> msgs = new ArrayList<String>();
        msgs.add(successMessages);

        showSuccessMessage(msgs);
    }

    /**
     * Success messages
     *
     * @param successMessages
     */
    public void showSuccessMessage(String successMessages,boolean alternateColorInd){
        List<String> msgs = new ArrayList<String>();
        msgs.add(successMessages);

        showSuccessMessage(msgs,alternateColorInd);
    }


    /**
     * Warning messages
     *
     * @param warningMessages
     */
    public void showWarningMessage(String warningMessages){
        List<String> msgs = new ArrayList<String>();
        msgs.add(warningMessages);

        showWarningMessage(msgs);
    }

    /**
     * Clear messages
     */
    public void clearMessages(){
        getChildren().clear();
    }

}
