/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.desktopapp.bean;

import com.stocktracker.domainobjs.json.sms.StockDetailsDO;

/**
 * Created by diwakar009 on 5/11/2019.
 */
public class ContractChangeIdentier implements Comparable <ContractChangeIdentier> {

    private String industryCode;
    private String transactionType;
    private Double weightment;

    public ContractChangeIdentier(StockDetailsDO stockDetailsDO) {
        this.setIndustryCode(stockDetailsDO.getOnBeHalf());
        this.setTransactionType(stockDetailsDO.getTransactionType());
        this.setWeightment(stockDetailsDO.getTotalWeightment());
    }

    public String getIndustryCode() {
        return industryCode;
    }

    public void setIndustryCode(String industryCode) {
        this.industryCode = industryCode;
    }


    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public Double getWeightment() {
        return weightment;
    }

    public void setWeightment(Double weightment) {
        this.weightment = weightment;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ContractChangeIdentier that = (ContractChangeIdentier) o;

        if (industryCode != null ? !industryCode.equals(that.industryCode) : that.industryCode != null) return false;
        if (transactionType != null ? !transactionType.equals(that.transactionType) : that.transactionType != null)
            return false;
        return weightment != null ? weightment.equals(that.weightment) : that.weightment == null;
    }

    @Override
    public int hashCode() {
        int result = industryCode != null ? industryCode.hashCode() : 0;
        result = 31 * result + (transactionType != null ? transactionType.hashCode() : 0);
        result = 31 * result + (weightment != null ? weightment.hashCode() : 0);
        return result;
    }

    @Override
    public int compareTo(ContractChangeIdentier o) {

        if(this.equals(o)){
            return 0;
        }

        return -1;

    }
}
