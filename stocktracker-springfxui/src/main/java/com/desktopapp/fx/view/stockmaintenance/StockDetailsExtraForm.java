/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.desktopapp.fx.view.stockmaintenance;

import com.desktopapp.fx.common.IGenericComponent;
import com.desktopapp.fx.control.CodeDecodeField;
import com.desktopapp.fx.control.TextAreaExt;
import com.stocktracker.component.constants.CodeDecodeConstants;
import com.stocktracker.domainobjs.json.sms.StockDetailsDO;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.*;
import javafx.scene.text.Text;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by diwakar009 on 10/11/2019.
 */
public class StockDetailsExtraForm extends StackPane{


    private List<IGenericComponent> components = new ArrayList<>();

    private TextAreaExt taRemarks;
    private CodeDecodeField cdOnBehalf;
    private CodeDecodeField cdRevisitFlag;
    private StockDetailsTransactionForm stockDetailsTransactionForm;

    public StockDetailsExtraForm(StockDetailsTransactionForm stockDetailsTransactionForm) {
        this.stockDetailsTransactionForm=stockDetailsTransactionForm;
        this.taRemarks = new TextAreaExt(100);
        this.taRemarks.setPrefHeight(50);
        this.taRemarks.setPrefWidth(200);

        this.cdOnBehalf = new CodeDecodeField(CodeDecodeConstants.CD_INDUSTRY_CODE,CodeDecodeConstants.DEFAULT_LANGUAGE);
        this.cdRevisitFlag = new CodeDecodeField(CodeDecodeConstants.CD_YESNO,CodeDecodeConstants.DEFAULT_LANGUAGE);
        this.cdRevisitFlag.setDataValue("NO");

        build();
        registerComponents();

    }

    public void build(){
        int columnIndex = 0;
        int rowIndex = 0;

        GridPane formPane = new GridPane();
        formPane.setPadding(new Insets(10, 10, 10, 10));
        formPane.setHgap(10);
        formPane.setVgap(10);
        formPane.setAlignment(Pos.CENTER);

        formPane.setGridLinesVisible(false);

        ColumnConstraints column1 = new ColumnConstraints();
        column1.setPercentWidth(50);

        ColumnConstraints column2 = new ColumnConstraints();
        column2.setPercentWidth(100);

       /* ColumnConstraints column3 = new ColumnConstraints();
        column3.setPercentWidth(2);*/

        formPane.getColumnConstraints().addAll(column1,column2/*,column3,column1,column2*/);

        formPane.add(new Label("On BehalfOf"), columnIndex, rowIndex);
        formPane.add(this.cdOnBehalf.asControl(), columnIndex + 1, rowIndex++);

        formPane.add(new Label("Remarks"), columnIndex, rowIndex);
        formPane.add(this.taRemarks, columnIndex + 1, rowIndex++);

        formPane.add(new Label("Revisit Again"), columnIndex, rowIndex);
        formPane.add(this.cdRevisitFlag.asControl(), columnIndex + 1, rowIndex++);

        VBox vbox = new VBox();
        HBox formPaneHBox = new HBox();
        formPaneHBox.getChildren().add(formPane);

        vbox.getChildren().add(formPaneHBox);

        this.getChildren().add(vbox);

        //setStyle("-fx-border-color: black");
    }

    /**
     * Get Generic Components
     *
     * @return
     */
    private List<IGenericComponent> registerComponents(){
        components.add(taRemarks);
        components.add(cdOnBehalf);
        components.add(cdRevisitFlag);
        return components;
    }


    /**
     * Clear all
     */
    public void clearAll(){
        components.forEach(component -> component.clearDataValue());
    }

    /**
     * Set DO to Controls
     *
     * @param stockDetailsDO
     */
    public void setDOToControls(StockDetailsDO stockDetailsDO){

        if(stockDetailsDO != null) {
            this.taRemarks.setDataValue(stockDetailsDO.getRemarks());
            this.cdOnBehalf.setDataValue(stockDetailsDO.getOnBeHalf());
            this.cdRevisitFlag.setDataValue(stockDetailsDO.getRevisitFlag());
        }

    }

    /**
     * Set controls to DO
     *
     * @param stockDetailsDO
     */
    public void setControlsToDO(StockDetailsDO stockDetailsDO){
        if(stockDetailsDO != null) {
            stockDetailsDO.setRemarks(this.taRemarks.getDataValue());

            if(this.cdOnBehalf.getDataValue().isEmpty()){
                cdOnBehalf.setDataValue(stockDetailsDO.getIndustryCode());
            }

            stockDetailsDO.setOnBeHalf(this.cdOnBehalf.getDataValue());

            if(this.cdRevisitFlag.getDataValue().isEmpty()){
                cdRevisitFlag.setDataValue("N");
            }

            stockDetailsDO.setRevisitFlag(this.cdRevisitFlag.getDataValue());


        }
    }

    public List<IGenericComponent> getComponents() {
        return components;
    }

    public void setComponents(List<IGenericComponent> components) {
        this.components = components;
    }
}
