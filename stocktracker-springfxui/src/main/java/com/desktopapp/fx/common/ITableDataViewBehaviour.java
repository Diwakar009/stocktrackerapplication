package com.desktopapp.fx.common;

import com.desktopapp.fx.mvc.View;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import com.stocktracker.domainobjs.json.DOPage;
import com.stocktracker.domainobjs.json.TableRowMapDO;

import java.util.List;

/**
 * Created by diwakar009 on 24/9/2019.
 */
public interface ITableDataViewBehaviour<T> extends View{

    public List<TableColumn<T, ?>> getTableViewColumns();
    public DOPage<TableRowMapDO<String, String>> getData(String filter, PageDetails details);
    public T mapTableRowToDO(TableRowMapDO<String, String> tableRowMapDO);

    public default void onSelectedItemChanged(){};
    /**
     * Button E
     */
    public default void onAddNew(){};
    public default void onEdit(){};
    public default void onDelete(){};
    public default void onExportToExcel(){};



    public default Boolean hasRowHighlighting(T item){return false;}
    public default Boolean hasAddNewActions() {
        return false;
    }
    public default Boolean hasEditActions() {
        return false;
    }
    public default Boolean hasDeleteActions() {
        return false;
    }
    public default Boolean hasSearchActions() {
        return false;
    }
    public default boolean hasExportToExcelAction(){return false;};

    public default Boolean hasPrintActions() {
        return false;
    }
    public default void onPrintPreview(){};
    public default void onPrint(){};
    public default void onPdf(){};

    public default List<MenuItem> getFloatingMenuItems(){ return null;};


}


