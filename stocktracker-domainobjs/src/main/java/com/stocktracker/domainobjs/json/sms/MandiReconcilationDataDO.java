/*
 * Copyright (c) 2020.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.domainobjs.json.sms;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.stocktracker.jpa.spring.extended.bean.MandiReconcilationData;

import java.io.Serializable;
import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown=true)
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class MandiReconcilationDataDO implements Serializable {

    @JsonProperty("id")
    private Long id;

    @JsonProperty("industryCode")
    private String industryCode;

    @JsonProperty("stockCode")
    private String stockCode;

    @JsonProperty("stockVarietyCode")
    private String stockVarietyCode;

    @JsonProperty("transactionType")
    private String transactionType;

    @JsonProperty("lampNameCode")
    private String lampNameCode;

    @JsonProperty("mandiNameCode")
    private String mandiNameCode;

    @JsonProperty("partyName")
    private String partyName;

    @JsonProperty("totalReceived")
    private Double totalReceived;

    @JsonProperty("totalAdjusted")
    private Double totalAdjusted;

    @JsonProperty("totalReceivedNAdjustedNPurchased")
    private Double totalReceivedNAdjustedNPurchased;

    @JsonProperty("totalEntered")
    private Double totalEntered;

    @JsonProperty("lampEntryAdjustment")
    private Double lampEntryAdjustment;

    @JsonProperty("totalLampEntryAfterAdjustment")
    private Double totalLampEntryAfterAdjustment;

    @JsonProperty("difference")
    private Double difference;

    @JsonProperty("onBehalf")
    private String onBehalf;

    @JsonProperty("transactionDate")
    private Date transactionDate;

    @JsonProperty("vehicleNumber")
    private String vehicleNumber;

    @JsonProperty("rstNumber")
    private String rstNumber;

    @JsonProperty("totalExcessPurchased")
    private Double totalExcessPurchased;

    public MandiReconcilationDataDO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIndustryCode() {
        return industryCode;
    }

    public void setIndustryCode(String industryCode) {
        this.industryCode = industryCode;
    }

    public String getStockCode() {
        return stockCode;
    }

    public void setStockCode(String stockCode) {
        this.stockCode = stockCode;
    }

    public String getStockVarietyCode() {
        return stockVarietyCode;
    }

    public void setStockVarietyCode(String stockVarietyCode) {
        this.stockVarietyCode = stockVarietyCode;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getLampNameCode() {
        return lampNameCode;
    }

    public void setLampNameCode(String lampNameCode) {
        this.lampNameCode = lampNameCode;
    }

    public String getMandiNameCode() {
        return mandiNameCode;
    }

    public void setMandiNameCode(String mandiNameCode) {
        this.mandiNameCode = mandiNameCode;
    }

    public String getPartyName() {
        return partyName;
    }

    public void setPartyName(String partyName) {
        this.partyName = partyName;
    }

    public Double getTotalReceived() {
        return totalReceived;
    }

    public void setTotalReceived(Double totalReceived) {
        this.totalReceived = totalReceived;
    }

    public Double getTotalAdjusted() {
        return totalAdjusted;
    }

    public void setTotalAdjusted(Double totalAdjusted) {
        this.totalAdjusted = totalAdjusted;
    }

    public Double getTotalReceivedNAdjustedNPurchased() {
        return totalReceivedNAdjustedNPurchased;
    }

    public void setTotalReceivedNAdjustedNPurchased(Double totalReceivedNAdjustedNPurchased) {
        this.totalReceivedNAdjustedNPurchased = totalReceivedNAdjustedNPurchased;
    }

    public Double getTotalEntered() {
        return totalEntered;
    }

    public void setTotalEntered(Double totalEntered) {
        this.totalEntered = totalEntered;
    }

    public Double getLampEntryAdjustment() {
        return lampEntryAdjustment;
    }

    public void setLampEntryAdjustment(Double lampEntryAdjustment) {
        this.lampEntryAdjustment = lampEntryAdjustment;
    }

    public Double getTotalLampEntryAfterAdjustment() {
        return totalLampEntryAfterAdjustment;
    }

    public void setTotalLampEntryAfterAdjustment(Double totalLampEntryAfterAdjustment) {
        this.totalLampEntryAfterAdjustment = totalLampEntryAfterAdjustment;
    }

    public Double getDifference() {
        return difference;
    }

    public void setDifference(Double difference) {
        this.difference = difference;
    }

    public String getOnBehalf() {
        return onBehalf;
    }

    public void setOnBehalf(String onBehalf) {
        this.onBehalf = onBehalf;
    }

    public String getVehicleNumber() {
        return vehicleNumber;
    }

    public void setVehicleNumber(String vehicleNumber) {
        this.vehicleNumber = vehicleNumber;
    }

    public void copyEntity2DomainObject(MandiReconcilationData mandiReconcilationData) {

        setId(mandiReconcilationData.getId());
        setIndustryCode(mandiReconcilationData.getIndustryCode());
        setStockCode(mandiReconcilationData.getStockCode());
        setStockVarietyCode(mandiReconcilationData.getStockVarietyCode());
        setTransactionType(mandiReconcilationData.getTransactionType());
        setMandiNameCode(mandiReconcilationData.getMandiNameCode());
        setLampNameCode(mandiReconcilationData.getLampNameCode());
        setPartyName(mandiReconcilationData.getPartyName());
        setTotalReceived(mandiReconcilationData.getTotalReceived());
        setTotalAdjusted(mandiReconcilationData.getTotalAdjusted());
        setTotalEntered(mandiReconcilationData.getTotalEntered());
        setTotalReceivedNAdjustedNPurchased(mandiReconcilationData.getTotalReceivedNAdjustedNPurchased());
        setLampEntryAdjustment(mandiReconcilationData.getLampEntryAdjustment());
        setTotalLampEntryAfterAdjustment(mandiReconcilationData.getTotalLampEntryAfterAdjustment());
        setDifference(mandiReconcilationData.getDifference());
        setOnBehalf(mandiReconcilationData.getOnBehalf());
        setTransactionDate(mandiReconcilationData.getTransactionDate());
        setVehicleNumber(mandiReconcilationData.getVehicleNumber());
        setRstNumber(mandiReconcilationData.getRstNumber());
        setTotalExcessPurchased(mandiReconcilationData.getTotalExcessPurchased());
    }

    public Double getTotalExcessPurchased() {
        return totalExcessPurchased;
    }

    public void setTotalExcessPurchased(Double totalExcessPurchased) {
        this.totalExcessPurchased = totalExcessPurchased;
    }

    public void setRstNumber(String rstNumber) {
        this.rstNumber = rstNumber;
    }

    public String getRstNumber() {
        return rstNumber;
    }

    @Override
    public String toString() {
        return "MandiStockDetailsDO{" +
                "id=" + id +
                ", industryCode='" + industryCode + '\'' +
                ", stockCode='" + stockCode + '\'' +
                ", stockVarietyCode='" + stockVarietyCode + '\'' +
                ", transactionType='" + transactionType + '\'' +
                ", lampNameCode='" + lampNameCode + '\'' +
                ", mandiNameCode='" + mandiNameCode + '\'' +
                ", partyName='" + partyName + '\'' +
                ", totalReceived=" + totalReceived +
                ", totalAdjusted=" + totalAdjusted +
                ", totalReceivedNAdjusted=" + totalReceivedNAdjustedNPurchased +
                ", totalEntered=" + totalEntered +
                ", lampEntryAdjustment=" + lampEntryAdjustment +
                ", totalLampEntryAfterAdjustment=" + totalLampEntryAfterAdjustment +
                ", difference=" + difference +
                ", onBehalf='" + onBehalf + '\'' +
                ", transactionDate='" + transactionDate + '\'' +
                ", vehicleNumber='" + vehicleNumber + '\'' +
                ", rstNumber='" + rstNumber + '\'' +
                ", totalExcessPurchased='" + totalExcessPurchased + '\'' +
                '}';
    }


    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }
}
