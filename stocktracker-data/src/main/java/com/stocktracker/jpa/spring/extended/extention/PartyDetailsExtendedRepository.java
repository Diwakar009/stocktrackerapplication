/*
 * Copyright (c) 2020.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.jpa.spring.extended.extention;

import com.stocktracker.model.sms.PartyDetails;
import com.stocktracker.model.sms.StockDetails;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Date;
import java.util.List;

/**
 * Created by diwakar009 on 22/10/2019.
 */
public interface PartyDetailsExtendedRepository {
    public List<PartyDetails> findPartyNames(String partyName);
}
