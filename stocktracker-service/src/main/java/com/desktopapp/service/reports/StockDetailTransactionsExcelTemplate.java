/*
 * Copyright (c) 2020.
 *
 * All Rights are with Diwakar C
 */

package com.desktopapp.service.reports;

import com.stocktracker.excel.ExcelUtils;
import com.stocktracker.exceptions.ExcelCellRenderingException;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;

import java.util.HashMap;
import java.util.Map;

public class StockDetailTransactionsExcelTemplate implements ExcelReportTemplate{
    public static Integer NO_OF_COLUMNS = Integer.valueOf(18);
    public static Integer INDEX_OF_DATA_ROW = Integer.valueOf(2);
    public static final String TEMPLATE_FILE_NAME = "../templates/StockDetailTransactionsTemplate.xls";
    public Workbook workbook;
    public Map<Integer, CellStyle> cellStyleMap;
    public Map<Integer, CellStyle> summaryCellStyleMap;

    public StockDetailTransactionsExcelTemplate(Workbook workbook) {
        this.workbook = workbook;
        this.cellStyleMap = getTemplateCellStyles(this.workbook);
        this.summaryCellStyleMap = getTemplateSummaryCellStyles(this.workbook,this.cellStyleMap);
    }

    private Map<Integer, CellStyle> getTemplateCellStyles(Workbook workbook) {
        Map<Integer, CellStyle> cellStyleMap = new HashMap<>();
        Integer numberOfCol = NO_OF_COLUMNS;
        Integer row = INDEX_OF_DATA_ROW;
        Sheet sheet = workbook.getSheet("Template");
        if(sheet != null){
            for (int col = 0; col <= numberOfCol; col++) {
                CellStyle cellStyle = sheet.getRow(row).getCell(col).getCellStyle();
                cellStyleMap.put(col, cellStyle);
            }
        }
        return cellStyleMap;
    }

    private Map<Integer, CellStyle> getTemplateSummaryCellStyles(Workbook workbook, Map<Integer, CellStyle> cellStyleMap) {
        Map<Integer, CellStyle> summaryCellStyleMap = new HashMap<>();
        cellStyleMap.forEach((integer, cellStyle) -> {
            CellStyle cellStyle1 = workbook.createCellStyle();
            cellStyle1.cloneStyleFrom(cellStyle);
            cellStyle1.setFillForegroundColor(IndexedColors.BLUE_GREY.getIndex());
            cellStyle1.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            Font font = workbook.createFont();
            font.setBold(true);
            cellStyle1.setFont(font);
            summaryCellStyleMap.put(integer, cellStyle1);
        });
        return summaryCellStyleMap;
    }

    @Override
    public Workbook getWorkbook() {
        return workbook;
    }

    public Map<Integer, CellStyle> getCellStyleMap() {
        return cellStyleMap;
    }

    public void applySummaryRow(Sheet sheet, Integer row) throws ExcelCellRenderingException {
        Row currentRow = sheet.getRow(row);
        if(currentRow == null){
            currentRow = sheet.createRow(row);
        }
        int col = 0;
        ExcelUtils.setCellStyle(currentRow,col,this.summaryCellStyleMap.get(col++));
        ExcelUtils.setCellStyle(currentRow,col,this.summaryCellStyleMap.get(col++));
        ExcelUtils.setCellStyle(currentRow,col,this.summaryCellStyleMap.get(col++));
        ExcelUtils.setCellStyle(currentRow,col,this.summaryCellStyleMap.get(col++));

        ExcelUtils.setCellValue(currentRow,col,"Total",this.summaryCellStyleMap.get(col));
        col++;
        ExcelUtils.setCellStyle(currentRow,col,this.summaryCellStyleMap.get(col));
        ExcelUtils.setCellFormula(currentRow,col,"SUBTOTAL(109," + new CellRangeAddress(INDEX_OF_DATA_ROW, row - 1 , col, col).formatAsString() + ")");
        col++;
        ExcelUtils.setCellStyle(currentRow,col,this.summaryCellStyleMap.get(col));
        ExcelUtils.setCellFormula(currentRow,col,"SUBTOTAL(109," + new CellRangeAddress(INDEX_OF_DATA_ROW, row -1, col, col).formatAsString() + ")");
        col++;
        ExcelUtils.setCellStyle(currentRow,col,this.summaryCellStyleMap.get(col));
        ExcelUtils.setCellFormula(currentRow,col,"SUBTOTAL(101," + new CellRangeAddress(INDEX_OF_DATA_ROW, row -1, col, col).formatAsString() + ")");
        col++;
        ExcelUtils.setCellStyle(currentRow,col,this.summaryCellStyleMap.get(col));
        ExcelUtils.setCellFormula(currentRow,col,"SUBTOTAL(109," + new CellRangeAddress(INDEX_OF_DATA_ROW, row -1, col, col).formatAsString() + ")");
        col++;
        ExcelUtils.setCellStyle(currentRow,col,this.summaryCellStyleMap.get(col++));
        ExcelUtils.setCellStyle(currentRow,col,this.summaryCellStyleMap.get(col++));
        ExcelUtils.setCellStyle(currentRow,col,this.summaryCellStyleMap.get(col++));

        ExcelUtils.setCellStyle(currentRow,col,this.summaryCellStyleMap.get(col));
        ExcelUtils.setCellFormula(currentRow,col,"SUBTOTAL(109," + new CellRangeAddress(INDEX_OF_DATA_ROW, row -1, col, col).formatAsString() + ")");
        col++;

        ExcelUtils.setCellStyle(currentRow,col,this.summaryCellStyleMap.get(col));
        ExcelUtils.setCellFormula(currentRow,col,"SUBTOTAL(109," + new CellRangeAddress(INDEX_OF_DATA_ROW, row -1, col, col).formatAsString() + ")");
        col++;

        ExcelUtils.setCellStyle(currentRow,col,this.summaryCellStyleMap.get(col++));
        ExcelUtils.setCellStyle(currentRow,col,this.summaryCellStyleMap.get(col++));
        ExcelUtils.setCellStyle(currentRow,col,this.summaryCellStyleMap.get(col++));
        ExcelUtils.setCellStyle(currentRow,col,this.summaryCellStyleMap.get(col));
        ExcelUtils.setCellFormula(currentRow,col,"SUBTOTAL(4," + new CellRangeAddress(INDEX_OF_DATA_ROW, row -1, col, col).formatAsString() + ")");
        col++;
        ExcelUtils.setCellStyle(currentRow,col,this.summaryCellStyleMap.get(col++));


    }
}
