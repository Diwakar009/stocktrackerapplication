package com.desktopapp.fx.session;

import com.desktopapp.fx.app.AppAccelerator;
import com.desktopapp.fx.common.StageViewBean;
import com.stocktracker.configuration.Configuration;
import com.stocktracker.domainobjs.json.AppUserLoginDO;
import com.stocktracker.domainobjs.json.CodeDataItemDO;
import com.stocktracker.domainobjs.json.PreferencesDO;

import java.util.*;


public class LoginSession {

    private String userName;
    private String loginTime;
    private String accessType;
    private List<String> functionAccessList;
    private String language;
    private Configuration globalConfiguration;

    private AppUserLoginDO loggedInUser;
    private PreferencesDO preferences;
    private StageViewBean stageView;
    private AppAccelerator appAccelerator;

    private Map<CodeDataItemDO,List<CodeDataItemDO>> cachedCodeDecodeData = new HashMap<>();
    public static final CodeDataItemDO EMPTY_CODE_DECODE_ITEM = new CodeDataItemDO("", "", "", "EN_US", "");

    public LoginSession() {
	// TODO Auto-generated constructor stub
	    functionAccessList = new ArrayList<String>();
        appAccelerator = new AppAccelerator();
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(String loginTime) {
        this.loginTime = loginTime;
    }

    public String getAccessType() {
        return accessType;
    }

    public void setAccessType(String accessType) {
        this.accessType = accessType;
    }
    
    public List<String> getFunctionAccessList() {
        return functionAccessList;
    }

    public void setFunctionAccessList(List<String> functionAccessList) {
        this.functionAccessList = functionAccessList;
    }
    
    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }
    

    public Configuration getGlobalConfiguration() {
		return globalConfiguration;
	}

	public void setGlobalConfiguration(Configuration globalConfiguration) {
		this.globalConfiguration = globalConfiguration;
	}

    public AppUserLoginDO getLoggedInUser() {
        return loggedInUser;
    }

    public void setLoggedInUser(AppUserLoginDO loggedInUser) {
        this.loggedInUser = loggedInUser;
    }

    public PreferencesDO getPreferences() {
        return preferences;
    }

    public void setPreferences(PreferencesDO preferences) {
        this.preferences = preferences;
    }

    public StageViewBean getStageView() {
        return stageView;
    }

    public void setStageView(StageViewBean stageView) {
        this.stageView = stageView;
    }

    public Map<CodeDataItemDO, List<CodeDataItemDO>> getCachedCodeDecodeData() {
        return cachedCodeDecodeData;
    }

    public void setCachedCodeDecodeData(Map<CodeDataItemDO, List<CodeDataItemDO>> cachedCodeDecodeData) {
        this.cachedCodeDecodeData = cachedCodeDecodeData;
    }

    public AppAccelerator getAppAccelerator() {
        return appAccelerator;
    }

    public void setAppAccelerator(AppAccelerator appAccelerator) {
        this.appAccelerator = appAccelerator;
    }

    @Override
    public String toString() {
        return "LoginSession{" +
                "userName='" + userName + '\'' +
                ", loginTime='" + loginTime + '\'' +
                ", accessType='" + accessType + '\'' +
                ", functionAccessList=" + functionAccessList +
                ", language='" + language + '\'' +
                ", offline=" + globalConfiguration.getGlobalProperties().isOffline() +
                '}';
    }
}
