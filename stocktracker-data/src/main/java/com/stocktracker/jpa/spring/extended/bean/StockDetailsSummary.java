/*
 * Copyright (c) 2020.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.jpa.spring.extended.bean;

import java.io.Serializable;

public class StockDetailsSummary implements Serializable {

    private String industryCode;
    private String stockCode;
    private String stockVarietyCode;
    private String transactionType;
    private Long totalNoOfPackets;
    private Double totalWeightment;
    private Double totalLampEntryInQtl;
    private Double totalLampEntryAfterAdjInQtl;
    private Double totalAmount;

    public StockDetailsSummary(String industryCode, String stockCode, String stockVarietyCode,Long totalNoOfPackets,Double totalWeightment) {
        this.industryCode = industryCode;
        this.stockCode = stockCode;
        this.stockVarietyCode = stockVarietyCode;
        this.totalNoOfPackets = totalNoOfPackets;
        this.totalWeightment = totalWeightment;
    }

    public StockDetailsSummary(String industryCode, String stockCode, String stockVarietyCode, String transactionType,Long totalNoOfPackets,Double totalWeightment) {
        this.industryCode = industryCode;
        this.stockCode = stockCode;
        this.stockVarietyCode = stockVarietyCode;
        this.transactionType = transactionType;
        this.totalNoOfPackets = totalNoOfPackets;
        this.totalWeightment = totalWeightment;
    }



    public StockDetailsSummary(String industryCode, String stockCode, String stockVarietyCode, String transactionType, Long totalNoOfPackets, Double totalWeightment, Double totalLampEntryInQtl,Double totalLampEntryAfterAdjInQtl, Double totalAmount) {
        this.industryCode = industryCode;
        this.stockCode = stockCode;
        this.stockVarietyCode = stockVarietyCode;
        this.transactionType = transactionType;
        this.totalNoOfPackets = totalNoOfPackets;
        this.totalWeightment = totalWeightment;
        this.totalLampEntryInQtl=totalLampEntryInQtl;
        this.totalLampEntryAfterAdjInQtl=totalLampEntryAfterAdjInQtl;
        this.totalAmount=totalAmount;
    }

    public String getIndustryCode() {
        return industryCode;
    }

    public void setIndustryCode(String industryCode) {
        this.industryCode = industryCode;
    }

    public String getStockCode() {
        return stockCode;
    }

    public void setStockCode(String stockCode) {
        this.stockCode = stockCode;
    }

    public String getStockVarietyCode() {
        return stockVarietyCode;
    }

    public void setStockVarietyCode(String stockVarietyCode) {
        this.stockVarietyCode = stockVarietyCode;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public Long getTotalNoOfPackets() {
        return totalNoOfPackets;
    }

    public void setTotalNoOfPackets(Long totalNoOfPackets) {
        this.totalNoOfPackets = totalNoOfPackets;
    }

    public Double getTotalWeightment() {
        return totalWeightment;
    }

    public void setTotalWeightment(Double totalWeightment) {
        this.totalWeightment = totalWeightment;
    }

    public Double getTotalLampEntryInQtl() {
        return totalLampEntryInQtl;
    }

    public void setTotalLampEntryInQtl(Double totalLampEntryInQtl) {
        this.totalLampEntryInQtl = totalLampEntryInQtl;
    }

    public Double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Double getTotalLampEntryAfterAdjInQtl() {
        return totalLampEntryAfterAdjInQtl;
    }

    public void setTotalLampEntryAfterAdjInQtl(Double totalLampEntryAfterAdjInQtl) {
        this.totalLampEntryAfterAdjInQtl = totalLampEntryAfterAdjInQtl;
    }

    @Override
    public String toString() {
        return "StockDetailsSummary{" +
                "industryCode='" + industryCode + '\'' +
                ", stockCode='" + stockCode + '\'' +
                ", stockVarietyCode='" + stockVarietyCode + '\'' +
                ", transactionType='" + transactionType + '\'' +
                ", totalNoOfPackets=" + totalNoOfPackets +
                ", totalWeightment=" + totalWeightment +
                ", totalLampEntryInQtl=" + totalLampEntryInQtl +
                ", totalLampEntryAfterAdjInQtl=" + totalLampEntryAfterAdjInQtl +
                ", totalAmount=" + totalAmount +
                '}';
    }
}
