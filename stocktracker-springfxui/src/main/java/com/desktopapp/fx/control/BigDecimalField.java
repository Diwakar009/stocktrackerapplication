/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.desktopapp.fx.control;

import com.desktopapp.fx.common.StringUtils;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.util.StringConverter;
import javafx.util.converter.IntegerStringConverter;

import java.math.BigDecimal;
import java.util.function.UnaryOperator;
import java.util.regex.Pattern;

/**
 * BigDecimal field control
 *
 * @see TextField
 * @see IntegerStringConverter
 * @see TextFormatter
 * @see UnaryOperator
 *
 * @author R Diwakar C
 */
public class BigDecimalField extends TextFieldExt {

    private Integer scale;
    private StringConverter<BigDecimal> stringConverter;
    private static Pattern decimalPattern = Pattern.compile("[-]?[0-9]*(\\.[0-9]*)?");

    public BigDecimalField() {
        super();
        this.scale = 2;
        init();
    }

    public BigDecimalField(Integer scale) {
        super();
        this.scale = scale;
        init();
    }

    public void init(){

        stringConverter = new StringConverter<BigDecimal>() {

            @Override
            public BigDecimal fromString(String value) {
                // If the specified value is null or zero-length, return null
                if (value == null) {
                    return null;
                }

                value = value.trim();

                if (value.length() < 1) {
                    return null;
                }

                if(value.equals("-")) {
                    return null;
                }


                return BigDecimal.valueOf(Double.parseDouble(value));
            }

            @Override
            public String toString(BigDecimal value) {
                // If the specified value is null, return a zero-length String
                if (value == null) {
                    return "";
                }

                return String.valueOf(value.doubleValue());
            }
        };

        //setTextFormatter(new TextFormatter<>(stringConverter, null,new FloatNumberUnaryOperator()));
        setOnAction(event -> stringConverter.fromString(this.getText()));
    }

    public BigDecimal getValue()
    {
        return stringConverter.fromString(this.getText());
    }

    public StringConverter<BigDecimal> getStringConverter() {
        return stringConverter;
    }

    public void setStringConverter(StringConverter<BigDecimal> stringConverter) {
        this.stringConverter = stringConverter;
    }

    /**
     * Set Data Value
     * @param value
     */
    public void setDataValue(BigDecimal value){
        setDataValue(value.doubleValue());
    }

    /**
     * Set Data Value
     * @param valueDouble
     */
    public void setDataValue(Double valueDouble){
        setDataValue(Double.toString(valueDouble));
    }

    @Override
    public void replaceText(int start, int end, String text) {
        if (validate(start, text)) {
            super.replaceText(start, end, text);
        }
    }

    @Override
    public void replaceSelection(String text) {
        if (validate(Integer.MAX_VALUE, text)) {
            super.replaceSelection(text);
        }
    }

    private boolean validate(int start, String text) {
        String currentText = StringUtils.isBlank(getText()) ? "" : getText();
        if(start == 0) { //to handle "-1.1" or ".1" cases
            return decimalPattern.matcher(text + currentText).matches();
        } else {
            return decimalPattern.matcher(currentText + text).matches();
        }
    }

}
