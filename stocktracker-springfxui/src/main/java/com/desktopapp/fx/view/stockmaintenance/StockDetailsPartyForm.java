/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.desktopapp.fx.view.stockmaintenance;

import com.desktopapp.fx.common.IGenericComponent;
import com.desktopapp.fx.control.BigDecimalField;
import com.desktopapp.fx.control.CodeDecodeField;
import com.desktopapp.fx.control.QuantityField;
import com.desktopapp.fx.control.TextFieldExt;
import com.desktopapp.fx.control.listeners.ValueChangeListener;
import com.desktopapp.fx.controller.StockMaintenanceController;
import com.stocktracker.component.constants.CodeDecodeConstants;
import com.stocktracker.domainobjs.json.sms.PartyDetailsDO;
import com.stocktracker.domainobjs.json.sms.StockDetailsDO;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.*;
import javafx.scene.text.Text;
import javafx.util.Callback;
import org.controlsfx.control.textfield.AutoCompletionBinding;
import org.controlsfx.control.textfield.TextFields;

import java.math.BigDecimal;
import java.util.*;

/**
 * Created by diwakar009 on 10/11/2019.
 */
public class StockDetailsPartyForm extends StackPane{

    public final static String PARTY_TYPE_OWN = "OWN";
    public final static String PARTY_TYPE_OTHERS = "OTH";

    public final static List<String> MANDI_ENTRY_IN_QTL = new ArrayList<String>(List.of("MDRE","MDAJ","MDEN","MDEP"));

    private CodeDecodeField cdPartyType;
    private CodeDecodeField cdStock;
    private CodeDecodeField cdStockVariety;
    private CodeDecodeField cdTransactionType;

    //private BigDecimalField bfLampEntryInQtls;
    private CodeDecodeField cdLampName;
    private CodeDecodeField cdMandiName;

    // TODO: Newly added
    private CodeDecodeField cdOwnIndustry;

    private StackPane spPartyName;
    private TextFieldExt tfPartyName;
    private CodeDecodeField cdPartyName;

    List<IGenericComponent> components = new ArrayList<>();

    private StockDetailsTransactionForm stockDetailsTransactionForm;


    public StockDetailsPartyForm(StockDetailsTransactionForm stockDetailsTransactionForm,CodeDecodeField cdStock,CodeDecodeField cdStockVariety,CodeDecodeField cdTransactionType) {

        this.setMouseTransparent(false);
        this.stockDetailsTransactionForm = stockDetailsTransactionForm;
        this.cdPartyType = new CodeDecodeField(CodeDecodeConstants.CD_PARTY_TYPE,CodeDecodeConstants.DEFAULT_LANGUAGE);

        this.cdOwnIndustry = new CodeDecodeField(CodeDecodeConstants.CD_INDUSTRY_CODE,CodeDecodeConstants.DEFAULT_LANGUAGE);
        this.cdLampName = new CodeDecodeField(CodeDecodeConstants.CD_LAMP_CODE,CodeDecodeConstants.DEFAULT_LANGUAGE);

        this.cdMandiName = new CodeDecodeField(CodeDecodeConstants.CD_MANDI_NAME,CodeDecodeConstants.DEFAULT_LANGUAGE);
        this.cdLampName.registerLinkedCodeDecodeField(cdMandiName);


        this.tfPartyName = new TextFieldExt(200);

        TextFields.bindAutoCompletion(this.tfPartyName, new Callback<AutoCompletionBinding.ISuggestionRequest, Collection<String>>() {
            @Override
            public Collection<String> call(AutoCompletionBinding.ISuggestionRequest iSuggestionRequest) {
                    return getPartyNameSuggestions(tfPartyName.getDataValue());
            }
        });

        this.cdStock = cdStock;
        this.cdStockVariety =cdStockVariety;
        this.cdTransactionType =cdTransactionType;

       /* this.bfLampEntryInQtls = new BigDecimalField();
        this.bfLampEntryInQtls.setPromptText(QuantityField.QUANTITY_WEIGHTMENT_UNIT);*/


        this.cdPartyName = new CodeDecodeField(CodeDecodeConstants.CD_PARTY_NAMES,CodeDecodeConstants.DEFAULT_LANGUAGE);
        this.cdPartyName.setSkipCache(true);

        this.cdPartyType.registerLinkedCodeDecodeField(cdPartyName);

        this.spPartyName = new StackPane();
        this.spPartyName.setMouseTransparent(false);

        //spPartyName.getChildren().add(tfPartyName);
        spPartyName.getChildren().clear();
        spPartyName.getChildren().add(cdPartyName.asControl());

        ValueChangeListener vcltransactionType = new ValueChangeListener() {
            @Override
            public void valueChanged() {
                if(MANDI_ENTRY_IN_QTL.contains(cdTransactionType.getDataValue())){
                    //bfLampEntryInQtls.setDisable(false);
                    cdLampName.setAsReadOnly(false);
                    cdMandiName.setAsReadOnly(false);

                    cdPartyType.setDataValue("OTH");
                }else {
                        /*bfLampEntryInQtls.setDisable(true);
                        bfLampEntryInQtls.clearDataValue();*/

                        cdLampName.setAsReadOnly(true);
                        cdLampName.clearDataValue();

                        cdMandiName.setAsReadOnly(true);
                        cdMandiName.clearDataValue();

                        cdPartyName.clearDataValue();
                        tfPartyName.clearDataValue();

                        cdPartyType.clearDataValue();
                }
            }
        };
        cdTransactionType.addValueChangeListener(vcltransactionType);

        ValueChangeListener vclPartyType = new ValueChangeListener() {
            @Override
            public void valueChanged() {
                if(PARTY_TYPE_OWN.equals(cdPartyType.getDataValue())){
                    cdOwnIndustry.setAsReadOnly(false);
                    tfPartyName.setAsReadOnly(true);
                    flipPartyNameControls(false,true);
                }else if(PARTY_TYPE_OTHERS.equals(cdPartyType.getDataValue())){
                    cdPartyName.clearDataValue();
                    tfPartyName.setAsReadOnly(false);
                    cdOwnIndustry.clearDataValue();
                    cdOwnIndustry.setAsReadOnly(true);
                    flipPartyNameControls(false,true);
                }else{
                    cdOwnIndustry.setAsReadOnly(true);
                    cdPartyName.setAsReadOnly(false);
                    tfPartyName.clearDataValue();
                    tfPartyName.setAsReadOnly(true);
                    flipPartyNameControls(true, false);
                }

            }
        };
        cdPartyType.addValueChangeListener(vclPartyType);

        ValueChangeListener vclOwnIndustry = new ValueChangeListener() {
            @Override
            public void valueChanged() {
                tfPartyName.setDataValue(cdOwnIndustry.getDataValueDescription());
            }
        };

        cdOwnIndustry.addValueChangeListener(vclOwnIndustry);

        ValueChangeListener vclCdPartyName = new ValueChangeListener() {
            @Override
            public void valueChanged() {
                tfPartyName.setDataValue(cdPartyName.getDataValueDescription());
            }
        };

        cdPartyName.addValueChangeListener(vclCdPartyName);

        build();

        registerComponents();
    }

    /**
     * Get Party Names.
     *
     * @param partyName
     * @return
     */
    public List<String> getPartyNameSuggestions(String partyName){
        Optional<List<String>> suggestedPartyNames = ((StockMaintenanceController)this.stockDetailsTransactionForm.getController()).getSuggestedPartyNames(partyName);
        if(suggestedPartyNames.isPresent()){
            return suggestedPartyNames.get();
        }
        return Arrays.asList();
    }

    /**
     * Flip between partyname and cdpartyname
     *
     * @param cdPartyNameFlag
     * @param tfPartyNameFlag
     */
    public void flipPartyNameControls(boolean cdPartyNameFlag,boolean tfPartyNameFlag){
         /*cdPartyName.setVisible(cdPartyNameFlag);
         tfPartyName.setVisible(tfPartyNameFlag);*/
         if(cdPartyNameFlag){
             spPartyName.getChildren().clear();
             spPartyName.getChildren().add(cdPartyName.asControl());
         }

        if(tfPartyNameFlag){
            spPartyName.getChildren().clear();
            spPartyName.getChildren().add(tfPartyName);
        }


    }

    public void build(){
        this.cdTransactionType.registerLinkedCodeDecodeField(cdPartyType);

        int columnIndex = 0;
        int rowIndex = 0;

        BorderPane borderPane = new BorderPane();
        Text transactionDtlsLbl = new Text();
        transactionDtlsLbl.setStyle("-fx-font-weight: bold");

        transactionDtlsLbl.setText("Party Details");
        HBox lblHBox = new HBox();
        lblHBox.getChildren().add(transactionDtlsLbl);
        lblHBox.setAlignment(Pos.CENTER);
        borderPane.setTop(lblHBox);

        GridPane formPane = new GridPane();
        formPane.setPadding(new Insets(10, 10, 10, 10));
        formPane.setHgap(10);
        formPane.setVgap(10);

        formPane.setGridLinesVisible(false);

        ColumnConstraints column1 = new ColumnConstraints();
        column1.setPercentWidth(50);

        ColumnConstraints column2 = new ColumnConstraints();
        column2.setPercentWidth(100);

        ColumnConstraints column3 = new ColumnConstraints();
        column3.setPercentWidth(2);

        formPane.getColumnConstraints().addAll(column1,column2,column3,column1,column2);

        formPane.add(new Label("Party Type"), columnIndex, rowIndex);
        formPane.add(this.cdPartyType.asControl(), columnIndex + 1, rowIndex);

        formPane.add(new Label("Own Mill"), columnIndex+3, rowIndex);
        formPane.add(this.cdOwnIndustry.asControl(), columnIndex + 4, rowIndex++);

        formPane.add(new Label("Party Name"), columnIndex, rowIndex);
        /*formPane.add(this.tfPartyName, columnIndex + 1, rowIndex);
        formPane.add(this.cdPartyName.asControl(), columnIndex + 1, rowIndex);*/
        formPane.add(this.spPartyName, columnIndex + 1, rowIndex);

        rowIndex = rowIndex + 1;

        formPane.add(new Label("Lamp Name"), columnIndex, rowIndex);
        formPane.add(this.cdLampName.asControl(), columnIndex + 1, rowIndex);

        formPane.add(new Label("Mandi Name"), columnIndex+3, rowIndex);
        formPane.add(this.cdMandiName.asControl(), columnIndex + 4, rowIndex++);

       /* formPane.add(new Text("Lamp Entry"), columnIndex+3, rowIndex);
        formPane.add(this.bfLampEntryInQtls, columnIndex + 4, rowIndex++);*/

        VBox vbox = new VBox();
        HBox formPaneHBox = new HBox();
        formPaneHBox.getChildren().add(formPane);

        vbox.getChildren().add(formPaneHBox);

        borderPane.setCenter(vbox);

        getChildren().add(borderPane);

        setStyle("-fx-border-color: gray");
    }

    /**
     * Get Generic Components
     *
     * @return
     */
    private List<IGenericComponent> registerComponents(){
        components.add(cdPartyType);
        components.add(cdOwnIndustry);
        components.add(tfPartyName);
        components.add(cdPartyName);
        /*components.add(bfLampEntryInQtls);*/
        components.add(cdLampName);
        components.add(cdMandiName);
        return components;
    }

    /**
     * Clear all
     */
    public void clearAll(){
        components.forEach(component -> component.clearDataValue());
    }

    /**
     * Set DO to Controls
     *
     * @param stockDetailsDO
     */
    public void setDOToControls(StockDetailsDO stockDetailsDO){
        if(stockDetailsDO != null) {

            PartyDetailsDO partyDetailsDO = null;
            if(stockDetailsDO.getPartyDetailsDO() != null){
                partyDetailsDO = stockDetailsDO.getPartyDetailsDO();
            }else{
                partyDetailsDO = new PartyDetailsDO();
            }

            cdPartyType.setDataValue(partyDetailsDO.getPartyTypeCode());

            cdLampName.setDataValue(stockDetailsDO.getLampNameCode());
            cdMandiName.setDataValue(stockDetailsDO.getMandiNameCode());

            cdOwnIndustry.setDataValue(partyDetailsDO.getMoreDetails().getOwnIndustryCode());
            cdPartyName.setDataValue(partyDetailsDO.getMoreDetails().getPartyNameCode());

            tfPartyName.setDataValue(partyDetailsDO.getPartyName1());
        }
    }

    /**
     * Set controls to DO
     *
     * @param stockDetailsDO
     */
    public void setControlsToDO(StockDetailsDO stockDetailsDO){
        if(stockDetailsDO != null) {

            PartyDetailsDO partyDetailsDO = null;
            if(stockDetailsDO.getPartyDetailsDO() != null){
                partyDetailsDO = stockDetailsDO.getPartyDetailsDO();
            }else{
                partyDetailsDO = new PartyDetailsDO();
            }

            partyDetailsDO.setPartyTypeCode(this.cdPartyType.getDataValue());

            // Lamp Name is must if lamp entry in Qtl
            stockDetailsDO.setLampNameCode(this.cdLampName.getDataValue());
            stockDetailsDO.setMandiNameCode(this.cdMandiName.getDataValue());

            partyDetailsDO.setPartyName1(this.tfPartyName.getDataValue());
            partyDetailsDO.getMoreDetails().setOwnIndustryCode(cdOwnIndustry.getDataValue());
            partyDetailsDO.getMoreDetails().setPartyNameCode(cdPartyName.getDataValue());

            stockDetailsDO.setPartyDetailsDO(partyDetailsDO);
        }
    }

   /* public void setControlsToDO(StockDetailsDO stockDetailsDO){
        if(stockDetailsDO != null) {
            if(!this.isDisabled()) {
                PartyDetailsDO partyDetailsDO = null;
                if (stockDetailsDO.getPartyDetailsDO() != null) {
                    partyDetailsDO = stockDetailsDO.getPartyDetailsDO();
                } else {
                    partyDetailsDO = new PartyDetailsDO();
                }

                partyDetailsDO.setPartyTypeCode(this.cdPartyType.getDataValue());

                // Lamp Name is must if lamp entry in Qtl
                stockDetailsDO.setLampNameCode(this.cdLampName.getDataValue());
                stockDetailsDO.setMandiNameCode(this.cdMandiName.getDataValue());

                partyDetailsDO.setPartyName1(this.tfPartyName.getDataValue());
                partyDetailsDO.getMoreDetails().setOwnIndustryCode(cdOwnIndustry.getDataValue());
                partyDetailsDO.getMoreDetails().setPartyNameCode(cdPartyName.getDataValue());

                stockDetailsDO.setPartyDetailsDO(partyDetailsDO);
            }else{
                stockDetailsDO.setPartyDetailsDO(null);
            }
        }
    }*/

    public List<IGenericComponent> getComponents() {
        return components;
    }

    public void setComponents(List<IGenericComponent> components) {
        this.components = components;
    }
}
