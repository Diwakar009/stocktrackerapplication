/*
 * Copyright (c) 2020.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.jpa.spring.extended.extention.impl;

import com.stocktracker.jpa.spring.extended.StockRepository;
import com.stocktracker.jpa.spring.extended.config.ExtendedRepositoryConfig;
import junit.framework.TestCase;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { ExtendedRepositoryConfig.class })
@Transactional
public class StockExtendedRepositoryImplTest extends TestCase {

    @Autowired
    private StockRepository stockRepository;

    @Before
    public void setUp() throws Exception {
    }

    @Test
    @Rollback(true)
    public void testAutowine(){
        Assert.assertEquals(true,stockRepository != null);
    }
}