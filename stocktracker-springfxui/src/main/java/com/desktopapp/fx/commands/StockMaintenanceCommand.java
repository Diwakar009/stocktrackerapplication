/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.desktopapp.fx.commands;

import com.desktopapp.fx.controller.StaticCodeDecodeController;
import com.desktopapp.fx.controller.StockMaintenanceController;
import com.desktopapp.fx.objectfactory.ObjectFactory;
import com.desktopapp.fx.view.AppView;

/**
 * Created by diwakar009 on 24/12/2018.
 */
public class StockMaintenanceCommand implements ICommand {

    private AppView view;

    private StockMaintenanceController stockMaintenanceController;

    public StockMaintenanceCommand(AppView view) {
        this.view = view;
        this.stockMaintenanceController = (StockMaintenanceController) ObjectFactory.getControllerObject(ObjectFactory.STOCK_MAINTENANCE_CTRL);
    }

    @Override
    public void executeCommand() {
        this.view.addPageToCenter(stockMaintenanceController.getDataPageView());
    }

}
