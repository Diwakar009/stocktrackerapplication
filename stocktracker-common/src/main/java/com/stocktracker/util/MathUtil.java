/*
 * Copyright (c) 2022.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.util;

public class MathUtil {

    public static Long add(Long a,Long b){
        if(a == null)
            a = 0l;

        if(b == null)
            b = 0l;

        return a + b;
    }

    public static Long substract(Long a,Long b){
        if(a == null)
            a = 0l;

        if(b == null)
            b = 0l;

        return a - b;
    }

    public static Double add(Double a,Double b){
        if(a == null)
            a = 0.00;

        if(b == null)
            b = 0.00;

        return a + b;
    }

    public static Double substract(Double a,Double b){
        if(a == null)
            a = 0.00;

        if(b == null)
            b = 0.00;

        return a - b;
    }

}
