/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.domainobjs;

import com.stocktracker.model.BaseEntity;

public interface IDomainObject {


	public Object getDomainObjectKey();
	public BaseEntity copyDomainObject2Entity(BaseEntity entity);
	public void copyEntity2DomainObject(BaseEntity entity);
}
