/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.domainobjs.json.sms;

import com.stocktracker.domainobjs.json.BaseDomainObject;
import com.stocktracker.model.BaseEntity;
import com.stocktracker.model.sms.Stock;
import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import java.io.Serializable;
import java.util.Date;


/**
 * StockDO class
 * 
 */
@JsonIgnoreProperties (ignoreUnknown=true)
@JsonInclude(Include.NON_EMPTY)
public class StockDO extends BaseDomainObject implements Serializable {
	private static final long serialVersionUID = 1L;

	public static String CLASS_NAME = StockDO.class.getName();

	@JsonProperty("id")
	private StockDOPK id;

	@JsonProperty("totalNoOfPackets")
	private Long totalNoOfPackets;

	@JsonProperty("totalWeightment")
	private Double totalWeightment;

	@JsonProperty("asOnDate")
	@JsonFormat(shape = JsonFormat.Shape.STRING,pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
	private Date asOnDate;

	@JsonProperty("moreDetails")
	private String moreDetails;


	public StockDO() {
		super();
	}

	public StockDO(StockDOPK id) {
		this.id = id;
		init();
	}

	public StockDO(Stock stock) {
		copyEntity2DomainObject(stock);
	}

	public void init(){
		setTotalNoOfPackets(0l);
		setTotalWeightment(0.0d);
		setAsOnDateAsNow();
	}

	@Override
	public StockDOPK getId() {
		return this.id;
	}

	public void setId(StockDOPK id) {
		this.id = id;
	}


	public String getIndustryCode() {

		if(getId() == null){
			id = new StockDOPK();
		}

		return getId().getIndustryCode();
	}

	public void setIndustryCode(String industryCode) {
		if(getId() == null){
			id = new StockDOPK();
		}

		getId().setIndustryCode(industryCode);
	}

	public String getStockCode() {

		if(getId() == null){
			id = new StockDOPK();
		}

		return getId().getStockCode();
	}

	public void setStockCode(String stockCode) {

		if(getId() == null){
			id = new StockDOPK();
		}

		getId().setStockCode(stockCode);
	}

	public String getStockVarietyCode() {

		if(getId() == null){
			id = new StockDOPK();
		}

		return getId().getStockVarietyCode();
	}

	public void setStockVarietyCode(String stockVarietyCode) {

		if(getId() == null){
			id = new StockDOPK();
		}

		getId().setStockVarietyCode(stockVarietyCode);
	}

	public Long getTotalNoOfPackets() {
		return totalNoOfPackets;
	}

	public void setTotalNoOfPackets(Long totalNoOfPackets) {
		this.totalNoOfPackets = totalNoOfPackets;
	}

	public Double getTotalWeightment() {
		return totalWeightment;
	}

	public void setTotalWeightment(Double totalWeightment) {
		this.totalWeightment = totalWeightment;
	}

	public Date getAsOnDate() {
		return asOnDate;
	}

	public void setAsOnDate(Date asOnDate) {
		this.asOnDate = asOnDate;
	}

	public String getMoreDetails() {
		return moreDetails;
	}

	public void setMoreDetails(String moreDetails) {
		this.moreDetails = moreDetails;
	}


	/**
	 * Add the total no of packets
	 *
	 * @param totalNoOfPackets
	 * @return
	 */
	public Long addTotalNoofPackets(Long totalNoOfPackets){
		setTotalNoOfPackets(getTotalNoOfPackets() + totalNoOfPackets);
		setAsOnDateAsNow();
		return getTotalNoOfPackets();
	}

	/**
	 * Add total weightments in quintals
	 *
	 * @param totalWeightment
	 * @return
	 */
	public Double addTotalWeightment(Double totalWeightment){
		setTotalWeightment(getTotalWeightment() + totalWeightment);
		setAsOnDateAsNow();
		return getTotalWeightment();
	}

	/**
	 * Set as on date as now.
	 *
	 */
	public void setAsOnDateAsNow(){
		setAsOnDate(new Date());
	}

	@JsonIgnore
	@Override
	public Object getDomainObjectKey() {
		return getId();
	}

	@Override
	public Stock copyDomainObject2Entity(BaseEntity entity) {
		Stock stock = (Stock)entity;

		if(entity == null){
			stock = new Stock();
		}else {
			stock = (Stock)entity;
		}

		stock.setCreatedBy(this.getCreatedBy());
		stock.setUpdatedBy(this.getUpdatedBy());
		stock.setCreatedAt(this.getCreatedAt());
		stock.setUpdatedAt(this.getUpdatedAt());

		stock.setIndustryCode(this.getIndustryCode());
		stock.setStockCode(this.getStockCode());
		stock.setStockVarietyCode(this.getStockVarietyCode());
		stock.setAsOnDate(this.getAsOnDate());
		stock.setTotalWeightment(this.getTotalWeightment());
		stock.setTotalNoOfPackets(this.getTotalNoOfPackets());

		return stock;
	}
	@Override
	public void copyEntity2DomainObject(BaseEntity entity) {
		Stock stock = (Stock)entity;
		this.setCreatedBy(stock.getCreatedBy());
		this.setUpdatedBy(stock.getUpdatedBy());
		this.setCreatedAt(stock.getCreatedAt());
		this.setUpdatedAt(stock.getUpdatedAt());

		this.setIndustryCode(stock.getIndustryCode());
		this.setStockCode(stock.getStockCode());
		this.setStockVarietyCode(stock.getStockVarietyCode());
		this.setAsOnDate(stock.getAsOnDate());
		this.setTotalNoOfPackets(stock.getTotalNoOfPackets());
		this.setTotalWeightment(stock.getTotalWeightment());
		this.setMoreDetails(stock.getMoreDetails());

	}

	@Override
	public String toString() {
		return "StockDO{" +
				"id=" + id.toString() +
				", totalNoOfPackets=" + totalNoOfPackets +
				", totalWeightment=" + totalWeightment +
				", asOnDate=" + asOnDate +
				", moreDetails='" + moreDetails + '\'' +
				'}';
	}
}