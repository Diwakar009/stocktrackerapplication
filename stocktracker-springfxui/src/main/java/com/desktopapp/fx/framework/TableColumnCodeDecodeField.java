/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.desktopapp.fx.framework;

import com.desktopapp.fx.DesktopFxApplication;
import com.desktopapp.fx.controller.StaticCodeDecodeController;
import com.desktopapp.fx.objectfactory.ObjectFactory;
import com.stocktracker.domainobjs.json.CodeDataItemDO;

import java.util.List;
import java.util.Optional;

/**
 * Created by diwakar009 on 21/10/2019.
 */
public class TableColumnCodeDecodeField {

    private String codeName;
    private String codeValue;
    private String filterCodeName;
    private StaticCodeDecodeController staticCodeDecodeController;

    public TableColumnCodeDecodeField(String codeName) {
        this.codeName = codeName;
        this.staticCodeDecodeController = (StaticCodeDecodeController) ObjectFactory.getControllerObject(ObjectFactory.STATIC_CODE_DECODE_CTRL);
    }

    /**
     *
     * Reterive code decode description
     *
     * @param codeValue
     * @return
     */
    public String reteriveCodeDescription(String codeValue){
        List<CodeDataItemDO> codeDataItemDOList = null;
        if(!DesktopFxApplication.getLoginSession().getCachedCodeDecodeData().containsKey(new CodeDataItemDO(codeName,"","","EN_US"))) {

            Optional<List<CodeDataItemDO>> codeDataItemOpt = staticCodeDecodeController.getStaticCodeDataItemList(this.codeName,"EN_US");

            if(codeDataItemOpt.isPresent()) {
                codeDataItemDOList = codeDataItemOpt.get();
                DesktopFxApplication.getLoginSession().getCachedCodeDecodeData().put(new CodeDataItemDO(codeName,"","","EN_US"),codeDataItemDOList);
            }

        }else {
            codeDataItemDOList = DesktopFxApplication.getLoginSession().getCachedCodeDecodeData().get(new CodeDataItemDO(codeName,"","","EN_US"));
        }

        Optional<CodeDataItemDO> selectedCodeDataItem = codeDataItemDOList.stream()
                    .filter(codeDataItemDO -> codeValue != null && codeValue.equals(codeDataItemDO.getCodeValue()))
                    .findFirst();

        if (selectedCodeDataItem.isPresent()) {
             return selectedCodeDataItem.get().getCodeDescription();
        }

        return codeValue;
    }

    public String getCodeName() {
        return codeName;
    }

    public void setCodeName(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeValue() {
        return codeValue;
    }

    public void setCodeValue(String codeValue) {
        this.codeValue = codeValue;
    }

    public String getFilterCodeName() {
        return filterCodeName;
    }

    public void setFilterCodeName(String filterCodeName) {
        this.filterCodeName = filterCodeName;
    }
}
