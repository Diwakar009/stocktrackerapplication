/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.desktopapp.service.stockmaintenance;


import com.desktopapp.service.AbstractService;
import com.stocktracker.jpa.spring.extended.PartyDetailsRepository;
import com.stocktracker.model.BaseEntity;
import com.stocktracker.model.sms.PartyDetails;
import com.stocktracker.domainobjs.IDomainObject;
import com.stocktracker.domainobjs.json.TableRowMapDO;
import com.stocktracker.domainobjs.json.sms.PartyDetailsDO;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * Party Details Service
 *
 * @author Ratnala Diwakar Choudhury
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS,readOnly = true)
public class PartyDetailsService extends AbstractService<PartyDetailsDO> {

    @Resource
    protected PartyDetailsRepository partyDetailsRepository;

    public PartyDetailsService() {
        super(PartyDetailsDO.class);
    }

    @Override
    public Class<PartyDetails> getEntityClass() {
        return PartyDetails.class;
    }


    public List<PartyDetailsDO> getPartyNameSuggestions(String partyName){

        List<PartyDetails> partyNames = partyDetailsRepository.findPartyNames(partyName);

        return returnDomainObjects(partyNames);
    }

    @Override
    public PartyDetailsDO returnDomainObject(BaseEntity entity) {

        if (entity == null) {
            return null;
        }

        PartyDetailsDO domainObject = new PartyDetailsDO((PartyDetails)entity);
        return domainObject;
    }

    @Override
    public List<PartyDetailsDO> returnDomainObjects(List<? extends BaseEntity> entities) {
        if (entities == null) {
            return null;
        }
        List<PartyDetailsDO> domainObjects = new ArrayList<PartyDetailsDO>();

        if (entities != null) {
            entities.forEach(entity -> {
                PartyDetailsDO domainObject = new PartyDetailsDO((PartyDetails) entity);
                domainObjects.add(domainObject);
            });
        }
        return domainObjects;
    }

    @Override
    public BaseEntity returnEntity(PartyDetailsDO domainObject) {
        PartyDetailsDO partyDetailsDO = (PartyDetailsDO) domainObject;
        PartyDetails partyDetails = new PartyDetails();
        partyDetailsDO.copyDomainObject2Entity(partyDetails);
        return partyDetails;
    }

   @Override
    public List<? extends BaseEntity> returnEntities(List<PartyDetailsDO> domainObjects) {
        List<PartyDetails> entities = new ArrayList();
        for (IDomainObject domainObject : domainObjects) {
            PartyDetailsDO partyDetailsDO = (PartyDetailsDO) domainObject;
            PartyDetails partyDetails = new PartyDetails();
            partyDetailsDO.copyDomainObject2Entity(partyDetails);
            entities.add(partyDetails);
        }
        return entities;
    }

    @Override
    public TableRowMapDO<String, String> copyDomainObjToTableRowMap(IDomainObject domainObject) {
        PartyDetailsDO partyDetailsDO = (PartyDetailsDO) domainObject;
        TableRowMapDO<String, String> tableRowMapDO = new TableRowMapDO<String, String>();
        tableRowMapDO.add("id", partyDetailsDO.getId().toString());
        tableRowMapDO.setClazzName(PartyDetailsDO.CLASS_NAME);
        return tableRowMapDO;
    }

    @Override
    public PartyDetailsRepository getRepository() {
        return this.partyDetailsRepository;
    }
}
