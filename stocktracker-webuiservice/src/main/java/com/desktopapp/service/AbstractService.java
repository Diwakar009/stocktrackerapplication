package com.desktopapp.service;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.desktopapp.resttemplate.BaseRestTemplate;
import com.stocktracker.domainobjs.json.*;
import com.stocktracker.util.DateTimeUtil;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.stocktracker.resource.util.ResourceUtil;

/**
 * Abstract Service for all services.
 *
 * @param <T> entity
 *
 * @author Ratnala Diwakar Choudhury
 */
/*@Service*/
public abstract class AbstractService<T extends BaseDomainObject> {
	
	
	private String webserviceURL;
    private final Class<T> entityClass;
    private BaseRestTemplate restTemplate;
    
    /**
     * Create default service
     *
     * @param entityClass entity class
     */
    public AbstractService(Class<T> entityClass) {
    	this.entityClass = entityClass;
    	this.webserviceURL = ResourceUtil.getPropertyValue("WS_URL") + getBaseResource();  
        this.restTemplate = new BaseRestTemplate();
    }
    

    
    /**
     * Gets entity class.
     *
     * @return entity class
     */
    public Class<T> getEntityClass() {
        return entityClass;
    }
    
    
    /**
     * Calls web services with the parameter support
     * 
     * @param resourceName
     * @param clazz
     * @param parameters
     * @return
     */
    @SuppressWarnings("unchecked")
	public Object callGetWebService (String resourceName,@SuppressWarnings("rawtypes") Class clazz,Map<String,Object> parameters){
    	return  getRestTemplate().getForObject(constructQueryParams(getWebserviceURL() + "/" + resourceName,parameters)
    				,clazz
    				,parameters);
    }
    
   
    
    /**
     * Post call for save
     * 
     * @param webServiceURL
     * @param entity
     * @return
     */
    public T postForChange(String webServiceURL,T entity,Class<T> entityClass){
    	return getRestTemplate().postForObject(webServiceURL, entity, entityClass);
    }
    
    /**
     * Post call for remove
     * 
     * @param webServiceURL
     * @param entity
     * @return
     */
    public Boolean postForRemove(String webServiceURL,T entity){
    	return getRestTemplate().postForObject(webServiceURL, entity,Boolean.class);
    }
    
    /**
     * Post call for get
     * 
     * @param webServiceURL
     * @param entity
     * @return
     */
    public T postForGet(String webServiceURL,T entity,Class<T> entityClass){
    	return getRestTemplate().postForObject(webServiceURL, entity, entityClass);
    }
    

    /**
     * Create entity.
     *
     * @param entity entity model.
     * @return created entity
     */
    public T create(T entity) {
   
    	T updatedEntity = postForChange(getWebserviceURL() + "/save", entity, entityClass);
    	
        return entity;
    }

    /**
     * Update entity.
     *
     * @param entity entity model
     * @return updated entity
     */
    public T update(T entity) {
    	
    	T updatedEntity = postForChange(getWebserviceURL() + "/save", entity, entityClass);
    	
        return entity;
    }
    
    /**
     * Update entity.
     *
     * @param entity entity model
     * @return updated entity
     */
    public T find(T entity) {
    	
    	T actualEntity = postForGet(getWebserviceURL() + "/get", entity, entityClass);
    	
        return actualEntity;
    }

    /**
     * Remove entity.
     *
     * @param entity entity model
     */
    public void remove(T entity) {
    	postForRemove(getWebserviceURL() + "/delete", entity);
    }
    
    /**
     * Remove entities.
     *
     * @param entities entity model
     */
    public void remove(List<T> entities) {
    	DomainObjectList<T> dolist = new DomainObjectList<T>();
    	dolist.setList(entities);
    	getRestTemplate().postForObject(getWebserviceURL() + "/deleteList", dolist, Boolean.class);
    }

	/**
	 * Get paginated list data using get method
	 *
	 * @param namedQueryName
	 * @param responseType
	 * @param parameters
	 * @return
	 */
    @SuppressWarnings("unchecked")
    public DOPage<TableRowMapDO<String,String>> getListWithNamedQuery(String namedQueryName, Class responseType,
																	  Map<String, Object> parameters) {
		DOPage<TableRowMapDO<String,String>> page = (DOPage<TableRowMapDO<String,String>>) getRestTemplate().getForObject(constructQueryParams(getWebserviceURL() + "/" + namedQueryName,parameters),responseType,parameters);
    	
    	return page;		
    }
    
    /**
     * Gets entity list with post query.
     *
     * @param namedQueryName named query name from entity
     * @param parameters parameters map for named query
     *
     * @return result list
     */
    @SuppressWarnings("unchecked")
    public DOPage<TableRowMapDO<String,String>> getListWithPostQuery(String namedQueryName,Map<String, Object> parameters) {
    	TableRowMapDO<String,String> request = new TableRowMapDO<String,String>();
    	
    	for(Map.Entry<String, Object> entry: parameters.entrySet()){

    		if(entry.getValue() instanceof Date){
				request.add(entry.getKey(), DateTimeUtil.convertDateToUTCString((Date)entry.getValue()));
			}else {
				request.add(entry.getKey(), String.valueOf(entry.getValue()));
			}
    	}
    	
    	DOPage<TableRowMapDO<String,String>> page = (DOPage<TableRowMapDO<String,String>>) getRestTemplate()
				.postForObject(getWebserviceURL() + "/" + namedQueryName,request,TableDOPage.class);
    	
    	return page;		
    }
    
    /**
     * Construct query string with the parameters
     * 
     * @param url
     * @param parameters
     * @return
     */
    protected String constructQueryParams(String url,
			Map<String, Object> parameters) {
		
    	StringBuilder buffer = new StringBuilder();
    	buffer.append(url);
    	
    	if(parameters != null && parameters.size() > 0){
    		buffer.append("?");
    		
    	    Iterator<String> i =  parameters.keySet().iterator();
    	    while(i.hasNext()){
    	    	String key = (String)i.next();
    	    	buffer.append(key + "={" +  key + "}");
    	    	if(i.hasNext()){
    	    		buffer.append("&");
    	    	}
    	    }
    	   
    	}
    	
    	return buffer.toString();
	}

	public DOPage<TableRowMapDO<String,String>> listAll(Map<String,Object> parameters){
		return getListWithNamedQuery("listAll",TableDOPage.class,parameters);
	}

	public DOPage<TableRowMapDO<String,String>> listByFilter(Map<String,Object> parameters){
		return getListWithPostQuery("listWithFilter",parameters);
	}


    protected String getBaseResource(){
    	return "/";
    }

	public String getWebserviceURL() {
		return webserviceURL;
	}


	public void setWebserviceURL(String webserviceURL) {
		this.webserviceURL = webserviceURL;
	}


	public BaseRestTemplate getRestTemplate() {
		return restTemplate;
	}


	public void setRestTemplate(BaseRestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}
    
    public Object callService(String URL,Class clazz,Map<String, String> params){
		return getRestTemplate().getForObject(URL,clazz,params);
	}

}
