/*
 * Customers Java Swing Application Demo
 *
 * Copyright(c) 2013, devsniper.com
 */
package com.desktopapp.service;


import com.stocktracker.framework.IEntity;
import com.stocktracker.jpa.spring.extended.AppUserRepository;
import com.stocktracker.model.AppUser;
import com.stocktracker.model.BaseEntity;
import com.stocktracker.domainobjs.IDomainObject;
import com.stocktracker.domainobjs.json.AppUserDO;
import com.stocktracker.domainobjs.json.TableRowMapDO;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * AppUser Service
 *
 * @author Ratnala Diwakar Choudhury
 */
@Service
@Transactional(
        propagation = Propagation.SUPPORTS,
        readOnly = false)
public class AppUserService extends AbstractService<AppUserDO> {

    @Resource
    protected AppUserRepository appUserRepository;


    public AppUserService() {
        super(AppUserDO.class);
        //repository = new AppUserStandaloneRepository(AppUser.class);
    }

    @Override
    public Class<AppUser> getEntityClass() {
        return AppUser.class;
    }

    @Override
    public AppUserDO returnDomainObject(BaseEntity entity) {

        if (entity == null) {
            return null;
        }

        AppUserDO domainObject = new AppUserDO((AppUser) entity);
        return domainObject;
    }

    @Override
    public List<AppUserDO> returnDomainObjects(List<? extends BaseEntity> entities) {

        if (entities == null) {
            return null;
        }

        List<AppUserDO> domainObjects = new ArrayList<AppUserDO>();
        if (entities != null) {
            for (IEntity entity : entities) {
                AppUserDO domainObject = new AppUserDO((AppUser) entity);
                domainObjects.add(domainObject);
            }
        }
        return domainObjects;
    }

    @Override
    public BaseEntity returnEntity(AppUserDO domainObject) {

        AppUserDO appUserDO = (AppUserDO) domainObject;
        AppUser appUser = new AppUser();
        appUserDO.copyDomainObject2Entity(appUser);

        return appUser;
    }

    @Override
    public List<? extends BaseEntity> returnEntities(List<AppUserDO> domainObjects) {
        List<AppUser> entities = new ArrayList();
        for (IDomainObject domainObject : domainObjects) {
            AppUserDO appUserDO = (AppUserDO) domainObject;
            AppUser appUser = new AppUser();
            appUserDO.copyDomainObject2Entity(appUser);
            entities.add(appUser);
        }
        return entities;

    }

    @Override
    public TableRowMapDO<String, String> copyDomainObjToTableRowMap(IDomainObject domainObject) {
        AppUserDO appuserDO = (AppUserDO) domainObject;
        TableRowMapDO<String, String> tableRowMapDO = new TableRowMapDO<String, String>();
        tableRowMapDO.add("id", appuserDO.getId().toString());
        tableRowMapDO.add("lastName", appuserDO.getLastName());
        tableRowMapDO.add("firstName", appuserDO.getFirstName());
        tableRowMapDO.add("shortName", appuserDO.getShortName());
        tableRowMapDO.add("mobile", appuserDO.getMobile());
        tableRowMapDO.add("active", new Boolean(appuserDO.getActive()).toString());
        tableRowMapDO.add("email", appuserDO.getEmail());
        tableRowMapDO.add("phone", appuserDO.getPhone());
        tableRowMapDO.add("title", appuserDO.getTitle());
        tableRowMapDO.add("city", appuserDO.getCity());
        tableRowMapDO.add("homepage", appuserDO.getHomepage());
        tableRowMapDO.setClazzName(AppUserDO.CLASS_NAME);
        return tableRowMapDO;
    }

    @Override
    public AppUserRepository getRepository() {
        return this.appUserRepository;
    }
}
