/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.desktopapp.fx.controller;


import com.desktopapp.fx.DesktopFxApplication;
import com.desktopapp.fx.common.PageDetails;
import com.desktopapp.fx.control.MessageBox;
import com.desktopapp.fx.control.multiscreen.StackedScreen;
import com.desktopapp.fx.mvc.AbstractDataPageController;
import com.desktopapp.fx.mvc.DataPageView;
import com.desktopapp.fx.util.I18n;
import com.desktopapp.fx.util.UIFileUtil;
import com.desktopapp.fx.view.stockmaintenance.StockDetailsTransactionForm;
import com.desktopapp.fx.view.stockmaintenance.StockDetailsTransactionSearchPage;
import com.desktopapp.service.AbstractService;
import com.desktopapp.service.reports.ExcelReportService;
import com.desktopapp.service.stockmaintenance.StockDetailsService;
import com.stocktracker.domainobjs.json.DOPage;
import com.stocktracker.domainobjs.json.ResultDO;
import com.stocktracker.domainobjs.json.TableRowMapDO;
import com.stocktracker.domainobjs.json.sms.PartyDetailsDO;
import com.stocktracker.domainobjs.json.sms.StockDetailsDO;
import com.stocktracker.exceptions.BusinessException;
import com.stocktracker.exceptions.TechnicalException;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.ByteArrayOutputStream;
import java.util.*;
import java.util.stream.Collectors;

import static com.desktopapp.service.QueryParameter.with;

/**
 * Stock Maintenance controller
 *
 * @author diwakar009
 */
@Component
public class StockMaintenanceController extends AbstractDataPageController<StockDetailsDO> {

    private final Logger logger = LoggerFactory.getLogger(StockMaintenanceController.class);

    @Autowired
    private StockDetailsService stockDetailsService;

    @Autowired
    private ExcelReportService excelReportService;


    /**
     * Get stock details data
     *
     * @param industryCode
     * @param stockCode
     * @param stockVarietyCode
     * @param transactionType
     * @param transStartDate
     * @param transEndDate
     * @param rstNumber
     * @param vehicleNumber
     * @param partyName
     * @param onBehalf
     * @param lampName
     * @param mandiName
     * @param pageDetails
     * @return
     */

    public DOPage<TableRowMapDO<String,String>> getData(String industryCode,
                                                        String stockCode,
                                                        String stockVarietyCode,
                                                        String transactionType,
                                                        Date transStartDate,
                                                        Date transEndDate,
                                                        String rstNumber,
                                                        String vehicleNumber,
                                                        String partyName,
                                                        String onBehalf,
                                                        String lampName,
                                                        String mandiName,String revisitFlag,
                                                        PageDetails pageDetails) {

        boolean filterEnabled = false;

        if(!"".equals(industryCode)
                || !"".equals(stockCode)
                || !"".equals(stockVarietyCode)
                || !"".equals(transactionType)
                || !"".equals(rstNumber)
                || !"".equals(vehicleNumber)
                || !"".equals(partyName)
                || !"".equals(onBehalf)
                || !"".equals(lampName)
                || !"".equals(mandiName)
                || !"".equals(revisitFlag)
                || transStartDate != null
                || transEndDate != null){
            filterEnabled = true;
        }

        if (!filterEnabled) {
            return getService().listAll(with("page", String.valueOf(pageDetails.getPageNumber())).
                    and("size", String.valueOf(pageDetails.getSize())).
                    and("start",pageDetails.getStart()).
                    and("end",pageDetails.getEnd()).parameters());
        } else {
            return getService().listByFilter(with("industryCode", industryCode).
                    and("stockCode", stockCode).
                    and("stockVarietyCode", stockVarietyCode).
                    and("transactionType", transactionType).
                    and("rstNumber", rstNumber).
                    and("vehicleNumber", vehicleNumber).
                    and("transStartDate", transStartDate).
                    and("transEndDate", transEndDate).
                    and("partyName", partyName).
                    and("onBehalf", onBehalf).
                    and("lampName", lampName).
                    and("mandiName", mandiName).
                    and("revisitFlag", revisitFlag).
                    and("page", String.valueOf(pageDetails.getPageNumber())).
                    and("size", String.valueOf(pageDetails.getSize())).
                    and("start",pageDetails.getStart()).
                    and("end",pageDetails.getEnd()).
                    parameters());

        }
    }


    /**
     *
     * Get stock details summary
     * @param industryCode
     * @param stockCode
     * @param stockVarietyCode
     * @param transactionType
     * @param transStartDate
     * @param transEndDate
     * @param rstNumber
     * @param vehicleNumber
     * @param partyName
     * @param onBehalf
     * @param lampName
     * @param mandiName
     * @param pageDetails
     * @return
     */

    public DOPage<TableRowMapDO<String,String>> getSummaryData(String industryCode,
                                                        String stockCode,
                                                        String stockVarietyCode,
                                                        String transactionType,
                                                        Date transStartDate,
                                                        Date transEndDate,
                                                        String rstNumber,
                                                        String vehicleNumber,
                                                        String partyName,
                                                        String onBehalf,
                                                               String lampName,
                                                               String mandiName,
                                                               String revisitFlag,
                                                        PageDetails pageDetails) {

        boolean filterEnabled = false;

        if(!"".equals(industryCode)
                || !"".equals(stockCode)
                || !"".equals(stockVarietyCode)
                || !"".equals(transactionType)
                || !"".equals(rstNumber)
                || !"".equals(vehicleNumber)
                || !"".equals(partyName)
                || !"".equals(onBehalf)
                || !"".equals(lampName)
                || !"".equals(mandiName)
                || !"".equals(revisitFlag)
                || transStartDate != null
                || transEndDate != null){
            filterEnabled = true;
        }

        if (!filterEnabled) {
            return ((StockDetailsService)getService()).findSummaryByFilter(with("page", String.valueOf(pageDetails.getPageNumber())).
                    and("size", String.valueOf(pageDetails.getSize())).
                    and("start",pageDetails.getStart()).
                    and("end",pageDetails.getEnd()).parameters());
        } else {
            return ((StockDetailsService)getService()).findSummaryByFilter(with("industryCode", industryCode).
                    and("stockCode", stockCode).
                    and("stockVarietyCode", stockVarietyCode).
                    and("transactionType", transactionType).
                    and("rstNumber", rstNumber).
                    and("vehicleNumber", vehicleNumber).
                    and("transStartDate", transStartDate).
                    and("transEndDate", transEndDate).
                    and("partyName", partyName).
                    and("onBehalf", onBehalf).
                    and("lampName", lampName).
                    and("mandiName", mandiName).
                    and("revisitFlag", revisitFlag).
                    and("page", String.valueOf(pageDetails.getPageNumber())).
                    and("size", String.valueOf(pageDetails.getSize())).
                    and("start",pageDetails.getStart()).
                    and("end",pageDetails.getEnd()).
                    parameters());

        }

    }

    public DOPage<TableRowMapDO<String,String>> getStockBalanceData(String industryCode,
                                                               String stockCode,
                                                               String stockVarietyCode,
                                                               PageDetails pageDetails) {

        boolean filterEnabled = false;

        if(!"".equals(industryCode)
                || !"".equals(stockCode)
                || !"".equals(stockVarietyCode)){
            filterEnabled = true;
        }

        if (!filterEnabled) {
            return ((StockDetailsService)getService()).reteriveStockBalanceByFilter((with("page", String.valueOf(pageDetails.getPageNumber())).
                    and("size", String.valueOf(pageDetails.getSize())).
                    and("start",pageDetails.getStart()).
                    and("end",pageDetails.getEnd()).parameters()));
        } else {
            return ((StockDetailsService)getService()).reteriveStockBalanceByFilter(with("industryCode", industryCode).
                    and("stockCode", stockCode).
                    and("stockVarietyCode", stockVarietyCode).
                    and("page", String.valueOf(pageDetails.getPageNumber())).
                    and("size", String.valueOf(pageDetails.getSize())).
                    and("start",pageDetails.getStart()).
                    and("end",pageDetails.getEnd()).
                    parameters());

        }
    }
    /**
     *  Delete stock details
     *
     * @param stockDetailsDO
     */
    public ResultDO onDelete(StockDetailsDO stockDetailsDO) {

        ResultDO resultDO = new ResultDO();

        logger.debug("form view on delete action");

        Optional<ButtonType> result = MessageBox.create()
                .owner(DesktopFxApplication.get().getMainStage())
                .contentText(I18n.COMMON.getString("confirm.delete"))
                .showDeleteConfirmation();

        if (result.isPresent() && result.get().getButtonData() == ButtonBar.ButtonData.OK_DONE) {
            try {

                resultDO = ((StockDetailsService)getService()).deleteStockDetails(stockDetailsDO);

                if(!resultDO.isError()){
                    onRefresh();
                }

            } catch (Exception e) {
                MessageBox.create()
                        .owner(DesktopFxApplication.get().getMainStage())
                        .contentText(I18n.COMMON.getString("exception.delete"))
                        .showError(e);
            }
        }else{
            return null; // return void
        }
        return  resultDO;
    }

    /**
     * Party Name Suggestion
     *
     * @param partyName
     * @return
     */
    public Optional<List<String>> getSuggestedPartyNames(String partyName){
        Optional<List<PartyDetailsDO>> partyNames = stockDetailsService.getPartyNameSuggestions(partyName);
        return Optional.of(partyNames.get().stream().
                    map(PartyDetailsDO::getPartyName1).
                    collect(Collectors.toList()));

    }

    @Override
    protected AbstractService<StockDetailsDO> createService() {
        return stockDetailsService;
    }

    @Override
    protected DataPageView<StockDetailsDO> createDataPageView() {
        return new StockDetailsTransactionSearchPage();
    }

    @Override
    public void openFormView(StockDetailsDO stockDetailsDO) {

        Map<String,Object> intent = new HashMap<String,Object>();

        if(!stockDetailsDO.isNew()){
            intent.put("is_edit",true);
        }

        new StockDetailsTransactionForm(this,stockDetailsDO,(StackedScreen) this.getDataPageView().getStackedScreen(),intent);
    }

    @Override
    public void onAddNew() {
        this.openFormView(new StockDetailsDO());
    }

    @Override
    public void onEdit() {

        logger.debug("on edit action");
        if (dataPageView == null) {
            return;
        }
        if (dataPageView.getSelectedModel() == null) {
            return;
        }
        this.openFormView(dataPageView.getSelectedModel());
    }

    @Override
    public ResultDO onSave(StockDetailsDO stockDetailsDO) {

        logger.debug("on save action {}", stockDetailsDO);
        try {
            if (stockDetailsDO.isNew())
                stockDetailsDO.setCreatedBy(DesktopFxApplication.get().getLoginSession().getLoggedInUser().getUserName());
            else
                stockDetailsDO.setUpdatedBy(DesktopFxApplication.get().getLoginSession().getLoggedInUser().getUserName());

            StockDetailsDO savedStockDetails = ((StockDetailsService)getService()).saveOrUpdateStockDetails(stockDetailsDO);

            if(!savedStockDetails.getResultDO().isError()){
                onRefresh();
            }

            return savedStockDetails.getResultDO();

        } catch (Exception e) {
            logger.error("save is not successful", e);
            MessageBox.create()
                    .owner(DesktopFxApplication.get().getMainStage())
                    .contentText(I18n.COMMON.getString("exception.save"))
                    .showError(e);
        }

        return new ResultDO();
    }

    /**
     * On delete direct from table.
     *
     */
    @Override
    public void onDelete() {
        logger.debug("on delete action");

        if (dataPageView == null) {
            return;
        }
        if (dataPageView.getSelectedModel() == null) {
            return;
        }

        ResultDO resultDO = onDelete(dataPageView.getSelectedModel());

        if(resultDO == null){
            return;
        }

        if(!resultDO.isError()) {
            onRefresh();
            DesktopFxApplication.showSuccessMessage("Record Deleted Successfully.");
        }else {
            DesktopFxApplication.showErrorMessageList(resultDO.getErrorList());
        }
   }

    @Override
    public String getName() {
        return "StockMaintenanceController";
    }

    public void onDownloadMandiReport(String industryCode,String lampNameCode,String mandiNameCode) throws BusinessException,TechnicalException{
        try {
            ByteArrayOutputStream report = (ByteArrayOutputStream) excelReportService.reteriveMandiReconReport(industryCode,lampNameCode,mandiNameCode);
            if(report != null) {
                UIFileUtil.openByteDataAsFile(report.toByteArray(), "MandiReport", "xls");
            }
        } catch (BusinessException | TechnicalException e1) {
           throw e1;
        }
    }

    public void onDownloadStockDetailTransaction(String industryCode,
                                                 String stockCode,
                                                 String stockVarietyCode,
                                                 String transactionType,
                                                 Date transStartDate,
                                                 Date transEndDate,
                                                 String rstNumber,
                                                 String vehicleNumber,
                                                 String partyName,
                                                 String onBehalf,
                                                 String lampName,
                                                 String mandiName,String revisitFlag) throws BusinessException,TechnicalException{

        boolean filterEnabled = false;

        if(!"".equals(industryCode)
                || !"".equals(stockCode)
                || !"".equals(stockVarietyCode)
                || !"".equals(transactionType)
                || !"".equals(rstNumber)
                || !"".equals(vehicleNumber)
                || !"".equals(partyName)
                || !"".equals(onBehalf)
                || !"".equals(lampName)
                || !"".equals(mandiName)
                || !"".equals(revisitFlag)
                || transStartDate != null
                || transEndDate != null){
            filterEnabled = true;
        }

        ByteArrayOutputStream report = null;
        if (!filterEnabled) {
            report =  (ByteArrayOutputStream)excelReportService.reteriveStockDetailTransactionsReport(with("","").parameters());
        } else {
            report =  (ByteArrayOutputStream)excelReportService.reteriveStockDetailTransactionsReport(with("industryCode", industryCode).
                    and("stockCode", stockCode).
                    and("stockVarietyCode", stockVarietyCode).
                    and("transactionType", transactionType).
                    and("rstNumber", rstNumber).
                    and("vehicleNumber", vehicleNumber).
                    and("transStartDate", transStartDate).
                    and("transEndDate", transEndDate).
                    and("partyName", partyName).
                    and("onBehalf", onBehalf).
                    and("lampName", lampName).
                    and("mandiName", mandiName).
                    and("revisitFlag", revisitFlag).
                    parameters());
        }
        if(report != null) {
            UIFileUtil.openByteDataAsFile(report.toByteArray(), "StockDtlTransaction", "xls");
        }
}

    public void toggleRevisitFlag() {

        if (dataPageView == null) {
            return;
        }
        if (dataPageView.getSelectedModel() == null) {
            return;
        }

        ResultDO resultDO = ((StockDetailsService)getService()).toggleRevisitFlag(dataPageView.getSelectedModel());

        if(resultDO == null){
            return;
        }

        if(!resultDO.isError()) {
            onRefresh();
            DesktopFxApplication.showSuccessMessage("Revisit Flag Toggled Successfully.");
        }else {
            DesktopFxApplication.showErrorMessageList(resultDO.getErrorList());
        }


    }
}