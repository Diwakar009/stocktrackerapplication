/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.domainobjs.json.sms;

import com.stocktracker.domainobjs.json.BaseDomainObject;
import com.stocktracker.model.BaseEntity;
import com.stocktracker.model.sms.Contract;
import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.stocktracker.model.sms.PartyDetails;
import org.apache.commons.lang.StringUtils;

import java.io.Serializable;
import java.util.Date;

/**
 * ContractDO .
 * 
 */
@JsonIgnoreProperties(ignoreUnknown=true)
@JsonInclude(Include.NON_EMPTY)
public class ContractDO extends BaseDomainObject implements Serializable {
	private static final long serialVersionUID = 1L;

	public static String CLASS_NAME = ContractDO.class.getName();

	public ContractDO() {
	}

	public ContractDO(Contract contract) {
		copyEntity2DomainObject(contract);
	}

	@JsonProperty("id")
	private Long id;

	@JsonProperty("industryCode")
	private String industryCode;

	@JsonProperty("startDate")
	@JsonFormat(shape = JsonFormat.Shape.STRING,pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
	private Date startDate;

	@JsonProperty("endDate")
	@JsonFormat(shape = JsonFormat.Shape.STRING,pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
	private Date endDate;

	@JsonProperty("contractTypeCode")
	private String contractTypeCode;

	@JsonProperty("contractName")
	private String contractName;

	@JsonProperty("rawMaterial")
	private String rawMaterial;

	@JsonProperty("approvedRawMaterialQuantity")
	private Double approvedRawMaterialQuantity;

	@JsonProperty("receivedRawMaterialQuantity")
	private Double receivedRawMaterialQuantity;

	@JsonProperty("deliveryProduct")
	private String deliveryProduct;

	@JsonProperty("productDeliveryQuantity")
	private Double productDeliveryQuantity;

	@JsonProperty("productDeliveredQuantity")
	private Double productDeliveredQuantity;

	@JsonProperty("status")
	private String status;

	@JsonProperty("remarks")
	private String remarks;

	@JsonProperty("moreDetails")
	private ContractMoreDetailsDO moreDetails;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIndustryCode() {
		return industryCode;
	}

	public void setIndustryCode(String industryCode) {
		this.industryCode = industryCode;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getContractTypeCode() {
		return contractTypeCode;
	}

	public void setContractTypeCode(String contractTypeCode) {
		this.contractTypeCode = contractTypeCode;
	}

	public String getContractName() {
		return contractName;
	}

	public void setContractName(String contractName) {
		this.contractName = contractName;
	}

	public String getDeliveryProduct() {
		return deliveryProduct;
	}

	public void setDeliveryProduct(String deliveryProduct) {
		this.deliveryProduct = deliveryProduct;
	}

	public String getRawMaterial() {
		return rawMaterial;
	}

	public void setRawMaterial(String rawMaterial) {
		this.rawMaterial = rawMaterial;
	}

	public Double getApprovedRawMaterialQuantity() {
		return approvedRawMaterialQuantity;
	}

	public void setApprovedRawMaterialQuantity(Double approvedRawMaterialQuantity) {
		this.approvedRawMaterialQuantity = approvedRawMaterialQuantity;
	}

	public Double getReceivedRawMaterialQuantity() {
		return receivedRawMaterialQuantity;
	}

	public void setReceivedRawMaterialQuantity(Double receivedRawMaterialQuantity) {
		this.receivedRawMaterialQuantity = receivedRawMaterialQuantity;
	}

	public Double getProductDeliveryQuantity() {
		return productDeliveryQuantity;
	}

	public void setProductDeliveryQuantity(Double productDeliveryQuantity) {
		this.productDeliveryQuantity = productDeliveryQuantity;
	}

	public Double getProductDeliveredQuantity() {
		return productDeliveredQuantity;
	}

	public void setProductDeliveredQuantity(Double productDeliveredQuantity) {
		this.productDeliveredQuantity = productDeliveredQuantity;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public ContractMoreDetailsDO getMoreDetails() {
		return moreDetails;
	}

	public void setMoreDetails(ContractMoreDetailsDO moreDetails) {
		this.moreDetails = moreDetails;
	}

	@JsonIgnore
	@Override
	public Object getDomainObjectKey() {
		return getId();
	}

	@Override
	public Contract copyDomainObject2Entity(BaseEntity entity) {
		Contract contract = null;

		if(entity == null){
			contract = new Contract();
		}else {
			contract = (Contract)entity;
		}

		contract.setCreatedBy(this.getCreatedBy());
		contract.setUpdatedBy(this.getUpdatedBy());
		contract.setCreatedAt(this.getCreatedAt());
		contract.setUpdatedAt(this.getUpdatedAt());

		contract.setId(this.getId());
		contract.setIndustryCode(this.getIndustryCode());
		contract.setContractTypeCode(this.getContractTypeCode());
		contract.setContractName(this.getContractName());
		contract.setStartDate(this.getStartDate());
		contract.setEndDate(this.getEndDate());
		contract.setRawMaterial(this.getRawMaterial());
		contract.setApprovedRawMaterialQuantity(this.getApprovedRawMaterialQuantity());
		contract.setReceivedRawMaterialQuantity(this.getReceivedRawMaterialQuantity());
		contract.setDeliveryProduct(this.getDeliveryProduct());
		contract.setProductDeliveryQuantity(this.getProductDeliveryQuantity());
		contract.setProductDeliveredQuantity(this.getProductDeliveredQuantity());
		contract.setRemarks(this.getRemarks());
		contract.setStatus(this.getStatus());
		if(this.getMoreDetails() != null) {
			contract.setMoreDetails(this.getMoreDetails().toJSON());
		}
		return contract;
	}
	@Override
	public void copyEntity2DomainObject(BaseEntity entity) {
		Contract contract = (Contract) entity;
		this.setCreatedBy(contract.getCreatedBy());
		this.setUpdatedBy(contract.getUpdatedBy());
		this.setCreatedAt(contract.getCreatedAt());
		this.setUpdatedAt(contract.getUpdatedAt());

		this.setId(contract.getId());
		this.setIndustryCode(contract.getIndustryCode());
		this.setContractTypeCode(contract.getContractTypeCode());
		this.setContractName(contract.getContractName());
		this.setStartDate(contract.getStartDate());
		this.setEndDate(contract.getEndDate());
		this.setRawMaterial(contract.getRawMaterial());
		this.setApprovedRawMaterialQuantity(contract.getApprovedRawMaterialQuantity());
		this.setReceivedRawMaterialQuantity(contract.getReceivedRawMaterialQuantity());
		this.setDeliveryProduct(contract.getDeliveryProduct());
		this.setProductDeliveryQuantity(contract.getProductDeliveryQuantity());
		this.setProductDeliveredQuantity(contract.getProductDeliveredQuantity());
		this.setRemarks(contract.getRemarks());
		this.setStatus(contract.getStatus());

		if(StringUtils.isNotBlank(contract.getMoreDetails())) {
			this.setMoreDetails(new ContractMoreDetailsDO().fromJSON(contract.getMoreDetails()));
		}

	}

	@Override
	public String toString() {
		return "ContractDO{" +
				"id=" + id +
				", industryCode='" + industryCode + '\'' +
				", startDate=" + startDate +
				", endDate=" + endDate +
				", contractTypeCode='" + contractTypeCode + '\'' +
				", contractName='" + contractName + '\'' +
				", rawMaterial='" + rawMaterial + '\'' +
				", approvedRawMaterialQuantity=" + approvedRawMaterialQuantity +
				", receivedRawMaterialQuantity=" + receivedRawMaterialQuantity +
				", deliveryProduct='" + deliveryProduct + '\'' +
				", productDeliveryQuantity=" + productDeliveryQuantity +
				", productDeliveredQuantity=" + productDeliveredQuantity +
				", status='" + status + '\'' +
				", remarks='" + remarks + '\'' +
				", moreDetails=" + moreDetails +
				'}';
	}
}