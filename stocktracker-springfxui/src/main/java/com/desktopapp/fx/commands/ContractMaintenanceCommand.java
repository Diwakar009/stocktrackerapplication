/*
 * Copyright (c) 2020.
 *
 * All Rights are with Diwakar C
 */

package com.desktopapp.fx.commands;

import com.desktopapp.fx.controller.ContractMaintenanceController;
import com.desktopapp.fx.controller.StockMaintenanceController;
import com.desktopapp.fx.objectfactory.ObjectFactory;
import com.desktopapp.fx.view.AppView;

/**
 * Created by diwakar009 on 24/12/2018.
 */
public class ContractMaintenanceCommand implements ICommand {

    private AppView view;

    private ContractMaintenanceController contractMantenanceController;

    public ContractMaintenanceCommand(AppView view) {
        this.view = view;
        this.contractMantenanceController = (ContractMaintenanceController) ObjectFactory.getControllerObject(ObjectFactory.CONTRACT_MAINTENANCE_CTRL);
    }

    @Override
    public void executeCommand() {
        this.view.addPageToCenter(contractMantenanceController.getDataPageView());
    }
}
