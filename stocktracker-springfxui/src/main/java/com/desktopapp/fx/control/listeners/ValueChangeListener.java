package com.desktopapp.fx.control.listeners;

public interface ValueChangeListener {
	
	public void valueChanged();

}
