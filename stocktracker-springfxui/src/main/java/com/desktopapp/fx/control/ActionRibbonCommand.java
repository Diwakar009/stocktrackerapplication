/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.desktopapp.fx.control;

import com.desktopapp.fx.DesktopFxApplication;
import com.desktopapp.fx.commands.ICommand;
import com.desktopapp.fx.control.fonticon.FontIcon;
import com.desktopapp.fx.control.fonticon.FontIconFactory;
import com.desktopapp.fx.control.fonticon.FontIconSize;
import javafx.scene.control.ContentDisplay;
import javafx.scene.input.KeyCodeCombination;

/**
 * Created by diwakar009 on 18/11/2019.
 */
public class ActionRibbonCommand extends ActionCommand{

    private static final String RIBBON_MENU_STYLE_CLASS = "ribbon-command";

    public ActionRibbonCommand(ICommand command, String text, FontIcon fontIcon, KeyCodeCombination keyCodeCombination) {

        super(text, fontIcon, ContentDisplay.TOP);

        if(command != null) {
            setCommand(command);
            setOnActionEvent(event -> {getCommand().executeCommand();});
        }
        setKeyCodeCombination(keyCodeCombination);

        getStyleClass().add(RIBBON_MENU_STYLE_CLASS);
    }

}
