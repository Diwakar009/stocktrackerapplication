/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.model.sms;

import com.stocktracker.framework.IEntity;
import com.stocktracker.model.BaseEntity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * The persistent class for the TBL_TRANSPORT_DTLS database table.
 * 
 */
@Entity
@Table(name = "TBL_TRANSPORT_DTLS")
@AttributeOverride(name = "id", column = @Column(name = "ID", nullable = false,
		columnDefinition = "BIGINT UNSIGNED"))
public class TransportDetails extends BaseEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "ID", nullable = false, columnDefinition = "BIGINT UNSIGNED")
	private Long id;

	/*@Column(name="INDUSTRY_CODE", length = 5)
	private String industryCode;

	@Column(name="STOCK_CODE", length = 5)
	private String stockCode;

	@Column(name="STOCK_VARIETIES_CODE", length = 5)
	private String stockVarietyCode;*/

	@Column(name="TRANSPORT_TYPE_CODE", length = 5)
	private String transportTypeCode;

	@Column(name="RST_NUMBER", length = 100)
	private String rstNumber;

	@Column(name="VEHICLE_NUMBER", length = 100)
	private String vehicleNumber;

	@Column(name="VEHICLE_CAPTAIN", length = 40)
	private String vehicleCaptain;

	@Column(name="MORE_DETAILS", length = 65535)
	private String moreDetails;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	/*public String getIndustryCode() {
		return industryCode;
	}

	public void setIndustryCode(String industryCode) {
		this.industryCode = industryCode;
	}

	public String getStockCode() {
		return stockCode;
	}

	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}

	public String getStockVarietyCode() {
		return stockVarietyCode;
	}

	public void setStockVarietyCode(String stockVarietyCode) {
		this.stockVarietyCode = stockVarietyCode;
	}*/

	public String getTransportTypeCode() {
		return transportTypeCode;
	}

	public void setTransportTypeCode(String transportTypeCode) {
		this.transportTypeCode = transportTypeCode;
	}

	public String getRstNumber() {
		return rstNumber;
	}

	public void setRstNumber(String rstNumber) {
		this.rstNumber = rstNumber;
	}

	public String getVehicleNumber() {
		return vehicleNumber;
	}

	public void setVehicleNumber(String vehicleNumber) {
		this.vehicleNumber = vehicleNumber;
	}

	public String getVehicleCaptain() {
		return vehicleCaptain;
	}

	public void setVehicleCaptain(String vehicleCaptain) {
		this.vehicleCaptain = vehicleCaptain;
	}

	public String getMoreDetails() {
		return moreDetails;
	}

	public void setMoreDetails(String moreDetails) {
		this.moreDetails = moreDetails;
	}

	@Override
	public Serializable getEntityPK() {
		return getId();
	}

	@Override
	public void copyEntity(IEntity entity) {
		TransportDetails source = (TransportDetails)entity;
		this.setCreatedBy(source.getCreatedBy());
		this.setUpdatedBy(source.getUpdatedBy());
		this.setCreatedAt(source.getCreatedAt());
		this.setUpdatedAt(source.getUpdatedAt());

		this.setId(source.getId());
		/*this.setIndustryCode(source.getIndustryCode());
		this.setStockCode(source.getStockCode());
		this.setStockVarietyCode(source.getStockVarietyCode());*/
		this.setTransportTypeCode(source.getTransportTypeCode());
		this.setRstNumber(source.getRstNumber());
		this.setVehicleNumber(source.getVehicleNumber());
		this.setVehicleCaptain(source.getVehicleCaptain());
		this.setMoreDetails(source.getMoreDetails());
	}
}