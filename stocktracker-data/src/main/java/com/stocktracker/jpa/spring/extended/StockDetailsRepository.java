/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.jpa.spring.extended;

import com.stocktracker.jpa.spring.extended.bean.StockDetailsSummary;
import com.stocktracker.jpa.spring.extended.custom.ExtendedRepository;
import com.stocktracker.jpa.spring.extended.extention.StockDetailsExtendedRepository;
import com.stocktracker.model.sms.StockDetails;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StockDetailsRepository extends ExtendedRepository<StockDetails, Long> ,StockDetailsExtendedRepository {

    /**
     * Finds the stock details with limited fields.
     *
     * @param pagable
     * @return
     */

    @Query("SELECT new StockDetails(s.id, s.industryCode, s.stockCode, s.stockVarietyCode, s.transactionType, s.transactionDate, s.noOfPackets, s.totalWeightment, " +
            " s.plusOrMinusOnTotal, s.remarks, s.onBeHalf,s.revisitFlag) FROM StockDetails s " +
            " order by  s.transactionDate desc")
    public Page<StockDetails> findAllStockDetails(Pageable pagable);


    @Query("SELECT new com.stocktracker.jpa.spring.extended.bean.StockDetailsSummary(s.industryCode, s.stockCode, s.stockVarietyCode, s.transactionType, sum(s.noOfPackets), sum(s.totalWeightment)) " +
            " FROM StockDetails s where s.stockCode in ('PADD','RICE','BRRI','BRBR','BRAN','HUSK')" +
            " group by  s.industryCode, s.stockCode, s.stockVarietyCode, s.transactionType" +
            " order by  s.industryCode, s.stockCode, s.stockVarietyCode, s.transactionType")
    public List<StockDetailsSummary> getStockDetailSummary();

}
