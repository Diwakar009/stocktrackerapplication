/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.domainobjs.json.sms;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.stocktracker.domainobjs.ICompositeDomainObjectKey;

import java.io.Serializable;

/**
 * The primary key class for the StockDOPK .
 * 
 */
@JsonIgnoreProperties (ignoreUnknown=true)
@JsonInclude(Include.NON_EMPTY)
public class StockDOPK implements Serializable,ICompositeDomainObjectKey {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@JsonProperty("industryCode")
	private String industryCode;

	@JsonProperty("stockCode")
	private String stockCode;

	@JsonProperty("stockVarietyCode")
	private String stockVarietyCode;

	public StockDOPK(String industryCode, String stockCode, String stockVarietyCode) {
		this.industryCode = industryCode;
		this.stockCode = stockCode;
		this.stockVarietyCode = stockVarietyCode;
	}

	public StockDOPK() {
	}

	public String getIndustryCode() {
		return industryCode;
	}

	public void setIndustryCode(String industryCode) {
		this.industryCode = industryCode;
	}

	public String getStockCode() {
		return stockCode;
	}

	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}

	public String getStockVarietyCode() {
		return stockVarietyCode;
	}

	public void setStockVarietyCode(String stockVarietyCode) {
		this.stockVarietyCode = stockVarietyCode;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		StockDOPK stockDOPK = (StockDOPK) o;

		if (industryCode != null ? !industryCode.equals(stockDOPK.industryCode) : stockDOPK.industryCode != null)
			return false;
		if (stockCode != null ? !stockCode.equals(stockDOPK.stockCode) : stockDOPK.stockCode != null) return false;
		return stockVarietyCode != null ? stockVarietyCode.equals(stockDOPK.stockVarietyCode) : stockDOPK.stockVarietyCode == null;
	}

	@Override
	public int hashCode() {
		int result = industryCode != null ? industryCode.hashCode() : 0;
		result = 31 * result + (stockCode != null ? stockCode.hashCode() : 0);
		result = 31 * result + (stockVarietyCode != null ? stockVarietyCode.hashCode() : 0);
		return result;
	}

	@Override
	public String toString() {
		return "StockDOPK{" +
				"industryCode='" + industryCode + '\'' +
				", stockCode='" + stockCode + '\'' +
				", stockVarietyCode='" + stockVarietyCode + '\'' +
				'}';
	}
}