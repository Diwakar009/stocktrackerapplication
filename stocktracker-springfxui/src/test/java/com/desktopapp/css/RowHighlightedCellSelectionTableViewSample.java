/*
 * Copyright (c) 2022.
 *
 * All Rights are with Diwakar C
 */

package com.desktopapp.css;

import javafx.application.Application;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener.Change;
import javafx.collections.ObservableList;
import javafx.collections.ObservableSet;
import javafx.css.PseudoClass;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class RowHighlightedCellSelectionTableViewSample extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) {
        Scene scene = new Scene(new Group());
        scene.getStylesheets().add(getClass().getResource("selected-row-table.css").toExternalForm());
        stage.setTitle("Table View Sample");
        stage.setWidth(450);
        stage.setHeight(500);


        final Label label = new Label("Address Book");
        label.setFont(new Font("Arial", 20));


        final TableView<Person> table = new TableView<>();
        final ObservableList<Person> data =
            FXCollections.observableArrayList(
                new Person("Jacob", "Smith", "jacob.smith@example.com"),
                new Person("Isabella", "Johnson", "isabella.johnson@example.com"),
                new Person("Ethan", "Williams", "ethan.williams@example.com"),
                new Person("Emma", "Jones", "emma.jones@example.com"),
                new Person("Michael", "Brown", "michael.brown@example.com")
        );
      
        table.getSelectionModel().setCellSelectionEnabled(true);
        table.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
      
        final PseudoClass selectedRowPseudoClass = PseudoClass.getPseudoClass("selected-row");
        final ObservableSet<Integer> selectedRowIndexes = FXCollections.observableSet();
        table.getSelectionModel().getSelectedCells().addListener((Change<? extends TablePosition> change) -> {
            selectedRowIndexes.clear();
            table.getSelectionModel().getSelectedCells().stream().map(TablePosition::getRow).forEach(row -> {
                selectedRowIndexes.add(row);
            });
        });
      
        table.setRowFactory(tableView -> {
            final TableRow<Person> row = new TableRow<>();
            BooleanBinding selectedRow = Bindings.createBooleanBinding(() ->
                    selectedRowIndexes.contains(new Integer(row.getIndex())), row.indexProperty(), selectedRowIndexes);
            selectedRow.addListener((observable, oldValue, newValue) ->
                row.pseudoClassStateChanged(selectedRowPseudoClass, newValue)
            );
            return row ;
        });

        TableColumn<Person, String> firstNameCol = new TableColumn<>("First Name");
        firstNameCol.setMinWidth(100);
        firstNameCol.setCellValueFactory(new PropertyValueFactory<>("firstName"));

        TableColumn<Person, String> lastNameCol = new TableColumn<>("Last Name");
        lastNameCol.setMinWidth(100);
        lastNameCol.setCellValueFactory(new PropertyValueFactory<>("lastName"));

        TableColumn<Person, String> emailCol = new TableColumn<>("Email");
        emailCol.setMinWidth(200);
        emailCol.setCellValueFactory(new PropertyValueFactory<>("email"));
      

        table.setItems(data);
        table.getColumns().addAll(firstNameCol, lastNameCol, emailCol);
      
        final VBox vbox = new VBox();
        vbox.setSpacing(5);
        vbox.setPadding(new Insets(10, 0, 0, 10));
        vbox.getChildren().addAll(label, table);

        ((Group) scene.getRoot()).getChildren().addAll(vbox);

        stage.setScene(scene);
        stage.show();
    }

    public static class Person {

        private final StringProperty firstName;
        private final StringProperty lastName;
        private final StringProperty email;

        private Person(String fName, String lName, String email) {
            this.firstName = new SimpleStringProperty(fName);
            this.lastName = new SimpleStringProperty(lName);
            this.email = new SimpleStringProperty(email);
        }

        public String getFirstName() {
            return firstName.get();
        }

        public void setFirstName(String fName) {
            firstName.set(fName);
        }
      
        public StringProperty firstNameProperty() {
            return firstName ;
        }

        public String getLastName() {
            return lastName.get();
        }

        public void setLastName(String fName) {
            lastName.set(fName);
        }
      
        public StringProperty lastNameProperty() {
            return lastName ;
        }

        public String getEmail() {
            return email.get();
        }

        public void setEmail(String fName) {
            email.set(fName);
        }
      
        public StringProperty emailProperty() {
            return email ;
        }
    }
}