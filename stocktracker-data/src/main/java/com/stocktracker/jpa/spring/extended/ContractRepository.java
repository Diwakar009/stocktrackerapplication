/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.jpa.spring.extended;

import com.stocktracker.jpa.spring.extended.custom.ExtendedRepository;
import com.stocktracker.jpa.spring.extended.extention.ContractExtendedRepository;
import com.stocktracker.model.AppUser;
import com.stocktracker.model.BaseEntity;
import com.stocktracker.model.sms.Contract;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface ContractRepository extends ExtendedRepository<Contract, Long> , ContractExtendedRepository {
    @Query(value="select c from Contract c where c.industryCode=:industryCode and c.status ='OPN'")
    Contract findOpenContract(@Param("industryCode")  String industryCode);
}
