/*
 * Copyright (c) 2020.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.exceptions;

public class BaseException extends Exception {
    public BaseException(String s) {
        super(s);
    }
}
