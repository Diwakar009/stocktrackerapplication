-- Mandi Paddy Report
select fn_codedecode_desc('CD_INDUSTRY_CODE','EN_US', result.MillName) AS 'Mill Name',
       fn_codedecode_desc('CD_STOCK_CODE','EN_US', result.StockCode) AS 'Stock Name',
       fn_codedecode_desc('CD_STOCK_VARIETY','EN_US', result.StockVarietyCode) AS 'Stock Variety',
       fn_codedecode_desc('CD_LAMP_CODE','EN_US', result.LampName) AS 'Lamp Name',
       fn_codedecode_desc('CD_MANDI_NAME','EN_US', result.MandiName) AS 'Mandi Name',
       result.PartyName as 'PartyName',
       id as 'id',
       result.Total_Received,
       result.Total_Adjusted,
       ROUND(result.Total_Received + result.Total_Adjusted,2) as 'Recv And Adjusted',
       result.Total_Entried,
       Entry_ADJ as 'Entry_ADJ',
       Entry_After_ADJ as 'Entry_After_ADJ',
       (ROUND(result.Total_Received + result.Total_Adjusted,2) - result.Total_Entried) as 'Difference',
       result.OnBehalf as 'OnBehalf'
from (

(select stockdetai0_.INDUSTRY_CODE as 'MillName',stockdetai0_.stock_code as 'StockCode',stockdetai0_.STOCK_VARIETIES_CODE as 'StockVarietyCode',stockdetai0_.LAMP_NAME_CODE as 'LampName',stockdetai0_.MANDI_NAME_CODE as 'MandiName' ,stockdetai0_.TRANSACTION_TYPE as 'transactionType',
partydetai2_.PARTY_NAME_1 as 'PartyName',stockdetai0_.id as  'id',
ROUND(sum(concat(stockdetai0_.PLUS_OR_MINUS_ON_TOTAL,stockdetai0_.TOTAL_WEIGHTMENT)),2) as 'Total_Received',
ROUND(sum(stockdetai0_.LAMP_ENTRY_IN_QTL),2) as 'Total_Entried',
stockdetai0_.LAMP_ENTRY_ADJ_IN_QTL as 'Entry_ADJ',
ROUND(sum(stockdetai0_.LAMP_ENTRY_AFT_ADJ_IN_QTL),2) as 'Entry_After_ADJ',
ROUND(sum(IF(TRANSACTION_TYPE = 'MDAJ', stockdetai0_.LAMP_ENTRY_IN_QTL, 0)),2) as 'Total_Adjusted',
stockdetai0_.ONBEHALF as 'OnBehalf'

from
        TBL_STOCK_DTLS stockdetai0_
    left outer join
        TBL_TRANSPORT_DTLS transportd1_
            on stockdetai0_.TRANSPORT_ID=transportd1_.ID
    left outer join
        TBL_PARTY_DTLS partydetai2_
            on stockdetai0_.PARTY_ID=partydetai2_.ID
where TRANSACTION_TYPE in ('MDRE','MDAJ','MDRT','MDEN')
and OnBehalf = 'KMR'
group by  LampName,MandiName,PARTY_NAME_1,id with rollup) as result
);





select * from tbl_stock ;

-- Number of paddy in offer
select stockdetai0_.INDUSTRY_CODE as 'MillName',
stockdetai0_.stock_code as 'StockCode',
stockdetai0_.STOCK_VARIETIES_CODE as 'StockVarietyCode',
stockdetai0_.TRANSACTION_TYPE as 'transactionType',
SUM(stockdetai0_.NO_OF_PACKETS) as 'Total_Packets',
ROUND(SUM(stockdetai0_.TOTAL_WEIGHTMENT),2) as 'Total_Weightment'
from
        TBL_STOCK_DTLS stockdetai0_
    left outer join
        TBL_TRANSPORT_DTLS transportd1_
            on stockdetai0_.TRANSPORT_ID=transportd1_.ID
    left outer join
        TBL_PARTY_DTLS partydetai2_
            on stockdetai0_.PARTY_ID=partydetai2_.ID
where TRANSACTION_TYPE in ('OFFER','MDRE')
group by MillName,StockCode,StockVarietyCode,transactionType;

select stockdetai0_.INDUSTRY_CODE as 'MillName',
stockdetai0_.stock_code as 'StockCode',
stockdetai0_.STOCK_VARIETIES_CODE as 'StockVarietyCode',
stockdetai0_.TRANSACTION_TYPE as 'transactionType',
SUM(stockdetai0_.NO_OF_PACKETS) as 'Total_Packets',
ROUND(SUM(stockdetai0_.TOTAL_WEIGHTMENT),2) as 'Total_Weightment'
from
        TBL_STOCK_DTLS stockdetai0_
    left outer join
        TBL_TRANSPORT_DTLS transportd1_
            on stockdetai0_.TRANSPORT_ID=transportd1_.ID
    left outer join
        TBL_PARTY_DTLS partydetai2_
            on stockdetai0_.PARTY_ID=partydetai2_.ID
where TRANSACTION_TYPE in ('KMDEL')
order by MillName,StockCode,StockVarietyCode,transactionType;


select stockdetai0_.INDUSTRY_CODE as 'MillName',
stockdetai0_.stock_code as 'StockCode',
stockdetai0_.STOCK_VARIETIES_CODE as 'StockVarietyCode',
stockdetai0_.TRANSACTION_TYPE as 'transactionType',
SUM(stockdetai0_.NO_OF_PACKETS) as 'Total_Packets',
ROUND(SUM(stockdetai0_.TOTAL_WEIGHTMENT),2) as 'Total_Weightment'
from
        TBL_STOCK_DTLS stockdetai0_
where TRANSACTION_TYPE in ('PACK','KMDEL','OFFER')
group by MillName,StockCode,StockVarietyCode,transactionType with rollup;




select INDUSTRY_CODE,STOCK_CODE,STOCK_VARIETIES_CODE,SUM(NO_OF_PACKETS),SUM(TOTAL_WEIGHTMENT),ROUND(SUM(costprice.ACTUAL_AMOUNT),2) from tbl_stock_dtls st
left outer join
        tbl_costprice_dtls costprice
            on st.costprice_id=costprice.ID
where STOCK_CODE = 'HUSK'
GROUP BY INDUSTRY_CODE,STOCK_CODE,STOCK_VARIETIES_CODE WITH ROLLUP;

select * from tbl_stock;














select * from tbl_stock_dtls;



DELIMITER $$
 
CREATE FUNCTION fn_codedecode_desc(
    codename varchar(50),
    lang varchar(5),
    codevalue varchar(50)
) 
RETURNS VARCHAR(1000)
DETERMINISTIC
BEGIN
    DECLARE description varchar(1000);
 
   select CODE_DESC into description from static_code_decode 
   where CODE_NAME = codename and 
   language = lang and 
   code_value = codevalue LIMIT 0,1;
   
    RETURN (description);
   
END$$
DELIMITER ;

execute fn_codedecode_desc('CD_CONTRACT_STATUS','EN_US','CLO') 


select * from tbl_stock;

select 
STOCK_CODE,TRANSACTION_TYPE,partydetai2_.PARTY_NAME_1,sum(NO_OF_PACKETS),sum(TOTAL_WEIGHTMENT) 
from TBL_STOCK_DTLS stockdetai0_
    left outer join
        TBL_TRANSPORT_DTLS transportd1_
            on stockdetai0_.TRANSPORT_ID=transportd1_.ID
    left outer join
        TBL_PARTY_DTLS partydetai2_
            on stockdetai0_.PARTY_ID=partydetai2_.ID
            
where TRANSACTION_Type IN ('PURC','SALE')
group by STOCK_CODE,TRANSACTION_TYPE,partydetai2_.PARTY_NAME_1 with rollup;





select fn_codedecode_desc('CD_INDUSTRY_CODE','EN_US', result.MillName) AS 'Mill Name',
       fn_codedecode_desc('CD_STOCK_CODE','EN_US', result.StockCode) AS 'Stock Name',
       fn_codedecode_desc('CD_STOCK_VARIETY','EN_US', result.StockVarietyCode) AS 'Stock Variety',
       fn_codedecode_desc('CD_LAMP_CODE','EN_US', result.LampName) AS 'Lamp Name',
       fn_codedecode_desc('CD_MANDI_NAME','EN_US', result.MandiName) AS 'Mandi Name',
       result.PartyName as 'PartyName',
       result.Total_Received,
       result.Total_Adjusted,
       ROUND(ifnull(result.Total_Received,0.00) + result.Total_Adjusted,2) as 'Recv And Adjusted',
       result.Total_Entried,
       (ROUND(ifnull(result.Total_Received,0.00) + result.Total_Adjusted,2) - result.Total_Entried) as 'Difference',
       result.OnBehalf as 'OnBehalf'
from (

(select stockdetai0_.INDUSTRY_CODE as 'MillName',stockdetai0_.stock_code as 'StockCode',stockdetai0_.STOCK_VARIETIES_CODE as 'StockVarietyCode',stockdetai0_.LAMP_NAME_CODE as 'LampName',stockdetai0_.MANDI_NAME_CODE as 'MandiName' ,stockdetai0_.TRANSACTION_TYPE as 'transactionType',partydetai2_.PARTY_NAME_1 as 'PartyName',
ROUND(sum(concat(stockdetai0_.PLUS_OR_MINUS_ON_TOTAL,stockdetai0_.TOTAL_WEIGHTMENT)),2) as 'Total_Received',ROUND(sum(stockdetai0_.LAMP_ENTRY_IN_QTL),2) as 'Total_Entried',ROUND(sum(IF(TRANSACTION_TYPE = 'MDAJ', stockdetai0_.LAMP_ENTRY_IN_QTL, 0)),2) as 'Total_Adjusted',
stockdetai0_.ONBEHALF as 'OnBehalf'

from
        TBL_STOCK_DTLS stockdetai0_
    left outer join
        TBL_TRANSPORT_DTLS transportd1_
            on stockdetai0_.TRANSPORT_ID=transportd1_.ID
    left outer join
        TBL_PARTY_DTLS partydetai2_
            on stockdetai0_.PARTY_ID=partydetai2_.ID
where TRANSACTION_TYPE in ('MDRE','MDAJ','MDRT','MDEN')
and INDUSTRY_CODE = 'SMR'
and OnBehalf = 'SMR'
group by  LampName,MandiName,PARTY_NAME_1 with rollup) as result
);


select * from tbl_stock_dtls; 


-- Duplicate RST Check

select RST_NUMBER , count(RST_NUMBER) from tbl_stock_dtls stockdetai0_
  left outer join
        TBL_TRANSPORT_DTLS transportd1_ 
            on stockdetai0_.TRANSPORT_ID=transportd1_.ID 
where 
INDUSTRY_CODE='SMR' and TRANSACTION_TYPE = 'MDRE'
group by RST_NUMBER 
having count(RST_NUMBER) > 1;





