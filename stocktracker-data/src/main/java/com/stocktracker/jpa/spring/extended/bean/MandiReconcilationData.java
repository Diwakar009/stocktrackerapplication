/*
 * Copyright (c) 2020.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.jpa.spring.extended.bean;


import java.util.Date;

public class MandiReconcilationData {

    private Long id;
    private String industryCode;
    private String stockCode;
    private String stockVarietyCode;
    private String transactionType;
    private String lampNameCode;
    private String mandiNameCode;
    private String partyName;
    private Double totalReceived;
    private Double totalAdjusted;
    private Double totalReceivedNAdjustedNPurchased;
    private Double totalEntered;
    private Double lampEntryAdjustment;
    private Double totalLampEntryAfterAdjustment;
    private Double difference;
    private String onBehalf;
    private Date transactionDate;
    private String vehicleNumber;
    private String rstNumber;
    private Double totalExcessPurchased;

    public MandiReconcilationData() {
    }

    public MandiReconcilationData(Long id, String industryCode, String stockCode, String stockVarietyCode,
                                  String transactionType, String lampNameCode, String mandiNameCode,
                                  String partyName, Double totalReceived, Double totalAdjusted, Double totalReceivedNAdjustedNPurchased,
                                  Double totalEntered, Double lampEntryAdjustment, Double totalLampEntryAfterAdjustment,
                                  Double difference, String onBehalf,String vehicleNumber,Double totalExcessPurchased) {
        this.id = id;
        this.industryCode = industryCode;
        this.stockCode = stockCode;
        this.stockVarietyCode = stockVarietyCode;
        this.transactionType = transactionType;
        this.lampNameCode = lampNameCode;
        this.mandiNameCode = mandiNameCode;
        this.partyName = partyName;
        this.totalReceived = totalReceived;
        this.totalAdjusted = totalAdjusted;
        this.totalReceivedNAdjustedNPurchased = totalReceivedNAdjustedNPurchased;
        this.totalEntered = totalEntered;
        this.lampEntryAdjustment = lampEntryAdjustment;
        this.totalLampEntryAfterAdjustment = totalLampEntryAfterAdjustment;
        this.difference = difference;
        this.onBehalf = onBehalf;
        this.vehicleNumber = vehicleNumber;
        this.totalExcessPurchased=totalExcessPurchased;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIndustryCode() {
        return industryCode;
    }

    public void setIndustryCode(String industryCode) {
        this.industryCode = industryCode;
    }

    public String getStockCode() {
        return stockCode;
    }

    public void setStockCode(String stockCode) {
        this.stockCode = stockCode;
    }

    public String getStockVarietyCode() {
        return stockVarietyCode;
    }

    public void setStockVarietyCode(String stockVarietyCode) {
        this.stockVarietyCode = stockVarietyCode;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getLampNameCode() {
        return lampNameCode;
    }

    public void setLampNameCode(String lampNameCode) {
        this.lampNameCode = lampNameCode;
    }

    public String getMandiNameCode() {
        return mandiNameCode;
    }

    public void setMandiNameCode(String mandiNameCode) {
        this.mandiNameCode = mandiNameCode;
    }

    public String getPartyName() {
        return partyName;
    }

    public void setPartyName(String partyName) {
        this.partyName = partyName;
    }

    public Double getTotalReceived() {
        return totalReceived;
    }

    public void setTotalReceived(Double totalReceived) {
        this.totalReceived = totalReceived;
    }

    public Double getTotalAdjusted() {
        return totalAdjusted;
    }

    public void setTotalAdjusted(Double totalAdjusted) {
        this.totalAdjusted = totalAdjusted;
    }

    public Double getTotalReceivedNAdjustedNPurchased() {
        return totalReceivedNAdjustedNPurchased;
    }

    public void setTotalReceivedNAdjustedNPurchased(Double totalReceivedNAdjustedNPurchased) {
        this.totalReceivedNAdjustedNPurchased = totalReceivedNAdjustedNPurchased;
    }

    public Double getTotalEntered() {
        return totalEntered;
    }

    public void setTotalEntered(Double totalEntered) {
        this.totalEntered = totalEntered;
    }

    public Double getLampEntryAdjustment() {
        return lampEntryAdjustment;
    }

    public void setLampEntryAdjustment(Double lampEntryAdjustment) {
        this.lampEntryAdjustment = lampEntryAdjustment;
    }

    public Double getTotalLampEntryAfterAdjustment() {
        return totalLampEntryAfterAdjustment;
    }

    public void setTotalLampEntryAfterAdjustment(Double totalLampEntryAfterAdjustment) {
        this.totalLampEntryAfterAdjustment = totalLampEntryAfterAdjustment;
    }

    public Double getDifference() {
        return difference;
    }

    public void setDifference(Double difference) {
        this.difference = difference;
    }

    public String getOnBehalf() {
        return onBehalf;
    }

    public void setOnBehalf(String onBehalf) {
        this.onBehalf = onBehalf;
    }

    public String getVehicleNumber() {
        return vehicleNumber;
    }

    public void setVehicleNumber(String vehicleNumber) {
        this.vehicleNumber = vehicleNumber;
    }

    public Double getTotalExcessPurchased() {
        return totalExcessPurchased;
    }

    public void setTotalExcessPurchased(Double totalExcessPurchased) {
        this.totalExcessPurchased = totalExcessPurchased;
    }

    @Override
    public String toString() {
        return "MandiStockDetails{" +
                "id=" + id +
                ", industryCode='" + industryCode + '\'' +
                ", stockCode='" + stockCode + '\'' +
                ", stockVarietyCode='" + stockVarietyCode + '\'' +
                ", transactionType='" + transactionType + '\'' +
                ", lampNameCode='" + lampNameCode + '\'' +
                ", mandiNameCode='" + mandiNameCode + '\'' +
                ", partyName='" + partyName + '\'' +
                ", totalReceived=" + totalReceived +
                ", totalAdjusted=" + totalAdjusted +
                ", totalReceivedNAdjusted=" + totalReceivedNAdjustedNPurchased +
                ", totalEntered=" + totalEntered +
                ", lampEntryAdjustment=" + lampEntryAdjustment +
                ", totalLampEntryAfterAdjustment=" + totalLampEntryAfterAdjustment +
                ", difference=" + difference +
                ", onBehalf='" + onBehalf + '\'' +
                ", transactionDate='" + transactionDate + '\'' +
                ", vehicleNumber='" + vehicleNumber + '\'' +
                ", rstNumber='" + rstNumber + '\'' +
                ", totalExcessPurchased='" + totalExcessPurchased + '\'' +
                '}';
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setRstNumber(String rstNumber) {
        this.rstNumber = rstNumber;
    }

    public String getRstNumber() {
        return rstNumber;
    }
}
