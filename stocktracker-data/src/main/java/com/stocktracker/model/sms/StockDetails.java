/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.model.sms;

import com.stocktracker.framework.IEntity;
import com.stocktracker.model.BaseEntity;
import org.hibernate.annotations.*;
import org.hibernate.annotations.CascadeType;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * The persistent class for the TBL_STOCK_DTLS database table.
 *
 */
@Entity
@Table(name = "TBL_STOCK_DTLS")
@AttributeOverride(name = "id", column = @Column(name = "ID", nullable = false,
		columnDefinition = "BIGINT UNSIGNED"))
public class StockDetails extends BaseEntity implements Serializable {
	private static final long serialVersionUID = 1L;


	public StockDetails() {
	}

	public StockDetails(String industryCode,
						String stockCode,
						String stockVarietyCode,
						String transactionType,
						Long noOfPackets,
						Double totalWeightment) {
		this.industryCode = industryCode;
		this.stockCode = stockCode;
		this.stockVarietyCode = stockVarietyCode;
		this.transactionType = transactionType;
		this.noOfPackets = noOfPackets;
		this.totalWeightment = totalWeightment;
	}

	public StockDetails(Long id, String industryCode,
						String stockCode,
						String stockVarietyCode,
						String transactionType,
						Date transactionDate,
						Long noOfPackets,
						Double totalWeightment,
						Character plusOrMinusOnTotal,
						Double lampEntryInQtl,
						Double lampEntryAfterAdjInQtl,
						String remarks,
						String onBeHalf,
						String rstNumber,
						String vehicleNumber,
						String partyName1,
						String lampNameCode,
						String mandiNameCode,
						Double appliedRate,
						Double actualAmount,
						Long deliveryLatNo,String revisitFlag) {
		this.id = id;
		this.industryCode = industryCode;
		this.stockCode = stockCode;
		this.stockVarietyCode = stockVarietyCode;
		this.transactionType = transactionType;
		this.transactionDate = transactionDate;
		this.noOfPackets = noOfPackets;
		this.totalWeightment = totalWeightment;
		this.plusOrMinusOnTotal = plusOrMinusOnTotal;
		this.lampNameCode=lampNameCode;
		this.mandiNameCode=mandiNameCode;
		this.lampEntryInQtl=lampEntryInQtl;
		this.lampEntryAfterAdjInQtl=lampEntryAfterAdjInQtl;
		this.remarks = remarks;
		this.onBeHalf = onBeHalf;
		this.revisitFlag = revisitFlag;


		this.transportDetails = new TransportDetails();
		this.transportDetails.setRstNumber(rstNumber);
		this.transportDetails.setVehicleNumber(vehicleNumber);

		this.partyDetails = new PartyDetails();
		this.partyDetails.setPartyName1(partyName1);

		this.costPriceDetails = new CostPriceDetails();
		this.costPriceDetails.setAppliedRate(appliedRate);
		this.costPriceDetails.setActualAmount(actualAmount);

		this.deliveryDetails = new DeliveryDetails();
		this.deliveryDetails.setDeliveryLatNo(deliveryLatNo);
	}

	public StockDetails(Long id, String industryCode, String stockCode, String stockVarietyCode, String transactionType, Date transactionDate, Long noOfPackets, Double totalWeightment, Character plusOrMinusOnTotal, String remarks, String onBeHalf,String revisitFlag) {
		this.id = id;
		this.industryCode = industryCode;
		this.stockCode = stockCode;
		this.stockVarietyCode = stockVarietyCode;
		this.transactionType = transactionType;
		this.transactionDate = transactionDate;
		this.noOfPackets = noOfPackets;
		this.totalWeightment = totalWeightment;
		this.plusOrMinusOnTotal = plusOrMinusOnTotal;
		this.remarks = remarks;
		this.onBeHalf = onBeHalf;
		this.revisitFlag = revisitFlag;

	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "ID", nullable = false, columnDefinition = "BIGINT UNSIGNED")
	private Long id;

	@Column(name="INDUSTRY_CODE", length = 5)
	private String industryCode;

	@Column(name="STOCK_CODE", length = 5)
	private String stockCode;

	@Column(name="STOCK_VARIETIES_CODE", length = 5)
	private String stockVarietyCode;

	@Column(name="TRANSACTION_TYPE", length = 5)
	private String transactionType;

	@Column(name="TRANSACTION_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date transactionDate;

	@Column(name = "NO_OF_PACKETS", nullable = true, columnDefinition = "BIGINT SIGNED")
	private Long noOfPackets;

	@Column(name = "TOTAL_WEIGHTMENT", nullable = true, columnDefinition = "DECIMAL SIGNED")
	private Double totalWeightment;

	@Column(name = "LAMP_ENTRY_IN_QTL", nullable = true, columnDefinition = "DECIMAL SIGNED")
	private Double lampEntryInQtl;

	@Column(name="LAMP_NAME_CODE", length = 5)
	private String lampNameCode;

	@Column(name="MANDI_NAME_CODE", length = 5)
	private String mandiNameCode;

	@Column(name = "PLUS_OR_MINUS_ON_TOTAL", length = 1)
	private Character plusOrMinusOnTotal;

	@Column(name="REMARKS", length = 100)
	private String remarks;

	@Column(name="ONBEHALF", length = 5)
	private String onBeHalf;

	@Column(name="MORE_DETAILS", length = 65535)
	private String moreDetails;

	@Column(name = "LAMP_ENTRY_ADJ_IN_QTL", nullable = true)
	private Double lampEntryAdjInQtl;

	@Column(name = "LAMP_ENTRY_AFT_ADJ_IN_QTL", nullable = true, columnDefinition = "DECIMAL SIGNED")
	private Double lampEntryAfterAdjInQtl;

	@Column(name="SETTLEMENT_STATUS", length = 5)
	private String settlementStatus;

	@Column(name="REVISIT_FLAG", length = 5)
	private String revisitFlag;

	@OneToOne
	@JoinColumn(name="PARTY_ID")
	//@Cascade({CascadeType.ALL,CascadeType.DELETE_ORPHAN})
	@Cascade(value = org.hibernate.annotations.CascadeType.ALL)
	private PartyDetails partyDetails;

	@OneToOne
	@JoinColumn(name="TRANSPORT_ID")
	@Cascade(value = org.hibernate.annotations.CascadeType.ALL)
	private TransportDetails transportDetails;

	@OneToOne
	@JoinColumn(name="DELIVERY_ID")
	@Cascade(value = org.hibernate.annotations.CascadeType.ALL)
	private DeliveryDetails deliveryDetails;

	@OneToOne
	@JoinColumn(name="CONTRACT_ID")
	@Cascade(value = org.hibernate.annotations.CascadeType.REFRESH)
	private Contract contractDetails;

	@OneToOne
	@JoinColumn(name="COSTPRICE_ID")
	@Cascade(value = org.hibernate.annotations.CascadeType.ALL)
	private CostPriceDetails costPriceDetails;

	@OneToOne
	@JoinColumns(
			{
					@JoinColumn(updatable=false,insertable=false, name="INDUSTRY_CODE", referencedColumnName="INDUSTRY_CODE"),
					@JoinColumn(updatable=false,insertable=false, name="STOCK_CODE", referencedColumnName="STOCK_CODE"),
					@JoinColumn(updatable=false,insertable=false, name="STOCK_VARIETIES_CODE", referencedColumnName="STOCK_VARIETIES_CODE")
			}
	)
	@Cascade(value = org.hibernate.annotations.CascadeType.REFRESH)
	private Stock stock;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getStockCode() {
		return stockCode;
	}

	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}

	public String getStockVarietyCode() {
		return stockVarietyCode;
	}

	public void setStockVarietyCode(String stockVarietyCode) {
		this.stockVarietyCode = stockVarietyCode;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public Long getNoOfPackets() {
		return noOfPackets;
	}

	public void setNoOfPackets(Long noOfPackets) {
		this.noOfPackets = noOfPackets;
	}

	public Double getTotalWeightment() {
		return totalWeightment;
	}

	public void setTotalWeightment(Double totalWeightment) {
		this.totalWeightment = totalWeightment;
	}

	public Character getPlusOrMinusOnTotal() {
		return plusOrMinusOnTotal;
	}

	public void setPlusOrMinusOnTotal(Character plusOrMinusOnTotal) {
		this.plusOrMinusOnTotal = plusOrMinusOnTotal;
	}

	public Double getLampEntryInQtl() {
		return lampEntryInQtl;
	}

	public void setLampEntryInQtl(Double lampEntryInQtl) {
		this.lampEntryInQtl = lampEntryInQtl;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getOnBeHalf() {
		return onBeHalf;
	}

	public void setOnBeHalf(String onBeHalf) {
		this.onBeHalf = onBeHalf;
	}

	public String getIndustryCode() {
		return industryCode;
	}

	public void setIndustryCode(String industryCode) {
		this.industryCode = industryCode;
	}

	public String getMoreDetails() {
		return moreDetails;
	}

	public void setMoreDetails(String moreDetails) {
		this.moreDetails = moreDetails;
	}

	public PartyDetails getPartyDetails() {
		return partyDetails;
	}

	public void setPartyDetails(PartyDetails partyDetails) {
		this.partyDetails = partyDetails;
	}

	public TransportDetails getTransportDetails() {
		return transportDetails;
	}

	public void setTransportDetails(TransportDetails transportDetails) {
		this.transportDetails = transportDetails;
	}

	public DeliveryDetails getDeliveryDetails() {
		return deliveryDetails;
	}

	public void setDeliveryDetails(DeliveryDetails deliveryDetails) {
		this.deliveryDetails = deliveryDetails;
	}

	public Stock getStock() {
		return stock;
	}

	public void setStock(Stock stock) {
		this.stock = stock;
	}

	public String getLampNameCode() {
		return lampNameCode;
	}

	public void setLampNameCode(String lampNameCode) {
		this.lampNameCode = lampNameCode;
	}

	public Contract getContractDetails() {
		return contractDetails;
	}

	public void setContractDetails(Contract contractDetails) {
		this.contractDetails = contractDetails;
	}

	public CostPriceDetails getCostPriceDetails() {
		return costPriceDetails;
	}

	public void setCostPriceDetails(CostPriceDetails costPriceDetails) {
		this.costPriceDetails = costPriceDetails;
	}

	public String getMandiNameCode() {
		return mandiNameCode;
	}

	public void setMandiNameCode(String mandiNameCode) {
		this.mandiNameCode = mandiNameCode;
	}

	public Double getLampEntryAdjInQtl() {
		return lampEntryAdjInQtl;
	}

	public void setLampEntryAdjInQtl(Double lampEntryAdjInQtl) {
		this.lampEntryAdjInQtl = lampEntryAdjInQtl;
	}

	public Double getLampEntryAfterAdjInQtl() {
		return lampEntryAfterAdjInQtl;
	}

	public void setLampEntryAfterAdjInQtl(Double lampEntryAfterAdjInQtl) {
		this.lampEntryAfterAdjInQtl = lampEntryAfterAdjInQtl;
	}

	public String getSettlementStatus() {
		return settlementStatus;
	}

	public void setSettlementStatus(String settlementStatus) {
		this.settlementStatus = settlementStatus;
	}

	public String getRevisitFlag() {
		return revisitFlag;
	}

	public void setRevisitFlag(String revisitFlag) {
		this.revisitFlag = revisitFlag;
	}

	@Override
	public Serializable getEntityPK() {
		return getId();
	}

	@Override
	public void copyEntity(IEntity entity) {
		StockDetails source = (StockDetails)entity;
		this.setCreatedBy(source.getCreatedBy());
		this.setUpdatedBy(source.getUpdatedBy());
		this.setCreatedAt(source.getCreatedAt());
		this.setUpdatedAt(source.getUpdatedAt());
		this.setIndustryCode(source.getIndustryCode());
		this.setStockCode(source.getStockCode());
		this.setStockVarietyCode(source.getStockVarietyCode());
		this.setTransactionType(source.getTransactionType());
		this.setTransactionDate(source.getTransactionDate());
		this.setNoOfPackets(source.getNoOfPackets());
		this.setTotalWeightment(source.getTotalWeightment());
		this.setPlusOrMinusOnTotal(source.getPlusOrMinusOnTotal());
		this.setLampEntryInQtl(source.getLampEntryInQtl());
		this.setLampNameCode(source.getLampNameCode());
		this.setMandiNameCode(source.getMandiNameCode());
		this.setRemarks(source.getRemarks());
		this.setOnBeHalf(source.getOnBeHalf());
		this.setRemarks(source.getRemarks());
		this.setMoreDetails(source.getMoreDetails());

		this.setLampEntryAdjInQtl(source.getLampEntryAdjInQtl());
		this.setLampEntryAfterAdjInQtl(source.getLampEntryAfterAdjInQtl());
		this.setSettlementStatus(source.getSettlementStatus());
		this.setRevisitFlag(source.getRevisitFlag());

		if(this.partyDetails != null){
			this.partyDetails.copyEntity(source.getPartyDetails());
		}

		if(this.deliveryDetails != null){
			this.deliveryDetails.copyEntity(source.getDeliveryDetails());
		}

		if(this.transportDetails != null){
			this.transportDetails.copyEntity(source.getTransportDetails());
		}

		if(this.contractDetails != null){
			this.contractDetails.copyEntity(source.getContractDetails());
		}

		if(this.costPriceDetails != null){
			this.costPriceDetails.copyEntity(source.getCostPriceDetails());
		}

	}

	@Override
	public String toString() {
		return "StockDetails{" +
				"id=" + id +
				", industryCode='" + industryCode + '\'' +
				", stockCode='" + stockCode + '\'' +
				", stockVarietyCode='" + stockVarietyCode + '\'' +
				", transactionType='" + transactionType + '\'' +
				", transactionDate=" + transactionDate +
				", noOfPackets=" + noOfPackets +
				", totalWeightment=" + totalWeightment +
				", lampEntryInQtl=" + lampEntryInQtl +
				", lampNameCode='" + lampNameCode + '\'' +
				", mandiNameCode='" + mandiNameCode + '\'' +
				", plusOrMinusOnTotal=" + plusOrMinusOnTotal +
				", remarks='" + remarks + '\'' +
				", onBeHalf='" + onBeHalf + '\'' +
				", moreDetails='" + moreDetails + '\'' +
				", lampEntryAdjInQtl=" + lampEntryAdjInQtl +
				", lampEntryAfterAdjInQtl=" + lampEntryAfterAdjInQtl +
				", settlementStatus='" + settlementStatus + '\'' +
				", revisitFlag='" + revisitFlag + '\'' +
				", partyDetails=" + partyDetails +
				", transportDetails=" + transportDetails +
				", deliveryDetails=" + deliveryDetails +
				", contractDetails=" + contractDetails +
				", costPriceDetails=" + costPriceDetails +
				", stock=" + stock +
				'}';
	}
}