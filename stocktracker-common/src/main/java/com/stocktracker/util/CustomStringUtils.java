/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.util;

import java.lang.reflect.InvocationTargetException;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang3.CharUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class CustomStringUtils {

	
	/**
	 * Returns a string form of the given bean by converting it into a map.
	 * 
	 * @param bean
	 * @return
	 */
	public static String beanToString(Object bean) {
		try {
			return PropertyUtils.describe(bean).toString();
		} catch (IllegalAccessException e) {
			e.printStackTrace(); // To change body of catch statement use
									// Options | File Templates.
		} catch (InvocationTargetException e) {
			e.printStackTrace(); // To change body of catch statement use
									// Options | File Templates.
		} catch (NoSuchMethodException e) {
			e.printStackTrace(); // To change body of catch statement use
									// Options | File Templates.
		}

		return null;
	}

	/**
	 * returns true if the given String ends with any String from the given list
	 * of Strings
	 * 
	 * @param input
	 * @param list
	 * @return
	 */
	public static boolean endsWithAny(String input, String[] list) {
		if (list != null && input != null) {
			for (int loop = 0; loop < list.length; loop++) {
				logger.debug(" comparing: " + list[loop] + " with " + input);
				if (input.endsWith(list[loop]))
					return true;
			}
		}
		return false;
	}

	/**
	 * Returns true if the input string matches any from the given list.
	 * 
	 * @param input
	 * @param list
	 * @return
	 */
	public static boolean equalsAny(String input, String[] list) {
		if (list != null && input != null) {
			for (int loop = 0; loop < list.length; loop++) {
				if (StringUtils.equals(input, list[loop]))
					return true;
			}
		}
		return false;
	}

	/**
	 * Test a string on an array of regular expressions, return true if any
	 * match.
	 *
	 * @see java.util.regex.Pattern
	 * @param stringToTest
	 *            The string to test
	 * @param listOfRegExp
	 *            The array of Regular Expressions to test with
	 * @return if any of the regular epxressions are true on the string, return
	 *         true.
	 */
	public static boolean regExpAny(String stringToTest, String[] listOfRegExp) {
		if (listOfRegExp != null && stringToTest != null) {
			for (int loop = 0; loop < listOfRegExp.length; loop++) {
				logger.debug("MoreStringUtils.regExpAny:comparing: "
						+ listOfRegExp[loop] + " with " + stringToTest);
				if (stringToTest.matches(listOfRegExp[loop])) {
					logger.debug("MoreStringUtils.regExpAny:pass on "
							+ listOfRegExp[loop]);
					logger.debug("MoreStringUtils.regExpAny:Exit");
					return true;
				}
			}
		}
		logger.debug("MoreStringUtils.regExpAny:fail");
		return false;
	}

	public static void main(String[] s) {

	}

	public static String padString(String stringToPad, int len) {

		if (stringToPad == null || stringToPad.trim().length() == 0) {
			String val = "";
			for (int p = 0; p < len; p++)
				val = "0" + val;

			return val;
		}

		for (int i = stringToPad.length(); i < len; i++) {
			stringToPad = "0" + stringToPad;
		}

		return stringToPad;

	}

	public static String stripTrailingSpaces(String in) {
		return StringUtils.stripEnd(in, " ");
	}

	private static Log logger = LogFactory.getLog(CustomStringUtils.class);

	public static String join(String string1, String string2) {
		return StringUtils.join(new String[] { string1, string2 });
	}

	public static String joinToCreateFilePath(String[] strings) {
		StringBuffer path = new StringBuffer();
		for (int loop = 0; loop < strings.length; loop++) {
			String string = strings[loop];
			string = StringUtils.stripEnd(string, "/");
			string = StringUtils.stripStart(string, "/");
			string = StringUtils.stripEnd(string, "\\");
			string = StringUtils.stripStart(string, "\\");
			path.append(string).append('/');
		}
		return path.toString();
	}

	public static boolean trimmedEquals(String string1, String string2) {
		string1 = string1 != null ? string1.trim() : string1;
		string2 = string2 != null ? string2.trim() : string2;

		return StringUtils.equals(string1, string2);
	}

	public static String replaceNonPrintableAsciiCharactersAndTrim(
			String stringObj) {
		return replaceNonPrintableAsciiCharacters(stringObj.trim()).trim();
	}

	public static String replaceNonPrintableAsciiCharacters(String stringObj) {
		if (!StringUtils.isAsciiPrintable(stringObj)) {
			for (int i = 0; i < stringObj.length(); i++) {
				char ch = stringObj.charAt(i);
				if (!CharUtils.isAsciiPrintable(ch)) {
					stringObj = stringObj.replace(ch, ' ');
					if (StringUtils.isAsciiPrintable(stringObj)) {
						break;
					}
				}
			}
		}
		return stringObj;
	}

	/**
	 * StringUtils.equals() does this but we wanted 2 nulls not to be equal.
	 * 
	 * @param s1
	 * @param s2
	 * @return
	 */
	public static boolean nullSafeEquals(String s1, String s2) {
		if (s1 == null && s2 == null)
			return false;
		return StringUtils.equals(s1, s2);
	}
	
	/**
	     * Indicates whether the supplied stating is null or empty
	     * 
	     * @param s
	     * @return
	     */
	    public static boolean isEmpty(String s){
		return (s == null || s.isEmpty());
	    }
}
