/*
 * Copyright (c) 2020.
 *
 * All Rights are with Diwakar C
 */

package com.desktopapp.fx.control.autocompletetf;

import com.desktopapp.fx.control.AutoCompleteTextField;
import com.desktopapp.fx.control.TextFieldExt;
import javafx.application.Application;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.util.Callback;
import org.controlsfx.control.textfield.AutoCompletionBinding;
import org.controlsfx.control.textfield.TextFields;

import java.util.Arrays;
import java.util.Collection;


/**
 * Created by diwakar009 on 1/12/2018.
 */
public class AutoCompleteTextFieldExtTest extends Application{
    public AutoCompleteTextFieldExtTest() {
        super();
    }

    @Override
    public void init() throws Exception {
        super.init();
    }

    @Override
    public void stop() throws Exception {
        super.stop();
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        testAutoCompleteTextFieldLength(primaryStage);
    }


    public void testAutoCompleteTextFieldLength(Stage primaryStage){
        primaryStage.setTitle("Test...");

        /*AutoCompleteTextField textField = new AutoCompleteTextField();
        textField.getEntries().addAll(Arrays.asList("Diwakar Choudhury", "Girish Patnaik", "Rakesh Kumar","I Kiran"));*/

        TextFieldExt textField = new TextFieldExt();
        TextFields.bindAutoCompletion(textField, new Callback<AutoCompletionBinding.ISuggestionRequest, Collection<String>>() {
            @Override
            public Collection<String> call(AutoCompletionBinding.ISuggestionRequest iSuggestionRequest) {

                String text= textField.getDataValue();
                if("Diwakar".equals(text)){
                    return Arrays.asList("Diwakar Choudhury");
                }

                return Arrays.asList("Girish Patnaik", "Rakesh Kumar","I Kiran");
            }
        });

        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));

        grid.add(textField,1,1);


        Scene scene = new Scene(grid, 300, 275);
        primaryStage.setScene(scene);
        primaryStage.show();

    }

    public static void main(String[] args) {
        launch(args);
    }
}
