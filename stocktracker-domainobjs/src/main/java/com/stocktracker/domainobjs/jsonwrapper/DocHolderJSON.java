/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.domainobjs.jsonwrapper;





/*
public class DocHolderJSON extends BaseObjectJSON{

	private static final long serialVersionUID = -3665363292829488982L;

	public DocHolderJSON() {
		this(new HashMap<String,Object>(),null);
		getArgumentMap().put(ArgumentConstants.ARG_MASKDOCMENTS, "Y");
	}

	public DocHolderJSON(Map<String,Object> argumentMap,IDomainObject entity) {
		super(argumentMap,entity);

		if(entity != null){
			copyEntityToJSON();
		}else{
			this.entity = new DocHolderDO();
		}
	}

	public DocHolderJSON(Map<String,Object> map,IDomainObject entity,String documentStoreDirPath) {
		super(map,entity);

		this.documentStoreDirPath = documentStoreDirPath;

		if(entity != null){
			copyEntityToJSON();
		}else{
			this.entity = new DocHolderDO();
		}



	}

	@Expose
	private Long id;

	@Expose
	private String docCoowner;

	@Expose
	private String docKeywords;

	@Expose
	private String docName;

	@Expose
	private String docOwner;

	@Expose
	private String docType;

	@Expose
	private String docTypeOthers;

	@Expose
	private String docAccess;

	@Expose
	private String docPersist;


	@Expose
	private List<DocContentJSON> docContentJsons;

	private String documentStoreDirPath;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDocCoowner() {
		return docCoowner;
	}

	public void setDocCoowner(String docCoowner) {
		this.docCoowner = docCoowner;
	}

	public String getDocKeywords() {
		return docKeywords;
	}

	public void setDocKeywords(String docKeywords) {
		this.docKeywords = docKeywords;
	}

	public String getDocName() {
		return docName;
	}

	public void setDocName(String docName) {
		this.docName = docName;
	}

	public String getDocOwner() {
		return docOwner;
	}

	public void setDocOwner(String docOwner) {
		this.docOwner = docOwner;
	}

	public String getDocType() {
		return docType;
	}

	public void setDocType(String docType) {
		this.docType = docType;
	}

	public String getDocTypeOthers() {
		return docTypeOthers;
	}

	public void setDocTypeOthers(String docTypeOthers) {
		this.docTypeOthers = docTypeOthers;
	}

	public List<DocContentJSON> getDocContents() {
		return docContentJsons;
	}


	public String getDocPersist() {
	    return docPersist;
	}

	public void setDocPersist(String docPersist) {
	    this.docPersist = docPersist;
	}

	public String getDocAccess() {
	    return docAccess;
	}

	public void setDocAccess(String docAccess) {
	    this.docAccess = docAccess;
	}

	public void setDocContents(List<DocContentJSON> docContents) {
		this.docContentJsons = docContents;
	}

	public String getDocumentStoreDirPath() {
		return documentStoreDirPath;
	}

	public void setDocumentStoreDirPath(String documentStoreDirPath) {
		this.documentStoreDirPath = documentStoreDirPath;
	}

	@Override
	public void copyEntityToJSON() {

		super.copyEntityToJSON();

		DocHolderDO docHolder = (DocHolderDO)entity;
		setId(docHolder.getId());
		setDocName(docHolder.getDocName());
		setDocCoowner(docHolder.getDocCoowner());
		setDocKeywords(docHolder.getDocKeywords());
		setDocOwner(docHolder.getDocOwner());
		setDocType(docHolder.getDocType());
		setDocTypeOthers(docHolder.getDocTypeOthers());
		setDocAccess(docHolder.getDocAccess());
		setDocPersist(docHolder.getDocPersist() == null ? "Y" : docHolder.getDocPersist());

		docContentJsons = new ArrayList<DocContentJSON>();

		for(DocContentDO content : docHolder.getDocContents()){
			DocContentJSON contentJSON = new DocContentJSON(getArgumentMap(),content,getDocumentStoreDirPath(),getDocPersist());
			docContentJsons.add(contentJSON);
		}

	}

	@Override
	public void copyJSONToEntity() {

		super.copyJSONToEntity();

		DocHolderDO docHolder = (DocHolderDO)entity;

		//docHolder.setId(getId());
		docHolder.setDocName(getDocName());
		docHolder.setDocCoowner(getDocCoowner());
		docHolder.setDocKeywords(getDocKeywords());
		docHolder.setDocOwner(getDocOwner());
		docHolder.setDocType(getDocType());
		docHolder.setDocTypeOthers(getDocTypeOthers());
		docHolder.setDocPersist(getDocPersist() == null ? "Y" :  getDocPersist() );
		docHolder.setDocAccess(getDocAccess() == null ? "*" :  getDocAccess() );
		for(DocContentJSON contentJson : docContentJsons){
			contentJson.setDocumentStoreDirPath(getDocumentStoreDirPath());
			contentJson.copyJSONToEntity();
			DocContentDO content = (DocContentDO)contentJson.getEntity();
			content.setDocHolder(docHolder);
			docHolder.addDocContent(content);
		}

	}


}*/
