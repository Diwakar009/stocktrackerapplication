package com.desktopapp.fx.control;

import com.desktopapp.fx.control.listeners.ValueChangeListener;
import javafx.application.Application;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;


/**
 * Created by diwakar009 on 1/12/2018.
 */
public class TextFieldExtTest extends Application{
    public TextFieldExtTest() {
        super();
    }

    @Override
    public void init() throws Exception {
        super.init();
    }

    @Override
    public void stop() throws Exception {
        super.stop();
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        //testTextFieldLength(primaryStage);
        //testTextFieldObservableList(primaryStage);
        //testTextFieldFocusOutListener(primaryStage);
        //testTextFieldInvalidateListener(primaryStage);
        //testValueChangeListener(primaryStage);
        testMandaoryError(primaryStage);
    }

    public void testValueChangeListener(Stage primaryStage){

        primaryStage.setTitle("Test...");
        TextFieldExt textField = new TextFieldExt(10);
        TextFieldExt textField1 = new TextFieldExt(10);
        TextFieldExt textField2 = new TextFieldExt(10);

        textField.addValueChangeListener(() -> {
                textField1.setDataValue(textField.getDataValue());
                textField2.setDataValue(textField.getDataValue());
        });

        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));

        grid.add(textField,1,1);
        grid.add(textField1,1,2);
        grid.add(textField2,1,3);

        Scene scene = new Scene(grid, 300, 275);
        primaryStage.setScene(scene);
        primaryStage.show();

    }

    public void testMandaoryError(Stage primaryStage){
        primaryStage.setTitle("Test...");
        Button btn = new Button();
        TextFieldExt textField = new TextFieldExt(10,true);
        TextFieldExt textField1 = new TextFieldExt(10);
        textField1.setMandatory(true);

        btn.setText("Say 'Hello World'");
        btn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                textField.setDataValue("Hello Test");

                if(textField1.validateInput()){

                }

            }
        });

        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));

        grid.add(textField,1,1);
        grid.add(textField1,1,2);

        grid.add(btn,1,3);


        Scene scene = new Scene(grid, 300, 275);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public void testTextFieldInvalidateListener(Stage primaryStage){
        primaryStage.setTitle("Test...");
        Button btn = new Button();
        TextFieldExt textField = new TextFieldExt(10);
        TextFieldExt textField1 = new TextFieldExt(10);
        btn.setText("Say 'Hello World'");
        btn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                textField.setDataValue("Hello Test");
            }
        });

        textField.textProperty().addListener(
                new InvalidationListener() {
                    @Override
                    public void invalidated(Observable observable) {
                        System.out.println("Invalidate Listener Called..");
                    }
                }
        );

        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));

        grid.add(textField,1,1);
        grid.add(btn,1,2);


        Scene scene = new Scene(grid, 300, 275);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public void testTextFieldFocusOutListener(Stage primaryStage){

        primaryStage.setTitle("Test...");
        Button btn = new Button();
        TextFieldExt textField = new TextFieldExt(10);
        btn.setText("Say 'Hello World'");
        btn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                textField.setDataValue("Hello Test");
            }
        });

        textField.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(newValue){
                System.out.println("Focused In");
            }else{
                System.out.println("Focused Out");
            }
        });

        textField.textProperty().addListener((observable, oldValue, newValue) -> {
            if("".equals(newValue) ){
                System.out.print("Text is blank...");
            }
        });

        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));

        grid.add(textField,1,1);
        grid.add(btn,1,2);


        Scene scene = new Scene(grid, 300, 275);
        primaryStage.setScene(scene);
        primaryStage.show();

    }

    public void testTextFieldLength(Stage primaryStage){
        primaryStage.setTitle("Test...");
        Button btn = new Button();
        TextFieldExt textField = new TextFieldExt(10);
        btn.setText("Say 'Hello World'");
        btn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                textField.setDataValue("Hello Test");
            }
        });

        textField.textProperty().addListener((observable, oldValue, newValue) -> {
            System.out.println("textfield changed from " + oldValue + " to " + newValue);
        });

        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));

        grid.add(textField,1,1);
        grid.add(btn,1,2);


        Scene scene = new Scene(grid, 300, 275);
        primaryStage.setScene(scene);
        primaryStage.show();

    }


    public void testTextFieldObservableList(Stage primaryStage){
        primaryStage.setTitle("Test...");
        TextFieldExt textField = new TextFieldExt(10);
        TextFieldExt textField1 = new TextFieldExt(10);
        TextFieldExt textField3 = new TextFieldExt(10);

        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));

        grid.add(textField,1,1);
        grid.add(textField1,1,2);


         ObservableList<TextField> oList =
                FXCollections.observableArrayList(tf -> new Observable[]{textField.textProperty()});

        oList.addListener((ListChangeListener.Change<? extends TextField> c) -> {
            while (c.next()) {
                if (c.wasUpdated()) {
                    for (int i = c.getFrom(); i < c.getTo(); ++i) {
                        System.out.println("Updated index: " + i + ", new value: " + c.getList().get(i).getText());
                    }
                }
            }
        });

        oList.add(textField1);
        oList.add(textField3);

        Scene scene = new Scene(grid, 300, 275);
        primaryStage.setScene(scene);
        primaryStage.show();

    }

    public static void main(String[] args) {
        launch(args);
    }
}
