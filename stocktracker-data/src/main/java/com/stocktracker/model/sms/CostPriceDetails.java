/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.model.sms;

import com.stocktracker.framework.IEntity;
import com.stocktracker.model.BaseEntity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * The persistent class for the TBL_COSTPRICE_DTLS database table.
 * 
 */
@Entity
@Table(name = "TBL_COSTPRICE_DTLS")
@AttributeOverride(name = "id", column = @Column(name = "ID", nullable = false,
		columnDefinition = "BIGINT UNSIGNED"))
public class CostPriceDetails extends BaseEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "ID", nullable = false, columnDefinition = "BIGINT UNSIGNED")
	private Long id;

	@Column(name = "APPLIED_RATE", nullable = true, columnDefinition = "DECIMAL SIGNED")
	private Double appliedRate;

	@Column(name = "DERIVED_AMOUNT", nullable = true, columnDefinition = "DECIMAL SIGNED")
	private Double derivedAmount;

	@Column(name = "ACTUAL_AMOUNT", nullable = true, columnDefinition = "DECIMAL SIGNED")
	private Double actualAmount;

	@Column(name = "PLUS_OR_MINUS_ON_TOTAL", length = 1)
	private Character plusOrMinusOnTotal;

	@Column(name="MORE_DETAILS", length = 65535)
	private String moreDetails;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getAppliedRate() {
		return appliedRate;
	}

	public void setAppliedRate(Double appliedRate) {
		this.appliedRate = appliedRate;
	}

	public Double getDerivedAmount() {
		return derivedAmount;
	}

	public void setDerivedAmount(Double derivedAmount) {
		this.derivedAmount = derivedAmount;
	}

	public Double getActualAmount() {
		return actualAmount;
	}

	public void setActualAmount(Double actualAmount) {
		this.actualAmount = actualAmount;
	}

	public Character getPlusOrMinusOnTotal() {
		return plusOrMinusOnTotal;
	}

	public void setPlusOrMinusOnTotal(Character plusOrMinusOnTotal) {
		this.plusOrMinusOnTotal = plusOrMinusOnTotal;
	}

	public String getMoreDetails() {
		return moreDetails;
	}

	public void setMoreDetails(String moreDetails) {
		this.moreDetails = moreDetails;
	}

	@Override
	public Serializable getEntityPK() {
		return id;
	}

	@Override
	public void copyEntity(IEntity entity) {
		CostPriceDetails source = (CostPriceDetails)entity;
		this.setCreatedBy(source.getCreatedBy());
		this.setUpdatedBy(source.getUpdatedBy());
		this.setCreatedAt(source.getCreatedAt());
		this.setUpdatedAt(source.getUpdatedAt());

		this.setId(source.getId());
		this.setAppliedRate(source.getAppliedRate());
		this.setDerivedAmount(source.getDerivedAmount());
		this.setActualAmount(source.getActualAmount());
		this.setPlusOrMinusOnTotal(source.getPlusOrMinusOnTotal());
		this.setMoreDetails(source.getMoreDetails());
	}
}