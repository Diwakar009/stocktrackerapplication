/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.domainobjs.json;

import com.stocktracker.model.BaseEntity;
import com.stocktracker.model.DocHolder;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;


/**
 * The persistent class for the doc_holder database table.
 * 
 */
@JsonIgnoreProperties(ignoreUnknown=true)
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class DocHolderDO extends BaseDomainObject implements Serializable {
	private static final long serialVersionUID = 1L;

	public static String CLASS_NAME = DocHolderDO.class.getName();

	@JsonProperty("id")
    private Long id;

	@JsonProperty("docCoowner")
	private String docCoowner;

	@JsonProperty("docKeywords")
	private String docKeywords;

	@JsonProperty("docName")
	private String docName;

	@JsonProperty("docOwner")
	private String docOwner;

	@JsonProperty("docType")
	private String docType;

	@JsonProperty("docTypeOthers")
	private String docTypeOthers;

	@JsonProperty("docAccess")
	private String docAccess;

	@JsonProperty("docPersist")
	private String docPersist;

	/*
	@JsonProperty("documentContents")
	private List<DocContentDO> documentContents;
	*/


	//bi-directional many-to-one association to DocContent
	//private List<DocContentDO> docContents;
	public DocHolderDO() {
		//docContents = new ArrayList<DocContentDO>();
	}
	public DocHolderDO(DocHolder holder) {
		copyEntity2DomainObject(holder);
	}

	public DocHolderDO(boolean fullObject) {
		//docContents = new ArrayList<DocContentDO>();

	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDocCoowner() {
		return this.docCoowner;
	}

	public void setDocCoowner(String docCoowner) {
		this.docCoowner = docCoowner;
	}

	public String getDocKeywords() {
		return this.docKeywords;
	}

	public void setDocKeywords(String docKeywords) {
		this.docKeywords = docKeywords;
	}

	public String getDocName() {
		return this.docName;
	}

	public void setDocName(String docName) {
		this.docName = docName;
	}

	public String getDocOwner() {
		return this.docOwner;
	}

	public void setDocOwner(String docOwner) {
		this.docOwner = docOwner;
	}

	public String getDocType() {
		return this.docType;
	}

	public void setDocType(String docType) {
		this.docType = docType;
	}

	public String getDocTypeOthers() {
		return this.docTypeOthers;
	}

	public void setDocTypeOthers(String docTypeOthers) {
		this.docTypeOthers = docTypeOthers;
	}


	@Override
	@JsonIgnore
	public Object getDomainObjectKey() {
		return getId();
	}

	public String getDocAccess() {
	    return docAccess;
	}

	public void setDocAccess(String docAccess) {
	    this.docAccess = docAccess;
	}
	
	

	public String getDocPersist() {
	    return docPersist;
	}

	public void setDocPersist(String docPersist) {
	    this.docPersist = docPersist;
	}

	@Override
	public String toString() {
	    return "DocHolderDO [id=" + id + ", docCoowner=" + docCoowner
		    + ", docKeywords=" + docKeywords + ", docName=" + docName
		    + ", docOwner=" + docOwner + ", docType=" + docType
		    + ", docTypeOthers=" + docTypeOthers + ", docAccess="
		    + docAccess + ", docPersist=" + docPersist
		    + "]";
	}

	@Override
	public DocHolder copyDomainObject2Entity(BaseEntity entity) {

		DocHolder holder = (DocHolder)entity;
		holder.setCreatedBy(this.getCreatedBy());
		holder.setUpdatedBy(this.getUpdatedBy());
		holder.setCreatedAt(this.getCreatedAt());
		holder.setUpdatedAt(this.getUpdatedAt());

		holder.setId(this.getId());
		holder.setDocAccess(this.getDocAccess());
		holder.setDocCoowner(this.getDocCoowner());
		holder.setDocKeywords(this.getDocKeywords());
		holder.setDocName(this.getDocName());
		holder.setDocType(this.getDocType());
		holder.setDocTypeOthers(this.getDocTypeOthers());
		holder.setDocPersist(this.getDocPersist());
		holder.setDocOwner(this.getDocOwner());

		return holder;

	}

	@Override
	public void copyEntity2DomainObject(BaseEntity entity) {
		DocHolder holder = (DocHolder)entity;

		setCreatedBy(holder.getCreatedBy());
		setUpdatedBy(holder.getUpdatedBy());
		setCreatedAt(holder.getCreatedAt());
		setUpdatedAt(holder.getUpdatedAt());

		setId(holder.getId());
		setDocAccess(holder.getDocAccess());
		setDocCoowner(holder.getDocCoowner());
		setDocKeywords(holder.getDocKeywords());
		setDocName(holder.getDocName());
		setDocType(holder.getDocType());
		setDocTypeOthers(holder.getDocTypeOthers());
		setDocPersist(holder.getDocPersist());
		setDocOwner(holder.getDocOwner());

		/*
		if(holder.getDocContents() != null && holder.getDocContents().size() > 0){
			documentContents = new ArrayList<DocContentDO>();
			for(DocContent content : holder.getDocContents()) {
				DocContentDO contentDO = new DocContentDO();
				contentDO.copyEntity2DomainObject(content);
			}
		}
		*/
	}
}