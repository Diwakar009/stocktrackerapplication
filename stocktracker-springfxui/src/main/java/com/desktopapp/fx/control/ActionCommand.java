/*
 * Copyright (C) 2017 Cem Ikta
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 *
 */

package com.desktopapp.fx.control;


import com.desktopapp.fx.DesktopFxApplication;
import com.desktopapp.fx.commands.ICommand;
import com.desktopapp.fx.control.fonticon.FontIcon;
import com.desktopapp.fx.control.fonticon.FontIconFactory;
import com.desktopapp.fx.control.fonticon.FontIconSize;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.input.KeyCodeCombination;

/**
 * Action  command
 *
 * @author Diwakar Choudhury
 */
public class ActionCommand extends Button {

    private ICommand command;

    private KeyCodeCombination keyCodeCombination;

    private boolean isChildAccelarator = true;

    public ActionCommand(String text, Node graphic) {
        super(text, graphic);
    }

    public ActionCommand(String text, Node graphic, ICommand command) {
        super(text, graphic);
        if(command != null) {
            this.command = command;
            setOnActionEvent(event -> {this.command.executeCommand();});
        }
    }

    public ActionCommand(ICommand command, String text, FontIcon fontIcon) {
        this(text, fontIcon, ContentDisplay.TOP);
        if(command != null) {
            this.command = command;
            setOnActionEvent(event -> {this.command.executeCommand();});
        }
    }

    public ActionCommand(ICommand command, String text, FontIcon fontIcon, KeyCodeCombination keyCodeCombination) {
        this(text, fontIcon, ContentDisplay.TOP);
        if(command != null) {
            this.command = command;
            setOnActionEvent(event -> {this.command.executeCommand();});
        }
        this.keyCodeCombination = keyCodeCombination;
     }

    public ActionCommand(String text, FontIcon fontIcon) {
        this(text, fontIcon, ContentDisplay.TOP);
    }

    public ActionCommand(String text, FontIcon fontIcon, ContentDisplay contentDisplay) {
        super(text, FontIconFactory.createIcon(fontIcon, FontIconSize.SM));
        setContentDisplay(contentDisplay);
    }

    public ActionCommand(String text, KeyCodeCombination keyCodeCombination){
        super(text);
        this.keyCodeCombination = keyCodeCombination;
    }

    public ActionCommand(String text, KeyCodeCombination keyCodeCombination, EventHandler<ActionEvent> action){
        super(text);
        this.keyCodeCombination = keyCodeCombination;
        setOnActionEvent(action);
    }

    public ActionCommand(String text, EventHandler<ActionEvent> action){
        super(text);
        setOnActionEvent(action);
    }

    public ActionCommand(String text){
        super(text);
    }

    public ActionCommand(ICommand command) {
        this.command = command;
    }

    public ICommand getCommand() {
        return command;
    }

    public void setCommand(ICommand command) {
        this.command = command;
    }

    public KeyCodeCombination getKeyCodeCombination() {
        return keyCodeCombination;
    }

    public void setKeyCodeCombination(KeyCodeCombination keyCodeCombination) {
        this.keyCodeCombination = keyCodeCombination;
    }


    public boolean isChildAccelarator() {
        return isChildAccelarator;
    }

    public void setChildAccelarator(boolean childAccelarator) {
        isChildAccelarator = childAccelarator;
    }

    /**
     * Set on action event, wrap the action with clearing of application info messages if any.
     *
     * @param action
     */
    public void setOnActionEvent(EventHandler<ActionEvent> action){

        EventHandler<ActionEvent> overAllAction = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                DesktopFxApplication.clearApplicationInfoMessages();
                action.handle(event);
            }
        };

        setOnAction(overAllAction);
    }

}
