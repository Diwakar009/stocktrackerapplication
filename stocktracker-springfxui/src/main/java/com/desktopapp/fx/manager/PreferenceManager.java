package com.desktopapp.fx.manager;

import com.desktopapp.fx.controller.PreferencesController;
import com.stocktracker.domainobjs.json.PreferencesDO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by diwakar009 on 13/12/2018.
 */
@Component
public class PreferenceManager {

    @Autowired
    private PreferencesController preferencesController;

    public PreferenceManager() {
    }

    public PreferencesDO updatePreferences(PreferencesDO updatedPreferences){
        PreferencesDO preferences = (PreferencesDO)
						preferencesController.update(updatedPreferences);
        return preferences;
    }

    public PreferencesDO find(Long id){
        PreferencesDO preferences = (PreferencesDO)preferencesController.find(id);
        return preferences;
    }

    public PreferencesController getPreferencesController() {
        return preferencesController;
    }

}
