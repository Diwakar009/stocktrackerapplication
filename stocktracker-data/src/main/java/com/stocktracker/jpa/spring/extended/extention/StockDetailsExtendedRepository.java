/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.jpa.spring.extended.extention;

import com.stocktracker.jpa.spring.extended.bean.MandiReconcilationData;
import com.stocktracker.jpa.spring.extended.bean.StockDetailsSummary;
import com.stocktracker.model.sms.StockDetails;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Date;
import java.util.List;


/**
 * Created by diwakar009 on 22/10/2019.
 */
public interface StockDetailsExtendedRepository {
    public Page<StockDetails> findAllStockDetailsByFilter(String industryCodeFilter, String stockCodeFilter,
                                                          String stockVarietyCodeFilter,String transTypeFilter,
                                                          String rstNumberFilter,String vehicleNumberFilter,
                                                          Date startDateFilter, Date endDateFilter,String partyName,String onBehalfFilter,String lampNameFilter,String mandiNameFilter,String revisitFlagFilter, Pageable pageRequest);

    public Page<StockDetailsSummary> findStockDetailsSummaryByFilter(String industryCodeFilter, String stockCodeFilter,
                                                                 String stockVarietyCodeFilter, String transTypeFilter,
                                                                 String rstNumberFilter, String vehicleNumberFilter,
                                                                 Date startDateFilter, Date endDateFilter, String partyName,String onBehalfFilter,String lampNameFilter,String mandiNameFilter,String revisitFlagFilter, Pageable pageRequest);


    public List<MandiReconcilationData> getMandiReconciliationReportData(String industryCode, String lampNameCode, String mandiNameCode);


}
