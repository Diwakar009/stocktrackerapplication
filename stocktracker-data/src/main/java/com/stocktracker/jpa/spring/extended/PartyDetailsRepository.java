/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.jpa.spring.extended;

import com.stocktracker.jpa.spring.extended.custom.ExtendedRepository;
import com.stocktracker.jpa.spring.extended.extention.PartyDetailsExtendedRepository;
import com.stocktracker.jpa.spring.extended.extention.StockDetailsExtendedRepository;
import com.stocktracker.model.sms.PartyDetails;
import com.stocktracker.model.sms.StockDetails;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PartyDetailsRepository extends ExtendedRepository<PartyDetails, Long> , PartyDetailsExtendedRepository{



}
