/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.model.sms;

import com.stocktracker.framework.ICompositeEntityPK;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * The primary key class for the TBL_STOCK database table.
 * 
 */
@Embeddable
public class StockPK implements Serializable,ICompositeEntityPK {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	public StockPK(String industryCode, String stockCode, String stockVarietyCode) {
		this.industryCode = industryCode;
		this.stockCode = stockCode;
		this.stockVarietyCode = stockVarietyCode;
	}

	public StockPK() {
	}

	@Column(name="INDUSTRY_CODE", length = 5)
	private String industryCode;

	@Column(name="STOCK_CODE", length = 5)
	private String stockCode;

	@Column(name="STOCK_VARIETIES_CODE", length = 5)
	private String stockVarietyCode;

	public String getIndustryCode() {
		return industryCode;
	}

	public void setIndustryCode(String industryCode) {
		this.industryCode = industryCode;
	}

	public String getStockCode() {
		return stockCode;
	}

	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}

	public String getStockVarietyCode() {
		return stockVarietyCode;
	}

	public void setStockVarietyCode(String stockVarietyCode) {
		this.stockVarietyCode = stockVarietyCode;
	}

	@Override
	public String toString() {
		return "StockPK{" +
				"industryCode='" + industryCode + '\'' +
				", stockCode='" + stockCode + '\'' +
				", stockVarietyCode='" + stockVarietyCode + '\'' +
				'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		StockPK stockPK = (StockPK) o;

		if (industryCode != null ? !industryCode.equals(stockPK.industryCode) : stockPK.industryCode != null)
			return false;
		if (stockCode != null ? !stockCode.equals(stockPK.stockCode) : stockPK.stockCode != null) return false;
		return stockVarietyCode != null ? stockVarietyCode.equals(stockPK.stockVarietyCode) : stockPK.stockVarietyCode == null;
	}

	@Override
	public int hashCode() {
		int result = industryCode != null ? industryCode.hashCode() : 0;
		result = 31 * result + (stockCode != null ? stockCode.hashCode() : 0);
		result = 31 * result + (stockVarietyCode != null ? stockVarietyCode.hashCode() : 0);
		return result;
	}
}