package com.stocktracker.ws.web.api;

import com.stocktracker.domainobjs.json.DocHolderDO;
import com.stocktracker.domainobjs.json.DomainObjectList;
import com.stocktracker.domainobjs.json.TableRowMapDO;
import com.stocktracker.util.DateTimeUtil;
import com.stocktracker.util.JSONUtil;
import com.stocktracker.ws.BaseUnitTest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;

/**
 * Unit tests for the GreetingController using Spring MVC Mocks.
 * 
 * These tests utilize the Spring MVC Mock objects to simulate sending actual
 * HTTP requests to the Controller component. This test ensures that the
 * RequestMappings are configured correctly. Also, these tests ensure that the
 * request and response bodies are serialized as expected.
 * 
 * @author Matt Warman
 */
@Transactional
public class DocumentHolderControllerTest extends BaseUnitTest {


    @Test
    public void testGet() throws Exception{

        String uri = "/api/documentHolder/get/";

        DocHolderDO docHolderDo = new DocHolderDO();
        docHolderDo.setId(new Long(2));


        String jsonString = JSONUtil.serializeToJSON(docHolderDo);

        MvcResult result = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON).content(jsonString)).andReturn();

        String content = result.getResponse().getContentAsString();
        int status = result.getResponse().getStatus();
        Gson gson = new GsonBuilder().setDateFormat(DateTimeUtil.UTC_FORMAT).setPrettyPrinting().create();
        JsonParser jp = new JsonParser();
        JsonElement je = jp.parse(content);
        String prettyJsonString = gson.toJson(je);

        //Assert.assertEquals("failure - expected HTTP status", 200, status);

        displayMessage(prettyJsonString);

        DocHolderDO sd =  (DocHolderDO)JSONUtil.deserializeToObject(content,DocHolderDO.class);

        DocHolderDO stdo = gson.fromJson(je,DocHolderDO.class);

      /*  Assert.assertEquals("Document Name :", "Varsha Documents", stdo.getDocName());
        Assert.assertEquals("Document Type :", "PE", stdo.getDocType());
        Assert.assertEquals("Document Owner :", "Chetana", stdo.getDocOwner());*/
    }

    //@Test
    public void testUpdate() throws  Exception{


        String uri = "/api/documentHolder/get/";

        DocHolderDO docHolderDo = new DocHolderDO();
        docHolderDo.setId(new Long(2));


        String jsonString = JSONUtil.serializeToJSON(docHolderDo);

        MvcResult result = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON).content(jsonString)).andReturn();

        String content = result.getResponse().getContentAsString();
        int status = result.getResponse().getStatus();
        Gson gson = new GsonBuilder().setDateFormat(DateTimeUtil.UTC_FORMAT).setPrettyPrinting().create();
        JsonParser jp = new JsonParser();
        JsonElement je = jp.parse(content);
        String prettyJsonString = gson.toJson(je);

        //Assert.assertEquals("failure - expected HTTP status", 200, status);

        displayMessage(prettyJsonString);

        DocHolderDO sd =  (DocHolderDO)JSONUtil.deserializeToObject(content,DocHolderDO.class);

        DocHolderDO stdo = gson.fromJson(je,DocHolderDO.class);

        stdo.setDocName("Chetana Documents");

        uri = "/api/documentHolder/save/";

        jsonString = JSONUtil.serializeToJSON(stdo);

        result = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON).content(jsonString)).andReturn();

        content = result.getResponse().getContentAsString();
        status = result.getResponse().getStatus();
        gson = new GsonBuilder().setDateFormat(DateTimeUtil.UTC_FORMAT).setPrettyPrinting().create();
        jp = new JsonParser();
        je = jp.parse(content);
        prettyJsonString = gson.toJson(je);

       /* Assert.assertEquals("failure - expected HTTP status", 201, status);*/

        displayMessage(content);

    }



    @Test
    public void testSave() throws Exception{

        String uri = "/api/documentHolder/save/";

        DocHolderDO holderDO  = new DocHolderDO();
        holderDO.setDocName("Sample Document Name");
        holderDO.setDocAccess("*");
        holderDO.setDocCoowner("Sample Coowner");
        holderDO.setDocOwner("Sample Owner");
        holderDO.setDocType("PE");
        holderDO.setDocPersist("Y");
        holderDO.setDocKeywords("Sample Keyword");

        String jsonString = JSONUtil.serializeToJSON(holderDO);

        MvcResult result = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON).content(jsonString)).andReturn();

        String content = result.getResponse().getContentAsString();
        int status = result.getResponse().getStatus();
        Gson gson = new GsonBuilder().setDateFormat(DateTimeUtil.UTC_FORMAT).setPrettyPrinting().create();
        JsonParser jp = new JsonParser();
        JsonElement je = jp.parse(content);
        String prettyJsonString = gson.toJson(je);

        Assert.assertEquals("failure - expected HTTP status", 201, status);

        displayMessage(content);

        DocHolderDO sd =  (DocHolderDO)JSONUtil.deserializeToObject(content,DocHolderDO.class);

        DocHolderDO stdo = gson.fromJson(je,DocHolderDO.class);


        uri = "/api/documentHolder/get/";


        jsonString = JSONUtil.serializeToJSON(stdo);

        result = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON).content(jsonString)).andReturn();

        content = result.getResponse().getContentAsString();
        status = result.getResponse().getStatus();
        gson = new GsonBuilder().setPrettyPrinting().create();
        jp = new JsonParser();
        je = jp.parse(content);
        prettyJsonString = gson.toJson(je);

      /*  Assert.assertEquals("failure - expected HTTP status", 200, status);*/

        displayMessage(content);



    }

    @Test
    public void testDelete() throws Exception{
        String uri = "/api/documentHolder/delete/";

        DocHolderDO docHolderDo = new DocHolderDO();
        docHolderDo.setId(new Long(2));

        String jsonString = JSONUtil.serializeToJSON(docHolderDo);

        MvcResult result = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON).content(jsonString)).andReturn();

        String content = result.getResponse().getContentAsString();
        int status = result.getResponse().getStatus();
        Gson gson = new GsonBuilder().setDateFormat(DateTimeUtil.UTC_FORMAT).setPrettyPrinting().create();
        JsonParser jp = new JsonParser();
        JsonElement je = jp.parse(content);
        String prettyJsonString = gson.toJson(je);

      /*  Assert.assertEquals("failure - expected HTTP status", 200, status);*/

        displayMessage(content);

    }

    @Test
    public void testDeleteList() throws Exception{
        String uri = "/api/documentHolder/deleteList/";

        DocHolderDO docHolderDo1 = new DocHolderDO();
        docHolderDo1.setId(new Long(2));

        DocHolderDO docHolderDo2 = new DocHolderDO();
        docHolderDo2.setId(new Long(10000));


        List<DocHolderDO> docHolderDOArrayList = new ArrayList<DocHolderDO>();
        docHolderDOArrayList.add(docHolderDo1);
        docHolderDOArrayList.add(docHolderDo2);


        DomainObjectList objectList = new DomainObjectList();
        objectList.setList(docHolderDOArrayList);

        //staticCodeDecodeDO.setCodeDesc("Sample Access Level");

        String jsonString = JSONUtil.serializeToJSON(objectList);

        MvcResult result = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON).content(jsonString)).andReturn();

        String content = result.getResponse().getContentAsString();
        int status = result.getResponse().getStatus();
        Gson gson = new GsonBuilder().setDateFormat(DateTimeUtil.UTC_FORMAT).setPrettyPrinting().create();
        JsonParser jp = new JsonParser();
        JsonElement je = jp.parse(content);
        String prettyJsonString = gson.toJson(je);

        /*Assert.assertEquals("failure - expected HTTP status", 200, status);*/

        displayMessage(content);

    }
    @Test
    public void testListAll() throws Exception{

        String uri = "/api/documentHolder/listAll/";

        TableRowMapDO<String, String> rowMap = new TableRowMapDO<String, String>();
        rowMap.setSize("5");
        rowMap.setPage("1");

        String jsonString = JSONUtil.serializeToJSON(rowMap);

        MvcResult result = mvc.perform(MockMvcRequestBuilders.get(uri)
                .accept(MediaType.APPLICATION_JSON)).andReturn();

        String content = result.getResponse().getContentAsString();
        int status = result.getResponse().getStatus();
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        JsonParser jp = new JsonParser();
        JsonElement je = jp.parse(content);
        String prettyJsonString = gson.toJson(je);

        /*Assert.assertEquals("failure - expected HTTP status", 200, status);*/

        displayMessage(content);

    }
/*

    @Test
    public void testListAllTableRowObjectConstruction() throws Exception{

        String uri = "/api/appUser/listAll/";

        TableRowMapDO<String, String> rowMap = new TableRowMapDO<String, String>();
        rowMap.setSize("2");
        rowMap.setPage("1");

        String jsonString = JSONUtil.serializeToJSON(rowMap);

        MvcResult result = mvc.perform(MockMvcRequestBuilders.get(uri)
                .accept(MediaType.APPLICATION_JSON)).andReturn();

        String content = result.getResponse().getContentAsString();
        int status = result.getResponse().getStatus();
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        JsonParser jp = new JsonParser();
        JsonElement je = jp.parse(content);
        String prettyJsonString = gson.toJson(je);

        Assert.assertEquals("failure - expected HTTP status", 200, status);

        displayMessage(content);
        TableDOPage page =  (TableDOPage)JSONUtil.deserializeToObject(content,TableDOPage.class);
        List<TableRowMapDO<String,String>> rows = page.getList();

        for(TableRowMapDO<String,String> row : rows){

            String clazzName = row.getClazzName();
            Class<?> clazz = Class.forName(clazzName);
            Constructor<?> ctor = clazz.getConstructor();
            Object object = ctor.newInstance(new Object[] {});

            AppUserDO userDO = new AppUserDO();
            userDO.setFirstName(row.getValue("firstName"));
            userDO.setLastName(row.getValue("lastName"));
            userDO.setShortName(row.getValue("shortName"));
            userDO.setMobile(row.getValue("mobile"));
            userDO.setEmail(row.getValue("email"));
            userDO.setActive(new Boolean(row.getValue("active")));
            userDO.setId(new Long(row.getValue("id")));

            displayMessage(userDO.getId().toString());
        }
    }
    */

    @Test
    public void testListWithFilter() throws Exception {
        String uri = "/api/documentHolder/listWithFilter/";

        TableRowMapDO<String, String> rowMap = new TableRowMapDO<String, String>();
        rowMap.setSize("5");
        rowMap.setPage("1");

        rowMap.addKeyValue("docOwner", "%di%");
        rowMap.addKeyValue("docCoOwner", "%di%");
        rowMap.addKeyValue("docName", "%di%");
        rowMap.addKeyValue("docKeywords", "%di%");

        String jsonString = JSONUtil.serializeToJSON(rowMap);

        MvcResult result = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON).content(jsonString)).andReturn();

        String content = result.getResponse().getContentAsString();
        int status = result.getResponse().getStatus();
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        JsonParser jp = new JsonParser();
        JsonElement je = jp.parse(content);
        String prettyJsonString = gson.toJson(je);



        /*Assert.assertEquals("failure - expected HTTP status", 200, status);*/

        displayMessage(prettyJsonString);
    }


}
