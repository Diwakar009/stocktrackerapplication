package com.desktopapp.service;


import com.desktopapp.jsonobject.DocHolderJSON;
import com.desktopapp.jsonobject.DocHolderListJSON;
import com.desktopapp.objectfactory.ObjectFactory;
import com.stocktracker.component.constants.DocStoreConstants;
import com.stocktracker.framework.IEntity;
import com.stocktracker.jpa.spring.extended.DocHolderRepository;
import com.stocktracker.model.BaseEntity;
import com.stocktracker.model.DocHolder;
import com.stocktracker.util.CommonFileUtil;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import com.stocktracker.domainobjs.IDomainObject;
import com.stocktracker.domainobjs.json.DocHolderDO;
import com.stocktracker.domainobjs.json.TableRowMapDO;
import com.stocktracker.domainobjs.jsonwrapper.ArgumentConstants;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Document Holder Service
 *
 * @author Ratnala Diwakar Choudhury
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS,readOnly = false)
public class DocumentHolderService extends AbstractService<DocHolderDO> {

    final static Logger logger = Logger.getLogger(DocumentHolderService.class);

    @Resource
    protected DocHolderRepository docHolderRepository;


    public DocumentHolderService() {
        super(DocHolderDO.class);
        //repository = new DocHolderStandaloneRepository(DocHolder.class);
    }

    /**
     * Remove entity.
     *
     * @param docHolderDO entity model
     */
    public void remove(DocHolderDO docHolderDO) {
        ((DocHolderRepository) docHolderRepository).delete((DocHolder) returnEntity(docHolderDO));
    }


    /**
     * Remove entities.
     *
     * @param entities entity model
     */
    public void remove(List<DocHolderDO> entities) {
        ((DocHolderRepository) docHolderRepository).deleteAll((List<DocHolder>) returnEntities(entities));
    }

    /**
     * Download all the documents
     *
     * @param exportDirPath
     * @param ids
     * @throws Exception
     */
    public void downloadDocHolders(String exportDirPath, ArrayList<DocHolderDO> ids) throws Exception {

        logger.debug("downloadDocHolders started ");

        Map<String, Object> argumentMap = new HashMap<String, Object>();
        argumentMap.put(ArgumentConstants.ARG_MASKDOCMENTS, "N");

        try {
            DocHolderListJSON backupDocHolder = new DocHolderListJSON();

            if (ids == null || ids.size() == 0) {
                ids = (ArrayList<DocHolderDO>)returnDomainObjects(this.getRepository().findAll());
            }

            ids.forEach(docHolderDO -> {
                DocHolder docHolder = (DocHolder) getRepository().getOne(docHolderDO.getId());
                DocHolderJSON holderJSON = new DocHolderJSON(argumentMap, docHolder, exportDirPath);
                backupDocHolder.getDocHolderJSONList().add(holderJSON);
            });

        } catch (Exception anyException) {
            anyException.printStackTrace();
            logger.error("Exception Occoured " + anyException.getMessage());
            throw anyException;
        }

        logger.debug("downloadDocHolders ended ");

    }

    /**
     * Upload documents in the stocktracker tables automatically scaning it.
     * <p>
     * Folder Name would be docholder name
     * File Name would be document content name
     * Document search key words would be parent folder to actual folder
     *
     * @param folderPath
     * @throws Exception
     */
    public void uploadFolder(String folderPath, String docOwner) throws Exception {

        //TODO: This bellow method returns worong results needs amendment
        Map<String, List<Path>> fileMap = CommonFileUtil.getDirectoryFileMap(folderPath);

        DocumentContentService contentService = (DocumentContentService) ObjectFactory.getServiceObject(ObjectFactory.DOC_CONTENT_SERVICE);

        fileMap.forEach((directory, fileList) -> {

            File directoryFile = new File(directory);
            DocHolderDO holder = new DocHolderDO();
            //holder.setDocOwner(CommonFileUtil.getFileOwner(directoryFile));
            holder.setDocOwner(docOwner); // Directory upload name
            holder.setDocName(directoryFile.getName());
            holder.setDocKeywords(directory);
            holder.setDocType("OT");
            holder.setDocTypeOthers("Folder Upload");
            holder.setDocPersist("Y");

            holder = (DocHolderDO) create(holder);
            try {
                contentService.uploadFolder(holder, directoryFile.getPath(), holder.getDocOwner());
            } catch (Exception e) {
                e.printStackTrace();
            }

        });
    }


    /**
     * Back up document holders
     *
     * @param backUpFolderPath
     * @throws Exception
     */
    public void backupDocumentHolder(String backUpFolderPath) throws Exception {

        try {
            List<DocHolder> docHolders = docHolderRepository.findAll();
            DocHolderListJSON backupDocHolder = new DocHolderListJSON();

            Map<String, Object> argumentMap = new HashMap<String, Object>();
            argumentMap.put(ArgumentConstants.ARG_MASKDOCMENTS, "Y");

            docHolders.forEach(holder -> {
                DocHolderJSON holderJSON = new DocHolderJSON(argumentMap, holder, backUpFolderPath);
                backupDocHolder.getDocHolderJSONList().add(holderJSON);
            });
            Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().setPrettyPrinting().create();
            try {
                FileUtils.writeStringToFile(new File(backUpFolderPath + File.separator + DocStoreConstants.MAIN_BACKUP_FILE), gson.toJson(backupDocHolder));
            } catch (IOException e) {
                throw e;
            }

        } catch (Exception anyException) {
            anyException.printStackTrace();
            throw anyException;
        }
    }

    /**
     * Restore documents holders
     *
     * @param backUpFolderPath
     * @throws Exception
     */
    public void restoreDocumentHolder(String backUpFolderPath) throws Exception {
        try {
            List<DocHolder> holderList = docHolderRepository.findAll();
            docHolderRepository.deleteAll(holderList);

            try (BufferedReader br = new BufferedReader(new FileReader(backUpFolderPath + File.separator + DocStoreConstants.MAIN_BACKUP_FILE))) {
                Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
                DocHolderListJSON backupDocHolder = gson.fromJson(br, DocHolderListJSON.class);

                backupDocHolder.getDocHolderJSONList().forEach(json -> {
                    json.setDocumentStoreDirPath(backUpFolderPath);
                    json.copyJSONToEntity();
                    DocHolder docHolderEntity = (DocHolder) json.getEntity();
                    docHolderRepository.save(docHolderEntity);
                });

            } catch (IOException e) {
                throw e;
            }
        } catch (Exception anyException) {
            throw anyException;
        }
    }


    @Override
    public Class<DocHolder> getEntityClass() {
        return DocHolder.class;
    }

    @Override
    public DocHolderDO returnDomainObject(BaseEntity entity) {

        if (entity == null) {
            return null;
        }

        DocHolderDO domainObject = new DocHolderDO((DocHolder) entity);
        return domainObject;
    }

    @Override
    public List<DocHolderDO> returnDomainObjects(List<? extends BaseEntity> entities) {

        if (entities == null) {
            return null;
        }

        List<DocHolderDO> domainObjects = new ArrayList<DocHolderDO>();
        if (entities != null) {
            for (IEntity entity : entities) {
                DocHolderDO domainObject = new DocHolderDO((DocHolder) entity);
                domainObjects.add(domainObject);
            }
        }
        return domainObjects;
    }

    @Override
    public BaseEntity returnEntity(DocHolderDO domainObject) {

        DocHolderDO docHolderDO = (DocHolderDO) domainObject;
        DocHolder docHolder = new DocHolder();
        docHolderDO.copyDomainObject2Entity(docHolder);

        return docHolder;
    }

    @Override
    public List<? extends BaseEntity> returnEntities(List<DocHolderDO> domainObjects) {
        List<DocHolder> entities = new ArrayList();
        for (IDomainObject domainObject : domainObjects) {
            DocHolderDO docHolderDO = (DocHolderDO) domainObject;
            DocHolder docHolder = new DocHolder();
            docHolderDO.copyDomainObject2Entity(docHolder);
            entities.add(docHolder);
        }
        return entities;

    }

    @Override
    public TableRowMapDO<String, String> copyDomainObjToTableRowMap(IDomainObject docHolderDO) {
        DocHolderDO docHlders = (DocHolderDO) docHolderDO;
        TableRowMapDO<String, String> tableRowMapDO = new TableRowMapDO<String, String>();
        tableRowMapDO.add("id", docHlders.getId().toString());
        tableRowMapDO.add("docName", docHlders.getDocName());
        tableRowMapDO.add("docOwner", docHlders.getDocOwner());
        tableRowMapDO.add("docKeywords", docHlders.getDocKeywords());
        tableRowMapDO.add("docType", docHlders.getDocType());
        tableRowMapDO.add("docTypeOthers", docHlders.getDocTypeOthers());
        tableRowMapDO.setClazzName(DocHolderDO.CLASS_NAME);

        return tableRowMapDO;
    }

    @Override
    public DocHolderRepository getRepository() {
        return this.docHolderRepository;
    }

}
