/*
 * Copyright (c) 2020.
 *
 * All Rights are with Diwakar C
 */

package com.desktopapp.service;

/**
 * Created by diwakar009 on 1/11/2018.
 */

import com.desktopapp.service.config.AppConfig;
import com.stocktracker.domainobjs.json.CodeDataItemDO;
import com.stocktracker.domainobjs.json.DOPage;
import com.stocktracker.domainobjs.json.StaticCodeDecodeDO;
import com.stocktracker.domainobjs.json.TableRowMapDO;
import com.stocktracker.jpa.spring.extended.StaticCodeDecodeRepository;
import com.stocktracker.jpa.spring.extended.config.ExtendedRepositoryConfig;
import com.stocktracker.model.StaticCodeDecode;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;


import java.util.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { AppConfig.class, ExtendedRepositoryConfig.class})
@Transactional
public class StaticCodeDecodeServiceTest {

    @Autowired
    private StaticCodeDecodeService staticCodeDecodeService;

    @InjectMocks
    private StaticCodeDecodeService staticCodeDecodeMockService;

    @Mock
    private StaticCodeDecodeRepository staticCodeDecodeMockRepository;

    private List<StaticCodeDecode> staticCodeDecodeList;

    @BeforeEach
    public void setup(){

    }



    @Test
    public void testMockAutowire(){
        Assert.assertEquals(true, staticCodeDecodeMockService != null);
    }

    @Test
    public void testAutowire(){
        Assert.assertEquals(true, staticCodeDecodeService != null);
    }

    @Test
    public void testMockStaticRepository(){

        staticCodeDecodeList = new ArrayList<>();
        StaticCodeDecode staticCodeDecode = new StaticCodeDecode();
        staticCodeDecode.setLanguage("EN_US");
        staticCodeDecode.setCodeName("CD_CURRENCY");
        staticCodeDecode.setCodeValue("USD");
        staticCodeDecode.setCodeDesc("US Dollar");
        staticCodeDecodeList.add(staticCodeDecode);

        staticCodeDecode = new StaticCodeDecode();
        staticCodeDecode.setLanguage("EN_US");
        staticCodeDecode.setCodeName("CD_CURRENCY");
        staticCodeDecode.setCodeValue("INR");
        staticCodeDecode.setCodeDesc("Indian Rupees");
        staticCodeDecodeList.add(staticCodeDecode);

        staticCodeDecode = new StaticCodeDecode();
        staticCodeDecode.setLanguage("EN_US");
        staticCodeDecode.setCodeName("CD_CURRENCY");
        staticCodeDecode.setCodeValue("GBP");
        staticCodeDecode.setCodeDesc("Great Briton Pound ");
        staticCodeDecodeList.add(staticCodeDecode);

        Mockito.when(((StaticCodeDecodeRepository) staticCodeDecodeMockRepository).findByCodeNameNLang("CD_CURRENCY", "EN_US"))
                .thenReturn(staticCodeDecodeList);

        Assert.assertEquals(4, staticCodeDecodeMockService.getStaticCodeDataItemList("CD_CURRENCY", "EN_US").get().size());
    }

    @Test
    public void testGetCodeDataItemList_OnlyCodeName() {

        staticCodeDecodeList = new ArrayList<>();
        StaticCodeDecode staticCodeDecode = new StaticCodeDecode();
        staticCodeDecode.setLanguage("EN_US");
        staticCodeDecode.setCodeName("CD_CURRENCY");
        staticCodeDecode.setCodeValue("USD");
        staticCodeDecode.setCodeDesc("US Dollar");
        staticCodeDecodeList.add(staticCodeDecode);

        staticCodeDecode = new StaticCodeDecode();
        staticCodeDecode.setLanguage("EN_US");
        staticCodeDecode.setCodeName("CD_CURRENCY");
        staticCodeDecode.setCodeValue("INR");
        staticCodeDecode.setCodeDesc("Indian Rupees");
        staticCodeDecodeList.add(staticCodeDecode);

        staticCodeDecode = new StaticCodeDecode();
        staticCodeDecode.setLanguage("EN_US");
        staticCodeDecode.setCodeName("CD_CURRENCY");
        staticCodeDecode.setCodeValue("GBP");
        staticCodeDecode.setCodeDesc("Great Briton Pound ");
        staticCodeDecodeList.add(staticCodeDecode);

        Mockito.when(((StaticCodeDecodeRepository) staticCodeDecodeMockRepository).findByCodeNameNLang("CD_CURRENCY", "EN_US"))
                .thenReturn(staticCodeDecodeList);

        Assert.assertEquals(4, staticCodeDecodeMockService.getCodeDataItemList("CD_CURRENCY").get().size());
    }

    @Test
    public void testGetCodeDataItemList_WithFilterCode() {

        staticCodeDecodeList = new ArrayList<>();
        StaticCodeDecode staticCodeDecode = new StaticCodeDecode();
        staticCodeDecode.setLanguage("EN_US");
        staticCodeDecode.setCodeName("CD_CURRENCY");
        staticCodeDecode.setCodeValue("USD");
        staticCodeDecode.setCodeDesc("US Dollar");
        staticCodeDecode.setCodeValueFilter("I");
        staticCodeDecodeList.add(staticCodeDecode);

        staticCodeDecode = new StaticCodeDecode();
        staticCodeDecode.setLanguage("EN_US");
        staticCodeDecode.setCodeName("CD_CURRENCY");
        staticCodeDecode.setCodeValue("GBP");
        staticCodeDecode.setCodeDesc("Great Briton Pound ");
        staticCodeDecode.setCodeValueFilter("I");
        staticCodeDecodeList.add(staticCodeDecode);

        Mockito.when(((StaticCodeDecodeRepository) staticCodeDecodeMockRepository).findByCodeNameAndCodeValueFilter("CD_CURRENCY", "I", "EN_US"))
                .thenReturn(staticCodeDecodeList);

        Assert.assertEquals(3, staticCodeDecodeMockService.getStaticCodeDataItemList("CD_CURRENCY","I","EN_US").get().size());

    }


    @Test
    public void testGetCodeDataItemList_WithoutEmptyCode() {
        staticCodeDecodeList = new ArrayList<>();
        StaticCodeDecode staticCodeDecode = new StaticCodeDecode();
        staticCodeDecode.setLanguage("EN_US");
        staticCodeDecode.setCodeName("CD_CURRENCY");
        staticCodeDecode.setCodeValue("USD");
        staticCodeDecode.setCodeDesc("US Dollar");
        staticCodeDecode.setCodeValueFilter("I");
        staticCodeDecodeList.add(staticCodeDecode);

        staticCodeDecode = new StaticCodeDecode();
        staticCodeDecode.setLanguage("EN_US");
        staticCodeDecode.setCodeName("CD_CURRENCY");
        staticCodeDecode.setCodeValue("GBP");
        staticCodeDecode.setCodeDesc("Great Briton Pound ");
        staticCodeDecode.setCodeValueFilter("I");
        staticCodeDecodeList.add(staticCodeDecode);

        Mockito.when(((StaticCodeDecodeRepository) staticCodeDecodeMockRepository).findByCodeNameAndCodeValueFilter("CD_CURRENCY", "I", "EN_US"))
                .thenReturn(staticCodeDecodeList);

        Assert.assertEquals(2, staticCodeDecodeMockService. getCodeDataItemList("CD_CURRENCY","I","EN_US",true).get().size());
    }


    @Test
    public void testListByCodeNameNLangMock(){

        staticCodeDecodeList = new ArrayList<>();
        StaticCodeDecode staticCodeDecode = new StaticCodeDecode();
        staticCodeDecode.setLanguage("EN_US");
        staticCodeDecode.setCodeName("CD_CURRENCY");
        staticCodeDecode.setCodeValue("USD");
        staticCodeDecode.setCodeDesc("US Dollar");
        staticCodeDecodeList.add(staticCodeDecode);

        staticCodeDecode = new StaticCodeDecode();
        staticCodeDecode.setLanguage("EN_US");
        staticCodeDecode.setCodeName("CD_CURRENCY");
        staticCodeDecode.setCodeValue("INR");
        staticCodeDecode.setCodeDesc("Indian Rupees");
        staticCodeDecodeList.add(staticCodeDecode);

        staticCodeDecode = new StaticCodeDecode();
        staticCodeDecode.setLanguage("EN_US");
        staticCodeDecode.setCodeName("CD_CURRENCY");
        staticCodeDecode.setCodeValue("GBP");
        staticCodeDecode.setCodeDesc("Great Briton Pound ");
        staticCodeDecodeList.add(staticCodeDecode);

        PageImpl pageImpl = new PageImpl(staticCodeDecodeList);
        String filter = "%";
        Mockito.when(staticCodeDecodeMockRepository.listByCodeNameNLangNFilter("CD_CURRENCY","EN_US",(filter == null || "".equals(filter)) ? "%" : filter, PageRequest.of(0,10)))
                .thenReturn(pageImpl);

        Map<String, Object> parameters = new HashMap();
        parameters.put("page","0");
        parameters.put("size","10");
        parameters.put("codeName","CD_CURRENCY");
        parameters.put("language","EN_US");
        parameters.put("search","%");
        DOPage<TableRowMapDO<String, String>> page = staticCodeDecodeMockService.listByCodeNameNLang(parameters);
        Assert.assertEquals(3, page.getNumberOfElements().intValue());
    }

    @Test
    @Rollback(true)
    public void testGetCodeDataItemList() {
        StaticCodeDecode staticCodeDecode = new StaticCodeDecode();
        staticCodeDecode.setCodeName("CD_TEST");
        staticCodeDecode.setLanguage("EN_US");
        staticCodeDecode.setCodeValue("1");
        staticCodeDecode.setCodeDesc("Desc 1");
        staticCodeDecode.setCodeValueFilter("");
        staticCodeDecodeService.create(new StaticCodeDecodeDO(staticCodeDecode));
        Optional<List<CodeDataItemDO>> codeDataItemDOList = staticCodeDecodeService.getCodeDataItemList("CD_TEST");
        Assert.assertEquals(2,codeDataItemDOList.get().size());
    }

    @Test
    @Rollback(true)
    public void testDeleteCodeDecodeItem(){
        StaticCodeDecode staticCodeDecode = new StaticCodeDecode();
        staticCodeDecode.setCodeName("CD_TEST");
        staticCodeDecode.setLanguage("EN_US");
        staticCodeDecode.setCodeValue("1");
        staticCodeDecode.setCodeDesc("Desc 1");
        staticCodeDecode.setCodeValueFilter("");
        staticCodeDecodeService.create(new StaticCodeDecodeDO(staticCodeDecode));
        staticCodeDecodeService.deleteByCodeNameAndLanguage(staticCodeDecode.getCodeName(),staticCodeDecode.getLanguage());
    }

    @Test
    public void testListByCodeNameNLang(){
       Map<String, Object> parameters = new HashMap();
       parameters.put("page","0");
       parameters.put("size","10");
       parameters.put("codeName","CD_CURRENCY");
       parameters.put("language","EN_US");
       parameters.put("filter","%");
       DOPage<TableRowMapDO<String, String>> page = staticCodeDecodeService.listByCodeNameNLang(parameters);
       Assert.assertEquals(6, page.getNumberOfElements().intValue());
    }

}
