## Food Processing Stock Tracker

Food Processing Stock Tracker is a customized end to end solution to manage the stock, purchase , sales and track outturn production of food processing unit.

The project has following modules

•	User Management.
•	Login Management.
•	Document Management
•	Preference Management
•	Static Data Management
•	Stock Maintenance
•	Outturn and Stock Reporting

Screen Shots:
https://bitbucket.org/Diwakar009/stocktrackerapplication/src/master/Product%20ScreenShots


Prerequisite softwares needed for running the application:

1) IntelliJ IDEA 2017.
2) Maven 3.3.*
3) JDK 1.8+
4) MYSQL Database
5) MYSQL Workbench.

Technology used for the project

1) Core Java (JDK 1.8)

2) Java FX 8.40.13

3) Maven

4) JUnit

5) Spring Boot

6) Hibernate JPA

7) CSS

9) MYSQL

6) Jasper Reports

7) Excel Reporting

8) Spring Boot App and Mysql deployed and setup at Pivotal Cloud Foundary.


Implementation Details:

The application specifically designed for both standalone and client server implementation as per infrastructure 
Needs.

  1) For standalone implementation the following modules are used.


                            ----------stocktracker-common----------
                            |                                     |
    stocktracker-springfxui --> stocktracker-service --> stocktracker-data --> MySql Database
                            |                                     |
                            ----------stocktracker-domainobjs------

  2) For client server implementation the following modules are used.


                            -----------------------------------stocktracker-common-------------------------------------
                            |                                                                                          |
    stocktracker-springfxui -->stocktracker-webuiservice-->stocktracker-webservice--> stocktracker-service --> stocktracker-data --> MySql Database
                            |                                                                                          |
                            -----------------------------------stocktracker-domainobjs---------------------------------


1) stocktracker-parent

        This is the maven module consists of 7 maven submodules.

2) stocktracker-common

        This is the maven module have utility classes used across all the submodules.

        Technologies:

        Java 1.8
        Spring
        Junit 3.8.1

3) stocktracker-data

        This is the maven module consist of the spring repository implementation via Hibenate JPA, it has all the necessary entity classes, repository class.

        Technologies:

            Java 1.8
            Spring Boot
            Spring Data
            Hibernate JPA
            MySql Database

4) stocktracker-domainobjs

        Module have data access object (DO) for interchanging of the data and it is used across all the submodules.

        Technologies:

            Java 1.8
            GSON


5) stocktracker-service

        Module which exposes service bussiness methods.
        Technologies:
            Java 1.8
            Spring 5.1.3
            Log4j 1.2.17

6) stocktracker-webservice

        Spring boot module consists of restfull services for the service classes, and tomcat server.

        Technologies:
             Java 1.8
             Spring Boot
             Log4j 1.2.17
             Mockito

7) stocktracker-webuiservice

         Module consists classes which calls restfull service's from the UI clent.

         Technologies:
             Java 1.8
             Spring Web
             Log4j 1.2.17

8) stocktracker-springfxui

         Java Fx UI module.
         Technologies:
              Java 1.8
              Spring Boot
              Java Fx
              CSS
              Iccomon icon factory
              Jasper reports
              POI API