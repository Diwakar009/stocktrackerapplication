/*
 * Copyright (c) 2020.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.jpa.spring.extended.extention.impl;

import com.stocktracker.jpa.spring.extended.StockDetailsRepository;
import com.stocktracker.jpa.spring.extended.bean.MandiReconcilationData;
import com.stocktracker.jpa.spring.extended.bean.StockDetailsSummary;
import com.stocktracker.jpa.spring.extended.config.ExtendedRepositoryConfig;
import com.stocktracker.model.sms.StockDetails;
import junit.framework.TestCase;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { ExtendedRepositoryConfig.class })
public class StockDetailsExtendedRepositoryImplTest extends TestCase {

    @Resource
    private StockDetailsRepository stockDetailsRepository;

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testMandiReportData() throws Exception{
        String industryCode = "KMR";
        List<MandiReconcilationData> mandiStockDetails = stockDetailsRepository.getMandiReconciliationReportData(industryCode, null, null);
        Assert.assertEquals(true,mandiStockDetails.size() > 1);
    }

    @Test
    public void testMandiReportData_FilterLampName() throws Exception{
        String industryCode = "KMR";
        List<MandiReconcilationData> mandiStockDetails = stockDetailsRepository.getMandiReconciliationReportData(industryCode, "LBAD", null);
        Assert.assertEquals(true,mandiStockDetails.size() > 1);
    }

    @Test
    public void testfindAllStockDetailsByFilter_AllFieldsNull() throws Exception{
        Page<StockDetails> stockDetails = stockDetailsRepository.findAllStockDetailsByFilter(null,null,null,null,null,null,null,null,null,null,null,null,null,null);
        Assert.assertTrue(stockDetails != null);
    }

    @Test
    public void testStockDetailSummary() throws Exception{
        List<StockDetailsSummary> summary = stockDetailsRepository.getStockDetailSummary();
        Assert.assertTrue(summary != null);
    }


}