package com.desktopapp.fx.commands;

import com.desktopapp.fx.view.AppView;

/**
 * Created by diwakar009 on 24/12/2018.
 */
public class ThemesCommand implements ICommand {

    private AppView appView;

    public ThemesCommand(AppView appView) {
        this.appView = appView;
    }

    @Override
    public void executeCommand() {
        this.appView.getApplicationViewManager()
                .getPreferenceManager()
                .getPreferencesController()
                .showThemes();
    }
}
