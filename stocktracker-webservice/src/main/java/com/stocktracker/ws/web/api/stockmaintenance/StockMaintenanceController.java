/*
 * Copyright (c) 2020.
 *
 * All Rights are with Diwakar C
 */

package com.stocktracker.ws.web.api.stockmaintenance;

import com.desktopapp.service.AbstractService;
import com.desktopapp.service.stockmaintenance.StockDetailsService;
import com.stocktracker.domainobjs.json.CodeDataItemDO;
import com.stocktracker.domainobjs.json.DOPage;
import com.stocktracker.domainobjs.json.TableRowMapDO;
import com.stocktracker.domainobjs.json.sms.PartyDetailsDO;
import com.stocktracker.domainobjs.json.sms.StockDetailsDO;
import com.stocktracker.util.DateTimeUtil;
import com.stocktracker.ws.web.api.GenericBaseController;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.*;

@RequestMapping(value="/api/stockMaintenance")
@RestController
public class StockMaintenanceController extends GenericBaseController<StockDetailsDO> {

	/**
	 * The StaticCodeDecodeService business service.
	 */
	@Autowired
	private StockDetailsService service;


	@RequestMapping(
			value = "/findSummaryByFilter",
			method = RequestMethod.POST,
			produces = MediaType.APPLICATION_JSON_VALUE)

	public ResponseEntity<DOPage<TableRowMapDO<String,String>>> findSummaryByFilter(@RequestBody TableRowMapDO<String,String> filterMap){
		logger.info("> findSummaryByFilter");

		Integer pageNumber =  1;

		if(filterMap.getValue("page") != null){
			pageNumber = Integer.valueOf(filterMap.getValue("page"));
		}else{
			if(filterMap.getPage() != null){
				pageNumber = Integer.valueOf(filterMap.getPage());
			}
		}

		Integer size = 0;

		if(filterMap.getRow().get("size") != null){
			size = Integer.valueOf(filterMap.getRow().get("size"));
		}else{
			if(filterMap.getSize() != null){
				size = Integer.valueOf(filterMap.getSize());
			}
		}

		Pageable pageRequest = PageRequest.of(pageNumber, size);
		DOPage<TableRowMapDO<String,String>> page = ((StockDetailsService)getService()).findSummaryByFilter(getFilterCriteriaMap(filterMap,pageRequest));

		logger.info("< findSummaryByFilter");

		return  new ResponseEntity<DOPage<TableRowMapDO<String,String>>>(page,HttpStatus.OK);
	}


	@RequestMapping(
			value = "/reteriveStockBalanceByFilter",
			method = RequestMethod.POST,
			produces = MediaType.APPLICATION_JSON_VALUE)

	public ResponseEntity<DOPage<TableRowMapDO<String,String>>> reteriveStockBalanceByFilter(@RequestBody TableRowMapDO<String,String> filterMap){
		logger.info("> reteriveStockBalanceByFilter");

		Integer pageNumber =  1;

		if(filterMap.getValue("page") != null){
			pageNumber = Integer.valueOf(filterMap.getValue("page"));
		}else{
			if(filterMap.getPage() != null){
				pageNumber = Integer.valueOf(filterMap.getPage());
			}
		}

		Integer size = 0;

		if(filterMap.getRow().get("size") != null){
			size = Integer.valueOf(filterMap.getRow().get("size"));
		}else{
			if(filterMap.getSize() != null){
				size = Integer.valueOf(filterMap.getSize());
			}
		}

		Pageable pageRequest = PageRequest.of(pageNumber, size);

		Map<String,Object> parameters = new HashMap<String,Object>();
		parameters.put("page",String.valueOf(pageRequest.getPageNumber()));
		parameters.put("size",String.valueOf(pageRequest.getPageSize()));
		parameters.put("stockCode", filterMap.getValue("stockCode"));
		parameters.put("stockVarietyCode", filterMap.getValue("stockVarietyCode"));
		parameters.put("transactionType", filterMap.getValue("transactionType"));

		DOPage<TableRowMapDO<String,String>> page = ((StockDetailsService)getService()).reteriveStockBalanceByFilter(parameters);

		logger.info("< reteriveStockBalanceByFilter");
		return  new ResponseEntity<DOPage<TableRowMapDO<String,String>>>(page,HttpStatus.OK);
	}

	@RequestMapping(value="/deleteStockDetails",method=RequestMethod.POST,consumes=MediaType.APPLICATION_JSON_VALUE,produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Boolean> deleteStockDetails(@RequestBody StockDetailsDO stockDetailsDO){

		boolean isSuccess = true;
		logger.info("< deleteStockDetails");
		((StockDetailsService)getService()).deleteStockDetails(stockDetailsDO);
		logger.info("> deleteStockDetails");

		if(!isSuccess){
			return new ResponseEntity<Boolean>(isSuccess,HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<Boolean>(isSuccess,HttpStatus.OK);

	}

	@RequestMapping(value="/saveOrUpdateStockDetails",method=RequestMethod.POST,consumes=MediaType.APPLICATION_JSON_VALUE,produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<StockDetailsDO> saveOrUpdateStockDetails(@RequestBody StockDetailsDO stockDetailsDO){
		logger.info("< saveOrUpdateStockDetails");
		StockDetailsDO newStockDetailsDO = ((StockDetailsService)getService()).saveOrUpdateStockDetails(stockDetailsDO);
		logger.info("> saveOrUpdateStockDetails");
		return new ResponseEntity<StockDetailsDO>(newStockDetailsDO,HttpStatus.CREATED);
	}

	@Override
	public Map<String, Object> getFilterCriteriaMap(TableRowMapDO<String, String> filterMap, Pageable pageRequest) {

		Map<String,Object> parameters = super.getFilterCriteriaMap(filterMap, pageRequest);
		parameters.put("stockCode", filterMap.getValue("stockCode"));
		parameters.put("stockVarietyCode", filterMap.getValue("stockVarietyCode"));
		parameters.put("transactionType", filterMap.getValue("transactionType"));
		parameters.put("rstNumber", filterMap.getValue("rstNumber"));
		parameters.put("vehicleNumber", filterMap.getValue("vehicleNumber"));

		String transStartDateStr = filterMap.getValue("transStartDate");
		if(transStartDateStr != null && StringUtils.isNotBlank(transStartDateStr)){
			Date transStartDate = null;
			try {
				transStartDate = DateTimeUtil.convertUTCStringToDate(transStartDateStr);
			}catch (ParseException parseException){
				parseException.printStackTrace();
			}
			parameters.put("transStartDate", transStartDate);
		}

		String transEndDateStr = filterMap.getValue("transEndDate");
		if(transEndDateStr != null && StringUtils.isNotBlank(transEndDateStr)){
			Date transEndDate = null;
			try {
				transEndDate = DateTimeUtil.convertUTCStringToDate(transEndDateStr);
			}catch (ParseException parseException){
				parseException.printStackTrace();
			}
			parameters.put("transEndDate", transEndDate);
		}
		parameters.put("partyName", filterMap.getValue("partyName"));
		parameters.put("onBehalf", filterMap.getValue("onBehalf"));

		return parameters;
	}

	@RequestMapping(
			value = "/suggestPartyNames/{partyName}",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<PartyDetailsDO>> suggestPartyNames(@PathVariable("partyName")String partyName) {
		logger.info("> suggestPartyNames ");
		Optional<List<PartyDetailsDO>> partyDetailsDOS = ((StockDetailsService)getService()).getPartyNameSuggestions(partyName);
		logger.info("< suggestPartyNames ");
		if(partyDetailsDOS.isPresent()){
			return new ResponseEntity<List<PartyDetailsDO>>(partyDetailsDOS.get(),HttpStatus.OK);
		}else{
			return new ResponseEntity<List<PartyDetailsDO>>(new ArrayList<>(),HttpStatus.OK);
		}
	}

	@Override
	public StockDetailsDO save(StockDetailsDO stockDetailsDO) {
		return (StockDetailsDO) service.saveOrUpdateStockDetails(stockDetailsDO);
	}

	@Override
	public boolean delete(StockDetailsDO stockDetailsDO){
		service.remove(stockDetailsDO);
		return true;
	}

	@Override
	public boolean deleteList(List<StockDetailsDO> doList) {
		service.remove(doList);
		return true;
	}

	@Override
	public StockDetailsDO find(StockDetailsDO stockDetailsDO) {
		return service.find(stockDetailsDO);
	}

	@Override
	public AbstractService getService() {
		return service;
	}

}
