/*
 * Copyright (c) 2020.
 *
 * All Rights are with Diwakar C
 */

package com.desktopapp.fx.view.contract;

import com.desktopapp.fx.app.AppFontIcons;
import com.desktopapp.fx.common.IGenericComponent;
import com.desktopapp.fx.control.*;
import com.desktopapp.fx.control.fonticon.FontIcon;
import com.desktopapp.fx.control.multiscreen.StackedScreen;
import com.desktopapp.fx.mvc.AbstractFormView;
import com.desktopapp.fx.mvc.DataPageController;
import com.stocktracker.component.constants.CodeDecodeConstants;
import com.stocktracker.domainobjs.json.sms.ContractDO;
import com.stocktracker.util.MathUtil;
import javafx.concurrent.Task;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;

/**
 * Created by diwakar009 on 3/3/2019.
 */
public class ContractForm extends AbstractFormView<ContractDO> {

    private ContractDO contractDO;

    private CodeDecodeField cdIndustry; // Industry Code
    private DateTimeField dfStartDate; // Start Date of Contract
    private DateTimeField dfEndDate;
    private CodeDecodeField cdContractType; // Contract Type :- KMS, Seeds
    private CodeDecodeField cdSeason; // Rabi or Kharif
    private CodeDecodeField cdKMSYear; // Year
    private CodeDecodeField cdMillerCode; // Miller Code
    private TextFieldExt tfContractName; // IndustryCode_MillerCode_ContractType_Season_KMSYear
    private CodeDecodeField cdDeliveryProduct; // Raw Rice, Boiled Rice
    private CodeDecodeField cdRawMaterial; // Paddy
    private BigDecimalField bfApprovedRawMaterialQuantity; // Approved Total Raw Material in Qtl
    private BigDecimalField bfReceivedRawMaterialQuantity; // Total Received Raw Material in Qtl
    private BigDecimalField bfPendingRawMaterialQuantity;  // Total Pending Raw Material in Qtl

    private BigDecimalField bfProductDeliveryQuantity; // Final Product Delivery Wmt (Rice)
    private BigDecimalField bfProductDeliveredQuantity; // Pending Product Delivery

    private CodeDecodeField cdContractStatus; // Open or Close
    private TextAreaExt taRemarks;

    private ScrollPane scrollPane;

    public ContractForm(DataPageController<ContractDO> controller, ContractDO contractDO, Map<String,Object> intent) {
        super(controller,intent);
        this.contractDO = contractDO;
        loadLatestEntity();
        this.getLblformTitle().setText(getTitle());
        pop();
    }

    public ContractForm(DataPageController<ContractDO> controller, ContractDO contractDO, StackedScreen stackedScreen, Map<String,Object> intent) {
        super(controller,stackedScreen,intent);
        this.contractDO = contractDO;
        loadLatestEntity();
        this.getLblformTitle().setText(getTitle());
        pop();
    }

    @Override
    public Node buildFormView() {
        this.cdIndustry = new CodeDecodeField(CodeDecodeConstants.CD_INDUSTRY_CODE,CodeDecodeConstants.DEFAULT_LANGUAGE);
        this.dfStartDate = new DateTimeField(LocalDate.now());
        this.dfEndDate = new DateTimeField(LocalDate.now());
        this.cdContractType = new CodeDecodeField(CodeDecodeConstants.CD_CONTRACT_TYPE,CodeDecodeConstants.DEFAULT_LANGUAGE);
        this.tfContractName = new TextFieldExt(200);
        this.cdDeliveryProduct = new CodeDecodeField(CodeDecodeConstants.CD_DELIVERY_PRODUCT,CodeDecodeConstants.DEFAULT_LANGUAGE);
        this.cdRawMaterial = new CodeDecodeField(CodeDecodeConstants.CD_RAW_MATERIAL,CodeDecodeConstants.DEFAULT_LANGUAGE);
        this.bfApprovedRawMaterialQuantity = new BigDecimalField(2);
        this.bfProductDeliveryQuantity = new BigDecimalField(2);
        this.bfProductDeliveredQuantity = new BigDecimalField(2);
        this.bfReceivedRawMaterialQuantity = new BigDecimalField(2);
        this.bfPendingRawMaterialQuantity = new BigDecimalField(2);
        this.bfPendingRawMaterialQuantity.setAsReadOnly(true);
        this.cdContractStatus = new CodeDecodeField(CodeDecodeConstants.CD_CONTRACT_STATUS,CodeDecodeConstants.DEFAULT_LANGUAGE);
        this.taRemarks= new TextAreaExt(1000);

        scrollPane = new ScrollPane();
        scrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
        scrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
        scrollPane.fitToWidthProperty().set(true);

        registerComponents(this.cdIndustry,this.dfStartDate,this.dfEndDate,this.cdContractType,
                this.tfContractName,this.cdDeliveryProduct,this.cdRawMaterial,this.bfApprovedRawMaterialQuantity,
                this.bfProductDeliveryQuantity,this.bfProductDeliveredQuantity,
                this.bfReceivedRawMaterialQuantity,this.cdContractStatus,this.bfPendingRawMaterialQuantity);

        int columnIndex = 0;
        int rowIndex = 0;

        BorderPane borderPane = new BorderPane();
        GridPane formPane = new GridPane();
        formPane.setPadding(new Insets(20, 30, 10, 30));
        formPane.setHgap(10);
        formPane.setVgap(10);

        formPane.setGridLinesVisible(false);

        formPane.add(new Label("Mill Name"), columnIndex, rowIndex);
        formPane.add(this.cdIndustry.asControl(), columnIndex + 1, rowIndex);

        formPane.add(new Label("Start Date"), columnIndex+3, rowIndex);
        formPane.add(this.dfStartDate, columnIndex + 4, rowIndex);

        formPane.add(new Label("Contract Type"), columnIndex, rowIndex+1);
        formPane.add(this.cdContractType.asControl(), columnIndex + 1, rowIndex+1);

        formPane.add(new Label("Contract Name"), columnIndex+3, rowIndex+1);
        formPane.add(this.tfContractName, columnIndex + 4, rowIndex+1);

        formPane.add(new Label("Delivery Product"), columnIndex, rowIndex+2);
        formPane.add(this.cdDeliveryProduct.asControl(), columnIndex+1, rowIndex+2);

        formPane.add(new Label("Raw Material"), columnIndex + 3, rowIndex+2);
        formPane.add(this.cdRawMaterial.asControl(), columnIndex + 4, rowIndex+2);

        formPane.add(new Label("Approved Entry"), columnIndex, rowIndex+3);
        formPane.add(this.bfApprovedRawMaterialQuantity, columnIndex+1, rowIndex+3);

        formPane.add(new Label("Received Raw Material"), columnIndex, rowIndex+4);
        formPane.add(this.bfReceivedRawMaterialQuantity, columnIndex+1, rowIndex+4);

        formPane.add(new Label("Pending Raw Material"), columnIndex+ 3, rowIndex+4);
        formPane.add(this.bfPendingRawMaterialQuantity, columnIndex + 4, rowIndex+4);

        formPane.add(new Label("Final Product"), columnIndex, rowIndex+5);
        formPane.add(this.bfProductDeliveryQuantity, columnIndex+1, rowIndex+5);

        formPane.add(new Label("Pending Product Wmt"), columnIndex + 3, rowIndex+5);
        formPane.add(this.bfProductDeliveredQuantity, columnIndex + 4, rowIndex+5);

        formPane.add(new Label("Contract Status"), columnIndex, rowIndex+6);
        formPane.add(this.cdContractStatus.asControl(), columnIndex + 1, rowIndex+6);

        formPane.setAlignment(Pos.CENTER);
        scrollPane.setContent(formPane);
        borderPane.setCenter(scrollPane);
        return borderPane;
    }



    @Override
    public ContractDO getCurrentEntity() {
        return contractDO;
    }

    @Override
    public void loadLatestEntity() {
        contractDO = getController().loadData(contractDO);

        if(contractDO == null){
            contractDO = new ContractDO();
        }
    }

    /**
     * Clears all the data from the formComponents
     *
     */
    public void onClear(){

        for(IGenericComponent component: this.getFormComponents()){
            component.clearDataValue();
        }

        requestFocus();
    }


    public void requestFocus(){
        // Hack code as due to scrollpane request focus has issues.
        new Thread( new Task<Void>()
        {
            @Override
            public Void call() throws Exception // This is NOT on FX thread
            {
                Thread.sleep(200);
                return null;
            }
            @Override
            public void succeeded()  // This is called on FX thread.
            {
                getCdIndustry().requestFocus();
            }

        }).start();

    }

    @Override
    public void pop() {
        this.setDOToControls(contractDO);
        this.requestFocus();
    }

    /**
     * Set DO to Controls
     *
     * @param contractDO
     */
    public void setDOToControls(ContractDO contractDO){
        cdIndustry.setDataValue(contractDO.getIndustryCode());
        cdContractStatus.setDataValue(contractDO.getStatus());
        cdContractType.setDataValue(contractDO.getContractTypeCode());
        cdDeliveryProduct.setDataValue(contractDO.getDeliveryProduct());
        cdRawMaterial.setDataValue(contractDO.getRawMaterial());
        tfContractName.setDataValue(contractDO.getContractName());
        dfStartDate.setDataValue(contractDO.getStartDate());
        dfEndDate.setDataValue(contractDO.getEndDate());


        if(contractDO.getApprovedRawMaterialQuantity() != null) {
            bfApprovedRawMaterialQuantity.setDataValue(new BigDecimal(contractDO.getApprovedRawMaterialQuantity()));
        }

        if(contractDO.getReceivedRawMaterialQuantity() != null) {
            bfReceivedRawMaterialQuantity.setDataValue(new BigDecimal(contractDO.getReceivedRawMaterialQuantity()));
            bfPendingRawMaterialQuantity.setDataValue(new BigDecimal(MathUtil.substract(contractDO.getApprovedRawMaterialQuantity(),contractDO.getReceivedRawMaterialQuantity())));
        }

        if(contractDO.getProductDeliveryQuantity() != null) {
            bfProductDeliveryQuantity.setDataValue(new BigDecimal(contractDO.getProductDeliveryQuantity()));
        }

        if(contractDO.getProductDeliveredQuantity() != null) {
            bfProductDeliveredQuantity.setDataValue(new BigDecimal(contractDO.getProductDeliveredQuantity()));
        }

        taRemarks.setDataValue(contractDO.getRemarks());

    }

    /**
     * Set controls to DO
     *
     * @param contractDO
     */
    public void setControlsToDO(ContractDO contractDO){
        contractDO.setIndustryCode(cdIndustry.getDataValue());
        contractDO.setStatus(cdContractStatus.getDataValue());
        contractDO.setContractTypeCode(cdContractType.getDataValue());
        contractDO.setDeliveryProduct(cdDeliveryProduct.getDataValue());
        contractDO.setContractName(tfContractName.getDataValue());
        contractDO.setRawMaterial(cdRawMaterial.getDataValue());
        contractDO.setStartDate(dfStartDate.getDataValueAsDate());
        contractDO.setEndDate(dfEndDate.getDataValueAsDate());


        if(bfApprovedRawMaterialQuantity.getDataValue().isEmpty()) {
            contractDO.setApprovedRawMaterialQuantity(0.0d);
        }else{
            contractDO.setApprovedRawMaterialQuantity(Double.parseDouble(bfApprovedRawMaterialQuantity.getDataValue()));
        }

        if(bfReceivedRawMaterialQuantity.getDataValue().isEmpty()) {
             contractDO.setReceivedRawMaterialQuantity(0.0d);
        }else{
            contractDO.setReceivedRawMaterialQuantity(Double.parseDouble(bfReceivedRawMaterialQuantity.getDataValue()));
        }


        if(bfProductDeliveryQuantity.getDataValue().isEmpty()) {
            contractDO.setProductDeliveryQuantity(0.0d);
        }else{
            contractDO.setProductDeliveryQuantity(Double.parseDouble(bfProductDeliveryQuantity.getDataValue()));
        }

        if(bfProductDeliveredQuantity.getDataValue().isEmpty()) {
            contractDO.setProductDeliveredQuantity(0.0d);
        }else{
            contractDO.setProductDeliveredQuantity(Double.parseDouble(bfProductDeliveredQuantity.getDataValue()));
        }
        contractDO.setRemarks(taRemarks.getDataValue());
    }

    @Override
    public void push() {
        setControlsToDO(contractDO);
    }

    @Override
    public void refreshForm() {

    }

    @Override
    public FontIcon getFontIcon() {
        return AppFontIcons.STOCKMANAGEMENT;
    }

    @Override
    public String getTitle() {
        return !isEditMode() ? "Add New Contract" : "Edit Contract";
    }

    @Override
    public Node asNode() {
        return this;
    }

    @Override
    public List<ActionCommand> addMoreActionCommands(List<ActionCommand> commands) {
        return commands;
    }

    public ContractDO getContractDO() {
        return contractDO;
    }

    public void setContractDO(ContractDO contractDO) {
        this.contractDO = contractDO;
    }

    public CodeDecodeField getCdIndustry() {
        return cdIndustry;
    }
}
