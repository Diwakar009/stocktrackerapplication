/*
 * Copyright (c) 2020.
 *
 * All Rights are with Diwakar C
 */

package com.desktopapp.service.stockmaintenance;

import com.desktopapp.service.AbstractService;
import com.stocktracker.domainobjs.json.*;
import com.stocktracker.domainobjs.json.sms.PartyDetailsDO;
import com.stocktracker.domainobjs.json.sms.StockDetailsDO;
import com.stocktracker.domainobjs.json.sms.StockDetailsSummaryDO;
import com.stocktracker.jpa.spring.extended.bean.StockDetailsSummary;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.*;


/**
 * Stock Details Service
 *
 * @author Ratnala Diwakar Choudhury
 */
@Service
public class StockDetailsService extends AbstractService<StockDetailsDO> {

	public StockDetailsService() {
        super(StockDetailsDO.class);
    }
    
	public static final String BASE_RESOURCE = "stockMaintenance";

	public DOPage<TableRowMapDO<String, String>> findSummaryByFilter(Map<String, Object> parameters) {
		return getListWithPostQuery("findSummaryByFilter",parameters);
	}

	public DOPage<TableRowMapDO<String, String>> reteriveStockBalanceByFilter(Map<String, Object> parameters) {
		return getListWithPostQuery("reteriveStockBalanceByFilter",parameters);
	}

	public ResultDO deleteStockDetails(StockDetailsDO stockDetailsDO) {
		return getRestTemplate().postForObject(getWebserviceURL() + "/deleteStockDetails", stockDetailsDO, ResultDO.class);
	}

	public StockDetailsDO saveOrUpdateStockDetails(StockDetailsDO stockDetailsDO){
		return postForChange(getWebserviceURL() + "/saveOrUpdateStockDetails",stockDetailsDO,StockDetailsDO.class);
	}

	public Optional<List<PartyDetailsDO>> getPartyNameSuggestions(String partyName){
		List<PartyDetailsDO> partyDetailsDOList = null;
		String getCallWebURL = getWebserviceURL() + "/suggestPartyNames/{partyName}";
		Map<String, String> params = new HashMap<String, String>();
		params.put("partyName", partyName);

		PartyDetailsDO[] partyDetailsDOS = (PartyDetailsDO[])callService(getCallWebURL,PartyDetailsDO[].class,params);

		if(partyDetailsDOS != null && partyDetailsDOS.length != 0){
			partyDetailsDOList = Arrays.asList(partyDetailsDOS);
		}else{
			return Optional.of(partyDetailsDOList);
		}
		return Optional.of(partyDetailsDOList);
	}


    @Override
	protected String getBaseResource() {
		return BASE_RESOURCE;
	}

}
