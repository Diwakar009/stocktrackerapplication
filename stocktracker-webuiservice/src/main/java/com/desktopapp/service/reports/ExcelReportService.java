/*
 * Copyright (c) 2020.
 *
 * All Rights are with Diwakar C
 */

package com.desktopapp.service.reports;

import com.desktopapp.service.AbstractService;
import com.stocktracker.domainobjs.json.DocContentDO;
import com.stocktracker.domainobjs.json.reports.ExcelReportDO;
import com.stocktracker.domainobjs.json.sms.ContractDO;
import com.stocktracker.domainobjs.json.sms.StockDetailsDO;
import com.stocktracker.exceptions.BusinessException;
import com.stocktracker.exceptions.TechnicalException;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.Map;


/**
 * Excel Report Service
 *
 * @author Ratnala Diwakar Choudhury
 */
@Service
public class ExcelReportService extends AbstractService<ExcelReportDO>{

	public ExcelReportService() {
			super(ExcelReportDO.class);
    }

 	public static final String BASE_RESOURCE = "excelreport";

  	protected String getBaseResource() {
		return BASE_RESOURCE;
	}

	public ByteArrayOutputStream reteriveMandiReconReport(String industryCode, String lampNameCode, String mandiNameCode) throws BusinessException, TechnicalException {
		String getCallWebURL = getWebserviceURL() + "/mandiReconReport/" + industryCode;
		byte[] response = getRestTemplate().postForObject(getCallWebURL, new ExcelReportDO(), byte[].class);
		ByteArrayOutputStream baos = new ByteArrayOutputStream(response.length);
		baos.write(response, 0, response.length);
		return baos;
	}

	public ByteArrayOutputStream reteriveStockDetailTransactionsReport(Map<String, Object> parameters) {
		return null;
	}
}
