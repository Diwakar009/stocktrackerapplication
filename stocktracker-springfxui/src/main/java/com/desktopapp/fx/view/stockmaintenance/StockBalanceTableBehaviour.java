/*
 * Copyright (c) 2020.
 *
 * All Rights are with Diwakar C
 */

package com.desktopapp.fx.view.stockmaintenance;

import com.desktopapp.fx.app.AppFontIcons;
import com.desktopapp.fx.common.ITableDataViewBehaviour;
import com.desktopapp.fx.common.PageDetails;
import com.desktopapp.fx.control.TableColumnBuilder;
import com.desktopapp.fx.control.fonticon.FontIcon;
import com.desktopapp.fx.controller.StockMaintenanceController;
import com.stocktracker.component.constants.CodeDecodeConstants;
import com.stocktracker.domainobjs.json.DOPage;
import com.stocktracker.domainobjs.json.TableRowMapDO;
import com.stocktracker.domainobjs.json.sms.StockDO;
import com.stocktracker.domainobjs.json.sms.StockDetailsDO;
import com.stocktracker.util.DateTimeUtil;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.scene.Node;
import javafx.scene.control.TableColumn;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Stock Details Table properties and behaviour
 */
class StockBalanceTableBehaviour implements ITableDataViewBehaviour<StockDO> {

    private final Logger logger = LoggerFactory.getLogger(StockBalanceTableBehaviour.class);

    private  StockDetailsTransactionSearchPage page;

    public StockBalanceTableBehaviour(StockDetailsTransactionSearchPage page) {
        this.page = page;
    }

    @Override
    public List<TableColumn<StockDO, ?>> getTableViewColumns() {

        List<TableColumn<StockDO, ?>> columns = new ArrayList<>();

        TableColumn<StockDO, String> industryCodeCol = TableColumnBuilder.<StockDO, String> create()
                .fieldName("industryCode")
                .decodeCode(CodeDecodeConstants.CD_INDUSTRY_CODE)
                .title("Industry Code")
                .build();

        columns.add(industryCodeCol);

        TableColumn<StockDO, String> stockCodeCol = TableColumnBuilder.<StockDO, String> create()
                .fieldName("stockCode")
                .decodeCode(CodeDecodeConstants.CD_STOCK_CODE)
                .title("Stock Code")
                .build();

        columns.add(stockCodeCol);

        TableColumn<StockDO, String> stockVarietyCodeCol = TableColumnBuilder.<StockDO, String> create()
                .fieldName("stockVarietyCode")
                .decodeCode(CodeDecodeConstants.CD_STOCK_VARIETY)
                .title("Stock Variety Code")
                .build();

        columns.add(stockVarietyCodeCol);

        TableColumn<StockDO, Long> noOfPacketsSummaryCol = TableColumnBuilder.<StockDO, Long> create()
                .title("Packets Balance")
                .build();

        noOfPacketsSummaryCol.setCellValueFactory(cellvaluefac -> {
            if(cellvaluefac.getValue().getTotalNoOfPackets() != null) {
                return new ReadOnlyObjectWrapper(cellvaluefac.getValue().getTotalNoOfPackets() + " " + TableColumnBuilder.getNoOfPacketsUnit(cellvaluefac.getValue().getStockCode(),
                        cellvaluefac.getValue().getStockVarietyCode()));
            }
            return new ReadOnlyObjectWrapper("");
        });

        columns.add(noOfPacketsSummaryCol);

        TableColumn<StockDO, Double> weightmentSummaryCol = TableColumnBuilder.<StockDO, Double> create()
                .title("Weightment Balance")
                .build();

        weightmentSummaryCol.setCellValueFactory(cellvaluefac -> {
            if(cellvaluefac.getValue().getTotalWeightment() != null) {
                return new ReadOnlyObjectWrapper(cellvaluefac.getValue().getTotalWeightment() + " " + TableColumnBuilder.getWeightmentUnit(cellvaluefac.getValue().getStockCode(),
                        cellvaluefac.getValue().getStockVarietyCode()));
            }
            return new ReadOnlyObjectWrapper("");
        });

        columns.add(weightmentSummaryCol);


        TableColumn<StockDO, Date> asOfDateCol = TableColumnBuilder.<StockDO, Date> create()
                .fieldName("asOnDate")
                .title("As on Date")
                .dataType(TableColumnBuilder.DATATYPE_DATE)
                .build();

        columns.add(asOfDateCol);

        return columns;
    }

    @Override
    public DOPage<TableRowMapDO<String, String>> getData(String filter, PageDetails details) {
        return ((StockMaintenanceController)getPage().getController()).getStockBalanceData(getPage().getCdIndustry().getDataValue(),
                getPage().getCdStock().getDataValue(),
                getPage().getCdStockVariety().getDataValue(),
                details);
    }

    @Override
    public StockDO mapTableRowToDO(TableRowMapDO<String,String> tableRowMapDO) {
        StockDO stockDO = new StockDO();
        stockDO.setIndustryCode(tableRowMapDO.getValue("industryCode"));
        stockDO.setStockCode(tableRowMapDO.getValue("stockCode"));
        stockDO.setStockVarietyCode(tableRowMapDO.getValue("stockVarietyCode"));
        if(tableRowMapDO.getValue("noOfPacketsSummary") != null) {
            stockDO.setTotalNoOfPackets(Long.parseLong(tableRowMapDO.getValue("noOfPacketsSummary")));
        }
        if(tableRowMapDO.getValue("weightmentSummary") != null) {
            stockDO.setTotalWeightment(Double.parseDouble(tableRowMapDO.getValue("weightmentSummary")));
        }

        try {
            Date asOnDate = DateTimeUtil.convertUTCStringToDate(tableRowMapDO.getValue("asOnDate"));
            stockDO.setAsOnDate(asOnDate);
        }catch (ParseException e){
            logger.error("As of date parse error ' " + tableRowMapDO.getValue("asOfDate") + "'");
        }
        return stockDO;
    }

    @Override
    public void onSelectedItemChanged() {

    }

    @Override
    public Node asNode() {
        return null;
    }

    @Override
    public FontIcon getFontIcon() {
        return AppFontIcons.STOCKMANAGEMENT;
    }

    @Override
    public String getTitle() {
        return "Stock Balance Summary";
    }

    public StockDetailsTransactionSearchPage getPage() {
        return page;
    }

    public void setPage(StockDetailsTransactionSearchPage form) {
        this.page = form;
    }
}
